const PROXY_CONFIG = [
  {
    context: ['/tce-auth-api', '/tce-teach-api', '/tce-repo-api', '/tce-report-api','/tce-school-api', '/tce-usage-report-local'],
    target: 'http://172.18.1.57:80',
    secure: false,
    onProxyRes: function(proxyRes, req, res) {
      proxyRes.headers['www-authenticate'] = 'none';
    }
  }
];

module.exports = PROXY_CONFIG;
