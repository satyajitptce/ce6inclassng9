module.exports = {
  name: 'tce-teacher',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/tce-teacher',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
