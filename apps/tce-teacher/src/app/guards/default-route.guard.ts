import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AppConfigService, CommonService } from '@tce/core';
import { AuthenticationService } from '@tce/core';
import { AuthStateEnum } from '@tce/core';

@Injectable()
export class DefaultRouteGuard implements CanActivate {
  isLoggedIn = false;
  routePath = '';
  constructor(
    private router: Router,
    private appConfigService: AppConfigService,
    private authService: AuthenticationService,
    private commonService: CommonService
  ) {
    this.authService.userState$.subscribe(authState => {
      console.log(authState);
      this.isLoggedIn = authState.authStatus === AuthStateEnum.AUTH;
    });
    //added Thiru oct
    this.commonService.classroomModeState$.subscribe(state => {
      console.log('state', state);
      if (state) {
        this.routePath = '/canvas';
      } else {
        this.routePath = '/plan';
      }
      console.log('routePath', this.routePath);
    });
  }

  canActivate(): boolean {
    console.log('canActivate', this.isLoggedIn);
    console.log('routePath', this.routePath);
    if (this.isLoggedIn) {
      this.routePath = this.appConfigService.getCmodeConfig().postloginroute;
      console.log('routePath', this.routePath);
    } else {
      this.routePath = '/canvas';
    }

    if (this.routePath) {
      console.log('if', this.routePath);
      //default
      //this.router.navigate([this.routePath]);
      if(this.routePath === 'admin'){
        this.routePath = 'http://172.18.1.57/admin/index.html';
        this.router.navigate([this.routePath]);
      }
      else{
       this.router.navigate([this.routePath]);

      }
     
    } else {
      console.log('else', this.routePath);
      return true;
    }
  }
}
