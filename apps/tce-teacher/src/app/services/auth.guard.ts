﻿import { Injectable } from '@angular/core';
import {
  Router,
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';
import { AuthenticationService, AuthStateEnum, CommonService } from '@tce/core';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
  isLoggedIn = false;
  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private commonService: CommonService
  ) {
    this.authService.userState$.subscribe(authState => {
      console.log('authState', authState);
      this.isLoggedIn = authState.authStatus === AuthStateEnum.AUTH;
    });
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    console.log('AuthGuard canActivate', this.isLoggedIn);
    if (this.isLoggedIn) {
      // this.commonService.setClassroomModeState(true)
      this.router.navigate(['/olan']);
    }
    return true;
  }
}
