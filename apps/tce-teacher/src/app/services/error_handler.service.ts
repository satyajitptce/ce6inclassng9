import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ErrorHandler {
  constructor() {}

  public handleSuccess(res: any) {
    //success block
  }

  public handleError(err: any) {
    switch (err) {
      case 401:
        throw new Error('401 Status!!1' + err);
      default:
        break;
    }
  }
}
