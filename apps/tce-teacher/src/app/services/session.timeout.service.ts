import { Injectable } from '@angular/core';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Keepalive } from '@ng-idle/keepalive';
import { ReplaySubject, Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionTimeoutService {
  idleState = '';
  timedOut = false;

  private showIdle = new ReplaySubject<Boolean>(1);
  public showIdle$ = this.showIdle.asObservable();

  private onTimeout = new ReplaySubject(1);
  public onTimeout$ = this.onTimeout.asObservable();

  public idleIsSet = false;

  private _subscriptions = new Subscription();

  constructor(private idle: Idle, private keepalive: Keepalive) {}

  makeSessionTimeout(callee: string) {
    // sets an idle timeout of 5 seconds, for testing purposes.
    this.idle.setIdle(29 * 60);
    // sets a timeout period of 5 seconds. after 10 seconds of inactivity, the user will be considered timed out.
    this.idle.setTimeout(60);
    // sets the default interrupts, in this case, things like clicks, scrolls, touches to the document
    this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
    this._subscriptions.add(
      this.idle.onTimeout.subscribe(() => {
        this.onTimeout.next();
      })
    );
    this._subscriptions.add(
      this.idle.onTimeoutWarning.subscribe(countdown => {
        console.log("countdown",countdown)
        this.showIdle.next(true);
        this.idleState = countdown + ' seconds';
      })
    );
    // sets the ping interval to 15 seconds
    // this.keepalive.interval(15);

    this.idleIsSet = true;

    this.reset();
  }

  reset() {
    this.idle.watch();
  }

  stop() {
    this.idle.stop();
    this.idleIsSet = false;
    this._subscriptions.unsubscribe();
  }

  setIdleScreen(value: boolean, callee: string) {
    this.showIdle.next(value);
  }
}
