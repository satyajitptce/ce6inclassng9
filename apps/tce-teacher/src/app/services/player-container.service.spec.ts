import { TestBed } from '@angular/core/testing';

import { PlayerContainerService } from './player-container.service';

describe('PlayerContainerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlayerContainerService = TestBed.get(PlayerContainerService);
    expect(service).toBeTruthy();
  });
});
