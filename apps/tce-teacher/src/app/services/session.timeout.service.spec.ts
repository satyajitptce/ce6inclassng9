/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SessionTimeoutService } from './session.timeout.service';

describe('Service: Session.timeout', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionTimeoutService]
    });
  });

  it('should ...', inject(
    [SessionTimeoutService],
    (service: SessionTimeoutService) => {
      expect(service).toBeTruthy();
    }
  ));
});
