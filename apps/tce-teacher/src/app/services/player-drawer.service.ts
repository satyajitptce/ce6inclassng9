import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import {
  ToolbarService,
  ContentResizeService,
  AppConfigService
} from '@tce/core';
import { delay } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayerDrawerService {
  private _preFocusModeDrawerOpenStatus = false;
  private _drawerOpenStatus = false;
  private drawerOpenStatus = new ReplaySubject<boolean>(1);
  public drawerOpenStatus$ = this.drawerOpenStatus.asObservable();

  private drawerVisibilityStatus = new ReplaySubject<boolean>(1);
  public drawerVisibilityStatus$ = this.drawerVisibilityStatus.asObservable();

  private _curriculumPlaylistHeight = 0;

  constructor(
    private toolbarService: ToolbarService,
    private contentResizeService: ContentResizeService,
    private appConfigService: AppConfigService
  ) {
    // 350 delay allows for transitions to occur.
    let animationTiming = this.appConfigService.getAnimationTimings().medium;
    if (animationTiming > 0) {
      animationTiming += 50;
    }
    this.drawerOpenStatus$.pipe(delay(animationTiming)).subscribe(() => {
      contentResizeService.triggerResize();
    });
    this.toolbarService.focusMode$.subscribe(focusState => {
      if (focusState) {
        this._preFocusModeDrawerOpenStatus = this._drawerOpenStatus;
        this.drawerVisibilityStatus.next(false);
        this.setDrawerOpenStatus(false);
      } else {
        this.setDrawerOpenStatus(this._preFocusModeDrawerOpenStatus);
        this.drawerVisibilityStatus.next(true);
      }
    });
  }

  setDrawerOpenStatus(status: boolean) {
    this._drawerOpenStatus = status;
    this.drawerOpenStatus.next(this._drawerOpenStatus);
    this.setToolbarAvailableHeightReduction();
  }

  setToolbarAvailableHeightReduction() {
    if (this._curriculumPlaylistHeight !== 0) {
      let newValue = 0;
      if (this._drawerOpenStatus) {
        newValue = this._curriculumPlaylistHeight;
      }
      this.toolbarService.setToolbarAvailableHeightReduction(newValue);
    }
  }

  public set curriculumPlaylistHeight(value: number) {
    this._curriculumPlaylistHeight = value;
    this.setToolbarAvailableHeightReduction();
  }

  public get curriculumPlaylistHeight() {
    return this._curriculumPlaylistHeight;
  }
}
