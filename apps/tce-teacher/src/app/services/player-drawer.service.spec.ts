import { TestBed } from '@angular/core/testing';

import { PlayerDrawerService } from './player-drawer.service';

describe('PlayerDrawerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PlayerDrawerService = TestBed.get(PlayerDrawerService);
    expect(service).toBeTruthy();
  });
});
