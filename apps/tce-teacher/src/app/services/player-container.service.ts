import { Injectable } from '@angular/core';
import {
  Resource,
  AppConfigService,
  AppStateStorageService,
  CurriculumPlaylistService,
  AuthenticationService,
  Ebook,
  PanZoomService,
  GlobalConfigState,
  ToolbarService,
  FileUploadService
} from '@tce/core';
import { ReplaySubject, Observable, BehaviorSubject, Subject } from 'rxjs';
import { PlayerConfigType } from '@app-teacher/models/enums/player-config-type.enum';
import { zip } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayerContainerService {
  currentAccessedResources: Resource[] = [];
  currentOpenedResources: Resource[] = [];
  customResourceFlag: any;
  private accessedResourcesSubject: ReplaySubject<
    Resource[]
  > = new ReplaySubject<Resource[]>(1);
  public accessedResources$: Observable<
    Resource[]
  > = this.accessedResourcesSubject.asObservable();

  private openResourcesSubject: ReplaySubject<Resource[]> = new ReplaySubject<
    Resource[]
  >(1);
  public openResources$: Observable<
    Resource[]
  > = this.openResourcesSubject.asObservable();

  private addOpenedResourceSubject: Subject<Resource> = new Subject<Resource>();
  private reopenResourceSubject: ReplaySubject<Resource> = new ReplaySubject<
    Resource
  >(1);
  private removedResourceSubject: ReplaySubject<Resource> = new ReplaySubject<
    Resource
  >(1);

  private destroyAllPlayersSubject: ReplaySubject<boolean> = new ReplaySubject<
    boolean
  >(1);
  public destroyAllPlayers$: Observable<
    boolean
  > = this.destroyAllPlayersSubject.asObservable();

  public addOpenedResource$: Observable<
    Resource
  > = this.addOpenedResourceSubject.asObservable();
  public reopenResource$: Observable<
    Resource
  > = this.reopenResourceSubject.asObservable();
  public removedResource$: Observable<
    Resource
  > = this.removedResourceSubject.asObservable();

  private currentActiveResourceIdSubject: BehaviorSubject<
    string
  > = new BehaviorSubject<string>('');
  public = this.currentActiveResourceIdSubject.asObservable();

  public currentActiveResourceId$: Observable<
    string
  > = this.currentActiveResourceIdSubject.asObservable();

  private addClosedResourceSubject: Subject<Resource> = new Subject<Resource>();
  public addClosedResource$: Observable<
    Resource
  > = this.addClosedResourceSubject.asObservable();

  playerBehaviorConfig: PlayerConfigType;

  private autoSelectToolOnResourceActive = false;
  private playerTypesData = null;

  constructor(
    private appConfigService: AppConfigService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private appStateStorageService: AppStateStorageService,
    private authService: AuthenticationService,
    private panZoomService: PanZoomService,
    private toolbarService: ToolbarService,
    private fileUploadService: FileUploadService
  ) {
    this.curriculumPlaylistService.availableCustomResources$.subscribe(
      availableResources => {
        
        this.customResourceFlag = availableResources;
      }
    );
    this.fileUploadService.updateCustomResourceFlag$.subscribe(data => {
      if (data) {
        if (
          this.currentAccessedResources &&
          this.currentAccessedResources.length > 0
        ) {
          for (
            let index = 0;
            index < this.currentAccessedResources.length;
            index++
          ) {
            if (this.currentAccessedResources[index].resourceId === data) {
              this.currentAccessedResources.splice(index, 1);
            }
          }
        }
      }
    });
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      fullContentSelection => {
        if (fullContentSelection) {
          this.destroyAllResources();
        }
      }
    );

    this.panZoomService.explicitPan$.subscribe(() => {
      this.setCurrentActiveResourceId('');
    });

    this.playerBehaviorConfig = this.appConfigService.getGlobalSettingConfig(
      'playerbehaviour'
    );

    this.authService.logoutTriggered$.subscribe(() => {
      this.saveCurrentPlayerState();
    });

    this.appStateStorageService.saveToWhiteBoard$.subscribe(() => {
      this.saveCurrentPlayerState();
    });

    this.toolbarService.clearWhiteboardBroadcaster$.subscribe(() => {
      this.destroyAllResources();
    });

    // zip(
    //   this.curriculumPlaylistService.fullContentSelection$,
    //   this.curriculumPlaylistService.availableResources$
    // ).subscribe(([fullContentSelection, availableResources]) => {
    //   console.log("PlayerContainerService -> availableResources", availableResources)
    //   if (fullContentSelection) {

    //     this.loadPlayersFromStorage(availableResources);
    //   }
    // });

    this.curriculumPlaylistService.availableResources$.subscribe(
      availableResources => {
        if (
          availableResources &&
          availableResources.length > 0 &&
          this.customResourceFlag
        ) {
          //console.log("PlayerContainerService -> availableResources", availableResources)
          this.loadPlayersFromStorage(availableResources);
        }
      }
    );

    if (
      this.appConfigService.getConfig('global_setting')['toolbarAutoSelect'] ===
      GlobalConfigState.ENABLED
    ) {
      this.playerTypesData = this.appConfigService.getConfig('general')[
        'playerTypesData'
      ];
      this.autoSelectToolOnResourceActive = true;
    }
  }

  saveCurrentPlayerState() {
    //console.log('TCL: PlayerContainerService--saveCurrentPlayerState');
    const playersState = this.getCurrentPlayersState();
    // console.log("PlayerContainerService -> saveCurrentPlayerState -> playersState", playersState)
    if (playersState && playersState.length > 0) {
      this.appStateStorageService.saveToLocalStorage({ players: playersState });
    }
  }

  setCurrentActiveResourceId(resourceId: string) {
    if (this.currentActiveResourceIdSubject.getValue() !== resourceId) {
      this.currentActiveResourceIdSubject.next(resourceId);
      if (resourceId && this.autoSelectToolOnResourceActive) {
        this.selectToolForActiveResource();
      }
    }
  }

  private selectToolForActiveResource() {
    if (
      this.currentActiveResourceIdSubject.getValue() &&
      this.currentAccessedResources
    ) {
      const currentResource = this.currentAccessedResources.find(resource => {
        return (
          resource.resourceId === this.currentActiveResourceIdSubject.getValue()
        );
      });
      if (currentResource && this.playerTypesData) {
        const uniqueArray = [];
        let autoSelectedTool: any;
        this.playerTypesData.forEach((item, index) => {
          if (uniqueArray.findIndex(i => i.type === item.type) === -1) {
            uniqueArray.push(item);
          }
        });
        if (uniqueArray.length > 0) {
          for (let index = 0; index < uniqueArray.length; index++) {
            if (currentResource.resourceType === uniqueArray[index].type) {
              autoSelectedTool = uniqueArray[index].defaultTool;
            }
          }
          if (autoSelectedTool) {
            this.toolbarService.selectTool(autoSelectedTool);
          }
        }
      }
    }
  }

  openResource(resourceToAdd: Resource | null, activateResource = true) {
    
    if (!resourceToAdd) {
      return;
    }
    //    console.log('this.currentAccessedResources', this.currentAccessedResources);
    const accessedResource = this.currentAccessedResources.find(value => {
      return value.resourceId === resourceToAdd.resourceId;
    });
    //console.log("🚀 ~ file: player-container.service.ts ~ line 238 ~ PlayerContainerService ~ openResource ~ accessedResource", accessedResource)
    if (
      this.playerBehaviorConfig === PlayerConfigType.ANY_PLAYER_SINGLE_INSTANCE
    ) {
      this.destroyAllResources();
    } else if (
      this.playerBehaviorConfig ===
      PlayerConfigType.MULTI_PLAYER_SINGLE_INSTANCE
    ) {
      const existingResourcesOfType = this.currentAccessedResources.filter(
        resource => resource.resourceType === resourceToAdd.resourceType
      );
      for (const resource of existingResourcesOfType) {
        this.closeResource(resource);
      }
    }
    // If resource to add is already in the accessed resource list, we just need to add it to the open lists.
    if (accessedResource) {
      this.reopenResourceSubject.next(accessedResource);
      this.currentOpenedResources.push(accessedResource);
      this.openResourcesSubject.next(this.currentOpenedResources);
      // Else add the resource as an opened resource.
    } else {
      this.currentAccessedResources.push(resourceToAdd);
      this.currentOpenedResources.push(resourceToAdd);
      this.addOpenedResourceSubject.next(resourceToAdd);
      this.accessedResourcesSubject.next(this.currentAccessedResources);
      this.openResourcesSubject.next(this.currentOpenedResources);
    }
    if (activateResource) {
      this.setCurrentActiveResourceId(resourceToAdd.resourceId);
    }
  }

  addClosedResource(resourceToAdd: Resource | null) {
    if (!resourceToAdd) {
      return;
    }
    const resourceAlreadyAccessed = this.currentAccessedResources.some(
      value => {
        return value.resourceId === resourceToAdd.resourceId;
      }
    );
    if (
      resourceAlreadyAccessed ||
      this.playerBehaviorConfig === PlayerConfigType.ANY_PLAYER_SINGLE_INSTANCE
    ) {
      return;
    }
    this.currentAccessedResources.push(resourceToAdd);
    this.accessedResourcesSubject.next(this.currentAccessedResources);
    this.addClosedResourceSubject.next(resourceToAdd);
  }

  closeResource(resourceToRemove: Resource) {
    //console.log("PlayerContainerService -> closeResource -> resourceToRemove", resourceToRemove)
    //console.log("this.currentOpenedResources",this.currentOpenedResources)
    //console.log("this.currentActiveResourceIdSubject",this.currentActiveResourceIdSubject.getValue())
    let itemWasRemoved = false;
    if (
      resourceToRemove.resourceId ===
      this.currentActiveResourceIdSubject.getValue()
    ) {
      //console.log("step1")
      this.setCurrentActiveResourceId('');
    }
    this.currentOpenedResources = this.currentOpenedResources.filter(
      (currentResource: Resource, index) => {
        if (currentResource.resourceId === resourceToRemove.resourceId) {
          itemWasRemoved = true;
          return false;
        }
        return true;
      }
    );

    if (itemWasRemoved) {
      //console.log("step2")
      this.removedResourceSubject.next(resourceToRemove);
      this.openResourcesSubject.next(this.currentOpenedResources);
    }
  }

  destroyAllResources() {
    this.currentAccessedResources = [];
    this.currentOpenedResources = [];
    this.setCurrentActiveResourceId('');
    this.destroyAllPlayersSubject.next(true);
    this.accessedResourcesSubject.next(this.currentAccessedResources);
    this.openResourcesSubject.next(this.currentOpenedResources);
    this.saveCurrentPlayerState();
  }
  // new code
  loadPlayersFromStorage(availableResources: Resource[]) {
    
    //const appState:any = this.appStateStorageService.getAppStorageByCurrentStorageKey();
    let appState: any;
    this.appStateStorageService
      .getAppStorageByCurrentStorageKey()
      .subscribe(data => {
        
        if (data && data['wbContent']) {
          appState = [];
          appState = JSON.parse(data['wbContent']);
          //console.log("TCL: PlayerContainerService -> loadPlayersFromStorage -> appState", appState)

          const availableEbooks = this.curriculumPlaylistService
            .availableEbooks;
          
          const resources = availableEbooks
            .map(ebook => {
              return ebook.convertToResource();
            })
            .concat(availableResources);
          //console.log("appState",appState)
          if (appState) {
            const { players: storedPlayers } = appState;
            // console.log(
            //   'TCL: PlayerContainerService -> loadPlayersFromStorage -> storedPlayers',
            //   storedPlayers
            // );
            if (
              storedPlayers &&
              storedPlayers.length > 0 &&
              resources &&
              resources.length > 0
            ) {
              //console.log("PlayerContainerService -> loadPlayersFromStorage -> storedPlayers", storedPlayers)
              storedPlayers.forEach(player => {
                const resourceToOpen = resources.find(
                  resource =>
                    resource.resourceId === player.resourceId &&
                    resource.visibility
                );
                //console.log("resourceToOpen",resourceToOpen)
                if (player.isOpen && resourceToOpen) {
                  if (resourceToOpen.downloadFileExtension) {
                    const suppredString = resourceToOpen.fileName.split('.')[0];
                    resourceToOpen.fileName = suppredString + '.pdf';
                  } else {
                    resourceToOpen.fileName = resourceToOpen.fileName;
                  }
                  this.openResource(resourceToOpen, player.isActive);

                  if (player.resourceId.includes('book-')) {
                    const selectedEbook = availableEbooks.find(
                      ebook => ebook.bookId === player.resourceId
                    );
                    this.curriculumPlaylistService.setEbookSelection(
                      selectedEbook
                    );
                  }
                } else {
                  this.addClosedResource(resourceToOpen);
                }
              });
            }
          }
        }
      });
  }

  // old code
  // loadPlayersFromStorage(availableResources: Resource[]) {
  //   const appState:any = this.appStateStorageService.getAppStorageByCurrentStorageKey();
  //   const availableEbooks = this.curriculumPlaylistService.availableEbooks;
  //   const resources = availableEbooks
  //     .map(ebook => {
  //       return ebook.convertToResource();
  //     })
  //     .concat(availableResources);

  //   if (appState) {
  //     const { players: storedPlayers }:any = appState;
  //     console.log("TCL: PlayerContainerService -> loadPlayersFromStorage -> storedPlayers", storedPlayers)
  //     if (storedPlayers) {
  //       storedPlayers.forEach(player => {
  //         const resourceToOpen = resources.find(
  //           resource => resource.resourceId === player.resourceId
  //         );
  //         if (player.isOpen) {
  //           this.openResource(resourceToOpen, player.isActive);

  //           if (player.resourceId.includes('book-')) {
  //             const selectedEbook = availableEbooks.find(
  //               ebook => ebook.bookId === player.resourceId
  //             );
  //             this.curriculumPlaylistService.setEbookSelection(selectedEbook);
  //           }
  //         } else {
  //           this.addClosedResource(resourceToOpen);
  //         }
  //       });
  //     }
  //   }
  // }

  getCurrentPlayersState(): Array<{
    resourceId: string;
    isOpen: boolean;
    isActive: boolean;
  }> {
    return this.currentAccessedResources.map(resource => {
      const resourceObj = {
        resourceId: resource.resourceId,
        isOpen: false,
        isActive:
          resource.resourceId === this.currentActiveResourceIdSubject.getValue()
      };
      if (this.currentOpenedResources.includes(resource)) {
        resourceObj.isOpen = true;
      }
      return resourceObj;
    });
  }
}
