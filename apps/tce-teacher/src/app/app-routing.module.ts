import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PlanningModeViewComponent } from '@tce/planning-mode';
import { BadgeComponent } from 'libs/core/src/lib/components/badge/badge.component';
import { SessionTimeoutComponent } from './features/canvas-view/components/session-timeout/session-timeout.component';
import { DefaultRouteGuard } from './guards/default-route.guard';
import { AuthGuard } from './services/auth.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () =>
      import('./features/login/sign-in/sign-in.module').then(
        mod => mod.SignInModule
      )
  },
  {
    path: 'canvas',
    loadChildren: () =>
      import('./features/canvas-view/canvas-view.module').then(
        mod => mod.CanvasViewModule
      )
  },
  // {
  //   path: 'admin',
  //   loadChildren: () =>
  //     import('./features/admin/admin.module').then(mod => mod.AdminModule)
  // },

  {
    path: '',
    canActivate: [DefaultRouteGuard],
    children: []
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
