import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedRoutingModule } from './shared-routing.module';
import { FactOfTheDayComponent } from './components/fact-of-the-day/fact-of-the-day.component';
// import { SessionDialogComponent } from './components/session-dialog/session-dialog.component';
// import { ActivityMonitorService } from '../services/activity-monitor.service';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [FactOfTheDayComponent], //SessionDialogComponent
  imports: [CommonModule, SharedRoutingModule, HttpClientModule],
  exports: [FactOfTheDayComponent], //SessionDialogComponent
  entryComponents: [], //SessionDialogComponent
  providers: []
})
export class SharedModule {}
