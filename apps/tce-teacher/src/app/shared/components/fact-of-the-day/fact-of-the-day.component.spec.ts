import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FactOfTheDayComponent } from './fact-of-the-day.component';

describe('FactOfTheDayComponent', () => {
  let component: FactOfTheDayComponent;
  let fixture: ComponentFixture<FactOfTheDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FactOfTheDayComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FactOfTheDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
