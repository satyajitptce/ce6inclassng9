import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'app-fact-of-the-day',
  templateUrl: './fact-of-the-day.component.html',
  styleUrls: ['./fact-of-the-day.component.scss']
})
export class FactOfTheDayComponent implements OnInit {
  slide = null;
  max = 4;
  min = 1;
  timer = null;
  current = 0;
  factIndex = 0;
  activeFact = 0;

  slides = [
    {
      image: 'assets/images/signin-cover-images/SignInCover.1@2x.jpg',
      facts: [
        {
          headerText: 'Geography',
          bodyText:
            'Earth rejoices our words, breathing and peaceful steps. Let every breath, every word and every step make the mother earth proud of us.',
          footerText: 'Amit Ray'
        },
        {
          headerText: 'Geography',
          bodyText:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
          footerText: 'Author John'
        },
        {
          headerText: 'Geography',
          bodyText:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
          footerText: 'Author Slide 3'
        }
      ]
    },
    {
      image: 'assets/images/signin-cover-images/SignInCover.2@2x.jpg',
      facts: [
        {
          headerText: 'History',
          bodyText:
            'Few will have the greatness to bend history itself; but each of us can work to change a small portion of events, and in the total; of all those acts will be written the history of this generation',
          footerText: 'Robert Kennedy'
        },
        {
          headerText: 'History',
          bodyText:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
          footerText: 'Author John'
        }
      ]
    },
    {
      image: 'assets/images/signin-cover-images/SignInCover.4@2x.jpg',
      facts: [
        {
          headerText: 'Computer Science',
          bodyText:
            "Computer science inverts the normal. In normal science, you're given a world, and your job is to find out the rules. In computer science, you give the computer the rules, and it creates the world",
          footerText: 'Alan Kay'
        },
        {
          headerText: 'Computer Science',
          bodyText:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
          footerText: 'Author John'
        }
      ]
    },
    {
      image: 'assets/images/signin-cover-images/SignInCover.3@2x.jpg',
      facts: [
        {
          headerText: 'Chemistry',
          bodyText:
            'We think there is color, we think there is sweet, we think there is bitter, but in reality there are atoms and a void.',
          footerText: 'Democritus, C. 460 - C.370 BC'
        },
        {
          headerText: 'Chemistry',
          bodyText:
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ',
          footerText: 'Author John'
        }
      ]
    }
  ];

  constructor() {
    this.current =
      Math.floor(Math.random() * (this.max - this.min + 1)) + this.min - 1;
  }

  ngOnInit() {
    this.initSlide();
  }

  initSlide() {
    this.slide = this.slides[this.current];
    this.timer = setInterval(() => {
      this.nextFact();
    }, 4000);
  }

  nextFact() {
    this.factIndex =
      this.factIndex === this.slides[this.current].facts.length - 1
        ? 0
        : this.factIndex + 1;
    this.activeFact = this.factIndex;
  }
}
