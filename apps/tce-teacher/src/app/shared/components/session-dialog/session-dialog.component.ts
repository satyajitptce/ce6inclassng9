// import { Component, OnDestroy, OnInit } from '@angular/core';
// import { MatDialogRef } from '@angular/material';
// import { Subscription } from 'rxjs';
// import { Router } from '@angular/router';
// import { Idle } from '@ng-idle/core';
// import { ActivityMonitorService } from '../../../services/activity-monitor.service';

// @Component({
//   // tslint:disable-next-line:component-selector
//   selector: 'app-session-dialog',
//   templateUrl: './session-dialog.component.html',
//   styleUrls: ['./session-dialog.component.scss']
// })
// export class SessionDialogComponent implements OnInit, OnDestroy {

//   status: any;
//   getAMStatusSubscription: Subscription;

//   constructor(
//     public dialogRef: MatDialogRef<SessionDialogComponent>,
//     private amService: ActivityMonitorService,
//     private router: Router, private idle: Idle
//   ) { }

//   ngOnInit() {
//     this.getAMStatusSubscription = this.idle.onTimeout.subscribe();
//     this.getAMStatusSubscription = this.amService.getAMData$.subscribe(status => {
//       console.log('inside message', status);
//       this.status = status;
//     });
//   }

//   onClose() {
//     this.idle.stop();
//     // this.idle.onTimeout.observers.length = 0;
//     // this.idle.onIdleStart.observers.length = 0;
//     // this.idle.onIdleEnd.observers.length = 0;
//     this.router.navigate(['/']);
//     this.dialogRef.close();
//     sessionStorage.removeItem('token');
//     // this.getAMStatusSubscription.unsubscribe();
//     //console.log('Token Value', sessionStorage.removeItem('token'));
//   }

//   onContinue() {
//     this.dialogRef.close();
//     console.log('API Call');
//   }

//   ngOnDestroy() {
//     this.getAMStatusSubscription.unsubscribe();
//   }
// }
