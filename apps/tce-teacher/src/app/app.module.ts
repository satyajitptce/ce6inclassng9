import { BrowserModule } from '@angular/platform-browser';

import { NgModule, APP_INITIALIZER, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import {
  AppConfigService,
  BaseRequestInterceptor,
  ErrorHandlerService
} from '@tce/core';
import { DefaultRouteGuard } from './guards/default-route.guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthenticationService } from '@tce/core';
import { NgIdleKeepaliveModule } from '@ng-idle/keepalive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from '@app-teacher/environments/environment';
import { ToastrModule } from 'ngx-toastr';
import { loadEditorModule } from 'libs/quiz-templates/src/lib/sharedComponents/core/providers/lazy.provider';
import { QUILL_TOKEN } from '@tce/quiz-templates';
import { QuillModule } from 'ngx-quill';
import { NgxMaskModule } from 'ngx-mask';
import { NgxSmartModalModule } from 'ngx-smart-modal';

@NgModule({
  declarations: [AppComponent],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    QuillModule.forRoot(),
    NgxMaskModule.forRoot(),
    NgIdleKeepaliveModule.forRoot(),
    NgxSmartModalModule.forRoot(),
    ToastrModule.forRoot({
      positionClass: 'toast-top-center'
    })
  ],
  providers: [
    DefaultRouteGuard,
    AuthenticationService,
    {
      provide: APP_INITIALIZER,
      // Config loader factory
      // Note: This service will load the config file upon application load
      useFactory: (config: AppConfigService) => () => config.load(),
      deps: [AppConfigService],
      multi: true
    },
    { provide: QUILL_TOKEN, useFactory: loadEditorModule },
    {
      provide: 'environment',
      useValue: environment
    },
    {
      provide: ErrorHandler,
      useClass: ErrorHandlerService
    },
    [
      {
        provide: HTTP_INTERCEPTORS,
        useClass: BaseRequestInterceptor,
        multi: true
      }
    ]
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
