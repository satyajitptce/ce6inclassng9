import { Component, OnInit, HostBinding } from '@angular/core';
import {
  AppConfigService,
  GlobalConfigState,
  CoreError,
  ToolbarService
} from '@tce/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'tce-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'tce-teacher';
  focusMode = false;

  constructor(
    private appConfigService: AppConfigService,
    private toolbarService: ToolbarService
  ) {}

  ngOnInit() {
    
    this.toolbarService.focusMode$.subscribe(newFocusValue => {
      this.focusMode = newFocusValue;
    });
  }

  @HostBinding('@.disabled')
  get animationsDisabled() {
    return (
      this.appConfigService.getGlobalSettingConfig('animation') ===
      GlobalConfigState.DISABLED
    );
  }

  @HostBinding('class')
  get appHostClassList() {
    const classes = [];
    if (this.focusMode) {
      classes.push('focus-mode');
    }
    if (
      this.appConfigService.getGlobalSettingConfig('animation') ===
      GlobalConfigState.DISABLED
    ) {
      classes.push('animation-disabled');
    }
    return classes.join(' ');
  }
}
