/**
 * Interface definition for question
 *
 * @export
 * @interface IPin
 */
export interface IPin {
  responseData: number;
}
