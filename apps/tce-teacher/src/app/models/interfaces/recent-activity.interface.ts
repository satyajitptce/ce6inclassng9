/**
 * Interface definition for question
 *
 * @export
 * @interface IRecentItem
 */
export interface IRecentItem {
  accessCount: number;
  bookId: string;
  chapterId: string;
  classId: string;
  className: string;
  division: string;
  gradeId: string;
  gradeName: string;
  lastAccessedOn: string;
  subjectId: string;
  subjectTitle: string;
  topicId: [];
}
