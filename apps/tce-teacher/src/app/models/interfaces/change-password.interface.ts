/**
 * Interface definition for question
 *
 * @export
 * @interface IChangePassword
 */
export interface IChangePassword {
  responseData: string;
}
