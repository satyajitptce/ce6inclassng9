/**
 * Interface definition for question
 *
 * @export
 * @interface ILogout
 */
export interface ILogout {
  success: string;
}
