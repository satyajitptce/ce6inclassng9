export enum PlayerConfigType {
  MULTI_PLAYER_MULTI_INSTANCE = 'mpmi',
  MULTI_PLAYER_SINGLE_INSTANCE = 'mpsi',
  ANY_PLAYER_SINGLE_INSTANCE = 'anypsi'
}
