export enum SignInState {
  LOGIN_PASSWORD = 'auth.login.password',
  LOGIN_PIN = 'auth.login.pin',
  CHANGE_PASSWORD = 'auth.change.password',
  CHANGE_PIN = 'auth.change.pin',
  DESTROY = 'auth.destroy',
  SESSION_DEFAULT = 'session_default'
}

export enum SignInModuleState {
  ACTIVE = 'active',
  INACTIVE = 'inactive'
}
