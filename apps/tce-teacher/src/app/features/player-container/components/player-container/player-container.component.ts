import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  HostListener
} from '@angular/core';
import { Subscription } from 'rxjs';
import { PlayerResourceComponent } from '../../../../../../../../libs/player-ebook/src/lib/components/player-resource/player-resource.component';
import { LibConfigService } from '@tce/lib-config';
import {
  Resource,
  PanZoomService,
  ToolbarService,
  ToolType,
  CommonService
} from '@tce/core';
import { PlayerContainerService } from '@app-teacher/services';
import { PlayerCapsuleComponent } from '../player-capsule/player-capsule.component';
@Component({
  selector: 'tce-player-container',
  templateUrl: './player-container.component.html',
  styleUrls: ['./player-container.component.scss']
})
export class PlayerContainerComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @ViewChild('playerOutlet', { static: true, read: ViewContainerRef })
  playerOutlet: ViewContainerRef | undefined;

  @ViewChild('innerPlayerContainer', { static: true, read: ElementRef })
  innerPlayerContainer: ElementRef | undefined;

  @ViewChild('playerResource', { static: true, read: ViewContainerRef })
  playerResource: ViewContainerRef | undefined;
  playerResourceRef: ComponentRef<any> | null = null;
  subscriptions: Subscription = new Subscription();

  currentScale = 1;
  currentActiveResourceId: string;
  repositionAfterResizeTimeout;

  isPointerDisabled = false;
  @HostBinding('class.-disable-pointer-events') get disablePointerEvents() {
    return this.isPointerDisabled;
  }

  constructor(
    private libConfigService: LibConfigService,
    private playerContainerService: PlayerContainerService,
    private panZoomService: PanZoomService,
    private factoryResolver: ComponentFactoryResolver,
    private toolbarService: ToolbarService,
    private commonService: CommonService
  ) {}

  ngOnInit() {
    this.commonService.ebookResourcelist$.subscribe(data => {
      if (data) {
        let ebokResource: any = data;
        this.loadEbookResourceList(ebokResource, true);
      }
    });
    this.commonService.ebookResourcelistFlag$.subscribe(data => {
      if (!data) {
        this.removeComponent();
      }
    });
    this.subscriptions.add(
      this.playerContainerService.addOpenedResource$.subscribe(
        (resource: Resource) => {
          this.loadResourceComponent(resource, true);
        }
      )
    );
    this.subscriptions.add(
      this.playerContainerService.addClosedResource$.subscribe(resource => {
        this.loadResourceComponent(resource, false);
      })
    );

    this.subscriptions.add(
      this.playerContainerService.destroyAllPlayers$.subscribe(
        shouldDestroy => {
          if (shouldDestroy) this.removeAllPlayers();
        }
      )
    );

    this.subscriptions.add(
      this.toolbarService.currentSelectedTool$.subscribe(tool => {
        if (tool === ToolType.Pan) {
          this.isPointerDisabled = true;
        } else {
          this.isPointerDisabled = false;
        }
      })
    );

    this.subscriptions.add(
      this.playerContainerService.reopenResource$.subscribe(
        (resource: Resource) => {
          this.reopenPlayer(resource);
        }
      )
    );

    this.subscriptions.add(
      this.playerContainerService.removedResource$.subscribe(
        (resource: Resource) => {
          //console.log("player-container-removePlayer",resource)
          this.removePlayer(resource);
        }
      )
    );

    this.subscriptions.add(
      this.playerContainerService.currentActiveResourceId$.subscribe(
        resourceId => (this.currentActiveResourceId = resourceId)
      )
    );

    this.subscriptions.add(
      this.panZoomService.currentPanZoomMatrix$.subscribe(matrix => {
        // Set new Matrix Translation to player inner.
        const matrixString = `matrix(${matrix.a}, ${matrix.b}, ${matrix.c}, ${matrix.d}, ${matrix.e}, ${matrix.f})`;
        this.innerPlayerContainer.nativeElement.style.transform = matrixString;

        // Set new Player Container Point.
        const newPlayerPoint = this.innerPlayerContainer.nativeElement.getBoundingClientRect();
        const { x, y, height, width } = newPlayerPoint;
        this.panZoomService.setPlayerPoint({ x, y, height, width });
      })
    );
  }

  ngAfterViewInit() {}

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  // This is to recenter on the current active player when the window is resized.
  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    if (this.repositionAfterResizeTimeout) {
      clearTimeout(this.repositionAfterResizeTimeout);
    }
    if (this.currentActiveResourceId) {
      this.repositionAfterResizeTimeout = setTimeout(() => {
        this.playerContainerService.setCurrentActiveResourceId(
          this.currentActiveResourceId
        );
      }, 300);
    }
  }

  reopenPlayer(passedResource: Resource) {
    if (
      passedResource.playerCapsuleRef &&
      passedResource.playerCapsuleRef.instance
    ) {
      passedResource.playerCapsuleRef.instance.openPlayer(passedResource);
    }
  }

  removePlayer(resourceToClose: Resource) {
    //console.log("removePlayer -> resourceToClose.playerCapsuleRef", resourceToClose.playerCapsuleRef)
    //console.log("removePlayer -> resourceToClose.playerCapsuleRef.instance", resourceToClose.playerCapsuleRef.instance)

    if (
      resourceToClose.playerCapsuleRef &&
      resourceToClose.playerCapsuleRef.instance
    ) {
      //console.log("resourceToClose")
      resourceToClose.playerCapsuleRef.instance.removePlayer();
    }
  }

  removeAllPlayers() {
    if (this.playerOutlet) {
      this.playerOutlet.clear();
    }
  }

  loadResourceComponent<T>(resource: Resource, openPlayer: boolean): any {
    //console.log("ngAfterViewInit -> resource", resource)
    // Load Player Capsule and add it to the DOM.
    //console.log('<--- plyer container playerOutlet --->', this.playerOutlet);
    if (resource.resourceType) {
      let playerCapsuleRef: ComponentRef<any> | null = null;
      const playerCapsuleFactory = this.factoryResolver.resolveComponentFactory(
        PlayerCapsuleComponent
      );
      playerCapsuleRef = this.playerOutlet.createComponent(
        playerCapsuleFactory
      );
      resource.setPlayerCapsuleRef(playerCapsuleRef);
      const playerCapsuleInstance = playerCapsuleRef.instance;
      playerCapsuleInstance.setAssociatedResource(resource);
      // console.log(
      //   'TCL: ngAfterViewInit -> resource.resourceType',
      //   resource.resourceType
      // );

      let playerType: any = resource.resourceType;
      if (playerType === 'game') {
        playerType = 'tcevideo';
      }
      // Add Appropriate Resource Player To Player Capsule
      this.libConfigService
        .getComponentFactory<T>('player-' + playerType)

        .subscribe({
          next: componentFactory => {
            //console.log('componentFactory', componentFactory);
            if (!this.playerOutlet) {
              return null;
            }
            playerCapsuleInstance.onLoadPlayerFactory(
              componentFactory,
              openPlayer
            );
            playerCapsuleRef.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }

  removeComponent() {
    if (this.playerResourceRef) {
      this.playerResourceRef.destroy();
    }
  }

  loadEbookResourceList<T>(resource: Resource, openPlayer: boolean) {
    const playerResourceFactory = this.factoryResolver.resolveComponentFactory(
      PlayerResourceComponent
    );
    this.playerResourceRef = this.playerResource.createComponent(
      playerResourceFactory
    );
    resource.setPlayerResourceRef(this.playerResourceRef);
    const playerResourceInstance = this.playerResourceRef.instance;
    playerResourceInstance.setAssociatedResource(resource);
    this.libConfigService
      .getComponentFactory<T>('player-' + resource.resourceType)
      .subscribe({
        next: componentFactory => {
          if (!this.playerOutlet) {
            return null;
          }
          playerResourceInstance.onLoadPlayerFactory(
            componentFactory,
            openPlayer
          );
          this.playerResourceRef.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }
}
