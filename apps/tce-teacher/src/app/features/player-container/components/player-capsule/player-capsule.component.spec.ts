import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerCapsuleComponent } from './player-capsule.component';

describe('PlayerCapsuleComponent', () => {
  let component: PlayerCapsuleComponent;
  let fixture: ComponentFixture<PlayerCapsuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerCapsuleComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerCapsuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
