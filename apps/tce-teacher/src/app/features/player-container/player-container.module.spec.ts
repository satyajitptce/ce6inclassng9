import { async, TestBed } from '@angular/core/testing';
import { PlayerContainerModule } from './player-container.module';

describe('PlayerContainerModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerContainerModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerContainerModule).toBeDefined();
  });
});
