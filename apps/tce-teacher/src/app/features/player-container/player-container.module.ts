import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlayerContainerComponent } from './components/player-container/player-container.component';
import { PlayerCapsuleComponent } from './components/player-capsule/player-capsule.component';
import { PlayerResourceComponent } from '../../../../../../libs/player-ebook/src/lib/components/player-resource/player-resource.component';
@NgModule({
  imports: [CommonModule],
  declarations: [
    PlayerContainerComponent,
    PlayerCapsuleComponent,
    PlayerResourceComponent
  ],
  exports: [PlayerContainerComponent, PlayerResourceComponent],
  entryComponents: [PlayerCapsuleComponent, PlayerResourceComponent]
})
export class PlayerContainerModule {}
