import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { ChangePinComponent } from './components/change-pin/change-pin.component';
import { LoginPwdComponent } from './components/login-pwd/login-pwd.component';
import { LoginPinComponent } from './components/login-pin/login-pin.component';
import { AuthGuard } from '@app-teacher/services/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginPageComponent,
    data: { title: 'Login' },
    children: [
      {
        path: 'loginpassword',
        component: LoginPwdComponent,
        data: { title: 'Change Password' }
      },
      {
        path: 'loginpin',
        component: LoginPinComponent,
        data: { title: 'Login Pin' }
      },
      {
        path: 'changepassword',
        component: ChangePasswordComponent,
        data: { title: 'Change Password' },
        canActivate: [AuthGuard]
      },
      {
        path: 'changepin',
        component: ChangePinComponent,
        data: { title: 'Change Pin' },
        canActivate: [AuthGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SignInRoutingModule {}
