import {
  Component,
  ElementRef,
  HostListener,
  OnDestroy,
  OnInit
} from '@angular/core';
import {
  SignInModuleState,
  SignInState
} from '@app-teacher/models/enums/signInState.enum';
import {
  AuthStateEnum,
  KeyboardService,
  AuthenticationService,
  CommonService
} from '@tce/core';
import { ApplicationMode } from 'libs/core/src/lib/enums/app-mode.enum';
import { Subscription } from 'rxjs';

@Component({
  selector: 'tce-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit, OnDestroy {
  public createForm = false;
  public cMode = false;
  public activeComponentRef;
  public currentSignInState: SignInState;
  public signInActive = false;
  private authState: AuthStateEnum;
  $subs = new Subscription();

  constructor(
    private authenticationService: AuthenticationService,
    private eRef: ElementRef,
    private keyboardService: KeyboardService,
    private commonService: CommonService
  ) {}

  /*
    getClientVersion method will hit the clientId Api,which will store cookies in browser automatically.
    Also, accessing the redux store for sign module.
  */
  ngOnInit() {
    //console.log('this.commonService.applicationMode', this.signInActive);

    this.$subs.add(
      this.authenticationService.currentSignInState$.subscribe(state => {
        this.currentSignInState = state;
        //console.log(this.currentSignInState);
      })
    );
    this.$subs.add(
      this.authenticationService.signInModuleState$.subscribe(state => {
        //console.log('2', state);
        this.signInActive = state === 'active' ? true : false;
        this.keyboardService.closeKeyboard();
        if (this.commonService.applicationMode === ApplicationMode.PLANNING) {
          this.signInActive = this.commonService.signInActive;
        }
        //console.log('this.commonService.applicationMode', this.signInActive);
      })
    );
    this.$subs.add(
      this.authenticationService.userState$.subscribe(userState => {
        //console.log('userState', userState);
        this.authState = userState.authStatus;
      })
    );
  }

  @HostListener('click', ['$event'])
  handleClickInside(event: Event) {
    event.stopPropagation();
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event: Event) {
    if (this.signInActive) {
      this.setSignInActive(event);
    }
  }

  ngOnDestroy() {
    this.$subs.unsubscribe();
  }

  showHideIconClick(event: Event) {
    event.stopPropagation();
    this.setSignInActive(event);
  }

  setSignInActive(
    event: Event,
    fromPosition: 'general' | 'topBar' = 'general'
  ) {
    if (this.signInActive && fromPosition === 'topBar') {
      //console.log('this.commonService.applicationMode', this.signInActive);
      return false;
    }
    if (this.authState === AuthStateEnum.AUTH) {
      this.authenticationService.setCurrentSignInState(SignInState.DESTROY);
    } else {
      const newState = this.signInActive
        ? SignInModuleState.INACTIVE
        : SignInModuleState.ACTIVE;
      this.authenticationService.setSignInModuleState(newState);
      if (newState === SignInModuleState.INACTIVE) {
        this.authenticationService.setCurrentSignInState(
          SignInState.SESSION_DEFAULT
        );
      }
    }
  }
}
