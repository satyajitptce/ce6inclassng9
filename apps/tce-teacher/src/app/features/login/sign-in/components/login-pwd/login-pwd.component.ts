import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  ViewChild,
  ElementRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticationService } from '@tce/core';
import { SignInState } from '@app-teacher/models/enums/signInState.enum';
import { KeyboardService, KeyboardState } from '@tce/core';
import { Subscription } from 'rxjs';

declare let clientLogin: any;
declare global {
  function customsingin<T>(
    someObject: T | null | undefined,
    defaultValue?: T | null | undefined
  ): T;
}
@Component({
  selector: 'tce-login-pwd',
  templateUrl: './login-pwd.component.html',
  styleUrls: [
    './login-pwd.component.scss',
    '../../pages/login-page/login-page.component.scss'
  ]
})
export class LoginPwdComponent implements OnInit, OnDestroy {
  // @ViewChild('email', { static: false }) email: ElementRef;
  @ViewChild('auto') auto;
  @Input('isSignInActive') set isSignInActive(bool: boolean) {
    this.signInActive = bool;
    if (bool) {
      //wait til the login component slides up
      //or else we get weird jumpy transitions
      setTimeout(() => {
        // this.email.nativeElement.focus();
        this.auto.focus();
      }, 300);
    }
  }
  public errorMessage = '';
  public isFetching = false;
  public loginForm: FormGroup;
  public signInActive = false;
  public $subs = new Subscription();
  keyword = '';
  data: any;
  errorMsg: string;
  isLoadingResult: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private keyboardService: KeyboardService
  ) {}

  getServerResponse(query) {
    this.authenticationService.getOrganizationList(query).subscribe((data) => {
      this.data = data.suggestions;
    })
  }

  searchCleared() {
    this.data = [];
  }

  selectEvent(item) {
    localStorage.setItem('organization', item);
  }

  ngOnInit() {
    this.$subs.add(
      this.authenticationService.authError$.subscribe(errorMessage => {
        this.errorMessage = errorMessage;
      })
    );

    this.$subs.add(
      this.authenticationService.authFetching$.subscribe(isFetching => {
        this.isFetching = isFetching;
      })
    );

    this.$subs.add(
      this.keyboardService.keyboardEnterPress$.subscribe(() => {
        this.onLoginSubmit();
      })
    );

    this.createForm();
    const self = this;
    const _global = (window /* browser */ || global) /* node */ as any;
    _global.customSignIn = function<T>(
      username: T | null | undefined,
      password: T | null | undefined
    ): any {
      self.onLoginSubmitBle(username, password);
    };
  }

  ngOnDestroy() {
    this.authenticationService.setAuthError('');
    this.loginForm.reset();
    this.$subs.unsubscribe();
  }

  createForm() {
    localStorage.removeItem('organization');
    this.loginForm = new FormBuilder().group({
      orgId: ['', Validators.compose([Validators.required])],
      emailId: ['', Validators.compose([Validators.required])],
      password: ['', Validators.compose([Validators.required])]
    });
  }

  onLoginSubmit() {
    //login api
    // Currently waiting to hear back about the correct password and the domain on which this
    // is to be run.
    const loginDetails = {
      userName: this.loginForm.value.emailId,
      password: this.loginForm.value.password,
      loginType: 'password'
    };
    this.authenticationService.loginPWD(loginDetails, 'onLogin-Submit');
    this.keyboardService.closeKeyboard();
  }
  onLoginSubmitBle(u, p) {
    //login api
    // Currently waiting to hear back about the correct password and the domain on which this
    // is to be run.
    const loginDetails = {
      userName: u,
      password: p,
      loginType: 'password'
    };
    this.authenticationService.loginPWD(loginDetails, 'onLogin-SubmitBle');
    this.keyboardService.closeKeyboard();
  }

  signInWithPin() {
    this.authenticationService.setCurrentSignInState(SignInState.LOGIN_PIN);
  }

  updateFieldValue(formControlName: string, val: string) {
    this.loginForm.controls[formControlName].setValue(val);
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN
    };
    if (this.signInActive) {
      this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
    }
  }

  changePassword() {
    this.authenticationService.setCurrentSignInState(
      SignInState.CHANGE_PASSWORD
    );
  }

  changePIN() {
    this.authenticationService.setCurrentSignInState(SignInState.CHANGE_PIN);
  }
}
