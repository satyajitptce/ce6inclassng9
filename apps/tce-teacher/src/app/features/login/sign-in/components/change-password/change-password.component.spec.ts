import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { ChangePasswordService } from '../../services/change-password.service';
import { ChangePasswordComponent } from './change-password.component';
describe('ChangePasswordComponent', () => {
  let component: ChangePasswordComponent;
  let fixture: ComponentFixture<ChangePasswordComponent>;
  beforeEach(() => {
    const routerStub = { navigate: array1 => ({}) };
    const activatedRouteStub = {};
    const formBuilderStub = {};
    const changePasswordServiceStub = {
      changePassword: arg1 => ({ pipe: () => ({ subscribe: () => ({}) }) })
    };
    TestBed.configureTestingModule({
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [ChangePasswordComponent],
      providers: [
        { provide: Router, useValue: routerStub },
        { provide: ActivatedRoute, useValue: activatedRouteStub },
        { provide: FormBuilder, useValue: formBuilderStub },
        { provide: ChangePasswordService, useValue: changePasswordServiceStub }
      ]
    });
    fixture = TestBed.createComponent(ChangePasswordComponent);
    component = fixture.componentInstance;
  });
  it('can load instance', () => {
    expect(component).toBeTruthy();
  });
  it('loading defaults to: false', () => {
    expect(component.loading).toEqual(false);
  });
  it('submitted defaults to: false', () => {
    expect(component.submitted).toEqual(false);
  });
  // describe('onSubmit', () => {
  //   it('makes expected calls', () => {
  //     const routerStub: Router = fixture.debugElement.injector.get(Router);
  //     const changePasswordServiceStub: ChangePasswordService = fixture.debugElement.injector.get(
  //       ChangePasswordService
  //     );
  //     spyOn(routerStub, 'navigate').and.callThrough();
  //     spyOn(changePasswordServiceStub, 'changePassword').and.callThrough();
  //     component.onSubmit();
  //     expect(routerStub.navigate).toHaveBeenCalled();
  //     expect(changePasswordServiceStub.changePassword).toHaveBeenCalled();
  //   });
  // });
  describe('onNoClick', () => {
    it('makes expected calls', () => {
      const routerStub: Router = fixture.debugElement.injector.get(Router);
      spyOn(routerStub, 'navigate').and.callThrough();
      component.onNoClick();
      expect(routerStub.navigate).toHaveBeenCalled();
    });
  });
});
