import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { AuthenticationService, KeyboardTheme } from '@tce/core';
import { SignInState } from '@app-teacher/models/enums/signInState.enum';
import { KeyboardState, KeyboardService } from '@tce/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'tce-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: [
    './change-pin.component.scss',
    '../../pages/login-page/login-page.component.scss'
  ]
})
export class ChangePinComponent implements OnInit {
  @Input() isSignInActive: boolean;
  public passwordChanged = true;
  public isFetching = false;
  public setPin = false;
  public verifyPin = false;
  public setPinForm: FormGroup;
  public verifyPinForm: FormGroup;
  public unamePattern = /^[0-9]*$/;
  public errorMessage: any;
  private setPinValue: string;
  public autoGenErrorMessage: string;
  public autoGenError = false;
  public autoGenerateIsFetching = false;

  constructor(
    private authenticationService: AuthenticationService,
    private keyboardService: KeyboardService,
    private toastrService: ToastrService
  ) {}

  ngOnInit() {
    this.createSetPinForm();

    this.keyboardService.keyboardEnterPress$.subscribe(() => {
      this.onVerifyPinSubmit();
      this.keyboardService.closeKeyboard();
    });
  }

  public cancelButton(): void {
    this.authenticationService.setCurrentSignInState(SignInState.DESTROY);
    this.keyboardService.closeKeyboard();
  }

  private createSetPinForm() {
    this.setPinForm = new FormBuilder().group({
      orgId: [''],
      otp1: ['', Validators.compose([Validators.required])],
      otp2: ['', Validators.compose([Validators.required])],
      otp3: ['', Validators.compose([Validators.required])],
      otp4: ['', Validators.compose([Validators.required])],
      otp5: ['', Validators.compose([Validators.required])]
    });
  }

  private createVerifyPinForm() {
    this.verifyPinForm = new FormBuilder().group({
      orgId: [''],
      otpv1: ['', Validators.compose([Validators.required])],
      otpv2: ['', Validators.compose([Validators.required])],
      otpv3: ['', Validators.compose([Validators.required])],
      otpv4: ['', Validators.compose([Validators.required])],
      otpv5: ['', Validators.compose([Validators.required])]
    });
  }

  public skipSetPin() {
    this.authenticationService.setCurrentSignInState(SignInState.DESTROY);
  }

  public onSetPinSubmit() {
    this.errorMessage = '';
    this.setPin = false;
    this.verifyPin = true;
    this.createVerifyPinForm();
    setTimeout(() => {
      document.getElementById('otpv1').focus();
    }, 10);
  }

  public onVerifyPinSubmit() {
    this.errorMessage = '';
    if (
      this.setPinForm.value.otp1 === this.verifyPinForm.value.otpv1 &&
      this.setPinForm.value.otp2 === this.verifyPinForm.value.otpv2 &&
      this.setPinForm.value.otp3 === this.verifyPinForm.value.otpv3 &&
      this.setPinForm.value.otp4 === this.verifyPinForm.value.otpv4 &&
      this.setPinForm.value.otp5 === this.verifyPinForm.value.otpv5
    ) {
      this.changePin();
    } else {
      this.errorMessage = "PINs didn't match. Please try again.";
      this.backToSetPin();
    }
  }

  private onBeginPinChangeFetch() {
    this.isFetching = true;
    this.errorMessage = '';
  }
  private changePin() {
    this.setPinValue = ''.concat(
      this.setPinForm.value.otp1,
      this.setPinForm.value.otp2,
      this.setPinForm.value.otp3,
      this.setPinForm.value.otp4,
      this.setPinForm.value.otp5
    );
    this.onBeginPinChangeFetch();
    this.authenticationService.changePIN(this.setPinValue).subscribe(
      data => {
        this.pinSetSuccess();
      },
      err => {
        this.isFetching = false;

        this.errorMessage = err.message
          ? err.message
          : 'A server error occurred while trying to set your PIN. Please try again';
        this.backToSetPin();
      }
    );
  }

  backToSetPin() {
    this.verifyPin = false;
    this.setPin = true;
    setTimeout(() => {
      document.getElementById('otp1').focus();
    }, 10);
  }

  pinSetSuccess() {
    this.isFetching = false;
    this.verifyPin = false;
    this.authenticationService.setCurrentSignInState(SignInState.DESTROY);
    this.toastrService.success('Pin Successfully Set');
  }

  updateFieldValue(formControlName: string, event: any) {
    const nextInput = event.srcElement.nextElementSibling;
    const key = parseInt(event.key, 10);
    const overrides = ['Backspace', 'Delete', '{bksp}', 'Tab'];
    const form = formControlName.includes('otpv')
      ? this.verifyPinForm
      : this.setPinForm;

    if (overrides.includes(event.key)) {
      return;
    }

    if (Number.isInteger(key)) {
      //virtual keyboard needs to update angular form values
      form.controls[formControlName].setValue(key);

      if (nextInput) {
        nextInput.classList.remove('emptyPin'); // To Remove
        nextInput.classList.add('enteredPin'); // To ADD
        nextInput.focus();
      } else {
        if (formControlName.includes('otpv')) {
          this.onVerifyPinSubmit();
        } else {
          this.onSetPinSubmit();
        }
      }
    }
  }

  public autoGeneratePin() {
    this.autoGenerateIsFetching = true;
    this.authenticationService.autoGeneratePin().subscribe(
      data => {
        this.autoGenerateIsFetching = false;
        this.setAutoGenPIN(data.pin);
      },
      err => {
        this.autoGenerateIsFetching = false;
        this.autoGenErrorMessage = err.errorMessage;
      }
    );
  }

  private setAutoGenPIN(pin: string) {
    this.setPinForm.setValue({
      orgId: [''],
      otp1: pin.slice(0, 1),
      otp2: pin.slice(1, 2),
      otp3: pin.slice(2, 3),
      otp4: pin.slice(3, 4),
      otp5: pin.slice(4, 5)
    });
  }

  onBlur(e: FocusEvent) {
    if (!(e.target as HTMLInputElement).value) {
      (e.target as HTMLElement).classList.add('emptyPin');
      (e.target as HTMLElement).classList.remove('enteredPin');
    }
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      inputPattern: this.unamePattern,
      maxLength: 1,
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.NUMERIC
    };
    if (this.isSignInActive) {
      (<HTMLInputElement>e.target).select();
      this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
      (e.target as HTMLElement).classList.remove('emptyPin');
      (e.target as HTMLElement).classList.add('enteredPin');
    }
  }

  loginWithPassword() {
    this.authenticationService.setCurrentSignInState(
      SignInState.LOGIN_PASSWORD
    );
  }
}
