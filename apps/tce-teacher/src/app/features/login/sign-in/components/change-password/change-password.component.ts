import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SignInState } from '@app-teacher/models/enums/signInState.enum';
import {
  AuthenticationService,
  KeyboardService,
  KeyboardState,
  UserStatusEnum
} from '@tce/core';
import { Subscription } from 'rxjs';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'tce-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: [
    './change-password.component.scss',
    '../../pages/login-page/login-page.component.scss'
  ]
})
export class ChangePasswordComponent implements OnInit {
  @Input() isSignInActive: boolean;
  public changePasswordForm: FormGroup;
  public loading = false;
  public submitted = false;
  public returnUrl: string;
  public passwordNotMatched: boolean;
  public errorMessage: string;
  public chapterSelection: Subscription;
  sub: Subscription;
  public isFetching = false;

  constructor(
    private authenticationService: AuthenticationService,
    private keyboardService: KeyboardService
  ) {}

  ngOnInit() {
    this.changePasswordFullForm();

    this.authenticationService.authError$.subscribe(errorMessage => {
      this.errorMessage = errorMessage;
    });

    this.authenticationService.authFetching$.subscribe(isFetching => {
      this.isFetching = isFetching;
    });

    this.keyboardService.keyboardEnterPress$.subscribe(() => {
      this.onSubmit();
      this.keyboardService.closeKeyboard();
    });
  }

  changePasswordFullForm() {
    this.changePasswordForm = new FormBuilder().group({
      newPassword: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ],
      verifyNewPassword: [
        '',
        Validators.compose([Validators.required, Validators.minLength(6)])
      ]
    });
  }

  // convenience getter for easy access to form fields
  get changePasswordFormFields() {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    if (
      this.changePasswordFormFields.newPassword.value ===
        this.changePasswordFormFields.verifyNewPassword.value &&
      this.changePasswordForm.valid
    ) {
      this.authenticationService.changePassword(
        this.changePasswordFormFields.newPassword.value
      );
      this.keyboardService.closeKeyboard();

    } else {
      this.passwordNotMatched = true;
    }
  }

  cancelButton(): void {
    const userStatus = this.authenticationService.getUserStatus();
    if (userStatus === UserStatusEnum.NEW_USER) {
      this.authenticationService.setCurrentSignInState(SignInState.CHANGE_PIN);
    } else {
      // const postLoginRoute = this.appConfigService.getCmodeConfig()
      //   .postloginroute;
      // this.router.navigate(['/' + postLoginRoute]);
      this.authenticationService.setCurrentSignInState(SignInState.DESTROY);
    }
  }

  clear(event) {
    this.passwordNotMatched = false;
  }

  updateFieldValue(formControlName: string, val: string) {
    this.changePasswordForm.controls[formControlName].setValue(val);
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN
    };
    if (this.isSignInActive) {
      this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
    }
  }

  loginWithPassword() {
    this.authenticationService.setCurrentSignInState(
      SignInState.LOGIN_PASSWORD
    );
  }
}
