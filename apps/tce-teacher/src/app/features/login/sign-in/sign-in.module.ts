import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SignInRoutingModule } from './sign-in-routing.module';

import { LoginPageComponent } from './pages/login-page/login-page.component';
import { LoginPwdComponent } from './components/login-pwd/login-pwd.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { LoginPinComponent } from './components/login-pin/login-pin.component';
import { ChangePinComponent } from './components/change-pin/change-pin.component';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';

@NgModule({
  declarations: [
    LoginPageComponent,
    LoginPwdComponent,
    ChangePasswordComponent,
    LoginPinComponent,
    ChangePinComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    AngularSvgIconModule,
    LibConfigModule.forChild(LoginPageComponent),
    CoreModule,
    AutocompleteLibModule
  ]
})
export class SignInModule {}
