import { Component, OnInit, ElementRef } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { ScriptLoaderService, AppConfigService } from '@tce/core';

declare let Vivus: any;

@Component({
  selector: 'tce-intro',
  templateUrl: './intro.component.html',
  styleUrls: ['./intro.component.scss']
})
export class IntroComponent implements OnInit {
  alldata: any;
  containerStyle = {
    opacity: '1'
  };

  constructor(
    private sanitizer: DomSanitizer,
    private scriptLoader: ScriptLoaderService,
    private appConfigService: AppConfigService,
    private hostElement: ElementRef
  ) {}

  ngOnInit() {
    this.lazyLoadLibs();
  }

  lazyLoadLibs() {
    this.scriptLoader.loadScript('assets/vendor/vivus.min.js').subscribe(_ => {
      this.animationsvg();
    });
  }

  animationsvg() {
    this.alldata = this.appConfigService.getConfig('general')['welcomemsg'][
      'general'
    ];
    if (this.alldata) {
      let svgData: any = '';
      let mydiv: any = document.getElementById('dynamicsvg') as HTMLElement;
      for (let x = 0; x < this.alldata.length; x++) {
        svgData += this.alldata[x].background as HTMLElement;
        mydiv.innerHTML = svgData;
      }
      this.startAnimation();
    }
  }
  transform(html) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }

  postRunCallback() {
    //meant to be tapped into to allow components including this to respond to the animation ending
  }

  startAnimation() {
    const svg1Element = document.getElementById('intro_svg');
    const svg2Element = document.getElementById('intro_text');
    if (svg1Element && svg2Element) {
      const aaa = new Vivus(
        svg1Element,
        { type: 'oneByOne', duration: 500, animTimingFunction: Vivus.EASE_IN },
        this.onAnimationEnd.bind(this)
      );
      const bbb = new Vivus(
        svg2Element,
        {
          type: 'oneByOne',
          duration: 500,
          animTimingFunction: Vivus.EASE_OUT_BOUNCE
        },
        this.onAnimationEnd.bind(this)
      );

      this.doFadeIn();
      this.doTextLeftIn();
      this.doBgLeftIn();
    }
  }

  doFadeIn() {
    let count = 0.1;

    const fadeTarget1: any = document.getElementById('intro_svg');
    const fadeTarget2: any = document.getElementById('intro_text');

    const fadeEffect = setInterval(function() {
      count = count + 0.1;
      fadeTarget1.style['opacity'] = count;
      fadeTarget2.style['opacity'] = count;
      if (count > 1) {
        clearInterval(fadeEffect);
      }
    }, 50);
  }

  doTextLeftIn() {
    let count = 10;
    const fadeTarget1: any = document.getElementById('intro_svg');

    const fadeEffect = setInterval(function() {
      count = count + 10;
      fadeTarget1.style['right'] = count + 'px';

      if (count > 250) {
        clearInterval(fadeEffect);
      }
    }, 20);
  }

  doBgLeftIn() {
    let count = 5;

    const fadeTarget2: any = document.getElementById('intro_text');
    const fadeEffect = setInterval(function() {
      count = count + 5;

      fadeTarget2.style['left'] = count + 'px';
      if (count > 150) {
        clearInterval(fadeEffect);
      }
    }, 20);
  }

  private _internalAnimationEnd() {
    const host = <HTMLElement>this.hostElement.nativeElement;
    host.removeEventListener('transitionend', this._internalAnimationEnd);
    this.postRunCallback();
  }

  onAnimationEnd() {
    const host = <HTMLElement>this.hostElement.nativeElement;
    host.addEventListener(
      'transitionend',
      this._internalAnimationEnd.bind(this)
    );
    host.style.opacity = '0';
  }
}
