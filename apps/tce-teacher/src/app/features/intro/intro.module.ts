import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IntroComponent } from './component/intro/intro.component';
import { LibConfigModule } from '@tce/lib-config';

@NgModule({
  declarations: [IntroComponent],
  imports: [CommonModule, LibConfigModule.forChild(IntroComponent)],
  exports: [IntroComponent]
})
export class IntroModule {}
