import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FTUEWelcomeComponent } from './components/ftue-welcome/ftue-welcome.component';
import { SubjectSelectorModule } from '../subject-selector/subject-selector.module';
import { LibConfigModule } from '@tce/lib-config';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  declarations: [FTUEWelcomeComponent],
  imports: [
    CommonModule,
    SubjectSelectorModule,
    LibConfigModule.forChild(FTUEWelcomeComponent),
    AngularSvgIconModule
  ]
})
export class FirstTimeUserModule {}
