import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FTUEWelcomeComponent } from './ftue-welcome.component';

describe('FTUEWelcomeComponent', () => {
  let component: FTUEWelcomeComponent;
  let fixture: ComponentFixture<FTUEWelcomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [FTUEWelcomeComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FTUEWelcomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
