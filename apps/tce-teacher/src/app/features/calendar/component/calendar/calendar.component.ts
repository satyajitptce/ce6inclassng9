import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
@Component({
  selector: 'tce-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  public now: Date = new Date();
  constructor() {}

  ngOnInit() {
    const numbers = timer(0, 1000);
    numbers.subscribe(x => {
      this.now = new Date();
    });
  }
}
