import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './component/calendar/calendar.component';
import { LibConfigModule } from '@tce/lib-config';

@NgModule({
  declarations: [CalendarComponent],
  imports: [CommonModule, LibConfigModule.forChild(CalendarComponent)],
  exports: [CalendarComponent]
})
export class CalendarModule {}
