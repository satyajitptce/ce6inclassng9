import {
  Component,
  OnInit,
  ChangeDetectorRef,
  ViewEncapsulation
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import {
  CurriculumPlaylistService,
  Resource,
  SubjectSelectorService,
  FullContentSelection,
  FileUploadService,
  AppConfigService,
  KeyboardService,
  FullClassSelection,
  KeyboardState,
  KeyboardTheme,
  Chapter,
  GradeLevel,
  ApiDivision,
  ApiSubject,
  CommonService
} from '@tce/core';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
import { PlayerContainerService } from '@app-teacher/services';
import {
  FormControl,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';

import { Subscription } from 'rxjs';
export const resourceFilterCss = {
  top: 'calc(100% + -680px)',
  display: 'block',
  right: '20px'
};
export const resourcePlaylistCss = {
  top: 'calc(100% + -690px)',
  display: 'none',
  right: '20px'
};
export const resourcePlaylistCL = {
  top: 'calc(100% + -640px)',
  display: 'none',
  right: '35%'
};
export function fileExtensionValidator(validExt: string): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let forbidden = true;
    if (control.value.getFile) {
      const fileExt =
        '.' +
        control.value.getFile
          .split('.')
          .pop()
          .toLowerCase();
      validExt.split(',').forEach(ext => {
        if (ext.trim() == fileExt) {
          forbidden = false;
        }
      });
    }
    return forbidden ? { inValidExt: true } : null;
  };
}
@Component({
  selector: 'tce-create-resource',
  templateUrl: './create-resource.component.html',
  styleUrls: ['./create-resource.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ]),
    trigger('slideInOutX', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateX(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(5%)', opacity: 0 })
        )
      ])
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class CreateResourceComponent implements OnInit {
  animationFlag: any;
  sharedCheck: any;
  shareCheckStatus: any;
  addResourcePosition: any;
  addResourcePositionProperties: any;
  addResourceOptionsForm: FormGroup;
  showOpengradeSubject = false;
  showOpenChapterTopic = false;
  myChapterUpload: any;
  mySubjectUpload: any;
  myFileUpload: any;
  gradeSubjectvalue: any;
  topicChaptervalue: any;
  fileType: any;
  fileSize: any;
  converMBtoByte: any;
  formStatus: any;
  customFileName: any;
  clicktoRenameFile = false;
  olddata: any;
  getAssetId: any;
  getFileName: any;
  warningMessage: any;
  addResourceOptions = {
    title: '',
    gradeSubject: '',
    chapterTopic: '',
    getFile: ''
  };
  resourceMessage = '';
  availableChapters: any;
  selectedChapter: any;
  availableTopics: any;
  selectedTopic: any;
  availableGrades: any;
  selectedGradeLevel: any;
  selectedSubject: any;
  availableSubjects: ApiSubject[] = [];
  activeResourceMessage: any = false;
  availableDivisions: ApiDivision[] = [];
  subjectSelectorSubscription: Subscription = new Subscription();
  constructor(
    private appConfigService: AppConfigService,
    private keyboardService: KeyboardService,
    private fileUploadService: FileUploadService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private subjectSelectorService: SubjectSelectorService,
    private toastrService: ToastrService,
    private cdr: ChangeDetectorRef,
    private commonService: CommonService
  ) {}

  ngOnInit() {
    
    this.warningMessage = this.appConfigService.getConfig('global_setting')[
      'addResourceMessage'
    ];
    this.fileType = this.appConfigService.getConfig('general')[
      'customResourceData'
    ]['addResourceFileType'];
    this.fileSize = this.appConfigService.getConfig('global_setting')[
      'addResourceMaxFileSize'
    ];
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.shareCheckStatus = this.appConfigService.getConfig('global_setting')[
      'addResourceShareOption'
    ];
    this.addResourcePosition = this.appConfigService.getConfig(
      'global_setting'
    )['addResourceFormPosition'];
    this.addResourcePositionProperties = this.appConfigService.getConfig(
      'formPosition'
    );
    if (this.addResourcePosition && this.addResourcePositionProperties) {
      this.setThemeCSSVars(
        this.addResourcePosition,
        this.addResourcePositionProperties
      );
    }
    this.addResourceOptionsForm = new FormGroup({
      title: new FormControl(
        this.addResourceOptions.title,
        Validators.required
      ),
      gradeSubject: new FormControl(
        this.addResourceOptions.gradeSubject,
        Validators.required
      ),
      chapterTopic: new FormControl(
        this.addResourceOptions.chapterTopic,
        Validators.required
      ),
      getFile: new FormControl(this.addResourceOptions.getFile)
    });
    this.fileUploadService.addResourceFormData$.subscribe(data => {
      //console.log('fileUpload data', data);
      if (data) {
        this.olddata = data;
        this.getAssetId = data['assetId'];
        this.customFileName = data['file'];
        this.getFileName = this.customFileName;
        this.addResourceOptionsForm.controls.title.setValue(data['title']);
        this.addResourceOptionsForm.controls.gradeSubject.setValue(
          data['grade']
        );
        this.addResourceOptionsForm.controls.chapterTopic.setValue(
          data['chapter']
        );
        this.sharedCheck = data['share'];
        this.formStatus = data['type'];
        //console.log("form",this.formStatus)

        if (this.formStatus === 'resource-filter') {
          this.addResourceOptionsForm.setValidators([
            Validators.required,
            fileExtensionValidator(this.fileType)
          ]);
          this.clicktoRenameFile = false;
          this.setPosition(resourceFilterCss);
        }
        if (this.formStatus === 'resource-playlist') {
          this.addResourceOptionsForm.setValidators(null);
          this.clicktoRenameFile = true;
          this.setPosition(resourcePlaylistCss);
        }
        if (this.formStatus === 'resource-playlist-CL') {
          this.addResourceOptionsForm.setValidators(null);
          this.clicktoRenameFile = true;
          this.setPosition(resourcePlaylistCL);
        }
      }
    });

    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.myChapterUpload = fullContentSelection;
        }
      }
    );
    this.curriculumPlaylistService.availableChapters$.subscribe(
      (chapters: Chapter[]) => {
        this.availableChapters = chapters;
      }
    );
    this.curriculumPlaylistService.chapterSelection$.subscribe(newChapter => {
      if (newChapter) {
        this.selectedChapter = newChapter;
        this.availableTopics = newChapter.topics;
      }
    });
    this.curriculumPlaylistService.topicSelection$.subscribe(newTopic => {
      this.selectedTopic = newTopic;
    });
    this.curriculumPlaylistService.selectedSubjectChapterList$.subscribe(
      (chapters: Chapter[]) => {
        //console.log("CreateResourceComponent -> ngOnInit -> chapters", chapters)
        if (chapters && chapters.length > 0) {
          this.availableChapters = chapters;
          this.selectedChapter = chapters[0];
          this.availableTopics = chapters[0].topics;
          this.selectedTopic = chapters[0].topics[0];
          this.myChapterUpload.chapter = chapters[0];
          this.myChapterUpload.topic = chapters[0].topics[0];

          this.topicChaptervalue =
            this.myChapterUpload.chapter.chapterNumber +
            '.' +
            this.myChapterUpload.topic.topicNumber +
            ' | ' +
            this.myChapterUpload.topic.topicTitle;
          this.addResourceOptionsForm.controls.chapterTopic.setValue(
            this.topicChaptervalue
          );
        }
        this.cdr.detectChanges();
      }
    );
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          //console.log("CreateResourceComponent -> ngOnInit -> newClassSelection", newClassSelection)
          this.mySubjectUpload = newClassSelection;
        }
      }
    );

    this.fileUploadService.ResourceFlagBroadcaster$.subscribe(flag => {
      this.activeResourceMessage = flag;
    });
    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.availableGrades$.subscribe(
        (gradeLevels: GradeLevel[]) => {
          this.availableGrades = gradeLevels;
        }
      )
    );
    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.gradeLevelSelection$.subscribe(gradeLevel => {
        this.selectedGradeLevel = gradeLevel;
        if (gradeLevel) {
          this.availableDivisions = gradeLevel.divisions || [];
          this.availableSubjects = gradeLevel.subjects;
        }
        this.cdr.detectChanges();
      })
    );
    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.subjectSelection$.subscribe(subject => {
        if (subject) {
          this.selectedSubject = subject;
        }
      })
    );

    this.curriculumPlaylistService.selectedChapterErrorMessage$.subscribe(
      errorMessage => {
        if (errorMessage) {
          this.addResourceOptionsForm.controls.chapterTopic.setValue('');
        }
      }
    );
  }
  setThemeCSSVars(position, properties) {
    const documentElement = document.documentElement;
    for (const [key, value] of Object.entries(properties[position])) {
      documentElement.style.setProperty(`--${key}`, `${value}`);
    }
  }
  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }
  editFile() {
    this.clicktoRenameFile = !this.clicktoRenameFile;
  }
  updateResource() {
    this.resourceMessage = 'Updating Resource';
    this.showOpengradeSubject = false;
    this.showOpenChapterTopic = false;
    this.closeKeyboard();
    if (this.sharedCheck) {
      this.sharedCheck = 1;
    } else {
      this.sharedCheck = 0;
    }
    if (this.olddata && this.addResourceOptionsForm.value) {
      if (
        this.olddata.title === this.addResourceOptionsForm.value.title &&
        this.olddata.grade === this.addResourceOptionsForm.value.gradeSubject &&
        this.olddata.chapter ===
          this.addResourceOptionsForm.value.chapterTopic &&
        this.olddata.share === this.sharedCheck &&
        this.customFileName === this.getFileName
      ) {
        this.toastrService.error('Please Update some Filed');
      } else {
        let mode = 'teachMode';
        if(this.formStatus === 'resource-playlist-CL'){
          mode = 'planMode'
        }
        this.activeResourceMessage = true;
        this.fileUploadService.updateCustomResource(
          this.myChapterUpload,
          this.mySubjectUpload,
          this.myFileUpload,
          this.sharedCheck,
          this.addResourceOptionsForm.value.title,
          this.getAssetId,
          mode
        );

      }
    }
  }
  onSubmit() {
    this.resourceMessage = 'Creating Resource';
    this.showOpengradeSubject = false;
    this.showOpenChapterTopic = false;
    this.closeKeyboard();
    if (this.sharedCheck) {
      this.sharedCheck = 1;
    } else {
      this.sharedCheck = 0;
    }
    if (this.myFileUpload && this.myChapterUpload && this.mySubjectUpload) {
      this.activeResourceMessage = true;
      // let mycreateDetail = {
      //   chapter:this.myChapterUpload,
      //   subject:this.mySubjectUpload,
      //   file:this.myFileUpload,
      // }
      // console.log("mycreateDetail",mycreateDetail)
      this.fileUploadService.createCustomResource(
        this.myChapterUpload,
        this.mySubjectUpload,
        this.myFileUpload,
        this.sharedCheck,
        this.addResourceOptionsForm.value.title,
        'teachMode'
      );
    } else {
      this.toastrService.error(
        'File Size should be less then or equal to ' + this.fileSize
      );
    }
  }

  async upload(files: FileList) {
    this.closeKeyboard();
    this.converMBtoByte = Number(this.fileSize) * 1048576;
    if (files && files[0]) {
      const fileNameExtn =
        '.' +
        files[0].name
          .split('.')
          .pop()
          .toLowerCase();
      const arrFiletype = this.fileType.split(',');
      let showEtnMessage = await this.getExtationFlag(
        arrFiletype,
        fileNameExtn
      );

      if (!showEtnMessage) {
        if (files[0].size >= this.converMBtoByte) {
          this.myFileUpload = '';
          this.toastrService.error(
            'File Size should be less then or equal to ' + this.fileSize
          );
        } else {
          this.getFileName = files[0].name;
          this.myFileUpload = files.item(0);
        }
      } else {
        this.getFileName = this.customFileName;
        this.toastrService.error('Invalid File Extention');
      }
    } else {
      this.getFileName = this.customFileName;
      this.toastrService.error('File Required');
    }
  }
  async getExtationFlag(arr, currentExtn) {
    let xyz = true;
    arr.forEach(element => {
      if (element == currentExtn) {
        xyz = false;
      }
    });
    return xyz;
  }
  updateFieldValue(Name: string, val) {
    this.addResourceOptionsForm.controls[Name].setValue(val);
  }

  openGradeSubject(value){
    if(this.formStatus === 'resource-playlist-CL'){
      this.showOpengradeSubject = false;
      this.showOpenChapterTopic = false;
    }
    else{
      if(value === "chapter"){
        this.showOpenChapterTopic = !this.showOpenChapterTopic;
        this.showOpengradeSubject = false;
      }
      if(value === "grade"){
        this.showOpengradeSubject = !this.showOpengradeSubject;
        this.showOpenChapterTopic = false;
      }
    }
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.DEFAULT
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }
  displayvalueGradeSubject(subjectGrade) {
    //console.log("CreateResourceComponent -> displayvalueGradeSubject -> subjectGrade", subjectGrade)
    this.curriculumPlaylistService.setselectedSubjectChapter(subjectGrade);
    this.mySubjectUpload = subjectGrade;
    this.selectedGradeLevel = subjectGrade.gradeLevel;
    this.selectedSubject = subjectGrade.subject;
    this.gradeSubjectvalue =
      subjectGrade.gradeLevel.title + ' | ' + subjectGrade.subject.title;
    this.addResourceOptionsForm.controls.gradeSubject.setValue(
      this.gradeSubjectvalue
    );
    this.showOpengradeSubject = false;
    this.cdr.detectChanges();
  }

  displayvalueChapterTopic(ChapterTopic) {
    //console.log("CreateResourceComponent -> displayvalueChapterTopic -> ChapterTopic", ChapterTopic)
    this.selectedChapter = ChapterTopic.chapter;
    this.availableTopics = ChapterTopic.chapter.topics;
    this.selectedTopic = ChapterTopic.topic;
    this.myChapterUpload = ChapterTopic;
    this.topicChaptervalue =
      ChapterTopic.chapter.chapterNumber +
      '.' +
      ChapterTopic.topic.topicNumber +
      ' | ' +
      ChapterTopic.topic.topicTitle;
    this.addResourceOptionsForm.controls.chapterTopic.setValue(
      this.topicChaptervalue
    );
    this.showOpenChapterTopic = false;
    this.cdr.detectChanges();
  }

  toggleshare(event) {
    this.sharedCheck = !this.sharedCheck;
  }
  closeAddResourceForm() {
    this.showOpengradeSubject = false;
    this.showOpenChapterTopic = false;
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    this.addResourceOptionsForm.controls.getFile.setValue('');
    this.addResourceOptionsForm.controls.title.setValue('');
    this.fileUploadService.setAddResourceFlag(true);
   this.commonService.setEditResource('')
   this.commonService.setAddResourcePanel(false);
  }

  private setPosition(theme: {}) {
    Object.keys(theme).forEach(k =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }

  ngOnDestroy() {
    this.subjectSelectorSubscription.unsubscribe();
  }
}
