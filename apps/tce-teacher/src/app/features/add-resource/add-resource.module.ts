import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateResourceComponent } from './component/create-resource/create-resource.component';
import { LibConfigModule } from '@tce/lib-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@tce/core';
import { CommonSelectorModule } from '../common-selector/common-selector.module';

@NgModule({
  declarations: [CreateResourceComponent],
  imports: [
    CommonModule,
    CoreModule,
    CommonSelectorModule,
    LibConfigModule.forChild(CreateResourceComponent),
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [CreateResourceComponent],
  entryComponents: [CreateResourceComponent]
})
export class AddResourceModule {}
