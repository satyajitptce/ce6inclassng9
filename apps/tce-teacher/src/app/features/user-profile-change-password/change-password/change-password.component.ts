import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  ToolbarService,
  CommonService,
  KeyboardService,
  KeyboardState,
  KeyboardTheme
} from '@tce/core';
import { PlayerDrawerService } from '@app-teacher/services';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { Subscription } from 'rxjs';
import { PlanningModeService } from '../../../../../../../libs/planning-mode/src/lib/services/planning-mode.service';
import { ToastrService } from 'ngx-toastr';
export function ConfirmedValidator(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName];
    const matchingControl = formGroup.controls[matchingControlName];
    if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ confirmedValidator: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
}

@Component({
  selector: 'tce-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {
  _type = false;
  newUserTop: any;
  ChangePasswordMessage: any = '';
  activeChangePasswordMessage: any = false;
  pattern =
    '^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)(?=.*?[#?!@$%^&*-])[0-9a-zA-Z@$!%*#?&]{6,}$';
  changePasswordSubscription: Subscription = new Subscription();
  changePassword: FormGroup = new FormGroup({});
  @Input() currentMode;
  @Input('type')
  set type(flag) {
    //console.log("UserSettingViewComponent -> settype -> flag", flag)
    this._type = flag;
    this.loadDynamicCSS(flag);
  }
  get type() {
    return this._type;
  }
  constructor(
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService,
    private commonService: CommonService,
    private fb: FormBuilder,
    private keyboardService: KeyboardService,
    private planningModeService: PlanningModeService,
    private toastrService: ToastrService

  ) {
    this.changePassword = fb.group(
      {
        currentPassword: ['', [Validators.required]],
        newPassword: [
          '',
          [Validators.required, Validators.pattern(new RegExp(this.pattern))]
        ],
        repeatNewPassword: [
          '',
          [Validators.required, Validators.pattern(new RegExp(this.pattern))]
        ]
      },
      {
        validator: ConfirmedValidator('newPassword', 'repeatNewPassword')
      }
    );
  }

  ngOnInit(): void {
    this.changePasswordSubscription.add(
      this.toolbarService.toolbarSubmenuTop$.subscribe((top: any) => {
        if (top) {
          this.newUserTop = top.userTop - 20;
        }
      })
    );
    this.changePasswordSubscription.add(
      this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
        let getMainTop: any;
        if (flag) {
          getMainTop = 70;
          this.newUserTop = this.newUserTop - 50;
        } else {
          getMainTop = 85;
          this.newUserTop = this.newUserTop - 35;
        }
        const drawerDocumentElement = document.documentElement;
        drawerDocumentElement.style.setProperty(
          `--${'maincontainerTop'}`,
          `${getMainTop + 'px'}`
        );
        drawerDocumentElement.style.setProperty(
          `--${'arrowtop'}`,
          `${this.newUserTop + 'px'}`
        );
        // const userStae = {
        //   userView:'',
        //   currentState:''
        // }
        // this.commonService.setUserProfileState(userStae);
      })
    );
  }
  get f() {
    return this.changePassword.controls;
  }

  loadDynamicCSS(res) {
    let viewsidePosition: any;
    let viewsideRotation: any;
    if (!res) {
      viewsidePosition = '101%';
      viewsideRotation = 'rotate(270deg)';
    } else {
      viewsidePosition = '-15px';
      viewsideRotation = 'rotate(90deg)';
    }
    if (viewsidePosition && viewsideRotation) {
      const toolbarDocumentElement = document.documentElement;
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerposition'}`,
        `${viewsidePosition}`
      );
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerRotation'}`,
        `${viewsideRotation}`
      );
    }
  }

  submitChangePassword() {
    this.ChangePasswordMessage = 'Update Password';
    this.activeChangePasswordMessage = true;
    console.log('this.changePassword', this.changePassword);
    let newData = {
      op: 'replace',
      oldpassword: this.changePassword.value.currentPassword,
      newpassword: this.changePassword.value.newPassword
    };

    if (newData) {
      this.commonService.setNewPassword(newData);
      this.activeChangePasswordMessage = false;
      this.cancelChangePassword();

    }
  }

  cancelChangePassword() {
    this.closeKeyboard();
    const userStae = {
      userView: 'userProfile',
      currentState: 'profile'
    };
    if (this.currentMode === 'teaching') {
      this.commonService.setUserProfileState(userStae);
    }
    if (this.currentMode === 'planning') {
      this.planningModeService.setUserProfileState(userStae);
    }
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.DEFAULT
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }

  updateFieldValue(Name: string, val) {
    this.changePassword.controls[Name].setValue(val);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.closeKeyboard();
    this.changePasswordSubscription.unsubscribe();
  }
}
