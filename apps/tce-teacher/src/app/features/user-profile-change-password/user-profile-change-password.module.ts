import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { LibConfigModule } from '@tce/lib-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@tce/core';
@NgModule({
  declarations: [ChangePasswordComponent],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    LibConfigModule.forChild(ChangePasswordComponent)
  ],
  exports: [ChangePasswordComponent],
  entryComponents: [ChangePasswordComponent]
})
export class UserProfileChangePasswordModule {}
