import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlayerDrawerComponent } from './components/player-drawer/player-drawer.component';

const routes: Routes = [
  {
    path: '',
    component: PlayerDrawerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlayerDrawerRoutingModule {}
