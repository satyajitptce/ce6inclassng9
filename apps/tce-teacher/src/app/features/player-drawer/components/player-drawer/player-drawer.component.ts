import { LessonPlanService } from '@app-teacher/features/lesson-plan/services/lesson-plan.service';
import {
  Component,
  Input,
  ViewChild,
  ViewContainerRef,
  OnInit,
  ComponentRef
} from '@angular/core';
import { LibConfigService } from '@tce/lib-config';
import { CurriculumContainerComponent } from '@app-teacher/features/curriculum-playlist/components/curriculum-container/curriculum-container.component';
import {
  FullClassSelection,
  AppConfigService,
  GlobalConfigState,
  SubjectSelectorService,
  ToolbarService,
  CurriculumPlaylistService
} from '@tce/core';
import { PlayerContainerService } from '@app-teacher/services';
import { LessonPlanComponent } from '@app-teacher/features/lesson-plan/lesson-plan.component';
import { PlayerDrawerService } from '@app-teacher/services/player-drawer.service';

declare let tcePlayerReSize: any;

@Component({
  selector: 'tce-player-drawer',
  templateUrl: './player-drawer.component.html',
  styleUrls: ['./player-drawer.component.scss']
})
export class PlayerDrawerComponent implements OnInit {
  @ViewChild('curriculumPlaylistOutlet', {
    read: ViewContainerRef
})
  curriculumPlaylistOutlet: ViewContainerRef | undefined;

  @ViewChild('lessonPlanOutlet', {
    read: ViewContainerRef
})
  lessonPlanOutlet: ViewContainerRef | undefined;

  curriculumPlaylistRef: ComponentRef<CurriculumContainerComponent> = null;
  lessonPlanRef = null;
  openLessonPlan = false;
  isPlayerDrawerOpen: boolean;
  isPlayerDrawerVisible = false;
  topicSelectionRecentlyMade = false;

  constructor(
    private libConfigService: LibConfigService,
    private subjectSelectorService: SubjectSelectorService,
    private appConfigService: AppConfigService,
    private playerContainerService: PlayerContainerService,
    private lessonPlanService: LessonPlanService,
    private playerDrawerService: PlayerDrawerService,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {}

  ngOnInit() {
    this.playerDrawerService.setDrawerOpenStatus(true);
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (classSelection: FullClassSelection) => {
        if (classSelection) {
          this.loadCurriculumPlaylist();
          this.isPlayerDrawerOpen = true;
        }
      }
    );
    if (
      this.appConfigService.getGlobalSettingConfig('playlistautoclose') ===
      GlobalConfigState.ENABLED
    ) {
      this.playerContainerService.addOpenedResource$.subscribe(() => {
        if (!this.topicSelectionRecentlyMade) {
          this.playerDrawerService.setDrawerOpenStatus(false);
        }
      });
      this.playerContainerService.reopenResource$.subscribe(() => {
        if (!this.topicSelectionRecentlyMade) {
          this.playerDrawerService.setDrawerOpenStatus(false);
        }
      });
    }
    /*
    Note from Josh: Here, we are setting a timeout for the sake of giving a 1 second "grace period", where if
    the topic was recently changed, we DON'T try to automatically close the drawer on open of a player. This is to account for the
    weird timing of the restoration of a topic's whiteboard state and the full content selection. Probably a better way to marshall
    all of this, will need to look into it in more depth at some point.
     */
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      fullContentSelection => {
        if (fullContentSelection) {
          this.topicSelectionRecentlyMade = true;
          this.playerDrawerService.setDrawerOpenStatus(true);
          setTimeout(() => {
            this.topicSelectionRecentlyMade = false;
          }, 1000);
        }
      }
    );

    this.playerDrawerService.drawerOpenStatus$.subscribe(newOpenStatus => {
      this.isPlayerDrawerOpen = newOpenStatus;
    });

    this.playerDrawerService.drawerVisibilityStatus$.subscribe(
      newVisibilityStatus => (this.isPlayerDrawerVisible = newVisibilityStatus)
    );

    this.lessonPlanService.currentLessonPlan$.subscribe(lessonPlan => {
      this.loadLessonPlan();
    });
  }

  togglePlayerDrawer() {
    this.playerDrawerService.setDrawerOpenStatus(!this.isPlayerDrawerOpen);
  }

  loadLessonPlan() {
    if (!this.lessonPlanRef) {
      this.libConfigService
        .getComponentFactory<LessonPlanComponent>('lazy-lesson-plan')
        .subscribe({
          next: componentFactory => {
            if (!this.lessonPlanOutlet) {
              return;
            }
            this.lessonPlanRef = this.lessonPlanOutlet.createComponent(
              componentFactory
            );
            this.lessonPlanRef.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }

  loadCurriculumPlaylist() {
    if (!this.curriculumPlaylistRef) {
      this.libConfigService
        .getComponentFactory<CurriculumContainerComponent>(
          'curriculum-playlist'
        )
        .subscribe({
          next: componentFactory => {
            if (!this.curriculumPlaylistOutlet) {
              return;
            }
            this.curriculumPlaylistRef = this.curriculumPlaylistOutlet.createComponent(
              componentFactory
            );

            this.curriculumPlaylistRef.changeDetectorRef.detectChanges();

            this.playerDrawerService.curriculumPlaylistHeight = (this
              .curriculumPlaylistRef.location
              .nativeElement as HTMLElement).clientHeight;
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }
}
