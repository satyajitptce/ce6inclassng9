import { LessonPlanModule } from './../lesson-plan/lesson-plan.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from '@tce/toolbar';

import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';
import { PlayerDrawerRoutingModule } from './player-drawer-routing.module';
import { PlayerDrawerComponent } from './components/player-drawer/player-drawer.component';
import { SubjectSelectorModule } from '../subject-selector/subject-selector.module';

const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'curriculum-playlist',
    path: 'curriculum-playlist', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../curriculum-playlist/curriculum-playlist.module').then(
        mod => mod.CurriculumPlaylistModule
      )
  },
  {
    componentId: 'subject-selector-drawer',
    path: 'subject-selector', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../subject-selector/subject-selector.module').then(
        mod => mod.SubjectSelectorModule
      )
  },
  {
    componentId: 'lazy-lesson-plan',
    path: 'lazy-lesson-plan', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../lesson-plan/lesson-plan.module').then(
        mod => mod.LessonPlanModule
      )
  }
];

@NgModule({
  declarations: [PlayerDrawerComponent],
  imports: [
    CommonModule,
    ToolbarModule,
    LibConfigModule.forRoot(manifests),
    PlayerDrawerRoutingModule,
    SubjectSelectorModule,
    LibConfigModule.forChild(PlayerDrawerComponent)
  ],
  exports: [PlayerDrawerComponent]
})
export class PlayerDrawerModule {}
