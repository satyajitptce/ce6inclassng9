import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EbookSelectorComponent } from './ebook-selector.component';

describe('EbookSelectorComponent', () => {
  let component: EbookSelectorComponent;
  let fixture: ComponentFixture<EbookSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EbookSelectorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EbookSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
