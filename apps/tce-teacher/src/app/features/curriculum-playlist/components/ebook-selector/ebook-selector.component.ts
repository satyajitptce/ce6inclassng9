import {
  Component,
  OnInit,
  Input,
  Output,
  HostListener,
  ElementRef
} from '@angular/core';
import { EventEmitter } from 'events';
import { PlayerContainerService } from '@app-teacher/services';
import {
  Resource,
  ResourceType,
  Ebook,
  AppConfigService,
  CurriculumPlaylistService,
  ToolbarService,
  CommonService
} from '@tce/core';
// import {EbookService} from '../../../../../../../../libs/player-ebook/src/lib/services/ebook.service'
interface IBook {
  thumbnail: string;
  title: string;
  author: string;
}

@Component({
  selector: 'tce-ebook-selector',
  templateUrl: './ebook-selector.component.html',
  styleUrls: ['./ebook-selector.component.scss']
})
export class EbookSelectorComponent implements OnInit {
  //td@old code  @Input() ebooks: Ebook[];
  @Input() ebooks: Ebook[] = [];
  @Output() ebookSelected = new EventEmitter();

  currentEbook: Ebook = (this.ebooks && this.ebooks[0]) || null;
  isOpen = false;
  public ebookPlayerOpen = false;
  public selectedBook: Ebook;
  public selectorBackground;
  public selectorTitle;
  public imgSrc;

  constructor(
    private playerContainerService: PlayerContainerService,
    private eRef: ElementRef,
    private appConfigService: AppConfigService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private toolbarservice: ToolbarService, // public ebookService: EbookService
    private commonService: CommonService
  ) {
    //console.log('currentEbook', this.currentEbook);
  }

  ngOnInit() {
    this.playerContainerService.destroyAllPlayers$.subscribe(shouldDestroy => {
      if (shouldDestroy) this.ebookPlayerOpen = false;
    });

    this.curriculumPlaylistService.ebookSelection$.subscribe(ebook => {
      this.ebookPlayerOpen = true;
      this.selectedBook = ebook;
    });
  }

  selectorClicked() {

    //this.ebookService.setebookResourcelistFlag(false);
    this.isOpen = !this.isOpen;
    if(this.isOpen){
      this.commonService.setLessonPlayerMode(false)
    }

    if (this.isOpen && this.ebookPlayerOpen) {
      const resource = this.selectedBook.convertToResource();
      this.playerContainerService.closeResource(resource);
      this.ebookPlayerOpen = false;
      this.playerContainerService.saveCurrentPlayerState();
      if (!this.ebookPlayerOpen) {
        this.commonService.setebookResourcelistFlag(false);
      }
      this.isOpen = false;
    }
  }

  openBook(book) {
    //console.log('openbook', book);

    this.selectedBook = book;
    const resource = this.selectedBook.convertToResource();
    this.isOpen = true;
    this.ebookPlayerOpen = true;
    this.playerContainerService.openResource(resource);
    this.playerContainerService.saveCurrentPlayerState();
  }

  getThumbnail(thumbnail) {
    if (!thumbnail) {
      return this.appConfigService.getResourceThumbnailIcon(
        'worksheet',
        'worksheet',
        'thumbnail'
      );
    } else {
      return thumbnail;
    }
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (this.eRef.nativeElement.contains(event.target)) {
      if (this.ebookPlayerOpen) this.isOpen = true;
    } else {
      this.isOpen = false;
    }
  }
}
