import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  ChangeDetectorRef
} from '@angular/core';
import {
  CurriculumPlaylistService,
  FilterMenu,
  ToolbarService,
  FullContentSelection,
  FullClassSelection,
  SubjectSelectorService,
  Resource,
  FileUploadService,
  AppConfigService,
  KeyboardService,
  CommonService
} from '@tce/core';
import { PlayerDrawerService } from '@app-teacher/services/player-drawer.service';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
@Component({
  selector: 'tce-resource-filter-panel',
  templateUrl: './resource-filter-panel.component.html',
  styleUrls: ['./resource-filter-panel.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ]),
    trigger('slideInOutX', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateX(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class ResourceFilterPanelComponent implements OnInit {
  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private eRef: ElementRef,
    private toolbarService: ToolbarService,
    private subjectSelectorService: SubjectSelectorService,
    private fileUploadService: FileUploadService,
    private appConfigService: AppConfigService,
    private keyboardService: KeyboardService,
    private playerDrawerService: PlayerDrawerService,
    private cdr: ChangeDetectorRef,
    private commonService: CommonService
  ) {
    
  }
  onEditValue;
  filteredMenu: FilterMenu[] = [];
  allSelected = false;
  showFilterMenu = false;
  showEditOption: any = false;
  showFilter = false;
  showReset = false;
  getUpdateResourceList: any;
  collectionPlaylist: any = [];
  currentGrade: any;
  currentSubject: any;
  showAddResource: any = true;
  showCreateOption: any = false;
  currentResources: Resource[];
  clickCreateResource: any = false;
  topicChaptervalue: any;
  myChapterUpload: any;
  mySubjectUpload: any;
  gradeSubjectvalue: any;
  addResourcePosition: any;
  addResourcePositionProperties: any;
  animationFlag: any;
  currentTopicId: any;
  sharedFlag: any;
  resourcePlaylistOpen: any;
  currentTpoicResource: any;
  myUpdatedResuorce: any;
  setTempFlag;
  ngOnInit() {
    // console.log("Thiru----->",this.clickCreateResource);
    
    // this.commonService.addResourcePanel$.subscribe(state=>{
    //   console.log("addResourcePanel -->",state);
    //   if(state){
    //     this.fileUploadService.setAddResourceFlagBroadcaster('create');
    //     this.clickCreateResource = true;
    //   }

    // })
    
    this.commonService.addResourceState$.subscribe(state=>{
      //console.log("state",state);
      this.showAddResource = state

    })

    this.commonService.onDeleteResource$.subscribe(state=>{
      //console.log("state",state);
      this.showEditOption = state;

    })

    this.commonService.tempUpdateResourceList$.subscribe(data=>{
      //console.log("currentTempData-->>",data)
      this.setTempFlag = data

    })

    this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
      this.resourcePlaylistOpen = flag;
      if (!flag) {
        this.showAddResource = true;
        this.showCreateOption = true;
        this.fileUploadService.setAddResourceFlagBroadcaster('');
        this.closeKeyboard();
      }
    });
    this.fileUploadService.addResourceFlag$.subscribe(flag => {
      this.showAddResource = flag;
      this.cdr.detectChanges();
    });
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.sharedFlag = this.appConfigService.getConfig('global_setting')[
      'addResourceShareValue'
    ];
    this.addResourcePosition = this.appConfigService.getConfig(
      'global_setting'
    )['addResourceFormPosition'];
    this.addResourcePositionProperties = this.appConfigService.getConfig(
      'formPosition'
    );

    this.toolbarService.editMode$.subscribe(flag => {
      this.showEditOption = flag;
      this.cdr.detectChanges();
    });
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.myChapterUpload = fullContentSelection;
          this.topicChaptervalue =
            fullContentSelection.chapter.chapterNumber +
            '.' +
            fullContentSelection.topic.topicNumber +
            ' | ' +
            fullContentSelection.topic.topicTitle;
        }
      }
    );
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.mySubjectUpload = newClassSelection;
          this.gradeSubjectvalue =
            newClassSelection.gradeLevel.title +
            ' | ' +
            newClassSelection.subject.title;
        }
      }
    );
    this.commonService.openAddResourceFormBrodCast$.subscribe((data: any) => {
      this.playerDrawerService.setDrawerOpenStatus(true);
      this.createResource();
      this.showAddResource = false;
    });
    this.curriculumPlaylistService.topicSelection$.subscribe(currentTopic => {
      if (currentTopic) {
        this.currentTopicId = currentTopic.topicId;
        this.currentTpoicResource = currentTopic;
        // console.log(
        //   'ResourcePlaylistComponent -> ngOnInit -> currentTopic',
        //   currentTopic
        // );
      }
    });
    this.subjectSelectorService.gradeLevelSelection$.subscribe(grade => {
      if (grade) {
        //console.log('filter-grade', grade);
        this.currentGrade = grade.id;
      }
    });
    this.subjectSelectorService.subjectSelection$.subscribe(subject => {
      if (subject) {
        //console.log('filter-subject', subject);
        this.currentSubject = subject.subjectId;
      }
    });
    this.collectionPlaylist = [
      {
        id: 1,
        name: 'Edit',
        svgSrc: 'assets/images/playlist/icons/ic.playlist.Edit.svg'
      },
      {
        id: 2,
        name: 'Reset Playlist',
        svgSrc: 'assets/images/playlist/icons/ic.Playlist.ResetPlaylist.svg'
      }
    ];
    this.curriculumPlaylistService.filteredMenu$.subscribe(
      (filteredMenu: FilterMenu[]) => {
        this.filteredMenu = filteredMenu;
        //console.log("filter Data",this.filteredMenu)
      }
    );
    this.commonService.updateResourceList$.subscribe(data => {
      if (data) {
        //console.log("Z ResourceFilterPanelComponent -> ngOnInit -> data", data)
        this.getUpdateResourceList = data;
        this.myUpdatedResuorce = data;
        //console.log("Zdrop -- this.myUpdatedResuorce-->> ",this.myUpdatedResuorce)
        if (data['type'] === 'search') {
          this.doneWithEdit();
        }
      }
    });
    this.curriculumPlaylistService.availableResources$.subscribe(
      (resources: Resource[]) => {
        //console.log("-->>resources-->>",resources)
        this.currentResources = resources;
      }
    );
  }
  toggleFilter(menuItem: FilterMenu) {
    //console.log("toggleFilter",menuItem)
    this.curriculumPlaylistService.toggleResourceFilter(menuItem);
     //this.toolbarService.setEditModeResoursePlayerList(true);

    // if(this.onEditValue === "Edit")
    // {
    //   this.toolbarService.setEditModeResoursePlayerList(true);
    // }

  }
  selectAll() {
    this.allSelected = !this.allSelected;
    this.curriculumPlaylistService.toggleAllResourceFilters(this.allSelected);
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event: Event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.showFilterMenu = false;
      if (!this.showAddResource) {
        //this.closeAll();
      }
    }
  }

  selectedPlaylist(value) {
    
    this.showFilterMenu = false;
    if (value === 'Edit') {
      this.onEditValue = value;
      this.showEditOption = true;
      this.toolbarService.setEditModeResoursePlayerList(true);
    }
    if (value === 'Reset Playlist') {
      this.showReset = true;
    }
  }

  doneWithEdit() {
    
    
    //console.log("finsih editing---> ",this.getUpdateResourceList)
    //console.log("this.currentResources-->>",this.currentResources)
    //console.log("finish this.currentTpoicResource-->>",this.currentTpoicResource)
    
    
    if (
      this.getUpdateResourceList &&
      this.getUpdateResourceList.playlistJson &&
      this.getUpdateResourceList.playlistJson.length > 0
    ) {
      this.currentTpoicResource.resources = this.getUpdateResourceList.currentResource;

    

      if(this.setTempFlag === 'TEMP'){
        console.log("FOUND TEMP")
      }else {
        console.log("NOT FOUND TEMP")
        this.curriculumPlaylistService.setTopicSelection(this.currentTpoicResource);

      }
      
       //this.curriculumPlaylistService.resetResourceFilters(this.currentResources);
      this.showEditOption = false;
      this.toolbarService.setEditModeResoursePlayerList(false);
      if (
        this.getUpdateResourceList &&
        this.currentGrade &&
        this.currentSubject
      ) {
        let newdata = {
          playlistJson: this.getUpdateResourceList,
          gradeId: this.currentGrade,
          subjectId: this.currentSubject
        };

         this.currentTpoicResource.resources = this.getUpdateResourceList.currentResource;
         
         //this.curriculumPlaylistService.setTopicSelection(this.currentTpoicResource);
        
        this.curriculumPlaylistService.UpdateResourceList(newdata);
      }
    } else {
      this.toolbarService.setEditModeResoursePlayerList(false);
    }
    setTimeout(() => {
      this.commonService.setTempResourceList("")
      console.log("RESET TEMP")
    
    }, 2000);
  }

  cancelResetResource() {
    this.showReset = false;
  }
  resetResource() {
    let currentTpId = this.currentTopicId;
    this.cancelResetResource();
    if (
      this.currentResources.length > 0 &&
      this.currentGrade &&
      this.currentSubject
    ) {
      if (currentTpId) {
        let newdata = {
          tpId: currentTpId,
          gradeId: this.currentGrade,
          subjectId: this.currentSubject
        };
        this.curriculumPlaylistService.resetResourceList(newdata);
      }
    }
  }

  createResource() {
    const customData = {
      title: '',
      grade: this.gradeSubjectvalue,
      chapter: this.topicChaptervalue,
      file: '',
      share: this.sharedFlag,
      type: 'resource-filter',
      assetId: ''
    };
    this.fileUploadService.setAddResourceFormData(customData);
    this.clickCreateResource = !this.clickCreateResource;
    this.fileUploadService.setAddResourceFlagBroadcaster('create');
    this.commonService.setAddResourcePanel(this.clickCreateResource)
  }
  openLib() {
    this.clickCreateResource = !this.clickCreateResource;
    this.fileUploadService.setAddResourceFlagBroadcaster('lib');
  }
  closeAll() {
    this.showAddResource = !this.showAddResource;
    this.showCreateOption = false;
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    this.closeKeyboard();
  }
  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }
  openResourcList() {
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    this.showFilterMenu = !this.showFilterMenu;
    this.playerDrawerService.setDrawerOpenStatus(true);
  }
  openCreateLib() {
    this.showFilterMenu = false;
    this.showAddResource = !this.showAddResource;
    this.clickCreateResource = false;
    this.fileUploadService.setAddResourceFlagBroadcaster('');
  }
}
