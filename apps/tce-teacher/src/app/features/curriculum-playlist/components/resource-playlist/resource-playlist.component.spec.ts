import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResourcePlaylistComponent } from './resource-playlist.component';

describe('ResourcePlaylistComponent', () => {
  let component: ResourcePlaylistComponent;
  let fixture: ComponentFixture<ResourcePlaylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ResourcePlaylistComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResourcePlaylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
