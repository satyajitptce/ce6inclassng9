import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
  ElementRef,
  IterableDiffer,
  IterableDiffers,
  AfterContentChecked,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import {
  CdkDragDrop,
  moveItemInArray,
  CdkDrag,
  CdkDropList,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import {
  Resource,
  ResourceType,
  AppConfigService,
  FullContentSelection,
  CurriculumPlaylistService,
  RequestApiService,
  FullClassSelection,
  ToolbarService,
  FilterMenu,
  SubjectSelectorService,
  FileUploadService,
  CommonService,
  UserProfileService,
  PlayerUsageService
} from '@tce/core';
import {
  PlayerContainerService,
  PlayerDrawerService
} from '@app-teacher/services';
import { interval } from 'rxjs';
import { Subscription } from 'rxjs';
import { Container } from '@angular/compiler/src/i18n/i18n_ast';
import { QuestionEditorService } from '@tce/template-editor';
declare let stopAllTCEMedia: any;

@Component({
  selector: 'tce-resource-playlist',
  templateUrl: './resource-playlist.component.html',
  styleUrls: ['./resource-playlist.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ResourcePlaylistComponent implements OnInit, AfterContentChecked {
  _resources: Resource[];
  @Input('resources')
  set resources(resource) {
    if (resource && resource.length > 0) {
      //console.log('this.myResource playlist', this.myResource);
      console.log('resource playlist', resource);

      this.curriculumPlaylistService.setResourcesLoadingError('');
      this._resources = resource;
      //this.myResource = resource;
      if (this.userPreferViewData && this.userPreferViewData.length > 0) {
        // console.log('userPreferView----------->', this.userPreferViewData);
        for (let index = 0; index < this.userPreferViewData.length; index++) {
          this.userPreferViewData[index].selected = true;
          this.userProfileService.setUserPrefereViewFilter(
            this.userPreferViewData[index]
          );
          //this.toggleFilter(data[index])
        }
        // this.curriculumPlaylistService.getCustomSharedData(
        //   this._resources
        // );
      } else {
        // console.log('else-------->THIRU', this.userPreferViewData);

        this.userPreferViewData = [];
        //this.curriculumPlaylistService.resetResourceFilters(resource);
      }
    } else {
      this._resources = [];
      //this.curriculumPlaylistService.setResourcesLoadingError('No Resources Available for Topic');
      // satyajit during loading the message was appearing.
      this.curriculumPlaylistService.setResourcesLoadingError('');
    }
    //console.log("--resource-->>",resource)
    this.cdr.detectChanges();
  }
  get resources() {
    return this._resources;
  }
  @Output() resourceSelected = new EventEmitter();

  //thiru added myResource for drag and drop data update(25th feb 21)
  myResource;
  isMyresourceAppended = true;
  dataForDROP;
  //-------------------------------------------------
  openResourceIds: string[] = [];
  newResourceList: any = {};
  showResourceNavArrows = false;
  isAtBeginningOfResources = true;
  isAtEndOfResources = true;
  hasPreviousResources = false;
  hasMoreResources = false;
  activeResourceId = '';
  resourceCardWrapper = null;
  selectedResourceDelet = false;
  deleteResource: any;
  isLoading = false;
  errorMessage = '';
  playerDrawerOpen = true;
  editmodeResourece: any = false;
  private intervalScroll;
  deleteResourceList: any = [];
  currentTopicId: any;
  topicChaptervalue: any;
  gradeSubjectvalue: any;
  currentFullSubject: any;
  currentFullChapter: any;
  currentTpoicResource: any;
  selectedResourcIsOwnerOption = false;
  userPreferViewData: any = [];
  private resourceDiffer: IterableDiffer<Resource>;
  private readonly scrollArrowVelocity = 20; // Resource Card Width + margin
  private readonly mouseScrollVelocity = 20;
  custEvent: any;
  resourceListSubscription: Subscription = new Subscription();
  resourceIcons = {
    [ResourceType.WORKSHEET]: './assets/images/playlist/icons/ic.Worksheet.svg',
    [ResourceType.TCEVIDEO]:
      './assets/images/playlist/icons/ic.AVMediaVideo.svg',
    [ResourceType.QUIZ]: './assets/images/playlist/icons/ic.Quiz.svg',
    [ResourceType.INTERACTIVITY]:
      './assets/images/playlist/icons/ic.Activity.svg'
  };
  constructor(
    private iterableDiffers: IterableDiffers,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private playerContainerService: PlayerContainerService,
    private elRef: ElementRef,
    private appConfigService: AppConfigService,
    private requestApiService: RequestApiService,
    private playerDrawerService: PlayerDrawerService,
    private toolbarService: ToolbarService,
    private subjectSelectorService: SubjectSelectorService,
    private fileUploadService: FileUploadService,
    private cdr: ChangeDetectorRef,
    private commonService: CommonService,
    private userProfileService: UserProfileService,
    private playerUsageService: PlayerUsageService,
    private questionEditorService: QuestionEditorService
  ) {
    this.resourceDiffer = this.iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.currentFullChapter = fullContentSelection;
          this.topicChaptervalue =
            fullContentSelection.chapter.chapterNumber +
            '.' +
            fullContentSelection.topic.topicNumber +
            ' | ' +
            fullContentSelection.topic.topicTitle;
        }
      }
    );
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.currentFullSubject = newClassSelection;
          this.gradeSubjectvalue =
            newClassSelection.gradeLevel.title +
            ' | ' +
            newClassSelection.subject.title;
        }
      }
    );
    this.resourceListSubscription.add(
      this.curriculumPlaylistService.topicSelection$.subscribe(currentTopic => {
        if (currentTopic) {
          // console.log('currentTopic++++++++++++++++++');
          this.currentTpoicResource = currentTopic;
          this.currentTopicId = currentTopic.topicId;
          this.curriculumPlaylistService.getCustomSharedData(
            this.currentTpoicResource.resources
          );
          this.cdr.detectChanges();
        }
      })
    );

    this.toolbarService.editMode$.subscribe(data => {
      this.deleteResource = '';
      this.selectedResourceDelet = false;
      this.editmodeResourece = data;
      this.cdr.detectChanges();
    });
    this.playerContainerService.openResources$.subscribe(resources => {
      //console.log('playerContainerService', resources);
      this.openResourceIds = resources.map(resource => resource.resourceId);
      this.cdr.detectChanges();
    });
    this.playerContainerService.currentActiveResourceId$.subscribe(
      resourceId => {
        if (typeof stopAllTCEMedia === 'function') {
          stopAllTCEMedia();
        }
        this.activeResourceId = resourceId;
        this.cdr.detectChanges();
      }
    );
    this.curriculumPlaylistService.resourcesFetching$.subscribe(
      fetchState => (this.isLoading = fetchState)
    );
    this.curriculumPlaylistService.resourcesLoadingError$.subscribe(
      errorMessage => (this.errorMessage = errorMessage)
    );

    this.curriculumPlaylistService.appliedResourceFilters$.subscribe(
      (appliedFilters: FilterMenu[]) => {
        // console.log('hello new ---resources',appliedFilters);
        if (this.resources && this.resources.length > 0) {
          this.resources.forEach((resource: Resource) => {
            resource.filterStatus = false;
            if (appliedFilters.length === 0) {
              resource.show = true;
            }
          });

          appliedFilters.forEach((menu: FilterMenu) => {
            if (menu.selected) {
              this.resources.forEach((resource: Resource) => {
                if (menu.type.includes(resource.resourceType)) {
                  resource.show = true;
                  resource.filterStatus = true;
                } else {
                  if (!resource.filterStatus) resource.show = false;
                }
              });
            }
          });
          //console.log("appliedFilters",appliedFilters)
        }
        this.updataResourceFilter(this.resources);
        this.cdr.detectChanges();
      }
    );
    this.playerDrawerService.drawerOpenStatus$.subscribe((status: boolean) => {
      this.playerDrawerOpen = status;
      this.selectedResourceDelet = false;
      this.selectedResourcIsOwnerOption = false;
    });

    this.commonService.SearchResource$.subscribe((resource: Resource) => {
      if (resource) {
        this.handleResourceClick(resource);
      }
    });
    this.userProfileService.userPrefereViewListBroadcast$.subscribe(
      (data: any) => {
        this.userPreferViewData = data;
        if (data && data.length > 0) {
          for (let index = 0; index < data.length; index++) {
            this.userProfileService.setUserPrefereViewFilter(data[index]);
            //this.toggleFilter(data[index])
          }
        } else {
          this.curriculumPlaylistService.resetResourceFilters(
            this.resources,
            'else'
          );
        }
      }
    );

    if (
      this.userProfileService.userProfileData.userSettings.addInfo1 != undefined
    ) {
      this.userPreferViewData = JSON.parse(
        this.userProfileService.userProfileData.userSettings.addInfo1
      );
    }

    if (this.userPreferViewData && this.userPreferViewData.length > 0) {
      for (let index = 0; index < this.userPreferViewData.length; index++) {
        this.userProfileService.setUserPrefereViewFilter(
          this.userPreferViewData[index]
        );
        //this.toggleFilter(data[index])
      }
    } else {
      this.curriculumPlaylistService.resetResourceFilters(
        this.resources,
        'resources--else'
      );
    }

    //thiru temporary for display issue with only custom resource and custom chapter and topic
    setTimeout(() => {
    this.curriculumPlaylistService.setTopicSelection(this.curriculumPlaylistService.auxTopic,'auxTopic selcetion');
    }, 1800);

  }
  updataResourceFilter(resource) {
    //thiru
    //    console.log("original resource -->> ", resource)
    this.myResource = [];
    this.dataForDROP = [];
    for (let i = 0; i < resource.length; i++) {
      if (resource[i].filterStatus === true) {
        this.myResource.push(resource[i]);
      }
    }

    let filter = [];
    let result = [];

    for (let j = 0; j < this.myResource.length; j++) {
      filter.push(this.myResource[j].resourceId);
    }
    //console.log('filter-->>',filter)

    filter.forEach(function(key) {
      let found = false;
      resource = resource.filter(function(item) {
        if (!found && item.resourceId === key) {
          result.push(item);
          found = true;
          return false;
        } else {
          return true;
        }
      });
    });
    //console.log('result-->>',result)

    result.forEach(function(key) {
      let found = false;
      resource = resource.filter(function(item) {
        if (!found) {
          result.push(item);
          found = true;
          return false;
        } else {
          return true;
        }
      });
    });
    resource = result;
    //satyajit now data is accurate with sequncing
    //console.log("final resource -->> ", resource)
  }

  ngAfterContentChecked() {
    const changes = this.resourceDiffer.diff(this.resources);
    if (changes) {
      this.resetScrollLeft();
      setTimeout(() => {
        this.resourceCardWrapper = this.elRef.nativeElement.querySelector(
          '.resource-card-wrapper'
        );
        this.calculateResourceSliderThresholds();
        this.cdr.detectChanges();
      }, 0);
    }
  }

  handleResourceClick(resource: Resource) {
    // console.log('handleResourceClick');
    this.questionEditorService.updateSubmitAnsShow(false);

    if (resource.downloadFileExtension) {
      const suppredString = resource.fileName.split('.')[0];
      resource.fileName = suppredString + '.pdf';
    } else {
      resource.fileName = resource.fileName;
    }

    const { resourceId } = resource;

    this.playerUsageService.logResourceUsage(resource);

    if (this.editmodeResourece) {
      event.stopPropagation();
    } else {
      if (this.openResourceIds.includes(resourceId)) {
        this.playerContainerService.setCurrentActiveResourceId(resourceId);
      } else {
        this.playerContainerService.openResource(resource);
      }
      this.playerContainerService.saveCurrentPlayerState();
    }
  }

  handleResourceCloseClick(event: MouseEvent, resource: Resource) {
    //console.log("handleResourceCloseClick")
    event.stopPropagation();
    this.playerContainerService.closeResource(resource);
    if (resource.resourceType === 'custom-video') {
      this.fileUploadService.customPlayerFlag.next(true);
    }
    this.playerContainerService.saveCurrentPlayerState();
    this.playerUsageService.logResourceUsageOnClose(resource);
  }

  scrollEvent(direction: string, event: Event) {
    const cachedCurrentTarget = event.currentTarget;
    if (event.type === 'mousedown' || event.type === 'touchstart') {
      this.emitScrollEvent(event, cachedCurrentTarget, direction);

      this.intervalScroll = setInterval(() => {
        this.emitScrollEvent(event, cachedCurrentTarget, direction);
      }, 100);
    } else {
      clearInterval(this.intervalScroll);
      this.emitScrollEvent(event, cachedCurrentTarget, null);
    }
  }

  emitScrollEvent(
    event: Event,
    cachedCurrentTarget: EventTarget,
    direction: string
  ) {
    if (direction) {
      this.selectedResourceDelet = false;
      this.selectedResourcIsOwnerOption = false;
      const scrollVelocity =
        direction === 'left'
          ? -this.scrollArrowVelocity
          : this.scrollArrowVelocity;
      this.resourceCardWrapper.scrollLeft += scrollVelocity;
      this.calculateResourceSliderThresholds();
    }
  }

  private resetScrollLeft() {
    setTimeout(() => {
      if (this.resourceCardWrapper) {
        this.resourceCardWrapper.scrollLeft = 0;
      }
    }, 5);
  }

  private calculateResourceSliderThresholds() {
    if (this.resourceCardWrapper) {
      this.hasMoreResources =
        this.resourceCardWrapper.scrollWidth >
        this.resourceCardWrapper.clientWidth;

      this.showResourceNavArrows = this.hasMoreResources === true;

      this.isAtBeginningOfResources = this.resourceCardWrapper.scrollLeft === 0;

      this.isAtEndOfResources =
        this.resourceCardWrapper.scrollLeft >=
        this.resourceCardWrapper.scrollWidth -
          this.resourceCardWrapper.clientWidth;
    } else {
      this.hasMoreResources = false;
      this.isAtBeginningOfResources = true;
      this.isAtEndOfResources = true;
      this.showResourceNavArrows = false;
    }
  }

  private isEndOfResources() {
    const ele = this.resourceCardWrapper;
    return ele.scrollLeft >= ele.scrollWidth - ele.clientWidth;
  }

  public getResourceThumbnailIcon(resource: Resource, type: string) {
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }

  getResourceThumbnail(thumbnailParams) {
    if (thumbnailParams) {
      return this.requestApiService.getUrl('getFile') + thumbnailParams;
    } else {
      return '';
    }
  }
  checkCustomResource(resoueceId: string) {    
    if (resoueceId && resoueceId.startsWith('casset')) {
      return false;
    } else {
      return true;
    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    this.calculateResourceSliderThresholds();
  }

  @HostListener('DOMMouseScroll', ['$event'])
  onScroll(event: WheelEvent) {
    //prevent default to keep body scroll from "rubber-banding" when this element is scrolled.
    if (this.resourceCardWrapper) {
      //event.preventDefault();
      const delta = Math.max(-1, Math.min(1, -event.detail));
      this.resourceCardWrapper.scrollLeft -= delta * this.mouseScrollVelocity;

      this.calculateResourceSliderThresholds();
    }
  }

  @HostListener('mousewheel', ['$event'])
  handleMouseWheel(event: WheelEvent) {
    if (this.resourceCardWrapper) {
      //prevent default to keep body scroll from "rubber-banding" when this element is scrolled.
      //event.preventDefault();
      // Touchpad always has very small deltas, return and don't manually
      // set scroll value if using touchpad
      if (event.deltaY > -4 && event.deltaY < 4) {
        return;
      }

      if (event.deltaY > 0) {
        this.resourceCardWrapper.scrollLeft += this.mouseScrollVelocity;
      } else {
        this.resourceCardWrapper.scrollLeft -= this.mouseScrollVelocity;
      }

      this.calculateResourceSliderThresholds();
    }
  }

  selectedResourceRemove(title, state, event, type) {
    //console.log("event",event)
    let custVal: any = 10;
    if (type === 'delete') {
      custVal = 80;
    }
    this.custEvent = event;
    this.dynamicLeft(this.custEvent, custVal);
    this.deleteResource = title;
    this.selectedResourceDelet = state;
    this.selectedResourcIsOwnerOption = false;
  }
  dynamicLeft(event, val) {
    const targetElementTarget = event.target;
    const targetElement = targetElementTarget.getClientRects()[0];
    const resourceRef: any = document.getElementsByClassName(
      'resource-card-wrapper'
    );
    const getResourceRef = resourceRef[0].getClientRects()[0];
    const selectedResourceLeft =
      getResourceRef.top -
      targetElement.top -
      val -
      this.resourceCardWrapper.scrollLeft;
    const documentElement = document.documentElement;
    documentElement.style.setProperty(
      `--${'resourceLeft'}`,
      `${(selectedResourceLeft+25) + 'px'}`
    );
  }
  selectedResourceUpdate(event, resource) {
    //console.log("resource-->",resource)
    this.selectedResourcIsOwnerOption = false;

    const customData = {
      title: resource.title,
      grade: this.gradeSubjectvalue,
      chapter: this.topicChaptervalue,
      file: resource.fileName,
      share: resource.isShared,
      type: 'resource-playlist',
      assetId: resource.resourceId,
      customResource: resource
    };
    //console.log("customData-->",customData)

    this.fileUploadService.setAddResourceFormData(customData);
    this.fileUploadService.setAddResourceFlagBroadcaster('create');
    this.handleResourceCloseClick(event, resource);
  }

  drop(event: CdkDragDrop<string[]>) {
    //console.log('CdkDragDropContainer', event);

    event.container.data = this.myResource;
    //console.log("--drop--this.myResource--",this.myResource)
    //console.log("--drop container.data--",event.container.data)
    //console.log("--event--",event)

    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
    //console.log("BEFORE REARRANGE this.resources -- ", this.resources)
    this.reArrangResource();
  }

  /// modified feb 26th

  reArrangResource() {
    //console.log('REARRANGE resources', this.resources);
    this.newResourceList = {};
    let newCollectionAssets: any = [];
    let newCollectionPractice: any;
    let newCollectionGallery: any;
    let currentResourceId = this.currentTopicId;
    let newCreationDate = Math.round(new Date().getTime() / 1000);
    let playlistJson = {
      creationDate: newCreationDate,
      asset: newCollectionAssets,
      gallery: newCollectionGallery,
      practice: newCollectionPractice
    };
    let tempResource = this.resources;
    this.resources = this.myResource;

    for (let index = 0; index < this.resources.length; index++) {
      if (this.resources[index].tcetype === 'quiz') {
        newCollectionAssets.push({ assets_type: 'practice' });
        newCollectionPractice = this.resources[index].metaData.questionIds;
        playlistJson.practice = newCollectionPractice;
      }
      if (this.resources[index].tcetype === 'gallery') {
        newCollectionAssets.push({ assets_type: 'gallery' });
        newCollectionGallery = this.resources[index].metaData;
        playlistJson.gallery = newCollectionGallery;
      }
      if (
        this.resources[index].tcetype !== 'quiz' &&
        this.resources[index].tcetype !== 'gallery'
      ) {
        if (!currentResourceId) {
          currentResourceId = this.resources[index].tpId;
        }
        //console.log(this.resources[index].metaData);
        delete this.resources[index].metaData.answerKeyResource;
        newCollectionAssets.push(this.resources[index].metaData);
      }
    }
    if (playlistJson) {
      let string = JSON.stringify(playlistJson, function(k, v) {
        if (v !== undefined) {
          return v;
        }
      });
      for (let i = 0; i < tempResource.length; i++) {
        if (tempResource[i].filterStatus === false) {
          this.resources.push(tempResource[i]);
        }
      }
      this.newResourceList = {
        id: currentResourceId,
        playlistJson: string,
        currentResource: this.resources,
        type: 'resourcePlaylist'
      };

      //console.log("Temp resources",tempResource);
      //console.log("New Final Resource",this.newResourceList);
      this.commonService.setupdateResourcelist(this.newResourceList);
      this.commonService.setTempResourceList('TEMP');
      // this.curriculumPlaylistService.UpdateResourceList(newResourceList);

      // this.currentTpoicResource.resources = this.resources;
      // this.curriculumPlaylistService.setTopicSelection(
      //   this.currentTpoicResource
      // );
    }

    //this.curriculumPlaylistService.setAvailableResources(this.resources);
  }

  /*reArrangResource() {
    this.newResourceList = {};
    let newCollectionAssets: any = [];
    let newCollectionPractice: any;
    let newCollectionGallery: any;
    let currentResourceId = this.currentTopicId;
    let newCreationDate = Math.round(new Date().getTime() / 1000);
    let playlistJson = {
      creationDate: newCreationDate,
      asset: newCollectionAssets,
      gallery: newCollectionGallery,
      practice: newCollectionPractice
    };

    for (let index = 0; index < this.resources.length; index++) {
      if (this.resources[index].tcetype === 'quiz') {
        newCollectionAssets.push({ assets_type: 'practice' });
        newCollectionPractice = this.resources[index].metaData.questionIds;
        playlistJson.practice = newCollectionPractice;
      }
      if (this.resources[index].tcetype === 'gallery') {
        newCollectionAssets.push({ assets_type: 'gallery' });
        newCollectionGallery = this.resources[index].metaData;
        playlistJson.gallery = newCollectionGallery;
      }
      if (
        this.resources[index].tcetype !== 'quiz' &&
        this.resources[index].tcetype !== 'gallery'
      ) {
        if (!currentResourceId) {
          currentResourceId = this.resources[index].tpId;
        }
        console.log(this.resources[index].metaData);
        delete this.resources[index].metaData.answerKeyResource;
        newCollectionAssets.push(this.resources[index].metaData);
      }
    }
    if (playlistJson) {
      
      let string = JSON.stringify(playlistJson, function(k, v) {
        if (v !== undefined) {
          return v;
        }
      });
      this.newResourceList = {
        id: currentResourceId,
        playlistJson: string,
        currentResource: this.resources,
        type: 'resourcePlaylist'
      };
      this.commonService.setupdateResourcelist(this.newResourceList);

      // this.currentTpoicResource.resources = this.resources;
      // this.curriculumPlaylistService.setTopicSelection(
      //   this.currentTpoicResource
      // );
    }

    //this.curriculumPlaylistService.setAvailableResources(this.resources);
  }*/

  isAllowed = (drag?: CdkDrag, drop?: CdkDropList) => {
    return false;
  };

  removeResource(event, id) {
    //console.log("removeResource",id)
    //console.log("resources-->",this.resources)
    for (let index = 0; index < this.resources.length; index++) {
      if (this.resources[index].resourceId === id) {
        //console.log("id matched--")
        //this.resources[index].show = false;
        this.handleResourceCloseClick(event, this.resources[index]);
        this.deleteResourceList.push(this.resources[index]);
        this.resources.splice(index, 1);
        this.curriculumPlaylistService.setAvailableResources(this.resources);
      }
    }

    this.reArrangResource();
  }

  selectedDeleteResource(event, resource) {
    //console.log(resource);
    this.selectedResourceDelet = false;

    for (let index = 0; index < this.resources.length; index++) {
      if (this.resources[index].resourceId == resource.resourceId) {
        console.log("--IN--",this.resources[index].resourceId);
        
        this.resources[index].visibility = 0;
        this.handleResourceCloseClick(event, this.resources[index]);
        
        //UNCOMMENTED BELOW splice for QUIZ delete THIRU/SATYAJIT
        this.resources.splice(index, 1);
        

        this.curriculumPlaylistService.setAvailableResources(this.resources);
        // this.reArrangResource()
        //  this.currentTpoicResource.resources = this.resources;
        //   this.curriculumPlaylistService.setTopicSelection(
        //     this.currentTpoicResource
        //   );
      }
    }
    const customData = {
      tpId: resource.tpId,
      gradeId: this.currentFullSubject.gradeLevel.id,
      subjectId: this.currentFullSubject.subject.subjectId,
      chapterId: this.currentFullChapter.chapter.chapterId,
      assetId: resource.resourceId,
      title: resource.title
    };

    //console.log("datafor delete->",customData,this.resources)
    this.fileUploadService.deleteResource(customData, this.resources);
  }
  setSelectedResourceIsOwnerOption(id, flag, event, resource) {
    this.selectedResourceDelet = false;
    this.dynamicLeft(event, -20);
    this.deleteResource = id;
    this.selectedResourcIsOwnerOption = flag;
  }

  ngOnDestroy() {
    this.resourceListSubscription.unsubscribe();
  }
}
