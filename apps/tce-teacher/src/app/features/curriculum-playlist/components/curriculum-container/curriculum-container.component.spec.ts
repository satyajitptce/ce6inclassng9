import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CurriculumContainerComponent } from './curriculum-container.component';

describe('ContainerComponent', () => {
  let component: CurriculumContainerComponent;
  let fixture: ComponentFixture<CurriculumContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CurriculumContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CurriculumContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
