import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Ebook, Chapter, Resource, CurriculumPlaylistService } from '@tce/core';
import { TCEAnimations } from '@tce/core';

@Component({
  selector: 'tce-curriculum-container',
  templateUrl: './curriculum-container.component.html',
  styleUrls: ['./curriculum-container.component.scss'],
  animations: [TCEAnimations.resourceDisplay]
})
export class CurriculumContainerComponent implements OnInit {
  currentEbooks: Ebook[];
  currentResources: Resource[];
  currentChapters: Chapter[];
  resourceChange = false;
  isLoading = false;
  errorMessage = '';

  constructor(private curriculumPlaylistService: CurriculumPlaylistService) {}

  ngOnInit() {
    this.curriculumPlaylistService.availableEbooks$.subscribe(
      (ebooks: Ebook[]) => {
        
        this.currentEbooks = ebooks;
      }
    );

    this.curriculumPlaylistService.ebookSelection$.subscribe((ebook: Ebook) => {
      this.currentChapters = ebook.chapters;
    });

    this.curriculumPlaylistService.availableChapters$.subscribe(chapters => {
      this.currentChapters = chapters;
    });

    this.curriculumPlaylistService.availableResources$.subscribe(
      (resources: Resource[]) => {
        //console.log("CurriculumContainerComponent -> ngOnInit -> resources", resources)
        let custResource = [];
        //console.log("resources",resources)
        if (resources && resources.length > 0) {
          resources.forEach(element => {
            if (element.visibility) {
              custResource.push(element);
            }
          });
          this.currentResources = custResource;
          //console.log("CurriculumContainerComponent -> ngOnInit -> this.currentResources", this.currentResources)
        } else {
          this.currentResources = custResource;
        }
      }
    );

    this.curriculumPlaylistService.ebookFetching$.subscribe(fetchState => {
      this.isLoading = fetchState;
    });

    this.curriculumPlaylistService.ebookFetchError$.subscribe(errorMessage => {
      this.errorMessage = errorMessage;
    });

    this.curriculumPlaylistService.topicSelection$.subscribe(currentTopic => {
      if (currentTopic) {
        this.resourceChange = !this.resourceChange;
      }
    });
  }
}
