import {
  Component,
  OnInit,
  Input,
  HostListener,
  ElementRef
} from '@angular/core';
import {
  Chapter,
  Topic,
  CurriculumPlaylistService,
  AppConfigService,
  CommonService,
  FileUploadService
} from '@tce/core';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
@Component({
  selector: 'tce-contents-selector',
  templateUrl: './contents-selector.component.html',
  styleUrls: ['./contents-selector.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class ContentsSelectorComponent implements OnInit {
  @Input() chapters: Chapter[];

  selectedChapter: Chapter;
  selectedTopic: Topic;
  selectedChapterIndex = 0;
  selectedTopicIndex = 0;
  animationFlag: any;
  displayContentFlyout = false;

  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private eRef: ElementRef,
    private appConfigService: AppConfigService,
    private commonService: CommonService,
    private fileUploadService: FileUploadService
  ) {}

  ngOnInit() {
    this.commonService.addResourcePanel$.subscribe(state=>{
      if(!state){
        this.fileUploadService.setAddResourceFlagBroadcaster('');

      }

      else{
        this.fileUploadService.setAddResourceFlagBroadcaster('create');
      }

    })
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      fullContentSelection => {
        if (fullContentSelection) {
          this.displayContentFlyout = false;
          this.selectedChapter = fullContentSelection.chapter;
          this.selectedTopic = fullContentSelection.topic;

          this.selectedChapterIndex = this.chapters.findIndex(
            c => c.chapterId === fullContentSelection.chapter.chapterId
          );
          this.selectedTopicIndex = this.selectedChapter.topics.findIndex(
            t => t.topicId === fullContentSelection.topic.topicId
          );
        }
      }
    );
  }

  goToNextChapterOrTopic(event: Event) {
    this.commonService.addResourcePanel$.subscribe(state=>{
        if(!state){
          this.fileUploadService.setAddResourceFlagBroadcaster('');
  
        }
  
        else{
          this.fileUploadService.setAddResourceFlagBroadcaster('create');
        }
  
      })

    event.stopPropagation();
    //console.log('this.selectedTopicIndex',this.selectedChapter.topics);
    if (this.selectedChapter.topics[this.selectedTopicIndex + 1]) {
      this.curriculumPlaylistService.setTopicSelection(
        this.selectedChapter.topics[this.selectedTopicIndex + 1]
      );
    } else if (this.chapters[this.selectedChapterIndex + 1]) {
      this.commonService.setAddResourcePanel(false)
      this.curriculumPlaylistService.setChapterSelection(
        this.chapters[this.selectedChapterIndex + 1],
        this.chapters[this.selectedChapterIndex + 1].topics[0]
      );
    }
  }

  goToPreviousChapterOrTopic(event: Event) {
    this.commonService.addResourcePanel$.subscribe(state=>{
      if(!state){
        this.fileUploadService.setAddResourceFlagBroadcaster('');

      }

      else{
        this.fileUploadService.setAddResourceFlagBroadcaster('create');
      }

    })
    event.stopPropagation();

    if (this.selectedChapter.topics[this.selectedTopicIndex - 1]) {
      
      this.curriculumPlaylistService.setTopicSelection(
        this.selectedChapter.topics[this.selectedTopicIndex - 1]
      );
    } else if (this.chapters[this.selectedChapterIndex - 1]) {
      this.commonService.setAddResourcePanel(false)
      this.curriculumPlaylistService.setChapterSelection(
        this.chapters[this.selectedChapterIndex - 1],
        this.chapters[this.selectedChapterIndex - 1].topics[
          this.chapters[this.selectedChapterIndex - 1].topics.length - 1
        ]
      );
    }
  }

  toggleContentFlyout() {
    this.displayContentFlyout = !this.displayContentFlyout;
    if(this.displayContentFlyout){
      this.commonService.setLessonPlayerMode(false);
    }
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event: Event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.displayContentFlyout = false;
    }
  }
}
