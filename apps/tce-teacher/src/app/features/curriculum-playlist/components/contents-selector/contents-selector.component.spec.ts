import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentsSelectorComponent } from './contents-selector.component';

describe('ContentsSelectorComponent', () => {
  let component: ContentsSelectorComponent;
  let fixture: ComponentFixture<ContentsSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContentsSelectorComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentsSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
