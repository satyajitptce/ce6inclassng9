import { async, TestBed } from '@angular/core/testing';
import { CurriculumPlaylistModule } from './curriculum-playlist.module';

describe('CurriculumPlaylistModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [CurriculumPlaylistModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(CurriculumPlaylistModule).toBeDefined();
  });
});
