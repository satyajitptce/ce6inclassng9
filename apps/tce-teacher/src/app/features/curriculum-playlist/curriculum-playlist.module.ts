import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { CurriculumContainerComponent } from './components/curriculum-container/curriculum-container.component';
import { EbookSelectorComponent } from './components/ebook-selector/ebook-selector.component';
import { ContentsSelectorComponent } from './components/contents-selector/contents-selector.component';
import { ResourcePlaylistComponent } from './components/resource-playlist/resource-playlist.component';
import { CurriculumLessonSelectorModule } from '../curriculum-lesson-selector/curriculum-lesson-selector.module';
import { ResourceFilterPanelComponent } from './components/resource-playlist/resource-filter-panel/resource-filter-panel.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommonSelectorModule } from '../common-selector/common-selector.module';
@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    CurriculumLessonSelectorModule,
    DragDropModule,
    CommonSelectorModule,
    LibConfigModule.forChild(CurriculumContainerComponent)
  ],
  declarations: [
    CurriculumContainerComponent,
    EbookSelectorComponent,
    ContentsSelectorComponent,
    ResourcePlaylistComponent,
    ResourceFilterPanelComponent
  ],
  exports: [CurriculumContainerComponent]
})
export class CurriculumPlaylistModule {}
