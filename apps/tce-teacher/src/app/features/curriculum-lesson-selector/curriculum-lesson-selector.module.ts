import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LessonSelectionContainerComponent } from './components/contents-selection-container/lesson-selection-container.component';
import { CoreModule } from '@tce/core';
import { ChapterTopicSelectionComponent } from './components/chapter-topic-selection/chapter-topic-selection.component';
import { GhostChapterTopicSelectionComponent } from './components/chapter-topic-selection/ghost-chapter-topic-selection/ghost-chapter-topic-selection.component';
import { ChapterSearchSelectionComponent } from './components/chapter-search-selection/chapter-search-selection.component';

@NgModule({
  declarations: [
    LessonSelectionContainerComponent,
    ChapterTopicSelectionComponent,
    GhostChapterTopicSelectionComponent,
    ChapterSearchSelectionComponent
  ],
  imports: [CommonModule, CoreModule],
  exports: [LessonSelectionContainerComponent]
})
export class CurriculumLessonSelectorModule {}
