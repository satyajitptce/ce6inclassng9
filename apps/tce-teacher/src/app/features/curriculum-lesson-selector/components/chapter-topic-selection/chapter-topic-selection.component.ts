import { Component, EventEmitter, OnInit, Input, Output } from '@angular/core';
import {
  TopicStatus,
  Chapter,
  Topic,
  RecentViewState,
  FullClassSelection,
  CurriculumPlaylistService,
  SubjectSelectorService,
  CommonService,
  FileUploadService
} from '@tce/core';
import { LessonPlanService } from '@app-teacher/features/lesson-plan/services/lesson-plan.service';

@Component({
  selector: 'tce-chapter-topic-selection',
  templateUrl: './chapter-topic-selection.component.html',
  styleUrls: ['./chapter-topic-selection.component.scss']
})
export class ChapterTopicSelectionComponent implements OnInit {
  availableChapters: Chapter[];
  @Output() lessonPlanClick = new EventEmitter();

  selectedChapter: Chapter;
  availableTopics: Topic[];
  selectedTopic: Topic;
  fullClassSelection: FullClassSelection;
  showGhost: Boolean = true;

  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private lessonPlanService: LessonPlanService,
    private subjectSelectorService: SubjectSelectorService,
    private commonService:CommonService,
    private fileUploadService:FileUploadService
  ) {}

  ngOnInit() {
    this.curriculumPlaylistService.availableChapters$.subscribe(
      (chapters: Chapter[]) => {
        this.availableChapters = chapters;
        if (chapters.length) {
          chapters[0].topics.sort(function(a: any, b: any) {
            return a.sequence - b.sequence;
          });
        }
        this.availableTopics = chapters.length && chapters[0].topics;
      }
    );

    this.curriculumPlaylistService.chapterSelection$.subscribe(newChapter => {
      if (newChapter) {
        this.selectedChapter = newChapter;
        this.availableTopics = newChapter.topics;
      } else {
        this.selectChapter = null;
        this.availableTopics = null;
      }
    });
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (fullClass: FullClassSelection) => {
        if (fullClass) {
          this.showGhost = false;
          this.fullClassSelection = fullClass;
        }
      }
    );
    this.curriculumPlaylistService.topicSelection$.subscribe(newTopic => {
      this.selectedTopic = newTopic;
    });
  }

  selectChapter(chapter: Chapter) {
    this.commonService.setAddResourcePanel(false)
    this.curriculumPlaylistService.setChapterSelection(
      chapter,
      chapter.topics[0]
    );
  }

  selectTopic(topic: Topic) {
    this.curriculumPlaylistService.setTopicSelection(topic,'chp selcetion');
    this.commonService.addResourcePanel$.subscribe(state=>{
      // console.log("thiru-content -->",state);
      if(!state){
        this.fileUploadService.setAddResourceFlagBroadcaster('');

      }

      else{
        this.fileUploadService.setAddResourceFlagBroadcaster('create');
      }

    })
  }

  get topicStatus() {
    return TopicStatus;
  }

  public loadLessonPlanPlayer(event: Event, topic: Topic) {
    // added stopPropagation to keep the window from autoclosing after the first render
    event.stopPropagation();
    this.lessonPlanService.setLessonPlanFromTopic(
      topic.topicId,
      topic.chapterName,
      topic.topicTitle
    );
    this.lessonPlanClick.emit(event);
  }
}
