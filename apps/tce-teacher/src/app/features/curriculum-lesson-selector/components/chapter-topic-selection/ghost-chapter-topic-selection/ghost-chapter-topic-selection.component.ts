import { Component, OnInit, Input } from '@angular/core';
import { Chapter, Topic } from '@tce/core';

@Component({
  selector: 'tce-ghost-chapter-topic-selection',
  templateUrl: './ghost-chapter-topic-selection.component.html',
  styleUrls: ['../chapter-topic-selection.component.scss']
})
export class GhostChapterTopicSelectionComponent implements OnInit {
  allFields = new Array(1);
  constructor() {}

  ngOnInit() {}
}
