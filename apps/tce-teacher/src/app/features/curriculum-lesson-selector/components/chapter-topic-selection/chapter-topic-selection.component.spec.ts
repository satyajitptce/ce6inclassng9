import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterTopicSelectionComponent } from './chapter-topic-selection.component';

describe('ChapterTopicSelectionComponent', () => {
  let component: ChapterTopicSelectionComponent;
  let fixture: ComponentFixture<ChapterTopicSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChapterTopicSelectionComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterTopicSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
