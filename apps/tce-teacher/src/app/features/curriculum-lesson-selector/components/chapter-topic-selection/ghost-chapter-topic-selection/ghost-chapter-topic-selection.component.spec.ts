/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GhostChapterTopicSelectionComponent } from './ghost-chapter-topic-selection.component';

describe('GhostChapterTopicSelectionComponent', () => {
  let component: GhostChapterTopicSelectionComponent;
  let fixture: ComponentFixture<GhostChapterTopicSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GhostChapterTopicSelectionComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhostChapterTopicSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
