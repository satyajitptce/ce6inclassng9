import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LessonSelectionContainerComponent } from './lesson-selection-container.component';

describe('ContentsSelectionContainerComponent', () => {
  let component: LessonSelectionContainerComponent;
  let fixture: ComponentFixture<LessonSelectionContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LessonSelectionContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LessonSelectionContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
