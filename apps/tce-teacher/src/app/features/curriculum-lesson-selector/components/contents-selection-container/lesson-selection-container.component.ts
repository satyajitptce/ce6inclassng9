import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Chapter } from '@tce/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'tce-lesson-selection-container',
  templateUrl: './lesson-selection-container.component.html',
  styleUrls: ['./lesson-selection-container.component.scss']
})
export class LessonSelectionContainerComponent implements OnInit {
  @Output() lessonPlanClick = new EventEmitter();
  public searchText: string;
  public itemSelected = new Subject<void>();

  constructor() {}

  ngOnInit() {}

  public setSearchText(value: string) {
    this.searchText = value;
  }

  public searchItemSelected() {
    this.itemSelected.next();
  }

  onLessonPlanClick() {
    this.lessonPlanClick.emit();
  }
}
