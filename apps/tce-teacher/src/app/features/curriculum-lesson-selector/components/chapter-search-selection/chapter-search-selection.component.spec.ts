import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChapterSearchSelectionComponent } from './chapter-search-selection.component';

describe('ChapterTopicSelectionComponent', () => {
  let component: ChapterSearchSelectionComponent;
  let fixture: ComponentFixture<ChapterSearchSelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ChapterSearchSelectionComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChapterSearchSelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
