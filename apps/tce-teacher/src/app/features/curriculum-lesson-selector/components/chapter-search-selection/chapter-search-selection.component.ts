import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Chapter, Ebook, CurriculumPlaylistService } from '@tce/core';

@Component({
  selector: 'tce-chapter-search-selection',
  templateUrl: './chapter-search-selection.component.html',
  styleUrls: ['./chapter-search-selection.component.scss']
})
export class ChapterSearchSelectionComponent implements OnInit {
  availableChapters: Chapter[];

  private availableOptions = {
    chapters: [],
    topics: []
  };
  public filteredOptions = {
    chapters: [],
    topics: []
  };

  @Input() set searchText(value: string) {
    const searchText = !value ? '' : value;
    if (searchText.trim() !== '') {
      const getFilteredData = this.searchForChapter(searchText.trim());
      this.filteredOptions.chapters = getFilteredData.filteredChapter.length
        ? getFilteredData.filteredChapter[0]
        : [];
      this.filteredOptions.topics = getFilteredData.filteredTopic.length
        ? getFilteredData.filteredTopic[0]
        : [];
    } else {
      this.filteredOptions.chapters = this.availableOptions.chapters;
      this.filteredOptions.topics = this.availableOptions.topics;
    }
  }

  @Output() itemSelected = new EventEmitter<boolean>();

  constructor(private curriculumPlaylistService: CurriculumPlaylistService) {}

  ngOnInit() {
    this.curriculumPlaylistService.availableChapters$.subscribe(
      (chapters: Chapter[]) => {
        this.availableChapters = chapters;
        this.processAvailableOptions();
      }
    );
  }

  processAvailableOptions() {
    const newAvailableOptions = {
      chapters: [],
      topics: []
    };
    this.availableChapters.forEach(chapter => {
      newAvailableOptions.chapters.push({
        id: chapter.chapterId,
        chapterId: chapter.chapterId,
        title: chapter.chapterTitle,
        display: `${chapter.chapterTitle}`,
        header: 'CHAPTER',
        item: chapter,
        chapter: chapter
      });
      chapter.topics.forEach(topic => {
        newAvailableOptions.topics.push({
          id: topic.topicId,
          chapterId: chapter.chapterId,
          title: topic.topicTitle,
          display: `${chapter.chapterNumber}.${topic.topicNumber} - ${topic.topicTitle}`,
          header: 'TOPIC',
          item: topic,
          chapter: chapter
        });
      });
    });
    this.filteredOptions = {
      chapters: [],
      topics: []
    };
    this.availableOptions = newAvailableOptions;
  }

  public setSelection(event: Event, option: any) {
    event.stopPropagation();

    const item = option.item;
    if (item instanceof Chapter) {
      this.curriculumPlaylistService.setChapterSelection(item, item.topics[0]);
    } else {
      this.curriculumPlaylistService.setChapterSelection(option.chapter, item);
    }
    this.itemSelected.emit();
  }

  private searchForChapter(searchText) {
    let filteredData = { filteredChapter: [], filteredTopic: [] };
    searchText = searchText.split(' ');
    searchText.map(toBeSearched => {
      const getFilterChapterData = this.availableOptions.chapters.filter(
        option =>
          option.title.toLowerCase().search(toBeSearched.toLowerCase()) > -1
      );

      if (getFilterChapterData.length > 0) {
        let present = false;
        getFilterChapterData.map(data => {
          if (filteredData.filteredChapter.includes(data)) {
            present = true;
          }
        });
        if (!present) filteredData.filteredChapter.push(getFilterChapterData);
      }

      const getFilterTopicData = this.availableOptions.topics.filter(
        option =>
          option.title.toLowerCase().search(toBeSearched.toLowerCase()) > -1
      );

      if (getFilterTopicData.length > 0) {
        let present = false;
        getFilterTopicData.map(data => {
          if (filteredData.filteredTopic.includes(data)) {
            present = true;
          }
        });
        if (!present) filteredData.filteredTopic.push(getFilterTopicData);
      }
    });

    return {
      filteredChapter: filteredData.filteredChapter,
      filteredTopic: filteredData.filteredTopic
    };
  }
}
