import { Component, OnInit } from '@angular/core';
import { CommonService } from '@tce/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'tce-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {
  url: SafeResourceUrl;
  constructor(
    private commonService: CommonService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit() {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl(
      'http://172.18.1.57:8080/adminedge/'
    );
  }

  closeAdmin() {
    this.commonService.setUserAdmin('');
  }
}
