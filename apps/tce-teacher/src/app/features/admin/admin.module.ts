import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminHomeComponent } from './admin-home/admin-home.component';
//import { AdminRoutingModule } from './admin-routing.module';
import { LibConfigModule } from '@tce/lib-config';

@NgModule({
  declarations: [AdminHomeComponent],
  imports: [CommonModule, LibConfigModule.forChild(AdminHomeComponent)]
})
export class AdminModule {}
