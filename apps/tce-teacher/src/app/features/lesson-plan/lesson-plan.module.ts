import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@tce/core';
import { LessonPlanComponent } from './lesson-plan.component';
import { LibConfigModule } from '@tce/lib-config';
import { TooltipDirective } from './tooltip.directive';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';
import { AngularSvgIconModule } from 'angular-svg-icon';

@NgModule({
  declarations: [LessonPlanComponent, TooltipDirective],
  imports: [
    CommonModule,
    CoreModule,
    PlayerAnnotationModule,
    LibConfigModule.forChild(LessonPlanComponent),
    AngularSvgIconModule
  ],
  exports: [LessonPlanComponent]
})
export class LessonPlanModule {}
