import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild
} from '@angular/core';
import { LessonPlanService } from './services/lesson-plan.service';
import { LessonPlan } from './interfaces/lesson-plan.interface';
import { SafeHtml } from '@angular/platform-browser';
import { CommonService, ToolType } from '@tce/core';

@Component({
  selector: 'tce-lesson-plan',
  templateUrl: './lesson-plan.component.html',
  styleUrls: ['./lesson-plan.component.scss']
})
export class LessonPlanComponent implements OnInit, AfterViewInit {
  public lessonChapter: string;
  public lessonTopic: string;
  public content: SafeHtml;
  public isOpen = false;
  public currentSelectedTool: ToolType;
  public tooltip: boolean;

  constructor(public lessonPlanService: LessonPlanService, private commonService: CommonService) {}

  ngAfterViewInit() {
    //this.activities.nativeElement.remove();
  }

  ngOnInit() {
    this.commonService.closeLessonPlayer$.subscribe(value=>{
      if(!value){
        this.isOpen = false;
      }
    })

    // let domReference =  document.getElementById('lesson_correctcontent');
    // if(domReference !== null){
    //   domReference.innerHTML = '';
    //}
    this.lessonPlanService.currentLessonPlan$.subscribe(
      (lessonPlan: LessonPlan) => {
        //console.log('lessonPlan', lessonPlan);

        if (!lessonPlan) {
          this.closeLessonPlan();
        } else {
          this.lessonChapter = lessonPlan.chapterName;
          this.lessonTopic = lessonPlan.topicName;
          this.content = this.lessonPlanService.setContentHtml(
            lessonPlan.htmlContent
          );
          this.loadContent(this.content);

          //this.content = lessonPlan.htmlContent;
          //this.content = correctContent(lessonPlan.htmlContent);
          //this.content = this.stringToHTML(lessonPlan.htmlContent)
          //this.stringToHTML(lessonPlan.htmlContent)
          //console.log("document----->",lessonPlan.htmlContent)

          //console.log(this.content)
          //this.testConvert()

          this.isOpen = true;
        }
      }
    );
  }

  // testConvert(){
  //   let doc = new DOMParser().parseFromString(xmlString, "text/xml");
  //   console.log(doc.body.firstChild); // => <a href="#">Link...
  //   console.log(doc.body.firstChild.firstChild); // => Link
  // }

  // contentData(docContent){
  //   docContent.getElementsByTagName("P")[0].removeAttribute("activities");
  //   //list.childNodes;
  //   return docContent;
  // }

  stringToHTML = function(str) {
    let parser = new DOMParser();
    let doc = parser.parseFromString(str, 'text/html');
    // doc.getElementsByTagName("P")[0].removeAttribute("activities");
    // console.log("document",doc)

    return doc.body;
  };

  correctCommonContent(content) {
    //console.log(content.childNodes);
    content.childNodes.forEach(element => {
      // if (element.nodeName === 'UL' && element.innerText === 'Click the following Thumbnail to view the media file.') {
      //   element.classList.add('hide')
      // }
      if (element.nodeName === 'UL') {
        element.classList.add('unorderedlist');
        element.childNodes.forEach(element => {
          if (element.nodeName === 'LI') {
            element.childNodes.forEach(element => {
              if (element.nodeName === 'B') {
                if (
                  element.innerText === 'Assignments' ||
                  element.innerText === 'Worksheets & Handouts'
                ) {
                  element.classList.add('resources_inner_heading');
                }
              }
              if (element.nodeName === 'OL') {
                element.childNodes.forEach(element => {
                  if (element.nodeName === 'LI') {
                    element.childNodes.forEach(element => {
                      if (element.nodeName === 'B') {
                        if (
                          element.innerText === 'Assignments' ||
                          element.innerText === 'Worksheets & Handouts'
                        ) {
                          element.classList.add('resources_inner_heading');
                        }
                      }
                    });
                  }
                });
              }
            });
          }
          if (
            element.nodeName === 'LI' &&
            element.innerText ===
              'Click the following Thumbnail to view the media file.'
          ) {
            element.remove();
          }
          if (element.nodeName === 'LI' && element.innerText === '') {
            element.remove();
          }
        });
      }

      if (element.nodeName === 'OL') {
        element.classList.add('orderedlist');
        element.childNodes.forEach(element => {
          if (element.nodeName === 'LI') {
            element.childNodes.forEach(element => {
              if (element.innerText === 'Assignments') {
                element.classList.add('resources_inner_heading');
              }
              if (element.innerText === 'Worksheets & Handouts') {
                element.classList.add('resources_inner_heading');
              }
            });
          }
        });
      }
      if (element.nodeName === 'ACTIVITY') {
        element.remove();
      }
      if (element.nodeName === 'H5') {
        element.classList.add('hide');
        //element.previousSibling.classList.add('hide')
      }
      if (element.nodeName === 'P' && element.className === 'activities') {
        element.remove();
      }
      if (element.nodeName === 'P' && element.childNodes.length < 1) {
        element.remove();
      }
      if (element.nodeName === 'BR') {
        element.remove();
      }
      if (element.nodeName === 'PRINT') {
        //console.log('print');
        element.classList.add('hide');
      }
      if (element.nodeName === 'H3' && element.innerText === 'Worksheets') {
        element.classList.add('resources_inner_heading');
      }
      // if (element.nodeName === 'B' && element.innerText === 'Media') {
      //   element.remove();
      // }
      if (element.nodeName === 'B') {
        if (element.innerText === 'Assignments') {
          element.classList.add('resources_inner_heading');
        }

        if (
          element.innerText === 'Worksheets & Handouts' ||
          element.innerText === 'Worksheets &Handouts'
        ) {
          element.classList.add('resources_inner_heading');
        }

        if (element.innerText === 'Media') {
          element.classList.add('resources_inner_heading');
        }
        if (element.childNodes.length > 1) {
          element.childNodes.forEach(element => {
            if (element.nodeName === 'ACTIVITY') {
              element.remove();
            }
          });
        }
      }

      if (element.nodeName === 'P') {
        if (element.innerText === 'Resources Available') {
          element.classList.add('resources');
        }
        if (element.innerHTML === '<b>​</b>') {
          element.remove();
        }

        if (
          element.outerText ===
            'Click the following Thumbnail to view the media file.Click the following Title to view the printable file.' ||
          element.innerText ===
            'Click the following Title to view the printable file.Click the following Thumbnail to view the media file.'
        ) {
          element.classList.add('hide');
        }

        if (
          element.innerText === 'Overview' &&
          element.classList.length === 0
        ) {
          element.classList.add('overview');
        }
        if (
          element.innerText === 'Coverage' &&
          element.classList.length === 0
        ) {
          element.classList.add('coverage');
        }
        if (element.childNodes.length > 0) {
          element.childNodes.forEach(element => {
            if (element.innerText === 'Media') {
              element.classList.add('resources_inner_heading');
            }
            if (element.nodeName === 'BR') {
              element.remove();
            }
            if (
              element.nodeName === 'STRONG' &&
              element.innerText === 'Media'
            ) {
              element.classList.add('resources_inner_heading');
            }
            if (
              element.nodeName === 'B' &&
              element.innerText === 'Assignments'
            ) {
              element.classList.add('resources_inner_heading');
            }
            if (
              element.nodeName === 'B' &&
              element.innerText === 'Worksheets & Handouts'
            ) {
              element.classList.add('resources_inner_heading');
            }
          });
        }

        //console.log(element.childNodes[0].innerText,"mm",element.childNodes.nodeName)
        // element.remove();
      }
    });

    //console.log('content------------>', content);

    return content;
  }

  public closeLessonPlan() {
    this.isOpen = false;
  }

  public triggerScroll(emittedObject) {
    if (
      emittedObject.eventName === 'mousedown' ||
      emittedObject.eventName === 'touchstart'
    ) {
      const scrollElement =
        emittedObject.cachedCurrentTarget.parentNode.parentNode.parentNode
          .parentNode.parentNode.parentNode.nextSibling;

      if (emittedObject.type === 'up') scrollElement.scrollTop -= 20;
      else scrollElement.scrollTop += 20;
    }
  }

  loadContent(content) {
    let finalContent = '';
    finalContent = this.correctCommonContent(content);
    setTimeout(() => {
      document.getElementById('lesson_correctcontent').innerHTML = '';
      document.getElementById('lesson_correctcontent').append(finalContent);
    }, 200);
  }
}
