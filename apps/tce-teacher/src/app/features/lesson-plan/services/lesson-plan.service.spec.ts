import { TestBed } from '@angular/core/testing';

import { LessonPlanService } from './lesson-plan.service';

describe('LessonPlanService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LessonPlanService = TestBed.get(LessonPlanService);
    expect(service).toBeTruthy();
  });
});
