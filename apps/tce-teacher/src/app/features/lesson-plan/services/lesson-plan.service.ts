import { Injectable, SecurityContext, Inject } from '@angular/core';
import { ReplaySubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

import { LessonPlan, ApiLessonPlan } from '../interfaces/lesson-plan.interface';
import {
  RequestApiService,
  CurriculumPlaylistService,
  SubjectSelectorService
} from '@tce/core';
import { PlayerContainerService } from '@app-teacher/services';


@Injectable({
  providedIn: 'root'
})
export class LessonPlanService {
  private currentLessonPlan = new Subject;
  public currentLessonPlan$ = this.currentLessonPlan.asObservable();


  private contentData = new Subject();
  public contentData$ = this.contentData.asObservable();

  


  private url = this.requestApiService.getUrl('getFile');
  private tpPath;

  selectedData = [];

  constructor(
    private http: HttpClient,
    private sanitizer: DomSanitizer,
    private requestApiService: RequestApiService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private subjectSelectorService: SubjectSelectorService,
    private playerContainerService: PlayerContainerService
  ) {
    this.curriculumPlaylistService.fullContentSelection$.subscribe(() => {
      //console.log("curriculumPlaylistService--1")
      this.clearCurrentLessonPlan();
    });
    this.subjectSelectorService.fullClassSelection$.subscribe(() => {
      //console.log("subjectSelectorService--2")
      this.clearCurrentLessonPlan();
    });
    this.playerContainerService.addOpenedResource$.subscribe(() => {
      //console.log("playerContainerService--3")
      this.clearCurrentLessonPlan();
    });
    this.playerContainerService.currentActiveResourceId$.subscribe(() => {
      //console.log("playerContainerService--4")
      this.clearCurrentLessonPlan();
    });
  }

  private clearCurrentLessonPlan() {
    this.currentLessonPlan.next(null);
  }

  public setLessonPlanFromTopic(
    tpId: string,
    chapterName: string,
    topicName: string
  ) {
    this.getLessonDetails(tpId, chapterName, topicName);
  }

  private getLessonDetails(
    tpId: string,
    chapterName: string,
    topicName: string
  ): void {
    const requestUrl = this.requestApiService
      .getUrl('lessonPlan')
      .replace('@tpid@', tpId);

    this.http
      .get<ApiLessonPlan>(requestUrl)
      .toPromise()
      .then(res => {
        this.currentLessonPlan.next(
          this.cleanLessonPlanResponse(res, chapterName, topicName)
        );
      })
      .catch(err => {
        console.error(err.message);
      });
  }

  setContentHtml(content){
   // console.log("Before",content)

    let finalContent;
    let finalResult;
    let result;
    const lpFileURL = `${this.url}/${this.tpPath}/`;

  
    content = content.replace(/&nbsp;/g," ");
    content = content.replace(/&ndash/g, "-");
    content = content.replace(/Activities/g,"");
    content = content.replace(/src="i/g,`src="${lpFileURL}i`)

    
    // result = content.substr(content.search("<p>Click"), content.length);
    // //finalContent = content.replace(result,"")
    // content = content.replace(result,"")


    // finalResult = content.substr(content.search('<p class="activities">'), content.length);
    // content = content.replace(finalResult,"")


    return this.correctCommonContent(content);

  }

  correctCommonContent(finalContent){

    let correctContent;
    let dv = '<div>';
    let finalDoc = dv.concat(finalContent.concat('</div>'));
    let doc = new DOMParser().parseFromString(finalDoc, "text/html");
    

  

    // if(doc.body.getElementsByClassName('p') === 'p'){

    // }

    

   // console.log("After",doc)
    
    //console.log("finalDoc-->",doc)
    return doc.body.firstElementChild

  }

  private cleanLessonPlanResponse(
    data: any,
    chapterName: string,
    topicName: string
  ): LessonPlan {
    this.tpPath = data.tpFilePath;
    const lpFileURL = `${this.url}/${data.tpFilePath}/`;

    const lessonPlanData = JSON.parse(data.tpJson);
    let htmlContent = '';
    if (lessonPlanData) {
      const sectionArray = lessonPlanData.sections.section;

      sectionArray.forEach((section: any) => {
        let sectionContent: any;
        if (section.visible && section.content.length > 0) {
          // let sectionContent = section.content.find((val)=>{val.hasOwnProperty('name')}) ? section.subsection.content[0].value.content : section.content;
          let checkSub;
          section.content.find(val => {
            checkSub = val.hasOwnProperty('name');
          });

          if (checkSub) {
            sectionContent = section.content[0].value.content;
          } else {
            sectionContent = section.content;
          }
          if (this.isArray(sectionContent)) {
            (<any[]>sectionContent).forEach(sectionArrayContent => {
              try {
                const contentValue = sectionArrayContent.value
                  ? sectionArrayContent.value
                  : sectionArrayContent;
                htmlContent += this.getReplacedContent(contentValue, lpFileURL);
              } catch (error) {}
            });
          } else {

            htmlContent += this.getReplacedContent(sectionContent, lpFileURL);
            //console.log("else sectionContent",htmlContent)

          }
        }
      });
    }

    const safeHTML = this.sanitizer.bypassSecurityTrustHtml(htmlContent);

    return new LessonPlan(chapterName, topicName, <SafeHtml>htmlContent);
  }

  private isArray(o: Object) {
    return Object.prototype.toString.call(o) === '[object Array]';
  }

  private getReplacedContent(content: string, fileURL: string) {
    let convertedContent = this.replaceString(content, /<br\s*\/?>/gi, '');
    convertedContent = this.replaceString(
      convertedContent,
      'src="',
      `src="${fileURL}`
    );
    return convertedContent;
  }

  private replaceString(content: string, replaceValue: any, replaceWith: any) {
    return content.replace(replaceValue, replaceWith);
  }

  public setSelectionData(data: any) {
    this.selectedData = new Array();
    this.selectedData.push(data);
  }

  public getSelectedData() {
    return this.selectedData;
  }
}
