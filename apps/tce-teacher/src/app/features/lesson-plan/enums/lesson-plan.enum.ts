export enum LessonPlanEnum {
  LO = 'LO',
  MT = 'MT',
  TP = 'TP',
  EX = 'EX',
  INTRO = 'INTRO',
  PT = 'PT',
  EMPTY = '',
  NOCONTENT = 'No Content Found'
}
