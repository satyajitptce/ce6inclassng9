import { SafeHtml } from '@angular/platform-browser';

export interface ApiLessonPlan {
  tpFilePath: string;
  tpJson: string;
}

export class LessonPlan {
  chapterName: string;
  topicName: string;
  htmlContent: SafeHtml;

  constructor(chapterName, topicName, htmlContent) {
    this.chapterName = chapterName;
    this.topicName = topicName;
    this.htmlContent = htmlContent;
  }
}
