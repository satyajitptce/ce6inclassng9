import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';
import * as rangy from 'rangy-updated';
import * as ClassApplier from 'rangy-updated/lib/rangy-classapplier';

@Directive({
  selector: '[tceTooltip]'
})
export class TooltipDirective {
  tooltip: HTMLElement;

  offset = 30;
  offsetLeft = 100;
  mouseXPosition: any;
  mouseYPosition: any;
  highlightApplier: any;
  toolTipTimer: ReturnType<typeof setTimeout>;

  constructor(private el: ElementRef, private renderer: Renderer2) {
    rangy.init();
    this.highlightApplier = ClassApplier.createClassApplier('highlight', {
      tagNames: ['span', 'img']
    });
  }

  @HostListener('mousedown', ['$event']) onMouseDown(event: any) {
    this.mouseXPosition = event.pageX;
    if (this.tooltip) {
      this.hide();
    }
  }

  @HostListener('mouseup', ['$event']) onMouseUp(event: any) {
    this.determineTooltipShow(event);
  }

  @HostListener('mouseleave', ['$event']) onMouseOut(event: MouseEvent) {
    if (
      (<HTMLElement>this.el.nativeElement).isSameNode(<HTMLElement>event.target)
    ) {
      this.determineTooltipShow(event);
    }
  }

  determineTooltipShow(event: any) {
    this.mouseYPosition = event.pageX;
    if (!this.tooltip && this.mouseXPosition !== this.mouseYPosition) {
      //if there is selected text
      if (rangy.getSelection().toString()) {
        this.show(event);
      }
    }
  }

  private show(event: any) {
    this.create();
    this.setPosition(event);
    this.renderer.addClass(this.tooltip, 'ng-tooltip-show');
  }

  private hide() {
    this.renderer.removeClass(this.tooltip, 'ng-tooltip-show');
    const appendElement = document.getElementsByClassName(
      'lesson-plan-content-container'
    )[0];
    this.renderer.removeChild(appendElement, this.tooltip);
    this.tooltip = null;
  }

  private create() {
    this.tooltip = this.renderer.createElement('span');
    this.renderer.appendChild(
      this.tooltip,
      this.renderer.createText('highlight')
    );

    const appendElement = document.getElementsByClassName(
      'lesson-plan-content-container'
    )[0];

    this.tooltip.addEventListener('mousedown', event => {
      //event.preventDefault();
      event.stopPropagation();
      this.highlightApplier.applyToSelection();
    });

    this.tooltip.addEventListener('click', event => {
      //event.preventDefault();
      event.stopPropagation();
      this.hide();
    });

    this.renderer.appendChild(appendElement, this.tooltip);
    this.renderer.addClass(this.tooltip, 'ng-tooltip');
    this.renderer.addClass(this.tooltip, `ng-tooltip-top`);
    this.renderer.setStyle(this.tooltip, 'transition', `opacity 100ms`);
  }

  private setPosition(event: MouseEvent) {
    const tooltipPos = this.tooltip.getBoundingClientRect();
    const scrollContainer = document.querySelector(
      '.lesson-plan-content-container'
    );
    let left =
      event.pageX -
      scrollContainer.getBoundingClientRect().left -
      tooltipPos.width / 2;
    if (left < 0) {
      left = 0;
    } else if (
      left + tooltipPos.width >
      scrollContainer.clientLeft + scrollContainer.clientWidth
    ) {
      left = left - tooltipPos.width / 2;
    }
    let top =
      event.pageY -
      scrollContainer.getBoundingClientRect().top -
      tooltipPos.height +
      scrollContainer.scrollTop;
    if (top < 0) {
      top = 0;
    }

    this.renderer.setStyle(this.tooltip, 'top', `${top}px`);
    this.renderer.setStyle(this.tooltip, 'left', `${left}px`);
  }
}
