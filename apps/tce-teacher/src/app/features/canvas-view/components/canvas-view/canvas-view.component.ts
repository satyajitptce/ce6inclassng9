import {
  AfterViewInit,
  Component,
  ComponentRef,
  Inject,
  OnInit,
  ViewChild,
  ViewContainerRef,
  HostListener,
  ElementRef,
  ChangeDetectorRef,
  Input,
  ComponentFactoryResolver,
  EventEmitter
} from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CalendarComponent } from '@app-teacher/features/calendar/component/calendar/calendar.component';
import { FTUEWelcomeComponent } from '@app-teacher/features/first-time-user-welcome/components/ftue-welcome/ftue-welcome.component';
import { IntroComponent } from '@app-teacher/features/intro/component/intro/intro.component';
import { LoginPwdComponent } from '@app-teacher/features/login/sign-in/components/login-pwd/login-pwd.component';
import { PlayerDrawerComponent } from '@app-teacher/features/player-drawer/components/player-drawer/player-drawer.component';
import {
  SignInModuleState,
  SignInState
} from '@app-teacher/models/enums/signInState.enum';
import {
  PlayerDrawerService,
  SessionTimeoutService,
  PlayerContainerService
} from '@app-teacher/services';
import {
  AppConfigService,
  AuthenticationService,
  AuthStateEnum,
  EnvironmentConfig,
  FirstTimeUserStatusEnum,
  GlobalConfigState,
  ThemeService,
  ToolbarService,
  UserState,
  WhiteboardService,
  FileUploadService,
  CurriculumPlaylistService,
  CommonService,
  UserProfileService,
  SubjectSelectorService,
  FullClassSelection,
  FilterMenu,
  ToolType
} from '@tce/core';
import { LibConfigService } from '@tce/lib-config';
import { WhiteboardComponent } from '@tce/whiteboard';
import { filter, takeUntil } from 'rxjs/operators';
import { CreateResourceComponent } from '@app-teacher/features/add-resource/component/create-resource/create-resource.component';
import { SearchViewComponent } from '../../../../../../../../libs/search/src/lib/component/search-view/search-view.component';
import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import { UserSettingViewComponent } from '@tce/user-profile';
import { ChangePasswordComponent } from '../../../user-profile-change-password/change-password/change-password.component';
import { ChangePinComponent } from '../../../user-profile-change-pin/change-pin/change-pin.component';
import { AdminHomeComponent } from '../../../admin/admin-home/admin-home.component';
import { PlanningModeViewComponent } from '@tce/planning-mode';
import { Router } from '@angular/router';
import { ApplicationMode } from '../../../../../../../../libs/core/src/lib/enums/app-mode.enum';
import { UserOptionsComponent } from 'libs/toolbar/src/lib/components/user-options/user-options.component';
import { from, Subject, Subscription } from 'rxjs';
import { stat, statSync } from 'fs';
import { WidgetsConfigService } from '@tce/player-widgets';

@Component({
  selector: 'tce-canvas-view',
  templateUrl: './canvas-view.component.html',
  styleUrls: ['./canvas-view.component.scss'],
  animations: [
    trigger('LogoutIcon', [
      state(
        'leftTop',
        style({
          bottom: '210px',
          transform: 'translateY(0)'
        })
      ),
      state(
        'leftBottom',
        style({
          bottom: '80px',
          transform: 'translateY(0)'
        })
      ),
      state(
        'rightTop',
        style({
          bottom: '170px',
          transform: 'translateY(0)'
        })
      ),
      state(
        'rightBottom',
        style({
          bottom: '40px',
          transform: 'translateY(0)'
        })
      ),
      transition('leftTop <=> leftBottom', animate(500)),
      transition('rightTop <=> rightBottom', animate(500))
    ])
  ]
})
export class CanvasViewComponent implements OnInit, AfterViewInit {
  @ViewChild('playerDrawerOutlet', { read: ViewContainerRef })
  playerDrawerLoader: ViewContainerRef | undefined;

  @ViewChild('worksheet', { static: true, read: ViewContainerRef })
  worksheet: ViewContainerRef | undefined;

  @ViewChild('canvasGeneralOutlet', { static: true, read: ViewContainerRef })
  canvasGeneralOutlet: ViewContainerRef | undefined;

  @ViewChild('ftueWelcomeOutlet', { read: ViewContainerRef })
  ftueWelcomeOutlet: ViewContainerRef | undefined;

  @ViewChild('loginOutlet', { read: ViewContainerRef })
  loginOutlet: ViewContainerRef | undefined;

  @ViewChild('addResource', { static: false, read: ViewContainerRef })
  addResource: ViewContainerRef | undefined;

  @ViewChild('searchResult', { static: false, read: ViewContainerRef })
  searchResult: ViewContainerRef | undefined;

  @ViewChild('userSettingView', { static: false, read: ViewContainerRef })
  userSettingView: ViewContainerRef | undefined;

  @ViewChild('changePassword', { static: false, read: ViewContainerRef })
  changePassword: ViewContainerRef | undefined;

  @ViewChild('changePin', { static: false, read: ViewContainerRef })
  changePin: ViewContainerRef | undefined;

  @ViewChild('userAdmin', { static: false, read: ViewContainerRef })
  userAdmin: ViewContainerRef | undefined;

  @ViewChild('planningView', { static: false, read: ViewContainerRef })
  planningView: ViewContainerRef | undefined;

  @ViewChild('widgetPlayer', { read: ViewContainerRef })
  widgetPlayer: ViewContainerRef;

  private destroy$: Subject<boolean> = new Subject<boolean>();

  playerDrawerComponentRef: ComponentRef<PlayerDrawerComponent>;
  playerDrawerComponentLoading = false;

  ftueComponentRef: ComponentRef<FTUEWelcomeComponent>;
  ftueComponentLoading = false;

  applicationMode = 'teaching';
  signInActive = false;
  signInActives = true;
  isLoggedIn = false;
  firstTimeUser = false;
  toolbarsetclass = false;
  drawerOpen = false;
  showIdle = false;
  authChecked = false;
  centerMessage = true;
  appVersion: string;
  welcomeBackMessage = true;
  showLogout = false;
  showRunner = true;
  IsAnimation: any;
  userStateValue: any;
  classRoomModeFlag: any;
  editFlag;
  inputPrefereView: any = [];
  userProfileData: any;
  currentSubject: any;
  userProfileImg: string;

  //timer
  FULL_DASH_ARRAY = 283;
  signoutCancelMessage = false;
  TIME_LIMIT = 10;
  WARNING_THRESHOLD = this.TIME_LIMIT / 2;
  ALERT_THRESHOLD = this.TIME_LIMIT / 3;
  timePassed = 0;
  timeLeft = this.TIME_LIMIT;
  timerInterval = null;
  @Input() resources;

  COLOR_CODES = {
    info: {
      color: 'green'
    },
    warning: {
      color: 'orange',
      threshold: this.WARNING_THRESHOLD
    },
    alert: {
      color: 'red',
      threshold: this.ALERT_THRESHOLD
    }
  };
  remainingPathColor = this.COLOR_CODES.info.color;

  //timer--end

  private canvasSubsribtion =  new Subscription();

  constructor(
    protected appConfigService: AppConfigService,
    private libConfigService: LibConfigService,
    private themeService: ThemeService,
    private authService: AuthenticationService,
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService,
    private sessionTimeoutService: SessionTimeoutService,
    private whiteboardService: WhiteboardService,
    private playerContainerService: PlayerContainerService,
    private fileUploadService: FileUploadService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private eRef: ElementRef,
    private commonService: CommonService,
    private changeDetector: ChangeDetectorRef,
    private userProfileService: UserProfileService,
    private subjectSelectorService: SubjectSelectorService,
    private toastrService: ToastrService,
    private router: Router,
    private widgetsConfigService: WidgetsConfigService,
    private componentFactoryResolver: ComponentFactoryResolver,

    @Inject('environment') private environment: EnvironmentConfig
  ) {}

  postAuthCheck(userState: UserState) {
    const { authStatus, previousAuthStatus, firstTimeUserStatus } = userState;
    //console.log('TCE: CanvasViewComponent -> postAuthCheck -> authStatus',  authStatus );

    if (authStatus !== AuthStateEnum.AUTH) {
      this.loadSignIn();
    }

    this.authChecked = true;
    this.firstTimeUser =
      firstTimeUserStatus === FirstTimeUserStatusEnum.FIRST_TIME;

    if (
      this.appConfigService.getGlobalSettingConfig('modulelogin') ===
      GlobalConfigState.ENABLED
    ) {
      this.isLoggedIn = authStatus === AuthStateEnum.AUTH;
      //console.log('cnvas check------------------?', this.isLoggedIn);
      if (
        this.commonService.applicationMode === ApplicationMode.PLANNING &&
        this.isLoggedIn
      ) {
        //console.log('hello');

        this.commonService.setClassroomModeState(false);
      }
    } else {
      this.isLoggedIn = true;
      //console.log('cnvaselse------------------?', this.isLoggedIn);
    }
    if (!this.isLoggedIn) {
      this.showIdle = false;
      if (this.playerDrawerComponentRef) {
        this.playerDrawerComponentRef.destroy();
        this.playerDrawerComponentRef = null;
      }
    }

    if ((!this.firstTimeUser || !this.isLoggedIn) && this.ftueComponentRef) {
      this.ftueComponentRef.destroy();
      this.ftueComponentRef = null;
    }

    this.loadFTUEWelcome();

    //Load the player drawer on change of user state. The method checks to see if it's already been loaded.
    this.loadPlayerDrawer();

    if (this.isLoggedIn) {
      //console.log('---@ canvas-view is logged in ----->> ');

      // this.userProfileService.userProfileAPIData$.subscribe((data: any) => {
      //   //this.userProfileData = data;
      //   console.log('data @ canvas-view ----->> ', this.userProfileData);
      //   this.resources = JSON.parse(data.userSettings.addInfo1);
      // });

      this.userProfileService.getUserProfileAPIData();
      //this.resources = this.userProfileService.prefereData;
      //console.log("resources canvas",this.resources)

      this.userProfileImg = this.userProfileService.userProfileImg;
      //console.log('userProfileService.userProfileData', this.userProfileImg);
    }
  }

  ngOnInit() {
    this.signInActive = this.commonService.signInActive;
    //console.log('applicationMode', this.applicationMode);
    this.applicationMode = this.commonService.applicationMode;
    if (this.router.url === '/canvas/plan') {
      this.commonService.applicationMode = 'planning';
      this.applicationMode = this.commonService.applicationMode;
      this.commonService.signInActive = true;
      this.signInActive = this.commonService.signInActive;
    }
    let context = this;
    window.addEventListener('beforeunload', function(e) {
      //e.preventDefault();
      //console.log(e.currentTarget['performance'].navigation.type)
      let currentUser = sessionStorage.getItem('token');
      if (e.currentTarget['performance'].navigation.type === 1 && currentUser) {
        //  satyajit Sir
      } else {
        if (currentUser) {
          context.logOutUser();
        }
      }
    });
    this.IsAnimation = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.classRoomModeFlag = this.appConfigService.getConfig('global_setting')[
      'classRoomMode'
    ];
    this.fileUploadService.addResourceFlagBroadcaster$.subscribe(flag => {
      // if(flag == ''){
      //   if(this.showLogout){
      //     this.showLogout = false;
      //   }
      // }
      if (flag && flag != null) {
        if (this.addResource) {
          this.addResource.clear();
        }
        if (this.searchResult) {
          this.searchResult.clear();
        }
        this.loadAddResource(flag);
      } else {
        //this.showLogout = false;
        if (this.addResource) {
          this.addResource.clear();
        }
        if (this.searchResult) {
          this.searchResult.clear();
        }
      }
    });
    this.appVersion = this.environment.coreLibVersion;
    this.authService.userState$
      .pipe(
        filter(
          (userState: UserState) =>
            userState.authStatus !== AuthStateEnum.PRE_AUTH
        )
      )
      .subscribe((userState: UserState) => {
        //console.log("canvas userState",userState)
        this.authChecked = true;
        if (this.authChecked) {
          this.userStateValue = userState;
          this.postAuthCheck(userState);
        }
      });
    //console.log('CANVAS-VIEW-INIT');
    this.playerDrawerService.drawerOpenStatus$.subscribe(newOpenStatus => {
      this.showLogout = false;
      this.showRunner = true;
      this.onTimesUp();
      this.drawerOpen = newOpenStatus;
      const userStae = {
        userView: '',
        currentState: ''
      };
      this.commonService.setUserProfileState(userStae);
    });

    this.sessionTimeoutService.showIdle$.subscribe((value: boolean) => {
      this.showIdle = value;
    });

    this.authService.currentSignInState$.subscribe(state => {
      //console.log("canvas-view",state)
      if (state === 'auth.destroy') {
        this.loginOutlet.clear();
      }
      else if(state === 'auth.change.password'){
        //this.isLoggedIn = false
        //console.log("userstate at canvas",state)
      }
    });

    this.whiteboardService.whiteBoardActive$.subscribe(activeState => {
      this.centerMessage = activeState ? false : true;
    });

    this.playerContainerService.openResources$.subscribe(openResources => {
      //console.log(openResources)
      if (openResources.length > 0) {
        this.welcomeBackMessage = false;
      }
    });

    this.commonService.setEditResoureForm$.subscribe(editFlag=>{
      this.editFlag = editFlag;
      });

    this.commonService.classroomModeState$.subscribe(flag => {
      //console.log('flag++++++++++>>>>>>>>>>', flag);
      // if (this.playerDrawerComponentRef) {
      //   this.playerDrawerComponentRef.destroy();
      //   this.playerDrawerComponentRef = null;

      // }
      // this.loadPlayerDrawer()
      this.classRoomModeFlag = flag;
      if (!flag) {
        if (this.planningView) {
          this.planningView.clear();
        }
        this.loadPlanningView();
      } else {
        if (this.planningView) {
          if (this.isLoggedIn) {
            this.subjectSelectorService.getInitialData();
          }

          this.planningView.clear();
        }
      }
      this.changeDetector.detectChanges();
    });

    this.commonService.userProfileState$.subscribe(state => {
      if (state && state['userView'] && state['userView'] != null) {
        if (this.userSettingView) {
          this.userSettingView.clear();
        }
        if (this.changePassword) {
          this.changePassword.clear();
        }
        if (this.changePin) {
          this.changePin.clear();
        }
        this.loadUserView(state);
      } else {
        if (this.userSettingView) {
          this.userSettingView.clear();
        }
        if (this.changePassword) {
          this.changePassword.clear();
        }
        if (this.changePin) {
          this.changePin.clear();
        }
      }
    });

    // this.canvasSubsribtion.add( this.userProfileService.userPrefereViewListBroadcast$.subscribe(data => {
    //   console.log('data', data);
    //   this.inputPrefereView = data;
    // }));

    // this.userProfileService.userPrefereViewListBroadcast$.subscribe(data => {
    //   console.log('data', data);
    //   this.inputPrefereView = data;
    // });

    // this.userProfileService.userPreferList$.subscribe(data => {
    //   console.log('data', data);
    //   this.inputPrefereView = data;
    //   localStorage.setItem('preferViewlist',this.inputPrefereView)
    // });

    

    this.curriculumPlaylistService.filteredMenu$.subscribe(
      (filteredMenu: FilterMenu[]) => {
        // console.log("filterMEnu--------->",filteredMenu)
        this.inputPrefereView = filteredMenu;
        //this.inputPrefereView = this.userProfileService.prefereData;
        //console.log("--------------userProfileServic",this.userProfileService.prefereData)

        if(this.userProfileService.prefereData === undefined || this.userProfileService.prefereData === 'null'){
          //  this.inputPrefereView = this.userProfileService.prefereData;
        //   console.log("userProfileServic",this.userProfileService.prefereData)
         this.inputPrefereView.forEach(element => {
              if(!element.selected){
                element.selected = true;
                element.state = "active";
              }})
    
        }
        
      }
    );
   

    this.userProfileService.getJSON().subscribe(data => {
      //console.log('CanvasViewComponent -> ngOnInit -> local user JSONdata', data);
      this.userProfileData = data;
      this.userProfileImg = data.user_img;
      //this.toolbarService.userProfileImg = this.userProfileImg;
      //console.log('CanvasViewComponent -> ngOnInit -> JSONdata',this.toolbarService.userProfileImg);
    });

    this.commonService.userAdmin$.subscribe(data => {
      if (data) {
        if (this.userAdmin) {
          this.userAdmin.clear();
        }
        this.loadUserAdmin();
      } else {
        if (this.userAdmin) {
          this.userAdmin.clear();
        }
      }
    });

    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.currentSubject = newClassSelection.subject.title;
        }
      }
    );
    // this.toolbarService.toolbarSubmenuTop$.subscribe(flag=>{
    //   if(flag && this.showLogout){
    //     this.showLogout = false
    //   }
    // })
    /*
    this.userProfileService.userProfileAPIData$.subscribe((data: any) => {
      this.userProfileData = data;
      //this.toolbarService.userProfileData = data;
      //this.userProfileService.userProfileData = data;
      console.log('data @ ngInit canvas-view ----->> ', this.userProfileData);
      console.log('data @ ngInit canvas-view this.userProfileService.userProfileAPIData ----->> ', this.userProfileService.userProfileAPIData);
    });
    */

   this.toolbarService.currentSelectedTool$
   .pipe(takeUntil(this.destroy$))
   .subscribe(newToolType => {
    //  console.log(
    //    'CanvasViewComponent -> postAuthCheck -> newToolType',
    //    newToolType
    //  );
     if (this.widgetsConfigService.checkSelectedToolExsitsInWidget(newToolType) &&
       !this.widgetsConfigService.isWidgetLoaded(newToolType)
     ) {
       this.loadWidget(newToolType);
     }
   });
  }

  ngAfterViewInit() {
    //console.log('CANVAS-VIEW-AFTER-INIT');
    if (
      this.appConfigService.getGlobalSettingConfig('moduledate') ===
      GlobalConfigState.ENABLED
    ) {
      this.loadCalendar();
    }

    if (
      this.appConfigService.getGlobalSettingConfig('modulewhiteboard') ===
      GlobalConfigState.ENABLED
    ) {
      this.loadWhiteboard();
    }

    if (
      this.appConfigService.getGlobalSettingConfig('moduleintro') ===
        GlobalConfigState.ENABLED &&
      !this.isLoggedIn
    ) {
      this.loadIntro();
    }
  }

  get globalConfigState() {
    return GlobalConfigState;
  }

  onToggleTheme() {
    if (this.themeService.currentThemeName === 'light') {
      this.themeService.setTheme('dark');
    } else {
      this.themeService.setTheme('light');
    }
  }

  loadIntro() {
    this.libConfigService
      .getComponentFactory<IntroComponent>('lazy-intro')
      .subscribe({
        next: componentFactory => {
          if (!this.canvasGeneralOutlet) {
            return;
          }
          const ref = this.canvasGeneralOutlet.createComponent(
            componentFactory
          );
          (<IntroComponent>ref.instance).postRunCallback = function() {
            ref.destroy();
          };
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadSignIn() {
    this.authService.setCurrentSignInState(SignInState.SESSION_DEFAULT);
    this.authService.setSignInModuleState(SignInModuleState.INACTIVE);
    this.libConfigService
      .getComponentFactory<LoginPwdComponent>('lazy-sign-in')
      .subscribe({
        next: componentFactory => {
          if (!this.loginOutlet) {
            return;
          }
          const ref = this.loginOutlet.createComponent(componentFactory);
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadFTUEWelcome() {
    if (
      this.isLoggedIn &&
      this.firstTimeUser &&
      !this.ftueComponentRef &&
      !this.ftueComponentLoading
    ) {
      this.ftueComponentLoading = true;
      this.libConfigService
        .getComponentFactory<FTUEWelcomeComponent>('lazy-ftue-welcome')
        .subscribe({
          next: componentFactory => {
            if (!this.ftueWelcomeOutlet) {
              return;
            }
            this.ftueComponentRef = this.ftueWelcomeOutlet.createComponent(
              componentFactory
            );
            this.ftueComponentRef.changeDetectorRef.detectChanges();
            this.ftueComponentLoading = false;
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }

  loadCalendar() {
    this.libConfigService
      .getComponentFactory<CalendarComponent>('lazy-calendar')
      .subscribe({
        next: componentFactory => {
          if (!this.canvasGeneralOutlet) {
            return;
          }
          const ref = this.canvasGeneralOutlet.createComponent(
            componentFactory
          );
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadWhiteboard() {
    this.libConfigService
      .getComponentFactory<WhiteboardComponent>('lazy-whiteboard')
      .subscribe({
        next: componentFactory => {
          if (!this.canvasGeneralOutlet) {
            return;
          }

          const ref = this.canvasGeneralOutlet.createComponent(
            componentFactory
          );
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadPlayerDrawer() {
    if (
      this.isLoggedIn &&
      !this.firstTimeUser &&
      !this.playerDrawerComponentRef &&
      !this.playerDrawerComponentLoading
    ) {
      this.playerDrawerComponentLoading = true;
      this.libConfigService
        .getComponentFactory<PlayerDrawerComponent>('player-drawer')
        .subscribe({
          next: componentFactory => {
            if (!this.playerDrawerLoader) {
              return;
            }
            this.playerDrawerComponentRef = this.playerDrawerLoader.createComponent(
              componentFactory
            );
            this.playerDrawerComponentRef.changeDetectorRef.detectChanges();
            this.playerDrawerComponentLoading = false;
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }
  toolbardirection(e, direction) {
    //console.log('direction', direction);
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    const userStae = {
      userView: '',
      currentState: ''
    };
    //console.log('direction-->> ', direction);
    this.commonService.setUserProfileState(userStae);
    if (direction === 'left') {
      this.toolbarsetclass = true;
      this.toolbarService.settoolbarside(this.toolbarsetclass);
    }
    if (direction === 'right') {
      this.toolbarsetclass = false;
      this.toolbarService.settoolbarside(this.toolbarsetclass);
    }
  }

  showSignIn(event: Event) {
    event.stopPropagation();
    this.authService.setSignInModuleState(SignInModuleState.ACTIVE);
  }

  // logOut(callee: string) {
  //   this.authService.logoutUser(false, 'canvas-view-logout');
  // }

  loadAddResource(value) {
    //console.log("CanvasViewComponent -> loadAddResource -> value", value)
    if (value === 'create') {
      this.libConfigService
        .getComponentFactory<CreateResourceComponent>('lazy-addresource')
        .subscribe({
          next: componentFactory => {
            if (!this.addResource) {
              return;
            }
            // console.log(
            //   'CanvasViewComponent -> loadAddResource -> componentFactory',
            //   componentFactory
            // );
            const ref = this.addResource.createComponent(componentFactory);
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
    if (value === 'lib') {
      this.libConfigService
        .getComponentFactory<SearchViewComponent>('lazy-search')
        .subscribe({
          next: componentFactory => {
            if (!this.searchResult) {
              return;
            }
            const ref = this.searchResult.createComponent(componentFactory);
            ref.instance['type'] = 'teachingMode';
            ref.instance['searchViewState'] = 'libraryView';
            ref.instance['currentSubject'] = this.currentSubject;
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }
  logout(event, value) {
    event.preventDefault();
    this.showLogout = value;
    //console.log('--showlogout--', value);
    if (value) {
      this.showRunner = false;
      this.startTimer();
      this.signoutCancelMessage = false;
    } else {
      this.showRunner = true;
      //this.toastrService.error('Welcome Back!');
      this.signoutCancelMessage = true;
      this.onTimesUp();
    }
  }

  // timer funct satyajit
  onTimesUp() {
    clearInterval(this.timerInterval);
    this.timerInterval = null;
    this.timePassed = 0;
    this.timeLeft = this.TIME_LIMIT;
    //console.log('--onTimesUp--');
  }

  startTimer() {
    this.timerInterval = setInterval(() => {
      //console.log('--IN timer--');
      this.timePassed = this.timePassed += 1;
      this.timeLeft = this.TIME_LIMIT - this.timePassed;
      if (document.getElementById('base-timer-label') !== null) {
        document.getElementById('base-timer-label').innerHTML = this.formatTime(
          this.timeLeft
        );
        this.setCircleDasharray();
        this.setRemainingPathColor(this.timeLeft);

        if (this.timeLeft === 0) {
          this.onTimesUp();
          this.logOutUser();
        }
      }
    }, 1000);
  }

  formatTime(time: any) {
    const minutes = Math.floor(time / 60);
    let seconds: any = time % 60;

    if (seconds < 10) {
      seconds = `${seconds}`;
    }

    //return `${minutes}:${seconds}`;
    return `${seconds}`;
  }

  setRemainingPathColor(timeLeft) {
    const { alert, warning, info } = this.COLOR_CODES;
    if (timeLeft <= alert.threshold) {
      document
        .getElementById('base-timer-path-remaining')
        .classList.remove(warning.color);
      document
        .getElementById('base-timer-path-remaining')
        .classList.add(alert.color);
    } else if (timeLeft <= warning.threshold) {
      document
        .getElementById('base-timer-path-remaining')
        .classList.remove(info.color);
      document
        .getElementById('base-timer-path-remaining')
        .classList.add(warning.color);
    }
  }

  calculateTimeFraction() {
    const rawTimeFraction = this.timeLeft / this.TIME_LIMIT;
    return rawTimeFraction - (1 / this.TIME_LIMIT) * (1 - rawTimeFraction);
  }

  setCircleDasharray() {
    const circleDasharray = `${(
      this.calculateTimeFraction() * this.FULL_DASH_ARRAY
    ).toFixed(0)} 283`;
    document
      .getElementById('base-timer-path-remaining')
      .setAttribute('stroke-dasharray', circleDasharray);
  }
  // end of timer funct satyajit

  logOutUser() {
    this.playerDrawerService.setDrawerOpenStatus(false);
    this.showLogout = false;
    this.authService.logoutUser(false, 'user logOut');
  }

  loadUserView(state) {
    if (state['userView'] === 'userProfile') {
      this.libConfigService
        .getComponentFactory<UserSettingViewComponent>('lazy-userProfile')
        .subscribe({
          next: componentFactory => {
            if (!this.userSettingView) {
              return;
            }
            const ref = this.userSettingView.createComponent(componentFactory);
            ref.instance['type'] = this.toolbarsetclass;
            ref.instance['currentView'] = state['currentState'];
            ref.instance['inputPrefereView'] = this.inputPrefereView;
            ref.instance['userProfile'] = this.userProfileData;
            ref.instance['currentMode'] = 'teaching';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
    if (state['userView'] === 'userChangePassword') {
      this.libConfigService
        .getComponentFactory<ChangePasswordComponent>('lazy-userChangePassword')
        .subscribe({
          next: componentFactory => {
            if (!this.changePassword) {
              return;
            }
            const ref = this.changePassword.createComponent(componentFactory);
            ref.instance['type'] = this.toolbarsetclass;
            ref.instance['currentMode'] = 'teaching';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
    if (state['userView'] === 'userChangePin') {
      this.libConfigService
        .getComponentFactory<ChangePinComponent>('lazy-userChangePin')
        .subscribe({
          next: componentFactory => {
            if (!this.changePin) {
              return;
            }
            const ref = this.changePin.createComponent(componentFactory);
            ref.instance['type'] = this.toolbarsetclass;
            ref.instance['currentMode'] = 'teaching';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }

  loadUserAdmin() {
    this.libConfigService
      .getComponentFactory<AdminHomeComponent>('lazy-userAdmin')
      .subscribe({
        next: componentFactory => {
          if (!this.userAdmin) {
            return;
          }
          const ref = this.userAdmin.createComponent(componentFactory);
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadPlanningView() {
    //console.log("--loadPlanningView")
    setTimeout(() => {
      this.libConfigService
      .getComponentFactory<PlanningModeViewComponent>('lazy-planningMode')
      .subscribe({
        next: componentFactory => {
          if (!this.planningView) {
            return;
          }
          //console.log("--loadPlanningView----->>")
          const ref = this.planningView.createComponent(componentFactory);
          ref.instance['userProfile'] = this.userProfileData;
          ref.instance['isLoggedIn'] = this.isLoggedIn;
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
    }, 100);
    
  }

  loadWidget(newToolType) {
    import('@tce/player-widgets').then(({ PlayerWidgetsComponent }) => {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
        PlayerWidgetsComponent
      );
      const componentRef = this.widgetPlayer.createComponent(componentFactory);
      const instance = componentRef.instance as any;
      instance.widgetId = newToolType;
      (instance.destroyPlayer as EventEmitter<boolean>).subscribe(destroy =>{
        componentRef.destroy();
        this.toolbarService.selectTool(ToolType.Pen);
      }
        
      );
    });
  }

  ngOnDestroy() {}
}
