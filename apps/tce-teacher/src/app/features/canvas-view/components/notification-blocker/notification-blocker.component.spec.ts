import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NotificationBlockerComponent } from './notification-blocker.component';

describe('NotificationBlockerComponent', () => {
  let component: NotificationBlockerComponent;
  let fixture: ComponentFixture<NotificationBlockerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NotificationBlockerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotificationBlockerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
