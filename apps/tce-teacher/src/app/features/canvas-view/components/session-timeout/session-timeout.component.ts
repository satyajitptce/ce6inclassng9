import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '@tce/core';
import { SessionTimeoutService } from '@app-teacher/services';

@Component({
  selector: 'tce-session-timeout',
  templateUrl: './session-timeout.component.html',
  styleUrls: ['./session-timeout.component.scss']
})
export class SessionTimeoutComponent implements OnInit {
  constructor(
    public sessionTimeoutService: SessionTimeoutService,
    public authService: AuthenticationService
  ) {}

  ngOnInit() {}
  staySigned(callee: string) {
    /** change tce  */
    // this.authService.refreshToken(callee + " > staySigned222");
    this.authService.extendSession(callee + ' > staySigned222');
    // this.authService.logoutUser(true, "staySigned");
    this.sessionTimeoutService.setIdleScreen(false, 'staySigned333');
  }
  signOut(callee: string) {
    this.authService.logoutUser(false, 'signOut 1');
  }
}
