import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarModule } from '@tce/toolbar';

import { CanvasViewComponent } from './components/canvas-view/canvas-view.component';
import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';
import { ResourceType, CoreModule } from '@tce/core';
import { CanvasViewRoutingModule } from './canvas-view-routing.module';
import { SessionTimeoutComponent } from './components/session-timeout/session-timeout.component';
import { PlayerContainerModule } from '../player-container/player-container.module';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PlanningModeModule } from 'libs/planning-mode/src/lib/planning-mode.module';
import { CalendarModule } from '../calendar/calendar.module';
import { from } from 'rxjs';
import { NotificationBlockerComponent } from './components/notification-blocker/notification-blocker.component';
import { PlayerWidgetsModule } from '@tce/player-widgets';
/* TCE palyer code starts*/
// import { PlayerTceVideoModule } from '@tce/player-tce-video';
/* TCE palyer code ends*/

const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'lazy-intro',
    path: 'lazy-intro', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../intro/intro.module').then(mod => mod.IntroModule)
  },
  {
    componentId: 'lazy-whiteboard',
    path: 'lazy-whiteboard', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/whiteboard').then(mod => mod.WhiteboardModule)
  },
  {
    componentId: 'lazy-calendar',
    path: 'lazy-calendar', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../calendar/calendar.module').then(mod => mod.CalendarModule)
  },
  {
    componentId: 'lazy-ftue-welcome',
    path: 'lazy-ftue-welcome', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../first-time-user-welcome/first-time-user-welcome.module').then(
        mod => mod.FirstTimeUserModule
      )
  },
  {
    componentId: 'lazy-sign-in',
    path: 'lazy-sign-in', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../login/sign-in/sign-in.module').then(mod => mod.SignInModule)
  },

  {
    componentId: 'curriculum-playlist',
    path: 'curriculum-playlist', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../curriculum-playlist/curriculum-playlist.module').then(
        mod => mod.CurriculumPlaylistModule
      )
  },
  {
    componentId: 'subject-selector',
    path: 'subject-selector', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../subject-selector/subject-selector.module').then(
        mod => mod.SubjectSelectorModule
      )
  },
  {
    componentId: 'player-drawer',
    path: 'player-drawer', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../player-drawer/player-drawer.module').then(
        mod => mod.PlayerDrawerModule
      )
  },
  {
    componentId: 'worksheet',
    path: 'worksheet', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-worksheet').then(mod => mod.PlayerWorksheetModule)
  },
  {
    componentId: 'player-' + ResourceType.TCEVIDEO,
    path: 'player-' + ResourceType.TCEVIDEO, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-tce-video').then(mod => mod.PlayerTceVideoModule)
  },
  {
    componentId: 'player-' + ResourceType.WORKSHEET,
    path: 'player-' + ResourceType.WORKSHEET, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-worksheet').then(mod => mod.PlayerWorksheetModule)
  },
  {
    componentId: 'player-' + ResourceType.EBOOK,
    path: 'player-' + ResourceType.EBOOK, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-ebook').then(mod => mod.PlayerEbookModule)
  },
  {
    componentId: 'player-' + ResourceType.QUIZ,
    path: 'player-' + ResourceType.QUIZ, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-quiz').then(mod => mod.PlayerQuizModule)
  },
  {
    componentId: 'player-' + ResourceType.GALLERY,
    path: 'player-' + ResourceType.GALLERY, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-gallery').then(mod => mod.PlayerGalleryModule)
  },
  {
    componentId: 'player-' + ResourceType.VIDEO,
    path: 'player-' + ResourceType.VIDEO, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-custom-video').then(
        mod => mod.PlayerCustomVideoModule
      )
  },
  {
    componentId: 'player-' + ResourceType.UNSUPPORT,
    path: 'player-' + ResourceType.UNSUPPORT, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-unsupport').then(mod => mod.PlayerUnsupportModule)
  },
  {
    componentId: 'lazy-addresource',
    path: 'lazy-addresource', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../add-resource/add-resource.module').then(
        mod => mod.AddResourceModule
      )
  },
  {
    componentId: 'lazy-search',
    path: 'lazy-search', // some globally-unique identifier, used internally by the router
    loadChildren: () => import('@tce/search').then(mod => mod.SearchModule)
  },
  {
    componentId: 'lazy-userProfile',
    path: 'lazy-userProfile', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/user-profile').then(mod => mod.UserProfileModule)
  },
  {
    componentId: 'lazy-userChangePassword',
    path: 'lazy-userChangePassword', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import(
        '../user-profile-change-password/user-profile-change-password.module'
      ).then(mod => mod.UserProfileChangePasswordModule)
  },
  {
    componentId: 'lazy-userChangePin',
    path: 'lazy-userChangePin', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../user-profile-change-pin/user-profile-change-pin.module').then(
        mod => mod.UserProfileChangePinModule
      )
  },
  {
    componentId: 'lazy-userAdmin',
    path: 'lazy-userAdmin', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('../admin/admin.module').then(mod => mod.AdminModule)
  },
  {
    componentId: 'lazy-planningMode',
    path: 'lazy-planningView', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/planning-mode').then(mod => mod.PlanningModeModule)
  }
];

@NgModule({
  declarations: [CanvasViewComponent, SessionTimeoutComponent, NotificationBlockerComponent],
  imports: [
    CommonModule,
    ToolbarModule,
    LibConfigModule.forRoot(manifests),
    CanvasViewRoutingModule,
    PlayerContainerModule,
    CoreModule,
    AngularSvgIconModule,
    PlanningModeModule,
    CalendarModule,
    PlayerWidgetsModule
  ],
  exports: [NotificationBlockerComponent],
  entryComponents: [NotificationBlockerComponent]
})
export class CanvasViewModule {}
