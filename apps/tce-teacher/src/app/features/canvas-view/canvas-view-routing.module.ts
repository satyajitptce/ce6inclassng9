import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlanningModeViewComponent } from '@tce/planning-mode';

import { CanvasViewComponent } from './components/canvas-view/canvas-view.component';

const routes: Routes = [
  {
    path: '',
    component: CanvasViewComponent
  },
  {
    path: 'plan',
    component: CanvasViewComponent
  }

  // {
  //   path: 'plan-modes',
  //   loadChildren: () =>
  //     import('libs/planning-mode/src/lib/planning-mode.module').then(
  //       mod => mod.PlanningModeModule
  //     )
  // }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CanvasViewRoutingModule {}
