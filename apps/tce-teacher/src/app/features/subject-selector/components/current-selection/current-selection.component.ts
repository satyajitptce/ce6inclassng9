import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  FullClassSelection,
  FullContentSelection,
  ThemeService,
  CurriculumPlaylistService,
  SubjectSelectorService,
  FileUploadService
} from '@tce/core';

@Component({
  selector: 'tce-current-selection',
  templateUrl: './current-selection.component.html',
  styleUrls: ['./current-selection.component.scss']
})
export class CurrentSelectionComponent implements OnInit {
  @Output() currentSubjectClick = new EventEmitter();
  @Output() currentContentClick = new EventEmitter();
  @Input() currentMode;
  currentClassSelectionText = 'Choose a class';
  currentClassSelectionTextMode = 'Choose a class';
  currentContentSelectionText = '';
  firstTimeUser = false;
  textShadow = false;
  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private themeService: ThemeService,
    private fileUploadService: FileUploadService
  ) {}

  ngOnInit() {
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.currentClassSelectionText =
            newClassSelection.gradeLevel.title +
            (newClassSelection.division
              ? newClassSelection.division.divisionTitle
              : '') +
            ' | ' +
            newClassSelection.subject.title;
          this.currentClassSelectionTextMode = this.currentClassSelectionText;
        }
      }
    );
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.currentContentSelectionText =
            fullContentSelection.chapter.chapterNumber +
            '.' +
            fullContentSelection.topic.topicNumber +
            ' | ' +
            fullContentSelection.topic.topicTitle;
        } else {
          this.currentContentSelectionText = '';
        }
      }
    );
    this.themeService.currentThemeNameSubject$.subscribe(theme => {
      if (this.currentMode) {
        this.textShadow = theme === 'light';
      } else {
        this.textShadow = true;
      }
    });
    this.curriculumPlaylistService.selectedSubjectChapterPlanningList$.subscribe(
      (data: any) => {
        if (data) {
          this.currentClassSelectionTextMode =
            data.currentGradeId.title +
            (data.currentGradeId.divisions && data.currentGradeId.divisions[0]
              ? data.currentGradeId.divisions[0].divisionTitle
              : '') +
            '|' +
            data.currentSubjectId.title;
        }
      }
    );
  }

  handleSubjectClick(event) {
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    this.currentSubjectClick.emit(event);
  }
  handleContentClick(event) {
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    this.currentContentClick.emit(event);
  }
}
