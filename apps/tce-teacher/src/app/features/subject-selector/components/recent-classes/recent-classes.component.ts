import { Component, OnInit } from '@angular/core';
import {
  IGetRecentData,
  GradeLevel,
  CurriculumPlaylistService,
  SubjectSelectorService,
  FullClassSelection,
  ApiSubject,
  RecentViewState
} from '@tce/core';

@Component({
  selector: 'tce-recent-classes',
  templateUrl: './recent-classes.component.html',
  styleUrls: ['./recent-classes.component.scss']
})
export class RecentClassesComponent implements OnInit {
  recentViewData: RecentViewState[] = [];
  selectedSubject: ApiSubject;
  recentClasses: FullClassSelection[];
  availableGrades: GradeLevel[];
  showGhost: Boolean = true;

  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {
    this.subjectSelectorService.availableGrades$.subscribe(grades => {
      this.availableGrades = grades;
    });
  }

  ngOnInit() {
    /*
    Below subscribe is done to get the GET Data and show up in the UI.
    */
    this.subjectSelectorService.recentViewsData$.subscribe(recentView => {
      this.recentViewData = recentView;
      this.showGhost = false;
    });
  }

  selectClass(recent: RecentViewState) {
    this.subjectSelectorService.selectRecentView(recent);
  }

  /*
    click event to update the current selection title, the current api structure will not be able to use the service until the models are corrected
  */
  // selectClass(selectedClass: FullClassSelection) {
  //   this.subjectSelectorService.setFullClassSelection({
  //     gradeLevel: selectedClass.gradeLevel,
  //     division: selectedClass.division,
  //     subject: selectedClass.subject
  //   });
  // }
}
