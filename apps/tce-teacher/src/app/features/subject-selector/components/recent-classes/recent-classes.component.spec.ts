import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentClassesComponent } from './recent-classes.component';

describe('RecentClassesComponent', () => {
  let component: RecentClassesComponent;
  let fixture: ComponentFixture<RecentClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [RecentClassesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
