/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GhostRecentClassesComponent } from './ghost-recent-classes.component';

describe('GhostRecentClassesComponent', () => {
  let component: GhostRecentClassesComponent;
  let fixture: ComponentFixture<GhostRecentClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GhostRecentClassesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhostRecentClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
