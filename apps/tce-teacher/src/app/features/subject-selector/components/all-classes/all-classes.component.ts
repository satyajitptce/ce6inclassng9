import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import {
  GradeLevel,
  ApiSubject,
  ApiDivision,
  SubjectSelectorService,
  CurriculumPlaylistService
} from '@tce/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'tce-all-classes',
  templateUrl: './all-classes.component.html',
  styleUrls: ['./all-classes.component.scss']
})
export class AllClassesComponent implements OnInit, OnDestroy {
  @Input() currentMode;
  availableGrades: GradeLevel[] = [];
  availableDivisions: ApiDivision[] = [];
  availableSubjects: ApiSubject[] = [];

  selectedGradeLevel: GradeLevel = null;
  selectedDivision: ApiDivision = null;
  selectedSubject: ApiSubject = null;

  subjectSelectorSubscription: Subscription = new Subscription();
  showGhost = true;

  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {}

  ngOnInit() {
    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.availableGrades$.subscribe(
        (gradeLevels: GradeLevel[]) => {
          this.showGhost = false;
          this.availableGrades = gradeLevels;
        }
      )
    );

    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.gradeLevelSelection$.subscribe(gradeLevel => {
        this.selectedGradeLevel = gradeLevel;
        if (gradeLevel) {
          this.availableDivisions = gradeLevel.divisions || [];
          this.availableSubjects = gradeLevel.subjects;
        }
      })
    );

    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.divisionSelection$.subscribe(
        division => (this.selectedDivision = division)
      )
    );

    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.subjectSelection$.subscribe(
        subject => (this.selectedSubject = subject)
      )
    );
  }

  selectGrade(gradeLevel: GradeLevel) {
    if (this.currentMode) {
      this.subjectSelectorService.setGradeLevelSelection(gradeLevel);
    } else {
      this.clickToSelectGradePlanning(gradeLevel);
    }
  }

  selectDivision(division: ApiDivision) {
    if (this.currentMode) {
      this.subjectSelectorService.setDivisionSelection(division);
    } else {
      this.clickToSelectDivisionPlanning(division);
    }
  }

  selectSubject(subject: ApiSubject) {
    if (this.currentMode) {
      this.subjectSelectorService.setSubjectSelection(subject);
    } else {
      this.clickToSelectSubjectPlanning(subject, this.selectedGradeLevel);
    }
  }
  clickToSelectGradePlanning(gradeLevel: GradeLevel) {
    this.selectedGradeLevel = gradeLevel;
    this.availableDivisions = gradeLevel.divisions || [];
    this.availableSubjects = gradeLevel.subjects;
    if (this.availableDivisions && this.availableDivisions.length > 0) {
      this.clickToSelectDivisionPlanning(this.availableDivisions[0]);
    }
    if (this.availableSubjects && this.availableSubjects.length > 0) {
      this.clickToSelectSubjectPlanning(this.availableSubjects[0], gradeLevel);
    }
  }
  clickToSelectDivisionPlanning(division: ApiDivision) {
    this.selectedDivision = division;
  }
  clickToSelectSubjectPlanning(subject: ApiSubject, gradeLevel: GradeLevel) {
    this.selectedSubject = subject;
    const subjectGrade = { gradeLevel, subject, type: 'planningMode' };
    this.curriculumPlaylistService.setselectedSubjectChapter(subjectGrade);
  }
  ngOnDestroy() {
    this.subjectSelectorSubscription.unsubscribe();
  }
}
