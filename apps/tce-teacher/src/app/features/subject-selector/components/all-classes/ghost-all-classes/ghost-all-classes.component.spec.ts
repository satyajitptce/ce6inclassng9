/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { GhostAllClassesComponent } from './ghost-all-classes.component';

describe('GhostAllClassesComponent', () => {
  let component: GhostAllClassesComponent;
  let fixture: ComponentFixture<GhostAllClassesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GhostAllClassesComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GhostAllClassesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
