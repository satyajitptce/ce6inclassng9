import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubjectSelectorContainerComponent } from './subject-selector-container.component';

describe('SubjectSelectorContainerComponent', () => {
  let component: SubjectSelectorContainerComponent;
  let fixture: ComponentFixture<SubjectSelectorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SubjectSelectorContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubjectSelectorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
