import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  Input,
  ViewChild,
  HostBinding
} from '@angular/core';

import {
  SubjectSelectorService,
  CurriculumPlaylistService,
  AppConfigService,
  CommonService
} from '@tce/core';
import { DomSanitizer } from '@angular/platform-browser';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
@Component({
  selector: 'tce-subject-selector-container',
  templateUrl: './subject-selector-container.component.html',
  styleUrls: ['./subject-selector-container.component.scss'],
  animations: [
    trigger('openClose', [
      state(
        'open',
        style({
          transform: 'translateY(0%)',
          opacity: 1
        })
      ),
      state(
        'closed',
        style({
          transform: 'translateY(5%)',
          opacity: 0
        })
      ),
      state(
        'in',
        style({
          opacity: 1
        })
      ),
      transition('* => *', [animate('300ms ease-in')]),
      transition('* => open', [animate('300ms ease-in')]),

      transition('* => closed', [animate('300ms ease-in')]),
      transition('open=> closed', [animate('300ms ease-in')])
    ]),
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class SubjectSelectorContainerComponent implements OnInit {
  displaySubjectSelectionFlyout = false;
  displayRecentClasses = false;
  @Input() firstTimeUser = false;
  @Input() currentMode;
  @ViewChild('currentSelection', { static: true, read: ElementRef })
  currentSelection: ElementRef;
  @ViewChild('selectorPanel', { static: true, read: ElementRef })
  selectorPanel: ElementRef;

  displayContentSelectionFlyout = false;
  animationFlag: any;
  constructor(
    private eRef: ElementRef,
    private subjectSelectorService: SubjectSelectorService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private sanitizer: DomSanitizer,
    private appConfigService: AppConfigService,
    private commonService: CommonService
  ) {}

  ngOnInit() {
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.subjectSelectorService.fullClassSelection$.subscribe(
      classSelection => {
        if (classSelection) {
          this.displaySubjectSelectionFlyout = false;
        }
      }
    );
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      fullContentSelection => {
        if (fullContentSelection) {
          this.displayContentSelectionFlyout = false;
        }
      }
    );
    this.subjectSelectorService.recentViewsData$.subscribe(
      recentClasses => (this.displayRecentClasses = !!recentClasses.length)
    );
    this.curriculumPlaylistService.selectedSubjectChapterPlanningList$.subscribe(
      data => {
        if (data) {
          this.displaySubjectSelectionFlyout = false;
        }
      }
    );
  }

  toggleSubjectFlyout(event: any) {
    this.displayContentSelectionFlyout = false;
    this.displaySubjectSelectionFlyout = !this.displaySubjectSelectionFlyout;
  }

  @HostListener('document:click', ['$event'])
  clickOutside(event: Event) {
    if (
      !(
        this.eRef.nativeElement.contains(event.target) ||
        this.eRef.nativeElement.contains(event.target)
      )
    ) {
      this.displaySubjectSelectionFlyout = false;
      this.displayContentSelectionFlyout = false;
    }
  }
  toggleContentFlyout() {
    this.displaySubjectSelectionFlyout = false;
    this.displayContentSelectionFlyout = !this.displayContentSelectionFlyout;
  }

  @HostBinding('attr.style')
  public get toolTip(): any {
    //still working on this
    const value =
      String(
        this.currentSelection.nativeElement.children[0].children[0].children[0]
          .offsetWidth + 10
      ) + 'px';
    return this.sanitizer.bypassSecurityTrustStyle(`--leftValue: ${value}`);
  }
}
