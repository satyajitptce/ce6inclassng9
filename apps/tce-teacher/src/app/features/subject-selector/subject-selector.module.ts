import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '@tce/core';
import { LibConfigModule } from '@tce/lib-config';
import { CurrentSelectionComponent } from './components/current-selection/current-selection.component';
import { RecentClassesComponent } from './components/recent-classes/recent-classes.component';
import { AllClassesComponent } from './components/all-classes/all-classes.component';
import { SubjectSelectorContainerComponent } from './components/subject-selector-container/subject-selector-container.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { GhostRecentClassesComponent } from './components/recent-classes/ghost-recent-classes/ghost-recent-classes.component';
import { GhostAllClassesComponent } from './components/all-classes/ghost-all-classes/ghost-all-classes.component';
import { CurriculumLessonSelectorModule } from '../curriculum-lesson-selector/curriculum-lesson-selector.module';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    LibConfigModule.forChild(SubjectSelectorContainerComponent),
    AngularSvgIconModule,
    CurriculumLessonSelectorModule
  ],
  declarations: [
    CurrentSelectionComponent,
    RecentClassesComponent,
    AllClassesComponent,
    SubjectSelectorContainerComponent,
    GhostAllClassesComponent,
    GhostRecentClassesComponent
  ],
  exports: [SubjectSelectorContainerComponent]
})
export class SubjectSelectorModule {}
