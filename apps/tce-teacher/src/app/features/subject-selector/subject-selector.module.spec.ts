import { async, TestBed } from '@angular/core/testing';
import { SubjectSelectorModule } from './subject-selector.module';

describe('SubjectSelectorModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [SubjectSelectorModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(SubjectSelectorModule).toBeDefined();
  });
});
