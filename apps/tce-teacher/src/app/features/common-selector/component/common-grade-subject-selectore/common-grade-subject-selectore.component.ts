import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  GradeLevel,
  ApiSubject,
  ApiDivision,
  SubjectSelectorService
} from '@tce/core';
import { Subscription } from 'rxjs';
@Component({
  selector: 'tce-common-grade-subject-selectore',
  templateUrl: './common-grade-subject-selectore.component.html',
  styleUrls: ['./common-grade-subject-selectore.component.scss']
})
export class CommonGradeSubjectSelectoreComponent implements OnInit {
  @Input() allGrade;
  @Input() allSubject;
  @Input() selectGrade;
  @Input() selectSubject;
  @Input() currentType;
  @Output() valueChangeGradeSubject = new EventEmitter();
  @Output() valueChangeGradeSubjectPlanning = new EventEmitter();
  availableGrades: GradeLevel[] = [];
  availableSubjects: ApiSubject[] = [];
  selectedGradeLevel: GradeLevel = null;
  selectedSubject: ApiSubject = null;

  subjectSelectorSubscription: Subscription = new Subscription();
  constructor(private subjectSelectorService: SubjectSelectorService) {}

  ngOnInit() {
    this.availableGrades = this.allGrade;
    this.availableSubjects = this.allSubject;
    this.selectedGradeLevel = this.selectGrade;
    this.selectedSubject = this.selectSubject;
  }

  clickToSelectGrade(gradeLevel: GradeLevel) {
    console.log('gradeLevel ', gradeLevel, this.currentType);

    this.selectedGradeLevel = gradeLevel;
    this.availableSubjects = gradeLevel.subjects;
    if (this.currentType === 'planning') {
      // this.clickToSelectGradePlanning(gradeLevel);
    }
    if (this.currentType === 'teaching') {
      this.clickToSelectSubject(gradeLevel.subjects[0]);
    }
  }
  clickToSelectGradePlanning(gradeLevel: GradeLevel) {
    console.log('grade planning');

    if (gradeLevel.subjects && gradeLevel.subjects.length > 0) {
      this.clickToSelectSubjectPlanning(gradeLevel.subjects[0], gradeLevel);
    }
  }
  clickToSelectSubject(subject: ApiSubject) {
    if (this.currentType === 'teaching') {
      if (subject) {
        if (
          this.selectedGradeLevel &&
          this.selectedGradeLevel.subjects &&
          this.selectedGradeLevel.subjects.length > 0
        ) {
          this.selectedGradeLevel.subjects.forEach(element => {
            if (element.subjectId === subject.subjectId) {
              this.selectedSubject = element;
              this.SelectedValueChangeGradeSubject(
                this.selectedGradeLevel,
                this.selectedSubject
              );
            }
          });
        }
      }
    }
    if (this.currentType === 'planning') {
      this.clickToSelectSubjectPlanning(subject, this.selectedGradeLevel);
    }
  }
  SelectedValueChangeGradeSubject(gradeLevel, subject) {
    const newdata = { gradeLevel, subject, type: 'addResource' };
    this.valueChangeGradeSubject.emit(newdata);
  }
  clickToSelectSubjectPlanning(subject: ApiSubject, gradeLevel: GradeLevel) {
    this.selectedSubject = subject;
    const subjectGrade = { gradeLevel, subject, type: 'planningMode' };
    this.valueChangeGradeSubjectPlanning.emit(subjectGrade);
    // this.curriculumPlaylistService.setselectedSubjectChapter(subjectGrade);
  }
}
