import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonGradeSubjectSelectoreComponent } from './common-grade-subject-selectore.component';

describe('CommonGradeSubjectSelectoreComponent', () => {
  let component: CommonGradeSubjectSelectoreComponent;
  let fixture: ComponentFixture<CommonGradeSubjectSelectoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonGradeSubjectSelectoreComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonGradeSubjectSelectoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
