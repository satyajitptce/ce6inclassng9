import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonChapterTopicSelectoreComponent } from './common-chapter-topic-selectore.component';

describe('CommonChapterTopicSelectoreComponent', () => {
  let component: CommonChapterTopicSelectoreComponent;
  let fixture: ComponentFixture<CommonChapterTopicSelectoreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CommonChapterTopicSelectoreComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonChapterTopicSelectoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
