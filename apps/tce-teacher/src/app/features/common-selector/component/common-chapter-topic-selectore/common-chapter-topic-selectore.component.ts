import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import {
  Chapter,
  Topic,
  FullClassSelection,
  CurriculumPlaylistService
} from '@tce/core';
@Component({
  selector: 'tce-common-chapter-topic-selectore',
  templateUrl: './common-chapter-topic-selectore.component.html',
  styleUrls: ['./common-chapter-topic-selectore.component.scss']
})
export class CommonChapterTopicSelectoreComponent implements OnInit {
  @Input() allChapters;
  @Input() allTopics;
  @Input() selectChapter;
  @Input() selectTopic;
  @Output() valueChangeChapterTopic = new EventEmitter();
  availableChapters: Chapter[];
  selectedChapter: Chapter;
  availableTopics: Topic[];
  selectedTopic: Topic;
  fullClassSelection: FullClassSelection;
  errorMessage: any;
  constructor(private curriculumPlaylistService: CurriculumPlaylistService) {}

  ngOnInit() {
    this.selectedChapter = this.selectChapter;
    this.selectedTopic = this.selectTopic;
    this.availableChapters = this.allChapters;
    this.availableTopics = this.allTopics;
    console.log('topic ', this.selectTopic);

    this.curriculumPlaylistService.selectedChapterErrorMessage$.subscribe(
      errorMessage => {
        if (errorMessage) {
          this.errorMessage = errorMessage;
          this.availableChapters = [];
          this.availableTopics = [];
        }
      }
    );
  }
  clickToSelectChapter(chapter: Chapter) {
    this.selectedChapter = chapter;
    this.availableTopics = chapter.topics;
    this.selectedTopic = chapter.topics[0];
    this.valueChangeChapterTopicOption(chapter, chapter.topics[0]);
  }

  clickToSelectTopic(topic: Topic) {
    this.selectedTopic = topic;
    this.valueChangeChapterTopicOption(this.selectedChapter, topic);
  }
  valueChangeChapterTopicOption(chapter, topic) {
    const newdata = { chapter, topic };
    this.valueChangeChapterTopic.emit(newdata);
  }
}
