import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CommonGradeSubjectSelectoreComponent } from './component/common-grade-subject-selectore/common-grade-subject-selectore.component';
import { CommonChapterTopicSelectoreComponent } from './component/common-chapter-topic-selectore/common-chapter-topic-selectore.component';
import { CoreModule } from '@tce/core';
@NgModule({
  declarations: [
    CommonGradeSubjectSelectoreComponent,
    CommonChapterTopicSelectoreComponent
  ],
  imports: [CommonModule, CoreModule],
  exports: [
    CommonGradeSubjectSelectoreComponent,
    CommonChapterTopicSelectoreComponent
  ],
  entryComponents: [
    CommonGradeSubjectSelectoreComponent,
    CommonChapterTopicSelectoreComponent
  ]
})
export class CommonSelectorModule {}
