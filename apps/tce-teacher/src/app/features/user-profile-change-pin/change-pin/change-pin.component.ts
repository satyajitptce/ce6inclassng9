import {
  Component,
  OnInit,
  Input,
  AfterViewInit,
  OnDestroy
} from '@angular/core';
import {
  ToolbarService,
  CommonService,
  KeyboardService,
  KeyboardState,
  KeyboardTheme
} from '@tce/core';
import { PlayerDrawerService } from '@app-teacher/services';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators,
  ValidatorFn
} from '@angular/forms';
import { Subscription, from } from 'rxjs';
import { PlanningModeService } from '../../../../../../../libs/planning-mode/src/lib/services/planning-mode.service';
import { ToastrService } from 'ngx-toastr';
export function ConfirmedValidator(
  controlName: string,
  matchingControlName: string
) {
  return (formGroup: FormGroup) => {
    if (formGroup.controls) {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (
        matchingControl.errors &&
        !matchingControl.errors.confirmedValidator
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  };
}
@Component({
  selector: 'tce-change-pin',
  templateUrl: './change-pin.component.html',
  styleUrls: ['./change-pin.component.scss']
})
export class ChangePinComponent implements OnInit, OnDestroy {
  _type = false;
  @Input() currentMode;
  newUserTop: any;
  ChangePinMessage: any = '';
  activeChangePinMessage: any = false;
  pattern = '^[0-9]*$';
  changePinSubscription: Subscription = new Subscription();
  changePin: FormGroup = new FormGroup({});
  @Input('type')
  set type(flag) {
    //console.log("UserSettingViewComponent -> settype -> flag", flag)
    this._type = flag;
    this.loadDynamicCSS(flag);
  }
  get type() {
    return this._type;
  }
  constructor(
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService,
    private commonService: CommonService,
    private fb: FormBuilder,
    private keyboardService: KeyboardService,
    private planningModeService: PlanningModeService,
    private toastrService: ToastrService
  ) {
    this.changePin = fb.group(
      {
        currentPinOtp1: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        currentPinOtp2: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        currentPinOtp3: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        currentPinOtp4: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        currentPinOtp5: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        newPinOtp1: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        newPinOtp2: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        newPinOtp3: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        newPinOtp4: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        newPinOtp5: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        repeatNewPinOtp1: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        repeatNewPinOtp2: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        repeatNewPinOtp3: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        repeatNewPinOtp4: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ],
        repeatNewPinOtp5: [
          '',
          [
            Validators.required,
            Validators.pattern(new RegExp(this.pattern)),
            Validators.minLength(1),
            Validators.maxLength(1)
          ]
        ]
      },
      {
        validators: [
          ConfirmedValidator('newPinOtp1', 'repeatNewPinOtp1'),
          ConfirmedValidator('newPinOtp2', 'repeatNewPinOtp2'),
          ConfirmedValidator('newPinOtp3', 'repeatNewPinOtp3'),
          ConfirmedValidator('newPinOtp4', 'repeatNewPinOtp4'),
          ConfirmedValidator('newPinOtp5', 'repeatNewPinOtp5')
        ]
      }
    );
  }

  ngOnInit(): void {
    this.changePinSubscription.add(
      this.toolbarService.toolbarSubmenuTop$.subscribe((top: any) => {
        if (top) {
          this.newUserTop = top.userTop - 20;
        }
      })
    );
    this.changePinSubscription.add(
      this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
        let getMainTop: any;
        if (flag) {
          getMainTop = 70;
          this.newUserTop = this.newUserTop - 50;
        } else {
          getMainTop = 85;
          this.newUserTop = this.newUserTop - 35;
        }
        const drawerDocumentElement = document.documentElement;
        drawerDocumentElement.style.setProperty(
          `--${'maincontainerTop'}`,
          `${getMainTop + 'px'}`
        );
        drawerDocumentElement.style.setProperty(
          `--${'arrowtop'}`,
          `${this.newUserTop + 'px'}`
        );
        // const userStae = {
        //   userView:'',
        //   currentState:''
        // }
        // this.commonService.setUserProfileState(userStae);
      })
    );
  }
  loadDynamicCSS(res) {
    let viewsidePosition: any;
    let viewsideRotation: any;
    if (!res) {
      viewsidePosition = '101%';
      viewsideRotation = 'rotate(270deg)';
    } else {
      viewsidePosition = '-15px';
      viewsideRotation = 'rotate(90deg)';
    }
    if (viewsidePosition && viewsideRotation) {
      const toolbarDocumentElement = document.documentElement;
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerposition'}`,
        `${viewsidePosition}`
      );
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerRotation'}`,
        `${viewsideRotation}`
      );
    }
  }

  get f() {
    return this.changePin.controls;
  }
  submitChangePin() {
    this.ChangePinMessage = 'Update PIN';
    this.activeChangePinMessage = true;
    console.log('this.changePin', this.changePin);
    let oldpin =
      this.changePin.value.currentPinOtp1 +
      this.changePin.value.currentPinOtp2 +
      this.changePin.value.currentPinOtp3 +
      this.changePin.value.currentPinOtp4 +
      this.changePin.value.currentPinOtp5;
    let newpin =
      this.changePin.value.newPinOtp1 +
      this.changePin.value.newPinOtp2 +
      this.changePin.value.newPinOtp3 +
      this.changePin.value.newPinOtp4 +
      this.changePin.value.newPinOtp5;
    if (oldpin && newpin) {
      let newData = {
        op: 'replace',
        oldPin: oldpin,
        pin: newpin
      };

      if (newData) {
        this.commonService.setNewPin(newData);
        this.activeChangePinMessage = false;
        this.cancelChangePin();

      }
    }
  }

  cancelChangePin() {
    this.closeKeyboard();
    const userStae = {
      userView: 'userProfile',
      currentState: 'profile'
    };
    if (this.currentMode === 'teaching') {
      this.commonService.setUserProfileState(userStae);
    }
    if (this.currentMode === 'planning') {
      this.planningModeService.setUserProfileState(userStae);
    }
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      maxLength: 1,
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.NUMERIC
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }

  updateFieldValue(Name: string, val) {
    this.changePin.controls[Name].setValue(val);
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  autoGeneratePIN() {
    const PinNumber = Math.floor(Math.random() * 90000) + 10000;
    if (PinNumber) {
      const stringNumber = PinNumber.toString().split('');
      if (stringNumber) {
        this.changePin.controls.newPinOtp1.setValue(stringNumber[0]);
        this.changePin.controls.newPinOtp2.setValue(stringNumber[1]);
        this.changePin.controls.newPinOtp3.setValue(stringNumber[2]);
        this.changePin.controls.newPinOtp4.setValue(stringNumber[3]);
        this.changePin.controls.newPinOtp5.setValue(stringNumber[4]);
      }
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.closeKeyboard();
    this.changePinSubscription.unsubscribe();
  }
}
