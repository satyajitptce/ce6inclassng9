import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChangePinComponent } from './change-pin/change-pin.component';
import { LibConfigModule } from '@tce/lib-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CoreModule } from '@tce/core';

@NgModule({
  declarations: [ChangePinComponent],
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    LibConfigModule.forChild(ChangePinComponent)
  ],
  exports: [ChangePinComponent],
  entryComponents: [ChangePinComponent]
})
export class UserProfileChangePinModule {}
