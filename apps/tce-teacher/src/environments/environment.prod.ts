import { ProductionEnvironmentBaseline } from '@tce/core';
import * as merge from 'deepmerge';

export const environment = merge(ProductionEnvironmentBaseline, {});
