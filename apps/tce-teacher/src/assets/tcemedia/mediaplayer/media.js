var isMediaEneded = false;
var isAllMediaPaused = false;
var testFactor = 1;
var currentScaleFactor = 1;
var bottomGap = 30;

var Media = function() {
  var self = this;
  //tce player
  self.mediaTop = 20;
  self.mediaLeft = 20;
  self.mediaTopZ = 0;
  self.shapeCount = 0;
  self.init = function() {
    //self.createMediaList();
    //self.zoomer(380);
  };


  self.pauseAllMedia = function (){

    for(var i = 0; i < tcePlayerInstanceArr.length; i++){
      //tcePlayerInstanceArr[i].externalPause();
      //console.log("--->>MEDIA PLAY PAUSE")
      if(!tcePlayerInstanceArr[i].playerIsPaused()){
        //tcePlayerInstanceArr[i].externalPause();
      }
      //tcePlayerInstanceArr[i].externalStop();
    }

  }
  self.setPlayerSizeData = function() {
    if (tcePlayerInstanceArr.length > 0  && tcePlayerInstanceArr[tcePlayerInstanceArr.length - 1].resizeWindowObj) {
      //console.log("RESIZE - WINDOW - MEDIA-->",tcePlayerInstanceArr[tcePlayerInstanceArr.length-1])
      //tcePlayerInstanceArr[tcePlayerInstanceArr.length-1].resizeWindowObj.externalData = externalData;
      tcePlayerInstanceArr[tcePlayerInstanceArr.length - 1].resizeWindowObj.onResize();
    }
  };
};

function tcePlayerReSize() {
  
  media = new Media();
  const a = setInterval(function(){
    if(media !== null){
      //console.log("--media.setPlayerSizeData()")
      media.setPlayerSizeData();
    }

    clearInterval(a);
  }, 100);

}

function stopAllTCEMedia(){
  media = new Media();
  if(media !== null){
    media.pauseAllMedia();
  }
}

function tcePlayerOnMediaEnded(){
  isMediaEneded = true;
  tceVideoPlayerRef.tcePlayerOnMediaEnded("end");
}

function tcePlayerReplay(providedIframe){
  var iframeRef = null;
  if (providedIframe) {
    iframeRef = providedIframe;
  } else {
    var foundIframe = document.getElementsByTagName('iframe');
    iframeRef = document.getElementById(foundIframe[0].id);
  }
  var elmnt = iframeRef.contentWindow.document.getElementById('nav-playAgainButton');
  elmnt.click();

}

var tceVideoPlayerRef;
