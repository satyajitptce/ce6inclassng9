$(document).ready(function(){
	var screen = "lxlessonplan";
	var config = $().globalconfig();
	function screenhandler(handlerdata) {
		$("#preloader").loading();
		managerightmenu();
		$(".sitewidth.content").attr("screen",screen);
		$().showFilterContent(screen);
		$().showFilterCriteria(screen);
		$("#lessonplanbody .sitewidth.content").showChaterTpCards({onlayoutcomplete:showtpcardinfo});
	}
	function managerightmenu() {
		$("#lessonplanbody .menubg .menuright ul:nth-child(2)").remove();
		$("#lessonplanbody .menubg .menuright").append("<ul class=\"allthree\"><li><a href=\"javascript:void(0);\" class=\"createpre createplanbtn\">New Plan</a></li></ul>");
	}
	
	function showtpcardinfo() {
		$("#lessonplanbody .sitewidth.content .lxtiles .chapterrecommended").remove();
		$("#lessonplanbody .sitewidth.content .lxtiles .chaptercustom").hide();
		$("#lessonplanbody .sitewidth.content .lxtiles .chaptershared").hide();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid].teachingpoint [type]").hide();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid].teachingpoint [type='default']").show();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-'].teachingpoint [type='default']").hide();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-']:not([sharedtpnode]).teachingpoint [type='custom']").show();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-'][sharedtpnode].teachingpoint [type='shared']").show();
		$("#lessonplanbody .sitewidth.content .lxtiles [tpid]").each(function(){
			$(this).find("[type='default'],[type='custom'],[type='shared']").attr("tpresourceid",$(this).attr("tpid"));
		})
		$("#lessonplanbody .sitewidth.content .lxtiles").setFavoriteMark();
		$().applyScreenState();
		$("#preloader").loading("remove");
		$().updateBadgePoints();
	}
	$.fn.toggleLPCard = function() {
		var eventsource = $(this);
		$(eventsource).children(".sub-chapter-details").toggle();
		$(eventsource).find(".actionicons .editdelete.editopen").hide();
		if($(eventsource).children(".sub-chapter-details").is(":visible")) {
			$(eventsource).find(".actionicons .editdelete.editopen").show();
		}
		if($(eventsource).find(".sub-chapter-details .lesson-plan-inner").length > 0) {
			return;
		}
		if($(eventsource).closest("[type='default']").length > 0) {
			loadDefaultCardView({cardcontainer:$(eventsource).closest("[type='default']")});
		}
		if($(eventsource).closest("[type='custom']").length > 0) {
			loadDefaultCardView({eventsource:$(eventsource),cardcontainer:$(eventsource).closest("[type='custom']")});
		}
		if($(eventsource).closest("[type='shared']").length > 0) {
			loadDefaultCardView({eventsource:$(eventsource),cardcontainer:$(eventsource).closest("[type='shared']")});
		}
	}
	function loadDefaultCardView(options) {
		$(options.cardcontainer).find(".content-loader").loading();
		var tpid = $(options.cardcontainer).closest("[tpid]").attr("tpid");
		if(config.debug) console.log("show tp content: " + tpid);
		$().uploadUsage({resourceId:tpid,resourceType:"tp"});
		var sections = [];
		var unrefActivityIds = [];
		
		if(!tpid.startsWith("ctp-")) {
			$(options.cardcontainer).find(".title-boxshadow .actionicons .edit-lesson-plan").hide();
		}
		$(options.cardcontainer).find(".sub-chapter-details").loadlayout({layout:screen + "/tpdetails",onlayoutcomplete:function(){
			$().getlayout({layout:screen + "/activitycontainer",onserverresponse:function(activitycontainerlayout){
				$().loadTP({tpId:tpid,onserverresponse:function(xml){
					var contentContainer = $(options.cardcontainer).find(".sub-chapter-details .lesson-plan-inner");
					$(xml).find("section").each(function(){
						if($(this).text().trim() == "" || !$(this).attr("referenceId")) {
							return;
						}
						if($(this).attr("referenceId") == "PT" || $(this).attr("referenceId") == "CP" || $(this).attr("referenceId") == "MT") {
							return;
						}
						if($(this).text().trim() == "" || !$(this).attr("referenceId")) {
							return;
						}
						if($(this).attr("referenceId") == "PT" || $(this).attr("referenceId") == "CP" || $(this).attr("referenceId") == "MT") {
							return;
						}
						if($(this).find("subsection").length <= 0) {
							$(contentContainer).append("<div class=\"containerdiv-inner\"></div>");
							var sectionContentContainer = $(contentContainer).find(".containerdiv-inner:last-child");
							if(config.screens[screen].sectiontitles[$(this).attr("referenceId")]) {
								sections.push($(this).attr("referenceId"));
								$(sectionContentContainer).append(config.screens[screen].sectiontitles[$(this).attr("referenceId")].containerElement);
								$(sectionContentContainer).children(":first-child").text(config.screens[screen].sectiontitles[$(this).attr("referenceId")].title);
							}
							$(sectionContentContainer).append($(this).text());
						}
						if($(this).find("subsection").length > 0) {
							$(this).find("subsection").each(function(){
								$(contentContainer).append("<div class=\"containerdiv-inner\"></div>");
								var sectionContentContainer = $(contentContainer).find(".containerdiv-inner:last-child");
								$(sectionContentContainer).append($(this).find("content").text());
								var activityref = $(this).attr("activity_ref");
								var refactivityids = activityref.split(",");
								for(var i = 0; i < refactivityids.length; i++) {
									var actfound = false;
									$(sectionContentContainer).find("activity").each(function(){
										var curactivityid = $(this).attr("id");
										if(curactivityid == refactivityids[i].trim()) {
											actfound = true;
										}
									});
									if(!actfound) {
										unrefActivityIds.push(refactivityids[i].trim());
									}
								}
							});
						}
						$(options.cardcontainer).attr("sections",JSON.stringify(sections));
					});
					$(options.cardcontainer).find(".content-loader").loading("remove");
					
					$(contentContainer).find("activity").each(function(){
						if(!$(this).attr("id")) {
							return;
						}
						if($(this).closest(".activitylistcontainer").length > 0) {
							return;
						}
						if(!$(this).prev().hasClass("activitylistcontainer")) {
							$(this).before(activitycontainerlayout);
						}
						$(this).prev(".activitylistcontainer").append($(this));
					});
					if(config.debug) console.log("finding activitylistcontainers");
					$(contentContainer).find(".activitylistcontainer").each(function(){
						if(config.debug) console.log("activity list container found");
						var activitycontainer = $(this);
						var presentationobj = {initprocess:[],sourceobj:{presentation:[{sequenceVo:[]}]},resources:[],quizquestions:[],exercisequestions:[],whiteboards:[]};
						var counter = 1;
						$(activitycontainer).find("activity").each(function(){
							if(!$(this).attr("id")) {
								return;
							}
							presentationobj.sourceobj.presentation[0].sequenceVo.push({activityId:$(this).attr("id"),orderNo:counter});
							counter++;
						});
						if(config.debug) console.log("loading tp activities for presentationobj: " + JSON.stringify(presentationobj));
						$(activitycontainer).loadTPActivities({id:tpid,tpId:tpid,presentationobj:presentationobj,targetview:"inlineview"});
					})
					var defaultoptions = {id:tpid,type:"tceseq.json",dataType:"json",onserverresponse:function(tceseq){
						//tceseq = $.parseJSON(tceseq);
						try {
							tceseq = $.parseJSON(tceseq);
						} catch(err){
							
						}
						if(config.debug) console.log("unreferenced activity ids: " + JSON.stringify(unrefActivityIds));
						$(contentContainer).append("<div class=\"containerdiv-inner\"><h6>Activity List</h6></div>");
						$(contentContainer).find(".containerdiv-inner:last-child").append(activitycontainerlayout);
						
						var presentationobj = {initprocess:[],sourceobj:{presentation:[{sequenceVo:[]}]},resources:[],quizquestions:[],exercisequestions:[],whiteboards:[]};
						if(unrefActivityIds.length > 0) {
							if(config.debug) console.log("unrefActivityIds: " + unrefActivityIds);
							$.each(unrefActivityIds,function(index,activityId){
								presentationobj.sourceobj.presentation[0].sequenceVo.push({activityId:activityId,orderNo:(index+1)});
							});
						}
						if(config.debug) console.log("tools: " + tceseq.toolid);
						presentationobj.sourceobj.toolid = tceseq.toolid;
						var resourceoptions = {id:tpid,tpId:tpid,presentationobj:presentationobj,targetview:"inlineview"};
						$(contentContainer).find(".activitylistcontainer").last().loadTPActivities(resourceoptions);
						$(contentContainer).find(".activitylistcontainer").last().loadAdditionalTPResources(resourceoptions);
					}};
					$().fetchFileContents(defaultoptions);
					
					correctContent(contentContainer);
				}});
			}});
		}});
		
	}
	
	function correctContent(contentContainer/*,sections*/) {
		if(config.debug) console.log("correcting content");
		$.each(config.screens[screen].sectiontitles,function(key,value){
			/*if($(contentContainer).find("p." + key).length > 0) {
				sections.push(key);
			}*/
			$(contentContainer).find("p." + key).before(value.containerElement)
			$(contentContainer).find("p." + key).prev().text(value.title);
			$(contentContainer).find("p." + key).remove();
		});
		$(contentContainer).find("print").remove();
		$(contentContainer).find("media").remove();
		$(contentContainer).find(".in-notes").after("<div class=\"notes-review\"><span class=\"noofnotes note-flyout\"><span class='reviewnoteicon'></span></span><div class='inline-notes-flyout vscroll' style='display:none;'><ul class='clearfix  notes-main'></ul></div></div><br/>");
		$(contentContainer).find(".notes-review").hide();		
		$(contentContainer).find("[lptype='LO']").siblings("ul").addClass("bulletno").addClass("mb15").addClass("containerdiv-font");
		$(contentContainer).find("[lptype='TS']").siblings("ul,ol").eq(0).addClass("bulletno").addClass("mb15").addClass("containerdiv-font");
		$(contentContainer).find("activity[acttype='plugpoints']").before(config.screens[screen].sectiontitles["plugpoints"].containerElement);
		$(contentContainer).find("activity[acttype='plugpoints']").prev().text(config.screens[screen].sectiontitles["plugpoints"].title);
		$(contentContainer).find("activity[acttype='plugpoints'] ul").addClass("bulletno").addClass("mb15").addClass("containerdiv-font");
		/*$(contentContainer).find("[lptype='INTRO']").each(function(){
			console.log ($(this));
		});  */   
		$(contentContainer).correctCommonContent();
		$(contentContainer).find(".activitylistcontainer .unorderedlist").removeClass("unorderedlist");
		$(contentContainer).find(".activitylistcontainer .orderedlist").removeClass("orderedlist");
		$(contentContainer).find(".activitylist.unorderedlist").removeClass("unorderedlist");
		$(contentContainer).find(".activitylist .unorderedlist").removeClass("unorderedlist");
		$(contentContainer).find(".activitybox .unorderedlist").removeClass("unorderedlist");
		$(contentContainer).find("h6 .unorderedlist").removeClass("unorderedlist");
		
		var tpid = $(contentContainer).closest("[tpid]").attr("tpid");
		if(tpid) {
			$(contentContainer).updateTPImagePath({tpId:tpid});
		}
		
		$(contentContainer).find(".activities").remove();
		// 27 sept: Arijit: to support p tags with images
		$(contentContainer).find("p").each(function(){
			if($(this).closest("li.activitybox").length <= 0 && $(this).text().trim() == "" && $(this).find("*").length < 1) {
				$(this).remove();
			}			
			if(($(this).text().trim() == "Click the following Title to view the printable file.") || ($(this).text().trim() == "Click the following Thumbnail to view the media file.") || ($(this).text().trim() == "Activities") || ($(this).text().trim() == "Click the following Thumbnail to view the media file.Click the following Title to view the printable file.")){								
				$(this).remove();
			}
			if($(this).text().trim() == "Extensions") {
				$(this).remove();
			}
			if($(this).text().trim() == "Reinforcement") {
				$(this).remove();
			}
			if($(this).text().trim() == "Challenge") {
				$(this).remove();
			}
			// 15 Nov: Arijit - find container empty and remove it
			// except that contains images.
			var img = $(this).find("img"), // select images inside .container
			len = img.length;
			if(($(this).text().trim() == "") & (len == 0)) {
				$(this).remove();
			}
			
			if($(this).text().trim() == "Comprehension and Practice") {
				$(this).html("<b>Comprehension and Practice</b>");
			}
			if($(this).text().trim() == "Challenge and Application") {
				$(this).html("<b>Challenge and Application</b>");
			}

		});
		// 27 sept: Arijit: adding formatting to tables within lesson plans
		$(contentContainer).find("table").each(function(){
			$(this).css("background","#e5f2fd");
			$(this).css("margin","5px");
		});
		//$(contentContainer).find("br + br + br").remove();
		$(contentContainer).find(".containerdiv-inner").each(function(){
			var counter = -1;
			var containerdiv = $(this);
			var newcontainerdiv = $(containerdiv);
			// 27 sept: Arijit: remove Introduction header with Null
			if ($(this).find("p").text() == "Null"){
				$(this).remove();
			}
			$(this).children().each(function(){
				if($(this).prop("tagName") == "H6") {
					counter++;
					if($().showLog()) console.log("creating new containerdiv-inner, counter: " + counter);
					if(counter > 0) {
						if($().showLog()) console.log("creating new containerdiv-inner");
						$(newcontainerdiv).after("<div class=\"containerdiv-inner\"></div>");
						newcontainerdiv = $(newcontainerdiv).next();
					}
				}
				if(counter > 0) {
					var elem = $(this).detach();
					$(newcontainerdiv).append(elem);
				}
				
			});
		});
		
		var lptype = $(contentContainer).closest("[type]").attr("type");
		var userid = $().loggedinuser();
		$(contentContainer).find(".notes-review").hide();
		$().getnotesbytp({tpId:tpid,onserverresponse:function(object){
			if(config.debug) console.log("object: " + JSON.stringify(object));
			$().getlayout({layout:screen + "/note-item",onserverresponse:function(noteitemlayout){
				$.each(object.objects,function(index,noteobject){
					var data = null;
					try {
						data = $.parseJSON(noteobject.xml);
					} catch(err) {
						data = createNoteFromXML(noteobject);
					}
					var notescontainer = $(contentContainer).find("[sectiontype='" + data.section + "'] + .notes-review");
					$(notescontainer).find(".inline-notes-flyout .notes-main").append(noteitemlayout);
					var notecontainer = $(notescontainer).find(".inline-notes-flyout .notes-main > li:last-child");
					$(notecontainer).attr("noteid",noteobject.noteId);
					$(notecontainer).find(".notes-header").text(data.title);
					$(notecontainer).find(".detail-notes").text(data.message);
					$(notecontainer).find(".notes-info .name").text(data.userName);
					if(noteobject.userId != userid) {
						$(notecontainer).find(".notes-options").hide();
					}
					$(notescontainer).show();
				});
				$(contentContainer).find("[sectiontype] + .notes-review").each(function(){
					var sectionnotecount = $(this).find(".inline-notes-flyout .notes-main > li").length;
					$(this).find(".noofnotes").append((sectionnotecount < 10? "0":"") + sectionnotecount)
				})
			}});
		}})
		$(contentContainer).find("li[activityid]").each(function(){
			if($().showLog()) console.log("activity id: " + $(this).attr("activityid") + ", parent: " + $(this).parent().prop("tagName"));
			if($(this).parent().prop("tagName") != "UL") {
				if($(this).prev().prop("tagName") != "UL") {
					$(this).before("<ul class='bulletno1 containerdiv-font refactivitylist'></ul>");
				}
				if($(this).prev().prop("tagName") == "UL") {
					$(this).prev().append($(this));
				}
			}
		});
		var sections = []
		$(contentContainer).find("[sectiontype]").each(function(){
			if(config.debug) console.log("section type: " + $(this).attr("sectiontype") + ": " + $(this).text());
			sections.push($(this).attr("sectiontype"));
		});
		$(contentContainer).closest("[sections]").attr("sections",JSON.stringify(sections));
		if(config.debug) console.log("sections: " + $(contentContainer).closest("[sections]").attr("sections"));
		
	}
	function createNoteFromXML(noteobj) {
		if(config.debug) console.log("creating note object from xml");
		var data = $.parseXML(noteobj.xml);
		var notedata = new Object();
		notedata.chapterId = noteobj.chapterId;
		notedata.gradeId = noteobj.gradeId;
		notedata.subjectId = noteobj.subjectId;
		notedata.tpId = noteobj.tpId;
		notedata.sharewith = "" + noteobj.isShared;
		notedata.userId = noteobj.userId;
		notedata.noteId = noteobj.noteId;
		notedata.section = "LO";
		$(".popupdialog3").empty();
		if(config.debug) console.log("finding note object content from xml: " + $(data).find("content").length + ", text: " + $(data).find("content").text());
		$(".popupdialog3").html($(data).find("content").text());
		notedata.userName = $(data).find("note").attr("user");
		notedata.title = $(".popupdialog3").find("data title").text();
		notedata.message = $(".popupdialog3").find("data TextFlow").text();
		
		$().storenote({noteobj:notedata,onserverresponse:function(response){
			if(config.debug) console.log("store note response: " + response + ", " + JSON.stringify(response));
		}});
		
		return notedata;
	}
	
	$.fn.showAddNoteDialog = function() {
		var eventsource = $(this);
		var tpid = $(eventsource).closest("[tpid]").attr("tpid");
		var noteid = $(eventsource).closest("[noteid]").attr("noteid");
		var tpcontainer = $(eventsource).closest("[type]");
		$(".activitypopupdialog").showDialog({height: "auto",width:430,layout:screen + "/noteform",onlayoutcomplete:function(){
			$().showSharingOptions({container:$(".activitypopupdialog .share-block")});
			$(".radio input[type='radio']").iCheck({
				radioClass: 'iradio_minimal-purple'
			});
			var sections = $.parseJSON($(tpcontainer).attr("sections"));
			var sectionconfigurations = [];
			$.each(sections,function(index,sectionid){
				var sectionconfig = config.screens[screen].sectiontitles[sectionid];
				if(sectionconfig) {
					sectionconfigurations.push({value:sectionid,label:sectionconfig.title});
				}
			})
			$(".activitypopupdialog .dialog-content").attr("tpid",tpid);
			$(".activitypopupdialog .dialog-content").attr("type",$(tpcontainer).attr("type"));
			$(".activitypopupdialog select[name='class']").populateDropDown({collection:sectionconfigurations,valuefield:"value",labelfield:"label"});
			if(config.debug) console.log("edit note with id: " + noteid);
			if(noteid) {
				var dialogcontent = $(".activitypopupdialog").find(".dialog-content");
				$().getnotesbytp({tpId:tpid,onserverresponse:function(object){
					if(config.debug) console.log("notes by tp: " + JSON.stringify(object));
					$.each(object.objects,function(index,noteobj){
						if(config.debug) console.log("noteobj: " + JSON.stringify(noteobj));
						if(noteobj.noteId == noteid) {
							var data = $.parseJSON(noteobj.xml);
							$(dialogcontent).attr("noteid",noteid);
							$(dialogcontent).find("input[name='title']").val(data.title);
							$(dialogcontent).find("select[name='class']").val(data.section);
							$(dialogcontent).find("textarea[name='message']").val(data.message);
							$(dialogcontent).find(".share-block input[value='" + data.sharewith + "']").iCheck("check");
							$(".activitypopupdialog select[name='class']").attr("disabled","disabled")
						}
					});
				}});
			}
		}});
	}
	$.fn.saveLessonPlanNote = function() {
		var eventsource = $(this);
		var dialogcontent = $(eventsource).closest(".dialog-content");
		var tpid = $(dialogcontent).attr("tpid");
		var noteid = $(dialogcontent).attr("noteid");
		var note = {};
		note.chapterId = $().chapter();
		var schoolclass = $().schoolclass();
		note.gradeId = schoolclass.gradeId;
		note.subjectId = schoolclass.subjectId;
		note.tpId = tpid;
		if(noteid) note.noteId = noteid;
		var isvalid = $(dialogcontent).find("input[name='title']").validateRequiredField();
		isvalid = $(dialogcontent).find("input[name='title']").validateFieldLength(245);
		if(!isvalid) {
			$().showNotification("Title is a required field and its length cannot be more than 245");
			return;
		}
		note.title = $(dialogcontent).find("input[name='title']").val();
		note.section = $(dialogcontent).find("select[name='class']").val();
		note.message = $(dialogcontent).find("textarea[name='message']").val();
		note.sharewith = $(dialogcontent).find(".share-block input[name='share']:checked").val();
		$().storenote({noteobj:note,onserverresponse:function(response){
			if(config.debug) console.log("store note response: " + response + ", " + JSON.stringify(response));
			if(!noteid) {
				$().notifyObjectCreation({resourceId:response.object.noteId,resourceType:"notes"});
			}
			$(eventsource).closest(".bottombuttons").find(".dialog-close").trigger("click");
			$().changescreen(screen);
		}});
	}
	$.fn.showLPNoteDeleteConfirmationPopup = function() {
		var eventsource = $(this);
		var noteid = $(eventsource).closest("[noteid]").attr("noteid");
		$(".activitypopupdialog").showDialog({width: 440, left: 290, top: -141,layout:screen + "/deletenote-confirmation",onlayoutcomplete:function(){
			$(".activitypopupdialog").find(".dialog-content").attr("noteid",noteid);
		}});
	}
	$.fn.deleteNote = function() {
		var eventsource = $(this);
		var noteid = $(eventsource).closest("[noteid]").attr("noteid");
		$().deleteNode({noteId:noteid,onserverresponse:function(response){
			if(response.status == "success") {
				$().changescreen(screen);
			}
		}})
	}
	$.fn.printLessonPlan = function() {
		var eventsource = $(this);
		var tpId = $(eventsource).closest("[tpid]").attr("tpid")
		$().uploadUsage({resourceId:tpId,resourceType:"printprinted"});
		var tpTitle = $(eventsource).closest("[tpid]").find(".quiz-headerbg > .quiz-header.chaptertitle").text();
		tpTitle = tpTitle.replace("<P>","").replace("</P>","");
		//var printcontainer = $(eventsource).closest("[type='default'].tilesopen").find(".lesson-plan-inner");
		var printcontainer = $(eventsource).closest("[type].tilesopen").find(".lesson-plan-inner");
		$(printcontainer).printContent({id:tpId,title:tpTitle,reportitemselector:"div.containerdiv-inner"
										,reportitemtitleselector:"div.containerdiv-inner > h6",printtype:"lessonplan"});
	}
	$.fn.showCreatePlanDialog = function(options) {
		var eventsource = $(this);
		$(eventsource).showCreatePresentationDialog({presentationtype:"lessonplanview",presentationformoptions:{onformloadcomplete:function(options){
			if(config.debug) console.log("create lesson plan dialog created...");
			//$(".activitypopupdialog .createpresentation .activityleft .createoptions > li:nth-child(1)").hide();
			//$(".activitypopupdialog .createpresentation .activityleft .createoptions > li:nth-child(3)").hide();
			$(".activitypopupdialog .createpresentation .activityleft .createoptions").hide();
		}}});
	}
	$.fn.showEditPlanDialog = function(options) {
		var eventsource = $(this);
		var tpid = $(eventsource).closest("[tpid]").attr("tpid");
		$().fetchFileContents({id:tpid,type:"tceseq.json",onserverresponse:function(filecontents){
			if(config.debug) console.log("load custom tp: " + filecontents);
			//var tceseq = $.parseJSON(filecontents);
			var tceseq = filecontents;
			try {
				tceseq = $.parseJSON(filecontents);
			} catch(err){
				
			}
			var lessonplanitem = tceseq.presentation[0];
			$(eventsource).showCreatePresentationDialog({presentationtype:"lessonplanview",presentationformoptions:{onformloadcomplete:function(options){
				$(".activitypopupdialog .dialog-content").populateLessonPlanitem({containerselector:".activitypopupdialog .dialog-content .activityright .resourceitem",lessonplanitem:lessonplanitem});
				$(".activitypopupdialog .dialog-content .sharewith .share-left [name='share'][value='" + lessonplanitem.sharewith + "']").iCheck("check");
				$(".activitypopupdialog .createpresentation .activityleft .createoptions").hide();
				$(".activitypopupdialog .dialog-content .sharewith .share-right button.deletemsg").show();
			}}});
		}})
		
	}
	$.fn.confirmTPDelete = function(options) {
		var eventsource = $(this);
		var tpId = $(eventsource).closest(".dialog-content.createpresentation").find(".activityleft [name='tpId']").val();
		if(tpId) {
			$(".popupdialog2").showDialog({width: 440, left: 290, top: -141,layout:"deleteitem-confirmation",onlayoutcomplete:function(){
				$(".popupdialog2").find(".dialog-content").attr("tpid",tpId);
				$(".popupdialog2 .deletenoteconfirmbtn").addClass("deletetpconfirmbtn").removeClass("deletenoteconfirmbtn");
			}});
		}
	}
	
	$().attachScreenHandler(screen,screenhandler);
})