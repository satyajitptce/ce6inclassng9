$(document).ready(function(){

	var config = $().globalconfig();
	var loadingarr = {};
	$.fn.loading = function(options) {
		var container = $(this);
		var identifier = $(container).attr("class");
		if(!identifier) {
			identifier = "#preloader";
		}
		if(!loadingarr[identifier]) {
			loadingarr[identifier] = [];
		}
		if(options == "remove") {
			loadingarr[identifier].pop();
			if(config.debug) console.log("removing loader: " + identifier + ", " + loadingarr[identifier].length);
			if(loadingarr[identifier].length == 0) {
				$(container).hide();
				delete loadingarr[identifier];
			}
			return;
		}
		if(options == "clear") {
			delete loadingarr[identifier];
			$(container).hide();
			return;
		}
		if(loadingarr[identifier].length == 0) {
			$(container).show();
		}
		loadingarr[identifier].push(1);
		if(config.debug) console.log("added loader: " + identifier + ", " + loadingarr[identifier].length);
	}
	$.fn.showUserBadgeLogs = function() {
		$(".activitypopupdialog").showDialog({width:1080,height: 600,layout:"userbadgelogpopup",onlayoutcomplete:function(){
			$().getuserbadgelogs({onserverresponse:function(response){
				var pointconfigurations = {};
				$.each(response.configurations,function(configindex,configuration){
					pointconfigurations[configuration.id] = configuration;
				})
				var container = $(".activitypopupdialog .logview tbody");
				var date = null;
				$.each(response.badgeLogs,function(logindex,log){
					if(config.debug) console.log("point config id: " + JSON.stringify(log));
					if(!pointconfigurations[log.pointConfigId]) {
						return;
					}
					date = new Date(log.creditedOn);
					$(container).append("<tr>" +
							"<td>" + pointconfigurations[log.pointConfigId].label + "</td>" +
							"<td>" + log.points + "</td>" +
							"<td>" + log.resourceId + "</td>" +
							"<td>" + $.datepicker.formatDate("dd-mm-yy",date) + " " + date.getHours() + ":" + date.getMinutes() + "</td>" +
							//"<td>" + log.resourceType + "</td>" +
							//"<td><div>" + JSON.stringify(pointconfigurations[log.pointConfigId],function(key,value){if(value == null) return undefined; return value;}) + "</div></td>" +
							"</tr>");
				})
				$(".activitypopupdialog .vscroll").css("height","500px");
				$(".activitypopupdialog .vscroll").mCustomScrollbar({
					theme:"rounded-dots",
					scrollInertia:400
				});
			}});
		}});
	}
	$.fn.updateBadgePoints = function() {
		$().loaduserbadge({onserverresponse:function(response){
			if(config.debug) console.log("user badge: " + JSON.stringify(response));
			if($(".header .mainheader .rightmenu > li").length > 2) {
				$(".header .mainheader .rightmenu > li:nth-child(1)").remove();
			}
			if(response.badge != null) {
				$(".header .mainheader .rightmenu .badge-list span").text(response.badge.points);
			}
			$(".header .mainheader .rightmenu .badge-list img").attr("src",config.badges.iconpathprefix + response.config.icon);
		}});
	}
	$.fn.applyScreenFilter = function(screen) {
		if(config.debug) console.log("applying screen filter...");
		if(!screen) {
			screen = $().currentScreen();
		}
		var isfavouriteselected = false;
		$("#lessonplanbody .filterposition .filters .filterselection input[name='view']").each(function(){
			var ischecked = $(this).iCheck("update")[0].checked;
			if(config.debug) console.log("filter: " + $(this).attr("name") + ", val: " + $(this).val() + ", ischecked: " + ischecked);
			var criteriael = $("#lessonplanbody .sitewidth.content .filterselected li[value='" + $(this).val() + "']").hide();
			if(ischecked) {
				var criteriael = $("#lessonplanbody .sitewidth.content .filterselected li[value='" + $(this).val() + "']").show();
				if($(this).val() == "favourite") {
					isfavouriteselected = true;
					if(screen == "lxquiz" || screen == "lxlessonplan") {
						if(config.debug) console.log("handle fav for lxquiz: ");
						$("#lessonplanbody .sitewidth.content [resourceid] .fav").not(".selected").closest("[resourceid]").hide();
					}
					return;
				}
				if(config.debug) console.log("showing " + $("#lessonplanbody .sitewidth.content [view='" + $(this).val() + "']").not("[resourcecount='0']").length + " items");
				$("#lessonplanbody .sitewidth.content [view='" + $(this).val() + "'][resourcecount='0']").hide();
				$("#lessonplanbody .sitewidth.content [view='" + $(this).val() + "']").not("[resourcecount='0']").show();
				if(screen == "lxlessonplan") {
					$("#lessonplanbody .sitewidth.content .lxtiles .chaptercustom").hide();
					$("#lessonplanbody .sitewidth.content .lxtiles .chaptershared").hide();
					$("#lessonplanbody .sitewidth.content .lxtiles [tpid].teachingpoint [type].tilesopen").hide();
					$("#lessonplanbody .sitewidth.content .lxtiles [tpid].teachingpoint [type='default']").show();
					$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-'].teachingpoint [type='default']").hide();
					$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-']:not([sharedtpnode]).teachingpoint [type='custom']").show();
					$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-'][sharedtpnode].teachingpoint [type='shared']").show();
				}
				if(isfavouriteselected) {
					if(config.debug) console.log("is favourite selected: " + isfavouriteselected);
					$("#lessonplanbody .sitewidth.content [view='" + $(this).val() + "'][resourceid] .fav").not(".selected").closest("[resourceid]").hide();
				}
				return;
			}
			$("#lessonplanbody .sitewidth.content [view='" + $(this).val() + "']").hide();
			if($(this).val() == "favourite") {
				if(screen == "lxquiz" || screen == "lxlessonplan") {
					if(config.debug) console.log("handle fav for lxquiz: ");
					$("#lessonplanbody .sitewidth.content [resourceid] .fav").not(".selected").closest("[resourceid]").show();
				}
				return;
			}
		});
		$().showFilterCriteria(screen);
	}
	$.fn.showChaterTpCards = function(options) {
		var defaultoptions = {onlayoutcomplete:function(){
								$("#lessonplanbody .sitewidth.content .lxtiles [tpid^='ctp-'].teachingpoint [type='default']").hide();
								}};
		var funcoptions = $.extend(defaultoptions,options);
		var screen = $(this).attr("screen");
		$().getlayout({layout:screen + "/card",onserverresponse:function(cardlayout){
			$("#lessonplanbody .sitewidth.content .lxtiles .tilesopen").html(cardlayout)
			$("#lessonplanbody .sitewidth.content .lxtiles [type].tilesopen .title-boxshadow").each(function(){
				var cardconfig = config.screens[screen].cards[$(this).closest("[type]").attr("type")];
				$(this).find(".tile-titles h6").text(cardconfig.title);
				$(this).find(".formaticons").removeClass().addClass("formaticons " + cardconfig.bgcolor);
				$(this).find(".formaticons span").removeClass().addClass(cardconfig.iconclass);
				$(this).find(".formaticons p").text(cardconfig.icontext? cardconfig.icontext : "");
				$(this).closest("[type]").show();
			});
			funcoptions.onlayoutcomplete();
			if(config.debug) console.log("actionicons container: " + $("#lessonplanbody .sitewidth.content .lxtiles [type='default'].tilesopen .actionicons li:gt(1)").length);
			$("#lessonplanbody .sitewidth.content .lxtiles [type='default'].tilesopen .actionicons").each(function(){
				$(this).find("li:gt(0)").show();
			});
			//$("#lessonplanbody .sitewidth.content .lxtiles [type='default'].tilesopen .actionicons > li:gt(0)").show();
		}});
	}
	$.fn.showDialog = function(options) {
		var container = $(this);
		var defaultoptions = {autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width:940};
		var dialogoptions = $.extend(defaultoptions,options);
		dialogoptions.onlayoutcomplete = function(){
			$(container).attr("screen",dialogoptions.screen);
			var dialoginstance = undefined;
			try {
				dialoginstance = $(container).dialog( "instance" );
			} catch(e) {
				
			}
			if(dialoginstance == undefined) {
				$(container).dialog(dialogoptions);
				$(container).dialog("open");
			}
			if(dialoginstance != undefined) {
				$(container).dialog("open");
			}
			if(options.onlayoutcomplete) {
				options.onlayoutcomplete();
			}
		}
		$(container).loadlayout(dialogoptions);
	}
	
	$.fn.markActive = function(containerselector,activeelementselector,activeclass) {
		var eventsource = $(this); 
		if(!activeclass) {
			activeclass = "active";
		}
		var config = $().globalconfig();
		if(config.debug) {
			var container = $(eventsource).closest(containerselector).find("." + activeclass);
			console.log("existing active node: " + $(container).length + ", nodeid: " + $(container).attr("nodeid"));
		}
		$(eventsource).closest(containerselector).find("." + activeclass).removeClass(activeclass);
		if(!activeelementselector) {
			$(eventsource).addClass(activeclass);
			return;
		}
		$(eventsource).closest(activeelementselector).addClass(activeclass);
	}
	$.fn.correctCommonContent = function(options) {
		console.log("correctCommonContent");
		var container = $(this);
		$(container).find("ul").addClass("unorderedlist");
		$(container).find("ol").addClass("orderedlist");
		// 04 Oct 18: added to show MathML images
		//$(contentContainer).find("br + br").remove();
		//$(contentContainer).find("br + br + br").remove();
		//$(container).replace(/(<br\s*\/?>){3,}/gi, '<br>');	
		//$(container).replace("br+br","");
		//$(container).replace("br+br+br","");
		// 04 Oct 18: replace the below 2 lines as it was removing all <br> tags
		$(container).find("br:lt(3)").remove();	
		$(container).find("br:lt(2)").remove();
		//$(container).find("br+br+br").remove();	// commented on 4 oct 18	
		//$(container).find("br+br").remove(); // commented on 4 oct 18
		$(container).find("ol > br").remove();
		$(container).find("ul > br").remove();
		$(container).find("li").each(function(){
			if($(this).closest("li[activityid]").length > 0) {
				return;
			}
			var htmlcontent = $(this).html();
			htmlcontent = htmlcontent.replace("&nbsp;","");
			if(htmlcontent.trim() == "") {
				$(this).remove();
			}
		})
		$(container).find("p").each(function(){
			if($(this).closest("li[activityid]").length > 0) {
				return;
			}
			//27 Sept: Arijit: p tag with images
			 var text = $(this).text().replace("&nbsp","");
			if((text.trim() == "") && ($(this).find("*").length < 1)) {
				$(this).remove();
			}
		})
	}
	$.fn.updateTPImagePath = function(options) {
		var container = $(this);
		$().fetchDLFilePath({id:options.tpId,type:"DUMMY",onserverresponse:function(filepath){
			if(config.debug) console.log("filepath");
			$(container).find("img[src]").each(function(){
				var imgelement = $(this);
				var imgsrc = $(this).attr("src");
				if (imgsrc.split("data:image/").length <= 1){
					var src = $(this).attr("src").replace(".swf",".png");
					$(imgelement).attr("src","/delegate/fileservice/" + filepath.replace("DUMMY",src));
				}
			});
		}});
	}
	
	$.fn.captureScreenState = function() {
		var screen = $().currentScreen();
		var screenstate = $().screenState();
		if(!screenstate) {
			screenstate = {};
		}
		/*
		if(!screenstate[screen]) {
			screenstate[screen] = {filters:[],opencards:[],closedcards:[]};
		}*/
		screenstate[screen] = {filters:[],opencards:[],closedcards:[]};
		$("#lessonplanbody [screen='" + screen + "'] .filterselected > li").each(function(){
			screenstate[screen]["filters"].push($(this).attr("value"));
		});
		$("#lessonplanbody [screen='" + screen + "'] .contentscroll .lxtiles .tilesopen").each(function(){
			if(screen == "lxquiz" && !$(this).find(".subchaptersdivider").is(":visible")) {
				var obj = {type:$(this).attr("type")};
				if($(this).closest("[tpid]")) {
					obj.tpid = $(this).closest("[tpid]").attr("tpid");
				}
				screenstate[screen].closedcards.push(obj);
				return;
			}
			if(!$(this).find(".sub-chapter-details").is(":visible")) {
				return;
			}
			var obj = {type:$(this).attr("type")};
			if($(this).closest("[tpid]")) {
				obj.tpid = $(this).closest("[tpid]").attr("tpid");
			}
			if(screen == "lxsearchresult") {
				obj.gradeid = $(this).closest("[gradeid]").attr("gradeid");
				obj.subjectid = $(this).closest("[subjectid]").attr("subjectid");
			}
			screenstate[screen].opencards.push(obj);
		});
		$().screenState(screenstate);
	}
	$.fn.applyScreenState = function() {
		var screen = $().currentScreen();
		var screenstate = $().screenState();
		if(!screenstate || !screenstate[screen]) {
			return;
		}
		var selector = "";
		if(screenstate[screen]["opencards"]) {
			$.each(screenstate[screen]["opencards"],function(index,card){
				selector = "#lessonplanbody [screen='" + screen + "'] .contentscroll .lxtiles";
				if(card.tpid) {
					selector += " [tpid='" + card.tpid + "']";
				}
				if(screen == "lxsearchresult") {
					if(card.gradeid) {
						selector += " [gradeid='" + card.gradeid + "']";
					}
					if(card.subjectid) {
						selector += "[subjectid='" + card.subjectid + "']";
					}
				}
				selector += " [type='" + card.type + "'].tilesopen";
				if(!card.tpid && screen != "lxsearchresult") {
					selector += ".chaptercustom";
				}
				if($().showLog()) console.log("handling open cards state selector: " + selector + ", count: " + $(selector).length);
				if(!$(selector).find(".sub-chapter-details").is(":visible")) {
					$(selector).click();
				}
			})
		}
		if(screen == "quiz" && screenstate[screen]["closedcards"]) {
			$.each(screenstate[screen]["closedcards"],function(index,card){
				selector = "#lessonplanbody [screen='" + screen + "'] .contentscroll .lxtiles";
				if(card.tpid) {
					selector += " [tpid='" + card.tpid + "']";
				}
				selector += " [type='" + card.type + "'].tilesopen";
				if(!card.tpid) {
					selector += ".chaptercustom";
				}
				selector += " > .clearfix";
				if($().showLog()) console.log("handling closed cards state selector: " + selector + ", count: " + $(selector).length);
				$(selector).click();
			})
		}
	}
	
	$.fn.showFilterContent = function(screen) {
		if(config.debug) console.log("show filters for screen: " + screen);
		$("section.filterposition .filters").empty();
		$("section.filterposition .filters").hide();
		var container = $("section.filterposition .filters");
		var itemcontainer = null;
		$.each(config.screens[screen].filters,function(index,filter){
			if(index > 0) {
				container = $("section.filterposition .filters");
				$(container).append("<div class=\"filtersdivider\"></div>");
				container = $(container).find(".filtersdivider");
			}
			$(container).append("<div class=\"filters-inner\"></div>");
			container = $(container).find(".filters-inner");
			$(container).append("<p class=\"sideheading\">" + filter.title + "</p>");
			$(container).append("<ul class=\"filterselection\"></ul>");
			itemcontainer = $(container).find(".filterselection");
			if(index == 0) {
				$(container).append("<button type=\"reset\" value=\"Reset\" class=\"resetbtn resetpos\">Reset</button>");
			}
			$.each(filter.items,function(itemindex,item){
				$(itemcontainer).append("<li>"
							            + "<label class=\"checkbox\">"
							            + "<input type=\"checkbox\" name=\"view\" " + (item.defaultchecked? "checked=\"\"" : "") + " value=\"" + item.value + "\" style=\"position: absolute; opacity: 0;\"/>" + item.title
							            + "</label>"
							            + "</li>");
			})
		});
		$("section.filterposition .filters .checkbox input[type='checkbox']").iCheck({
			checkboxClass: 'icheckbox_minimal-purple',
		});
	}

	$.fn.hidetoplayers = function() {
		hidefilters();
	}
	function hidefilters() {
		$("#lessonplanbody .menubg .menuright .filterclick").removeClass("active");
		$("#lessonplanbody .filterposition .filters").hide()
	}

	
	$.fn.showFilters = function() {
		var eventsource = $(this);
		$('.filters').slideToggle();
		$(eventsource).toggleClass("active");
	}
	
	$.fn.showFilterCriteria = function(screen) {
		var container = $("[screen=\"" + screen + "\"].sitewidth.content .filterselected");
		$(container).empty();
		if(config.debug) console.log("selected filter criteria container: " + $(container).length + ", for screen: " + screen);
		var allfound = false;
		$("section.filterposition .filters .filters-inner ").each(function(){
			allfound = false;
			$(this).find("label.checkbox input").each(function(){
				if(config.debug) console.log("allfound: " + allfound + ", val: " + $(this).val() + ", is checked: " + $(this).iCheck("updated")[0].checked);
				if(!allfound && $(this).iCheck("updated")[0].checked) {
					$(container).append("<li value=\"" + $(this).val() + "\">" + $(this).closest("label").text() + " <span></span></li>");
				}
				if(!allfound) {
					allfound = $(this).val() == "all" && $(this).iCheck("updated")[0].checked;
				}
			});
		});
		
		/*if($().showLog()) console.log("show filter criteria for screen: " + screen);
		var container = $("[screen=\"" + screen + "\"].sitewidth.content .filterselected");
		$(container).removeChildren();
		var allfound = false;
		var allvaluefound = false;
		var favselected = false;
		
		$("section.filterposition .filters label.checkbox input").each(function(){
			var ischecked = $(this).attr("checked") != null;
			var value = $(this).val();
			if($().showLog()) console.log("val: " + value + ", ischecked: " + ischecked);
			if(value == "all") {
				allvaluefound = true;
			}
			if(!allfound && ischecked) {
				$(container).append("<li value=\"" + value + "\">" + $(this).closest("label").text() + " <span></span></li>");
				allfound = value == "all";
			}
			if(ischecked) {
				$("#lessonplanbody .sitewidth.content [type='" + value + "'].tilesopen").show();
				
				
				//$("#lessonplanbody .sitewidth.content [type='" + value + "'].tilesopen").show();
				$("#lessonplanbody .sitewidth.content li[activitytype='" + value + "']").show();
				$("#lessonplanbody .sitewidth.content [type='" + value + "'][resourcecount='0'].tilesopen").hide();
				if(favselected) {
					$("#lessonplanbody .sitewidth.content li[activityid]:not([favourite])").hide();
				}
				if(value == "favourite") {
					favselected = true;
					$("#lessonplanbody .sitewidth.content li[presentationid]:not([favourite])").hide();
					if($().showLog()) console.log("non favourite activities: " + $("#lessonplanbody .sitewidth.content li[activityid]:not([favourite])").length);
					$("#lessonplanbody .sitewidth.content li[activityid]:not([favourite])").hide();
					$("#lessonplanbody .sitewidth.content tr[quid]:not([favourite])").hide();
				}
			}
			if(!ischecked) {
				$(container).find("li[activityid='" + value +"']").hide();
				$("#lessonplanbody .sitewidth.content [type='" + value + "'].tilesopen").hide();
				$("#lessonplanbody .sitewidth.content li[activityid][activitytype='" + value + "']").hide();
				if(!favselected) {
					$("#lessonplanbody .sitewidth.content li[activityid]:not([favourite])").show();
				} 
				if(value == "favourite") {
					favselected = false;
					$("#lessonplanbody .sitewidth.content li[presentationid]:not([favourite])").show();
					$("#lessonplanbody .sitewidth.content li[activityid]:not([favourite])").show();
					$("#lessonplanbody .sitewidth.content tr[quid]:not([favourite])").show();
				}
				if(allfound) {
					$("section.filterposition .filters label.checkbox input[value='all']").removeAttr("checked");
					$("section.filterposition .filters label.checkbox input[value='all']").closest("div").removeClass("checked");
				}
			}
		})
		if(!allfound) {
			$("section.filterposition .filters label.checkbox input[value='all']").removeAttr("checked");
			$("section.filterposition .filters label.checkbox input[value='all']").closest("div").removeClass("checked");
		}
		var allchecked = true;
		$("section.filterposition .filters label.checkbox input[value='all']").closest("li").nextAll().each(function(){
			if($(this).find("input[checked]").length <= 0) {
				allchecked = false;
			}
		})
		if(allchecked) {
			$("section.filterposition .filters label.checkbox input[value='all']").iCheck("check");
		}*/
	}
	
	
	$.fn.filterFavourite = function(eventsource) {
		$(".tilesopen").hide();
		$(".fav.selected").closest(".tilesopen").show();
		$(".filterselected .nodelete").removeChildren();
		$(".filterselected .nodelete").html("Favourite <span></span>");
	}
	
	$.fn.filterDefault = function(eventsource) {
		$(".tilesopen").show();
		$(".filterselected .nodelete").removeChildren();
		$(".filterselected .nodelete").html("Default <span></span>");
	}
	
	$.fn.showNextActivityInPopup = function(eventsource) {
		var currentid = $(".activitypopupdialog").attr("activityid");
		var nextactivityid = $(".activitypopupdialog").attr("nextactivityid");
		var presentationid = $(".activitypopupdialog").attr("presentationid");
		var cardtype = $(".activitypopupdialog").attr("cardtype");
		if(nextactivityid) {
			$().showActivitySnapshot({presentationid:presentationid,activityid:nextactivityid,changeLanguages:true,onsnapshotloaded:function(){
				$(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection > li[langid]:first-child input[name='lang']").iCheck("check");
			}});
			$(".activitypopupdialog").removeAttr("activityid");
			$(".activitypopupdialog").attr("activityid",nextactivityid);
			$(".activitypopupdialog").removeAttr("prevactivityid");
			$(".activitypopupdialog").attr("prevactivityid",currentid);
			if($().showLog()) console.log("current id: " + currentid + ", next activity id: " + nextactivityid);
			if($().showLog()) console.log("next activity id selector: " + (".lxtiles li[tpid='" + presentationid + "'] .tiles [type='" + cardtype + "'].tilesopen .sub-chapter-details .sub-chapters li[activityid='"+ nextactivityid +"']"));
			var newnextactivityid = $(".lxtiles li[tpid='" + presentationid + "'] .tiles [type='" + cardtype + "'].tilesopen .sub-chapter-details .sub-chapters li[activityid='"+ nextactivityid +"']").next().attr("activityid");
			$(".activitypopupdialog").removeAttr("nextactivityid");
			$(".activitypopupdialog").attr("nextactivityid",newnextactivityid);
		}
	}
	$.fn.showActivityInLanguage = function(options) {
		var primlang = $(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection > li[langid]:first-child").find("input[name='lang']").val();
		if($().showLog()) console.log("primary language: " + primlang);
		var presentationid = options.presentationid;
		var activityid = options.activityid;
		if(options.language != primlang) {
			presentationid = presentationid + "_" + options.language;
			activityid = activityid + "_" + options.language;
		}
		$().showActivitySnapshot({presentationid:presentationid,activityid:activityid,changeLanguages:false});
	}
	
	$.fn.showPrevActivityInPopup = function(eventsource) {
		
		var currentid = $(".activitypopupdialog").attr("activityid");
		var prevactivityid = $(".activitypopupdialog").attr("prevactivityid");
		var presentationid = $(".activitypopupdialog").attr("presentationid");
		var cardtype = $(".activitypopupdialog").attr("cardtype");
		if(prevactivityid) {
			$().showActivitySnapshot({changeLanguages:true,presentationid:presentationid,activityid:prevactivityid,onsnapshotloaded:function(){
				$(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection > li[langid]:first-child input[name='lang']").iCheck("check");
			}});
			$(".ui-dialog .activitypopupdialog").removeAttr("activityid");
			$(".ui-dialog .activitypopupdialog").attr("activityid",prevactivityid);
			$(".activitypopupdialog").removeAttr("nextactivityid");
			$(".activitypopupdialog").attr("nextactivityid",currentid);
			if($().showLog()) console.log("current id: " + currentid + ", prev activity id: " + prevactivityid);
			if($().showLog()) console.log("next activity id selector: " + (".lxtiles .tiles .tilesopen .sub-chapter-details .sub-chapters li[tpid='" + presentationid + "'] [type='" + cardtype + "'].tilesopen li[activityid='"+ prevactivityid +"']"));
			
			var newprevactivityid = $(".lxtiles li[tpid='" + presentationid + "'] .tiles [type='" + cardtype + "'].tilesopen .sub-chapter-details .sub-chapters li[activityid='"+ prevactivityid +"']").prev().attr("activityid");
			$(".activitypopupdialog").removeAttr("prevactivityid");
			$(".activitypopupdialog").attr("prevactivityid",newprevactivityid);
		}
	}
	$.fn.showLanguages = function(options) {
		var container = $(this);
		var schoolsettings = $().localschoolsettings();
		if(config.debug) console.log("schoollanguages: " + JSON.stringify(schoolsettings));
		var sortedlanguages = $().sortObjectArray(schoolsettings.schoolLanguages,"orderBy");
		$.each(sortedlanguages,function(langindex,language){
			$(container).append("<li langid='" + language.lanUId + "' style='display:none'><label class=\"radio\"><input type=\"radio\" name=\"lang\" value='" + language.code + "'>" + language.name + "</label></li>");
		});
		$(container).find(".radio input[type='radio']").iCheck({
			radioClass: 'iradio_minimal-purple',
		});
	}
	/*
	$.fn.showLanguages = function(options) {
		var container = $(this);
		$().getSchoolSettings({onserverresponse:function(settings){
			$(container).empty();
			if(settings && settings.languages) {
				if(options.activityId && options.activityId.startsWith("cact-")) {
					return;
				}
				settings.schoolLanguages = $().sortObjectArray(settings.schoolLanguages,"orderBy");
				$.each(settings.schoolLanguages,function(langindex,language){
					$(container).append("<li langid='" + language.lanUId + "' style='display:none'><label class=\"radio\"><input type=\"radio\" name=\"lang\" value=''" + (langindex == 0? " checked='checked'" : "") + "></label></li>");
				});
				$.each(settings.languages,function(langindex,language){
					$(container).find("li[langid='" + language.lanUId + "'] input[name='lang']").val(language.code);
					$(container).find("li[langid='" + language.lanUId + "'] label").append(language.name);
				});
				$(container).find(".radio input[type='radio']").iCheck({
					radioClass: 'iradio_minimal-purple',
				});
				var selectedchapterid = $().getChapter();
				$().getChapterTpLanguages({chapterId:selectedchapterid,onserverresponse:function(chapterTps){
					$.each(chapterTps,function(tpindex,tp){
						if(options.tpId == tp.tpId) {
							var languages = tp.language.split(",");
							$.each(languages,function(langcodeindex,langcode){
								if(langcodeindex == 0) {
									$(container).find("li[langid] input[name='lang'][value='" + langcode + "']").closest("li").show();
								}
								//$(container).find("li[langid] input[name='lang'][value='" + langcode + "']").closest("li").show();
								if(langcodeindex > 0 && options.activityId) {
									var langtpid = options.tpId + "_" + langcode;
									var langactivityid = options.activityId + "_" + langcode;
									$().getActivitySnapshot({tpId:langtpid,activityId:langactivityid,onserverresponse:function(){
										if($().showLog()) console.log("hiding lang button for langcode: " + langcode + ", container: " + $(container).find("li[langid] input[name='lang'][value='" + langcode + "']").closest("li").length);
										$(container).find("li[langid] input[name='lang'][value='" + langcode + "']").closest("li").show();
									}});
								}
							})
						}
					})
					
				}})
			}
		}});
	}*/
	$.fn.showActivityPopup = function(eventsource,isplayerreplacement) {
		var screen = $().currentScreen();
		if($().showLog()) console.log("screen: " + screen + ", dialog screen: " + $(".activitypopupdialog").attr("screen"));
		if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
			$(".activitypopupdialog").dialog( "destroy" )
			$(".activitypopupdialog").attr("screen",screen);
		}
		var activityid = $(eventsource).attr("activityId");
		if(!activityid) {
			activityid = $(eventsource).closest("[activityId]").attr("activityId");
		}
		var cardtype = $(eventsource).closest("[type].tilesopen").attr("type");
		$(".activitypopupdialog").attr("cardtype",cardtype);
		$(".activitypopupdialog").attr("activityid",activityid);
		var prevactivityelement = ($(eventsource).attr("activityId")? $(eventsource).prev() : undefined);
		if(!prevactivityelement) {
			prevactivityelement = $(eventsource).closest("[activityId]").prev();
		}
		var nextactivityelement = ($(eventsource).attr("activityId")? $(eventsource).next() : undefined);
		if(!nextactivityelement) {
			nextactivityelement = $(eventsource).closest("[activityId]").next();
		} 
		var presentationid = $(eventsource).closest("[tpId]").attr("tpId");
		if(!presentationid) {
			presentationid = $(eventsource).closest("[tpid]").attr("tpid");
		}
		$(".activitypopupdialog").attr("presentationid",presentationid);
		if(isplayerreplacement && isplayerreplacement == true) {
			$(".activitypopupdialog").addClass("playerreplacement");
		}
		if(prevactivityelement) {
			$(".activitypopupdialog").attr("prevactivityid",$(prevactivityelement).attr("activityid"));
		}
		if(nextactivityelement) {
			$(".activitypopupdialog").attr("nextactivityid",$(nextactivityelement).attr("activityid"));
		}
		$(".activitypopupdialog").loadlayout({layout:screen + "/activitypopup",onlayoutcomplete:function(){
			$(".radio input[type='radio']").iCheck({
				radioClass: 'iradio_minimal-purple',
			});
			//$(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection").showLanguages({tpId:presentationid});
			if($().showLog()) console.log("get activity snapshot for tpId: " + presentationid + ", activityId: " + activityid);
			$().showActivitySnapshot({presentationid:presentationid,activityid:activityid,changeLanguages:true});
			$(".activitypopupdialog .vscroll").mCustomScrollbar({
				theme:"rounded-dots",
				scrollInertia:400
			});
			if($(".activitypopupdialog").dialog( "instance" ) == undefined) {
				$(".activitypopupdialog").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 940});
				$(".activitypopupdialog").dialog("open");
			} 
			if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
				$(".activitypopupdialog").dialog("open");
			}
		}});
	
	}
	$.fn.showCustomActivityPopup = function(eventsource,isplayerreplacement) {
		var screen = $().currentScreen();
		if($().showLog()) console.log("screen: " + screen + ", dialog screen: " + $(".activitypopupdialog").attr("screen"));
		if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
			$(".activitypopupdialog").dialog( "destroy" )
			$(".activitypopupdialog").attr("screen",screen);
		}
		var activityid = $(eventsource).attr("activityId");
		if(!activityid) {
			activityid = $(eventsource).closest("[activityId]").attr("activityId");
		}
		$(".activitypopupdialog").attr("activityid",activityid);
		var cardtype = $(eventsource).closest("[type].tilesopen").attr("type");
		$(".activitypopupdialog").attr("cardtype",cardtype);
		var prevactivityelement = ($(eventsource).attr("activityId")? $(eventsource).prev() : undefined);
		if(!prevactivityelement) {
			prevactivityelement = $(eventsource).closest("[activityId]").prev();
		}
		var nextactivityelement = ($(eventsource).attr("activityId")? $(eventsource).next() : undefined);
		if(!nextactivityelement) {
			nextactivityelement = $(eventsource).closest("[activityId]").next();
		} 
		var presentationid = $(eventsource).closest("[tpId]").attr("tpId");
		if(!presentationid) {
			presentationid = $(eventsource).closest("[tpid]").attr("tpid");
		}
		$(".activitypopupdialog").attr("presentationid",presentationid);
		if(isplayerreplacement && isplayerreplacement == true) {
			$(".activitypopupdialog").addClass("playerreplacement");
		}
		if(prevactivityelement) {
			$(".activitypopupdialog").attr("prevactivityid",$(prevactivityelement).attr("activityid"));
		}
		if(nextactivityelement) {
			$(".activitypopupdialog").attr("nextactivityid",$(nextactivityelement).attr("activityid"));
		}
		$(".activitypopupdialog").loadlayout({layout:screen + "/activitypopup",onlayoutcomplete:function(){
			$(".radio input[type='radio']").iCheck({
				radioClass: 'iradio_minimal-purple',
			});
			if($().showLog()) console.log("get activity snapshot for activityId: " + activityid);
			$().showActivitySnapshot({presentationid:presentationid,activityid:activityid});
			
			$(".activitypopupdialog .vscroll").mCustomScrollbar({
				theme:"rounded-dots",
				scrollInertia:400
			});
			if($(".activitypopupdialog").dialog( "instance" ) == undefined) {
				$(".activitypopupdialog").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 940});
				$(".activitypopupdialog").dialog("open");
			} 
			if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
				$(".activitypopupdialog").dialog("open");
			}
		}});
	}
	
	
	$.fn.playPresentation = function(tpId,activityId,resourceId) {
		if($().showLog()) console.log("Play presentation...");
		$().clearAssetTray();
		$().getPresentationInfo({tpId:tpId,onserverresponse:function(presentationobj){
			presentationobj = presentationobj.presentation[0];
			presentationobj.tpId = tpId;
			if(activityId) {
				presentationobj.selectedActivityId = activityId;
			}
			if(resourceId) {
				presentationobj.selectedResourceId = resourceId;
			}
			$().setPlayableObject(presentationobj,"presentation");
			if($().showLog()) console.log("setting playable object: " + JSON.stringify(presentationobj));
			$().manageScreenState();
			$().changescreen("player");
		}});
	}
	$.fn.updatePresentationResources = function(presentationobj) {
		if(presentationobj.resources) {
			return;
		}
		presentationobj.resources = [];
		if(presentationobj.sequenceVo) {
			$.each(presentationobj.sequenceVo,function(index,sequence){
				presentationobj.resources.push({id:sequence.activityId,type:"activity"});
			})
		}
		if(presentationobj.questions) {
			$.each(presentationobj.questions,function(index,question){
				presentationobj.resources.push({id:question,type:"question"});
			})
		}
		if(presentationobj.tools) {
			$.each(presentationobj.tools,function(index,toolid){
				presentationobj.resources.push({id:toolid,type:"tool"});
			})
		}
		if(presentationobj.games) {
			$.each(presentationobj.games,function(index,gameid){
				presentationobj.resources.push({id:gameid,type:"game"});
			})
		}
	}
	function createPresentationObjFromXML(xml) {
		if(config.debug) console.log("xml contents: " + xml);
		var presentationobj = {initprocess:[],sourceobj:{presentation:[{sequenceVo:[]}]},exercisequestions:[],whiteboards:[],resources:[],quizquestions:[],tags:"",description:""};
		if(config.debug) console.log("xml contents: " + $(xml).find("classpresentation > metadata > title").text());
		presentationobj.title = $(xml).find("classpresentation > metadata > title").text();
		var presentationseq = $(xml).find("classpresentation metadata presentationSequence").text();
		if(config.debug) console.log("presentationseq: " + presentationseq);
		var assetIds = presentationseq.split(",");
		var questionids = [];
		$(xml).find("classpresentation media question").each(function(){
			questionids.push($(this).attr("UID"));
		});
		$.each(assetIds,function(index,assetId){
			if($.inArray(assetId,questionids) >= 0) {
				presentationobj.quizquestions.push({questionId:assetId});
				return;
			}
			presentationobj.resources.push({resourceId:assetId,resourcetype:"resource"});
		})
		return presentationobj;
	}

	$.fn.fetchPresentationTCESEQFileContents = function(options) {
		var eventsource = $(this);
		var defaultoptions = {filename:"tceseq.json",onerrorresponse:function(){
			var presentationid = options.id
			$("#preloading").loading("remove");
			if(config.debug) console.log("failed to load tceseq json for presentation id: " + presentationid);
			$("#preloading").loading();
			$().fetchPresentationXML({presentationId:presentationid,onserverresponse:function(filecontents){
				$("#preloading").loading("remove");
				if(config.debug) console.log("xml contents: " + filecontents);
				var presentationobj = createPresentationObjFromXML($.parseXML(filecontents));
				$("#preloading").loading();
				$().storePresentationObj({presentationId:presentationid,object:presentationobj,onserverresponse:function(response){
					$("#preloading").loading("remove");
					if(config.debug) console.log("store presentation obj response: " + response + ", string: " + JSON.stringify(response));
					if(response.status == "success") {
						/*if(options.onconversionsuccess) {
							defaultoptions.onconversionsuccess(defaultoptions,options,response);
						}*/
						$().fetchPresentationFileContents(defaultoptions);
					}
				}})
			}})
		}};
		$.extend(defaultoptions,options)
		$().fetchPresentationFileContents(defaultoptions);
	}
	$.fn.playCustomPresentation = function(presentationId,activityId,resourceId) {
		$().clearAssetTray();
		$().fetchPresentationTCESEQFileContents({id:presentationId,onserverresponse:function(presentationobj){
			if(activityId) {
				presentationobj.selectedActivityId = activityId;
			}
			if(resourceId) {
				presentationobj.selectedResourceId = resourceId;
			}
			presentationobj = $.parseJSON(presentationobj);
			$().updatePresentationResources(presentationobj);
			$().setPlayableObject(presentationobj,"custompresentation");
			if($().showLog()) console.log("setting playable object: " + JSON.stringify(presentationobj));
			$().manageScreenState();
			$().changescreen("player");
		}});
		/*$().fetchPresentationFileContents({id:presentationId,filename:"tceseq.json",onserverresponse:function(presentationobj){
			if(activityId) {
				presentationobj.selectedActivityId = activityId;
			}
			if(resourceId) {
				presentationobj.selectedResourceId = resourceId;
			}
			presentationobj = $.parseJSON(presentationobj);
			$().updatePresentationResources(presentationobj);
			$().setPlayableObject(presentationobj,"custompresentation");
			if($().showLog()) console.log("setting playable object: " + JSON.stringify(presentationobj));
			$().manageScreenState();
			$().changescreen("player");
		}});*/
	}
	
	$.fn.playUnsavedCustomPresentation = function(presentationobj,activityId,resourceId) {
		if(activityId) {
			presentationobj.selectedActivityId = activityId;
		}
		if(resourceId) {
			presentationobj.selectedResourceId = resourceId;
		}
		$().updatePresentationResources(presentationobj);
		$().setPlayableObject(presentationobj,"custompresentation");
		if($().showLog()) console.log("setting playable object: " + JSON.stringify(presentationobj));
		$().manageScreenState();
		$().changescreen("player");
	}
	/*
	$.fn.showCreatePresentationDialog = function(options) {
		if($().showLog()) console.log("create presentation dialog created: ");
		var eventsource = $(this);
		var screen = $().currentScreen();
		if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
			$(".activitypopupdialog").dialog( "destroy" )
		}
		$(".activitypopupdialog").loadlayout({layout:screen + "/createpresentationpopup",onlayoutcomplete:function(){
			//$(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"][value='lessonplanview']").attr("disabled","disabled");
			//$(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"][value='lessonplanview']").closest("li").addClass("disabled-radio")
			$(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"][value='Assessment']").attr("disabled","disabled");
			$(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"][value='Assessment']").closest("li").addClass("disabled-radio");
			$().populateSelectedActivities(options);
			$(".dialog-content .vscroll").mCustomScrollbar({
				theme:"rounded-dots",
				scrollInertia:400
			});
			var schoolSettings = $().schoolSettings();
			if(schoolSettings.autoShare) {
				$(".activitypopupdialog .share-left input[name='share'][value='0']").closest("li").remove();
				$(".activitypopupdialog .share-left input[name='share'][value='1']").attr("checked","checked");
			}
			
			$(".radio input[type='radio']").iCheck({
				radioClass: 'iradio_minimal-purple'
			});
			if(!options || !options.isedit) {
				$(".radio input[type='radio']").on("ifChanged",function(e){
					if($(this).is(":checked")) {
						$().showPresentationDetailForm(options);
					}
				});
			}
			
			if($(".activitypopupdialog").dialog( "instance" ) == undefined) {
				$(".activitypopupdialog").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 940, height:520});
				$(".activitypopupdialog").dialog("open");
				$(".activitypopupdialog").attr("screen",screen);
			} 
			if($(".activitypopupdialog").dialog( "instance" ) != undefined) {
				$(".activitypopupdialog").dialog("open");
				$(".activitypopupdialog").attr("screen",screen);
			}
		}});
	}*/
	$.fn.showPresentationDetailForm = function(options) {
		var screen = $().currentScreen();
		var presentationtype = $(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"]:checked").val();
		console.log("presentation type: " + $(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"]:checked").length + ", " + presentationtype);
		$(".activitypopupdialog .dialog-content .activityleft .form-view").loadlayout({layout:screen + "/detail-form/" + presentationtype.toLowerCase() + "-detail",onlayoutcomplete:function(){
			$(".activitypopupdialog").addClass("create-presentation-dialog add-dialog");
			if(presentationtype.toLowerCase() != "assessment") {
				$(".activitypopupdialog select[name='type']").populateActivityTypes();
			}
			$(".activitypopupdialog select[name='duration']").populateActivityDurations();
			$(".activitypopupdialog select[name='class']").populateClasses(function(){
				var schoolClass = $().getSelectedClass();
				$(".activitypopupdialog select[name='class']").val(schoolClass.grade);
				$(".activitypopupdialog select[name='subject']").populateSubjects(schoolClass.grade,function(){
					$(".activitypopupdialog select[name='chapter']").populateChapters(schoolClass.grade,schoolClass.subject,function(){
						var chapterId = $().getChapter();
						if(chapterId) {
							$(".activitypopupdialog select[name='chapter']").val(chapterId)
						}
						var chapterid = $(".activitypopupdialog select[name='chapter']").val();
						$(".activitypopupdialog select[name='topic']").populateTopics(chapterid);
						if(options && options.populatedependenciescomplete) {
							options.populatedependenciescomplete();
						}
					});
				});
				$(".activitypopupdialog select[name='subject']").val(schoolClass.subject);
			});
			$(".activitypopupdialog select[name='timings']").populateSchoolTimings();
			$('.activitypopupdialog .datepicker').datepicker({
		        showOn: "both",
		        buttonImage: '/ce-static/classedgelx/css/images/calender.png',
		        buttonImageOnly: true,
		        dateFormat:"dd-mm-yy"
		    });
			if(options && options.onformlayoutcomplete) {
				options.onformlayoutcomplete();
			}
		}});
	}
	/*
	$.fn.populateSchoolTimings = function() {
		var container = $(this);
		$(container).removeChildren();
		$().loadSchoolTimings({onserverresponse:function(objects){
			$.each(objects.timeList,function(index,period){
				if(!period.breakPeriod) {
					$(container).append("<option value=\"" + period.startTime + "\">" + period.startTime + "</option>");
				}
			})
		}});
	}*/
	$.fn.populateSelectedActivities = function(options) {
		var screen = $().currentScreen();
		var isassessment = false;
		if(options && options.isedit) {
			$().showPresentationDetailForm(options);
			return;
		}
		if($(".activities-tray .tray-flyouts-slider li[activitytype != 'quiz']").length > 0) {
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='lessonplanview']").closest("li").addClass("disabled-radio")
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='lessonplanview']").attr("disabled","");
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='Assessment']").closest("li").addClass("disabled-radio")
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='Assessment']").attr("disabled","");
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='presentationview']").click();
		}
		if($(".activities-tray .tray-flyouts-slider li[activitytype='quiz']").length > 0 && $(".activities-tray .tray-flyouts-slider li[activitytype]").length == $(".activities-tray .tray-flyouts-slider li[activitytype='quiz']").length) {
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='presentationview']").closest("li").addClass("disabled-radio")
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='presentationview']").attr("disabled","");
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='lessonplanview']").closest("li").addClass("disabled-radio")
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='lessonplanview']").attr("disabled","");
			//$(".activitypopupdialog .createoptions input[name='planpresent'][value='Assessment']").attr("checked","checked");
			//$(".activitypopupdialog .createoptions input[name='planpresent'][value='Assessment']").click();
			$(".activitypopupdialog .createoptions input[name='planpresent'][value='Assessment']").iCheck("check");
			isassessment = true;
		}
		$().showPresentationDetailForm(options);
		var questionids = [];
		$(".activities-tray .tray-flyouts-slider li[activitytype='quiz']").each(function(){
			questionids.push($(this).attr("questionid"));
		});
		if(isassessment) {
			$(".activitypopupdialog .dialog-content .activityright .activityfromhead").html("Question List <a href=\"javascript:void(0)\" class=\"addingfiles dialog-close\">Minimize</a>");
			//var trayactivitycontainer = $(".activities-tray .tray-flyouts-slider li[activitytype='quiz']");
			//var questionidsval = $(trayactivitycontainer).attr("questionids");
			//var questionids = questionidsval.split(",");
			$().getlayout({layout:screen + "/createassessment-question",onserverresponse:function(questionlayout){
				$.each(questionids,function(index,questionid){
					$(".activitypopupdialog .activityright .resourceitem").append(questionlayout);
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("questionid",questionid);
					if(questionid.startsWith("cquest-")) {
						$().getCustomQuestionById({questionId:questionid,onserverresponse:function(question){
							question = question.data.xml;
							question = $.parseXML(question);
							$(".activitypopupdialog .activityright .resourceitem > li[questionid='" + questionid + "'] .quiz-question-text").html($(question).find("qtext").text());
						}});
						return;
					}
					if(questionid.startsWith("teqb-")) {
						$().getTestedgeQuestionData({questionId:questionid,onserverresponse:function(object){
							var question = $.parseXML(object.object.questionData);
							var questiontext = $(question).find("Question_Stem > Question_Text > Text").text();
							$(".activitypopupdialog .activityright .resourceitem > li[questionid='" + questionid + "'] .quiz-question-text").html(questiontext);
						}});
						return;
					}
					$().getQuestionById({questionId:questionid,onserverresponse:function(question){
						question = question.data.xml;
						question = $.parseXML(question);
						$(".activitypopupdialog .activityright .resourceitem > li[questionid='" + questionid + "'] .quiz-question-text").html($(question).find("qtext").text());
					}});
				});
			}});
			return;
		}
		$().getlayout({layout:screen + "/createpresentation-activity",onserverresponse:function(activitylayout){
			$(".activities-tray .tray-flyouts-slider li[activitytype]").each(function(){
				var presentationtype = $(".activitypopupdialog .dialog-content .activityleft .createoptions input[name=\"planpresent\"]:checked").val();
				
				var activitytype = $(this).attr("activitytype");
				var activityid = $(this).attr("activityid")
				var tpid = $(this).attr("tpid");
				if(activitytype != "quiz") {
					$(".activitypopupdialog .activityright .resourceitem").append(activitylayout);
					var activityObj = {activityId:activityid};
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("activityobj",JSON.stringify(activityObj));
				}
				if(activitytype == "quiz" && $(".activitypopupdialog .activityright .resourceitem > li[questionids]").length <= 0) {
					$(".activitypopupdialog .activityright .resourceitem").append(activitylayout);
				}
				
				//var questionids = $(this).attr("questionids");
				if($().showLog()) console.log("activitytype: " + activitytype + ", activityid: " + activityid + ", tpId: " + tpid + ", questionids: " + questionids);
				if(activityid) {
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("activityid",activityid);
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").hide();
				}
				if(activitytype == "quiz") {
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("questionids",questionids);
				}
				if(activitytype == "tool") {
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("toolid",$(this).attr("toolid"));
				}
				if(activitytype == "game") {
					$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("gameid",$(this).attr("gameid"));
				}
				if(activitytype == "default") {
					if($().showLog()) console.log("tpid: " + tpid + " to tray");
					$().getActivitySnapshot({tpId:tpid,activityId:activityid,onserverresponse:function(activitysnapshot){
						activitysnapshot.activityId = activitysnapshot.id;
						$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activityid + "']").show();
						$().addSelectedActivitiesUIToPresentationForm(activitylayout,activitysnapshot,activitytype);
					},onerrrorresponse:function(){
						$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activityid + "']").remove();
					}});
					return;
				}
				if(activitytype == "custom" || activitytype == "shared") {
					$().getActivityContent({activityId:activityid,onserverresponse:function(activity){
						activity.activityId = activityid;
						$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activityid + "']").show();
						$().addSelectedActivitiesUIToPresentationForm(activitylayout,activity,activitytype);
					}});
					return;
				}
				if(activitytype == "quiz") {
					$().addSelectedQuizUIToPresentation(activitylayout,questionids);
				}
				if(activitytype == "tool") {
					//$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .chapter-title").text($(this).attr("label"));
					$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .chapter-title").html("<div class='title'>" + $(this).attr("label") + "</div>");
					$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .sch-formaticons").addClass("darkbgcolor13");
					$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .sch-formaticons span").removeClass();
					$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .sch-formaticons span").addClass("labedge");
					$(".activitypopupdialog .activityright .resourceitem > li[toolid='" + $(this).attr("toolid") + "'] .sch-formaticons p").text("Lab");
				}
				if(activitytype == "game") {
					//$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .chapter-title").text($(this).attr("gametitle"));
					$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .chapter-title").html("<div class='title'>" + $(this).attr("gametitle") + "</div>");
					$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .sch-formaticons").addClass("darkbgcolor12");
					$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .sch-formaticons span").removeClass();
					$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .sch-formaticons span").addClass("puzzel");
					$(".activitypopupdialog .activityright .resourceitem > li[gameid='" + $(this).attr("gameid") + "'] .sch-formaticons p").text("Game");
				}
			});
			$().showPresentationResourceCount();
		}});
	}
	$.fn.addSelectedActivitiesUIToPresentationForm = function (activitylayout,activity,activitytype) {
		var configurations = $().config();
		var mediaformat = configurations.mediaformats["default"];
		if(configurations.mediaformats[activity.type]) {
			mediaformat = configurations.mediaformats[activity.type];
		}
		
		var screen = $().currentScreen();
		//$("[screen='" + screen + "'].activitypopupdialog .activityright .resourceitem").append(activitylayout);
		//$("[screen='" + screen + "'].activitypopupdialog .activityright .resourceitem > li:last-child").attr("activityid",activity.activityId);
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "']").attr("activitytype",activitytype);
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .sch-formaticons span").removeClass();
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .sch-formaticons span").addClass(mediaformat.format + "format");
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .sch-formaticons p").text(mediaformat.text);
		if(mediaformat.darkcolor) {
			$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .sch-formaticons").addClass(mediaformat.darkcolor);
		}
		//$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .chapter-title").text(activity.title)
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .chapter-title").html("<div class='title'>" + activity.title + "</div>");
		
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .chapter-subtitle li:first-child").text((activity.duration? activity.duration : "") + " mins");
		$(".activitypopupdialog .activityright .resourceitem > li[activityid='" + activity.activityId + "'] .chapter-subtitle li:last-child").text(activity.activityType);
	}
	$.fn.addSelectedQuizUIToPresentation = function(activitylayout,questionids) {
		//var screen = $().currentScreen();
		//$(".activitypopupdialog .activityright .resourceitem").append(activitylayout);
		//$(".activitypopupdialog .activityright .resourceitem > li:last-child").attr("questionids",questionids);
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .sch-formaticons").addClass("darkbgcolor4");
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .sch-formaticons span").removeClass();
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .sch-formaticons span").addClass("quizformat");
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .sch-formaticons p").text("Quiz");
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .chapter-title").text("Quiz")
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .chapter-subtitle li:first-child").text(questionids.length + " Questions");
		$(".activitypopupdialog .activityright .resourceitem > li[questionids] .chapter-subtitle li:last-child").text("Objectives");
	}
	$.fn.filterCards = function() {
		var container = $(this);
		if($().showLog()) console.log("filter cards for container: " + $(container).prop("tagName") + ", " +  $(container).attr("class") + ", " + $(container).closest("li[tpid]").attr("tpid"))
		var allfound = false;
		var favselected = false;
		$("section.filterposition .filters label.checkbox input").each(function(){
			var ischecked = $(this).attr("checked") != null;
			var value = $(this).val();
			if($().showLog()) console.log("val: " + value + ", ischecked: " + ischecked);
			if(ischecked) {
				$(container).find("li[activityid][activitytype='" + value + "']").show();
				if(favselected) {
					if($().showLog()) console.log("favourite is selected hence hiding non favourites: " + $(container).find("li[activityid]:not([favourite])").length);
					$(container).find("li[activityid]:not([favourite])").hide();
				}
				if(value == "favourite") {
					favselected = true;
					$(container).find("li[presentationid]:not([favourite])").hide();
					$(container).find("li[activityid]:not([favourite])").hide();
				}
			}
			if(!ischecked) {
				$(container).find("li[activityid='" + value +"']").hide();
				$(container).find("li[activityid][activitytype='" + value + "']").hide();
				if(value == "favourite") {
					$(container).find("li[presentationid]:not([favourite])").show();
					$(container).find("li[activityid]:not([favourite])").show();
				}
			}
		})
	}
	$.fn.markFavourite = function() {
		var userfavourites = $().getUserFavourites();
		if(!userfavourites) {
			return;
		}
		var container = $(this);
		$.each(userfavourites.objects,function(index,favourite){
			$(container).find("li[" + favourite.uGeneratedMyLibraryPK.typeId + "='" + favourite.uGeneratedMyLibraryPK.assetId + "']").attr("favourite","true");
			$(container).find("li[" + favourite.uGeneratedMyLibraryPK.typeId + "='" + favourite.uGeneratedMyLibraryPK.assetId + "'] .fav").addClass("selected");
			$(container).find("tr[" + favourite.uGeneratedMyLibraryPK.typeId + "='" + favourite.uGeneratedMyLibraryPK.assetId + "']").attr("favourite","true");
			$(container).find("tr[" + favourite.uGeneratedMyLibraryPK.typeId + "='" + favourite.uGeneratedMyLibraryPK.assetId + "'] .fav").addClass("selected");
		});
	}
	$.fn.markPopupFavourite  = function(type,id) {
		var container = $(this);
		$(container).find(".title-boxshadow .fav").removeClass("selected");
		var userfavourites = $().getUserFavourites();
		if(!userfavourites) {
			return;
		}
		$.each(userfavourites.objects,function(index,favourite){
			if(favourite.uGeneratedMyLibraryPK.typeId == type && id == favourite.uGeneratedMyLibraryPK.assetId) {
				$(container).find(".title-boxshadow .fav").addClass("selected");
			}
		});
	}
	$.fn.recalculateTrayDuration = function() {
		var durationcount = 0;
		$("#lessonplanbody .trayoutline .tray-left li[duration]").each(function(){
			durationcount += parseInt($(this).attr("duration"));
		})
		$("#lessonplanbody .trayoutline .tray-right .trayactions > li:first-child a").html("Duration <br/>" + durationcount + " mins");
		if($("#lessonplanbody .trayoutline .tray-left .tray-flyouts-slider > li").length <= 0) {
			$("#lessonplanbody .trayoutline .tray-right .closetray").click();
		}
	}
	$.fn.validateRequiredField = function() {
		if($(this).length <= 0) {
			return true;
		}
		if($(this).val().trim() == "") {
			$(this).addClass("error");
			return false;
		}
		return true;
	}
	$.fn.regexValidation = function(regexstring,errormessage) {
		if(!errormessage) {
			errormessage = "invalid format";
		}
		var format = new RegExp(regexstring);
		var value = $(this).val();
		var found = format.test(value);
		if($().showLog()) console.log("format: " + regexstring + ", value: " + value + ", data valid: " + found);
		if(!found) {
			$(this).addClass("error");
			if($(this).next(".errormessage").length <= 0) {
				$(this).after("<span class='errormessage'>" + errormessage + "</span>");
			}
			$(this).next(".errormessage").text(errormessage);
		}
		return found;
	}
	$.fn.addQuestionToTray = function(screen,questionId,tpid) {
		var eventsource = $(this);
		if(!screen) {
			screen = $().currentScreen();
		}
		if(!questionId) {
			questionId = $(this).closest("tr[quid]").attr("quid");
		}
		if(!tpid) {
			tpid = $(this).closest("[tpid]").attr("tpid");
		}
		$().getlayout({layout:screen + "/trayquiz",onserverresponse:function(quizlayout){
			addQuestionUIToTray(quizlayout,questionId,tpid);
		}});
	}
	function addQuestionUIToTray(quizlayout,questionId,tpid) {
		if($(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']").length <= 0) {
			$(".activities-tray .tray-flyouts-slider").append(quizlayout);
			$(".activities-tray .tray-flyouts-slider li:last-child").attr("questionid",questionId);
		}
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']").attr("resourceid",questionId);
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']").attr("resourcetype","question");
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']").attr("activitytype","quiz");
		if(tpid) {
			$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']").attr("tpid",tpid);
		}
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "'] .sch-formaticons").addClass("darkbgcolor10")
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "'] .sch-formaticons span").removeClass();
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "'] .sch-formaticons span").addClass("quizformat1");
		
		var selectedClass = $().getSelectedClass();
		var selectedGradeObj = $().retrieveSelectedGradeObject(selectedClass.grade);
		console.log("selected grade obj: " + JSON.stringify(selectedGradeObj) + ", grade: " + selectedClass.grade);
		var selectedSubjectObj = null;
		$.each(selectedGradeObj.subjects,function(subjectindex,subject){
			if(subject.id == selectedClass.subject) {
				selectedSubjectObj = subject;
			}
		});
				
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "'] .tray-item-content p:nth-child(3) > span:first-child").text(selectedGradeObj.label? selectedGradeObj.label : (selectedGradeObj.title? selectedGradeObj.title : "") );
		$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "'] .tray-item-content p:nth-child(3) > span:last-child").text(selectedSubjectObj.title);
		$(".activities-tray .tray-flyouts-slider > li:nth-child(n+13)").hide();
		$(".activities-tray").trigger("activitytray:itemadded",[$(".activities-tray .tray-flyouts-slider li[questionid='" + questionId + "']")]);
	}
	$.fn.showCreateActivityDialog = function(container,screen,onpopuploaded) {
		var eventsource = $(this);
		if(!container) {
			container = ".activitypopupdialog";
		}
		if(!screen) {
			screen = $().currentScreen();
		}
		if($(container).dialog( "instance" ) != undefined) {
			$(container).dialog( "destroy" )
		}
		$(container).loadlayout({layout:screen + "/createactivitypopup",onlayoutcomplete:function(){
			$(container).addClass("add-activity-dialog add-dialog");
			$(container + " select[name='type']").populateActivityTypes();
			$(container + " select[name='duration']").populateActivityDurations();
			$(container + " select[name='class']").populateClasses(function(){
				var schoolClass = $().getSelectedClass();
				$(container + " select[name='class']").val(schoolClass.grade);
				$(container + " select[name='subject']").populateSubjects(schoolClass.grade,function(){
					$(container + " select[name='subject']").val(schoolClass.subject);
					$(container + " select[name='chapter']").populateChapters(schoolClass.grade,schoolClass.subject,function(){
						var chapterId = $().getChapter();
						if(chapterId) {
							$(container + " select[name='chapter']").val(chapterId)
						}
						var selectedchapterid = $(container + " select[name='chapter']").val();
						$(container + " select[name='topic']").populateTopics(selectedchapterid);
						if(onpopuploaded) {
							onpopuploaded();
						}
					});
				});
			});
			$(".dialog-content .vscroll").mCustomScrollbar({
				theme:"rounded-dots",
				scrollInertia:400
			});
			var schoolSettings = $().schoolSettings();
			if(schoolSettings.autoShare) {
				$(".activitypopupdialog .share-left input[name='share'][value='0']").closest("li").remove();
				$(".activitypopupdialog .share-left input[name='share'][value='1']").attr("checked","checked");
			}
			//if(schoolSettings)
			$(".radio input[type='radio']").iCheck({
				radioClass: 'iradio_minimal-purple'
			});
			if($(container).dialog( "instance" ) == undefined) {
				$(container).dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 940, height:520});
				$(container).dialog("open");
				$(container).attr("screen",screen)
			} 
			if($(container).dialog( "instance" ) != undefined) {
				$(container).dialog("open");
				$(container).attr("screen",screen)
			}
		}});
	}
	$.fn.populateActivityTypes = function() {
		var configurations = $().config();
		var container = $(this);
		$(container).removeChildren();
		if($().showLog()) console.log("custom activity types: " + JSON.stringify(configurations.createactivity.customactivitytypes));
		$.each(configurations.createactivity.customactivitytypes,function(activitytypeindex,type){
			$(container).append("<option value=\"" + type.value + "\">" + type.title + "</option>");
		});
	}
	$.fn.populateActivityDurations = function() {
		var configurations = $().config();
		var container = $(this);
		$(container).removeChildren();
		$.each(configurations.createactivity.duration,function(durationindex,duration){
			$(container).append("<option value=\"" + duration.value + "\">" + duration.title + "</option>");
		});
	}
	$.fn.populateClasses = function(onloadcomplete) {
		if(config.debug) console.log("populating classes...");
		var container = $(this);
		$(this).removeChildren();
		$().getCategories({onserverresponse:function(objects){
			$.each(objects["default"],function(defaultcategoryindex,defaultcategory){
				$.each(defaultcategory.grades,function(gradeindex,grade){
					$(container).append("<option value=\"" + grade.id + "\">" + grade.label + "</option>");
				});
			});
			if(onloadcomplete) {
				onloadcomplete(container);
			}
		}});
	}
	$.fn.populateSubjects = function(gradeId,onloadcomplete) {
		if($().showLog()) console.log("populate subjects for gradeId: " + gradeId);
		var container = $(this);
		var gradesubjectmap = $().getGradeSubjectMap();
		var selectedgradeobj = null;
		$.each(gradesubjectmap["default"],function(index,category){
			$.each(category.grades,function(gradeindex,grade){
				if(grade.id == gradeId) {
					selectedgradeobj = grade;
				}
			});
		});
		$(container).removeChildren();
		$.each(selectedgradeobj.subjects,function(subjectindex, subject){
			$(container).append("<option value=\"" + subject.id + "\">" + subject.title + "</option>");
		});
		if(onloadcomplete) {
			onloadcomplete();
		}
	}
	
	var tocxml = null;
	$.fn.populateChapters = function(gradeId,subjectId,onloadcomplete) {
		if($().showLog()) console.log("populating chapters for gradeId: " + gradeId + ", subjectId: " + subjectId);
		var container = $(this);
		$(container).removeChildren();
		$().loadToc({board:board,grade:gradeId,subject:subjectId,onserverresponse:function(xml){
			tocxml = xml;
			if($().showLog()) console.log("TOC xml for gradeId: " + gradeId + ", subjectId: " + subjectId + ", " + $().xmlToString(xml));
			$(xml).find("toc node[type='chapter']").each(function(){
				if($().showLog()) console.log("chapter id: " + $(this).attr("id") + ", label: " + $(this).children("label").text());
				$(container).append("<option value=\"" + $(this).attr("id")  + "\">" + $(this).children("label").text() + "</option>");
			});
			if(onloadcomplete) {
				onloadcomplete();
			}
		}});
	}
	$.fn.populateTopics = function(chapterid) {
		var container = $(this);
		$(container).removeChildren();
		$(container).append("<option value=\"\">Topic</option>");
		$(tocxml).find("toc node[type='chapter'][id='" + chapterid + "'] node[type='teaching point']").each(function(){
			if($().showLog()) console.log("chapter id: " + $(this).attr("id") + ", label: " + $(this).children("label").text());
			$(container).append("<option value=\"" + $(this).attr("id")  + "\">" + $(this).children("label").text() + "</option>");
		});
	}
	$.fn.showAddResourceDialog = function(options) {
		var screen = $().currentScreen();
		if(options && options.screen) {
			screen = options.screen;
		}
		$(".popupdialog2").removeClass("ratings-dialog");
		$(".popupdialog2").removeAttr("rating-resourceid");
		$(".popupdialog2").removeAttr("rating-resourcetype");
		if($(".popupdialog2").dialog( "instance" ) != undefined) {
			$(".popupdialog2").dialog( "destroy" )
		}
		$(".popupdialog2").loadlayout({layout:screen + "/addresourcepopup",onlayoutcomplete:function(){
			$().getSchoolSettings({onserverresponse:function(settings){
				$(".popupdialog2 .dialog-content .message .maxfilesize").text(settings.fileuploadSize);
			}})
			$(".popupdialog2").attr("screen",screen);
			if($(".popupdialog2").dialog( "instance" ) == undefined) {
				$(".popupdialog2").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 400});
				$(".popupdialog2").dialog("open");
			} 
			if($(".popupdialog2").dialog( "instance" ) != undefined) {
				$(".popupdialog2").dialog("open");
			}
			$('.popupdialog2 #resourcefileuploadform .attachadddialog').unbind("click");
			$('.popupdialog2 #resourcefileuploadform .attachadddialog').click(function() {
				var isvaliddata = true;
				$(".popupdialog2 #resourcefileuploadform .error").removeClass("error");
				isvaliddata = $(".popupdialog2 #resourcefileuploadform input[name=\"title\"]").validateRequiredField();
				isvaliddata = isvaliddata && $(".popupdialog2 #resourcefileuploadform input[name=\"title\"]").regexValidation("^([^']+)$","single quote is not allowed");
				if(!isvaliddata) {
					if($().showLog()) console.log("data validation failed");
					return false;
				}
				return true;
			});
			$('.popupdialog2 #resourcefileuploadform').fileupload({url:$().getactionbaseurl() + "/storeactivityresource"
				,singleFileUploads:true
				,maxFileSize:5000
				,change:function(e,data) {
					if($().showLog()) console.log("data on change: " + JSON.stringify(data));
					var filename = data.files[0].name;
					$('.popupdialog2 #resourcefileuploadform input[name="sname"]').val(filename);
					var uploadErrors = [];
			        if(data.files[0]['size'].length && data.files[0]['size'] > 5000) {
			            uploadErrors.push('Filesize is too big');
			        }
			        if(uploadErrors.length > 0) {
			            alert(uploadErrors.join("\n"));
			        }					
				}
				,getNumberOfFiles: function () { return 1 }
				,add:function(e,data){
					if($().showLog()) console.log("data on change: " + JSON.stringify(data));
					var uploadErrors = [];
			        if(data.files[0]['size'].length && data.files[0]['size'] > 5000) {
			            uploadErrors.push('Filesize is too big');
			        }
			        if(uploadErrors.length > 0) {
			            alert(uploadErrors.join("\n"));
			            return;
			        }					
					var lastfile = data.files.pop();
					data.files = [];
					data.files.push(lastfile);
					$('.popupdialog2 #resourcefileuploadform .attachadddialog').unbind("click");
					$('.popupdialog2 #resourcefileuploadform .attachadddialog').click(function() {
						var isvaliddata = true;
						$(".popupdialog2 #resourcefileuploadform .error").removeClass("error");
						isvaliddata = $(".popupdialog2 #resourcefileuploadform input[name=\"title\"]").validateRequiredField();
						//isvaliddata = isvaliddata && $(".popupdialog2 #resourcefileuploadform input[name=\"title\"]").regexValidation("^([^']+)$","single quote is not allowed");
						if(!isvaliddata) {
							if($().showLog()) console.log("data validation failed");
							return false;
						}
						if($().showLog()) console.log("submitting the form...using fileupload plugin: ");
						$("#preloader").show();
						data.submit();
					});
				},done:function(e,data){
					if($().showLog()) console.log("file upload response: " + JSON.stringify(data.result));
					/*
					var title = $('.popupdialog2 #resourcefileuploadform input[name="title"]').val();
					var type = $('.popupdialog2 #resourcefileuploadform select[name="type"]').val();
					if($().showLog()) console.log("title elem: " + $('.popupdialog2 #resourcefileuploadform input[name="title"]').length + ", type elem: " + $('.popupdialog2 #resourcefileuploadform select[name="type"]').length);
					if($().showLog()) console.log("title: " + title + ", type: " + type);
					options.uploadresponse = data.result;
					options.title = title;
					options.type = type;
					$("#preloader").hide();
					$().showAddedResource(options);*/
					var obj = {};
					obj = data.result;
					if(obj.status == "failed"){
						//alert("file size exceeded");
						$(".popupdialog2").loadlayout({layout:screen + "/filesizeexceeded",onlayoutcomplete:function(){
							$().getSchoolSettings({onserverresponse:function(settings){
								$(".popupdialog2 .dialog-content .message .maxfilesize").text(settings.fileuploadSize);
							}})
							$(".popupdialog2").attr("screen",screen);
							if($(".popupdialog2").dialog( "instance" ) == undefined) {
								$(".popupdialog2").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 400});
								$(".popupdialog2").dialog("open");
							} 
							if($(".popupdialog2").dialog( "instance" ) != undefined) {
								$(".popupdialog2").dialog("open");
							}
						}});
						$("#preloader").hide();
						return;
					}
					var title = $('.popupdialog2 #resourcefileuploadform input[name="title"]').val();
					var type = $('.popupdialog2 #resourcefileuploadform select[name="type"]').val();
					if($().showLog()) console.log("title elem: " + $('.popupdialog2 #resourcefileuploadform input[name="title"]').length + ", type elem: " + $('.popupdialog2 #resourcefileuploadform select[name="type"]').length);
					if($().showLog()) console.log("title: " + title + ", type: " + type);
					options.uploadresponse = data.result;
					options.title = title;
					options.type = type;
					$("#preloader").hide();
					$().showAddedResource(options);
				},maxNumberOfFiles: 1});
		}});
	}

	$.fn.showAddedResource = function(options) {
		if($().showLog()) console.log("show added response in " + options.container);
		if(!options.container) {
			options.container = ".activitypopupdialog";
		}
		if(!options.screen) {
			options.screen = $().currentScreen();
		}
		$(options.container + " .addcreateactivity .resourceitem").appendlayout({layout:options.screen + "/createactivity-addedresource",onlayoutcomplete:function(){
			//$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").text(options.title);
			$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").html("<div class='title'>" + options.title + "</div>");
			if(options.assetId) {
				$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").attr("assetId",options.assetId);
			}
			$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").attr("title",options.title);
			$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").attr("type",options.type);
			$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").attr("filename",options.uploadresponse.filename);
			$(options.container + " .addcreateactivity .resourceitem li:last-child .chapter-title").attr("filepath",options.uploadresponse.filepath);
		}});
		$(options.container + " .createactivity").hide();
		$(options.container + " .addcreateactivity").show();
	}
	/*
	$.fn.showRatingDialog = function(options) {
		var eventsource = $(this);
		var screen = $().currentScreen();
		if(options && options.screen) {
			screen = options.screen;
		}
		if($(".popupdialog2").dialog( "instance" ) != undefined) {
			$(".popupdialog2").dialog( "destroy" )
		}
		$(".popupdialog2").loadlayout({layout:"ratingdialogcontent",onlayoutcomplete:function(){
			$(".popupdialog2").attr("screen",screen);
			$(".popupdialog2").addClass("ratings-dialog");
			$(".tabhoriz").tabs();
			if($(".popupdialog2").dialog( "instance" ) == undefined) {
				$(".popupdialog2").dialog({ autoOpen: false, draggable: false, resizable: true, modal: true, autoReposition: true,closeOnEscape:true,width: 500});
				$(".popupdialog2").dialog("open");
			} 
			if($(".popupdialog2").dialog( "instance" ) != undefined) {
				$(".popupdialog2").dialog("open");
			}
			$(".popupdialog2 .tabhoriz .ratingcontent .vscroll").mCustomScrollbar({
				theme:"rounded-dots",
				scrollInertia:400
			});
			$().fetchRatingAnalytics({resourceType:options.resourceType,resourceId:options.resourceId,onserverresponse:function(object){
				if($().showLog()) console.log("rating: " + JSON.stringify(object));
				if(object) {
					if($().showLog()) console.log("search criteria: " + ".popupdialog2 .dialog-content .overalrating .rating li:nth-child(" + $().formatNumber(object.avgRating,0) + ")");
					if($().showLog()) console.log("length: " + $(".popupdialog2 .dialog-content .overalrating .rating li:nth-child(" + $().formatNumber(object.avgRating,0) + ")").length);
					$(".popupdialog2 .dialog-content .overalrating .ratingno").html($().formatNumber(object.avgRating,1));
					$(".popupdialog2 .dialog-content .overalrating .rating li:nth-child(" + $().formatNumber(object.avgRating,0) + ")").addClass("selected");
					$(".popupdialog2 .dialog-content .overalrating .rating li:nth-child(" + $().formatNumber(object.avgRating,0) + ")").prevAll().addClass("selected");
					$(".popupdialog2 .dialog-content .overalrating .overalratesec > li:last-child").text(object.totalRatingsValue + " Total");
					$(".popupdialog2 .dialog-content .tile-subtitles > li:nth-child(3)").remove();
					$(".popupdialog2 .dialog-content .overalrating-graph").loadlayout({layout:"ratinggraph",onlayoutcomplete:function(){
						$(".popupdialog2 .dialog-content .overalrating-graph").plotGraph(object.pointRatings);
					}});
				}
				$().fetchRatingReviews({resourceType:options.resourceType,resourceId:options.resourceId, onserverresponse:function(objects){
					if($().showLog()) console.log("rating reviews: " + JSON.stringify(objects));
					if(objects && objects.length > 0) {
						$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist").removeChildren();
						$().getlayout({layout:"rating-review-item",onserverresponse:function(layout){
							var loggedinuserid = $().loggedInUser();
							var reviewcounter = 0;
							$.each(objects,function(index,userrating){
								if(userrating.userId == loggedinuserid) {
									$(".popupdialog2 .dialog-content .myrating .myreviewrating li:nth-child(" + userrating.rating + ")").addClass("selected");
									$(".popupdialog2 .dialog-content .myrating .myreviewrating li:nth-child(" + userrating.rating + ")").prevAll().addClass("selected");
									$(".popupdialog2 .tabhoriz textarea[name='optional']").val(userrating.comment);
								}
								if(userrating.comment == null || userrating.comment.trim() == "") {
									return;
								}
								reviewcounter++;
								$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist").append(layout);
								$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist > li:last-child .reviewrgt .rating > li:nth-child(" + userrating.rating + ")").addClass("selected");
								$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist > li:last-child .reviewrgt .rating > li:nth-child(" + userrating.rating + ")").prevAll().addClass("selected");
								$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist > li:last-child p.content").text(userrating.comment);
								$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist > li:last-child .reviewrgt .tile-subtitles > li:last-child").text("(" + $().formatNumber(userrating.rating,1) + ")");
								if(userrating.userName) {
									$(".popupdialog2 .tabhoriz .ratingcontent .reviewlist > li:last-child .reviewlft .rightmenu > li:nth-child(1)").html(userrating.userName + "<br/><p class='schoolname'>" + userrating.schoolName + "</p>");
								}
							});
							$(".popupdialog2 .tabhoriz .reviewcount").text(reviewcounter);
						}});
					}
				}})
			}})
			if(options.onlayoutcomplete) {
				options.onlayoutcomplete();
			}
		}})
	}*/
	
	$.fn.resizeScroll = function() {
		if($(".activities-tray").is(":visible")) {
			$(".vscroll").addClass("contentscroll-bottom");
			return;
		}
		$(".vscroll").removeClass("contentscroll-bottom");
	}
	
	$.fn.addPresentationActivitiesToTray = function(options) {
		var screen = $().currentScreen();
		if(options && options.screen) {
			screen = options.screen;
		}
		var eventsource = $(this);
		var presentationId = $(this).closest("li[presentationid]").attr("presentationid");
		$().fetchPresentationTCESEQFileContents({id:presentationId,onserverresponse:function(presentationobj){
			if($().showLog()) console.log("presentation obj: " + presentationobj);
			presentationobj = $.parseJSON(presentationobj);
			$().updatePresentationResources(presentationobj);
			$().getlayout({layout:screen + "/trayactivity",onserverresponse:function(activitylayout){
				$.each(presentationobj.resources,function(resourceindex,resource){
					if(resource.type == "activity") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("activityid",resource.id);
						if(presentationId.startsWith("tp-") && !resource.id.startsWith("cact-")) {
							$().getActivitySnapshot({tpId:presentationId,activityId:resource.id,onserverresponse:function(activity){
								activity.activityId = resource.id;
								addActivityUIToTray(activitylayout,activity,"custom");
							}});
							return;
						}
						$().getActivityContent({activityId:resource.id,onserverresponse:function(activity){
							activity.activityId = resource.id;
							addActivityUIToTray(activitylayout,activity,"custom");
						}});
					}
					if(resource.type == "question") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("questionid",resource.id);
						addQuestionUIToTray(null,resource.id,null)
					}
					if(resource.type == "tool") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("toolid",resource.id);
						$().getToolById({toolId:resource.id,onserverresponse:function(object){
							if($().showLog()) console.log("tool: " + JSON.stringify(object));
							$().addToolToTray({toolid:resource.id,label:object.toolName});
						}});
					}
					if(resource.type == "game") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("gameid",resource.id);
						$().fetchgame({gameId:resource.id,onserverresponse:function(object){
							if($().showLog()) console.log("game: " + JSON.stringify(object));
							$().addGameToTray({gameid:resource.id,title:object.title});
						}});
					}
				});
				/*
				$().sortPresentationActivities(presentationobj);
				$.each(presentationobj.sequenceVo,function(activityindex,activityvo){
					if(presentationId.startsWith("tp-") && !activityvo.activityId.startsWith("cact-")) {
						$().getActivitySnapshot({tpId:presentationId,activityId:activityvo.activityId,onserverresponse:function(activity){
							activity.activityId = activityvo.activityId;
							addActivityUIToTray(activitylayout,activity,"custom");
						}});
						return;
					}
					$().getActivityContent({activityId:activityvo.activityId,onserverresponse:function(activity){
						activity.activityId = activityvo.activityId;
						addActivityUIToTray(activitylayout,activity,"custom");
					}});
				});*/
			}});
			$(".activities-tray").show();
		}})
		
		/*$().fetchPresentationFileContents({id:presentationId,filename:"tceseq.json",onserverresponse:function(presentationobj){
			if($().showLog()) console.log("presentation obj: " + presentationobj);
			presentationobj = $.parseJSON(presentationobj);
			$().updatePresentationResources(presentationobj);
			$().getlayout({layout:screen + "/trayactivity",onserverresponse:function(activitylayout){
				$.each(presentationobj.resources,function(resourceindex,resource){
					if(resource.type == "activity") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("activityid",resource.id);
						if(presentationId.startsWith("tp-") && !resource.id.startsWith("cact-")) {
							$().getActivitySnapshot({tpId:presentationId,activityId:resource.id,onserverresponse:function(activity){
								activity.activityId = resource.id;
								addActivityUIToTray(activitylayout,activity,"custom");
							}});
							return;
						}
						$().getActivityContent({activityId:resource.id,onserverresponse:function(activity){
							activity.activityId = resource.id;
							addActivityUIToTray(activitylayout,activity,"custom");
						}});
					}
					if(resource.type == "question") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("questionid",resource.id);
						addQuestionUIToTray(null,resource.id,null)
					}
					if(resource.type == "tool") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("toolid",resource.id);
						$().getToolById({toolId:resource.id,onserverresponse:function(object){
							if($().showLog()) console.log("tool: " + JSON.stringify(object));
							$().addToolToTray({toolid:resource.id,label:object.toolName});
						}});
					}
					if(resource.type == "game") {
						$(".activities-tray .tray-flyouts-slider").append(activitylayout);
						$(".activities-tray .tray-flyouts-slider li:last-child").attr("gameid",resource.id);
						$().fetchgame({gameId:resource.id,onserverresponse:function(object){
							if($().showLog()) console.log("game: " + JSON.stringify(object));
							$().addGameToTray({gameid:resource.id,title:object.title});
						}});
					}
				});
			}});
			$(".activities-tray").show();
		}});*/
		
	}
	function addActivityUIToTray(activitylayout,activity,activitytype,tpid) {
		var configurations = $().config();
		var mediaformat = configurations.mediaformats["default"];
		if(configurations.mediaformats[activity.type]) {
			mediaformat = configurations.mediaformats[activity.type];
		}
		if($(".activities-tray .tray-flyouts-slider > li[activityid='" + activity.activityId + "']").length <= 0) {
			$(".activities-tray .tray-flyouts-slider").append(activitylayout);
			$(".activities-tray .tray-flyouts-slider li:last-child").attr("activityid",activity.activityId);
			$(".activities-tray .tray-flyouts-slider li:last-child").show();
		}
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").attr("activitytype",activitytype);
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").attr("duration",activity.duration);
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").attr("resourceid",activity.activityId);
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").attr("resourcetype","activity");
		if(activitytype == "default") {
			$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").attr("tpid",tpid);
		}
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .sch-formaticons span").removeClass();
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .sch-formaticons span").addClass(mediaformat.format + "format");
		if(mediaformat.darkcolor) {
			$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .sch-formaticons").addClass(mediaformat.darkcolor);
		}		
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .tray-item-content h6").text(activity.title)
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] > a.tray-item-click").attr("data-tooltip",activity.title);
		
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .tray-item-content p:nth-child(2) > span:first-child").text((activity.duration? activity.duration : "") + " mins");
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .tray-item-content p:nth-child(2) > span:last-child").text(activity.activityType);
		var selectedClass = $().getSelectedClass();
		var selectedGradeObj = $().retrieveSelectedGradeObject(selectedClass.grade);
		console.log("selected grade obj: " + JSON.stringify(selectedGradeObj) + ", grade: " + selectedClass.grade);
		var selectedSubjectObj = null;
		$.each(selectedGradeObj.subjects,function(subjectindex,subject){
			if(subject.id == selectedClass.subject) {
				selectedSubjectObj = subject;
			}
		});
				
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .tray-item-content p:nth-child(3) > span:first-child").text(selectedGradeObj.label? selectedGradeObj.label : (selectedGradeObj.title? selectedGradeObj.title : "") );
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "'] .tray-item-content p:nth-child(3) > span:last-child").text(selectedSubjectObj.title);
		var totalduration = 0;
		$(".activities-tray .tray-flyouts-slider li[activityid][duration]").each(function(){
			totalduration += parseInt($(this).attr("duration"));
		})
		$(".activities-tray .tray-right .trayactions > li:first-child a").html("Duration <br/>" + totalduration + " mins");
		$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']").show();
		$(".activities-tray .tray-flyouts-slider > li:nth-child(n+13)").hide();
		$(".activities-tray").trigger("activitytray:itemadded",[$(".activities-tray .tray-flyouts-slider li[activityid='" + activity.activityId + "']")]);
	}
	$.fn.addActivityToTray = function(tpid,activityid,activitytype,screen) {
		if(!screen) {
			screen = $().currentScreen();
		}
		var eventsource = $(this);
		if(!activityid) {
			activityid = $(this).closest("[activityid]").attr("activityid");
		}
		if($().showLog()) console.log("add activity: " + activityid + " to tray,  activitytype: " + activitytype);
		if(!activitytype) {
			activitytype = $(this).closest("[type].tilesopen").attr("type");
		}
		/*
		if($(".activities-tray .tray-flyouts-slider li[activityid='" + activityid + "']").length > 0) {
			return;
		}*/

		$().getlayout({layout:screen + "/trayactivity",onserverresponse:function(activitylayout){
			if(activitytype == "default") {
				if(!tpid) {
					tpid = $(eventsource).closest("[tpid]").attr("tpid");
				}
				if($().showLog()) console.log("tpid: " + tpid + " to tray");
				$().getActivitySnapshot({tpId:tpid,activityId:activityid,onserverresponse:function(activitysnapshot){
					activitysnapshot.activityId = activitysnapshot.id;
					addActivityUIToTray(activitylayout,activitysnapshot,"default",tpid);
				}});
				return;
			}
			if(activitytype == "custom") {
				$().getActivityContent({activityId:activityid,onserverresponse:function(activity){
					activity.activityId = activityid;
					addActivityUIToTray(activitylayout,activity,"custom");
				}});
				return;
			}
			if(activitytype == "shared") {
				$().getActivityContent({activityId:activityid,onserverresponse:function(activity){
					activity.activityId = activityid;
					addActivityUIToTray(activitylayout,activity,"shared");
				}});
				return;
			}
		}});
	}
	$.fn.addQuizActivityToTray = function(tpid) {
		var screen = $().currentScreen();
		$().getDefaultQuestionsByTP({tpId:tpid,onserverresponse:function(questions){
			if($().showLog()) console.log("custom question for tpId: " + tpid + ", : " + JSON.stringify(questions));
			var questioncounter = 0;
			$.each(questions.data,function(questionindex,questiondata){
				questioncounter++;
				var questionxml = questiondata.xml;
				//questionxml = questionxml.substring(questionxml.indexOf("<question"));
				if($().showLog()) console.log("questionxml: " + questionxml);
				var questionxml = $.parseXML(questionxml);
				var questionuid = $(questionxml).find("question").attr("UID");
				$().addQuestionToTray(screen,questionuid,tpid);
			})
			$().getTestedgeQuestions({tpId:tpid,onserverresponse:function(testedgequestions){
				if($().showLog()) console.log("testedge questions for tpId: " + tpid + ", " + JSON.stringify(testedgequestions));
				if(testedgequestions.objects) {
					$.each(testedgequestions.objects,function(index,questiondata){
						questioncounter++;
						if($().showLog()) console.log("testedge question data: " + JSON.stringify(questiondata));
						//questiondata.questionData = $.parseXML(questiondata.questionData);
						//ui = "<tr quid=\"" + questiondata.questionId + "\"><td class=\"td1\">" + questioncounter + ".</td><td>" + $(questiondata.questionData).find("Question_Stem > Question_Text > Text").text() + "</td><td class=\"td3\"><ul class=\"actionicons\"><li><a href=\"javascript:void(0)\" class=\"testedgeicon\"><img src=\"/ce-static/images/testicon.png\" alt=\"\" class=\"mCS_img_loaded\"></a></li><li><a href=\"javascript:void(0);\" data-tooltip=\"Favourites\"><span class=\"fav\"></span></a></li><li><a href=\"javascript:void(0);\" class=\"question addtotray\" data-tooltip=\"Add to tray\"><span class=\"add\"></span></a></li><li><a href=\"javascript:void(0);\" data-tooltip=\"Preview\"><span class=\"preview\"></span></a></li></ul></td></tr>";
						//$(".contentscroll .scrolltolist li[tpid='" + tpId + "'] [type='default'] table.questionslist tbody").append(ui);
						//var questionuid = 
						$().addQuestionToTray(screen,questiondata.questionId,tpid);
					});
				}
				$(".contentscroll .scrolltolist li[tpid='" + tpid + "'] [type='default'] table.questionslist tbody").markFavourite();
				$().showFilterCriteria(screen);
				$().getPresentationInfo({tpId:tpid,onserverresponse:function(object){
					//object = $.parseJSON(object);
					if(object.toolid) {/*
						$().getlayout({layout:screen + "/activity",onserverresponse:function(activitylayout){
							$.each(object.toolid,function(appindex,toolid){
								$().getToolById({toolId:toolid,onserverresponse:function(object){
									$(this).addToolToTray({toolid:toolid,label:object.toolName});
								}})
							})
						}});
						//$(container).append("<li class=\"nocolor applist\"><ul class=\"clearfix apps-placholder\"></ul></li>");
					*/}
				}})
			}});
			
		}});
	}
	$.fn.addToolToTray = function(options) {
		if(!options || !options.screen) {
			screen = $().currentScreen();
		}
		$().getlayout({layout:screen + "/tray-tool",onserverresponse:function(traytoollayout){
			var trayitemcontainer = $("#lessonplanbody section.activities-tray .trayoutline .tray-left .tray-flyouts-slider");
			if($("#lessonplanbody section.activities-tray .trayoutline .tray-left .tray-flyouts-slider > li[toolid='" + options.toolid + "']").length <= 0) {
				$(trayitemcontainer).append(traytoollayout);
				$(trayitemcontainer).find("li:last-child").attr("toolid",options.toolid);
			}
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "'] .sch-formaticons").addClass("darkbgcolor13");
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "'] .sch-formaticons span").removeClass();
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "'] .sch-formaticons span").addClass("labedge");
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "']").attr("label",options.label);
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "']").attr("resourceid",options.toolid);
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "']").attr("resourcetype","tool");
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "']").attr("activitytype","tool");
			$(trayitemcontainer).find("li[toolid='" + options.toolid + "'] .tray-flyout .tray-flyout-content .tray-item-content h6").text(options.label);
			
			if($().showLog()) console.log("hiding extra tools");
			$(".activities-tray .tray-flyouts-slider > li:nth-child(n+13)").hide();
			$(".activities-tray").trigger("activitytray:itemadded",[$(trayitemcontainer).find("li[toolid='" + options.toolid + "']")]);
		}})
	}
	$.fn.addGameToTray = function(options) {
		if(!options || !options.screen) {
			screen = $().currentScreen();
		}
		$().getlayout({layout:screen + "/tray-game",onserverresponse:function(gamelayout){
			var trayitemcontainer = $("#lessonplanbody section.activities-tray .trayoutline .tray-left .tray-flyouts-slider");
			if($("#lessonplanbody section.activities-tray .trayoutline .tray-left .tray-flyouts-slider > li[gameid='" + options.gameid + "']").length <= 0) {
				$(trayitemcontainer).append(gamelayout);
				$(trayitemcontainer).find("li:last-child").attr("gameid",options.gameid);
			}
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "']").attr("gametitle",options.title);
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "']").attr("resourceid",options.gameid);
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "']").attr("resourcetype","game");
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "']").attr("activitytype","game");
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "'] .sch-formaticons").addClass("darkbgcolor12");
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "'] .sch-formaticons span").removeClass();
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "'] .sch-formaticons span").addClass("puzzel");
			$(trayitemcontainer).find("li[gameid='" + options.gameid + "'] .tray-flyout .tray-flyout-content .tray-item-content h6").text(options.title);
			$(".activities-tray .tray-flyouts-slider > li:nth-child(n+13)").hide();
			$(".activities-tray").trigger("activitytray:itemadded",[$(trayitemcontainer).find("li:last-child")]);
		}})
	}
	
	$.fn.plotGraph = function(data) {
		var defaultdata = {"5":0,"4":0,"3":0,"2":0,"1":0};
		defaultdata = $.extend(defaultdata,data);
		var container = $(this);
		var maxvalue = 0;
		$.each(defaultdata,function(key,value){
			$(container).find("[starid='" + key + "'] .change").text(value);
			if(value > maxvalue) {
				maxvalue = value;
			}
		})
		$.each(defaultdata,function(key,value){
			$(container).find("[starid='" + key + "'] .ex" + key).css("width",maxvalue/value*95 + "%");
		});
	}
	
	$.fn.showEditActivityPopup = function(options) {
		var eventsource = $(this);
		var activityId = $(this).closest("[activityid]").attr("activityid");
		$().getActivityContent({tpId:"",activityId:activityId,onserverresponse:function(object){
			$().showCreateActivityDialog(null,null,function(){
				if($().showLog()) console.log("activity obj for edit: " + JSON.stringify(object));
				if($(".activitypopupdialog .dialog-content .field-wrapper input[name='activityId']").length <= 0) {
					$(".activitypopupdialog .dialog-content .field-wrapper input[name='title']").after("<input name='activityId' type='hidden'/>");
				}
				$(".activitypopupdialog .dialog-content .field-wrapper input[name='activityId']").val(activityId);
				$(".activitypopupdialog .dialog-content .field-wrapper input[name='title']").val(object.title);
				$(".activitypopupdialog .dialog-content .field-wrapper select[name='type']").val(object.type);
				$(".activitypopupdialog .dialog-content .field-wrapper select[name='duration']").val(object.duration);
				$(".activitypopupdialog .dialog-content .field-wrapper input[name='tags']").val(object.tags);
				$(".activitypopupdialog .dialog-content .field-wrapper textarea[name='objectives']").val(object.objective);
				$(".activitypopupdialog .dialog-content .field-wrapper textarea[name='purpose']").val(object.purpose);
				$(".activitypopupdialog .dialog-content .field-wrapper textarea[name='outcome']").val(object.outcome);
				$(".activitypopupdialog .dialog-content .field-wrapper textarea[name='procedure']").val(object.procedure);
				$(".activitypopupdialog .dialog-content .field-wrapper textarea[name='hints']").val(object.hints);
				$(".activitypopupdialog .dialog-content .field-wrapper select[name='topic']").val(object.topic);
				$.each(object.resources,function(resourceindex,resource){
					resource.uploadresponse = {};
					resource.uploadresponse.filename = resource.fileName;
					resource.uploadresponse.filepath = resource.filepath;
					$().showAddedResource(resource);
				});
			});
		}})
	}

	$.fn.showActivitySnapshot = function(options){
		console.log("in show snapshot :"+ JSON.stringify(options));
		if(options.activityid.startsWith("cact-")){
			$().getActivityContent({tpId:options.presentationid,activityId:options.activityid,onserverresponse:function(activity){
				if(options.changeLanguages) {
					$(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection").showLanguages({tpId:options.presentationid,activityId:options.activityid});
				}
				$().showActivityContent(activity,options.presentationid);
			}});
			return;
		}
		$().getActivitySnapshot({tpId:options.presentationid,activityId:options.activityid,onserverresponse:function(activity){
			if(options.changeLanguages) {
				$(".activitypopupdialog .sub-chapter-details .sharewith .share-left .filterselection").showLanguages({tpId:options.presentationid,activityId:options.activityid});
			}
			$().showActivityContent(activity,options.presentationid);
		}});
	}
	
	$.fn.showActivityContent = function(activity,tpId){
		$(".activitypopupdialog").markPopupFavourite("activityid",activity.id);
		
		//$().getRating({resourceId:activityid,onserverresponse:function(objects){
		$().fetchRatingAnalytics({resourceType:"activity",resourceId:activity.id,onserverresponse:function(object){
			if(object) {
				var maxratingelem = $(".activitypopupdialog .tile-subtitles .rating li:nth-child(" + $().formatNumber(object.avgRating,0) + ")");
				$(maxratingelem).addClass("selected");
				$(maxratingelem).prevAll().addClass("selected");
				$(".activitypopupdialog .tile-subtitles > li:nth-child(2)").text("(" + $().formatNumber(object.avgRating,1) + ")");
			}
			if(!object) {
				$(".activitypopupdialog .tile-subtitles .rating").hide();
				if($(".activitypopupdialog .tile-subtitles .noratingpopup").length <= 0) {
					$(".activitypopupdialog .tile-subtitles .rating").before("<span class='noratingpopup'>(Be the first to Rate)</span>");
				}
				$(".activitypopupdialog .tile-subtitles > li:nth-child(2)").hide();
			}
		}});
		$().uploadUsage({resourceId:activity.id,resourceType:"activity"});
		if($().showLog()) console.log("activity for popup: " + JSON.stringify(activity));
		
		$(".activitypopupdialog .tile-titles h6").text(activity.title);
		//$(".activitypopupdialog .tile-subtitles > li:nth-child(2)").text("(" + activity.rating + ")");
		$(".activitypopupdialog .tile-subtitles > li:nth-child(3)").text(activity.duration + " mins");
		$(".activitypopupdialog .tile-subtitles > li:nth-child(4)").text(activity.activityType);
		
		$(".activitypopupdialog .formaticons span").removeClass();
		var configurations = $().config();
		var mediaformat = configurations.mediaformats["default"];
		if(configurations.mediaformats[activity.type]) {
			mediaformat = configurations.mediaformats[activity.type];
		}
		$(".activitypopupdialog .formaticons span").addClass(mediaformat.format + "format");
		$(".activitypopupdialog .formaticons > p").text(mediaformat.text);
		$(".activitypopupdialog .title-boxshadow .formaticons").removeClass().addClass("formaticons");
		if(mediaformat.darkcolor) {
			$(".activitypopupdialog .title-boxshadow .formaticons").addClass(mediaformat.darkcolor);
		}
		if($().showLog()) console.log("objective container: " + $(".activitypopupdialog .objectivecontainer").length + ", objective: '" + activity.objective + "'");
		$(".activitypopupdialog .objectivecontainer").removeChildren();
		if(activity.objective) {
			$(".activitypopupdialog .objectivecontainer").html(activity.objective);
		}
		$(".activitypopupdialog .instructionalpurposecontainer").removeChildren();
		if(activity.purpose) {
			$(".activitypopupdialog .instructionalpurposecontainer").html(activity.purpose);
		}
		$(".activitypopupdialog .outcomecontainer").removeChildren();
		if(activity.outcome) {
			$(".activitypopupdialog .outcomecontainer").html(activity.outcome);
		}
		$(".activitypopupdialog .procedurecontainer").removeChildren();
		if(activity.procedure) {
			$(".activitypopupdialog .procedurecontainer").html(activity.procedure);
		}
		$(".activitypopupdialog .hintscontainer").removeChildren();
		if(activity.hints) {
			$(".activitypopupdialog .hintscontainer").html(activity.hints);
		}
		$(".activitypopupdialog .preparationcontainer").removeChildren();
		if(activity.preparation) {
			$(".activitypopupdialog .preparationcontainer").html(activity.preparation);
		}
		var resources = []
		if(activity.resourceId && activity.resourceId.trim() != "") {
			resources = activity.resourceId.split(",");
		}
		$(".activitypopupdialog ul.sub-chapter-details-right").removeChildren();
		if(resources.length > 0) {
			$(".activitypopupdialog ul.sub-chapter-details-right").html("<li>Materials(" + resources.length + "):</li>");
		}
		$(".activitypopupdialog .sub-chapter-details-left div").correctContent();
		if($().showLog()) console.log("tpid value: "+ tpId);
		$(".activitypopupdialog .sub-chapter-details-left").updateTPImagePath(tpId);
		var anskeyids = [];
		var assetdetailscounter = 0;
		$.each(resources,function(index,resourceId){
			$().loadAssetDetails({assetId:resourceId.trim(),onserverresponse:function(assetdetails){
				assetdetailscounter++;
				var resourcetype = configurations.resourcetypes["default"];
				if(assetdetails != null && assetdetails.type) {
					resourcetype = configurations.resourcetypes[assetdetails.type];
				}
				if(assetdetails != null && assetdetails.mimeType) {
					resourcetype = configurations.resourcetypes[assetdetails.mimeType];
				}
				$(".activitypopupdialog .sub-chapter-details-right").append("<li resourceid='" + resourceId + "'><a href=\"javascript:void(0)\"><span class=\"" + resourcetype + "icon\" player=\"true\" resourceId=\"" + resourceId + "\"></span>" + (assetdetails != null? assetdetails.title : "") + "</a> <span>1 min</span></li>");
				if(assetdetails != null && assetdetails.assetType == "asset_print" && assetdetails.ansKeyId && assetdetails.ansKeyId != "") {
					anskeyids.push(assetdetails.ansKeyId);
				}
				if($().showLog()) console.log("assetdetailscounter: " + assetdetailscounter + ", resources: " + resources.length);
				if(assetdetailscounter == resources.length) {
					if($().showLog()) console.log("anskeyids length: " + anskeyids.length);
					$.each(anskeyids,function(index,anskeyid){
						if($().showLog()) console.log("anskeyid: " + anskeyid + ", length: " + $(".activitypopupdialog .sub-chapter-details-right li[resourceid='" + anskeyid + "']").length);
						$(".activitypopupdialog .sub-chapter-details-right li[resourceid='" + anskeyid + "']").remove();
					})
				}
			}})
		});
	}
	$.fn.showPresentationResourceCount = function() {
		/*var resourcelength = $(".activitypopupdialog .activityright .resourceitem > li[activityid]").length;
		resourcelength +=  $(".activitypopupdialog .activityright .resourceitem > li[questionids]").length;*/
		var resourcelength = $(".activitypopupdialog .activityright .resourceitem > li").length;
		if($().showLog()) console.log("resource length: " + resourcelength);
		$(".activitypopupdialog .activityright .activityfromhead .activitycount").html((resourcelength < 10)? "0" + resourcelength : resourcelength);
	}
	$.fn.showQuestionPreviewUI = function(question,quizxml,onlayoutcomplete) {
		var screen = "quiz";
		var source = $(this);
		var popupdialog = ".activitypopupdialog";
		if($(source).closest(".popupcontent").hasClass("popupdialog2")) {
			popupdialog = ".popupdialog2";
		}
		if($().showLog()) console.log("showing question preview for quid: " + $(question).attr("UID"));
		var layouttype = $().getQuestionLayout(question);
		$(source).loadlayout({layout:screen + "/quiz/" + layouttype,onlayoutcomplete:function(){
			var questionid = $(question).attr("UID");
			$().fetchQuestionResourcesDLFilePath({id:questionid,filename:"DUMMY",onserverresponse:function(dummyfilepath){
				if($().showLog()) console.log("dummy question resource dl filepath: " + dummyfilepath);
				
				var questionno = $(".questionslist tr[quid='" + questionid + "'] > td:first-child").text();
				if($().showLog()) console.log("question no.: " + questionno.replace("\.",""));
				var qind = parseInt(questionno.replace("\.",""));
				
				$(popupdialog + " .title-boxshadow .tile-titles h6").text((questionid.startsWith("cquest-")? "Custom Question " + qind : "Default Question " + qind));
				$(source).find(".question-part .questno").text((qind < 10? "0" + qind : qind));
				var questiontext = $(question).find("qtext").text();
				if($(question).find("qadd").length > 			0 && $(question).find("qadd").text().trim() != "") {
					questiontext += "<br/>" + $(question).find("qadd").text();
				}
				$(source).find(".question-block .questno").next().html(questiontext);
				$(source).find(".question-block .questno").next().find("img").each(function(){
					var filename = $(this).attr("src");
					if(!filename.startsWith("/delegate")) {
						$(source).find(".question-block .questno").next().find("img[src='" + filename + "']").attr("src","/delegate/fileservice/" + dummyfilepath.replace("DUMMY",filename));
						/*$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
							if($().showLog()) console.log("question id: " + questionid + ", filename: " + filename + ", filepath: " + filepath);
							$(source).find(".question-block .questno").next().find("img[src='" + filename + "']").attr("src","/delegate/fileservice/" + filepath);
						}})*/
					}
				})
				$(popupdialog).addImagePartInPreview(quizxml,question,layouttype,dummyfilepath);
				var optioncounter = 1;
				$(question).find("options > option").each(function(){
					$(source).find(".answer-block [option='" + optioncounter + "']").attr("qoption",optioncounter);
					$(source).find(".answer-block [option='" + optioncounter + "'] .answer").html($(question).find("options > option[id='" + optioncounter + "']").text());
					if($(this).attr("isCorrect") == "true") {
						$(source).find(".answer-block [option='" + optioncounter + "']").attr("correctAnswer","true");
					}
					optioncounter++;
				});
				
				$(source).find("img").each(function(){
					var filename = $(this).attr("src");
					if(!filename.startsWith("/delegate")) {
						$(source).find(".answer-block [option] .answer img[src='" + filename + "']").closest(".answer").addClass("imgcontainer");
						$(source).find(".answer-block [option] .answer img[src='" + filename + "']").attr("src","/delegate/fileservice/" + dummyfilepath.replace("DUMMY",filename));
						/*$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
							$(source).find(".answer-block [option] .answer img[src='" + filename + "']").closest(".answer").addClass("imgcontainer");
							$(source).find(".answer-block [option] .answer img[src='" + filename + "']").attr("src","/delegate/fileservice/" + filepath);
						}})*/
					}
					//$(this).closest(".answer-block").addClass("answer-imgblock answer-other");
				})
				//$(".activitypopupdialog").attr("prevquid",$(question).prev().attr("UID"));
				//$(".activitypopupdialog").attr("nextquid",$(question).next().attr("UID"));
				$(popupdialog + " input[name='showAnswer']").iCheck("uncheck");
				$(popupdialog + " input[name='showAnswer']").iCheck("check");
				if(onlayoutcomplete) {
					onlayoutcomplete();
				}
			}})
		}});
	}
	$.fn.addImagePartInPreview = function(quizxml,question,layouttype,resourcepath,questmediacontainer) {
		var container = ($(this) && $(this).hasClass("popupdialog2"))? ".popupdialog2" : ".activitypopupdialog";
		if($().showLog()) console.log("show image part in preview");
		var tpid = $(container).attr("tpid");
		var imagecontainer = container + " .questionarea .question-block";
		var questionid = $(question).attr("UID");
		var filename = $(question).find("media[src]").attr('src');
		if($(question).find("media[src]").length > 0) {
			filename = $(question).find("media[src]").attr('src');
			if(layouttype == "qtype1") {
				imagecontainer = container + " .questionarea .answer-block";
			}
			if(questmediacontainer) {
				imagecontainer = questmediacontainer;
			}
			if($(imagecontainer + " .image-part").length <= 0) {
				$(imagecontainer).append("<div class=\"image-part  animated fadeIn\"><object data=\"bookmark.swf\"></object></div>")
			}
			if(resourcepath && resourcepath != null) {
				$(imagecontainer + " .image-part object").remove();
				if(filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".gif")) {
					$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + resourcepath.replace("DUMMY",filename) + "\"/>");
					return;
				}
				$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + resourcepath.replace("DUMMY",filename) + "\"></object>")
			}
			if(!resourcepath || resourcepath == null) {
				$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
					$(imagecontainer + " .image-part object").remove();
					if(filepath.endsWith(".png") || filepath.endsWith(".jpg") || filepath.endsWith(".jpeg") || filepath.endsWith(".gif")) {
						$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + filepath + "\"/>");
						return;
					}
					$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + filepath + "\"></object>")
				}});
			}
			return;
		}
		if($(question).find("questmedia[src]").length > 0) {
			filename = $(question).find("questmedia[src]").attr('src');
			if(layouttype == "qtype1") {
				imagecontainer = container + " .questionarea .answer-block";
			}
			if(questmediacontainer) {
				imagecontainer = questmediacontainer;
			}
			
			if($(imagecontainer + " .image-part").length <= 0) {
				$(container + " .questionarea .question-block").append("<div class=\"image-part  animated fadeIn\"><object data=\"bookmark.swf\"></object></div>")
			}
			if(resourcepath && resourcepath != null) {
				$(imagecontainer + " .image-part object").remove();
				if(filename.endsWith(".png") || filename.endsWith(".jpg") || filename.endsWith(".jpeg") || filename.endsWith(".gif")) {
					$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + resourcepath.replace("DUMMY",filename) + "\"/>");
					return;
				}
				$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + resourcepath.replace("DUMMY",filename) + "\"></object>")
			}
			if(!resourcepath || resourcepath == null) {
				$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
					$(imagecontainer + " .image-part object").remove();
					if(filepath.endsWith(".png") || filepath.endsWith(".jpg") || filepath.endsWith(".jpeg") || filepath.endsWith(".gif")) {
						$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + filepath + "\"/>");
						return;
					}
					$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + filepath + "\"></object>");
				}});
			}
			return;
		}
		$(container + " .questionarea .question-part").addClass("full-part");
	}
	/*
	$.fn.addImagePartInPreview = function(quizxml,question,layouttype,questmediacontainer) {
		if($().showLog()) console.log("show image part in preview");
		var tpid = $(".activitypopupdialog").attr("tpid");
		var imagecontainer = ".activitypopupdialog .questionarea .question-block";
		var questionid = $(question).attr("UID");
		var filename = $(question).find("media[src]").attr('src');
		if($(question).find("media[src]").length > 0) {
			filename = $(question).find("media[src]").attr('src');
			if(layouttype == "qtype1") {
				imagecontainer = ".activitypopupdialog .questionarea .answer-block";
			}
			if(questmediacontainer) {
				imagecontainer = questmediacontainer;
			}
			if($(imagecontainer + " .image-part").length <= 0) {
				$(imagecontainer).append("<div class=\"image-part  animated fadeIn\"><object data=\"bookmark.swf\"></object></div>")
			}
			$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
				$(imagecontainer + " .image-part object").remove();
				if(filepath.endsWith(".png") || filepath.endsWith(".jpg") || filepath.endsWith(".jpeg") || filepath.endsWith(".gif")) {
					$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + filepath + "\"/>");
					return;
				}
				$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + filepath + "\"></object>")
			}});
			return;
		}
		if($(question).find("questmedia[src]").length > 0) {
			filename = $(question).find("questmedia[src]").attr('src');
			if(layouttype == "qtype1") {
				imagecontainer = ".activitypopupdialog .questionarea .answer-block";
			}
			if(questmediacontainer) {
				imagecontainer = questmediacontainer;
			}
			
			if($(imagecontainer + " .image-part").length <= 0) {
				$(".activitypopupdialog .questionarea .question-block").append("<div class=\"image-part  animated fadeIn\"><object data=\"bookmark.swf\"></object></div>")
			}
			$().fetchQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
				$(imagecontainer + " .image-part object").remove();
				if(filepath.endsWith(".png") || filepath.endsWith(".jpg") || filepath.endsWith(".jpeg") || filepath.endsWith(".gif")) {
					$(imagecontainer + " .image-part").append("<img src=\"/delegate/fileservice/" + filepath + "\"/>");
					return;
				}
				$(imagecontainer + " .image-part").append("<object data=\"/delegate/fileservice/" + filepath + "\"></object>")
			}});
			return;
		}
		$(".activitypopupdialog .questionarea .question-part").addClass("full-part");
	}*/
	
	$.fn.getQuestionLayout = function(question) {
		if(($(question).attr("type") == "SC" 
				&& $(question).attr("layout") == "3")) {
			return "qtype1";
		}
		
		if(($(question).attr("type") == "SC" && $(question).attr("layout") == "2")) {
			return "qtype2";
		}
		if(($(question).attr("type") == "SC" 
				&& $(question).attr("layout") == "4")) {
			return "qtype2";
		}
		if(($(question).attr("type") == "SC" 
				&& $(question).attr("layout") == "5")) {
			return "qtype3";
		}
		
		return "qtype1";
	}
	$.fn.showTestedgeQuestionPreviewUI = function(question,onlayoutcomplete) {
		var source = $(this);
		var screen = "quiz";
		var popupdialog = ".activitypopupdialog";
		if($(source).closest(".popupcontent").hasClass("popupdialog2")) {
			popupdialog = ".popupdialog2";
		}
		
		$(source).loadlayout({layout:screen + "/quiz/qtype1",onlayoutcomplete:function(){
			var questionid = $(question).find("Question").attr("question_id");
			var questionno = $(".questionslist tr[quid='" + questionid + "'] > td:first-child").text();
			if($().showLog()) console.log("question no.: " + questionno.replace("\.",""));
			var qind = parseInt(questionno.replace("\.",""));
			$(popupdialog + " .title-boxshadow .tile-titles h6").text("Default Question " + qind);
			$(source).find(".question-part .questno").text((qind < 10? "0" + qind : qind));
			var sourcetextcontainer = $(source).find(".question-block .questno").next();
			$(sourcetextcontainer).empty();
			$(question).find("Question_Stem > Question_Text > Text,Question_Stem > Question_Text  Image").each(function(){
				if($(this).prop("tagName") == "Text") {
					$(sourcetextcontainer).append($(this).text());
				}
				if($(this).prop("tagName") == "Image") {
					$(sourcetextcontainer).append("<img src='" + $(this).attr("src") + "'/>");
				}
			})
			//var questiontext = $(question).find("Question_Stem > Question_Text > Text").text();
			//$(source).find(".question-block .questno").next().html(questiontext);
			var type = $(question).find("Question").attr("template_Type");
			if($().showLog()) console.log("type: " + type);
			var optioncounter = 1;
			$(source).find(".answer-block [option]").show();
			if(type == "OPENENDEDSTEMONLY") {
				$(source).find(".answer-block [option]").hide();
			}
			if(type == "MCSS" || type == "MCMS" || type == "MCTWO" || type == "FIBSINGLE") {
				$(question).find("Choice_Option").each(function(){
					$(source).find(".answer-block [option='" + optioncounter + "']").attr("qoption",optioncounter);
					$(source).find(".answer-block [option='" + optioncounter + "'] .answer").html($(this).find("Text").text());
					if($(this).find("Image").length > 0) {
						$(source).find(".answer-block [option='" + optioncounter + "'] .answer").append("<img src='" + $(this).find("Image").attr("src") + "'/>");
					}
					if($(this).attr("correct") == "true") {
						$(source).find(".answer-block [option='" + optioncounter + "']").attr("correctAnswer","true");
					}
					optioncounter++;
				});
				if(type == "MCTWO") {
					$(source).find(".answer-block [option='3']").hide();
					$(source).find(".answer-block [option='4']").hide();
				}
				if(type == "FIBSINGLE") {
					$(question).find("Answer Blank").each(function(){
						$(source).find(".answer-block [option='" + optioncounter + "']").attr("qoption",optioncounter);
						$(source).find(".answer-block [option='" + optioncounter + "'] .answer").html($(this).find("Text").text());
						$(this).find("Image").each(function(){
							$(source).find(".answer-block [option='" + optioncounter + "'] .answer").append("<img src='" + $(this).attr("src") + "'/>");
						});
						if($(this).attr("correct") == "true") {
							$(source).find(".answer-block [option='" + optioncounter + "']").attr("correctAnswer","true");
						}
						optioncounter++;
					});
					$(source).find(".answer-block .choice-block ul > li[option]:nth-child(n + " + optioncounter + ")").hide();
				}
			}
			$(source).find("img[src]").each(function(){
				var imgelement = $(this);
				var filename = $(this).attr("src");
				$().streamTestedgqQuestionResourcesDLFilePath({id:questionid,filename:filename,onserverresponse:function(filepath){
					$(imgelement).attr("src","/delegate/fileservice/" + filepath);
				}})
			});
			$(popupdialog + " input[name='showAnswer']").iCheck("uncheck");
			$(popupdialog + " input[name='showAnswer']").iCheck("check");
			if(onlayoutcomplete) {
				onlayoutcomplete();
			}
		}});
	}
	
	
	$.fn.cleanHtmlForPrint = function(tpcontent,oncleanupcomplete) {
		var tempdiv = $(this);
		$(tempdiv).empty();
		tpcontent = tpcontent.replace(/&nbsp;/g," ");
		$(tempdiv).append(tpcontent);
		var xmldoc = $.parseXML("<test></test>");
		$(tempdiv).find(".refactivitylist").before("<p><ol class='refactivitylist'></ol></p>");
		$(tempdiv).find(".refactivitylist li[activityid]").each(function(){
			$(".popupdialog2 ol.refactivitylist").append("<li>" + $(this).find("h6.titlecontainer").text() + " (" + $(this).find(".formaticons p").text() + ")" + "</li>")
		});
		$(tempdiv).find("ul.refactivitylist").remove();
		
		$(tempdiv).find(".activitylist").before("<p><ol class='activitylist'></ol></p>");
		$(".popupdialog2 span.labelhead").remove();
		$(tempdiv).find(".activitylist li[activityid]").each(function(){
			$(".popupdialog2 ol.activitylist").append("<li>" + $(this).find("h6.titlecontainer").text() + " (" + $(this).find(".formaticons p").text() + ")" + "</li>")
		});
		$(tempdiv).find("ul.activitylist").remove();
		$(tempdiv).find(".notes-review").remove();
		$(tempdiv).find(".sub-chapters .titlediv,.sub-chapters .sch-formaticons,.sub-chapters .tile-subtitles,.sub-chapters .actionicons").remove();
		if($(tempdiv).attr("activityid") != "quiz") {
			$(tempdiv).find("p").each(function(){
				if($(this).closest(".notes-review").length > 0 || $(this).closest(".formaticons").length > 0 ) {
					return;
				}
				if($().showLog()) console.log("p - " + $(this).html());
				//$(this).html($(this).html().replace(/&nbsp;/g," "));
				//var cdatasec = xmldoc.createCDATASection($(this).html());
				//$(this).empty();
				//$(this).append(cdatasec);
			});
			$(tempdiv).find("ul.unorderedlist li,ol li").each(function(){
				if($(this).closest(".notes-review").length > 0) {
					return;
				}
				if($().showLog()) console.log("unorderedlist found...");
				if($().showLog()) console.log("li - " + $(this).html());
				$(this).attr("listitem","true");
				var ui = "" + $(this).html();
				ui = ui.replace("<em>"," <i>").replace("</em>","</i> ");
				if(config.debug) console.log("ui: " + ui);
				var cdatasec = xmldoc.createCDATASection(ui);
				$(this).html(cdatasec);
				//$(this).before("<p>h1</p>");
				//$(this).prev("p").html(cdatasec);
				//$(this).prev("p").html("<ul>" + $(this).html() + "</ul>");
				//$(this).remove();
			});
		}
		$(tempdiv).find("img").each(function(){
			var pretextnode = $(this).prev();
			var suftextnode = $(this).next();
			var imgnode = $(this);
			if(config.debug) console.log("prefix text node: " + $(pretextnode).text());
			if(config.debug) console.log("suffix text node: " + $(suftextnode).text());
			if($(pretextnode).text().trim() != "") {
				$(imgnode).before("<span>" + $(pretextnode).text() + "</span>");
			}
			if($(suftextnode).text().trim() != "") {
				$(imgnode).after("<span>" + $(suftextnode).text() + "</span>");
			}
		});
		if(config.debug) console.log("loading image dimensions: ...");
		$(tempdiv).loadImageDimensions({onloaddimensionscomplete:function(){
			var existingoptiontype = null;
			var existingimgcatindex = null;
			var imgheight = null;
			var ulelement = null;
			var imgelement = null;
			$(tempdiv).find("img").each(function(){
				imgheight = $(this).attr("actheight");
				imgelement = $(this);
				if($(this).closest("[option]").length > 0) {
					return;
				}
				existingimgcatindex = $(this).closest("ul").attr("imgcatindex");
				$.each(config.imagecategories,function(index,imgcat){
					if(imgheight >= imgcat.minheight && imgheight <= imgcat.maxheight && (!existingimgcatindex || index > existingimgcatindex)) {
						$(imgelement).attr("imgcatindex",index);
					}
				});
			});
			$(tempdiv).find("[option] .answer").each(function(){
				ulelement = $(this).closest("ul");
				existingoptiontype = $(this).closest("ul").attr("optiontype");
				existingimgcatindex = $(this).closest("ul").attr("imgcatindex");
				if(existingimgcatindex) {
					existingimgcatindex = parseInt(existingimgcatindex);
				}
				if($(this).attr("emptyline")) {
					$(this).closest("ul").attr("optiontype","exercise");
				}
				if($(this).find("img").length <= 0) {
					if(config.debug) console.log("text only options: html: " + $(this).html());
					if(!existingoptiontype) {
						$(this).closest("ul").attr("optiontype","textonly");
					}
				}
				if($(this).find("img").length > 0) {
					$(this).closest("ul").attr("optiontype","mixed");
					$(this).find("img").each(function(){
						imgheight = $(this).attr("actheight");
						var src = $(this).attr("src");
						if(config.debug) console.log("src: " + src + ", height: " + imgheight);
						$.each(config.imagecategories,function(index,imgcat){
							if(imgheight >= imgcat.minheight && imgheight <= imgcat.maxheight && (!existingimgcatindex || index > existingimgcatindex)) {
								$(ulelement).attr("imgcatindex",index);
							}
						});
					});
				}
			});
			tpcontent = $(tempdiv).htmlClean().html();
			if(oncleanupcomplete) {
				oncleanupcomplete(tpcontent);
			}
		}})
		/*var xmldoc = $.parseXML("<test></test>");
		tpcontent = $(tpcontent).html();*/
		//tpcontent = $(tempdiv).htmlClean().html();
		//return tpcontent;
	}
	$.fn.loadImageDimensions = function(options) {
		var container = $(this);
		if($(container).find("img").length == 0) {
			if(options && options.onloaddimensionscomplete) {
				options.onloaddimensionscomplete();
			}
			return;
		}
		$(container).find("img").each(function(){
			var src = $(this).attr("src")
			var img = $(this);
			$().getImageDimensions({imagesrc:src,onserverresponse:function(response){
				$(img).attr("actwidth",response.width);
				$(img).attr("actheight",response.height);
				if(config.debug) console.log("all img length: " + $(container).find("img").length + ", loaded length: " + $(container).find("img[actheight][actwidth]").length);
				if($(container).find("img").length == $(container).find("img[actheight][actwidth]").length) {
					if(options && options.onloaddimensionscomplete) {
						options.onloaddimensionscomplete();
					}
				}
			}});
		});
	}
	/*
	$.fn.manageTrayPrintIcon = function() {
		$(".activities-tray .tray-right .trayactions > li:nth-child(5) span").removeClass().addClass("noicon");
		if($(".activities-tray .tray-left .tray-flyouts-slider > li[questionid]").length == $(".activities-tray .tray-left .tray-flyouts-slider > li").length) {
			$(".activities-tray .tray-right .trayactions > li:nth-child(5) span").removeClass().addClass("card-icon-size icon-i-print");
		}
	}
	*/
});