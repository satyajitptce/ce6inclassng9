function showgrade(submitUrl){
	var url =submitUrl;
	callAjax('#gradeDivNew',url,null);
}


function onselectgrade(schoolGradeId){

	fsetvalue('blanksubDiv', 'subjectDiv', 'subjectDivBox', 'reflect');
	fsetvalue(schoolGradeId, 'month2TextImg4', 'month2DivImg', 'reflect');
	fsetvalue(schoolGradeId, 'classDiv', 'classDivBox', 'reflect');
	fsetvalue('blankDiv', 'lessonPDiv', 'attachLessonBox', 'reflect');
	fsetvalue('blankDiv', 'tpDiv', 'attachLessonBox', 'reflect');
	document.getElementById('schoolGradeId').value=schoolGradeId;
	document.getElementById('subjectId').value='';

}



function showsubject(submitUrl){
	var schoolGradeId = document.getElementById('schoolGradeId').value;
	var url =submitUrl;
	var data ={
			schoolGradeId:schoolGradeId
	};
	callAjax('#subjectDivNew',url,data);
}

function onselectsubject(subjectId){
	fsetvalue(subjectId, 'subTextImg4', 'subDivImg', 'reflect');
	fsetvalue(subjectId, 'subjectDiv', 'subjectDivBox', 'reflect');
	fsetvalue('blankDiv', 'lessonPDiv', 'attachLessonBox', 'reflect');
	fsetvalue('blankDiv', 'tpDiv', 'attachLessonBox', 'reflect');
	document.getElementById('subjectId').value=subjectId;
	document.getElementById('chapterId').value='';
}



function showlesson(submitUrl){
	var schoolGradeId = document.getElementById('schoolGradeId').value;
	var subjectId = document.getElementById('subjectId').value;
	if(schoolGradeId == ""){
		alert("Please select the Grade");
		return;
	}
	if(subjectId == ""){
		alert("Please select the Subject");
		return;
	}
	var url =submitUrl;
	var data = {
			schoolGradeId : schoolGradeId,
			subjectId : subjectId
	};
	callAjax('#lessonDiv',url,data);
}


function onselectlesson(chapterId,submitUrl){

	document.getElementById('chapterId').value=chapterId;
	var url =submitUrl;
	var data = {
			chapterId : chapterId
	};

	callAjax('#tpDiv',url,data);
}


function done(formName,submitUrl){
	var schoolGradeId = document.getElementById('schoolGradeId').value;
	var subjectId = document.getElementById('subjectId').value;
	var chapterId = document.getElementById('chapterId').value;
	var count = 0;
	var check = 0;
	if(schoolGradeId == ""){
		alert("Please select the Grade");
		return;
	}
	if(subjectId == ""){
		alert("Please select the Subject");
		return;
	}
	for(i=0; i<document.editForm.elements.length; i++){
		if (document.editForm.elements[i].checked==true) {
			var level =document.editForm.elements[i].id;
			if(level.indexOf("presentcheck~")==0){
				count++;
			}

		}
	}
	if (count<1) {
		if(chapterId == ""){
			alert("Please select the Chapter");
			return;
		}
	}
	if (chapterId != "") {
		for(i=0; i<document.editForm.elements.length; i++){
			if (document.editForm.elements[i].checked==true) {
				var level =document.editForm.elements[i].id;
				if(level.indexOf("tpcheck~")==0){
					check++;
				}
			}
		}
		if (check<1) {
			alert("Please select atleast one lesson");
			return;
		}
	}
	document.getElementById(formName).action = submitUrl;
	document.getElementById(formName).submit();
}



function popupPresentation(submitUrl, listValue) {
	var schoolGradeId = document.getElementById('schoolGradeId').value;
	var subjectId = document.getElementById('subjectId').value;
	var currentDate= document.getElementById('currentDate').value;
	var startTime= document.getElementById('startTime').value;
	var endTime= document.getElementById('endTime').value;
	if(schoolGradeId == ""){
		alert("Please select the Grade");
		return;
	}
	if(subjectId == ""){
		alert("Please select the Subject");
		return;
	}
	var url = submitUrl;
	var data={
			currentDate:currentDate,
			startTime:startTime,
			endTime:endTime,
			schoolGradeId:schoolGradeId,
			subjectId: subjectId
	};
	Liferay.ClassedgePopup.displayPopup(url,data,listValue);
	$.modal.close();
}



function check(formName,submitUrl){

	var chekedArr=new Array();
	var j=0;
	for(i=0; i<document.editForm.elements.length; i++){
		if(document.editForm.elements[i].type=="checkbox"){
			var level =document.editForm.elements[i].id;
			if(level.indexOf("tpcheck~")==0){
				if(document.editForm.elements[i].checked==true){
					chekedArr[j]=document.editForm.elements[i].id;
					j++;
				}
			}
		}
	}
	var str = document.getElementById('tplist').value;
	var temp = new Array();
	if(str != ''){
		temp = str.split(",");
	}
	var test=false;
	chekedArr.sort();
	for ( var i = 1; i < chekedArr.length; i++) {
		if (chekedArr[i] === chekedArr[i-1]) {
			chekedArr.splice(i--, 1);
		}
	}
	if(chekedArr.length!=temp.length){
		test = true;
	}else{
		var count =0;
		for(var i=0; i<chekedArr.length; i++){
			for(var j=0; j<temp.length; j++){
				var newString=temp[j];
				if("tpcheck~"+newString == chekedArr[i]){
					count++;
				}
			}
			if (i == chekedArr.length-1 && count == chekedArr.length) {
				test = false;
			}
			else if (i == chekedArr.length-1 && count != chekedArr.length) {
				test = true;
			}
		}
	}

	if(test==true){
		document.getElementById('addBox').style.display = "none";
		var hi= confirm("The Default presentation has not been saved. Do you want to save the current presentation ?");
		if (hi== true){
			done(formName,submitUrl);
		}

	}else{
		hideDiv('addBox');
		//toggle('addBox', 'add');
	}
}

function hideDiv(id){
	var onediv = document.getElementById(id);
	var divs=['classDivBox', 'subjectDivBox', 'addBox', 'attachLessonBox'];
	for (var i=0;i<divs.length;i++){
		if (id != divs[i]){
			document.getElementById(divs[i]).style.display='none';
		}
	}
	if(onediv.style.display == "block"){
		onediv.style.display = "none";
	}else{
		onediv.style.display = "block";
	}
}

function loadCpPlayer(presentationId, url) {
	url += "&referenceId=" + presentationId + "&mode=presentation";
	window
	.open(
			url,
			'popUpWindow',
	'height=768,width=1024,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no');
}

function loadCRSTest(qPaperId, url, crsSetUrl, aypId, myClassesUrl){
	var QPUrl = crsSetUrl.replace("DUMMY", "");
	var myClassUrl = myClassesUrl.replace("DUMMY", "");
	var data = {
			qPaperId : qPaperId
	};
	jQuery.ajax({
		type : 'POST',
		url : url,
		data: data,
		success : function(content) {
			window.quizObject = {
					"aypUrl" : "",
					"QPUrl" : QPUrl,
					"allQuestionsObj" : content,
					"aypId" : aypId,
					"qPaperId" : qPaperId,
					"myClassesUrl" : myClassUrl
				};
			window.open("/ce-static-testedge/quiz/quiz_clicker.html",
					'popUpWindow',
					'height=768,width=1024,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no');

		},
		error : function(jqXHR, textStatus, errorThrown) {}
	});
}

function show(startTimeStr, endTimeStr, GradeName, subjectName, subjectId, schoolGradeId, submitUrl) {
	var startTime;
	var endTime;
	var GradeName;
	var subjectName;
	var subjectId;
	var schoolGradeId;

	var url = submitUrl;
	var data = {
			startTime : startTimeStr,
			endTime : endTimeStr,
			GradeName : GradeName,
			subjectName : subjectName,
			subjectId : subjectId,
			schoolGradeId : schoolGradeId
	};
	callAjax('#calendarpopup', url, data);
	jQuery('#basic-modal-content').modal();

}

function loadGameCpPlayer(gameContentId, mode, url, subjectId, topicId) {
	var formHandle = document.forms["lPresentation"];
	url += "&referenceId=" + gameContentId + "&mode=" + mode
	+ "&subjectId=" + subjectId + "&topicId=" + topicId
	+ "&launchedFrom=presentation";
	formHandle.action = url;
	window
	.open(
			formHandle.action,
			'popUpWindow',
	'height=768,width=1024,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no');
}

function addPresentation(formName, url) {
	if(!$('input[type="checkbox"]').is(':checked')){
		alert("select atleast one checkbox");
		return;
	}
	document.getElementById(formName).action = url;
	document.getElementById(formName).submit();
}

function showTopic(id, relId, dropID, relid1, url, listUrl) {
	var temp = document.getElementById(id).innerHTML;
	var currentDate = document.getElementById('currentDate').value;
	var startTime = document.getElementById('startTime').value;
	document.getElementById('hsubjectId').value = id;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			subjectId : id,
			currentDate : currentDate,
			startTime : startTime
	};
	new LoadDataIntoGamesDiv(url, data, listUrl);
	document.getElementById('topicDivImg').style.display = 'block';
}

function showGames(id, relId, dropID, relid1, url) {
	var temp = document.getElementById(id).innerHTML;
	var currentDate = document.getElementById('currentDate').value;
	var startTime = document.getElementById('startTime').value;
	document.getElementById('htopicId').value = id;
	var subjectId = document.getElementById('hsubjectId').value;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			subjectId : subjectId,
			topicId : id,
			currentDate : currentDate,
			startTime : startTime
	};
	callAjax('#gamesDiv', url, data);

}



function showSubjectList(id, relId, dropID, relid1, url, listUrl) {
	var temp = document.getElementById(id).innerHTML;
	var currentDate = document.getElementById('currentDate').value;
	var startTime = document.getElementById('startTime').value;
	document.getElementById('hgradeId').value = id;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			gradeId : id,
			currentDate : currentDate,
			startTime : startTime
	};
	new LoadDataIntoPresentationDiv(url, data, listUrl);
	document.getElementById('topicDivImg').style.display = 'block';
}

function showPresentationList(id, relId, dropID, relid1, url) {
	var temp = document.getElementById(id).innerHTML;
	var currentDate = document.getElementById('currentDate').value;
	var startTime = document.getElementById('startTime').value;
	document.getElementById('hsubjectId').value = id;
	var gradeId = document.getElementById('hgradeId').value;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			gradeId : gradeId,
			subjectId : id,
			currentDate : currentDate,
			startTime : startTime
	};
	callAjax('#presentDiv', url, data);
}

function LoadDataIntoPresentationDiv(urlVal,data, listUrl) {

	this.baseUrl = urlVal;
	this.data = data;
		jQuery.getJSON(this.baseUrl, this.data, function(result){
			$("#jsonTopicDiv ul").empty();
			jQuery.each(result, function(index, item){
				var childId = item.key;
				$("#jsonTopicDiv ul").append('<li id="'+childId+'" class="attachListBoxbg1" onclick="fsetvalue(\''+childId+'\', \'topicTextImg4\', \'topicDivImg\', \'reflect\');showPresentationList(\''+childId+'\', \'topicTextImg4\', \'topicDivImg\', \'reflect\',\''+listUrl+'\');">'+item.value+'</li>');

			});
		});
}

function showSubjectDdList(id, relId, dropID, relid1, url, listUrl,chapterUrl) {

	var data = {
			gradeId : id
	};
	new LoadDataIntoSubjectDiv(url, data, listUrl,id,chapterUrl);
	document.getElementById('schoolsubjectdd').style.display = 'block';
}



function LoadDataIntoSubjectDiv(urlVal,data, listUrl,gradeId,chapterUrl) {
	this.baseUrl = urlVal;
	this.data = data;
		jQuery.getJSON(this.baseUrl, this.data, function(result){
			$("#schoolsubjectdd ul").empty();
		jQuery.each(result, function(index, item){
				var childId = item.key;
				$("#schoolsubjectdd ul").append('<li id="'+childId+'" onclick="fsetvaluewlen(\''+childId+'\', \'schoolsubjectddshell\', \'schoolsubjectdd\', \'reflect\',12);showLessonData(\''+childId+'\', \''+gradeId+'\' ,\''+listUrl+'\',\''+chapterUrl+'\');">'+item.value+'</li>');

			});
		});
}

function showLessonData(subjectId,gradeId,submitUrl,chapterUrl){

	var schoolGradeId = gradeId;
	var subjectId = subjectId;
	var url =submitUrl;
	var data = {
			schoolGradeId : schoolGradeId,
			subjectId : subjectId
	};
	new LoadDataIntoChapterDiv(url, data,chapterUrl);

}


function LoadDataIntoChapterDiv(urlVal,data,chapterUrl) {

	this.baseUrl = urlVal;
	this.data = data;
		jQuery.getJSON(this.baseUrl, this.data, function(result){
			$("#chapterListDiv ul").empty();
		  jQuery.each(result, function(index, item){
			     var childId = item.contentChapterTataPK.chapterId;
				 $("#chapterListDiv ul").append('<li id=\''+childId+'\' class="attachListBoxbg" style="width:235px;" onclick="fsetvalue(\''+childId+'\', \'lessonPDiv\', \'attachLessonBox\', \'reflect\');onselectlesson(\''+childId+'\',\''+chapterUrl+'\');">'+item.title+'</li>');

			});
		});
}

function LoadDataIntoGamesDiv(urlVal,data, listUrl) {

	this.baseUrl = urlVal;
	this.data = data;
		jQuery.getJSON(this.baseUrl, this.data, function(result){
			$("#jsonTopicDiv ul").empty();
			jQuery.each(result, function(index, item){
				var childId = item.key;
				$("#jsonTopicDiv ul").append('<li id="'+childId+'" class="attachListBoxbg1" onclick="fsetvalue(\''+childId+'\', \'topicTextImg4\', \'topicDivImg\', \'reflect\');showGames(\''+childId+'\', \'topicTextImg4\', \'topicDivImg\', \'reflect\',\''+listUrl+'\');">'+item.value+'</li>');

			});
		});
}

function toggleDiv(activeId, inActiveId) {
	if ($(activeId).hasClass('unchecked')) {
		$(activeId).toggleClass('checked unchecked');
		$(inActiveId).toggleClass('checked unchecked');
		if ($(activeId).hasClass('checked') && activeId.id == 'by_Title') {
			$('#titleDiv').show();
			$('#chapterDiv').hide();
		} else {
			$('#chapterDiv').show();
			$('#titleDiv').hide();
		}
	}
}

function filterDataBySearchText(divId, url){
	var currentDate = document.getElementById('currentDate').value;
	var startTime = document.getElementById('startTime').value;
	var title = document.getElementById('title').value;
	var data = {
		currentDate : currentDate,
		startTime : startTime,
		title : title
	};
	callAjax(divId, url, data);
}