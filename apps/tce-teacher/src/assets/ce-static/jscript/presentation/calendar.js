function calenderTable(showTable) {
		
		var childCurrentDate = document.getElementById("childCurrentDate").value;
		document.getElementById("displayDate").innerHTML = childCurrentDate;
		var arrayOfDate = childCurrentDate.split("-");
		arrayOfDate[1] = arrayOfDate[1]-1;
		document.getElementById("monthId").value = arrayOfDate[1];
		document.getElementById("yearId").value = arrayOfDate[2];
		printCalender(arrayOfDate[2], arrayOfDate[1], showTable);
		
	}

function calculateMonth(month, showTable) {
	document.getElementById("monthId").value = month;
	var year = document.getElementById("yearId").value;
	printCalender(year, month, showTable);

}

function calculateYear(year, showTable) {
	document.getElementById("yearId").value = year;
	var month = document.getElementById("monthId").value;
	printCalender(year, month, showTable);
}

function printCalender(year, month, id) {
	
	var childCurrentDate = document.getElementById("childCurrentDate").value;
	var arrayOfDate = childCurrentDate.split("-");
	var currDate = arrayOfDate[0];
	var currMonth = arrayOfDate[1]-1;
	var currYear = arrayOfDate[2];
		
	var days_in_month = new Array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31,
				30, 31);
	if (year % 4 == 0 && year != 1900) {
		days_in_month[1] = 29;
	}
	var beginDate = new Date(year, month, 1);
	if (beginDate.getDate() == 2) {
		beginDate = setDate(0);
	}
	beginDate = beginDate.getDay();
		
	var calenderBody = '<table width="100%" border="0" cellspacing="0" cellpadding="0" class="calenderTBL">';
	calenderBody += '<tr><th>S</th><th>M</th><th>T</th><th>W</th><th>T</th><th>F</th><th>S</th></tr><tr>';
	count = 0;
	for (i = 1; i <= beginDate; i++) {
		calenderBody += '<td class="blank">&nbsp;</td>';
		count++;
	}
	for (i = 1; i <= days_in_month[month]; i++) {
		
		if (count == 0) {
			calenderBody += '<tr>';
		}
        if(currDate == i && currMonth == month && currYear == year){
				calenderBody += '<div id="'+i+'"><td class="today1">' + i + '</td></div>';    	
		}
        else{
            	calenderBody += '<div id="'+i+'"><td onclick="goToDate(' + i + ','
				+ month + ',' + year + ');">' + i + '</td></div>';
        }
		count++;
		if (count == 7) {
			calenderBody += '</tr>';
			count = 0;
		}
	}
	for (i = 1; count != 0; i++) {
		calenderBody += '<td class="blank">&nbsp;</td>';
		count++;
		if (count == 7) {
			calenderBody += '</tr>';
			count = 0;
		}
	}
	calenderBody += '</table>';
	document.getElementById(id).innerHTML = calenderBody;
	}
function goToDate(dd, mm, yyyy) {
	var date = new Date(yyyy, mm, dd);
	var day = date.getDay();
	var days = ["Sunday","Monday","Tuesday","Wedne","Thurs","Friday","Satur"];
	if(dd < 10){
		dd = "0"+dd;
	}
	var month = mm+1;
	if(month < 10){
		month = "0"+month;
	}
	var format = dd+'-'+month+'-'+yyyy;
	for ( var i = 0; i < days.length; i++) {
		if(i == day){
			switchDate(days[i]);
			changeWeek(format);
			showSchedule(format);
		}
	}
}
	
function setMonth(textDiv, imgDiv, reflect) {
	
	var month = document.getElementById("monthId").value;
	fsetvalue(month, textDiv, imgDiv, reflect);
}

function setYear(textDiv, imgDiv, reflect) {
	
	var year = document.getElementById("yearId").value;
	fsetvalue(year, textDiv, imgDiv, reflect);
}
	
function changeToCurrentDate(){
	var element = document.getElementById("calenderBox");
	if(element.style.display = "block"){
		element.style.display = "none";
	}
	
}