// JavaScript Document
var baseUrl;
var currentSchoolId;
var monthId = new Date().getMonth() + 1;
var currentMonthId = 0;
var yearId = new Date().getFullYear();
var currentYearId = 0;
var prvMonthId = prvYearId = 0;
var monthDisplayArr = {"1":"Jan","2":"Feb","3":"Mar","4":"Apr",
				"5":"May","6":"Jun","7":"Jul","8":"Aug",
				"9":"Sep","10":"Oct","11":"Nov","12":"Dec"};
var monthRange=12;
var xmlData_arr = new Array();
var totalSchools;
var report_arr = new Array();
var error_arr = new Array();
var schoolName_arr = new Array();
var schoolAbbr_arr = new Array();
var state_arr = new Array();
var stateList_arr = ['Andaman and Nicobar Islands', 'Dadra and Nagar Haveli', 'Daman and Diu', 'Delhi', 'Goa', 'Lakshadweep', 'Pondicherry'];
var academic_arr = new Array();
var cityBarColor = null;
var citySelected = false;
var academicBarColor = null;
var academicSelected = false;

var contentLegend1 = "> 95%";
var contentLegend2 = "90 - 95%";
var contentLegend3 = "85 - 90%";
var contentLegend4 = "85 - 50%";
var contentLegend5 = "< 50%";
var contentLegend6 = "No data";

var connectivityLegend1 = "Reliable";
var connectivityLegend2 = "Moderate";
var connectivityLegend3 = "Sporadic";
var connectivityLegend4 = "No connectivity";

var ceUtilisationLegend1 = activeUserLegend1 = activeAssetLegend1 = capacityLegend1 = "No data";
var ceUtilisationLegend2 = activeUserLegend2 = activeAssetLegend2 = capacityLegend2 = "Poor";
var ceUtilisationLegend3 = activeUserLegend3 = activeAssetLegend3 = capacityLegend3 = "Inconsistent";
var ceUtilisationLegend4 = activeUserLegend4 = activeAssetLegend4 = capacityLegend4 = "Fair";
var ceUtilisationLegend5 = activeUserLegend5 = activeAssetLegend5 = capacityLegend5 = "Good";
var ceUtilisationLegend6 = activeUserLegend6 = activeAssetLegend6 = capacityLegend6 = "Excellent";

var india;
var selected_state = null;
var clicked_flag = false;
var selected_state_data = null;
var proj = d3.geo.mercator();
var path = d3.geo.path().projection(proj);
var t = proj.translate(); // the projection's default translation
var s = proj.scale(); // the projection's default scale

var schoolSearch = stateSearch = citySearch = academicSearch = contentSearch = connectivitySearch = 
	ceUtilisationSearch = activeUserSearch = activeAssetSearch = capacitySearch = null;
var totHeight = 0;
var counter = 0;
var csv = '';
var csv_arr = new Array();
var csvHeadings = '"School Abbreviation","School Name","Subject","Level","Type","Count","User Count","Subject List"\r\n';
var braekCSVLoop = false;

$(document).ready(function(e) {	
	$('#containerDiv').children().each(function(){	    	
    	totHeight += $(this).outerHeight();	    	
	});

	$('.preloaderStyle').css("height",totHeight);
	
	$( "#download_dialog" ).dialog({
      autoOpen: false,
      modal: true,
      resizable: false,
      height:100,
      width:400,
	  closeOnEscape: false,
	  close: function( event, ui ) {
		if(counter != schoolAbbr_arr.length) {
			braekCSVLoop = true;
		}
	  }
    });
	
	if(window.opener != null){
		baseUrl = window.opener.baseUrl;
	}

	loadXML(monthId, yearId);
	/*$.ajax({
		type:"POST",
		url:"xml/ceReport10.xml",
		dataType: "xml",
		success: parseXml,
		error: xmlIOError
	});*/
	
	// Stick the #nav to the top of the window
    var nav = $('#filterPanel');
   
    var navHomeY = nav.offset().top;
	var isFixed = true;
	    var $w = $(window);	    
	    $w.scroll(function() {  
	    	$('#viewsPanel').css('visibility', 'hidden'); 	
	        var scrollTop = $w.scrollTop();
	        var shouldBeFixed = scrollTop > navHomeY;
	        if (shouldBeFixed && !isFixed) {
	        	panelOffset = 30;
	            nav.css({
	                position: 'fixed',
	                top: "0",
	                zIndex:2,
	                left: nav.offset().left,
	                width: nav.width()
	            });
	            isFixed = true;
	        }
	        else if (!shouldBeFixed && isFixed)
	        {
	        	panelOffset=105;
	        	nav.css({
	                position: 'relative',
	                zIndex:1,
	                left:0
	                
	            });
	            isFixed = false;
	        }
	    });
	    
	    $w.click(function() {
	    	if($('#viewsPanel').css('visibility') != 'hidden') {
	    		$('#viewsPanel').css('visibility', 'hidden');
	    	}
	    });
		
		$("#monthOpt").on( "selectmenuchange", function( event, ui ) {
			$("#preloader").css("visibility","visible");
			setTimeout(function() { monthSelected(event) },500)
		});
});

function loadXML(month, year) {
	var newURL;
	if(baseUrl != undefined) {
		newURL = baseUrl.split('DUMMY').join('ceWideUsageReport')+'&monthId='+month+'&yearId='+year;
	} else {
		newURL = "xml/ceReport_"+year+"_"+month+".xml";
	}
	
	$.ajax({
		type:"POST",
		url: newURL,
		dataType: "xml",
		success: storeXML,
		error: xmlIOError
	});
}

function storeXML(xml) {
	/*var opt = $('<option value="'+monthId+"-"+yearId+'">'+monthDisplayArr[monthId]+"-"+yearId+'</option>');
	$("#monthOpt").append(opt);*/
	
	$("#monthOpt")
      .selectmenu()
      .selectmenu( "menuWidget" )
        .addClass( "overflow" );
	$("#monthOpt").selectmenu( "option", "width", 115 );
	
	monthRange = monthDiff(new Date(2013, 7, 1), new Date());
	//$("#monthOpt").attr('size',6);
	
	if(currentMonthId == 0) {
		currentMonthId = monthId;
		currentYearId = yearId;
		
		for(var i=monthRange; i > 0; i--) {
			var opt = $('<option value="'+monthId+"-"+yearId+'">'+monthDisplayArr[monthId]+"-"+yearId+'</option>');
			$("#monthOpt").append(opt);
			
			if(monthId == 1) {
				monthId = 12;
				yearId--;
			} else {
				monthId--;
			}
		}
		
		$('#monthOpt').selectmenu('refresh', true);
	} else {
		refreshPage();
	}
	
	var xmlObj = {id: currentMonthId+"-"+currentYearId, data: xml};
	xmlData_arr.push(xmlObj);
	
	parseXml(xml);
	
	/*xmlData_arr.push(xml);
	monthRange--;
	if(monthRange > 0) {
		if(monthId == 1) {
			monthId = 12;
			yearId--;
		} else {
			monthId--;
		}
		loadXML(monthId, yearId);
	} else {
		parseXml(xmlData_arr[0]);
	}*/
}

function xmlIOError(e){
	/*monthRange--;
	if(monthRange > 0) {
		if(monthId == 1) {
			monthId = 12;
			yearId--;
		} else {
			monthId--;
		}
		loadXML(monthId, yearId);
	} else {
		parseXml(xmlData_arr[0]);
	}*/
	if(currentMonthId == 0) {
		if(monthId == 1) {
			monthId = 12;
			yearId--;
		} else {
			monthId--;
		}
		loadXML(monthId, yearId);
	} else {
		var xmlObj = {id: currentMonthId+"-"+currentYearId, data: 'File Not Found'};
		xmlData_arr.push(xmlObj);
	
		alert('Report for '+monthDisplayArr[currentMonthId]+'-'+currentYearId+' is not available.');
		currentMonthId = prvMonthId;
		currentYearId = prvYearId;
		$('#monthOpt option[value="'+currentMonthId+'-'+currentYearId+'"]').prop('selected', true);
		$("#preloader").css("visibility","hidden");
	}
}	
	
function parseXml(xml){	
	var xmlData = xml;
	$(xmlData).find('ceWideReport').each(function () {
		var school_arr = new Array();
		
		if($(this).children().length != 0 && $(this).find("error").text() == '') {	
			school_arr.push((report_arr.length+1).toString());
			
			var usage = $(this).find("usage");
			var remark ="";
			/*var classroom = 0;
			var edgeroom = 0;
			if(usage.children().length != 0) {
				$(usage).find('resource').each(function () {
					var type = $(this).attr('type');
					if(['asset_annotation', 'default-presentation', 'presentation', 'chapter', 'tools', 'whiteboard', 'printprinted', 'quizprint'].indexOf(type) == -1) {
						classroom += Number($(this).attr('class'));
						edgeroom += Number($(this).attr('edge'));
					}
				});
			}*/
			
			var activeUser = $(this).find("userTypes").attr('active')
			var registeredUser = $(this).find("userTypes").attr('registered')
			var eligibliUser = $(this).find("userTypes").attr('eligible')
			
			if(eligibliUser == 0 && registeredUser != 0) {
				remark += "• Eligible users not defined\r\n</br>";
			}
			
			if(activeUser <= 1 && registeredUser != 0) {
				remark += "• No active usage for the month\r\n</br>"
			}
			
			//if(usage.children().length != 0 && $(this).find("schoolId").text() != '') {
			if(registeredUser != 0 && $(this).find("schoolId").text() != '') {
				school_arr.push('<a href="#" onclick="javascript:openSchoolReport(event, \''+$(this).find("schoolId").text()+'\')">'+$(this).find("schoolName").text()+'</a>');
				schoolAbbr_arr.push({name: $(this).find("schoolName").text(), id: $(this).find("schoolId").text()});
			} else {
				school_arr.push($(this).find("schoolName").text());
			}
			school_arr.push($(this).find("schoolId").text());
			
			var state;
			if($(this).find("state").text().indexOf('&') > -1) {
				state = $(this).find("state").text().split('&').join('and');
			} else {
				state = $(this).find("state").text();
			}
			school_arr.push(state);
			school_arr.push($(this).find("city").text());
			
			schoolName_arr.push($(this).find("schoolName").text()+", "+$(this).find("city").text()+", "+state);
			
			// Date http://blog.stevenlevithan.com/archives/date-time-format
			school_arr.push($(this).find("commission").attr('date'));
			school_arr.push($(this).find("academicManager").text());
			school_arr.push($(this).find("version").text());
							
			var activeUserIndex;
			if(Number($(this).find("userTypes").attr('eligible')) != 0) {
				activeUserIndex = getActiveUserIndex(Number($(this).find("userTypes").attr('active')), Number($(this).find("userTypes").attr('eligible')));
			} else {
				activeUserIndex = getActiveUserIndex(Number($(this).find("userTypes").attr('active')), Number($(this).find("userTypes").attr('registered')));
			}
				
			var activeAssetUsageIndex = getActiveAssetUsageIndex(Number($(this).find("activeUsage").text()), Number($(this).find("userTypes").attr('active')));
			//var activeAssetUsageIndex = getActiveAssetUsageIndex((edgeroom+classroom), Number($(this).find("userTypes").attr('active')));
			
			if(Number($(this).find("userTypes").attr('active')) == 0 && $(this).find("usage").children().length != 0) {
				activeUserIndex = activeAssetUsageIndex = 2;
			}
			
			var modewiseUsage = $(this).find("modewiseUsage");
			var usageTime = 0;
			if(modewiseUsage.children().length != 0) {
				$(modewiseUsage).find('terminalwiseUsage').each(function () {
					if($(this).attr('terminalType') != 'server') {
						usageTime += Number($(this).attr('usageTime'));
					}
				});
			}
			
			// 31 Oct '17 (optimization): Remove Edgeroom from the calculation	for ceTerminal
			// 31 Oct '17 (optimization): TCE active time constant of 40%	
			//var ceTerminal = Number($(this).find("terminals").attr('edge')) + Number($(this).find("terminals").attr('class')); // earlier calculation
			var ceTerminal =  Number($(this).find("terminals").attr('class'));
			var minutesPerDay = Number($(this).find("activeTimings").attr('minsOfDay'));
			
			if(minutesPerDay == 0 && registeredUser != 0) {
				remark += "• School timing not defined\r\n</br>"
			}
			minutesPerDay = (minutesPerDay == 0) ? 360 : (minutesPerDay < 0) ? minutesPerDay*-1: minutesPerDay;
			// 31 Oct '17: defining minimum minutesPerDay
			if (minutesPerDay < 240) {minutesPerDay = 360;} 
			
			
			var daysPerWeek = Number($(this).find("activeTimings").attr('noOfDaysOfWeek'));
			daysPerWeek = (daysPerWeek == 0) ? 5 : daysPerWeek;
			// 31 Oct '17: TCE active time constant of 40%
			// 31 Oct '17: holiday's added - average 3 holidays per month
			var holidays = 3;
			var daysPerMonth = (daysPerWeek * (30 / 7)) - holidays;
			var installCapacity = ceTerminal * minutesPerDay * daysPerMonth * 0.4;
			var capacityUtilisationIndex = getCapacityUtilisationIndex(usageTime, installCapacity);
				
			var oneCounter = 0;
			if((activeUserIndex-1) == 1) {
				oneCounter++;
			}
			if((activeAssetUsageIndex-1) == 1) {
				oneCounter++;
			}
			if((capacityUtilisationIndex-1) == 1) {
				oneCounter++;
			}
			
			var constant = 0;
			if(oneCounter == 1) {
				constant = 1;
			} else if(oneCounter == 2) {
				constant = 0.6;
			} else {
				constant = 0.31;
			}
			
			var overallCEUtilisationIndex;			
			var tceUsage = (0.4*(activeUserIndex-1))+(0.3*(activeAssetUsageIndex-1))+(0.3*(capacityUtilisationIndex-1))-(oneCounter*constant);
			if(tceUsage == 0) {
				overallCEUtilisationIndex = 1;
			} else if(tceUsage > 0 && tceUsage < 1) {
				overallCEUtilisationIndex = 2;
			} else if(tceUsage >= 1 && tceUsage < 2) {
				overallCEUtilisationIndex = 3;
			} else if(tceUsage >= 2 && tceUsage < 3) {
				overallCEUtilisationIndex = 4;
			} else if(tceUsage >= 3 && tceUsage <= 4) {
				overallCEUtilisationIndex = 5;
			} else {
				overallCEUtilisationIndex = 6;
			}
			
			/*console.log($(this).find("schoolName").text());
			console.log("activeUserIndex", activeUserIndex);
			console.log("activeAssetUsageIndex", activeAssetUsageIndex);
			console.log("capacityUtilisationIndex", capacityUtilisationIndex);
			console.log("overallCEUtilisationIndex", overallCEUtilisationIndex);*/
			
			
			if(usage.children().length == 0 && activeUser == 0 && registeredUser == 0 && eligibliUser == 0) {
				activeUserIndex = 1;
				activeAssetUsageIndex = 1;
				capacityUtilisationIndex = 1;
				overallCEUtilisationIndex = 1;
			}
			
			/*if(activeUserIndex == 1 || activeAssetUsageIndex == 1 || capacityUtilisationIndex == 1 || overallCEUtilisationIndex == 1) {
				activeUserIndex = 1;
				activeAssetUsageIndex = 1;
				capacityUtilisationIndex = 1;
				overallCEUtilisationIndex = 1;
			} else {
				//console.log($(this).find("schoolName").text()+" : "+activeUserIndex+" : "+activeAssetUsageIndex+" : "+capacityUtilisationIndex+" : "+oneCounter+" : "+constant+" : "+tceUsage+" : "+overallCEUtilisationIndex);
			}*/
			
			school_arr.push(getStatusLabel(overallCEUtilisationIndex));
			school_arr.push(getStatusLabel(activeUserIndex));
			school_arr.push(getStatusLabel(activeAssetUsageIndex));
			school_arr.push(getStatusLabel(capacityUtilisationIndex));
			
			school_arr.push($(this).find("userTypes").attr('registered').toString());
			school_arr.push($(this).find("userTypes").attr('eligible').toString());
			school_arr.push($(this).find("userTypes").attr('active').toString());
			
			var com = $(this).find("connectivity").attr('lastSynced');
			if(com.indexOf('IST') != -1) {
				var temp_arr = com.split(' ');
				var month_arr = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
				for(var i=0; i<month_arr.length; i++) {
					if(temp_arr[1] == month_arr[i]) {
						break;
					}
				}
				var month = i+1;
				if(month < 9) {
					month = "0"+month;
				} 
				com = temp_arr[temp_arr.length-1]+'-'+month+'-'+temp_arr[2]+' '+temp_arr[3];
				
			} 
			school_arr.push(com);
			
			school_arr.push($(this).find("terminals").attr('edge').toString());
			school_arr.push($(this).find("terminals").attr('class').toString());
			
			var availableTP = Number($(this).find("tp").attr('available'));
			var mappedTP = 	Number($(this).find("tp").attr('mapped'));
			var installedTP = Number($(this).find("tp").attr('installed'));
			var bookTP = Number($(this).find("tp").attr('book'));
			var coverage = Math.round((mappedTP/bookTP) * 10000)/100;;
			var tp_percentage = Math.round((installedTP/mappedTP) * 10000)/100;
			if(coverage == Infinity || isNaN(coverage)) {
				coverage = 0;
			}
			if(tp_percentage == Infinity || isNaN(tp_percentage)) {
				tp_percentage = 0;
			} else if(tp_percentage > 100) {
				tp_percentage = 100;
			} 
			var temp_arr = tp_percentage.toString().split('.');
			if(temp_arr.length == 1 && tp_percentage != 100 ) {
				tp_percentage = tp_percentage.toString() + ".00";
			} else if(temp_arr.length == 2 && temp_arr[1].length == 1) {
				tp_percentage = tp_percentage.toString() + "0";
			}
			
			school_arr.push(coverage.toString());
			school_arr.push(availableTP.toString());
			school_arr.push(mappedTP.toString());
			school_arr.push(installedTP.toString());
			school_arr.push(tp_percentage.toString());
			var healthNo;
			if(Number(tp_percentage) >= 95) {
				healthNo = 1;
			} else if(Number(tp_percentage) >= 90 && Number(tp_percentage) < 95) {
				healthNo = 2;
			} else if(Number(tp_percentage) >= 85 && Number(tp_percentage) < 90) {
				healthNo = 3;
			} else if(Number(tp_percentage) >= 50 && Number(tp_percentage) < 85) {
				healthNo = 4;
			} else if(Number(tp_percentage) >= 0.1 && Number(tp_percentage) < 50) {
				healthNo = 5;
			} else {
				healthNo = 6;
			}
			
			var classroom = 0;
			var edgeroom = 0;
			if(usage.children().length != 0) {
				$(usage).find('resource').each(function () {
					var type = $(this).attr('type');
					//if(['asset_annotation', 'default-presentation', 'presentation', 'chapter', 'tools', 'whiteboard', 'printprinted', 'quizprint'].indexOf(type) == -1) {
						classroom += Number($(this).attr('class'));
						edgeroom += Number($(this).attr('edge'));
					//}
				});
			}
			var terminal_usage = Math.round(((edgeroom+classroom)/ceTerminal)*100)/100;
			if(terminal_usage == Infinity || isNaN(terminal_usage)) {
				terminal_usage = 0;
			}
			school_arr.push(edgeroom.toString());
			school_arr.push(classroom.toString());
			//school_arr.push(terminal_usage.toString());
			
			updateAcademic($(this).find("academicManager").text(), (edgeroom+classroom));
			
			var user_ratio = find_ratio(Number($(this).find("userTypes").attr('registered')), Number($(this).find("userTypes").attr('active')));
			if(user_ratio.indexOf('NaN') > -1) {
				user_ratio = '';
			}
			//school_arr.push(user_ratio);
			
			var classUsageTime = Math.round(Number($(this).find("[terminalType='class']").attr('usageTime'))/Number($(this).find("terminals").attr('class')));
			var edgeUsageTime = Math.round(Number($(this).find("[terminalType='edge']").attr('usageTime'))/Number($(this).find("terminals").attr('edge')));
			
			school_arr.push(isNaN(edgeUsageTime) ? 0 : edgeUsageTime);
			school_arr.push(isNaN(classUsageTime) ? 0 : classUsageTime);
			
			var capacityUtilisation = Math.round((usageTime/installCapacity)*10000)/100; 
			school_arr.push(isNaN(capacityUtilisation) || capacityUtilisation == Infinity ? 0 : capacityUtilisation);
			
			var synced_no = Number($(this).find("connectivity").attr('count'));
			school_arr.push(synced_no);
			
			var connectivity;
			var connectivityNo;
			if(synced_no >= 40) {
				connectivity = connectivityLegend1;
				connectivityNo = 1;
			} else if(synced_no < 40 && synced_no > 20) {
				connectivity = connectivityLegend2;
				connectivityNo = 2;
			} else if(synced_no <= 20 && synced_no >= 5) {
				connectivity = connectivityLegend3;
				connectivityNo = 3;
			} else {
				connectivity = connectivityLegend4;
				connectivityNo = 4;
			}
			school_arr.push(connectivity);
			
			var languages;
			if($(this).find("languages").text() != '') {
				var lang_arr = $(this).find("languages").text().split(',');
				var temp_arr = new Array();
				for(var i=0; i<lang_arr.length; i++) {
					switch(lang_arr[i]) {
						case "en":
							temp_arr.push("English");
							break;
						case "ma":
							temp_arr.push("Marathi");
							break;
						case "hi":
							temp_arr.push("Hindi");
							break;
						case "ga":
							temp_arr.push("Gujarati");
							break;
					}
				}
				languages = temp_arr.join(", ");
			} else {
				languages = 'English';
			}
			school_arr.push(languages);
			school_arr.push(remark);
			
			report_arr.push(school_arr);
			
			updateStateData(state, healthNo, connectivityNo, overallCEUtilisationIndex, activeUserIndex, activeAssetUsageIndex, capacityUtilisationIndex);
		} else {
			school_arr.push((error_arr.length+1).toString());
			
			school_arr.push($(this).find("schoolName").text());
			
			var state;
			if($(this).find("state").text().indexOf('&') > -1) {
				state = $(this).find("state").text().split('&').join('and');
			} else {
				state = $(this).find("state").text();
			}
			school_arr.push(state);
			school_arr.push($(this).find("city").text());
			school_arr.push($(this).find("error").text());
			
			error_arr.push(school_arr);
		}
	});
	
	totalSchools = report_arr.length + error_arr.length;
	var maxSchoolCount = 0;
	for(var i=0; i<state_arr.length; i++) {
		if(state_arr[i].schoolCount > maxSchoolCount) {
			maxSchoolCount = state_arr[i].schoolCount;
		}
	}
	
	$('#maxSchool').html(maxSchoolCount);
	for(var i=0; i<state_arr.length; i++) {
		state_arr[i].percentage = Math.round(((state_arr[i].schoolCount/maxSchoolCount)*100)*100)/100;
	}
			
	renderPage();
}

function getActiveUserIndex(num1, num2) {
	var num =  Math.round((num1/num2)*10000)/100;
	num = (num == Infinity) ? 0 : (isNaN(num)) ? 0 : num;
	var index;
	if(num >= 75) {
		index = 6;
	} else if(num < 75 && num >= 60) {
		index = 5;
	} else if(num < 60 && num >= 50) {
		index = 4;
	} else if(num < 50 && num >= 40) {
		index = 3;
	} else if(num < 40 && num >= 0) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}

function getStatusLabel(index) {
	var label;
	switch(index) {
		case 1:
			label = ceUtilisationLegend1;
			break;
		case 2:
			label = ceUtilisationLegend2;
			break;
		case 3:
			label = ceUtilisationLegend3;
			break;
		case 4:
			label = ceUtilisationLegend4;
			break;
		case 5:
			label = ceUtilisationLegend5;
			break;
		default:
			label = ceUtilisationLegend6;
			break;
	}
	return label;
}

function getActiveAssetUsageIndex(num1, num2) {
	var num =  Math.round((num1/num2)*100)/100;
	num = (num == Infinity) ? 0 : (isNaN(num)) ? 0 : num;
	
	var index;
	if(num >= 75) {
		index = 6;
	} else if(num < 75 && num >= 50) {
		index = 5;
	} else if(num < 50 && num >= 30) {
		index = 4;
	} else if(num < 30 && num >= 22) {
		index = 3;
	} else if(num < 22 && num >= 0) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}

function getCapacityUtilisationIndex(num1, num2) {
	var num =  Math.round((num1/num2)*10000)/100;
	num = (num == Infinity) ? 0 : (isNaN(num)) ? 0 : num;
	var index;
	if(num >= 21) {
		index = 6;
	} else if(num < 21 && num >= 12) {
		index = 5;
	} else if(num < 12 && num >= 8) {
		index = 4;
	} else if(num < 8 && num >= 5) {
		index = 3;
	} else if(num < 5 && num >= 0) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}	

function updateAcademic(manager, usage) {
	var exist = false;
	for(var i=0; i<academic_arr.length; i++) {	
		if(academic_arr[i].name == manager) {
			exist = true;
			academic_arr[i].value++;
			academic_arr[i].usage += usage;
			break;
		}
	}

	if(!exist){
		var temp_arr = manager.split(' ');
		var label = temp_arr[0];
		if(temp_arr.length > 1) {
			label = temp_arr[0] + ' ' + temp_arr[1].substr(0,1);
		}
		
		var managerObj = {"label":label, "value":1, "usage":usage, "name":manager};
		academic_arr.push(managerObj);
	}
}

function updateStateData(state, health, connectivity, ceUtilisation, activeUser, activeAsset, capacity) {
	var exist = false;
	for(var i=0; i<state_arr.length; i++) {
		if(state_arr[i].id == state.split(' ').join('_')) {
			exist = true;
			break;
		}
	}

	if(!exist){
		var stateObj = {id:state.split(' ').join('_'), schoolCount:1, 
			health1:0, health2:0, health3:0, health4:0, health5:0, health6:0, 
			connectivity1:0, connectivity2:0, connectivity3:0, connectivity4:0,
			ceUtilisation1:0, ceUtilisation2:0, ceUtilisation3:0, ceUtilisation4:0, ceUtilisation5:0, ceUtilisation6:0,
			activeUser1:0, activeUser2:0, activeUser3:0, activeUser4:0, activeUser5:0, activeUser6:0,
			activeAsset1:0, activeAsset2:0, activeAsset3:0, activeAsset4:0, activeAsset5:0, activeAsset6:0,
			capacity1:0, capacity2:0, capacity3:0, capacity4:0, capacity5:0, capacity6:0};
		stateObj["health"+health] = 1;
		stateObj["connectivity"+connectivity] = 1;
		stateObj["ceUtilisation"+ceUtilisation] = 1;
		stateObj["activeUser"+activeUser] = 1;
		stateObj["activeAsset"+activeAsset] = 1;
		stateObj["capacity"+capacity] = 1;
		state_arr.push(stateObj);
	} else {
		state_arr[i].schoolCount++;
		
		switch(health) {
			case 1:
				state_arr[i].health1++;
				break;
			case 2:
				state_arr[i].health2++;
				break;
			case 3:
				state_arr[i].health3++;
				break;
			case 4:
				state_arr[i].health4++;
				break;
			case 5:
				state_arr[i].health5++;
				break;
			default:
				state_arr[i].health6++;
				break;
		}
		
		switch(connectivity) {
			case 1:
				state_arr[i].connectivity1++;
				break;
			case 2:
				state_arr[i].connectivity2++;
				break;
			case 3:
				state_arr[i].connectivity3++;
				break;
			default:
				state_arr[i].connectivity4++;
				break;
		}
		
		switch(ceUtilisation) {
			case 1:
				state_arr[i].ceUtilisation1++;
				break;
			case 2:
				state_arr[i].ceUtilisation2++;
				break;
			case 3:
				state_arr[i].ceUtilisation3++;
				break;
			case 4:
				state_arr[i].ceUtilisation4++;
				break;
			case 5:
				state_arr[i].ceUtilisation5++;
				break;
			default:
				state_arr[i].ceUtilisation6++;
				break;
		}
		
		switch(activeUser) {
			case 1:
				state_arr[i].activeUser1++;
				break;
			case 2:
				state_arr[i].activeUser2++;
				break;
			case 3:
				state_arr[i].activeUser3++;
				break;
			case 4:
				state_arr[i].activeUser4++;
				break;
			case 5:
				state_arr[i].activeUser5++;
				break;
			default:
				state_arr[i].activeUser6++;
				break;
		}
		
		switch(activeAsset) {
			case 1:
				state_arr[i].activeAsset1++;
				break;
			case 2:
				state_arr[i].activeAsset2++;
				break;
			case 3:
				state_arr[i].activeAsset3++;
				break;
			case 4:
				state_arr[i].activeAsset4++;
				break;
			case 5:
				state_arr[i].activeAsset5++;
				break;
			default:
				state_arr[i].activeAsset6++;
				break;
		}
		
		switch(capacity) {
			case 1:
				state_arr[i].capacity1++;
				break;
			case 2:
				state_arr[i].capacity2++;
				break;
			case 3:
				state_arr[i].capacity3++;
				break;
			case 4:
				state_arr[i].capacity4++;
				break;
			case 5:
				state_arr[i].capacity5++;
				break;
			default:
				state_arr[i].capacity6++;
				break;
		}
	}	
}

function renderPage() {
	$('#viwButton').click(function(e) {
		$('#viewsPanel').css('top', $('#viwButton').offset().top + $('#viwButton').height() + 10);
		$('#viewsPanel').css('left', ($('#viwButton').offset().left + $('#viwButton').width() + 12) - $('#viewsPanel').width());
		
		if($('#viewsPanel').css('visibility') == 'visible') {
			$('#viewsPanel').css('visibility', 'hidden');
		} else {
			$('#viewsPanel').css('visibility', 'visible');
		}
		
		e.stopPropagation();
	});
		
	$('#school').autocomplete({
		source: schoolName_arr,
		position: { my : "right top", at: "right bottom" },
		close: function( event, ui ) {
			serachClicked();
		}
	});
	
    $('#school').on('focus',function(){
      	if ( $(this).attr('placeholder') ) $(this).data('placeholder', $(this).attr('placeholder')).removeAttr('placeholder');
	}).on('blur', function(){
        if ( $(this).data('placeholder') ) $(this).attr('placeholder', $(this).data('placeholder')).removeData('placeholder');
    });
    
    $("#checkBoxDiv input[type=checkbox]").click(function(e) {
    	e.stopPropagation();
    	checkBoxClicked(e);
    });

	if(error_arr.length > 0) {
		$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools+' (Error Schools: '+error_arr.length+')');
	} else {
		$('#dataErrorTabelDiv').css("visibility","hidden");
		$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools);
	}
	
	
	var map = d3.select("#map").append("svg:svg")
	.attr("width", "600px")
	.attr("height", "800px")
	// .call(d3.behavior.zoom().on("zoom", redraw))
	.call(initialize);
	 
	india = map.append("svg:g")
	.attr("id", "india")
	.attr("class", "Blues");
	
	all_india = india.selectAll("path");
	
	d3.json("data/states.json", function (json) {
		var showValue = false;
		var state = india.selectAll("path")
		.data(json.features)
		.enter().append("svg:g")
		.attr("id", function(d){
	        return d.id.split(' ').join('_');
	   });
		
		var orgPath = state.append("path")
		.attr("d", path)
		.attr("class", quantize)
		.on("click", function(d) { stateClicked(d, this); });
		
	   state.append("svg:rect")
	   .attr("width", "30")
	   .attr("height", "20")
	   .attr("rx", "10")
	   .attr("ry", "10")
	   .attr("stroke", "grey")
	   .attr("stroke-width", "0.5")
	   .attr("style", "fill:rgb(255,255,255)")
	   .attr("opacity", function(d) {
			var opacity = "0";
			for(var i=0; i<stateList_arr.length; i++) {
				if(stateList_arr[i] == d.id) {
					return opacity;
				}
			}
			for(var j=0; j<state_arr.length; j++) {
	        	if(state_arr[j].id == d.id.split(' ').join('_')) {
	        		opacity = "1";
	        		break;
	        	}
	        }
	        return opacity;
	   })
	   .attr("x", function(d){
	        return path.centroid(d)[0] - 15;
	    })
	    .attr("y", function(d){
	        return  path.centroid(d)[1] - 10;
	    });
	    
		state.append("svg:text")
		.attr("text-anchor", "middle")
		.attr("style", "fill: #226B9E; font-weight: bold")
		.attr("x", function(d){
	        return path.centroid(d)[0];
	    })
	    .attr("y", function(d){
	        return  path.centroid(d)[1] + 5;
	    })
		.text(function(d){
			var schoolCount = '';
			for(var i=0; i<stateList_arr.length; i++) {
				if(stateList_arr[i] == d.id) {
					return schoolCount;
				}
			}
	        for(var j=0; j<state_arr.length; j++) {
	        	if(state_arr[j].id == d.id.split(' ').join('_')) {
	        		schoolCount = state_arr[j].schoolCount;
	        		break;
	        	}
	        }
	        return schoolCount;
	    });
	    
	    state.on("mouseover", stateMouseOver)
		.on("mouseout", stateMouseOut);
	});
	
	for(var i=0; i<stateList_arr.length; i++) {
		for(var j=0; j<state_arr.length; j++) {
			if(stateList_arr[i].split(' ').join('_') == state_arr[j].id) {
				$('#stateListDiv').append('<tr id="state'+i+'"><td class="stateColumn">'+stateList_arr[i]+'</td><td class="schoolCountColumn">'+state_arr[j].schoolCount+'</td></tr>');
				break;
			}
		}
	}
	
	var connectivity1 = connectivity2 = connectivity3 = connectivity4 = 0;
	var health1 = health2 = health3 = health4 = health5 = health6 = 0;
	var ceUtilisation1 = ceUtilisation2 = ceUtilisation3 = ceUtilisation4 = ceUtilisation5 = ceUtilisation6 = 0;
	var activeUser1 = activeUser2 = activeUser3 = activeUser4 = activeUser5 = activeUser6 = 0;
	var activeAsset1 = activeAsset2 = activeAsset3 = activeAsset4 = activeAsset5 = activeAsset6 = 0;
	var capacity1 = capacity2 = capacity3 = capacity4 = capacity5 = capacity6 = 0;
	
	for(var i=0; i<state_arr.length; i++) {
		health1 += state_arr[i].health1;
		health2 += state_arr[i].health2;
		health3 += state_arr[i].health3;
		health4 += state_arr[i].health4;
		health5 += state_arr[i].health5;
		health6 += state_arr[i].health6;
		
		connectivity1 += state_arr[i].connectivity1;
		connectivity2 += state_arr[i].connectivity2;
		connectivity3 += state_arr[i].connectivity3;
		connectivity4 += state_arr[i].connectivity4;
		
		ceUtilisation1 += state_arr[i].ceUtilisation1;
		ceUtilisation2 += state_arr[i].ceUtilisation2;
		ceUtilisation3 += state_arr[i].ceUtilisation3;
		ceUtilisation4 += state_arr[i].ceUtilisation4;
		ceUtilisation5 += state_arr[i].ceUtilisation5;
		ceUtilisation6 += state_arr[i].ceUtilisation6;
		
		activeUser1 += state_arr[i].activeUser1;
		activeUser2 += state_arr[i].activeUser2;
		activeUser3 += state_arr[i].activeUser3;
		activeUser4 += state_arr[i].activeUser4;
		activeUser5 += state_arr[i].activeUser5;
		activeUser6 += state_arr[i].activeUser6;
		
		activeAsset1 += state_arr[i].activeAsset1;
		activeAsset2 += state_arr[i].activeAsset2;
		activeAsset3 += state_arr[i].activeAsset3;
		activeAsset4 += state_arr[i].activeAsset4;
		activeAsset5 += state_arr[i].activeAsset5;
		activeAsset6 += state_arr[i].activeAsset6;
		
		capacity1 += state_arr[i].capacity1;
		capacity2 += state_arr[i].capacity2;
		capacity3 += state_arr[i].capacity3;
		capacity4 += state_arr[i].capacity4;
		capacity5 += state_arr[i].capacity5;
		capacity6 += state_arr[i].capacity6;
	}
	
	drawAcademicChart(academic_arr);

	  drawContentChart(health1, health2, health3, health4, health5, health6);
	  
	  drawConnectivityChart(connectivity1, connectivity2, connectivity3, connectivity4);
	  
	var donut_arr = new Array();
	donut_arr.push(createDonutObject('Overall CE Utilization', ceUtilisation1, ceUtilisation2, ceUtilisation3, ceUtilisation4, ceUtilisation5, ceUtilisation6));
	donut_arr.push(createDonutObject('Active User Index', activeUser1, activeUser2, activeUser3, activeUser4, activeUser5, activeUser6));
	donut_arr.push(createDonutObject('Active Asset Usage', activeAsset1, activeAsset2, activeAsset3, activeAsset4, activeAsset5, activeAsset6));
	donut_arr.push(createDonutObject('Capacity Utilization', capacity1, capacity2, capacity3, capacity4, capacity5, capacity6));
	
	drawDonutChart(donut_arr);

	var oTable = $('#report').dataTable( {
		"bPaginate": false,        
		"bLengthChange": false,        
		"bFilter": true,        
		"bSort": true,        
		"bInfo": false,        
		"bAutoWidth": true,
		"bDestroy": true,
		"sDom": 'Tfrtip',
		"oTableTools": {
			"sSwfPath": "../../css/ui-reports/images/copy_csv_xls_pdf.swf",
			"aButtons": [ {
					"sExtends": "csv",
					"sButtonText": "Export to CSV"
				}, ]
		},
		
        "aaData": report_arr,
        "aoColumns": [
            { "sTitle": "Sr. No.", "bSearchable": false, "bSortable": false },
            { "sTitle": "School Name" },
            { "sTitle": "School ID", "bSearchable": false, "bVisible": false },
            { "sTitle": "State" },
            { "sTitle": "City" },
            { "sTitle": "Commission Date", "bSearchable": false },
            { "sTitle": "Academic Manager" },
            { "sTitle": "ClassEdge Version", "bSearchable": false },
            { "sTitle": "Overall ClassEdge Utilization", "sClass": "center", "bSearchable": false },
            { "sTitle": "Active User Index", "sClass": "center", "bSearchable": false },
            { "sTitle": "Active Asset Usage", "sClass": "center", "bSearchable": false },
            { "sTitle": "Capacity Utilization", "sClass": "center", "bSearchable": false },
            { "sTitle": "No. of Registered Users", "sClass": "center", "bSearchable": false },
            { "sTitle": "No. of Eligible Users", "sClass": "center", "bSearchable": false },
            { "sTitle": "No. of Active Users", "sClass": "center", "bSearchable": false },
            { "sTitle": "Last synced date", "bSearchable": false },
            { "sTitle": "No. of Teacher terminals", "sClass": "center", "bSearchable": false },
            { "sTitle": "No. of Class room terminals", "sClass": "center", "bSearchable": false },
            { "sTitle": "% Content coverage", "sClass": "center", "bSearchable": false },
            { "sTitle": "Available Topics", "sClass": "center", "bSearchable": false },
            { "sTitle": "Mapped Topics", "sClass": "center", "bSearchable": false },
            { "sTitle": "Downloaded Topics", "sClass": "center", "bSearchable": false },
            { "sTitle": "Content Availability (%)", "sClass": "center" },
            { "sTitle": "TT CE Usage", "sClass": "center", "bSearchable": false },
            { "sTitle": "CT CE Usage", "sClass": "center", "bSearchable": false },
            { "sTitle": "TT Usage Time per terminal (min)", "sClass": "center", "bSearchable": false },
            { "sTitle": "CT Usage Time per terminal (min)", "sClass": "center", "bSearchable": false },
            { "sTitle": "% Capacity Utilisation", "sClass": "center", "bSearchable": false },
			{ "sTitle": "No. of Synced", "bSearchable": false },    
            { "sTitle": "Connectivity" },
            { "sTitle": "Languages", "bSearchable": false },
			{ "sTitle": "Remarks", "bSearchable": false }, 
        ],
        
        "sScrollY": "520px",
		"sScrollX": "100%",
		"bScrollCollapse": true,
		"bPaginate": false,
        "fnDrawCallback": function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered ) {
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ) {
					var nTr = oSettings.aoData[ oSettings.aiDisplay[i] ].nTr;

						// Update the index column and do so without redrawing the table
						this.fnUpdate( i+1, nTr, 0, false, false );
					}
				}
			},
		} );  
		
		var oErrorTable = $('#error').dataTable( {
			"bPaginate": false,        
			"bLengthChange": false,        
			"bFilter": false,        
			"bSort": false,        
			"bInfo": false,        
			"bAutoWidth": true,
			"bDestroy": true,
			"sDom": 'T<"clear">lfrtip',
			"oTableTools": {
				"sSwfPath": "../../css/ui-reports/images/copy_csv_xls_pdf.swf",
				"aButtons": [ {
						"sExtends": "csv",
						"sButtonText": "Export to CSV"
					}, ]
			},
			
			"aaData": error_arr,
			"aoColumns": [
				{ "sTitle": "Sr. No.", "bSearchable": false, "sClass": "center", "sWidth": "5%", "bSortable": false },
				{ "sTitle": "School Name", "bSearchable": false, "sWidth": "35%", "bSortable": false },
				{ "sTitle": "State", "bSearchable": false, "sWidth": "10%", "bSortable": false },
				{ "sTitle": "City", "bSearchable": false, "sWidth": "10%", "bSortable": false },
				{ "sTitle": "Error", "bSearchable": false, "sWidth": "40%", "bSortable": false },
			],
			
			"sScrollY": "520px",
			"bScrollCollapse": true,
			"bPaginate": false,
		} );  

    $("#preloader").css("visibility","hidden");
    
   	/*new FixedColumns( oTable, {
        "iLeftColumns": 4
    } );*/
    
	
	$("#download").click(function(event) {
		event.preventDefault();

		csv = '';
		for(var i=0; i < csv_arr.length; i++) {
			if(csv_arr[i].id == currentMonthId) {
				csv = csv_arr[i].data;
				break;
			}
		}
		
		if(csv != '') {
			$("#dwnBtnContainer").css("visibility","visible");
			$("#dwnImageContainer").css("visibility","hidden");
		} else {
			loadSchoolXML();
		}
		$( "#download_dialog" ).dialog( "open" );
	});
	
	$("#downloadCSV").click(function() {
		//var csvData = 'data:text/csv;charset=utf-8,\uFEFF' + encodeURIComponent(csvHeadings+csv);
		var filename = 'Content_Usage_Pattern_'+currentMonthId+'_'+currentYearId;
		
		var zip = new JSZip();
		zip.file(filename+'.csv', csvHeadings+csv, {compression :"DEFLATE"});
  
		$(this).attr({
			'download': filename+'.zip',
			'href': "data:application/zip;base64," + zip.generate(),
			'target': '_blank'
		});
		
		$( "#download_dialog" ).dialog( "close" );
		$("#dwnBtnContainer").css("visibility","hidden");
		$("#dwnImageContainer").css("visibility","visible");
	});
}

function find_ratio(x, y) {
   var gcd=calc(x,y);
   var r1=x/gcd;
   var r2=y/gcd;
   var ratio=r1+":"+r2;
   return ratio;
}
function calc(n1, n2) {
  var num1,num2;
  if(n1 < n2){ 
      num1=n1;
      num2=n2;  
   }
   else{
      num1=n2;
      num2=n1;
    }
  var remain=num2%num1;
  while(remain>0){
      num2=num1;
      num1=remain;
      remain=num2%num1;
  }
  return num1;
}

function drawCityChart(city_arr) {
	cityBarChart = [ 
	  {
	    key: "Cumulative Return",
	    values: city_arr
	  }
	];
	
	var barColor = [cityBarColor];
	if(!citySelected) {
		barColor = nv.utils.defaultColor();
	}
	
	nv.addGraph(function() {  
	  var chart = nv.models.discreteBarChart()
	      .x(function(d) { return d.label; })
	      .y(function(d) { return d.value; })
	      .staggerLabels(true)
	      .tooltips(false)
	      .showValues(true)
	      .valueFormat(d3.format("f"))
	      .color(barColor);
	      
	  chart.margin({left: 90, bottom: 60});
	  
	  chart.yAxis
	  	.tickFormat(d3.format('f'))
	  	.axisLabel('No. of Schools');
	
	  d3.select('#chart1')
	      .datum(cityBarChart)
		  .transition().duration(500)
	      .call(chart);
	
	  chart.discretebar.dispatch.on('elementClick', function(e) { cityElementClicked(e); });
	  nv.utils.windowResize(chart.update);
	  	
	  return chart;
	});
}

function drawAcademicChart(academic_arr) {
	academicBarChart = [ 
	  {
	    key: "Academic Manager",
	    values: academic_arr
	  }
	];
	
	var barColor = [academicBarColor];
	if(!academicSelected) {
		barColor = nv.utils.defaultColor();
	} 
	
	nv.addGraph(function() {  
	  var chart = nv.models.discreteBarChart()
	      .x(function(d) { return d.label; })
	      .y(function(d) { return d.value; })
	      .staggerLabels(true)
	      .tooltips(false)
	      .showValues(true)
	      .valueFormat(d3.format("f"))
	      .color(barColor);
	      
	  chart.margin({left: 100, bottom: 60});
	      
	  chart.yAxis
	  .axisLabel('No. of Schools')
	  .tickFormat(d3.format('f'));
	  
	  d3.select('#chart2')
	      .datum(academicBarChart)
		  .transition().duration(500)
	      .call(chart);
	
	  chart.discretebar.dispatch.on('elementClick', function(e) { academicElementClicked(e); });
	  nv.utils.windowResize(chart.update);
	  
	  var xTicks = d3.select('.nv-x.nv-axis > g').selectAll('g');
		xTicks
		  .selectAll('text')
		  .attr('transform', function(d,i,j) { return 'translate (-25, 25) rotate(-45 0,0)'; });
	
	  return chart;
	});
}

function drawContentChart(health1, health2, health3, health4, health5, health6) {
	var data1 = [
	    {
	      key: contentLegend1,
	      y: health1
	    },
	    {
	      key: contentLegend2,
	      y: health2
	    },
	    {
	      key: contentLegend3,
	      y: health3
	    },
	    {
	      key: contentLegend4,
	      y: health4
	    },
	    {
	      key: contentLegend5,
	      y: health5
	    },
	    {
	      key: contentLegend6,
	      y: health6
	    }
	  ];
	  
	var myColors = ["#009900", "#81db44", "#f9d426", "#ff6c00", "#d00e0d", "#c7c7c7"];
	d3.scale.myColors = function() {
    	return d3.scale.ordinal().range(myColors);
	};
	
	  nv.addGraph(function() {
	    var width = 450,
	        height = 250;
	
	    var chart = nv.models.pieChart()
	        .x(function(d) { return d.key; })
	        .y(function(d) { return d.y; })
	        .showLabels(false)
	        .showLegend(true)
	        .values(function(d) { return d; })
	        .color(d3.scale.myColors().range())
	        .width(width)
	        .height(height)
	        .valueFormat(d3.format('f'))
	        .tooltipContent(function(key, y, e, graph) { return '<h3>' + getSchoolPercentage(y) + '%</h3>' +'<p>' + y + ' Schools</p>'; });
		      
	      d3.select("#chart3")
	          .datum([data1])
	          .transition().duration(1200)
	          .attr('width', width)
	          .attr('height', height)
	          .call(chart);
	
	    chart.pie.dispatch.on('elementClick', function(e) { contentElementClicked(e); });
	    chart.legend.dispatch.on('legendClick', null);
	
	    return chart;
	});
}

function getSchoolPercentage(y) {
	return Math.round((y/totalSchools)*1000)/10;
}

function drawConnectivityChart(connectivity1, connectivity2, connectivity3, connectivity4) { 
	 var data2 = [
	    {
	      key: connectivityLegend1,
	      y: connectivity1
	    },
	    {
	      key: connectivityLegend2,
	      y: connectivity2
	    },
	    {
	      key: connectivityLegend3,
	      y: connectivity3
	    },
	    {
	      key: connectivityLegend4,
	      y: connectivity4
	    }
	  ];
	  
	  var myColors2 = ["#009900", "#f9d426", "#ff6c00", "#d00e0d"];
	  d3.scale.myColors2 = function() {
	  	return d3.scale.ordinal().range(myColors2);
	  };
	  
	  nv.addGraph(function() {
	    var width = 450,
	        height = 250;
	
	    var chart = nv.models.pieChart()
	        .x(function(d) { return d.key; })
	        .y(function(d) { return d.y; })
	        .showLabels(false)
	        .showLegend(true)
	        .values(function(d) { return d; })
	        .color(d3.scale.myColors2().range())
	        .width(width)
	        .height(height)
	        .valueFormat(d3.format('f'))
	        .tooltipContent(function(key, y, e, graph) { return '<h3>' + getSchoolPercentage(y) + '%</h3>' +'<p>' + y + ' Schools</p>'; });
	
	      d3.select("#chart4")
	          .datum([data2])
	          .transition().duration(1200)
	          .attr('width', width)
	          .attr('height', height)
	          .call(chart);
	
	    chart.pie.dispatch.on('elementClick', function(e) { connectivityElementClicked(e); });
	    chart.legend.dispatch.on('legendClick', null);
	
	    return chart;
	});
}

function drawUsageChart(usage_arr) {
	usageBarChart = [ 
	  {
	    key: "Cumulative Return",
	    values: usage_arr
	  }
	];
	
	nv.addGraph(function() {  
	  var chart = nv.models.discreteBarChart()
	      .x(function(d) { return d.label; })
	      .y(function(d) { return d.value; })
	      .staggerLabels(true)
	      .tooltips(false)
	      .showValues(true)
	      .valueFormat(d3.format("f"));
	
	  chart.yAxis.tickFormat(d3.format('f'));
	  
	  d3.select('#chart5')
	      .datum(usageBarChart)
		  .transition().duration(500)
	      .call(chart);
	
	  nv.utils.windowResize(chart.update);
	
	  return chart;
	});
}

function cityElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(citySelected) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(citySearch == null) {
		$("#filter").append('<div><button id="city" class="btnClass" onclick=reset(this)><span>'+e.point.label+'</span></button></div>');
	}
	citySearch = e.point.label;
	
	var color = nv.utils.getColor();
	cityBarColor = color(e.pointIndex, e.pointIndex);
	citySelected = true;
	
	filterDataTable();
	$("#preloader").css("visibility","hidden");
}

function academicElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null || academicBarColor != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(academicSearch == null) {
		$("#filter").append('<div><button id="academic" class="btnClass" onclick=reset(this)><span>'+e.point.label+'</span></button></div>');		
	}
	for(var i=0; i<academic_arr.length; i++) {
		if(academic_arr[i].label == e.point.label) {
			break;
		}
	}
	academicSearch = academic_arr[i].name;
	
	var color = nv.utils.getColor();
	academicBarColor = color(e.pointIndex, e.pointIndex);
	academicSelected = true;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function contentElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(contentSearch == null) {
		$("#filter").append('<div><button id="content" class="btnClass" onclick=reset(this)><span>Content Health: '+e.label+' TPs</span></button></div>');
	}	
	
	var expr;
	var healthNo;
	//http://www.myregextester.com/
	if(e.label == contentLegend1) {
		expr = "^(?:9(?:5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|100)$";
		healthNo = 1;
	} else if(e.label == contentLegend2) {
		expr = "^9(?:0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9]))$";
		healthNo = 2;
	} else if(e.label == contentLegend3) {
		expr = "^8(?:5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))$";
		healthNo = 3;
	} else if(e.label == contentLegend4) {
		expr = "^(?:5(?:0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|6(?:0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|7(?:0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|8(?:0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])))$";
		healthNo = 4;
	} else if(e.label == contentLegend5) {
		expr = "^(?:0\.(?:[1-9][0-9])|1(?:\.(?:[0-9][0-9])|0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|2(?:\.(?:[0-9][0-9])|0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|3(?:\.(?:[0-9][0-9])|0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|4(?:\.(?:[0-9][0-9])|0\.(?:[0-9][0-9])|1\.(?:[0-9][0-9])|2\.(?:[0-9][0-9])|3\.(?:[0-9][0-9])|4\.(?:[0-9][0-9])|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))|5\.(?:[0-9][0-9])|6\.(?:[0-9][0-9])|7\.(?:[0-9][0-9])|8\.(?:[0-9][0-9])|9\.(?:[0-9][0-9]))$";
		healthNo = 5;
	} else {
		expr = "^((0\.0[0-9])|0)$";
		healthNo = 6;
	}
	
	contentSearch = expr;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function connectivityElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(connectivitySearch == null) {
		$("#filter").append('<div><button id="connectivity" class="btnClass" onclick=reset(this)><span>Connectivity: '+e.label+'</span></button></div>');
	}
	connectivitySearch = e.label;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function ceUtilisationElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(ceUtilisationSearch == null) {
		$("#filter").append('<div><button id="ceUtilisation" class="btnClass" onclick=reset(this)><span>CE Utilization: '+e.status+'</span></button></div>');
	}
	ceUtilisationSearch = e.status;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function activeUserElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(activeUserSearch == null) {
		$("#filter").append('<div><button id="activeUser" class="btnClass" onclick=reset(this)><span>Active User Index: '+e.status+'</span></button></div>');
	}
	activeUserSearch = e.status;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function activeAssetElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(activeAssetSearch == null) {
		$("#filter").append('<div><button id="activeAsset" class="btnClass" onclick=reset(this)><span>Active Asset Usage: '+e.status+'</span></button></div>');
	}
	activeAssetSearch = e.status;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function capacityElementClicked(e) {
	$("#preloader").css("visibility","visible");
	if(schoolSearch != null) {
		$("#preloader").css("visibility","hidden");
		return;
	}
	
	if(capacitySearch == null) {
		$("#filter").append('<div><button id="capacity" class="btnClass" onclick=reset(this)><span>Capacity Utilization: '+e.status+'</span></button></div>');
	}
	capacitySearch = e.status;
	
	filterDataTable();
	
	$("#preloader").css("visibility","hidden");
}

function initialize() {
	proj.scale(5700);
	proj.translate([-1075, 645]);
}
 
 function stateMouseOver(d) {
 	var schoolCount = 0;
 	for(var i=0; i<state_arr.length; i++) {
		if(state_arr[i].id == d.id.split(' ').join('_')) {
			schoolCount = state_arr[i].schoolCount;
			break;
		}
	}
 	
 	d3.select("#tooltip")
		.transition().duration(200)
		.style("opacity", 1);
	d3.select("#tooltip")
		.html('<b>'+d.id+': '+schoolCount+'</b>')
		.style("left", (d3.event.pageX) + "px")     
		.style("top", (d3.event.pageY) + "px");
 }
 
 function stateMouseOut() {
	d3.select("#tooltip").transition()        
		.duration(500)      
		.style("opacity", 0);   
 }
 function stateClicked(d, state) {
 	$("#preloader").css("visibility","visible");
 	var stateFound = false;
 	for(var i=0; i<state_arr.length; i++) {
		if(state_arr[i].id == d.id.split(' ').join('_')) {
			stateFound = true;
			break;
		}
	}
	if(!stateFound) {
		$("#preloader").css("visibility","hidden");
		return;
	}

 	resetAll();
 	
 	if(schoolSearch != null) {
		stateSearch = null;
		citySearch = null;
		schoolSearch = null;
		$('#school').detach();
	}
 	if(stateSearch == null) {
 		$("#cityChartHolderDiv").addClass("barChartHolder");
 		$("#cityChartHolderDiv").append("<div class='chartTitleHolder'><div class='chartTitle'>Location</div></div><div id='cityChartDiv' class='barChart'><svg id='chart1'></svg></div>");
 		
 		$("#filter").append('<div><button id="state" class="btnClass" onclick=reset(this)><span>'+d.id+'</span></button></div>');
 	}
 	
 	d3.select("#tooltip").transition()        
		.duration(500)      
		.style("opacity", 0);

 	selected_state_data = d;
 	if(!clicked_flag) {
 		selected_state = state;
 		d3.select(state).classed("clicked", true);
 		india.selectAll("path").on("mouseout", null);
    	india.selectAll("path").on("mouseover", null);
 		clicked_flag = true;
 	} else {
 		old_state = selected_state;
    	selected_state = state;
    	d3.select(old_state).classed("clicked", false);
    	d3.select(state).classed("clicked", true);
    	all_india
    	.on("mouseover", stateMouseOver)
    	.on("mouseout", stateMouseOut);
 	}
 	stateMouseOver(selected_state_data);

	$('#regionTitleDiv').html(d.id+' - Total Schools: '+state_arr[i].schoolCount);
	
 	stateSearch = d.id;
 	
 	filterDataTable();
 	
 	$("#preloader").css("visibility","hidden");
 }
 
 function filterDataTable() {
 	var oTable = $('#report').dataTable();
  	
  	var oSettings = oTable.fnSettings();
	var colTitles = $.map(oSettings.aoColumns, function(node) {
		return node.sTitle;
	});
	
	var views = false;
	if($('#viewsPanel').css('visibility') != 'hidden') {
		views = true;
	}
	
	if(schoolSearch != null) {
		oTable.fnFilter(schoolSearch, colTitles.indexOf('School Name'));
	}
	if(stateSearch != null) {
		oTable.fnFilter(stateSearch, colTitles.indexOf('State'));
	}
	if(citySearch != null) {
		oTable.fnFilter(citySearch, colTitles.indexOf('City'));
	}
	if(academicSearch != null) {
		oTable.fnFilter(academicSearch, colTitles.indexOf('Academic Manager'));
	}
	if(contentSearch != null) {
		oTable.fnFilter(contentSearch, colTitles.indexOf('Content Availability (%)'), true);
	}
	if(connectivitySearch != null) {
		oTable.fnFilter(connectivitySearch, colTitles.indexOf('Connectivity'));
	}
	if(ceUtilisationSearch != null) {
		oTable.fnFilter(ceUtilisationSearch, colTitles.indexOf('Overall ClassEdge Utilization'));
	}
	if(activeUserSearch != null) {
		oTable.fnFilter(activeUserSearch, colTitles.indexOf('Active User Index'));
	}
  	if(activeAssetSearch != null) {
		oTable.fnFilter(activeAssetSearch, colTitles.indexOf('Active Asset Usage'));
	}
	if(capacitySearch != null) {
		oTable.fnFilter(capacitySearch, colTitles.indexOf('Capacity Utilization'));
	}
	
	if(views) {
		$('#viewsPanel').css('visibility', 'visible');
	}
	
	var myFilteredRows = oTable._('tr', {"filter":"applied"});
	
	var fconnectivity1 = fconnectivity2 = fconnectivity3 = fconnectivity4 = 0;
	var fhealth1 = fhealth2 = fhealth3 = fhealth4 = fhealth5 = fhealth6 = 0;
	var fceUtilisation1 = fceUtilisation2 = fceUtilisation3 = fceUtilisation4 = fceUtilisation5 = fceUtilisation6 = 0;
	var factiveUser1 = factiveUser2 = factiveUser3 = factiveUser4 = factiveUser5 = factiveUser6 = 0;
	var factiveAsset1 = factiveAsset2 = factiveAsset3 = factiveAsset4 = factiveAsset5 = factiveAsset6 = 0;
	var fcapacity1 = fcapacity2 = fcapacity3 = fcapacity4 = fcapacity5 = fcapacity6 = 0;
	
	var academic = new Array();
	var city_arr = new Array();
	var fstate_arr = new Array();
	totalSchools = myFilteredRows.length;
	
	for(var i=0; i<myFilteredRows.length; i++) {
		var state = myFilteredRows[i][colTitles.indexOf('State')];
		var exist = false;
		for(var j=0; j<fstate_arr.length; j++) {
			if(fstate_arr[j].id == state) {
				fstate_arr[j].value++;
				exist = true;
				break;
			}
		}
		if(!exist) {
			var stateObj = {"id":state, "value":1};
			fstate_arr.push(stateObj);
		}
		
		var city = myFilteredRows[i][colTitles.indexOf('City')];
		var exist = false;
		for(var j=0; j<city_arr.length; j++) {
			if(city_arr[j].label == city) {
				city_arr[j].value++;
				exist = true;
				break;
			}
		}
		if(!exist) {
			var cityObj = {"label":city, "value":1};
			city_arr.push(cityObj);
		}
		
		var manager = myFilteredRows[i][colTitles.indexOf('Academic Manager')];
		var tp_percentage = Number(myFilteredRows[i][colTitles.indexOf('Content Availability (%)')]);
		var usage = Number(myFilteredRows[i][colTitles.indexOf('CE Usage: Staff room mode')]) + Number(myFilteredRows[i][colTitles.indexOf('CE Usage: Class room mode')]);
		var exist = false;
		for(var j=0; j<academic.length; j++) {
			if(academic[j].name == manager) {
				academic[j].value++;
				academic[j].usage += usage;
				exist = true;
				break;
			}
		}
		if(!exist) {
			var temp_arr = manager.split(' ');
			var label = temp_arr[0];
			if(temp_arr.length > 1) {
				label = temp_arr[0] + ' ' + temp_arr[1].substr(0,1);
			}
			
			var managerObj = {"label":label, "value":1, "usage":usage, "name":manager};
			academic.push(managerObj);
		}	
		
		if(tp_percentage >= 95) {
			fhealth1++;
		} else if(tp_percentage >= 90 && tp_percentage < 95) {
			fhealth2++;
		} else if(tp_percentage >= 85 && tp_percentage < 90) {
			fhealth3++;
		} else if(tp_percentage >= 50 && tp_percentage < 85) {
			fhealth4++;
		} else if(tp_percentage >= 0.1 && tp_percentage < 50) {
			fhealth5++;
		} else {
			fhealth6++;
		}
		
		var connectivity = myFilteredRows[i][colTitles.indexOf('Connectivity')];
		switch(connectivity) {
			case connectivityLegend1:
				fconnectivity1++;
				break;
			case connectivityLegend2:
				fconnectivity2++;
				break;
			case connectivityLegend3:
				fconnectivity3++;
				break;
			default:
				fconnectivity4++;
				break;
		}
		
		var ceUtilisation = myFilteredRows[i][colTitles.indexOf('Overall ClassEdge Utilization')];
		switch(ceUtilisation) {
			case ceUtilisationLegend1:
				fceUtilisation1++;
				break;
			case ceUtilisationLegend2:
				fceUtilisation2++;
				break;
			case ceUtilisationLegend3:
				fceUtilisation3++;
				break;
			case ceUtilisationLegend4:
				fceUtilisation4++;
				break;
			case ceUtilisationLegend5:
				fceUtilisation5++;
				break;
			default:
				fceUtilisation6++;
				break;
			
		}
		
		var activeUser = myFilteredRows[i][colTitles.indexOf('Active User Index')];
		switch(activeUser) {
			case activeUserLegend1:
				factiveUser1++;
				break;
			case activeUserLegend2:
				factiveUser2++;
				break;
			case activeUserLegend3:
				factiveUser3++;
				break;
			case activeUserLegend4:
				factiveUser4++;
				break;
			case activeUserLegend5:
				factiveUser5++;
				break;
			default:
				factiveUser6++;
				break;
		}
		
		var activeAsset = myFilteredRows[i][colTitles.indexOf('Active Asset Usage')];
		switch(activeAsset) {
			case activeAssetLegend1:
				factiveAsset1++;
				break;
			case activeAssetLegend2:
				factiveAsset2++;
				break;
			case activeAssetLegend3:
				factiveAsset3++;
				break;
			case activeAssetLegend4:
				factiveAsset4++;
				break;
			case activeAssetLegend5:
				factiveAsset5++;
				break;
			default:
				factiveAsset6++;
				break;
		}
		
		var capacity = myFilteredRows[i][colTitles.indexOf('Capacity Utilization')];
		switch(capacity) {
			case capacityLegend1:
				fcapacity1++;
				break;
			case capacityLegend2:
				fcapacity2++;
				break;
			case capacityLegend3:
				fcapacity3++;
				break;
			case capacityLegend4:
				fcapacity4++;
				break;
			case capacityLegend5:
				fcapacity5++;
				break;
			default:
				fcapacity6++;
				break;
		}
	}
	
	for(var i=0; i<state_arr.length; i++) {
		var stateFound = false;
		for(var j=0; j <fstate_arr.length; j++) {
			if(state_arr[i].id == fstate_arr[j].id.split(' ').join('_')) {
				stateFound = true;
				break;
			}
		}
		
		var stateID = state_arr[i].id.split(' ').join('_');
		if(stateFound) {	
			$('#'+stateID+' text').text(fstate_arr[j].value);
			$('#'+stateID+' rect').attr('opacity', '1');
		} else {
			$('#'+stateID+' text').text('');
			$('#'+stateID+' rect').attr('opacity', '0');
		}
	}
	
	
	for(var i=0; i<stateList_arr.length; i++) {
		var stateID = stateList_arr[i].split(' ').join('_');
		$('#'+stateID+' text').text('');
		$('#'+stateID+' rect').remove();
		
		$('#state'+i).remove();
		
		for(var j=0; j <fstate_arr.length; j++) {
			if(stateList_arr[i] == fstate_arr[j].id) {
				$('#stateListDiv').append('<tr id="state'+i+'"><td class="stateColumn">'+stateList_arr[i]+'</td><td class="schoolCountColumn">'+fstate_arr[j].value+'</td></tr>');
				break;
			}
		}
	}
	
	if($('#stateListDiv tr').length == 1) {
		$('#stateListDiv').hide();
	} else {
		$('#stateListDiv').show();
	}
	
	if(stateSearch != null) {
		drawCityChart(city_arr);
	}
	drawAcademicChart(academic);
	
	drawContentChart(fhealth1, fhealth2, fhealth3, fhealth4, fhealth5, fhealth6);
	
	drawConnectivityChart(fconnectivity1, fconnectivity2, fconnectivity3, fconnectivity4);
		
	var donut_arr = new Array();
	donut_arr.push(createDonutObject('Overall CE Utilization', fceUtilisation1, fceUtilisation2, fceUtilisation3, fceUtilisation4, fceUtilisation5, fceUtilisation6));
	donut_arr.push(createDonutObject('Active User Index', factiveUser1, factiveUser2, factiveUser3, factiveUser4, factiveUser5, factiveUser6));
	donut_arr.push(createDonutObject('Active Asset Usage', factiveAsset1, factiveAsset2, factiveAsset3, factiveAsset4, factiveAsset5, factiveAsset6));
	donut_arr.push(createDonutObject('Capacity Utilization', fcapacity1, fcapacity2, fcapacity3, fcapacity4, fcapacity5, fcapacity6));
	
	$('#chart6').empty();
	drawDonutChart(donut_arr);
	
	var usage_arr = new Array();
	if(academicSearch == null) {
		for(var i=0; i<academic.length; i++) {
			var value = Math.round((academic[i].usage/academic[i].value)*100)/100;
			var managerObj = {"label":academic[i].label, "value":value};
			usage_arr.push(managerObj);
		}
	} else {
		for(var i=0; i<myFilteredRows.length; i++) {
			var school = myFilteredRows[i][colTitles.indexOf('School Name')];
			var usage = Number(myFilteredRows[i][colTitles.indexOf('CE Usage: Staff room mode')]) + Number(myFilteredRows[i][colTitles.indexOf('CE Usage: Class room mode')]);
			
			var schoolObj = {"label":school, "value":usage};
			usage_arr.push(schoolObj);
		}
	}
	drawUsageChart(usage_arr);
 }
 
 function reset(e) {
 	var filter = $(e).attr('id');
 	$(e).detach();
 	
 	var oTable = $('#report').dataTable();
 	var oSettings = oTable.fnSettings();
	var colTitles = $.map(oSettings.aoColumns, function(node) {
		return node.sTitle;
	});
	
 	switch(filter) {
 		case "state":
 			oTable.fnFilter('', colTitles.indexOf('State'));
 			stateSearch = null;
 			clicked_flag = false;
 			india.selectAll("path").on("mouseout", stateMouseOut);
    		india.selectAll("path").on("mouseover", stateMouseOver);
 			$("#cityChartHolderDiv").removeClass("barChartHolder");
 			$("#cityChartHolderDiv").empty();
    		d3.select(selected_state).classed("clicked", false);
    		
			totalSchools = report_arr.length + error_arr.length;
			if(error_arr.length > 0) {
				$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools+' (Error Schools: '+error_arr.length+')');
			} else {
				$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools);
			}
 			break;
 		case "city":
 			oTable.fnFilter('', colTitles.indexOf('City'));
 			citySearch = null;
 			citySelected = false;
 			cityBarColor = null;
 			break;
 		case "academic":
 			oTable.fnFilter('', colTitles.indexOf('Academic Manager'));
 			academicSearch = null;
 			academicSelected = false;
 			academicBarColor = null;
 			break;
 		case "content":
 			oTable.fnFilter('', colTitles.indexOf('Content Availability (%)'));
 			contentSearch = null;
 			break;
 		case "connectivity":
 			oTable.fnFilter('', colTitles.indexOf('Connectivity'));
 			connectivitySearch = null;
 			break;
 		case "schoolfilter":
 			oTable.fnFilter('', colTitles.indexOf('School Name'));
 			oTable.fnFilter('', colTitles.indexOf('State'));
 			oTable.fnFilter('', colTitles.indexOf('City'));
 			schoolSearch = null;
 			citySearch = null;
 			stateSearch = null;
 			break;
 		case "ceUtilisation":
 			oTable.fnFilter('', colTitles.indexOf('Overall ClassEdge Utilization'));
 			ceUtilisationSearch = null;
 			break;
 		case "activeUser":
			oTable.fnFilter('', colTitles.indexOf('Active User Index'));
			activeUserSearch = null;
			break;
		case "activeAsset":
			oTable.fnFilter('', colTitles.indexOf('Active Asset Usage'));
			activeAssetSearch = null;
			break;
		case "capacity":
			oTable.fnFilter('', colTitles.indexOf('Capacity Utilization'));
			capacitySearch = null;
			break;
 	}
 	
 	filterDataTable();
 }
 
 function resetAll() {
 	clicked_flag = false;
 	india.selectAll("path").on("mouseout", stateMouseOut);
    india.selectAll("path").on("mouseover", stateMouseOver);
    	
 	$("#cityChartHolderDiv").removeClass("barChartHolder");
 	$("#cityChartHolderDiv").empty();
    d3.select(selected_state).classed("clicked", false);
    
    var oTable = $('#report').dataTable();
 	var oSettings = oTable.fnSettings();
	var colTitles = $.map(oSettings.aoColumns, function(node) {
		return node.sTitle;
	});
    if(schoolSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('School Name'));
		schoolSearch = null;
	}
	if(stateSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('State'));
		stateSearch = null;
	}
	if(citySearch != null) {
		oTable.fnFilter('', colTitles.indexOf('City'));
		citySearch = null;
	}
	if(academicSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Academic Manager'));
		academicSearch = null;
	}
	if(contentSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Content Availability (%)'));
		contentSearch = null;
	}
	if(connectivitySearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Connectivity'));
		connectivitySearch = null;
	}
	if(ceUtilisationSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Overall ClassEdge Utilization'));
		ceUtilisationSearch = null;
	}
	if(activeUserSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Active User Index'));
		activeUserSearch = null;
	}
	if(activeAssetSearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Active Asset Usage'));
		activeAssetSearch = null;
	}
	if(capacitySearch != null) {
		oTable.fnFilter('', colTitles.indexOf('Capacity Utilization'));
		capacitySearch = null;
	}
 	
    cityBarColor = null;
	citySelected = false;
	academicBarColor = null;
	academicSelected = false;
    $("#filter").empty();
    
	totalSchools = report_arr.length + error_arr.length;
	if(error_arr.length > 0) {
		$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools+' (Error Schools: '+error_arr.length+')');
	} else {
		$('#regionTitleDiv').html('All India - Total Schools: '+totalSchools);
	}
			
 	filterDataTable();
 }
 
 function serachClicked() {
 	if($("#school").val() == '') {
 		return;
 	}
 	
 	var schoolFound = false;
 	for(var i=0; i<schoolName_arr.length; i++) {
 		if($("#school").val() == schoolName_arr[i]) {
 			schoolFound = true;
 			break;
 		}
 	}
 	if(!schoolFound) {
 		alert('School not found.');
 		return;
 	}
 	
 	resetAll();	
 	
 	var schoolName = $("#school").val().split(", ");
 	$("#school").val('');
 	 	
 	if(schoolSearch == null) {
		$("#filter").append('<div><button id="schoolfilter" class="btnClass" onclick=reset(this)><span>'+schoolName.join(', ')+'</span></button></div>');
	}
	
	schoolSearch = schoolName[0];
	
	citySearch = schoolName[1];
	
	stateSearch = schoolName[2];
	
	filterDataTable();
 }


function quantize(d) {
	var percentage = 0; 
    for(var i=0; i<state_arr.length; i++) {
    	if(state_arr[i].id == d.id.split(' ').join('_')) {
			percentage = state_arr[i].percentage;
			break;
		}
	}
    color = "q" + Math.min(8, ~~(percentage / 12.5)) + "-9";
	return color;
 }
 
function monthSelected(e) {
	prvMonthId = currentMonthId;
	prvYearId = currentYearId;
	var temp_arr = e.target.value.split('-');
	currentMonthId = temp_arr[0];
	currentYearId = temp_arr[1];
	
	var xmlStrored = false;
	for(var i=0; i<xmlData_arr.length; i++) {
		if(xmlData_arr[i].id == (currentMonthId+'-'+currentYearId)) {
			xmlStrored = true;
			break;
		}
	}
	
	if(xmlStrored) {
		if(xmlData_arr[i].data != 'File Not Found') {
			refreshPage();
			parseXml(xmlData_arr[i].data);
		} else {
			alert('Report for '+monthDisplayArr[currentMonthId]+'-'+currentYearId+' is not available.');
			currentMonthId = prvMonthId;
			currentYearId = prvYearId;
			$('#monthOpt option[value="'+currentMonthId+'-'+currentYearId+'"]').prop('selected', true);
		}
		$("#preloader").css("visibility","hidden");
	} else {
		loadXML(currentMonthId, currentYearId);
	}
		
	/*parseXml(xmlData_arr[e.target.selectedIndex]);
	$("#preloader").css("visibility","hidden");*/
}

function refreshPage() {
	resetAll();
	
	for(var i=$('#stateListDiv tr').length-1; i>0; i--) {
		$('#stateListDiv tr')[i].remove();
	}
	$('#map').empty();
	$('#chart6').empty();
	
	var oTable = $('#report').dataTable();
	oTable.fnClearTable();
	var oErrorTable = $('#error').dataTable();
	oErrorTable.fnClearTable();
	
	report_arr = new Array();
	error_arr = new Array();
	schoolName_arr = new Array();
	schoolAbbr_arr = new Array();
	state_arr = new Array();
	academic_arr = new Array();
	cityBarColor = null;
	citySelected = false;
	academicBarColor = null;
	academicSelected = false;
	
	selected_state = null;
	clicked_flag = false;
	selected_state_data = null;

	schoolSearch = stateSearch = citySearch = academicSearch = contentSearch = connectivitySearch = 
	ceUtilisationSearch = activeUserSearch = activeAssetSearch = capacitySearch = null;
	totHeight = 0;
	counter = 0;
	csv = '';
	braekCSVLoop = false;
	
	$("#dwnBtnContainer").css("visibility","hidden");
	$("#dwnImageContainer").css("visibility","visible");
}

function checkBoxClicked(e) {
	if(e.target.checked) {
		$('#'+e.target.value).css('visibility','visible');
		$('#'+e.target.value).css('position','relative');
	} else {
		$('#'+e.target.value).css('visibility','hidden');
		$('#'+e.target.value).css('position','fixed');
	}

	if($('#mapHolderDiv').css('visibility') != 'hidden') {
		$('#dataDiv').css('height','568px');
		$('#chartHolderDiv1').parent().css('width','50%');
		$('#chartHolderDiv1').removeClass( "hPieChartHolder" ).addClass( "vPieChartHolder" );
		$('#chartHolderDiv2').removeClass( "hPieChartHolder" ).addClass( "vPieChartHolder" );
		$('#chartHolderDiv1 #mapSplitterDiv').remove();
		$('#chartHolderDiv1').css('width', '100%');
		$('#chartHolderDiv2').css('width', '100%');
	} else {
		$('#dataDiv').css('height','284px');
		$('#chartHolderDiv1').parent().css('width','100%');
		$('#chartHolderDiv1').removeClass( "vPieChartHolder" ).addClass( "hPieChartHolder" );
		$('#chartHolderDiv2').removeClass( "vPieChartHolder" ).addClass( "hPieChartHolder" );
		$('#chartHolderDiv1').append("<div id='mapSplitterDiv'></div>");
			
		if($('#chartHolderDiv1').css('visibility') == 'hidden') {
			$('#chartHolderDiv2').css('width', '100%');
		} else {
			$('#chartHolderDiv2').css('width', '50%');
		} 
		if($('#chartHolderDiv2').css('visibility') == 'hidden') {
			$('#chartHolderDiv1').css('width', '100%');
		} else {
			$('#chartHolderDiv1').css('width', '50%');
		}
	}
	
	if($('#mapHolderDiv').css('visibility') != 'hidden') {
		$('#dataDiv').css('height','568px');
	} else if($('#chartHolderDiv1').css('visibility') != 'hidden' || $('#chartHolderDiv2').css('visibility') != 'hidden') {
		$('#dataDiv').css('height','284px');
	} else {
		$('#dataDiv').css('height','0px');
	}
		
	filterDataTable();
}

function openSchoolReport(event, schoolId) {
	event.preventDefault();
	currentSchoolId = schoolId;
	window.open('schoolreport.html', 'schoolreport');
}

function createDonutObject(index, val1, val2, val3, val4, val5, val6) {
	var indexObj = new Object();
	indexObj['index'] = index;
	indexObj[ceUtilisationLegend6] = val6;
	indexObj[ceUtilisationLegend5] = val5;
	indexObj[ceUtilisationLegend4] = val4;
	indexObj[ceUtilisationLegend3] = val3;
	indexObj[ceUtilisationLegend2] = val2;
	indexObj[ceUtilisationLegend1] = val1;
	
	return indexObj;
}

function drawDonutChart(donut_arr) {
	var data = donut_arr;
	var radius = 100,
	    padding = 10;
	
	var color = d3.scale.ordinal()
	    .range(["#009900", "#81db44", "#f9d426", "#ff6c00", "#d00e0d", "#c7c7c7"]);
	
	var arc = d3.svg.arc()
	    .outerRadius(radius)
	    .innerRadius(radius/1.6);
	
	var pie = d3.layout.pie()
	    .sort(null)
	    .value(function(d) { return d.value; });
	    
	color.domain(d3.keys(data[0]).filter(function(key) { return key !== "index"; }));
	
	data.forEach(function(d) {
	    d.ages = color.domain().map(function(name) {
	      return {index: d.index, status: name, value: +d[name]};
	    });
	  });
	  
	var legend = d3.select("#chart6").append("svg")
	      .attr("class", "legend")
	      .attr("width", radius)
	      .attr("height", radius * 2)
	    .selectAll("g")
	      .data(color.domain().slice())
	    .enter().append("g")
	      .attr("transform", function(d, i) { return "translate(0," + i * 20 + ")"; });
	
	  legend.append("circle")
	      .attr("cx", 7)
	      .attr("cy", 7)
	      .attr("r", 7)
	      .style("fill", color);
	
	  legend.append("text")
	      .attr("x", 20)
	      .attr("y", 8)
	      .attr("dy", ".35em")
	      .text(function(d) { return d; });
	
	  var svg = d3.select("#chart6").selectAll(".pie")
	      .data(data)
	    .enter().append("svg")
	      .attr("class", "pie")
	      .attr("width", radius * 2)
	      .attr("height", radius * 2)
	    .append("g")
	      .attr("transform", "translate(" + radius + "," + radius + ")");
	
	  var g = svg.selectAll(".arc")
	      .data(function(d) { return pie(d.ages); })
	    .enter().append("path")
	      .attr("class", "arc")
	      .attr("d", arc)
	      .each(function(d) { this._current = d; })
	      .style("fill", function(d) { return color(d.data.status); })
	      .on('click', function(d) { donutElementClicked(d.data); })
	      .append("svg:title")
	      .attr("class", "titleClass")
      		.text(function(d) { return d.data.status + ": " + d.data.value + " Schools"; });
      				
	  var text = svg.append("text")
	      .attr("dy", ".35em")
	      .style("text-anchor", "middle")
	      .style("font-weight", "bold")
	      .text(function(d) { return d.index; });
}

function donutElementClicked(data) {
	switch(data.index) {
		case 'Overall CE Utilization':
			ceUtilisationElementClicked(data);
			break;
		case 'Active User Index':
			activeUserElementClicked(data);
			break;
		case 'Active Asset Usage':
			activeAssetElementClicked(data);
			break;
		case 'Capacity Utilization':
			capacityElementClicked(data);
			break;
	}
}

function arcTween(a) {
  var i = d3.interpolate(this._current, a);
  this._current = i(0);
  return function(t) {
    return arc(i(t));
  };
}

function loadSchoolXML() {
	if(counter < schoolAbbr_arr.length) {
		var newURL = baseUrl.split('DUMMY').join('schoolUsageReport');
		$.ajax({
			type:"POST",
			url: newURL+'&monthId='+currentMonthId+'&yearId='+currentYearId+'&schoolId='+schoolAbbr_arr[counter].id,
			dataType: "xml",
			//async: false,
			success: getHeatMapData,
			error: nextCall		
		});
	} else {
		var csvObj = {id:currentMonthId, data:csv};
		csv_arr.push(csvObj);
		$("#dwnBtnContainer").css("visibility","visible");
		$("#dwnImageContainer").css("visibility","hidden");
	}
}

function getHeatMapData(xml) {
	$(xml).find('heatMapResource').each(function () {
		csv += '"'+schoolAbbr_arr[counter].id+'",';
		csv += '"'+schoolAbbr_arr[counter].name+'",';
		csv += '"'+$(this).attr('subject')+'",';
		csv += '"'+$(this).attr('level')+'",';
		
		var type = $(this).attr('type')
		switch(type) {
			case 'activity':
				type = 'Activity';
				break;
			case 'asset_media':
				type = 'Media';
				break;
			case 'asset_print':
				type = 'Printable';
				break;
			case 'question':
				type = 'Question';
				break;
			case 'tp':
				type = 'TP';
				break;
		}
		
		csv += '"'+type+'",';
		csv += '"'+$(this).attr('count')+'",';
		csv += '"",';
		csv += '"",';
		csv += '"'+currentMonthId+'-'+currentYearId+'"\r\n';
	});
		
	//var toolCount = gameCount = presentCount = dpresentCount = wboardCount = annotCount = dictCount = 0;
	var counter_arr = new Array();
	var userId_arr = new Array();
	$(xml).find('viewed usage').filter('[type="tools"],[type="game"],[type="presentation"],[type="whiteboard"],[type="dictionary"],[type="default-presentation"],[type="asset_annotation"]').each(function () {
		var objFound = false;
		for(var i=0; i<counter_arr.length; i++) {
			if(counter_arr[i].type == $(this).attr('type')) {
				objFound = true;
				counter_arr[i].count += Number($(this).attr('count'));
				counter_arr[i].userList = getUserList(counter_arr[i].userList, $(this).attr('userId'));
				var userFilter = '[userId="'+$(this).attr('userId')+'"]';
				counter_arr[i].subjectList = getsubjectList(counter_arr[i].subjectList, $(xml).find('userSubjects').filter(userFilter));
			}
		}
		if(!objFound) {
			var subjectList = new Array();
			var userList = new Array();
			userList.push($(this).attr('userId'));
			var userFilter = '[userId="'+$(this).attr('userId')+'"]';
			subjectList = getsubjectList(subjectList, $(xml).find('userSubjects').filter(userFilter));
			var obj = {"type":$(this).attr('type'), "count":Number($(this).attr('count')), "userList":userList, "subjectList":subjectList};
			counter_arr.push(obj);
		}
		
		//$(xml).find('userSubjects');
	});
	
	for(var i=0; i<counter_arr.length; i++) {
		type = capitalizeMe(counter_arr[i].type.replace("asset_", "").replace("-", " "));
		csv += '"'+schoolAbbr_arr[counter].id+'",';
		csv += '"'+schoolAbbr_arr[counter].name+'",';
		csv += ',';
		csv += ',';
		csv += '"'+type+'",';
		csv += '"'+counter_arr[i].count+'",';
		csv += '"'+counter_arr[i].userList.length+'",';
		csv += '"'+counter_arr[i].subjectList.join(',')+'",';
		csv += '"'+currentMonthId+'-'+currentYearId+'"\r\n';
	}
	
	counter_arr = new Array();
	$(xml).find('added usage').each(function () {
		var objFound = false;
		for(var i=0; i<counter_arr.length; i++) {
			if(counter_arr[i].type == $(this).attr('type')) {
				objFound = true;
				counter_arr[i].count += Number($(this).attr('count'));
				counter_arr[i].userList = getUserList(counter_arr[i].userList, $(this).attr('userId'));
				var userFilter = '[userId="'+$(this).attr('userId')+'"]';
				counter_arr[i].subjectList = getsubjectList(counter_arr[i].subjectList, $(xml).find('userSubjects').filter(userFilter));
			}
		}
		if(!objFound) {
			var subjectList = new Array();
			var userList = new Array();
			userList.push($(this).attr('userId'));
			var userFilter = '[userId="'+$(this).attr('userId')+'"]';
			subjectList = getsubjectList(subjectList, $(xml).find('userSubjects').filter(userFilter));
			var obj = {"type":$(this).attr('type'), "count":Number($(this).attr('count')), "userList":userList, "subjectList":subjectList};
			counter_arr.push(obj);
		}
	});
	
	for(var i=0; i<counter_arr.length; i++) {
		type = 'Added '+capitalizeMe(counter_arr[i].type.replace("asset_", "").replace("-", " "));
		csv += '"'+schoolAbbr_arr[counter].id+'",';
		csv += '"'+schoolAbbr_arr[counter].name+'",';
		csv += ',';
		csv += ',';
		csv += '"'+type+'",';
		csv += '"'+counter_arr[i].count+'",';
		csv += '"'+counter_arr[i].userList.length+'",';
		csv += '"'+counter_arr[i].subjectList.join(',')+'",';
		csv += '"'+currentMonthId+'-'+currentYearId+'"\r\n';
	}
	/*var percentage = Math.round((counter/schoolAbbr_arr.length)*100);
	$('#csvPercentage').text(percentage);
	counter++;
	loadSchoolXML();*/
	nextCall();
}

function capitalizeMe(val){
	var temp_arr = val.split(" ");
	for(var i=0; i<temp_arr.length; i++) {
		temp_arr[i] = temp_arr[i].charAt(0).toUpperCase()+temp_arr[i].substr(1).toLowerCase()
	}
    return temp_arr.join(" ");
}

function nextCall(){
	 if(!braekCSVLoop) {
		var percentage = Math.round((counter/schoolAbbr_arr.length)*100);
		$('#csvPercentage').text(percentage);
		counter++;
		loadSchoolXML();
	} else {
		counter = 0;
		csv = '';
		braekCSVLoop = false;
	}
}

function getUserList(id_arr, id) {
	var idFound = false;
	for(var i=0; i<id_arr.length; i++) {
		if(id_arr[i] == id) {
			idFound = true;
			break;
		}
	}
	
	if(!idFound) {
		id_arr.push(id);
	}
	return id_arr;
}

function getsubjectList(sub_arr, xml) {
	if($(xml).find('subjects').text() != '') {
		var temp_arr = $(xml).find('subjects').text().split(',');
		
		for(var i=0; i<temp_arr.length; i++) {
			if(temp_arr[i] != '') {
				var subFound = false;
				for(var j=0; j<sub_arr.length; j++) {
					if(temp_arr[i] == sub_arr[j]) {
						subFound = true;
						break;
					}
				}
				
				if(!subFound) {
					sub_arr.push(temp_arr[i])
				}
			}
		}
	}
	
	return sub_arr;
}

function monthDiff(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}