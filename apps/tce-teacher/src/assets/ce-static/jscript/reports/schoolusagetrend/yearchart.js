var currMonth = monthId;
var currYear = yearId;
var monthCnt = monthRange;
var isDataAvailable = false;
var isFirst = false;
var languageList = {"en":"English","hi":"Hindi","ma":"Marathi","gu":"Gujarati","or":"Oriya"};
var monthDisplayArr = {"1":"Jan","2":"Feb","3":"Mar","4":"Apr",
				"5":"May","6":"Jun","7":"Jul","8":"Aug",
				"9":"Sep","10":"Oct","11":"Nov","12":"Dec"};
var monthNameArr = {"Jan":"1","Feb":"2","Mar":"3","Apr":"4",
				"May":"5","Jun":"6","Jul":"7","Aug":"8",
				"Sep":"9","Oct":"10","Nov":"11","Dec":"12"};
// for feature utilization data
var presentationCnt=0;
var whiteboardCnt=0;
var annotationCnt=0;
var dictionaryCnt=0;
//for contribution data
var lpCnt_added=0;
var resourceCnt_added=0;
var questionCnt_added=0;
var gameCnt_added=0;
var noteCnt_added=0;	
var addedTotalCnt=0;	
var maxYValue=0;
var counterVal=0;
var chartAxisData = [];
var totalsArray = [];
var peakValue;
var isSummaryAvailable = {};


//for displaying help
function displayHelp(_id,flag){
	if(flag){
		var txt = _id.parent().parent().find("div:eq(0)").text().replace(/ /g,"").toLowerCase();
		//console.log("txt:",txt,_id.parent().parent())
		var helpTxt = helpText[txt];
		var pos = _id.offset();
		//console.log(_id, pos);
		$("#helpWindow span").html(helpTxt);
		//$("#helpWindow").css("visibility","visible");
		$("#helpWindow").fadeIn();
		$("#helpWindow").css(
			{left:(pos.left)-$("#helpWindow").width()+25,
			top:pos.top+25}
			);
	} else {
		//$("#helpWindow").css("visibility","hidden");
		$("#helpWindow").fadeOut();	
	}
	//_id.attr('data-id');
};


function scrollToAnchor(aid){
    var aTag = $("div[id='"+ aid +"']");
    var offset = $("#btnHolder").height()+panelOffset+45;
   // console.log("aTag.offset().top",$("#btnHolder").height());
    $('html,body').animate({scrollTop: (aTag.offset().top)-offset},'slow');
}
		
function updateGraphs(e){
	//$(window).scrollTop(0);
	scrollToAnchor("header");
	$("#monthTxt").css("display","block");	
	$("#monthTxt").text($("#monthOpt>option:selected").text());
	$("#monthTxt").animate({opacity:0.5},1000);
	$("#monthTxt").animate({opacity:0},1000,function(){$("#monthTxt").css("display","none")});
	var val = e.target.value.split("-");	
	monthId = val[0];
	yearId = val[1];
	
	currMonth = monthId;
	currYear = yearId;
	var mthYr = currMonth+""+currYear;
	
	if(userUtilizationArray[mthYr] == undefined) {
		currMonthJson = yearwiseData[mthYr];
		
		getTerminalStatistics();
			
		getUserData();	
			
		getheatmapData();
	}
	
	renderUpdatedData();
	//scrollToAnchor("mthResourceChartContainer");
}

function getCompleteYearData(){
	
	//console.log("YEAR log:",currMonth, currYear,":",$('#mthResourceHeading'))
//	$('#mthResourceHeading').text(monthDisplayArr[monthId]+"-"+yearId+" Resource Hits");
	if(counterVal == 0) {
		if($('.preloadPanel > div').length == 1) {
			$('.preloadPanel').css("width","350px");
			$('.preloadPanel').append('<div id="loaderText" style="color:white;">Analysing usage pattern for the month '+monthDisplayArr[monthId]+' - '+yearId+'</div>');
		} else {
			$('#loaderText').text('Analysing usage pattern for the month '+monthDisplayArr[monthId]+' - '+yearId);
		}
	}
	$('#mthYr').text(monthDisplayArr[monthId]+" - "+yearId);
	if(monthCnt > 0){		
		loadMonthXML(currMonth,currYear);		
		// monthCnt--;		
	} else {
		//console.log("getCompleteYearData() - ELSE")
				
		//getMonthUsageData(monthId+""+yearId);
		if(isDataAvailable){
			//add the values in reverse order
			var cnt = topicTrendArr.total.length;
			for(var i=0; i<cnt; i++){
				topicTrendArr.inClassroom[i].unshift(counterArr[i]);
				topicTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				topicTrendArr.total[i].unshift(counterArr[i]);
				//
				mediaTrendArr.inClassroom[i].unshift(counterArr[i]);
				mediaTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				mediaTrendArr.total[i].unshift(counterArr[i]);
				//
				printableTrendArr.inClassroom[i].unshift(counterArr[i]);
				printableTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				printableTrendArr.total[i].unshift(counterArr[i]);
				//
				activityTrendArr.inClassroom[i].unshift(counterArr[i]);
				activityTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				activityTrendArr.total[i].unshift(counterArr[i]);
				//
				gameTrendArr.inClassroom[i].unshift(counterArr[i]);
				gameTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				gameTrendArr.total[i].unshift(counterArr[i]);
				//
				questionTrendArr.inClassroom[i].unshift(counterArr[i]);
				questionTrendArr.inEdgeroom[i].unshift(counterArr[i]);
				questionTrendArr.total[i].unshift(counterArr[i]);
			};
			//console.log("topicTrendArr:", topicTrendArr);
			//all loaded
			overAllUsageTrendArr.topic = topicTrendArr;
			overAllUsageTrendArr.media = mediaTrendArr;
			overAllUsageTrendArr.printable = printableTrendArr;
			overAllUsageTrendArr.activity = activityTrendArr;
			overAllUsageTrendArr.game = gameTrendArr;
			overAllUsageTrendArr.question = questionTrendArr;
			
			peakValue = _.max(totalsArray)+50;
			//console.log("options:",$("#monthOpt")[0].length,$("#monthOpt"));
			$("#slider-range").slider("option","min",0);
			$("#slider-range").slider("option","max",$("#monthOpt")[0].length-1);
			var $slider =  $('#slider-range');
		    var max =  $slider.slider("option", "max");    
		    var spacing =  $slider.width() / (max);		
		    $slider.find('.ui-slider-tick-mark').remove();
		    for (var i = 0; i <= max ; i++) {
		        $('<span class="ui-slider-tick-mark"></span>').css('left', (spacing * i)).appendTo($slider);
		        $('<span class="ui-slider-tick-text">'+shortenMthYr(monthYearOptionsArr[i])+'</span>').css('left', (spacing * i)-15).appendTo($slider); 
		     }			
			//$("#slider-range").slider.max = $("#monthOpt")[0].length;
			
			//summary data
			calculateSummary();
			drawCompleteYearChart();
			if(tSchoolId){
				var temp_arr = $("#monthOpt")[0].value.split('-');
				monthId = Number(temp_arr[0]);
				yearId = Number(temp_arr[1]);
				$('#monthOpt option[value="'+monthId+'-'+yearId+'"]').prop('selected', true);
			}
			renderData();
		} else {
			$(".preloadPanel").css("visibility","hidden");
			alert("No data available!")
		}
	}	
}

function shortenMthYr(val){
	var strArr = val.split("-");
	var str = strArr[0]+"-"+strArr[1].substr(-2,strArr[1].length); 
	return str;
}
//load xml
function loadMonthXML(month,year){	
	//console.log("loadMonthXML",month, year);
	//alert("LOAD - "+"xml/"+year+"_"+month+".xml");
	var operationName = "schoolUsageReport";	
	var tUrl = "";
	if(baseUrl){
		if(cugServer) {
			tUrl = baseUrl+'delegate/proxyservice?urlType=schoolusage&ce_operation='+operationName+'&ce_monthId='+month+'&ce_yearId='+year;
		} else {
			tempUrl = baseUrl.replace("report-school-usage","report-school-usage/"+month+"_"+year+".xml");
			tempUrl = tempUrl.replace("DUMMY",operationName);
			tUrl = tempUrl+"&monthId="+month+"&yearId="+year+"";	
		}
	} else {
		tUrl = "xml/"+year+"_"+month+".xml";
	}
	
	//console.log("tempurl",tUrl);
	if(tSchoolId){
		if(cugServer) {
			tUrl = tUrl+"&ce_schoolId="+tSchoolId;
		} else {
			tUrl = tUrl+"&schoolId="+tSchoolId;
		}
	}
	
	$.ajax({
		type:"GET",
		url:tUrl,
		dataType:"xml",
		success: onCurrXMLLoad, 
		error: onCurrXMLIOError			
	});
	//$('#container').dataTable();
}

function onCurrXMLLoad(xml){
	isDataAvailable = true;		
	//console.log("currMonthData------XML-----",xml);	
	currMonthJson = $.xml2json(xml);
	getSchoolDetails();
	yearwiseData[currMonth+""+currYear] = currMonthJson;
	//Added by manmeet on 5-July
	loadTerminalStatisticsData();
	//console.log("SERVER STATS DONE....")	
	
}

function loadTerminalStatisticsData(){
	//var operationName = "serverStatsReport";
	var operationName = "terminalStatsReport";
	if(baseUrl){
		if(cugServer) {
			tUrl = baseUrl+'delegate/proxyservice?urlType=schoolusage&ce_operation='+operationName+'&ce_monthId='+currMonth+'&ce_yearId='+currYear;
		} else {
			tempUrl = baseUrl.replace("report-school-usage","report-school-usage/terminal_"+currMonth+"_"+currYear+".xml");
			tempUrl = tempUrl.replace("DUMMY",operationName);
			tUrl = tempUrl+"&monthId="+currMonth+"&yearId="+currYear;	
		}
	} else {
		tUrl = "xml/term_"+currYear+"_"+currMonth+".xml";
	}
	//tempUrl = baseUrl.replace("DUMMY",operationName);
	//console.log("terminal stats:",tUrl);
	if(tSchoolId) {
		if(cugServer) {
			tUrl = tUrl+"&ce_schoolId="+tSchoolId;	
		} else {
			tUrl = tUrl+"&schoolId="+tSchoolId;	
		}
	}
	
	$.ajax({
		type:"POST",		
		url:tUrl,
		dataType:"xml",
		success: onTerminalStatsXMLLoad, 
		error: onTerminalStatsXMLIOError			
	});
	
}
function onTerminalStatsXMLLoad(xml){
	//console.log("terminal stats xml:",xml);
	//to check if there was any terminal usage else ignore
	var tempJson = $.xml2json(xml);
	/*if(typeof(tempJson.terminal_statistics_list.terminal_statistics) == 'object') {
		var temp_arr = new Array();
		temp_arr.push(tempJson.terminal_statistics_list.terminal_statistics);
		tempJson.terminal_statistics_list.terminal_statistics = temp_arr;
	}*/
	console.log("MAX",tempJson.terminal_statistics_list.terminal_statistics);
	var isAvailable = false;
	if(tempJson.terminal_statistics_list.terminal_statistics != undefined){
		for(var i=0; i<tempJson.terminal_statistics_list.terminal_statistics.length; i++){
			console.log("---:",tempJson.terminal_statistics_list.terminal_statistics[i].usageMinutes);
			if(tempJson.terminal_statistics_list.terminal_statistics[i].usageMinutes > 0 && tempJson.terminal_statistics_list.terminal_statistics[i].type != "server"){
				isAvailable = true;
				break;
			}
		}			
	}
	if(typeof(tempJson.terminal_statistics_list.terminal_statistics) == 'object') {
		isAvailable = true;
	}
	if(isAvailable){	
		terminalStatsData[currMonth+""+currYear] = $.xml2json(xml);
	}	
	console.log("isSummaryAvailable:",isAvailable)
	isSummaryAvailable[currMonth+""+currYear]=isAvailable;	
	loadServerStatisticsData();
	//parseCurrMonthData();
	//updateCounter();
}
function onTerminalStatsXMLIOError(jqXHR,txt,err){
	//console.log("err loading terminal stats:",txt);
	console.log("isSummaryAvailable",false)
	isSummaryAvailable[currMonth+""+currYear]=false;
	loadServerStatisticsData();
	//parseCurrMonthData();
	//updateCounter();
}
function loadServerStatisticsData(){
	var operationName = "serverStatsReport";
	if(baseUrl){
		if(cugServer) {
			tUrl = baseUrl+'delegate/proxyservice?urlType=schoolusage&ce_operation='+operationName+'&ce_monthId='+currMonth+'&ce_yearId='+currYear;
		} else {
			tempUrl = baseUrl.replace("report-school-usage","report-school-usage/server_"+currMonth+"_"+currYear+".xml");
			tempUrl = tempUrl.replace("DUMMY",operationName);
			tUrl = tempUrl+"&monthId="+currMonth+"&yearId="+currYear;	
		}
	} else {
		tUrl = "xml/serv_"+currYear+"_"+currMonth+".xml";
	}
	//tempUrl = baseUrl.replace("DUMMY",operationName);
	//console.log("Server stats url:",tUrl);
	if(tSchoolId){
		if(cugServer) {
			tUrl = tUrl+"&ce_schoolId="+tSchoolId;
		} else {
			tUrl = tUrl+"&schoolId="+tSchoolId;
		}		
	} 
	
	$.ajax({
		type:"POST",		
		url:tUrl,
		dataType:"xml",
		success: onServerStatsXMLLoad, 
		error: onServerStatsXMLIOError			
	});
}
function onServerStatsXMLLoad(xml){
	//console.log("server stats xml:",xml);
	serverStatsData[currMonth+""+currYear] = $.xml2json(xml);	
	parseCurrMonthData();
	//updateCounter();
}
function onServerStatsXMLIOError(jqXHR,txt,err){
	//console.log("err loading server stats:",txt);		
	parseCurrMonthData();
	//updateCounter();
} 

function updateCounter(){
	counterVal++;
	monthCnt--;
	if(currMonth > 1){
		currMonth--;
	} else {
		currMonth = 12;
		currYear--;
	}
	getCompleteYearData();
}

var schoolData;
function getSchoolDetails(){
	if(!isFirst){
		isFirst = true;	
		schoolData = jsonPath(currMonthJson, '$.schoolMetadata.school');
		var contentData = jsonPath(currMonthJson, '$.schoolMetadata.content');
		//console.log("ACADEMIC MANAGER",schoolData[0]);
		$("#academicManager").text(schoolData[0].academicMngr);
		
		$("#location").text(schoolData[0].state+" ("+schoolData[0].city+")");
		//$("#city").text(schoolData[0].city);
		$("#schoolTitle").text(schoolData[0].name);
		//$("#state").text(schoolData[0].state);
		//
		$("#availableTopicsCnt").text(contentData[0].totalTps);
		perc = (contentData[0].mappedTps/contentData[0].bookTps)*100;
		if(perc){
			$("#contentCoverage").text(parseInt(perc)+"%");
		} else {
			$("#contentCoverage").text("-");
		}	
		perc = (contentData[0].dwnTps/contentData[0].mappedTps)*100;
		if(perc){
			$("#contentAvailability").text(parseInt(perc)+"%");
		} else {
			$("#contentAvailability").text("-");
		}
		//
		var lang = "English";		
		if(schoolData[0].languages){
			lang = getLanguages(schoolData[0].languages);
		}
		//
		$("#version").text(schoolData[0].ceVersion)
		//$("#languages").text(schoolData[0].languages);
		$("#languages").text(lang);
		$("#noOfTeacherTerminalCnt").text(schoolData[0].terminalsER);
		$("#noOfClassroomTerminalCnt").text(schoolData[0].terminalsCR);
		// hide table divs
		$("#utilizationTable").hide();
		$("#classCapacityUtilTable").hide();
		$("#edgeCapacityUtilTable").hide();
		$("#featureTable").hide();
		$("#contributionTable").hide();
		$("#usageHeatTable").hide();
		//hide it initially
		/*$("#reportSummary").hide();
		$("#summaryTable tbody").css("visibility","hidden");*/
	}
}
function getLanguages(val){
	var langStr = "";
	var langArr = val.split(",");
	if(langArr.length){
		for(var i=0; i<langArr.length; i++){
			if(langArr[i] != ""){
				langStr = langStr + languageList[langArr[i]] + ",";
			}
		}
	}
	return langStr.substr(0,langStr.length-1);
}


function onCurrXMLIOError(jqXHR,txt,err){
	//console.log("Err-",txt,err);
	//checking this if the first month xml is not available - update the monthId and yearId
	//console.log("ERROR! - ",monthId,yearId);	
	if(counterVal == 0){
		if(monthId > 1){
			monthId--;
		} else {
			monthId = 12;
			yearId--;
		}
		//for array counter (coz updateCounter will increment the counterVal by 1)
		counterVal--;
		updateCounter();
	} else {	
		parseCurrMonthData(true);
	}
	//alert("# "+err);
	//counterVal++;
	/*monthCnt--;
	if(currMonth > 1){
		currMonth--;
	} else {
		currMonth = 12;
		currYear--;
	}
	getCompleteYearData();*/
	/*
	getMonthUsageData(monthId+""+yearId);

	overAllUsageTrendArr.topic = topicTrendArr;
	overAllUsageTrendArr.media = mediaTrendArr;
	overAllUsageTrendArr.printable = printableTrendArr;
	overAllUsageTrendArr.activity = activityTrendArr;
	overAllUsageTrendArr.game = gameTrendArr;
	overAllUsageTrendArr.question = questionTrendArr;
	console.log("overAllUsageTrendArr --------FINAL---------", overAllUsageTrendArr);
		
	drawCompleteYearChart();
	renderData();
	*/
}

function loadMonthData(p_btn){
	switch(p_btn){
		case "nxtBtn":
		monthId++;
		break;
		case "prevBtn":
		monthId--;
		break;
	}
	renderUpdatedData();
}

function renderData(p_flag){
	//console.log("chartAxisData",chartAxisData);
	if(isSummaryAvailable[monthId+""+yearId]){
		$("#capacityUtilDiv").show();
		$("#reportSummary").show();
		$("#summaryTable tbody").css("visibility","visible");
		$("#btnPanel #g8").show();
		$("#btnPanel #g9").show();
	} else {
		$("#btnPanel #g8").hide();
		$("#btnPanel #g9").hide();
		$("#capacityUtilDiv").hide();
		$("#reportSummary").hide();
		$("#summaryTable tbody").css("visibility","hidden");
	}
	getMonthUsageData(monthId+""+yearId);
	//$("#utilTrend").text("Active user count: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].activeUsrCnt+" / Total user count: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].totalUsrCnt);
	if(userUtilizationArray[monthId+""+yearId]){
		//console.log("totalEligibleUsersArr[monthId+yearId]",totalEligibleUsersArr,totalEligibleUsersArr[monthId+""+yearId]);
		var registeredUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..registered")[0]);
		var activeUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..active")[0]);
		var eligibleUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..eligible")[0]);
		if(totalEligibleUsersArr[monthId+""+yearId] > 0){			
			var perc = (activeUserCount/eligibleUserCount)*100;
			var rounded = Math.round(perc * 100) / 100;
			$("#eligibleUsers").css("display","block");
			$("#percActUsers").css("display","block");
			$("#eligibleUsers").text("Eligible users:"+eligibleUserCount);
			$("#percActUsers").text("Percentage of active users: "+rounded+"%");		
		} else {
			$("#eligibleUsers").css("display","none");
			$("#percActUsers").css("display","none");
		}
		//console.log("eligible users",totalEligibleUsersArr);
		//$("#totalRegUsers").text("Registered users: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].totalUsrCnt);
		//$("#activeUsers").text("Active users: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].activeUsrCnt);
		$("#totalRegUsers").text("Registered users: "+registeredUserCount);
		$("#activeUsers").text("Active users: "+activeUserCount);
	} else {
		alert("No user found! Please contact system administrator.")
	}
	
	//For feature utilization chart/table
	if(featureUtilizationArray[monthId+""+yearId]){
		$("#presCnt").text(featureUtilizationArray[monthId+""+yearId].additionalInfo[0]+" users");
		$("#wbCnt").text(featureUtilizationArray[monthId+""+yearId].additionalInfo[1]+" users");
		$("#annCnt").text(featureUtilizationArray[monthId+""+yearId].additionalInfo[2]+" users");
		$("#dictCnt").text(featureUtilizationArray[monthId+""+yearId].additionalInfo[3]+" users");
	}
	if(contributionIndexArray[monthId+""+yearId]){
		//For contribution chart/table
		$("#topicCnt").text(contributionIndexArray[monthId+""+yearId].additionalInfo[0]+" users");
		$("#resCnt").text(contributionIndexArray[monthId+""+yearId].additionalInfo[1]+" users");
		$("#questCnt").text(contributionIndexArray[monthId+""+yearId].additionalInfo[2]+" users");
		$("#gamesCnt").text(contributionIndexArray[monthId+""+yearId].additionalInfo[3]+" users");
		$("#notesCnt").text(contributionIndexArray[monthId+""+yearId].additionalInfo[4]+" users");
	}
	//
	$(".monthTitle").text("("+monthDisplayArr[monthId]+"-"+yearId+")");
	
	//Terminal statistics chart
	//moved down to get complete year data
	//getTerminalStatistics(monthId+""+yearId);
	drawTerminalStatisticsChart(monthId+""+yearId);
	//	
	drawBubbleChart(monthId+""+yearId);
	drawContributionIndexChart(monthId+""+yearId);
	drawFeatureUtilizationChart(monthId+""+yearId);
	//drawBubbleChartRange("112012","72013");	
	if(userUtilizationArray[monthId+""+yearId]){
		drawUserUtilizationChart(monthId+""+yearId);
	}
	//console.log("userUtilizationArray[monthId+yearId]",userUtilizationArray[monthId+""+yearId]);
	if(!p_flag){
		if(userUtilizationArray[monthId+""+yearId]){
			drawUserTable(monthId+""+yearId);
		}
		if(heatmapTableData[monthId+""+yearId]){
			drawUsageHeatmapTable(monthId+""+yearId);
		}
		if(terminalUsageStatsData[monthId+""+yearId]){
			drawClassRoomCapacityUtilTable(monthId+""+yearId);
			drawEdgeRoomCapacityUtilTable(monthId+""+yearId);
		}
		
		drawContributionIndexTable(monthId+""+yearId);
		drawFeatureUtilizationTable(monthId+""+yearId);
	}
	updateSummaryTable(monthId+""+yearId);
	updateFilters();
	$("#preloader").css("visibility","hidden");
}

function renderUpdatedData(){
//	console.log("RENDER UPDATED:", userUtilizationArray[monthId+""+yearId].utilizationData);	
	//$("#utilTrend").text("Active user count: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].activeUsrCnt+" / Total user count: "+userUtilizationArray[monthId+""+yearId].utilizationData[0].totalUsrCnt);
	var registeredUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..registered")[0]);
	var activeUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..active")[0]);
	var eligibleUserCount = Number(jsonPath(yearwiseData[monthId+""+yearId], "$.ceWideReport.userTypes..eligible")[0]);
	//var perc = userUtilizationArray[monthId+""+yearId].utilizationData[0].activeUsrCnt/userUtilizationArray[monthId+""+yearId].utilizationData[0].totalUsrCnt;
	var perc = activeUserCount/registeredUserCount;
	var rounded = Math.round(perc * 100) / 100;
	$("#percActUsers").text("% of active users: "+rounded+"%");
	$("#totalRegUsers").text("Total registered users: "+registeredUserCount);
	$("#activeUsers").text("Active users: "+activeUserCount);		
	renderData(true);
	$('#userTable').dataTable().fnClearTable();
	$('#userTable').dataTable().fnAddData(userUtilizationArray[monthId+""+yearId].tableData);
	//
	$('#usageHeatmapTable').dataTable().fnClearTable();
	$('#usageHeatmapTable').dataTable().fnAddData(heatmapTableData[monthId+""+yearId]);	
	//
	$('#contributionIndexTable').dataTable().fnClearTable();
	$('#contributionIndexTable').dataTable().fnAddData(contributionIndexArray[monthId+""+yearId].tableData);	
	//
	$('#featureUtilTable').dataTable().fnClearTable();
	$('#featureUtilTable').dataTable().fnAddData(featureUtilizationArray[monthId+""+yearId].tableData);
	//
	$('#classCapacityUtilizationTable').dataTable().fnClearTable();
	$('#classCapacityUtilizationTable').dataTable().fnAddData(terminalUsageStatsData[monthId+""+yearId].inClassRoom.tableData);
	//
	$('#edgeCapacityUtilizationTable').dataTable().fnClearTable();
	$('#edgeCapacityUtilizationTable').dataTable().fnAddData(terminalUsageStatsData[monthId+""+yearId].inEdgeRoom.tableData);
}

var topicTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};
var mediaTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};
var printableTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};;
var activityTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};;
var gameTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};;
var questionTrendArr = {"inClassroom":[],"inEdgeroom":[],"total":[]};;

var presentationArr = {"chartData":[],"tableData":[]};
var whiteboardArr = {"chartData":[],"tableData":[]};
var annotationArr = {"chartData":[],"tableData":[]};
var dictionaryArr = {"chartData":[],"tableData":[]};

var topicAddedArr = {"chartData":[],"tableData":[]};
var resourcesAddedArr = {"chartData":[],"tableData":[]};
var questionsAddedArr = {"chartData":[],"tableData":[]};
var gamesAddedArr = {"chartData":[],"tableData":[]};
var notesAddedArr = {"chartData":[],"tableData":[]};

var counterArr = [];

function parseCurrMonthData(p_flag){	
	//console.log("Parse curr month data");
	var mthYr = currMonth+""+currYear;
	
	presentationCnt=0;
	whiteboardCnt=0;
	annotationCnt=0;
	dictionaryCnt=0;
	//
	lpCnt_added=0;
	resourceCnt_added=0;			
	questionCnt_added=0;
	gameCnt_added=0;
	noteCnt_added=0;		
	addedTotalCnt=0;
	//			
	var currMonthTxt = monthDisplayArr[currMonth];
	var dateValue = currMonthTxt + " " + currYear;
	tempmonthlyUsageArr=[];
	var data = {};
	if(!p_flag){
		// console.log("CURRENT MONTH TXT ", currMonthTxt);
		var currMonthData = jsonPath(currMonthJson, '$.viewed.usage');	
		//console.log("currMonthData-----------",currMonth, currMonthTxt+" "+currYear);
		//console.log("D----ATA:",jsonPath(currMonthData,'$.[?(@.type=="tp" || @.type=="chapter")]'));
		//
		var inEdgeRoom = jsonPath(currMonthData,'$.[?(@.accessMode=="0")]');
		var inClassRoom = jsonPath(currMonthData,'$.[?(@.accessMode=="1")]');
	}	
	//for topic
	data = {};
	if(!p_flag){						  
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="tp" || @.type=="chapter")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="tp" || @.type=="chapter")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	//console.log("data.inEdgeRoom",data.inEdgeRoom);
	//console.log("data.inClassRoom",data.inClassRoom);
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}	
	}
	topicTrendArr.inClassroom.push([data.inClassRoomCount]);
	topicTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	topicTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	//console.log("====================",data.inClassRoomCount,data.inEdgeRoomCount);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);
	
	//for media
	data = {};
	if(!p_flag){	
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="asset_media")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="asset_media")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}	
	}
	mediaTrendArr.inClassroom.push([data.inClassRoomCount]);
	mediaTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	mediaTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);
	
	//overAllUsageTrendArr.media.push([currMonth,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	
	//overAllUsageTrendArr.media.push([counterVal,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	// console.log("currMonthData------MEDIA-----",currMonth," : ", (data.inEdgeRoomCount+data.inClassRoomCount));
	// yearwiseChartData.push({"key":data.key,"values":data.values});
	
	//for Printable
	data = {};
	if(!p_flag){	
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="asset_print")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="asset_print")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}	
	}
	printableTrendArr.inClassroom.push([data.inClassRoomCount]);
	printableTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	printableTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);
	
	//overAllUsageTrendArr.printable.push([currMonth,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	
	//overAllUsageTrendArr.printable.push([counterVal,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	// console.log("currMonthData------PRINTABLE-----",currMonth," : ", (data.inEdgeRoomCount+data.inClassRoomCount));
	// yearwiseChartData.push({"key":data.key,"values":data.values});	
	//for Activity
	data = {};
	if(!p_flag){	
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="activity")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="activity")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}	
	}
	activityTrendArr.inClassroom.push([data.inClassRoomCount]);
	activityTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	activityTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);
	
	//overAllUsageTrendArr.activity.push([currMonth,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	
	//overAllUsageTrendArr.activity.push([counterVal,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	// console.log("currMonthData------ACTIVITY-----",currMonth," : ", (data.inEdgeRoomCount+data.inClassRoomCount));
	// yearwiseChartData.push({"key":data.key,"values":data.values});	
	//for Games
	data = {};
	if(!p_flag){	
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="game")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="game")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}	
	}
	gameTrendArr.inClassroom.push([data.inClassRoomCount]);
	gameTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	gameTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);
	
	//overAllUsageTrendArr.game.push([currMonth,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	
	//overAllUsageTrendArr.game.push([counterVal,(data.inEdgeRoomCount+data.inClassRoomCount)]);
	//console.log("currMonthData------GAME-----",currMonth," : ", (data.inEdgeRoomCount+data.inClassRoomCount));
	// yearwiseChartData.push({"key":data.key,"values":data.values});
	//for Questions
	data = {};
	if(!p_flag){	
		data.inEdgeRoom = jsonPath(inEdgeRoom,'$.[?(@.type=="question")]');
		data.inClassRoom = jsonPath(inClassRoom,'$.[?(@.type=="question")]');
	}
	data.inEdgeRoomCount = 0;
	data.inClassRoomCount = 0;
	if(data.inEdgeRoom){
		for(var i=0; i<data.inEdgeRoom.length;i++){
			data.inEdgeRoomCount += Number(data.inEdgeRoom[i].count);
		}
	}
	if(data.inClassRoom){
		for(var i=0; i<data.inClassRoom.length;i++){
			data.inClassRoomCount += Number(data.inClassRoom[i].count);
		}
	}	
	questionTrendArr.inClassroom.push([data.inClassRoomCount]);
	questionTrendArr.inEdgeroom.push([data.inEdgeRoomCount]);
	questionTrendArr.total.push([(data.inClassRoomCount+data.inEdgeRoomCount)]);
	totalsArray.push(data.inClassRoomCount,data.inEdgeRoomCount);	
	counterArr.unshift(counterVal);
	
	if(!p_flag){
		//for total assets count
		var totalAssetsCount = 0;
		_.each(currMonthData,function(val){			
			if(val.length){
				_.each(val, function(data){
					totalAssetsCount += Number(data.count);
				})
			} else {
				totalAssetsCount += Number(val.count);
			}
		});
		tempmonthlyUsageArr.push(totalAssetsCount);
	}
	monthlyUsageArr[mthYr] = tempmonthlyUsageArr;
	
	if(!p_flag){
		// for feature utilization		
		var currMonthFeaturesArr = jsonPath(currMonthJson, '$.viewed.usage');
		var usersArrList = {};
		usersArrList.presentations = [];
		usersArrList.whiteboard = [];
		usersArrList.annotation = [];
		usersArrList.dictionary = [];
		//console.log("currMonthFeaturesArr",currMonthFeaturesArr);
		if(currMonthFeaturesArr){
			for(var i=0; i<currMonthFeaturesArr[0].length; i++){						
				var count = Number(currMonthFeaturesArr[0][i].count);				
				//
				switch(currMonthFeaturesArr[0][i].type){			
					case 'presentation':
					case 'default-presentation':
					presentationCnt += count;
					usersArrList.presentations.push(currMonthFeaturesArr[0][i].userId);
					break;
					case 'whiteboard':
					whiteboardCnt += count;
					usersArrList.whiteboard.push(currMonthFeaturesArr[0][i].userId);
					break;
					case 'asset_annotation':
					annotationCnt += count;
					usersArrList.annotation.push(currMonthFeaturesArr[0][i].userId);				
					break;
					case 'dictionary':
					dictionaryCnt += count;
					usersArrList.dictionary.push(currMonthFeaturesArr[0][i].userId);
					break;					
				}			
			}		
		}
		var features = {};
		//console.log("====>",_.uniq(usersArrList.presentations,true).length);
		/*var arr = [["Presentation ("+_.uniq(usersArrList.presentations,true).length+" users)",presentationCnt], 
					["Whiteboard ("+_.uniq(usersArrList.whiteboard,true).length+" users)",whiteboardCnt], 
					["Annotation ("+_.uniq(usersArrList.annotation,true).length+" users)",annotationCnt], 
					["Dictionary ("+_.uniq(usersArrList.dictionary,true).length+" users)",dictionaryCnt]];
					*/
		var presCnt = _.uniq(usersArrList.presentations,true).length;
		var wbCnt = _.uniq(usersArrList.whiteboard,true).length;
		var annCnt = _.uniq(usersArrList.annotation,true).length;
		var dictCnt = _.uniq(usersArrList.dictionary,true).length;
		
		var arr = [["Presentation",presentationCnt,presCnt],
					["Whiteboard",whiteboardCnt,wbCnt], 
					["Annotation",annotationCnt,annCnt], 
					["Dictionary",dictionaryCnt,dictCnt]];
		features.tableData = arr;
		features.chartData = arr;
		features.additionalInfo = [presCnt,wbCnt,annCnt,dictCnt];
		featureUtilizationArray[mthYr] = features;
		
		// for Contribution index		
		var currMonthContributionArr = jsonPath(currMonthJson, '$.added.usage');
		var usersArrList = {};
		usersArrList.topic = [];
		usersArrList.resources = [];
		usersArrList.questions = [];
		usersArrList.games = [];
		usersArrList.notes = [];
		if(currMonthContributionArr){
			for(var i=0; i<currMonthContributionArr[0].length; i++){						
				var count = Number(currMonthContributionArr[0][i].count);				
				//
				switch(currMonthContributionArr[0][i].type){			
					case 'tp':					
					lpCnt_added += count;
					usersArrList.topic.push(currMonthContributionArr[0][i].userId);
					break;					
					case 'asset':
					case 'activity':					
					resourceCnt_added += count;
					usersArrList.resources.push(currMonthContributionArr[0][i].userId);
					break;
					case 'question':
					questionCnt_added += count;
					usersArrList.questions.push(currMonthContributionArr[0][i].userId);
					break;					
					case 'game':
					gameCnt_added += count;
					usersArrList.games.push(currMonthContributionArr[0][i].userId);
					break;
					case 'notes':
					noteCnt_added += count;
					usersArrList.notes.push(currMonthContributionArr[0][i].userId);
					break;					
				}			
		}
		}
		var contribution = {};
		var topicCnt = _.uniq(usersArrList.topic,true).length;
		var resCnt = _.uniq(usersArrList.resources,true).length;
		var quesCnt = _.uniq(usersArrList.questions,true).length;
		var gameCnt = _.uniq(usersArrList.games,true).length;
		var notesCnt = _.uniq(usersArrList.notes,true).length;
		/*var arr = [["Topics ("+_.uniq(usersArrList.topic,true).length+" users)",lpCnt_added],
					["Resources ("+_.uniq(usersArrList.resources,true).length+" users)",resourceCnt_added], 
					["Questions ("+_.uniq(usersArrList.questions,true).length+" users)",questionCnt_added], 
					["Games ("+_.uniq(usersArrList.games,true).length+" users)",gameCnt_added], 
					["Notes ("+_.uniq(usersArrList.notes,true).length+" users)",noteCnt_added]];
					*/
		var arr = [["Topics",lpCnt_added,topicCnt],
					["Resources",resourceCnt_added,resCnt], 
					["Questions",questionCnt_added,quesCnt], 
					["Games",gameCnt_added,gameCnt], 
					["Notes",noteCnt_added,notesCnt]];
					
		contribution.chartData = arr; 
		contribution.tableData = arr;
		contribution.additionalInfo = [topicCnt,resCnt,quesCnt,gameCnt,notesCnt];
		/*contribution.additionalInfo = [_.uniq(usersArrList.topic,true).length,
									_.uniq(usersArrList.resources,true).length,
									_.uniq(usersArrList.questions,true).length,
									_.uniq(usersArrList.games,true).length,
									_.uniq(usersArrList.notes,true).length];*/
		contributionIndexArray[mthYr] = contribution;
		
		//get Server statistics data (moved to render data)
		//if(isSummaryAvailable){
		//console.log("YEAR CHART:",mthYr);
		if(counterVal < 1) {
		getTerminalStatistics();
		//}
		//Get user level data - for the month
		
		getUserData();	
		
		//Get heat map data - for each month
		getheatmapData();
		}
		$('#loaderText').text('Comparing with the previous months data...');
		monthYearArr.push(currMonth+""+currYear);
		monthYearOptionsArr.unshift(monthDisplayArr[currMonth]+"-"+currYear);
		var opt = $('<option value="'+currMonth+"-"+currYear+'">'+monthDisplayArr[currMonth]+"-"+currYear+'</option>');
		$("#monthOpt").append(opt);
	}
	chartAxisData.unshift(monthDisplayArr[currMonth]+"-"+currYear);
	//console.log("FINAL",yearwiseData);
	//console.log("YEARWISE DATA", overAllUsageTrendArr);
	//console.log("contributionIndexArray", contributionIndexArray);
	//console.log("Feature utilization Array ", featureUtilizationArray);
	//console.log("==========tempmonthlyUsageArr==========",mthYr,totalsArray,tempmonthlyUsageArr);
	
	updateCounter();
	
	/*
	//Added by manmeet on 5-July
	loadServerStatisticsData();
	*/
	//MOVE THIS PART TO SERVER USAGE FUNCTION
	/*
	counterVal++;
	monthCnt--;
	if(currMonth > 1){
		currMonth--;
	} else {
		currMonth = 12;
		currYear--;
	}
	getCompleteYearData();
	*/
}



function drawCompleteYearChart(){
	//console.log("chartAxisData",chartAxisData);
	//chartAxisData.reverse();
	$('#chart svg').remove();
	$('#chart').append('<svg></svg>');
	//$('#chart svg').empty();
	//console.log("draw complete year graph - yearwiseData",yearwiseData);	
	nv.addGraph(function() {
		var chart = nv.models.stackedAreaChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                //.showLegend(false)
	                .clipEdge(true);
	      chart.margin({left: 80});          
		/*chart.legend.disptach.on('legendClick', function(e) { 
			return; //do nothing
		});*/
		  chart.xAxis
		  		.tickFormat(function(d){
		  			//console.log("D:",d,chartAxisData[d]);
		  			var mth = new Date(d).getMonth()+1;
		  			var year = String(new Date(d).getFullYear()).substring(2);
		  			//console.log("DATE ######", monthDisplayArr[mth],":",year);
		  			var currMonthTxt = monthDisplayArr[mth]+"-"+year;
		  			//console.log("***************",d);
		  			return chartAxisData[d];
		  		})		  		
		  		//.tickFormat(d3.format(',r'))
		  		.showMaxMin(false);		      	
			
		
		  chart.yAxis
		      .axisLabel('Assets Used')		      		     
		      .tickFormat(d3.format('.0f'));
		
		
		  
		  d3.select('#chart svg')
		      .datum(getYearData())
		    .transition().duration(500)
		      .call(chart);
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getYearData(){
	//console.log("media data:", overAllUsageTrendArr)
	
	return [
		{
			"key":"Topic",
			"values": overAllUsageTrendArr.topic.total
		}, 
		{ 
			"key" : "Media" , 		
			"values" : overAllUsageTrendArr.media.total		
		},
		{ 
			"key" : "Printable" , 		
			"values" : overAllUsageTrendArr.printable.total		
		},
		{ 
			"key" : "Activity" , 		
			"values" : overAllUsageTrendArr.activity.total
		},
		{ 
			"key" : "Game" , 		
			"values" : overAllUsageTrendArr.game.total		
		},		
		{ 
			"key" : "Question" , 		
			"values" : overAllUsageTrendArr.question.total		
		}
	]; 
}

function drawYearChartIndividual(e){
	$('#chart svg').remove();
	$('#chart').append('<svg></svg>');
	//$('#chart svg').empty();
	//console.log("evt.target",e.target.id);
	var id = e.target.id;
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .clipEdge(true)
	                .forceY([0,peakValue]) ;
				chart.margin({left: 80});
		  	chart.xAxis
		  		.tickFormat(function(d){
		  			var mth = new Date(d).getMonth()+1;
		  			var year = String(new Date(d).getFullYear()).substring(2);
		  			//console.log("DATE ######", monthDisplayArr[mth],":",year);
		  			var currMonthTxt = monthDisplayArr[mth]+"-"+year;
		  			//console.log("***************",d);
		  			return chartAxisData[d];
		  		})		  		
		  		//.tickFormat(d3.format(',r'))
		  		.showMaxMin(false);		      	
		
		  chart.yAxis
		      .axisLabel('Assets Used')
		      .tickFormat(d3.format('.0f'));
		
		  d3.select('#chart svg')
		      .datum(getYearDataIndividual(id))
		    	.transition().duration(500)
		      .call(chart);
		// d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getYearDataIndividual(p_id){	
	//console.log("-----overAllUsageTrendArr[p_id].inClassroom-----",overAllUsageTrendArr[p_id].inClassroom);
	return [ 
		{ 
			"key" : "Classroom Usage" , 		
			"values" : overAllUsageTrendArr[p_id].inClassroom	
		},
		{ 
			"key" : "Edgeroom Usage" , 		
			"values" : overAllUsageTrendArr[p_id].inEdgeroom		
		}
	]; 
}

//draw contribution index chart
function drawContributionIndexChart(p_mthYr){
	$('#contributionChart svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.discreteBarChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .staggerLabels(true)
	                .tooltips(false)	                
	                .valueFormat(d3.format("f"))
	                .showValues(true);
	      chart.margin({left: 80});
			chart.yAxis
		      .axisLabel('Usage')
		      .tickFormat(d3.format('d'));
		     chart.xAxis
		     	.rotateLabels(-45);
		  d3.select('#contributionChart svg')
		      .datum(getContributionIndexData(p_mthYr))
		    .transition().duration(500)
		      .call(chart);
		// d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getContributionIndexData(p_mthYr){
	//console.log("DRAW CONTRIBUTION INDEX CHART",contributionIndexArray[p_mthYr].chartData);
	return [ 
		{ 
			"key" : "Contribution" , 		
			"values" : contributionIndexArray[p_mthYr].chartData		
		}
	]; 
}

//Draw feature utilization chart 
function drawFeatureUtilizationChart(p_mthYr){
	$('#featureUtilzationChart svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.discreteBarChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .staggerLabels(true)
	                .tooltips(false)
	                .valueFormat(d3.format("f"))
	                .showValues(true);
	     chart.margin({left: 80});
	         chart.xAxis
		     	.rotateLabels(-45);
		chart.yAxis
		      .axisLabel('Usage')
		      .tickFormat(d3.format('d'));
		  d3.select('#featureUtilzationChart svg')
		      .datum(getFeatureUtilData(p_mthYr))
		    .transition().duration(500)
		      .call(chart);
		// d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getFeatureUtilData(p_mthYr){
	//console.log("DRAW FEATURE UTIL CHART",featureUtilizationArray[p_mthYr].chartData);
	return [ 
		{ 
			"key" : "Feature utilization" , 		
			"values" : featureUtilizationArray[p_mthYr].chartData		
		}
	]; 
}

//New user utilization chart (bar graph)
function drawUserUtilizationChart_bar(p_mthYr){
	console.log("drawUserUtilizationChart_bar", p_mthYr);
	$('#userUtilChartDiv svg').remove();
	$('#userUtilChartDiv').append('<svg></svg>');
	if(userUtilizationArray[p_mthYr].chartData.length < 7){		
		$("#userUtilChartDiv svg").css("height","480px");
	} else {
		var tempHeight = (userUtilizationArray[p_mthYr].chartData.length * 80);
		$("#userUtilChartDiv svg").css("height", tempHeight+"px");
	}
	nv.addGraph(function() {
		var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .margin({top: 30, right: 20, bottom: 50, left: 120})
          .showValues(true)
          .tooltips(true)
          .showControls(false);				
				
		chart.yAxis
		      .tickFormat(d3.format('d'));
		
		  d3.select('#userUtilChartDiv svg')
		      .datum(getUserUtilizationData(p_mthYr))
		      .transition().duration(500)
		      .call(chart);
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
	
}

function getUserUtilizationData(p_mthYr){
	//console.log("DRAW USER UTILIZATION CHART",userUtilizationArray[p_mthYr]);
	var data = [];
	var loginValues = [];
	var hitValues = [];
	var timeValues = [];
	var tempUserUtilArr = _.sortBy(userUtilizationArray[p_mthYr].chartData,
		function(num){
			return num[0].loginCnt
			});
	tempUserUtilArr.reverse();
	//console.log("tempUserUtilArr",_.sortBy(userUtilizationArray[p_mthYr].chartData,function(num){return num[0].loginCnt}));
	//check if the server session data is available
	//var sessionData = jsonPath(terminalStatsData[p_mthYr],'$.session_time_per_users');
	var sessionData = jsonPath(terminalStatsData[p_mthYr],'$.user_session_statistics_list');
	//console.log("TIME:",terminalStatsData[p_mthYr],sessionData);
	/*console.log("Sorted:",_.sortBy(userUtilizationArray[p_mthYr].chartData,function(num){
		console.log("Num:",num[0]);
		return num[0].loginCnt}));*/
	for (var i=0; i<tempUserUtilArr.length; i++) {
	//		console.log("U S E R :",tempUserUtilArr[p_mthYr].chartData[i][0].user);
    	loginValues.push({label:tempUserUtilArr[i][0].user,
    		value:tempUserUtilArr[i][0].loginCnt});
    	hitValues.push({label:tempUserUtilArr[i][0].user,
    		value:tempUserUtilArr[i][0].hitCnt});
    	if(sessionData){
    		timeValues.push({label:tempUserUtilArr[i][0].user,
    		value:tempUserUtilArr[i][0].timeUsed});
    	}
	}
	//console.log("VALUES:",loginValues);
    data.push({
      key: "Logins",
      values: loginValues
    });
    data.push({
      key: "Hits",
      values: hitValues
    });
    if(sessionData){
	    data.push({
	      key: "Time",
	      values: timeValues
	    });     
 	}
   return data;
	 
}

//Draw User utilization chart
function drawUserUtilizationChart_old(p_mthYr){
	$('#userUtilzationChart svg').remove();
	$('#userUtilzationChart').append('<svg></svg>');
	nv.addGraph(function() {
		var chart = nv.models.scatterChart()
					.showDistX(true)
                	.showDistY(true)
                	.showLegend(false)                	
                	//.tooltip
               		 .color(d3.scale.category10().range());
			
		chart.tooltipContent(function(key, x, y, e, graph) {
			return '<h3>' + key + '</h3>' +
               '<p>' + parseInt(x) + ' Logins / ' + parseInt(y) + ' Hits</p>'; 
		});
		
		chart.xAxis		
		      .axisLabel('Login')
			.tickFormat(d3.format('d'));
		 
		chart.yAxis
			 .axisLabel('Hits')
			.tickFormat(d3.format('d'));
		
		  d3.select('#userUtilzationChart svg')
		      .datum(getUserUtilizationData(p_mthYr))
		    .transition().duration(500)
		      .call(chart);
		      
		// d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
	
}

function getUserUtilizationData_old(p_mthYr){
	//console.log("DRAW USER UTILIZATION CHART",userUtilizationArray[p_mthYr]);
	var data = [];
	for (var i=0; i<userUtilizationArray[p_mthYr].chartData.length; i++) {		
     data.push({
       key: userUtilizationArray[p_mthYr].chartData[i][0],
       values: [userUtilizationArray[p_mthYr].chartData[i][1]]
     });     
    
   }
 
   return data;
	 
}
