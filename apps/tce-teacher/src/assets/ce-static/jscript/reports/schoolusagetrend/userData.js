//final data is in model.js

// for user utilization trend
var tpCnt=0;
var mediaCnt = 0;
var printCnt = 0;
var activityCnt = 0;
var questionCnt = 0;
var gameCnt = 0;		
var totalCnt = 0;
var totalUserCnt = 0;
var activeUserCnt = 0;
var activeUserAssetCnt = 0; 
var timeCnt=0;

function getUserData(){
	totalUserCnt = 0;
	activeUserCnt = 0;
	activeUserAssetCnt = 0;
	//console.log("getUserData #####",currMonthJson);
	//console.log("jsonPath(currMonthJson, '$.users.user')",jsonPath(currMonthJson, '$.users.user'));
	var mthYr = currMonth+""+currYear;
	var tempUsers = jsonPath(currMonthJson, '$.users.user');
	if(tempUsers){	
		if(tempUsers[0].length > 1){
			var users = jsonPath(currMonthJson, '$.users.user')[0];
		} else {
			var users = jsonPath(currMonthJson, '$.users.user');
		}
	}
	
	if(users){
		//console.log("USERS:", jsonPath(currMonthJson, "$.users.user"));
		var userCnt = users.length;
		var isUserActive = false;
		totalUserCnt = userCnt;
		activeUserCnt=0;
		activeUserAssetCnt = 0;
		userUtilizationArray[mthYr] = {};
		userUtilizationArray[mthYr].tableData = [];
		userUtilizationArray[mthYr].chartData = [];
		userUtilizationArray[mthYr].utilizationData = [];
		//console.log("user count:", userCnt,":",users);
		for(var i=0; i<userCnt; i++){		
			tpCnt=0;
			mediaCnt = 0;
			printCnt = 0;
			activityCnt = 0;
			questionCnt = 0;
			gameCnt = 0;		
			totalCnt = 0;
			timeCnt = 0;
			isUserActive = false;
			//console.log("users[i].userId",users[i].userId);
			var userUsageStats = jsonPath(terminalStatsData[mthYr],'$.user_session_statistics_list..[?(@.userId=="'+users[i].userId+'")]');
			if(userUsageStats){
				for(var j=0; j<userUsageStats.length; j++){
					timeCnt+= Number(userUsageStats[j].usageMinutes)/60;
				}
				//console.log("USER USAGE STATS AVAILABLE",timeCnt);
			}
			/*console.log("server stats data-",terminalStatsData);
			console.log("userUsageStats:",userUsageStats);*/
			//		
			var userData = jsonPath(currMonthJson, '$.viewed..[?(@.userId=="'+users[i].userId+'")]');
			var userMetadata = jsonPath(currMonthJson, '$.usersMetadata..[?(@.userId=="'+users[i].userId+'")]');
			var subjectsList = jsonPath(currMonthJson, '$.userSubjectsList..[?(@.userId=="'+users[i].userId+'")].subjects')[0];
			//console.log("user subject list:",users[i].userId,subjectsList);
			if(subjectsList == undefined){
				subjectsList = "n.a.";
			}
			var totalTimeSpent = getTimeSpent(timeCnt);
			/*console.log("USERS:",jsonPath(currMonthJson, '$.usersMetadata.user[?(@.userId=="'+users[i].userId+'")]'));
			console.log("USERS-1:",jsonPath(currMonthJson, '$.usersMetadata..[?(@.userId=="'+users[i].userId+'")]'));*/
			var totalLoginCount = 0;
			//console.log("USER META DATA::::",mthYr,":",users[i].userId,":",userMetadata);
			if(userMetadata){
				for(var j=0; j<userMetadata.length; j++){				
					totalLoginCount += Number(userMetadata[j].count);
				}
			}
			//console.log("totalLoginCount:",userMetadata,totalLoginCount);
			if(totalLoginCount>5 || timeCnt > 1){
				isUserActive = true;				
				activeUserCnt++;				
			}
			if(totalLoginCount>0){
				//moved out to another condition
				//activeUserCnt++;
				//console.log("Meta data:", userMetadata);	
				//var addedResourceData = jsonPath(currMonthJson, '$.added.usage[?(@.userId=="'+users[0][i].userId+'")]');		
				//add user name		
				var userName = users[i].firstName + " " + users[i].lastName;		
				var userId = users[i].userId;
				//console.log("USER ID:",users[i].userId,">>>>>", userData);
				//get resource count
				for(var j=0; j<userData.length; j++){						
					var dataCnt = Number(userData[j].count);
					// console.log("Data cnt:",dataCnt,":",userData[j].type);						
					totalCnt += dataCnt;
					if(isUserActive){
						activeUserAssetCnt+=dataCnt;		
					}	
					//
					switch(userData[j].type){
						case 'tp':					
						tpCnt += dataCnt;
						break;
						case 'asset_media':					
						mediaCnt += dataCnt;
						break;
						case 'asset_print':
						printCnt += dataCnt;
						break;
						case 'activity':
						activityCnt += dataCnt;
						break;
						case 'question':
						questionCnt += dataCnt;
						break;
						case 'game':
						gameCnt += dataCnt;
						break;
						case 'presentation':
						presentationCnt += dataCnt;
						break;
						case 'whiteboard':
						whiteboardCnt += dataCnt;
						break;
						case 'asset_annotation':
						annotationCnt += dataCnt;				
						break;
						case 'dictionary':
						dictionaryCnt += dataCnt;
						break;					
					}			
				}		
				
				/*var user = {};
				user.name = userName;
				user.chartData = [["TP",tpCnt], ["Media",mediaCnt], ["Printable",printCnt], ["Activity",activityCnt], ["Question",questionCnt], ["Game",gameCnt], ["Total",totalCnt]];*/		
				//old table structure data
				//userUtilizationArray[mthYr].tableData.push([userName, totalLoginCount, tpCnt, mediaCnt, printCnt, activityCnt, questionCnt, gameCnt, totalCnt]);
				var details = "TP:"+tpCnt+", Media:"+mediaCnt+", Printable:"+printCnt+", Activity:"+activityCnt+", Question:"+questionCnt+", Game:"+gameCnt;
				userUtilizationArray[mthYr].tableData.push([userName,subjectsList,totalLoginCount,totalCnt,totalTimeSpent,details]);
				//userUtilizationArray[mthYr].chartData.push([userName,{x:totalLoginCount,y:totalCnt,size:Math.random(),user:userName}]);
				//updated for the new bar chart
				//console.log("USER:",totalLoginCount,totalCnt,timeCnt);
				userUtilizationArray[mthYr].chartData.push({celogins:totalLoginCount,ceresourcehits:totalCnt,timespent:timeCnt,id:userId,name:userName,subjects:subjectsList});
				//console.log("MATH-", Math.random());		
			}
		}
		//console.log("ACTIVE USER COUNT #####",activeUserCnt);
		userUtilizationArray[mthYr].utilizationData.push({totalUsrCnt:totalUserCnt,activeUsrCnt:activeUserCnt,activeAssetUserCount:activeUserAssetCnt});
			
	}
}

/* for single user - month data */
function getUserDataForMonth(id, mthYr){
	userUtilizationMonthArr = {};
	userUtilizationMonthArr.loginData = [];
	userUtilizationMonthArr.timeData = [];
	userUtilizationMonthArr.assetsData = [];
	
	var daysInMth = daysInMonth(monthId, yearId);	
	var jsonDataForMonth = yearwiseData[mthYr];
	var tempUsers = jsonPath(jsonDataForMonth, '$.users.user');
	if(tempUsers){	
		if(tempUsers[0].length > 1){
			var users = jsonPath(jsonDataForMonth, '$.users.user')[0];
		} else {
			var users = jsonPath(jsonDataForMonth, '$.users.user');
		}
	}	
	if(users){		
		/*
		var userCnt = users.length;
		var isUserActive = false;
		totalUserCnt = userCnt;
		activeUserCnt=0;
		activeUserAssetCnt = 0;
		userUtilizationArray[mthYr] = {};
		userUtilizationArray[mthYr].tableData = [];
		userUtilizationArray[mthYr].chartData = [];
		userUtilizationArray[mthYr].utilizationData = [];
		*/
		//console.log("user count:", userCnt,":",users);
		for(var i=1; i<=daysInMth; i++){			
			totalCnt = 0;
			timeCnt = 0;
			var dayStr = yearId+"-"+(monthId<10?'0'+monthId:monthId)+"-"+(i<10?'0'+i:i)+" 00:00:00";
			
			//console.log("Day string:",dayStr);			
			var userUsageStats = jsonPath(terminalStatsData[mthYr],'$.user_session_statistics_list..[?(@.userId=="'+id+'" && @.usageDay=="'+dayStr+'")]');
			//console.log("userUsageStats --- ",userUsageStats);
			if(userUsageStats){
				for(var j=0; j<userUsageStats.length; j++){
					timeCnt+= Number(userUsageStats[j].usageMinutes)/60;
				}
			}
			timeCnt = Number(getTimeSpent(timeCnt,"decimal"));
			var userData = jsonPath(jsonDataForMonth, '$.viewed..[?(@.userId=="'+id+'" && @.logDay=="'+i+'")]');		
			var userMetadata = jsonPath(jsonDataForMonth, '$.usersMetadata..[?(@.userId=="'+id+'" && @.logDay=="'+i+'")]');
			//console.log("USER METADATA:",userMetadata);
			//console.log("USERs DATA:",userData);
			var totalLoginCount = 0;
			if(userMetadata){
				for(var j=0; j<userMetadata.length; j++){				
					totalLoginCount += Number(userMetadata[j].count);
				}
			}
			//if(totalLoginCount>0){			
				//add user name		
				//var userName = users[i].firstName + " " + users[i].lastName;		
				//var userId = users[i].userId;
				
				//get resource count
				for(var j=0; j<userData.length; j++){						
					var dataCnt = Number(userData[j].count);
					// console.log("Data cnt:",dataCnt,":",userData[j].type);						
					totalCnt += dataCnt;				
				}
			//}
			userUtilizationMonthArr.loginData.push(totalLoginCount);
			userUtilizationMonthArr.timeData.push(timeCnt);
			userUtilizationMonthArr.assetsData.push(totalCnt);
		}		
	}
	//console.log("USER details:",userUtilizationMonthArr);
	renderDailyLoginUserChart(id,mthYr);
}