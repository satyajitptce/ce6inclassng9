var obj = [{
	name : "Manmeet",
	logins : 6,
	hits : 10,
	time : 1.23
}, {
	name : "Sushant",
	logins : 8,
	hits : 18,
	time : 2.03
}, {
	name : "Vikash",
	logins : 4,
	hits : 22,
	time : 0.53
}, {
	name : "Anoop",
	logins : 10,
	hits : 10,
	time : 1.55
}, {
	name : "Yugandhar",
	logins : 2,
	hits : 4,
	time : 0.41
}];

/*
 $(document).ready(function(e){
 //sortOn("logins");
 $(".btns").click(function(e){
 console.log("e",e.target.text);
 change(e.target.text);

 })
 drawUserChart(obj);
 });
 */

var yScale, xScale, xAxis, yAxis, svg, barLabels;
var userData, noOfColumns;
function drawUserUtilizationChart(p_mthYr) {
	//hide all sort icons (reset on month change)
	$(".btnBar .cols div").css("visibility", "hidden");
	$("#userchart>svg").remove();
	userData = userUtilizationArray[p_mthYr].chartData;
	userData = sortOn("celogins");
	$(".btnBar .cols:first>div").css("visibility", "visible");
	userData.reverse();
	//console.log("userUtilizationArray[p_mthYr].chartData",userUtilizationArray[p_mthYr].chartData,$("#userUtilzationChart").width());
	var divWidth = $("#userUtilzationChart").width() - 100;
	var margin = {
		top : 0,
		right : 60,
		bottom : 0,
		left : 150
	};
	var width = divWidth - margin.left - margin.right;
	//var height = 450 - margin.top - margin.bottom;
	var heightVal;
	if (userUtilizationArray[p_mthYr].chartData.length < 26) {
		heightVal = 420;
	} else {
		heightVal = (userUtilizationArray[p_mthYr].chartData.length * 17);
	}

	var height = heightVal - margin.top - margin.bottom;
	var sessionData;
	noOfColumns = 2;
	if (terminalStatsData[p_mthYr]) {
		sessionData = jsonPath(terminalStatsData[p_mthYr], '$.user_session_statistics_list');
		if (sessionData) {
			noOfColumns = 3;
		}
	}
	if (noOfColumns > 2) {
		$(".btnBar").find(".timespent").css("display", "block");
		$(".btnBar td.cols").css("width", "41%");
	} else {
		$(".btnBar").find(".timespent").css("display", "none");
		$(".btnBar td.cols").css("width", "71%");
	}
	//alert(noOfColumns);
	var offset = (width + 100) / noOfColumns;
	//var max = d3.max(userData,function(d){return d.ceresourcehits});
	/*
	 var maxArr = [];
	 maxArr.push(d3.max(userData,function(d){return d.ceresourcehits}));
	 maxArr.push(d3.max(userData,function(d){return d.celogins}));
	 if(noOfColumns > 2){
	 maxArr.push(d3.max(userData,function(d){return d.timespent}));
	 }
	 */
	var max = d3.max(userData, function(d) {
		return d.celogins
	});
	xScale = d3.scale.linear().domain([0, max]).range([0, (width / noOfColumns)]);
	//alert("width/noOfColumns"+width+":"+noOfColumns);
	barLabels = userData.map(function(datum) {
		return datum.name
	});
	yScale = d3.scale.ordinal().domain(barLabels).rangeRoundBands([0, height], 0.1);

	xAxis = d3.svg.axis().scale(xScale).orient("bottom");

	yAxis = d3.svg.axis().scale(yScale).orient("left");

	svg = d3.select("#userchart").append("svg").attr("width", width + margin.left + margin.right).attr("height", height + margin.top + margin.bottom).append("g").attr("transform", "translate(" + margin.left + "," + margin.top + ")");

	//div to display tool tip (subjects)
	var div = d3.select("#userchart").append("div").attr("class", "subjectToolTip").style("opacity", "0");

	$("#userchart>svg").css("height", height + 25);
	/*svg.append("g")
	 .attr("class", "x axis")
	 .attr("transform", "translate(0," + height + ")")
	 .call(xAxis);*/
	svg.append("svg:line").attr("x1", 0).attr("y1", 0).attr("x2", (divWidth - margin.right) - 20).attr("y2", 0).style("fill", "none").style("shape-rendering", "crispEdges").style("stroke", "#000");

	svg.append("g").attr("class", "y axis").call(yAxis);
	/*.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", -100)
	.attr("dy", ".71em")
	.style("text-anchor", "end")
	.text("Users");
	*/
	// grid lines
	svg.selectAll("g.bar").data(userData).enter().append("svg:line").attr("x1", 0).attr("x2", (divWidth - margin.right) - 20).attr("y1", function(d) {
		return yScale(d.name) + yScale.rangeBand() / 2
	}).attr("y2", function(d) {
		return yScale(d.name) + yScale.rangeBand() / 2
	}).style("stroke", "#ddd");

	//login bars
	svg.selectAll("g.bar").data(userData).enter().append("svg:rect").attr("class", "bar barlogins")
	//.attr("x", function(d){console.log(d.celogins,x(d.celogins));return x(d.celogins);})
	.attr("x", 0).attr("width", function(d) {
		return xScale(d.celogins);
	}).attr("y", function(d) {
		return yScale(d.name);
	}).attr("height", yScale.rangeBand()).on('mouseover', function(d) {
		d3.select(this).attr("fill-opacity", .5);
		if (d.subjects && d.subjects != "" && d.subjects.toLowerCase() != "others") {
			div.transition().duration(500).style("opacity", .9);
			div.html(d.name + ": " + d.subjects).style("left", ((d3.event.pageX) + 15) + "px").style("top", (d3.event.pageY - 28) + "px");
		}
	}).on('mouseout', function(d) {
		d3.select(this).attr("fill-opacity", .9);
		div.transition().duration(500, function() {
			this.style("visibility", "hidden")
		}).style("opacity", 0);
	}).on('click', function(d) {
		//console.log("ID:", d.id, p_mthYr);
		getUserDataForMonth(d.id, p_mthYr);
	})
	svg.selectAll("g.bar").data(userData).enter().append("svg:text").text(function(d) {
		//console.log("---ddddd:",d.celogins)
		return d.celogins;
	}).attr({
		'x' : function(d) {
			//console.log("ddddd:",d)
			return xScale(d.celogins) + 5;
		},
		'y' : function(d) {
			return yScale(d.name) + yScale.rangeBand() / 2
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		//'text-anchor':'middle'
	});
	//hits bars
	var max = d3.max(userData, function(d) {
		return d.ceresourcehits
	});
	xScale = d3.scale.linear().domain([0, max]).range([0, (width / noOfColumns)]);
	//var offset = 150;
	//creating bars
	svg.selectAll("g.bar").data(userData).enter().append("svg:rect").attr("class", "bar barhits")
	//.attr("x", function(d){console.log(d.celogins,x(d.celogins));return x(d.celogins);})
	.attr("x", offset).attr("width", function(d) {
		return xScale(d.ceresourcehits);
	}).attr("y", function(d) {
		return yScale(d.name);
	}).attr("height", yScale.rangeBand()).on('mouseover', function(d) {
		if (d.subjects && d.subjects != "" && d.subjects.toLowerCase() != "others") {
			div.transition().duration(500).style("opacity", .9);
			div.html(d.name + ": " + d.subjects).style("left", ((d3.event.pageX) + 15) + "px").style("top", (d3.event.pageY - 28) + "px");
		}
	}).on('mouseout', function(d) {
		div.transition().duration(500, function() {
			this.style("visibility", "hidden")
		}).style("opacity", 0);
	}).on('click', function(d) {
		//console.log("ID:", d.id);
		getUserDataForMonth(d.id, p_mthYr);
	})
	//creating text
	svg.selectAll("g.bar").data(userData).enter().append("svg:text").text(function(d) {
		//console.log("---ddddd:",d.ceresourcehits)
		return d.ceresourcehits;
	}).attr({
		'x' : function(d) {
			//console.log("ddddd:",d)
			return xScale(d.ceresourcehits) + offset + 5;
		},
		'y' : function(d) {
			return yScale(d.name) + yScale.rangeBand() / 2
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		//'text-anchor':'middle'
	});

	if (noOfColumns > 2) {
		var max = d3.max(userData, function(d) {
			return d.timespent
		});
		xScale = d3.scale.linear().domain([0, max]).range([0, (width / noOfColumns)]);
		//time-spent bars
		offset += offset;
		svg.selectAll("g.bar").data(userData).enter().append("svg:rect").attr("class", "bar bartime")
		//.attr("x", function(d){console.log(d.celogins,x(d.celogins));return x(d.celogins);})
		.attr("x", offset).attr("width", function(d) {
			return xScale(d.timespent);
		}).attr("y", function(d) {
			return yScale(d.name);
		}).attr("height", yScale.rangeBand()).on('mouseover', function(d) {
			d3.select(this).attr("fill-opacity", .5);
			if (d.subjects && d.subjects != "" && d.subjects.toLowerCase() != "others") {
				div.transition().duration(500).style("opacity", .9);
				div.html(d.name + ": " + d.subjects).style("left", ((d3.event.pageX) + 15) + "px").style("top", (d3.event.pageY - 28) + "px");
			}
		}).on('mouseout', function(d) {
			d3.select(this).attr("fill-opacity", .9);
			div.transition().duration(500, function() {
				this.style("visibility", "hidden")
			}).style("opacity", 0);
		}).on('click', function(d) {
			//console.log("ID:", d.id);
			getUserDataForMonth(d.id, p_mthYr);
		})
		svg.selectAll("g.bar").data(userData).enter().append("svg:text").text(function(d) {
			//console.log("---ddddd:",d.timeSpent)
			return getTimeSpent(d.timespent) + " (hrs)";
		}).attr({
			'x' : function(d) {
				//console.log("ddddd:",d)
				return xScale(d.timespent) + offset + 5;
			},
			'y' : function(d) {
				return yScale(d.name) + yScale.rangeBand() / 2;
			},
			'font-family' : 'Tahoma',
			'font-size' : '10px',
			'fill' : 'black',
			//'text-anchor':'middle'
		});
	}
}

function sortUserChart(p_item, p_val) {
	$(".btnBar .cols div").css("visibility", "hidden");
	$(p_item).parent().find('div.sortBtn').css("visibility", "visible");
	p_val = p_val.replace(/ /g, "");
	var newData = sortOn((p_val).toLowerCase());
	newData.reverse();

	var y0 = yScale.domain(newData.map(function(d) {
		return d.name
	})).copy();

	var transition = svg.transition().duration(300), delay = function(d, i) {
		return i * 50;
	};

	transition.selectAll(".bar").delay(delay).attr("y", function(d) {
		return y0(d.name);
	})

	transition.selectAll("svg>g>text").delay(delay).attr("y", function(d) {
		return y0(d.name) + y0.rangeBand() / 2;
	})

	transition.select(".y.axis").call(yAxis).selectAll("g").delay(delay);
}

function sortOn(val) {
	var sorted = _.sortBy(userData, val);
	//console.log(obj,sorted);
	return sorted;
}

function renderDailyUsageUserChart(id, mthYr) {
	var usrDetails = jsonPath(yearwiseData[mthYr],'$.users.user..[?(@.userId=="'+id+'")]');	
	if(usrDetails){
		var userName = usrDetails[0].firstName + " " + usrDetails[0].lastName;
	}
	
	/*
	userUtilizationMonthArr.loginData;
	userUtilizationMonthArr.timeData;
	userUtilizationMonthArr.assetsData;
	*/
	// define dimensions of graph
	var m = [40, 100, 80, 100];
	// margins
	var divWidth = $("#userMonthChart").width();
	var w = divWidth - m[1] - m[3];
	// width
	var h = 400 - m[0] - m[2];
	// height

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
	var data1 = userUtilizationMonthArr.loginData;
	var data2 = userUtilizationMonthArr.timeData;

	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, data1.length-1]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	var maxY1 = _.max(userUtilizationMonthArr.loginData);
	var y1 = d3.scale.linear().domain([0, maxY1]).range([h, 0]);
	// in real world the domain would be dynamically calculated from the data
	var maxY2 = _.max(userUtilizationMonthArr.timeData);
	maxY2 = maxY2>1?maxY2:1;
	var y2 = d3.scale.linear().domain([0, maxY2]).range([h, 0]);
	// in real world the domain would be dynamically calculated from the data
	// automatically determining max range can work something like this
	// var y = d3.scale.linear().domain([0, d3.max(data)]).range([h, 0]);

	// create a line function that can convert data[] into x and y points
	var line1 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X1 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y1 value for data point: ' + d + ' to be at: ' + y1(d) + " using our y1Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y1(d);
	})
	// create a line function that can convert data[] into x and y points
	var line2 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X2 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y2 value for data point: ' + d + ' to be at: ' + y2(d) + " using our y2Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y2(d);
	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#userMonthChart").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform", "translate(" + m[3] + "," + m[0] + ")");

	// create xAxis
	var xAxis = d3.svg.axis()
		.scale(x)
		.tickSize(-h)
		.tickFormat(function (d){return (d+1)})
		.ticks(userUtilizationMonthArr.loginData.length)
		.tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g").attr("class", "x axis").attr("transform", "translate(0," + h + ")").call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y1).orient("left").ticks(maxY1).tickSubdivide(false);
	// Add the y-axis to the left
	graph.append("svg:g").attr("class", "y axis axisLeft").attr("transform", "translate(-15,0)").call(yAxisLeft);

	// create right yAxis
	var yAxisRight = d3.svg.axis().scale(y2).orient("right").ticks(maxY2).tickSubdivide(false);
	// Add the y-axis to the right
	graph.append("svg:g").attr("class", "y axis axisRight").attr("transform", "translate(" + (w + 15) + ",0)").call(yAxisRight);

	// add lines
	// do this AFTER the axes above so that the line is above the tick-lines
	
	graph.append("svg:path").attr("d", line1(data1)).attr("class", "data1");
	graph.append("svg:path").attr("d", line2(data2)).attr("class", "data2");
	
	graph.append("svg:text").text(userName).attr({
		'x' : function(d) {
			//console.log("ddddd:",d)
			return 0;
		},
		'y' : function(d) {
			return -10;
		},
		'font-family' : 'Tahoma',
		'font-size' : '14px',
		'font-weight':'bold',
		'fill' : 'black',
		//'text-anchor':'middle'
	});
	//
	graph.append("svg:text").text("Logins").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return -h/2;
		},
		'transform':'rotate(-90)',
		'y' : function(d) {
			return -45;
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	//
	graph.append("svg:text").text("Time").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return h/2;
		},
		'transform':'rotate(90)',
		'y' : -(w+35),
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	
	renderDailyHitsUserChart();
};

//user daily login hits chart
function renderDailyLoginUserChart(id, mthYr) {
	$("#userMonthLoginChart").empty();
	var usrDetails = jsonPath(yearwiseData[mthYr],'$.users.user..[?(@.userId=="'+id+'")]');	
	if(usrDetails){
		var userName = usrDetails[0].firstName + " " + usrDetails[0].lastName;
	}
	$('#userChartsMain').css('visibility','visible');
	/*
	var usrDetails = jsonPath(yearwiseData[mthYr],'$.users.user..[?(@.userId=="'+id+'")]');	
	if(usrDetails){
		var userName = usrDetails[0].firstName + " " + usrDetails[0].lastName;
	}
	*/
	
	// define dimensions of graph
	var m = [20, 100, 20, 120];
	// margins
	var divWidth = $("#userMonthLoginChart").width();
	var w = divWidth - m[1] - m[3];
	// width
	var h = 150 - m[0] - m[2];
	// height

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
	var data = userUtilizationMonthArr.loginData;
	//console.log("data",data);
	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, data.length-1]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	var maxY = _.max(userUtilizationMonthArr.loginData);
	var y1 = d3.scale.linear().domain([0, maxY]).range([h, 0]);
	
	// create a line function that can convert data[] into x and y points
	var line1 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X1 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y1 value for data point: ' + d + ' to be at: ' + y1(d) + " using our y1Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y1(d);
	})
	// create a line function that can convert data[] into x and y points
	var line2 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X2 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y2 value for data point: ' + d + ' to be at: ' + y2(d) + " using our y2Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y2(d);
	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#userMonthLoginChart").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform", "translate(" + m[3] + "," + m[0] + ")");
	//d3.select("#userMonthLoginChart svg").style("background-color", "rgba(255,0,0,0.3)");
	// create xAxis
	var xAxis = d3.svg.axis()
		.scale(x)
		.tickSize(-h)
		.tickFormat(function (d){return (d+1)})
		.ticks(userUtilizationMonthArr.loginData.length)
		.tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g").attr("class", "x axis").attr("transform", "translate(0," + h + ")").call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y1).orient("left").ticks(maxY).tickSubdivide(false);
	// Add the y-axis to the left
	graph.append("svg:g").attr("class", "y axis axisLeft").attr("transform", "translate(-15,0)").call(yAxisLeft);

	
	// add lines
	// do this AFTER the axes above so that the line is above the tick-lines
	
	graph.append("svg:path").attr("d", line1(data)).attr("class", "data1");
	//graph.append("svg:path").attr("d", line2(data2)).attr("class", "data2");
		
	$("#usernameTitle").text(userName);
	/*
	graph.append("svg:text").text(userName).attr({
		'x' : function(d) {
			//console.log("ddddd:",d)
			return -25;
		},
		'y' : function(d) {
			return -25;
		},
		'font-family' : 'Tahoma',
		'font-size' : '14px',
		'font-weight':'bold',
		'fill' : 'black',
		//'text-anchor':'middle'
	});	
	*/
	 graph.append("svg:text").text("Login count").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return -h/2;
		},
		'transform':'rotate(-90)',
		'y' : function(d) {
			return -45;
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	renderDailyHitsUserChart();
	
};

//user daily asset hits chart
function renderDailyHitsUserChart() {
	$("#userMonthAssetChart").empty();
	/*
	var usrDetails = jsonPath(yearwiseData[mthYr],'$.users.user..[?(@.userId=="'+id+'")]');	
	if(usrDetails){
		var userName = usrDetails[0].firstName + " " + usrDetails[0].lastName;
	}
	*/
	
	// define dimensions of graph
	var m = [20, 100, 20, 120];
	// margins
	var divWidth = $("#userMonthAssetChart").width();
	var w = divWidth - m[1] - m[3];
	// width
	var h = 150 - m[0] - m[2];
	// height

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
	var data = userUtilizationMonthArr.assetsData;
	//console.log("data",data);
	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, data.length-1]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	var maxY = _.max(userUtilizationMonthArr.assetsData);
	var y1 = d3.scale.linear().domain([0, maxY]).range([h, 0]);
	
	// create a line function that can convert data[] into x and y points
	var line1 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X1 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y1 value for data point: ' + d + ' to be at: ' + y1(d) + " using our y1Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y1(d);
	})
	// create a line function that can convert data[] into x and y points
	var line2 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X2 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y2 value for data point: ' + d + ' to be at: ' + y2(d) + " using our y2Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y2(d);
	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#userMonthAssetChart").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform", "translate(" + m[3] + "," + m[0] + ")");

	// create xAxis
	var xAxis = d3.svg.axis()
		.scale(x)
		.tickSize(-h)
		.tickFormat(function (d){return (d+1)})
		.ticks(userUtilizationMonthArr.assetsData.length)
		.tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g").attr("class", "x axis").attr("transform", "translate(0," + h + ")").call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y1).orient("left").ticks(3).tickSubdivide(false);
	// Add the y-axis to the left
	graph.append("svg:g").attr("class", "y axis axisLeft").attr("transform", "translate(-15,0)").call(yAxisLeft);

	
	// add lines
	// do this AFTER the axes above so that the line is above the tick-lines
	
	graph.append("svg:path").attr("d", line1(data)).attr("class", "data1");
	//graph.append("svg:path").attr("d", line2(data2)).attr("class", "data2");
		
	 graph.append("svg:text").text("Assets Used").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return -h/2;
		},
		'transform':'rotate(-90)',
		'y' : function(d) {
			return -45;
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	if (noOfColumns > 2) {
		renderDailyTimeUserChart();
	}
};

//user daily time-spent chart
function renderDailyTimeUserChart() {
	$("#userMonthTimeChart").empty();
	/*
	var usrDetails = jsonPath(yearwiseData[mthYr],'$.users.user..[?(@.userId=="'+id+'")]');	
	if(usrDetails){
		var userName = usrDetails[0].firstName + " " + usrDetails[0].lastName;
	}
	*/
	
	// define dimensions of graph
	var m = [20, 100, 20, 120];
	// margins
	
	var divWidth = $("#userMonthTimeChart").width();
	var w = divWidth - m[1] - m[3];
	// width
	var h = 150 - m[0] - m[2];
	// height

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
	var data = userUtilizationMonthArr.timeData;
	//console.log("data",data);
	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, data.length-1]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	var maxY = _.max(userUtilizationMonthArr.timeData);
	var y1 = d3.scale.linear().domain([0, maxY]).range([h, 0]);
	
	// create a line function that can convert data[] into x and y points
	var line1 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X1 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y1 value for data point: ' + d + ' to be at: ' + y1(d) + " using our y1Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y1(d);
	})
	// create a line function that can convert data[] into x and y points
	var line2 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X2 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y2 value for data point: ' + d + ' to be at: ' + y2(d) + " using our y2Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y2(d);
	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#userMonthTimeChart").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform", "translate(" + m[3] + "," + m[0] + ")");

	// create xAxis
	var xAxis = d3.svg.axis()
		.scale(x)
		.tickSize(-h)
		.tickFormat(function (d){return (d+1)})
		.ticks(userUtilizationMonthArr.timeData.length)
		.tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g").attr("class", "x axis").attr("transform", "translate(0," + h + ")").call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y1).orient("left").ticks(maxY).tickSubdivide(false);
	// Add the y-axis to the left
	graph.append("svg:g").attr("class", "y axis axisLeft").attr("transform", "translate(-15,0)").call(yAxisLeft);

	
	// add lines
	// do this AFTER the axes above so that the line is above the tick-lines
	
	graph.append("svg:path").attr("d", line1(data)).attr("class", "data1");
	//graph.append("svg:path").attr("d", line2(data2)).attr("class", "data2");
		
	 graph.append("svg:text").text("Time spent (hrs)").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return -h/2;
		},
		'transform':'rotate(-90)',
		'y' : function(d) {
			return -45;
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	
};
