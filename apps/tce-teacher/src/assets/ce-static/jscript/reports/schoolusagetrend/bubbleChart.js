//var dataChart = '{ "name": "subjects", "children": [  {   "name": "accountancy",   "children": [          {"name": "EC", "size": 38},      {"name": "Gr 1-2", "size": 12},      {"name": "Gr 3-4", "size": 14},      {"name": "Gr 5-6", "size": 43}     ]    },    {     "name": "Biology",     "children": [      {"name": "EC", "size": 18},      {"name": "Gr 1-2", "size": 0},      {"name": "Gr 3-4", "size": 64},      {"name": "Gr 5-6", "size": 33}     ]    },    {     "name": "Chemistry",     "children": [      {"name": "EC", "size": 0},      {"name": "Gr 1-2", "size": 42},      {"name": "Gr 3-4", "size": 0},      {"name": "Gr 5-6", "size": 22}     ]    },    {     "name": "Economics",     "children": [      {"name": "EC", "size": 28},      {"name": "Gr 1-2", "size": 42},      {"name": "Gr 3-4", "size": 0},      {"name": "Gr 5-6", "size": 22}     ]    },    {     "name": "EVS",     "children": [      {"name": "EC", "size": 28},      {"name": "Gr 1-2", "size": 0},      {"name": "Gr 3-4", "size": 0},      {"name": "Gr 5-6", "size": 22}     ]    }   ]  }';
var dataChart;
function drawBubbleChart(p_mthYr){
	//
	dataChart = heatmapChartData[p_mthYr][0];
	//console.log("dataChart:",dataChart);
	$("#contentMatrixBubbleChart>svg").empty();
	//console.log("# # #",JSON.stringify(dataChart));
	renderBubbleChart();
}
//for range data
function drawBubbleChartRange(mthStart,mthEnd){
	//	
	//dataChart = heatmapChartData[p_mthYr][0];
	var data = getRangeDataForHeatmap(mthStart,mthEnd);
	//console.log("data:",data);
	dataChart = data[0];	
	$("#contentMatrixBubbleChart>svg").empty();
	//console.log("# # #",JSON.stringify(dataChart));
	renderBubbleChart();
}

function renderBubbleChart(){
	var diameter = 600,
	    format = d3.format(",d"),
	    color = d3.scale.category20();
	
	var bubble = d3.layout.pack()
	    .sort(null)
	    .size([diameter, diameter])
	    .padding(1.5);
	
	var svg = d3.select("#contentMatrixBubbleChart svg")
	    .attr("width", diameter)
	    .attr("height", diameter)
	    .attr("class", "bubble");
	
	//div to display tool tip (subjects)
	var div = d3.select("#contentMatrixBubbleChart").append("div")
		.attr("class","subjectToolTip")
		.style("position","absolute")
		.style("opacity","0");
		
	//d3.json("test.json", function(error, root) {
	json = dataChart;
	  var node = svg.selectAll(".node")
	      .data(bubble.nodes(classes(json))
	      .filter(function(d) { return !d.children; }))
	    .enter().append("g")
	      .attr("class", "node")
	      .attr("transform", function(d) { return "translate(" + d.x + "," + d.y + ")"; });
	
	  node.append("title")
	      //.text(function(d) { return d.className + ": " + format(d.value)+" views"; });
			.text(function(d) { return d.className });
	  node.append("circle")
	      .attr("r", function(d) { return d.r; })	      
	      .style("fill", function(d) { return color(d.packageName); })
	      .on('mousedown',function(d){
	      	//var parentOffset = $(this).parent().offset();
	      	var relX = $("#contentMatrixBubbleChart").width() - 150;//d3.event.pageX - parentOffset.left;
	      	var relY = 150//d3.event.pageY - parentOffset.top;
	      //	console.log("thisssssss=",hexToRgb($(this).css("fill")));
	      	var tempFill = hexToRgb($(this).css("fill"));
			d3.select(this).attr("fill-opacity",.5);			
			div.transition()
			.duration(500)
			.style("opacity",.9);
			div.html(d.tooltip)
			.style("left",relX+"px")
			.style("top",relY+"px")			
			.style("background-color","rgba("+tempFill.r+","+tempFill.g+","+tempFill.b+","+0.4+")");
			//.style("left",((d3.event.pageX)+15)+"px")
			//.style("top",(d3.event.pageY-28)+"px");
			
		})
		.on('mouseout',function(d){
			d3.select(this).attr("fill-opacity",.9);
			div.transition()
				.duration(500,function(){this.style("visibility","hidden")})
				.style("opacity",0);
				
		});
	
	  node.append("text")
	      .attr("dy", ".3em")
	      .style("text-anchor", "middle")
	      .text(function(d) { return d.className.substring(0, d.r / 3); });
	//});
	
	d3.select(self.frameElement).style("height", diameter + "px");
}
	
function hexToRgb(hex) {
    // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
        return r + r + g + g + b + b;
    });

    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    return result ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16)
    } : null;
}

// Returns a flattened hierarchy containing all leaf nodes under the root.
function classes(root) {
  var classes = [];

  function recurse(name, node) {
    if (node.children) node.children.forEach(function(child) { recurse(node.name, child); });
    else classes.push({packageName: name, className: node.name, value: node.size, tooltip: node.tooltip});
  }

  recurse(null, root);
  return {children: classes};
}
	
	