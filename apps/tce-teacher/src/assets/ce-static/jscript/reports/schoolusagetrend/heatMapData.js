//var heatmapTableData={};
var uniqueSubjectList=[];
var gradesArr = ["ECE","1-2","3-4", "5-6", "7-8", "9-10", "11-12"];
var heatmapData;
var mthYr;
var rangeHeatmapJson = [];
//
var gradesFilterArr = [];
var subjectsFilterArr = [];
var typeFilterArr = [];

function getheatmapData(){
	mthYr = currMonth+""+currYear;	
	//console.log("heatmapData---------",heatmapData);
	getUniqueSubjectList();
}
/*
function getHeatMapRange(mthStart,mthEnd){	
	getRangeDataForHeatmap(mthStart,mthEnd);
}
*/
function getRangeDataForHeatmap(mthStart,mthEnd){
	hmGradeListArr = [];
	hmSubjectListArr = [];
	hmTypeListArr = [];	
	uniqueSubjectList = [];
	var rangeArr = getRange(mthStart,mthEnd);
	//heatmapRangeData = heatmapChartData[rangeArr[0]].slice(0);
	rangeHeatmapJson = [];
	heatmapRangeData = [{"name":"subjects","children":[]}];
	//console.log("range arr.length",rangeArr.length);
	for(var i=0; i<rangeArr.length; i++){
		var mthYr = rangeArr[i];
		var heatmapJson = jsonPath(yearwiseData[mthYr], '$.heatMapData.heatMapResource');
		if(heatmapJson){
			rangeHeatmapJson.push(heatmapJson);
			//get the data here
			//change all variables here
			var hmData = [];	 							
				if(heatmapJson[0].length > 1){
					var tempSubList = _.uniq(_.flatten(_.compact(_.map(heatmapJson[0], 
					function(sub){	 		
						return toTitleCase(sub.subject);
						//return String(sub.subject).toLowerCase();
					}))));
					uniqueSubjectList = uniqueSubjectList.concat(tempSubList); 	 		
				} else {
					uniqueSubjectList.push(heatmapJson[0].subject);
				}
			/*
			var tempSubList = _.uniq(_.flatten(_.compact(_.map(heatmapJson[0], 
				function(sub){	 		
					return toTitleCase(sub.subject);
					//return String(sub.subject).toLowerCase();
				}))));
			uniqueSubjectList = uniqueSubjectList.concat(tempSubList);
			*/ 	 	
			//console.log("tempSubList",tempSubList);
		}
	}		
	uniqueSubjectList = _.uniq(uniqueSubjectList);		
	//console.log("rangeHeatmapJson",rangeHeatmapJson);
	//console.log("uniqueSubjectList.length",uniqueSubjectList.length);
 	for(var j=0; j<uniqueSubjectList.length; j++){ 		
	 	var subject = uniqueSubjectList[j];
	 	//console.log("subjectsFilterArr:",subjectsFilterArr,subject,subjectsFilterArr.indexOf(subject));
	 	if(subjectsFilterArr.indexOf(subject) == -1){				 	
		 	hmData = [];
		 	var str = "";
		 	var tooltipObj={};
		 	hmData.push(subject);
		 	hmSubjectListArr.push(subject);
		 	var cnt = 0;
		 	var obj = {};
		 	obj.name = subject;
		 	obj.children = [];
	 		for(var i=0; i<7; i++){
		 		//var data = jsonPath(heatmapJson, '$.[?(@.subject=="'+subject+'"&&@.level=="Level '+i+'")]');
	 			var data = [];
	 			for(var cnt = 0; cnt < rangeHeatmapJson.length; cnt++){ 				
		 			for(var m=0; m<rangeHeatmapJson[cnt][0].length; m++){
		 				if(rangeHeatmapJson[cnt][0][m].subject.toLowerCase() == subject.toLowerCase() && rangeHeatmapJson[cnt][0][m].level == "Level "+i){
		 					data.push(rangeHeatmapJson[cnt][0][m]);
		 				}
		 			}
	 			}
	 			//console.log("heatmapDATA:",data);
		 		//console.log("data:"+subject+":",i,">>",data);
		 		var val = 0;	 		
		 		if(data.length){
		 			hmGradeListArr.push(gradesArr[i]);
		 			var val = 0;
		 			tooltipObj.type = {};
		 			//var str = "";
		 			for(var m=0; m<data.length; m++){
		 				var assetType = getAssetType(data[m].type);
		 				if(typeFilterArr.indexOf(assetType) == -1){ 
			 				hmTypeListArr.push(assetType);
			 				if(tooltipObj.type[assetType]){
			 					tooltipObj.type[assetType]+=Number(data[m].count);
			 				} else { 					
			 					tooltipObj.type[assetType]=Number(data[m].count);
			 				}
			 				//str += getAssetType(data[m].type) + "-" + data[m].count+"<br>";
			 				val += Number(data[m].count);
		 				}
		 			}
		 			/*tooltipObj.grade = gradesArr[i];
		 			tooltipObj.subject = subject;
		 			tooltipObj.hits = val;*/
		 			str = "<b>"+gradesArr[i] + ", "+subject+"</b><br><b>Total hits:</b>("+val+")<br>";
		 			//val = Number(data[0].count);
		 			//console.log("strObject:",tooltipObj,str);
		 			for(var p in tooltipObj.type){
		 				//console.log("--->",p,tooltipObj.type[p]);
		 				str += p + "-" + tooltipObj.type[p]+"<br>";
		 			}
		 			//console.log("str:",str)
		 			if(gradesFilterArr.indexOf(gradesArr[i]) == -1){		 
			 			hmData.push(val);
			 			//cnt+=val;
			 			obj.children.push({"grade":gradesArr[i], "subject":subject, "hits":val, "name":(gradesArr[i]+", "+subject+" ("+val+" Hits)"),"size":val,"tooltip":str});
		 			}
		 		} else {
		 			val = 0;
		 			hmData.push(0);
		 		}
		 		
		 	}
		 	
		 	//heatmapTableData[mthYr].push(hmData);
		 	heatmapRangeData[0].children.push(obj);
	 	}
	 }
	 hmGradeListArr = _.uniq(hmGradeListArr);
	 hmSubjectListArr = _.uniq(hmSubjectListArr);
	 hmTypeListArr = _.uniq(hmTypeListArr);
	 
	 //console.log("heatmapRange--Data:",heatmapRangeData);
	 //console.log("HM--DATA:", hmGradeListArr, hmSubjectListArr, hmTypeListArr);
	 return heatmapRangeData;	
}

function getUniqueSubjectList(){
	heatmapData = jsonPath(yearwiseData[mthYr], '$.heatMapData.heatMapResource');
	hmGradeListObj[mthYr] = [];
	hmSubjectListObj[mthYr] = [];
	hmTypeListObj[mthYr] = [];
	uniqueSubjectList = [];	
	//console.log("HEATMAP data:", heatmapData);
	//var subList = jsonPath(heatmapData, '$.[*].subject');
	//_.uniq(subList);
	var hmData = [];	 
	heatmapTableData[mthYr] = [];	
	heatmapChartData[mthYr] = [{"name":"subjects","children":[]}];
	//console.log("heatmapChartData >>>>BEFORE>>>>>",monthId,yearId,mthYr,heatmapChartData);
	if(heatmapData){	
		if(heatmapData[0].length > 1){
			uniqueSubjectList = _.uniq(_.flatten(_.compact(_.map(heatmapData[0], 
				function(sub){	 		
					return toTitleCase(sub.subject);
				//return String(sub.subject).toLowerCase();
				})))); 	 		
		} else {
			uniqueSubjectList.push(heatmapData[0].subject);
		}
	}
	//console.log("UNIQUE subject list:", uniqueSubjectList);
		
 	for(var j=0; j<uniqueSubjectList.length; j++){ 		
	 	var subject = uniqueSubjectList[j];
	 	//console.log("subjectsFilterArr:",subjectsFilterArr,subject,subjectsFilterArr.indexOf(subject));
	 	if(subjectsFilterArr.indexOf(subject) == -1){	
		 	hmData = [];
		 	hmData.push(subject);
			hmSubjectListObj[mthYr].push(subject);
		 	var cnt = 0;
		 	var obj = {};
		 	obj.name = subject;
		 	obj.children = [];
	 		for(var i=0; i<7; i++){
		 		//var data = jsonPath(heatmapData, '$.[?(@.subject=="'+subject+'"&&@.level=="Level '+i+'")]');
	 			var data = [];
	 			for(var m=0; m<heatmapData[0].length; m++){
	 				if(heatmapData[0][m].subject.toLowerCase() == subject.toLowerCase() && heatmapData[0][m].level.includes("Level")){
	 					//console.log("heatmapData[0][m]==============",heatmapData[0][m]);
	 					data.push(heatmapData[0][m]);
	 				}
	 			}
	 			//console.log("heatmapData:",data);
		 		//console.log("data:"+subject+":",i,">>",data);
		 		var val = 0;	 		
		 		if(data.length){
		 			hmGradeListObj[mthYr].push(gradesArr[i]);
		 			var val = 0;
		 			var str = "";
		 			for(var m=0; m<data.length; m++){
		 				var assetType = getAssetType(data[m].type);
		 				//console.log("    assetType        :",assetType,hmTypeListObj[mthYr]);
		 				if(typeFilterArr.indexOf(assetType) == -1){ 
		 					hmTypeListObj[mthYr].push(assetType);
			 				str += assetType + "-" + data[m].count+"<br>";
			 				val += Number(data[m].count);
		 				}
		 			}
		 			str = "<b>"+gradesArr[i] + ", "+subject+"</b><br><b>Total hits:</b>("+val+")<br>" + str;
		 			//val = Number(data[0].count);
		 			if(gradesFilterArr.indexOf(gradesArr[i]) == -1){		 				
			 			hmData.push(val);
			 			//cnt+=val;
			 			obj.children.push({"grade":gradesArr[i], "subject":subject, "hits":val, "name":(gradesArr[i]+", "+subject+" ("+val+" Hits)"),"size":val,"tooltip":str});		 				
		 			}
		 			
		 		} else {
		 			val = 0;
		 			hmData.push(0);
		 		}
		 		
		 	}
		 	
		 	heatmapTableData[mthYr].push(hmData);
		 	heatmapChartData[mthYr][0].children.push(obj);
		 }
	 }
	 hmGradeListObj[mthYr] = _.uniq(hmGradeListObj[mthYr]);
	 hmSubjectListObj[mthYr] = _.uniq(hmSubjectListObj[mthYr]);
	 hmTypeListObj[mthYr] = _.uniq(hmTypeListObj[mthYr]);
	 
	 //console.log("heatmapTableData >>>>>>>>>",heatmapTableData);
	 //console.log("heatmapChartData >>>>>>>>>",heatmapChartData);
	 //console.log("HM DATA:", hmGradeListObj[mthYr], hmSubjectListObj[mthYr], hmTypeListObj[mthYr]);
}

function updateFilters(){
	mthYr = monthId+""+yearId;
	//console.log("updateFilters",mthYr,hmSubjectListObj);
	$("#heatmapFiltersDiv").empty();
	if(hmGradeListObj[mthYr].length){
		$("#heatmapFiltersDiv").append($("<div id='gradeslist' class='filterDivs'></div>"));
		$("#gradeslist").append($('<input id="gradesChk" type="checkbox" class="selectallchk" name="gradeslist" checked="checked" value="gradeslist">Select All<br><br>'));
		for(var i=0; i<hmGradeListObj[mthYr].length; i++){
			//console.log("-->",'<input type="checkbox" name="gradelist" value="'+hmGradeListObj[mthYr][i]+'">'+hmGradeListObj[mthYr][i]);		
			$("#gradeslist").append($('<input type="checkbox" class="chkbox" name="gradesChk" value="'+hmGradeListObj[mthYr][i]+'">'));
			$("#gradeslist input:eq('"+(i+1)+"')").attr("checked","checked");
			$("#gradeslist input:eq('"+(i+1)+"')").after(hmGradeListObj[mthYr][i]+'<br>');
		}
	}
	
	if(hmSubjectListObj[mthYr].length){
		$("#heatmapFiltersDiv").append($("<div id='subjectslist' class='filterDivs'></div>"));
		$("#subjectslist").append($('<input id="subjectsChk" type="checkbox" class="selectallchk" name="subjectslist" checked="checked" value="subjectslist">Select All<br><br>'));	
		for(var i=0; i<hmSubjectListObj[mthYr].length; i++){
			//console.log("-->",'<input type="checkbox" name="gradelist" value="'+hmGradeListArr[i]+'">'+hmGradeListArr[i]);		
			$("#subjectslist").append($('<input type="checkbox" class="chkbox" name="subjectsChk" value="'+hmSubjectListObj[mthYr][i]+'">'));
			$("#subjectslist input:eq('"+(i+1)+"')").attr("checked","checked");
			$("#subjectslist input:eq('"+(i+1)+"')").after(hmSubjectListObj[mthYr][i]+'<br>');
		}
	}
	
	if(hmTypeListObj[mthYr].length){
		$("#heatmapFiltersDiv").append($("<div id='typelist' class='filterDivs'></div>"));	
		$("#typelist").append($('<input id="typesChk" type="checkbox" class="selectallchk" name="typelist" checked="checked" value="typelist">Select All<br><br>'));
		for(var i=0; i<hmTypeListObj[mthYr].length; i++){
			//console.log("-->",'<input type="checkbox" name="gradelist" value="'+hmGradeListObj[mthYr][i]+'">'+hmGradeListObj[mthYr][i]);		
			$("#typelist").append($('<input type="checkbox" class="chkbox" name="typesChk" value="'+hmTypeListObj[mthYr][i]+'">'));
			$("#typelist input:eq('"+(i+1)+"')").attr("checked","checked");
			$("#typelist input:eq('"+(i+1)+"')").after(hmTypeListObj[mthYr][i]+'<br>');
		}
	}
	$("#heatmapFiltersDiv").append($("<div id='filterSubButtons' class = 'filterBtn'></div>"))
	$("#filterSubButtons").append($("<input type='button' data-type='apply' class='subBtnClass fltSubButtons' value='Apply'>"));
	$("#filterSubButtons").append($("<input type='button' data-type='close' class='subBtnClass fltSubButtons' value='Close'>"));
	
	$(".fltSubButtons").click(function(){
		/*e.preventDefault();
		e.stopPropagation();*/				
		var val = $(this).attr("data-type");		
		if(val == "apply"){
			filterFlag = true;
			filterData();			
		} else if(val == "close"){
			$("#heatmapFiltersDiv").css("visibility","hidden");
		} else if(val == "closeSlider"){			
			$(".heatmapSliderDiv").css("visibility","hidden");
		}
	});
	
	$(".selectallchk").click(function(e){		
		markSelections(e.target);
	});
	$(".chkbox").click(function(e){
		//console.log("chkbox-",e.target.name);
		if(!e.target.checked){
			//console.log("E",$("#"+e.target.name),e.target.name,$("#"+e.target.name).attr("name"));
			$("#"+e.target.name).prop("checked", false);
		}
	});
}

function markSelections(val){	
	//console.log("markSelections",val)
	$("#"+val.value+" input").each(function(i){
		this.checked = val.checked;
	});

}

function filterData(flag){
	//console.log("Filter data - ",flag);
	if(filterFlag){
		//console.log("HI!")		
		$("#clearFilterBtn").css("position","relative");
		$("#clearFilterBtn").css("visibility","visible");
	}
	
	//
	gradesFilterArr = [];	
	subjectsFilterArr = [];	
	typeFilterArr = [];
	if(flag == undefined){	
		$("#gradeslist input").each(function(i){
			if(!this.checked){
				gradesFilterArr.push(this.value);
			}		
		});
		//
		$("#subjectslist input").each(function(i){
			if(!this.checked){
				subjectsFilterArr.push(this.value);
			}		
		});
		//
		$("#typelist input").each(function(i){
			if(!this.checked){
				typeFilterArr.push(this.value);
			}		
		});
	} else {
		//on clear filters - select all to be selected by default
		$(".selectallchk").prop("checked", true);
		//
		$("input.chkbox").each(function(i){
			this.checked = true;
		});
	}
	mthYr = monthId+""+yearId;
	//mthYr = currMonth+""+currYear;
	//console.log("month year id:",mthYr);
	if(rangeFlag){
		if(!heatmapRangeValues.length){
			heatmapRangeValues.push($("#monthOpt")[0].length-2,$("#monthOpt")[0].length-1);
		}
		$("#slider-range").slider("option","values",heatmapRangeValues);
	} else {
		getUniqueSubjectList();
		drawBubbleChart(monthId+""+yearId);		
	}
}

function getRange(mStart, mEnd){
	var startMonth = getMonthNum(mStart);
	var startYear = mStart.substr(-4,mStart.length);
	var endMonth = getMonthNum(mEnd);
	var endYear = mEnd.substr(-4,mEnd.length);
	//console.log("startMonth",startMonth,"- startYear",startYear);
	//console.log("endMonth",endMonth,"- endYear",endYear);
	var rangeArr = [];
	while((startMonth+""+startYear) != (endMonth+""+endYear)){		
		rangeArr.unshift(startMonth+""+startYear);
		startMonth++;
		if(startMonth > 12){
			startMonth=1;
			startYear++;
		}
	}
	rangeArr.unshift(endMonth+""+endYear);
	//console.log("rangeArr === ", rangeArr);
	return rangeArr;
}

function getMonthNum(p_str){
	var tempyr = p_str.substr(-4,p_str.length);		
	var tempmth;		
	if(p_str.length == 5){
		tempmth = Number(p_str.substr(0,1));
	} else if(p_str.length == 6){
		tempmth = Number(p_str.substr(0,2));
	}
	return tempmth;
}

function toTitleCase(str)
{
    return str.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
	

function getAssetType(val){
	switch(val){
		case "tp":
		return "Topic";
		break;
		case "asset_media":
		return "Media";
		break;
		case "asset_print":
		return "Printable";
		break;
		case "activity":
		return "Activity";
		break;
		case "question":
		return "Question";
		break;
		case "game":
		return "Game";
		break;
	}
}
