var edgeRoomData = [];
var classRoomData = [];

function getTerminalStatistics(){
	var mthYr = currMonth+""+currYear;
	getTerminalStatisticsData(mthYr);
	var terminalData = jsonPath(terminalStatsData[mthYr],'$.terminal_statistics_list');
	var ipList = jsonPath(terminalStatsData[mthYr],'$.terminal_statistics_list..ip');
	//console.log(">>>",ipList,terminalData);
	/*
	var hasUniqueNames = true;
	if(terminalData != undefined){
		for(var i=0; i<terminalData[0].terminal_statistics.length; i++){
			var location = terminalData[0].terminal_statistics[i].location;
			console.log("location:",location,terminalData[0].terminal_statistics,location);
			var check = _.find(terminalData[0].terminal_statistics,function(loc){				
				console.log("loc chk", loc.location, location);
				return loc.location == location; 
			});		
			if(check){
				hasUniqueNames = false;
				break;
			}		
		}		
	}
	console.log("HAS UNIQUE names:",hasUniqueNames);
	*/
	var uniqIps = _.uniq(ipList);
	// console.log("=Uniq=>",uniqIps);

	terminalUsageStatsData[mthYr]={};
	terminalUsageStatsData[mthYr].inClassRoom = {chartData:[],tableData:[]};
	terminalUsageStatsData[mthYr].inEdgeRoom = {chartData:[],tableData:[]};
	terminalUsageStatsData[mthYr].inClassRoomOverAllCapacity = [];
	terminalUsageStatsData[mthYr].inEdgeRoomOverAllCapacity = [];
	
	var croomTableData=[];
	var croomChartData=[];
	var eroomTableData=[];
	var eroomChartData=[];
	
	//console.log("capactity statistics:", mtotalServerOnTime, mnoOfWorkingDays, mweeksInMth);
	for(var i=0; i<uniqIps.length; i++){
		var tempData = jsonPath(terminalData,'$.[?(@.ip == "'+uniqIps[i]+'")]');
		//console.log("tempDATA - terminal = ",terminalData,tempData);
		var tempCnt = 0;
		for(var j=0; j<tempData.length; j++){
			tempCnt += Number(tempData[j].usageMinutes);
		}
		tempCnt = (tempCnt/60).toFixed(2);
		var timeSpent = getTimeSpent(tempCnt);
		var loc = "";
		if(tempData[0].location == undefined || tempData[0].location == ""){			
			loc = uniqIps[i];
		} else {			
			loc = tempData[0].location;
		}
		if(tempData[0].type == "edge"){
			terminalUsageStatsData[mthYr].inEdgeRoom.chartData.push({label:loc,ip:uniqIps[i],value:tempCnt,type:tempData[0].type});
			terminalUsageStatsData[mthYr].inEdgeRoom.tableData.push([loc,uniqIps[i],timeSpent]);
		} else {
			terminalUsageStatsData[mthYr].inClassRoom.chartData.push({label:loc,ip:uniqIps[i],value:tempCnt,type:tempData[0].type});
			terminalUsageStatsData[mthYr].inClassRoom.tableData.push([loc,uniqIps[i],timeSpent]);
		}
	}
	/*terminalUsageStatsData[mthYr].inEdgeRoom.chartData.push({label:"Total Capacity",value:totalSchoolCapacity});
	terminalUsageStatsData[mthYr].inClassRoom.chartData.push({label:"Total Capacity",value:totalSchoolCapacity});*/
	terminalUsageStatsData[mthYr].inClassRoomOverAllCapacity.push({label:"Capacity per room",value:totalSchoolCapacity});
	terminalUsageStatsData[mthYr].inEdgeRoomOverAllCapacity.push({label:"Capacity per room",value:totalSchoolCapacity});
	/*terminalUsageStatsData[mthYr].inClassRoom.push(croomChartData);
	terminalUsageStatsData[mthYr].inEdgeRoom.push(eroomChartData);*/
	//console.log("# # terminalUsageStatsData[mthYr] # #",terminalUsageStatsData,terminalUsageStatsData[mthYr]);
	//terminalUsageStatsData[mthYr].chartData.push(_.union(edgeRoomData,classRoomData));
	//drawTerminalStatisticsChart(mthYr);	
}

function getSelectedTerminalData(p_ip,name){
	terminalUsageIndividualData = [];
	var mthYr = monthId+""+yearId;	
	//console.log("terminalStatsData",terminalStatsData, mthYr, p_ip);
	var terminalData = jsonPath(terminalStatsData[mthYr],'$.terminal_statistics_list');
	var daysInMth = daysInMonth(monthId, yearId);
	for(var i=1; i<=daysInMth; i++){
		var dayStr = yearId+"-"+(monthId<10?'0'+monthId:monthId)+"-"+(i<10?'0'+i:i)+" 00:00:00";
		var tempTerminalData = jsonPath(terminalData,'$.[?(@.ip=="'+p_ip+'" && @.usageDay=="'+dayStr+'")]');
		var usageMins = 0;
		var timeSpent = 0;
		if(tempTerminalData){			
			usageMins = tempTerminalData[0].usageMinutes;					
			console.log("$$$ tempTerminalData $$$",tempTerminalData);		
			var tempCnt = (usageMins/60).toFixed(2);
			//timeSpent = getTimeSpent(tempCnt);
			timeSpent = Number(tempCnt);
		}
		terminalUsageIndividualData.push(timeSpent);
	}
	//console.log("TempTerminal data:",terminalUsageIndividualData);			
	drawTerminalStatsForMonth(name);
}

function drawTerminalStatisticsChart(p_mthYr){
	$('#classCapacityUtilization svg').empty();
	$('#edgeCapacityUtilization svg').empty();
	$('#classOverAllCapacityDiv svg').empty();
	$('#edgeOverAllCapacityDiv svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label+"~"+d.ip })
          .y(function(d) { return d.value })
          .valueFormat(d3.format(',.2f'))
          .forceY([0,terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity[0].value])
          .margin({top: 30, right: 0, bottom: 50, left: 110})
          .showValues(true)
          .showTime(true)
          .tooltips(true)          
          .showControls(false);				
				
		chart.xAxis.tickFormat(function(d){
			//console.log("xAxis d",d);
			return d.split("~")[0];
		});
		
		chart.yAxis
		      .tickFormat(d3.format('d'));
		
		  d3.select('#classCapacityUtilization svg')
		      .datum(getClassTerminalStatisticsData(p_mthYr))
		      .transition().duration(500)
		      .call(chart);
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 },function(){
          d3.selectAll(".nv-bar").on('click',
               function(d){
               		var name = d.label+"~"+d.ip;
                   getSelectedTerminalData(d.ip,name);
           });
      });
	 
	nv.addGraph(function() {
		var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .valueFormat(d3.format(',.2f'))
          .margin({top: 30, right: 0, bottom: 50, left: 110})
          .showValues(true)
          .showTime(true)
          .showLegend(false)
          .tooltips(true)          
          .showControls(false);				
				
		
		chart.yAxis
		      .tickFormat(d3.format('d'));
		
		  d3.select('#classOverAllCapacityDiv svg')
		      .datum(getClassTerminalCapacityData(p_mthYr))
		      .transition().duration(500)
		      .call(chart);
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
	 
	 //
	 nv.addGraph(function() {
		var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label+"~"+d.ip })
          .y(function(d) { return d.value })
          .valueFormat(d3.format(',.2f'))
          .forceY([0,terminalUsageStatsData[p_mthYr].inEdgeRoomOverAllCapacity[0].value])
          .margin({top: 30, right: 0, bottom: 50, left: 110})
          .showValues(true)
          .showTime(true)
          .tooltips(true)
          .showControls(false);				
				
		chart.xAxis.tickFormat(function(d){
			//console.log("xAxis d",d);
			return d.split("~")[0];
		});
		chart.yAxis
		      .tickFormat(d3.format('d'));
		
		  d3.select('#edgeCapacityUtilization svg')
		      .datum(getEdgeTerminalStatisticsData(p_mthYr))
		      .transition().duration(500)
		      .call(chart);
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
		},function(){
          d3.selectAll(".nv-bar").on('click',
               function(d){
               	var name = d.label+"~"+d.ip;
                   getSelectedTerminalData(d.ip,name);
           });
	 });
	 nv.addGraph(function() {
		var chart = nv.models.multiBarHorizontalChart()
          .x(function(d) { return d.label })
          .y(function(d) { return d.value })
          .valueFormat(d3.format(',.2f'))
          .margin({top: 30, right: 0, bottom: 50, left: 110})
          .showValues(true)
          .showTime(true)
          .showLegend(false)
          .tooltips(true)          
          .showControls(false);				
				
		
		chart.yAxis
		      .tickFormat(d3.format('d'));
		
		  d3.select('#edgeOverAllCapacityDiv svg')
		      .datum(getEdgeTerminalCapacityData(p_mthYr))
		      .transition().duration(500)
		      .call(chart);
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getClassTerminalStatisticsData(p_mthYr){
	var data = [];
	/*
	var valuesData = [];
	var tempData = terminalUsageStatsData[p_mthYr].chartData[0];
	console.log("tempData",tempData);
	for(var i=0; i<tempData.length; i++){
		valuesData.push({label:tempData[i].location,value:tempData[i].timeSpent});
	}
	
	console.log("**",valuesData);
	*/
	data.push({
		key: "Time spent(hrs)",
      	values: terminalUsageStatsData[p_mthYr].inClassRoom.chartData
	});
	return data;
}
function getClassTerminalCapacityData(p_mthYr){
		//console.log("terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity.value",terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity,terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity[0],terminalUsageStatsData[p_mthYr].inClassRoom.chartData);
	return [ 
		{ 
			key: "Per terminal capacity",
		    values: terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity,
		    color: "#2ca02c"
		}
	];
}
function getEdgeTerminalStatisticsData(p_mthYr){
	var data = [];
	/*
	var valuesData = [];
	var tempData = terminalUsageStatsData[p_mthYr].chartData[0];
	console.log("tempData",tempData);
	for(var i=0; i<tempData.length; i++){
		valuesData.push({label:tempData[i].location,value:tempData[i].timeSpent});
	}
	console.log("**",valuesData);
	*/
	data.push({
		key: "Time spent(hrs)",
      	values: terminalUsageStatsData[p_mthYr].inEdgeRoom.chartData
	});
	return data;
}
function getEdgeTerminalCapacityData(p_mthYr){
		//console.log("terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity.value",terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity,terminalUsageStatsData[p_mthYr].inClassRoomOverAllCapacity[0],terminalUsageStatsData[p_mthYr].inClassRoom.chartData);
	return [ 
		{ 
			key: "Per terminal capacity",
		    values: terminalUsageStatsData[p_mthYr].inEdgeRoomOverAllCapacity,
		    color: "#2ca02c"
		}
	];
}

//monthly stats chart
function drawTerminalStatsForMonth(name){
	$('#terminalMonthlyChart').empty();
	$('#terminalChartModal').css('visibility','visible');
	// define dimensions of graph
	var m = [20, 75, 40, 100];
	// margins
	var divWidth = $("#terminalMonthlyChart").width();
	var w = divWidth - m[1] - m[3];
	// width
	var h = 350 - m[0] - m[2];
	// height

	// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)
	var data = terminalUsageIndividualData;
	//console.log("data",data);
	// X scale will fit all values from data[] within pixels 0-w
	var x = d3.scale.linear().domain([0, data.length-1]).range([0, w]);
	// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
	console.log("totalServerOnTime:",totalServerOnTime);
	var maxY = _.max(terminalUsageIndividualData);
	var y1 = d3.scale.linear().domain([0, totalServerOnTime]).range([h, 0]);
	
	// create a line function that can convert data[] into x and y points
	var line1 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X1 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y1 value for data point: ' + d + ' to be at: ' + y1(d) + " using our y1Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y1(d);
	})
	// create a line function that can convert data[] into x and y points
	var line2 = d3.svg.line()
	// assign the X function to plot our line as we wish
	.x(function(d, i) {
		// verbose logging to show what's actually being done
		//console.log('Plotting X2 value for data point: ' + d + ' using index: ' + i + ' to be at: ' + x(i) + ' using our xScale.');
		// return the X coordinate where we want to plot this datapoint
		return x(i);
	}).y(function(d) {
		// verbose logging to show what's actually being done
		//console.log('Plotting Y2 value for data point: ' + d + ' to be at: ' + y2(d) + " using our y2Scale.");
		// return the Y coordinate where we want to plot this datapoint
		return y2(d);
	})
	// Add an SVG element with the desired dimensions and margin.
	var graph = d3.select("#terminalMonthlyChart").append("svg:svg").attr("width", w + m[1] + m[3]).attr("height", h + m[0] + m[2]).append("svg:g").attr("transform", "translate(" + m[3] + "," + m[0] + ")");
	//d3.select("#userMonthLoginChart svg").style("background-color", "rgba(255,0,0,0.3)");
	// create xAxis
	var xAxis = d3.svg.axis()
		.scale(x)
		.tickSize(-h)
		.tickFormat(function (d){return (d+1)})
		.ticks(terminalUsageIndividualData.length)
		.tickSubdivide(true);
	// Add the x-axis.
	graph.append("svg:g").attr("class", "x axis").attr("transform", "translate(0," + h + ")").call(xAxis);

	// create left yAxis
	var yAxisLeft = d3.svg.axis().scale(y1).orient("left").ticks(totalServerOnTime).tickSubdivide(false);
	// Add the y-axis to the left
	graph.append("svg:g").attr("class", "y axis axisLeft").attr("transform", "translate(-15,0)").call(yAxisLeft);

	
	// add lines
	// do this AFTER the axes above so that the line is above the tick-lines
	
	graph.append("svg:path").attr("d", line1(data)).attr("class", "data1");
	//graph.append("svg:path").attr("d", line2(data2)).attr("class", "data2");
		
	$("#terminalTitle").text(name);
	/*
	graph.append("svg:text").text(userName).attr({
		'x' : function(d) {
			//console.log("ddddd:",d)
			return -25;
		},
		'y' : function(d) {
			return -25;
		},
		'font-family' : 'Tahoma',
		'font-size' : '14px',
		'font-weight':'bold',
		'fill' : 'black',
		//'text-anchor':'middle'
	});	
	*/
	 graph.append("svg:text").text("Time spent (hrs)").attr({
		'x' : function(d) {
			//console.log("ddddd:",d);
			return -h/2;
		},
		'transform':'rotate(-90)',
		'y' : function(d) {
			return -45;
		},
		'font-family' : 'Tahoma',
		'font-size' : '10px',
		'fill' : 'black',
		'text-anchor':'middle'
	});
	
}
