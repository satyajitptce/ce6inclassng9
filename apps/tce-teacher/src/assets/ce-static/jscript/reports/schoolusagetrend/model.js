var monthId= (new Date().getMonth()+1);
var yearId= new Date().getFullYear();

var monthRange=12;
var monthYearArr = [];
var monthYearOptionsArr = [];
//complete xml for the month
var jsonObj;
//user level data
var userDataArr=[];
//feature utilization data
var utilizationDataArr=[];
//resource added data
var resourceAddedDataArr=[];

//
var currMonthJson;
var yearwiseData={};
// total school on time
var totalServerOnTime;
//This is for the over all usage chart
var overAllUsageTrendArr = {};

//overall user utilization array
var userUtilizationArray = {};
//monthly user utilization array
var userUtilizationMonthArr = {};

//overall feature utilization array
var featureUtilizationArray = {};

//overall contribution index array
var contributionIndexArray = {};

//monthly usage trend
var monthlyUsageTrend = {};
//Monthly login trend
//var monthlyLoginTrend = {};

//heat map data
var heatmapTableData={};
//heat map chart data
var heatmapChartData={};
//heat map range data
var heatmapRangeData = [];
//to store range values
var heatmapRangeValues = [];
//to store filters 
var hmGradeListArr = [];
var hmSubjectListArr = [];
var hmTypeListArr = [];
//to store filters for range data
var hmGradeListObj = {};
var hmSubjectListObj = {};
var hmTypeListObj = {};

//server statistics data
var serverStatsData = {};
//terminal statistics data
var terminalStatsData = {};

//terminal usage statistics data
var terminalUsageStatsData = {};
//terminal usage statistics individual data
var terminalUsageIndividualData = [];
//
var monthlyUsageArr = {};
//
var rangeFlag = false;
var filterFlag = false;
//for summary table
var summaryTableStatus = {
	1:"Poor",
	2:"Inconsistent",
	3:"Fair",
	4:"Good",
	5:"Excellent"	
};
var summaryTableTitles = {1:"Teachers using ClassEdge for the month.",
	2:"Effective usage of ClassEdge content.",
	3:"Capacity utilization of Tata ClassEdge set-up.",
	4:"<b>Overall Tata ClassEdge Usage.</b>"};
var summaryTableComments = {1:"This is a cause of concern. Please contact {#} to help the school improve the usage.",
	2:"The usage needs to be improved and made consistent. Please contact {#} to help the school improve the usage.",
	3:"Your school’s usage of Tata ClassEdge is satisfactory. However, the usage needs to be further improved. Please contact {#} to help the school improve the usage.",
	4:"Your school is utilising Tata ClassEdge reasonably well. However, there is still some scope for improvement. If you need support in improving the usage, please contact {#}.",
	5:"Your school is utilising Tata ClassEdge very effectively. Please continue the good work."};

//Help text
var helpText = {
	"reportsummary":"Report summary provides a snap-shot report of ClassEdge product utilization of the month. Viewing this summary report, the School can identify areas of improvement.",
	"activeuserindex":"Active User Index convey the percentage of eligible users actively using ClassEdge product during the month. (Eligible users are Teacher for whom, content is provided in ClassEdge). The report also shows the trend for the year.<br><br>It’s recommended to have the Active User Index more than 60%.",
	"activeassetusage":"Active Asset Usage gives the average number of assets used active users during the month. The report also shows the trend for a year.<br><br>It’s recommended, to have the Active Asset Usage more than 50.",
	"capacityutilization":"Capacity Utilization signifies the utilization of the installed equipment during the month.<br><br>It’s recommended, to have the Capacity Utilization more than 70%.",
	"overallclassedgeutilization":"",
	"monthlyusagetrend":"The Monthly Usage trend indicates the various ClassEdge resources viewed by the school during the month and compares the usage trend across the year. Using the Filters: Topic, Media, Printable, Activity, Game & Question; the school can view the resource usage trend in Edge rooms and compare it with Class room’s usage.<br><br>It’s recommended, the resources: Media, Printable, Activity, Games and Question usage in Class room should exceed the usage in Edge room.",
	"dailyusagetrend":"Daily Usage Trend denotes the usage pattern on a daily basis. In addition to the usage pattern, it also conveys the number of daily logins and Facility uptime (Tata ClassEdge server and Internet connectivity uptime).<br><br>It’s recommended, the School Server be connected to internet so that the School Server is up-to-date with all application and content updates.",
	"featureutilization":"The graph represents the various features of ClassEdge product used by Teachers during the month.<br><br>Analyzing the graph, the School can identify the features that are under-utilized.",
	"schoolcontributionindex":"The graph represents the number of contributions that were made by Teachers during the month.",
	"userwisetrends":"The analytical graph provides the detailed usage pattern of various Teachers using the ClassEdge applications in-terms to number of logins, number of resources viewed and the time spend (in hrs.) in a month.<br><br>It’s recommended, that each eligible Teacher spend atleast 2 hours in a month.",
	"classroomcapacityutilization":"The graph conveys the ClassEdge equipment/terminal locations, its utilization (in hrs.) for the month and compares it with the ideal capacity per terminal.",
	"edgeroomcapacityutilization":"The graph conveys the ClassEdge equipment/terminal locations, its utilization (in hrs.) for the month and compares it with the ideal capacity per terminal.",
	"contentusage":"The graph provides detailed information on the number of ClassEdge resources viewed for grades and subjects during the month."
};
//
var countOfOneTable = {
	0:0,
	1:1,
	2:0.6,
	3:0.31
}
