var activeUserIndexArr = [];
var activeAssetUsageArr = [];
var capacityUtilizationArr = {};
var overallClassEdgeUtilizationArr = [];
//
var activeUserIndexData = {};
var activeAssetUsageData = {};
var capacityUtilizationData = {};
var overallClassEdgeUtilizationData = {};
//
var mtotalServerOnTime = 0;
var mnoOfWorkingDays = 0;
var mweeksInMth = 0; 
var totalSchoolCapacity=0;
var totalEligibleUsersArr = [];

function getTerminalStatisticsData(p_mthYr){
	mtotalServerOnTime = totalServerOnTime;
	mnoOfWorkingDays = 0;
	mweeksInMth = 0;
	totalSchoolCapacity=0;
	//get the school hrs (server end time - server start time)
	//console.log("terminalStatsData",terminalStatsData,p_mthYr);
	if(terminalStatsData[p_mthYr]){
		var tempServerStartTime = jsonPath(terminalStatsData[p_mthYr],'$.server_stats_metadata.startTime');
		var tempServerEndTime = jsonPath(terminalStatsData[p_mthYr],'$.server_stats_metadata.endTime');
		if(tempServerStartTime){
			tempServerStartTime = tempServerStartTime[0].replace(":",".");			
		} else {
			tempServerStartTime = 0;
		}		
		if(tempServerEndTime){
			tempServerEndTime = tempServerEndTime[0].replace(":",".");					
		} else {
			tempServerEndTime = 0;
		}
		if(tempServerStartTime && tempServerEndTime){
			mtotalServerOnTime = Number((Number(tempServerEndTime)-Number(tempServerStartTime)).toFixed(2));
		}
		mtotalServerOnTime = (mtotalServerOnTime == 0) ? 6 : (mtotalServerOnTime < 0) ? mtotalServerOnTime*-1: mtotalServerOnTime;
		
		//get no of working days			
		var tempWorkingDays = jsonPath(terminalStatsData[p_mthYr],'$.server_stats_metadata.workingDays');
		if(tempWorkingDays){
			var tempWorkingArr = tempWorkingDays[0].split(",");
			mnoOfWorkingDays = _.without(tempWorkingArr,"").length;
		
		}
		mnoOfWorkingDays = (mnoOfWorkingDays == 0) ? 5 : mnoOfWorkingDays;
		//get the no of CT and ET
		var noOfTerminals = Number(schoolData[0].terminalsER) + Number(schoolData[0].terminalsCR);
		//			
		var daysInMth = daysInMonth(monthId, yearId);
		mweeksInMth = daysInMth/7;
		totalSchoolCapacity = mtotalServerOnTime * mnoOfWorkingDays * mweeksInMth;
	}
}
function calculateSummary(){
	capacityUtilizationArr.inClassRoom = [];
	capacityUtilizationArr.inEdgeRoom = [];
	//active user index calculation
	var len = counterVal;
	var reverseValue = counterVal-1;
	//Run through the entire year data for summary
	for(var i=0; i<len; i++){		
		//console.log("====== utilizationData =========:",monthYearArr[i],userUtilizationArray[monthYearArr[i]]);
		if(monthYearArr[i] != undefined){
			activeUserIndexData[monthYearArr[i]] = [];
			activeAssetUsageData[monthYearArr[i]] = [];
			capacityUtilizationData[monthYearArr[i]] = [];
			overallClassEdgeUtilizationData[monthYearArr[i]] = [];
		//}
		/*
		 * For active user index calculations
		 */
		//console.log("userUtilizationArray[monthYearArr[i]]",monthYearArr[i],userUtilizationArray[monthYearArr[i]]);
		//if(userUtilizationArray[monthYearArr[i]] != undefined){
			/*
			 * Get eligible users count
			 */
			/*var userSubjectsList = jsonPath(yearwiseData[monthYearArr[i]], "$.userSubjectsList..userSubjects");
			var totalEligibleUsers = 0;
			//console.log("value:",userSubjectsList);
			_.each(userSubjectsList[0], function(val){
				if(val.subjects != "" && val.subjects != undefined){
					if(val.subjects.toLowerCase() != "others"){
						totalEligibleUsers++;
					}					
				}
			});*/
			var totalEligibleUsers = Number(jsonPath(yearwiseData[monthYearArr[i]], "$.ceWideReport.userTypes..eligible")[0]);
			
			/*var ceWideReport = jsonPath(yearwiseData[monthYearArr[i]], "$.ceWideReport.activeUsage");
			console.log("ceWideReport", Number(ceWideReport[0]));*/
			/*var ceWideReport = jsonPath(yearwiseData[monthYearArr[i]], "$.ceWideReport.userTypes..active");
			console.log("ceWideReport", Number(ceWideReport[0]));*/
			//var activeUsersCnt = userUtilizationArray[monthYearArr[i]].utilizationData[0].activeUsrCnt;
			var activeUsersCnt = Number(jsonPath(yearwiseData[monthYearArr[i]], "$.ceWideReport.userTypes..active")[0]);
			//console.log("activeUsersCnt", activeUsersCnt);
			totalEligibleUsersArr[monthYearArr[i]] = totalEligibleUsers;
			//console.log("userUtilizationArray=",userUtilizationArray,totalEligibleUsers,activeUsersCnt);
			//var totalUsersCnt = userUtilizationArray[monthYearArr[i]].utilizationData[0].totalUsrCnt;
			var activeUserIndex = 0;
			if(totalEligibleUsers > 0){
				activeUserIndex = ((activeUsersCnt/totalEligibleUsers)*100);
			}
			//console.log("totalEligibleUsers:",activeUsersCnt,totalEligibleUsers,activeUserIndex);
		/*} else {
			activeUserIndex = 0;
		}*/
		/*
		 * For active asset usage calculations
		 */
		var totalAssetCnt = 0;
		//var activeUsersCnt = 0;
		//console.log("monthlyUsageArr[monthYearArr[i]]",userUtilizationArray,monthlyUsageArr,monthlyUsageArr[monthYearArr[i]],monthYearArr[i]);
		//if(userUtilizationArray[monthYearArr[i]] != undefined){
			//activeUsersCnt = userUtilizationArray[monthYearArr[i]].utilizationData[0].activeUsrCnt;
			/*_.each(monthlyUsageArr[monthYearArr[i]],function(num){
				totalAssetCnt += num;
			});*/
			//totalAssetCnt = monthlyUsageArr[monthYearArr[i]];
			//totalAssetCnt = userUtilizationArray[monthYearArr[i]].utilizationData[0].activeAssetUserCount;
			totalAssetCnt = Number(jsonPath(yearwiseData[monthYearArr[i]], "$.ceWideReport.activeUsage")[0]);
			//console.log("totalAssetCnt", totalAssetCnt);
		//}
		//console.log("***********",userUtilizationArray,monthYearArr[i],totalAssetCnt, activeUsersCnt);
		//console.log("Active USAGE = ", activeUsersCnt, totalAssetCnt);
		} else {
			activeUserIndex = 0;
		}
		/*
		 * Total usage statistics data
		 */
		var activeAssetUsage = 0;
		if(totalAssetCnt > 0){	
			activeAssetUsage = (totalAssetCnt / activeUsersCnt).toFixed(2);
		}
		//console.log("## = ",activeAssetUsage);
		/*
		 * For capacity utilization data
		 */
		var tempERTerminalData = jsonPath(terminalStatsData[monthYearArr[i]],'$.terminal_statistics_list.[?(@.type == "edge")].usageMinutes');
		var tempCRTerminalData = jsonPath(terminalStatsData[monthYearArr[i]],'$.terminal_statistics_list.[?(@.type == "class")].usageMinutes');
		//console.log("~",tempERTerminalData,tempCRTerminalData);
		var totalERTerminalTimeUsage = 0;
		var totalCRTerminalTimeUsage = 0;
		_.each(tempERTerminalData, function(num){
			//console.log("Each...",num);			
			totalERTerminalTimeUsage += Number(num);
		});
		_.each(tempCRTerminalData, function(num){
			//console.log("Each...",num);			
			totalCRTerminalTimeUsage += Number(num);
		});
		//console.log("Total Terminal time usage:",totalERTerminalTimeUsage, totalCRTerminalTimeUsage);
		var classRoomUtilization = 0;
		var edgeRoomUtilization = 0;
		
		//Total installed capacity - server details (if not available use 6 hrs as default)
		var totalInstalledCapacity=0;
		//console.log("=jsonPath=",jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.startTime'));
		//if(jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.startTime') == false || jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.startTime') == undefined){
			//totalInstalledCapacity = 0;
		//} else {
			//get the school hrs (server end time - server start time)
			var tServStartTime = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.startTime');
			var tServEndTime = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.endTime');
			var tempServerStartTime = 0;
			//console.log("### tServStartTime ###",tServStartTime);
			if(tServStartTime != "" && tServStartTime){
				tempServerStartTime = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.startTime')[0].replace(":",".");			
			}
			var tempServerEndTime = 0;
			if(tServEndTime != "" && tServEndTime){
				tempServerEndTime = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.endTime')[0].replace(":",".");				
			}
			//console.log("SERVER START TIME:",tempServerStartTime,tempServerEndTime,(Number(tempServerEndTime)-Number(tempServerStartTime)));
			
			if(Number(tempServerEndTime)-Number(tempServerStartTime) > 0){
				totalServerOnTime = Number((Number(tempServerEndTime)-Number(tempServerStartTime)).toFixed(2));
			} else {
				totalServerOnTime = 6;
			}
			// 31 Oct '17: defining minimum minutesPerDay
			if (totalServerOnTime < 4) {totalServerOnTime = 6;} 
			
			//get no of working days			
			//console.log("Temp working days:",jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.workingDays'));
			var tWorkingDays = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.workingDays');
			var tempWorkingDays = "";
			if(tWorkingDays != "" && tWorkingDays){
				var tempWorkingDays = jsonPath(terminalStatsData[monthYearArr[i]],'$.server_stats_metadata.workingDays')[0];			
			}
			var tempWorkingArr = [];
			if(tempWorkingDays != ""){
				tempWorkingArr = tempWorkingDays.split(",");
			}
			var noOfWorkingDays = _.without(tempWorkingArr,"").length;
			if(noOfWorkingDays < 1){
				noOfWorkingDays = 5;
			}
			//get the no of CT and ET
			var noOfTerminals = Number(schoolData[0].terminalsER) + Number(schoolData[0].terminalsCR);
			//			
			var daysInMth = daysInMonth(monthId, yearId);
			var weeksInMth = daysInMth/7;
			
			// 31 Oct '17: TCE active time constant of 40%
			// 31 Oct '17: holiday's added - average 3 holidays per month
			var holidays = 3;
			var daysPerMonth = (noOfWorkingDays * (30 / 7)) - holidays;
			//
			//console.log("totalTerminalTimeUsage",totalTerminalTimeUsage);
			//console.log("totalServerOnTime",i,(totalERTerminalTimeUsage/60),totalServerOnTime,noOfWorkingDays,weeksInMth,Number(schoolData[0].terminalsER),Number(schoolData[0].terminalsER));
			console.log("totalERTerminalTimeUsage",totalERTerminalTimeUsage)
			console.log("totalCRTerminalTimeUsage",totalCRTerminalTimeUsage)
			if(totalERTerminalTimeUsage == 0 && totalCRTerminalTimeUsage == 0){
				classRoomUtilization = 0;
				edgeRoomUtilization = 0;
			} else {			
				//console.log("***** (totalCRTerminalTimeUsage/60):",(totalCRTerminalTimeUsage/60),(totalERTerminalTimeUsage/60));
				//console.log("*** ", totalServerOnTime, " no of working days: ", noOfWorkingDays, " weeks in mth: ", weeksInMth, " total CR terminals: ",schoolData[0].terminalsCR, " ER terminals: ",schoolData[0].terminalsER);
				/*
				classRoomUtilization = (((totalCRTerminalTimeUsage/60)/(totalServerOnTime * noOfWorkingDays * weeksInMth * Number(schoolData[0].terminalsCR)))*100).toFixed(2);			
				edgeRoomUtilization = (((totalERTerminalTimeUsage/60)/(totalServerOnTime * noOfWorkingDays * weeksInMth * Number(schoolData[0].terminalsER)))*100).toFixed(2);
				*/
				classRoomUtilization = (totalCRTerminalTimeUsage/60);
				edgeRoomUtilization = (totalERTerminalTimeUsage/60);
			}
			// 31 Oct '17 (optimization): Remove Edgeroom from the calculation	for ceTerminal
			// 31 Oct '17 (optimization): TCE active time constant of 40%	
			//var den = (totalServerOnTime * noOfWorkingDays * weeksInMth * (Number(schoolData[0].terminalsCR)+Number(schoolData[0].terminalsER)));// original
			var den = (totalServerOnTime * daysPerMonth * Number(schoolData[0].terminalsCR) * 0.4);
			var tempVar = ((classRoomUtilization+edgeRoomUtilization)/den)*100;
			//console.log("^^^^ :",den,"<><><>",tempVar,"::::::",(totalServerOnTime * noOfWorkingDays * weeksInMth * Number(schoolData[0].terminalsCR)));
			totalInstalledCapacity = tempVar;//Number(classRoomUtilization)+Number(edgeRoomUtilization);
			//convert it to minutes
			/*var serverOnTimeData = totalServerOnTime.split(".");
			totalInstalledCapacity = Number(serverOnTimeData[0]*60) + Number(serverOnTimeData[1]);
			//console.log("totalServerOnTime",totalInstalledCapacity);*/
		//}
		/*
		 * Over all classedge utilization data
		 */
		//console.log("activeUserIndex:",activeUserIndex,":",activeAssetUsage,"::",totalInstalledCapacity);
		var activeUserPoints = getIndexPoint(activeUserIndex);
		var activeAssetPoints = getAssetPoint(activeAssetUsage);
		//console.log("TOT capacity",totalInstalledCapacity);
		var totalCapacityPoints = getCapacityUtilizationPoint(totalInstalledCapacity);
		
		// 31 Oct 17: added the constant that was missing - already exist in All India dashboard
		//var overallUtilization = getIndexPoint(((5*(Number(activeUserPoints)) + 3*(Number(activeAssetPoints)) + 2*(Number(totalCapacityPoints)))/40)*100);
		var overallUtilization = (0.4*(Number(activeUserPoints)) + 0.3*(Number(activeAssetPoints)) + 0.3*(Number(totalCapacityPoints))).toFixed(1);
		var countOfOne = 0;
		var tempArr = [activeUserPoints,activeAssetPoints,totalCapacityPoints];
		for(var n=0; n<tempArr.length; n++){
			if(tempArr[n] == 1){
				countOfOne++;
			}
		}
		//console.log("Count of 1:",countOfOne.one," overall",overallUtilization);
		//console.log(">>>>>>",overallUtilization, countOfOneTable,"  countOfOne:: ",countOfOne);
		var totalCEUtilization = getOverallCEUtilPoint((overallUtilization-(countOfOneTable[countOfOne]*countOfOne)).toFixed(1));
		//console.log("overallUtilization:",activeUserPoints, activeAssetPoints,totalCapacityPoints,overallUtilization,totalCEUtilization);
		//console.log("activeUserPoints:",activeUserPoints," activeAssetPoints:",activeAssetPoints," totalCapacityPoints:",totalCapacityPoints);
		//console.log("totalCEUtilization:",totalCEUtilization);
		//console.log("overall utilization data:",overallUtilization);
		//
		activeUserIndexArr.push([reverseValue,Number(activeUserIndex)]);
		activeAssetUsageArr.push([reverseValue,Number(activeAssetUsage)]);
		capacityUtilizationArr.inClassRoom.push([reverseValue,Number(classRoomUtilization)]);
		capacityUtilizationArr.inEdgeRoom.push([reverseValue,Number(edgeRoomUtilization)]);
		//
		//console.log("monthYearArr[i]",monthYearArr[i]);
		if(monthYearArr[i] != undefined){
			activeUserIndexData[monthYearArr[i]].push(activeUserPoints);
			activeAssetUsageData[monthYearArr[i]].push(activeAssetPoints);
			capacityUtilizationData[monthYearArr[i]].push(totalCapacityPoints);
			overallClassEdgeUtilizationData[monthYearArr[i]].push(totalCEUtilization);
		}
		//capacityUtilizationArr.push([reverseValue,Number(totalInstalledCapacity)]);
		overallClassEdgeUtilizationArr.push([reverseValue,Number(totalCEUtilization)]);
		reverseValue--;
		//summarychart
	}
	//console.log("activeAssetUsageArr array:",activeAssetUsageArr);
	//console.log("terminalUsageStatsData[mthYr].chartData",terminalUsageStatsData);
	//console.log("capacityUtilizationArr",capacityUtilizationArr);
	//console.log("overallClassEdgeUtilizationArr:",overallClassEdgeUtilizationArr);
	//console.log("ALLLLLLLLLLLLLL",monthYearArr,activeUserIndexData,activeAssetUsageData,capacityUtilizationData,overallClassEdgeUtilizationData);
	//activeUserIndexArr.reverse();
	//updateSummaryTable();
	drawActiveUserIndexChart();
	drawActiveAssetUsageChart()
	drawCapacityUtilizationChart();
	drawOverallClassedgeUsageChart();
}

function updateSummaryTable(p_mthYr){
	//console.log("UPDATE SUMMARY TABLE");
	/* Clear old content */
	$("#summaryTable>tbody tr:not(:first)").remove();
	//get server data here
	var tempyr = Number(p_mthYr.substr(-4,p_mthYr.length));		
	var tempmth = "";
	if(p_mthYr.length == 5){
		tempmth = Number(p_mthYr.substr(0,1))-1;
	} else if(p_mthYr.length == 6){
		tempmth = Number(p_mthYr.substr(0,2))-1;
	}
	
	var activeUsrIdxCnt=0;		
	var activeUsrIdxAvg=0;
	
	var activeAssetUsageCnt=0;
	var activeAssetUsageAvg=0;
	
	var capacityUtilizationCnt=0;
	var capacityUtilizationAvg=0;
	
	var overallClassEdgeUtilizationCnt=0;
	var overallClassEdgeUtilizationAvg=0;

	var avgCnt = 0;
	//get comparision value
	for(var i=1; i<=3; i++){
		//console.log("comparision:",tempmth+""+tempyr,activeUserIndexData[tempmth+""+tempyr]);
		if(isSummaryAvailable[tempmth+""+tempyr]){
			avgCnt++;
			if(activeUserIndexData[tempmth+""+tempyr]){
				activeUsrIdxCnt++;
				activeUsrIdxAvg += Number(activeUserIndexData[tempmth+""+tempyr]);
			}
			if(activeAssetUsageData[tempmth+""+tempyr]){
				activeAssetUsageCnt++;
				activeAssetUsageAvg += Number(activeAssetUsageData[tempmth+""+tempyr]);
			}
			if(capacityUtilizationData[tempmth+""+tempyr]){
				capacityUtilizationCnt++;
				capacityUtilizationAvg += Number(capacityUtilizationData[tempmth+""+tempyr]);
			}
			if(overallClassEdgeUtilizationData[tempmth+""+tempyr]){
				overallClassEdgeUtilizationCnt++;
				overallClassEdgeUtilizationAvg += Number(overallClassEdgeUtilizationData[tempmth+""+tempyr]);
			}
		}
		
		if(tempmth > 1){
			tempmth--;			
		} else {
			tempmth = 12;
			tempyr--;
		}
	}
	//
	/*var str = $("#summaryMonthTxt").text().replace("{#}", activeUsrIdxCnt);
	console.log('$("#summaryTable monthId").text()',$("#summaryMonthTxt").text());*/
	if(activeUsrIdxCnt == 0){
		avgCnt="-";
	}
	$("#summaryMonthTxt").text("Compared to last "+avgCnt+" months");
	//console.log("average:",activeUsrIdxAvg,activeUsrIdxCnt,(activeUsrIdxAvg/activeUsrIdxCnt));
	var compVal = "";
	var prevVal = activeUsrIdxAvg/activeUsrIdxCnt;
	var imgPath = "";
	if(prevVal > 0){
		if(prevVal < activeUserIndexData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_up.png";
			compVal = "Increased";
		} else if(prevVal > activeUserIndexData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_down.png";
			compVal = "Decreased";
		} else if(prevVal == activeUserIndexData[p_mthYr]){
			compVal = "Same";
			imgPath = "/ce-static/css/ui-reports/images/arrow_side.png";
		}
	} else {
		compVal = "-";
	}
	var summaryTxt = "";
	//console.log("summaryTableComments:",summaryTableComments,activeUserIndexData,p_mthYr,summaryTableStatus);
	summaryTxt = summaryTableComments[activeUserIndexData[p_mthYr]].replace("{#}",$("#academicManager").text());
	var tdata = "<tr><td align='center'>1.</td><td>"+summaryTableTitles[1]+"</td><td align='center'>"+activeUserIndexData[p_mthYr]+"</td><td class=''><div></div><span class='summaryStatusSpan'>"+summaryTableStatus[activeUserIndexData[p_mthYr]]+"</span></td><td><img src='"+imgPath+"'></img>"+compVal+"</td><td>"+summaryTxt+"</td></tr>";
	$("#summaryTable>tbody").append(tdata);
	//console.log(">>",summaryTableStatus[activeUserIndexData[p_mthYr]].toLowerCase());
	$("#summaryTable tbody tr:last-child").toggleClass(summaryTableStatus[activeUserIndexData[p_mthYr]].toLowerCase());
	//
	var prevVal = activeAssetUsageAvg/activeAssetUsageCnt;
	if(prevVal > 0){
		if(prevVal < activeAssetUsageData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_up.png";
			compVal = "Increased";
		} else if(prevVal > activeAssetUsageData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_down.png";
			compVal = "Decreased";
		} else if(prevVal == activeAssetUsageData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_side.png";
			compVal = "Same";
		}	
	} else {
		compVal = "-";
	}
	summaryTxt = summaryTableComments[activeAssetUsageData[p_mthYr]].replace("{#}",$("#academicManager").text());
	var tdata = "<tr><td align='center'>2.</td><td>"+summaryTableTitles[2]+"</td><td align='center'>"+activeAssetUsageData[p_mthYr]+"</td><td class=''><div></div><span class='summaryStatusSpan'>"+summaryTableStatus[activeAssetUsageData[p_mthYr]]+"</span></td><td><img src='"+imgPath+"'></img>"+compVal+"</td><td>"+summaryTxt+"</td></tr>";
	$("#summaryTable>tbody").append(tdata);
	//console.log(">>",summaryTableStatus[activeAssetUsageData[p_mthYr]].toLowerCase());	
	$("#summaryTable tbody tr:last-child").toggleClass(summaryTableStatus[activeAssetUsageData[p_mthYr]].toLowerCase());	
	//
	var prevVal = capacityUtilizationAvg/capacityUtilizationCnt;
	if(prevVal > 0){
		if(prevVal < capacityUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_up.png";
			compVal = "Increased";
		} else if(prevVal > capacityUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_down.png";
			compVal = "Decreased";
		} else if(prevVal == capacityUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_side.png";
			compVal = "Same";
		}
	} else {
		compVal = "-";
	}
	summaryTxt = summaryTableComments[capacityUtilizationData[p_mthYr]].replace("{#}",$("#academicManager").text());
	var tdata = "<tr><td align='center'>3.</td><td>"+summaryTableTitles[3]+"</td><td align='center'>"+capacityUtilizationData[p_mthYr]+"</td><td class=''><div></div><span class='summaryStatusSpan'>"+summaryTableStatus[capacityUtilizationData[p_mthYr]]+"</span></td><td><img src='"+imgPath+"'></img>"+compVal+"</td><td>"+summaryTxt+"</td></tr>";
	$("#summaryTable>tbody").append(tdata);
	//console.log(">>",summaryTableStatus[capacityUtilizationData[p_mthYr]].toLowerCase());
	$("#summaryTable tbody tr:last-child").toggleClass(summaryTableStatus[capacityUtilizationData[p_mthYr]].toLowerCase());
	//
	var prevVal = overallClassEdgeUtilizationAvg/overallClassEdgeUtilizationCnt;
	if(prevVal > 0){
		if(prevVal < overallClassEdgeUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_up.png";
			compVal = "Increased";
		} else if(prevVal > overallClassEdgeUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_down.png";
			compVal = "Decreased";
		} else if(prevVal == overallClassEdgeUtilizationData[p_mthYr]){
			imgPath = "/ce-static/css/ui-reports/images/arrow_side.png";
			compVal = "Same";
		}
	} else {
		compVal = "-";
	}
	//console.log("overallClassEdgeUtilizationData",overallClassEdgeUtilizationData)
	summaryTxt = summaryTableComments[overallClassEdgeUtilizationData[p_mthYr]].replace("{#}",$("#academicManager").text()); //TODO: Fix the summary text
	var tdata = "<tr><td align='center'>4.</td><td>"+summaryTableTitles[4]+"</td><td align='center'>"+overallClassEdgeUtilizationData[p_mthYr]+"</td><td class=''><div></div><span class='summaryStatusSpan'>"+summaryTableStatus[overallClassEdgeUtilizationData[p_mthYr]]+"</span></td><td><img src='"+imgPath+"'></img>"+compVal+"</td><td>"+summaryTxt+"</td></tr>";
	$("#summaryTable>tbody").append(tdata);
	//console.log(">>",summaryTableStatus[overallClassEdgeUtilizationData[p_mthYr]].toLowerCase());
	$("#summaryTable tbody tr:last-child").toggleClass(summaryTableStatus[overallClassEdgeUtilizationData[p_mthYr]].toLowerCase()); //TODO: Fix this
	//$("#academicManager").text
	
}

function drawActiveUserIndexChart(){
	$('#activeUserIndexChart svg').remove();
	$('#activeUserIndexChart').append('<svg></svg>');
	//$('#chart svg').empty();
	//console.log("draw complete year graph - yearwiseData",yearwiseData);	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(false)
	                .forceY([0,100]) ;
	      chart.margin({left: 80,right:40});          
		
		chart.xAxis
	  		.tickFormat(function(d){
	  			//console.log("d:",d);
	  			return chartAxisData[d];
	  		})
		
		  chart.yAxis
		      .axisLabel('Active User %')
		      .showMaxMin(true)
		      .tickFormat(function(d){
		      		return parseInt(d)+"%";	
		      });		      		     
		     // .tickFormat(d3.format('.0f'));
		
		
		  
		  d3.select('#activeUserIndexChart svg')
		      .datum(getActiveUserIndexData())
		    .transition().duration(500)
		      .call(chart);
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getActiveUserIndexData(){
	//console.log("media data:", overAllUsageTrendArr)
	
	return [
		{
			"key":"Active User Index",
			"values": activeUserIndexArr,
			"color": '#8c564b'
		}
		/*
		{
			"key":"Active Asset Usage",
			"values": activeAssetUsageArr
		}
		*/
	]; 
}

/*
 * Active asset usage chart
 */
function drawActiveAssetUsageChart(){
	$('#activeAssetUsageChart svg').remove();
	$('#activeAssetUsageChart').append('<svg></svg>');
	//$('#chart svg').empty();
	//console.log("draw complete year graph - yearwiseData",yearwiseData);	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(false)
	                //.forceY([0,100]) ;
	      chart.margin({left: 80,right:40});          
		
		chart.xAxis
	  		.tickFormat(function(d){
	  			//console.log("d:",d);
	  			return chartAxisData[d];
	  		})
		
		  chart.yAxis
		      .axisLabel('Active Asset Usage')
		      .showMaxMin(true);
		      				
		  
		  d3.select('#activeAssetUsageChart svg')
		      .datum(getActiveAssetUsageData())
		    .transition().duration(500)
		      .call(chart);
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getActiveAssetUsageData(){
	//console.log("media data:", overAllUsageTrendArr)
	
	return [		
		{
			"key":"Active Asset Usage",
			"values": activeAssetUsageArr,
			"color": '#a55194'
		}
		
	]; 
}

// Capacity utilization chart
function drawCapacityUtilizationChart(){
	$('#capacityUtilizationChart svg').remove();
	$('#capacityUtilizationChart').append('<svg></svg>');
	//$('#chart svg').empty();
	//console.log("draw complete year graph - yearwiseData",yearwiseData);	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(true)
	                .forceY([0,100]) ;
	      chart.margin({left: 80,right:40});          
		
		chart.xAxis
	  		.tickFormat(function(d){
	  			//console.log("d:",d);
	  			return chartAxisData[d];
	  		})
		
		  chart.yAxis
		      .axisLabel('Capacity Utilization')
		      .tickFormat(function(d){	  				
	  				return d+"%";
	  			})
		      .showMaxMin(true);
		      				
		  
		  d3.select('#capacityUtilizationChart svg')
		      .datum(getCapacityUtilizationData())
		    .transition().duration(500)
		      .call(chart);
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getCapacityUtilizationData(){
	//console.log("media data:", overAllUsageTrendArr)
	
	return [		
		{
			"key":"Classroom Capacity Utilization",
			"values": capacityUtilizationArr.inClassRoom,
			"color": '#FF9147'
		},
		{
			"key":"Edgeroom Capacity Utilization",
			"values": capacityUtilizationArr.inEdgeRoom,
			"color": '#2ca02c'
		}
		
	]; 
}

/*
 * overall classedge usage chart
 */
function drawOverallClassedgeUsageChart(){
	/*$('#overallUtilizationChart svg').remove();
	$('#overallUtilizationChart').append('<svg></svg>');*/
	//$('#chart svg').empty();
	//console.log("draw complete year graph - yearwiseData",yearwiseData);	
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(false)
	                .forceY([1,5]) ;
	      chart.margin({left: 100,right:40});          
		
		chart.xAxis
	  		.tickFormat(function(d){	  			
	  			return chartAxisData[d];
	  		})
		
		  chart.yAxis
		  		.tickFormat(function(d){
		  			var str = "";
		  			switch(d){
		  				case 1: 
		  				str = "Poor";
		  				break;
		  				case 2:
		  				str = "Inconsistent";
		  				break;
		  				case 3: 
		  				str = "Fair";
		  				break;
		  				case 4: 
		  				str = "Good";
		  				break;
		  				case 5: 
		  				str = "Excellent";
		  				break;
		  			}
		  			return str+"("+d+")";
		  		})
		      //.axisLabel('Overall ClassEdge Utilization')
		      .showMaxMin(true);
		      				
		  
		  d3.select('#overallUtilizationChart svg')
		      .datum(getOverallUtilizationData())
		    .transition().duration(500)
		      .call(chart);
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getOverallUtilizationData(){
	//console.log("media data:", overAllUsageTrendArr)
	
	return [		
		{
			"key":"Overall Usage Index",
			"values": overallClassEdgeUtilizationArr,
			"color": '#3182bd'
		}
		
	]; 
}


//for get points
function getIndexPoint(num) {
	//var num =  Math.round((num1/num2)*10000)/100;
	//console.log("num::",num);
	var index;
	
	if(num >= 75) {
		index = 5;	
	} else if(num < 75 && num >= 60) {
		index = 4;
	} else if(num < 60 && num >= 50) {
		index = 3;
	} else if(num < 50 && num >= 40) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}
//for get points
function getCapacityUtilizationPoint(num) {
	//var num =  Math.round((num1/num2)*10000)/100;
	//console.log("getCapacityUtilizationPoint::",num);
	var index;
	/*
	if(num >= 75) {
		index = 5;	
	} else if(num < 75 && num >= 60) {
		index = 4;
	} else if(num < 60 && num >= 50) {
		index = 3;
	} else if(num < 50 && num >= 40) {
		index = 2;
	} else {
		index = 1;
	}
	*/
	if(num >= 21) {
		index = 5;	
	} else if(num < 21 && num >= 12) {
		index = 4;
	} else if(num < 12 && num >= 8) {
		index = 3;
	} else if(num < 8 && num >= 5) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}
function getAssetPoint(num) {
	//var num =  Math.round((num1/num2)*10000)/100;
	var index;
	if(num >= 75) {
		index = 5;
	} else if(num < 75 && num >= 50) {
		index = 4;
	} else if(num < 50 && num >= 30) {
		index = 3;
	} else if(num < 30 && num >= 22) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}
//for overall ce util points
function getOverallCEUtilPoint(num){
	var index;
	if(num >= 4) {
		index = 5;
	} else if(num < 4 && num >= 3) {
		index = 4;
	} else if(num < 3 && num >= 2) {
		index = 3;
	} else if(num < 2 && num >= 1) {
		index = 2;
	} else {
		index = 1;
	}
	return index;
}
