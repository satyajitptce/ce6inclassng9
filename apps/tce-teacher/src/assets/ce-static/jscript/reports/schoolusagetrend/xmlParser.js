
var flag = false;

//load xml
function loadXML(month,year, pflag){
	/*if(flag){
		$fn = onXMLLoad;
	} else {
		$fn = onPrevXMLLoad;
	}*/
	flag = pflag;
	$.ajax({
		type:"POST",
		url:"http://mvms121.tatainteractive.com:8080/group/classedge/flex-service?p_p_id=ReportManagement_WAR_ceportallessonplan&p_p_lifecycle=0&p_p_state=exclusive&_ReportManagement_WAR_ceportallessonplan_Operation=sheetReport&monthId="+month+"&yearId="+year,
		dataType:"xml",
		success: onXMLLoad, 
		error: onIOError			
	});
	//$('#container').dataTable();
}

function onXMLLoad(xml){	
	//console.log("xml:", xml);
	jsonObj = $.xml2json(xml);
	parseUserData(flag);
}

function onIOError(jqXHR,txt,err){
	alert("Error loading XML! "+err);
}


// for user utilization trend
var cnt=0;
var tpCnt=0;
var mediaCnt = 0;
var printCnt = 0;
var activityCnt = 0;
var questionCnt = 0;
var gameCnt = 0;		
var totalCnt = 0;
// for feature utilization data
var presentationCnt=0;
var whiteboardCnt=0;
var annotationCnt=0;
var dictionaryCnt=0;
//for contribution data
var lpCnt_added=0;
var resourceCnt_added=0;
var questionCnt_added=0;
var gameCnt_added=0;
var noteCnt_added=0;	
var addedTotalCnt=0;	

function parseUserData(){
	//
	var pflag = false;	
	//console.log("currMonthJson",currMonthJson);
	var userCnt = jsonPath(currMonthJson, '$.users.user')[0].length;
	var users = jsonPath(currMonthJson, '$.users.user');	
	for(var i=0; i<userCnt; i++){
		//if(!pflag){
			cnt=0;
			tpCnt=0;
			mediaCnt = 0;
			printCnt = 0;
			activityCnt = 0;
			questionCnt = 0;
			gameCnt = 0;		
			totalCnt = 0;
			//
			presentationCnt=0;
			whiteboardCnt=0;
			annotationCnt=0;
			dictionaryCnt=0;
			//
			lpCnt_added=0;
			resourceCnt_added=0;			
			questionCnt_added=0;
			gameCnt_added=0;
			noteCnt_added=0;		
			addedTotalCnt=0;
		/*}else{
			cnt = 0;
			totalCnt = 0;
			// for feature utilization data
			presentationCnt=0;
			whiteboardCnt=0;
			annotationCnt=0;
			dictionaryCnt=0;
			//
			lpCnt_added=0;
			resourceCnt_added=0;			
			questionCnt_added=0;
			gameCnt_added=0;
			noteCnt_added=0;
			addedTotalCnt=0;
		}*/
		//var usrDataArr=[];
		//get the details for the user		
		//console.log("user",users[0][i].userId);
	//	var userData = jsonPath(jsonObj, '$.viewed.usage[?(@.userId=="'+users[0][i].userId+'" && @.accessMode == "0")]');
		var userData = jsonPath(currMonthJson, '$.viewed.usage[?(@.userId=="'+users[0][i].userId+'")]');
		var addedResourceData = jsonPath(currMonthJson, '$.added.usage[?(@.userId=="'+users[0][i].userId+'")]');
		//console.log("userdata:", addedResourceData);
		//console.log("COUNT:", cnt, ":", totalCnt);
		//add user name
		if(!pflag){
			var userName = users[0][i].firstName + " " + users[0][i].lastName;
		}
		
		//get resource count
		for(var j=0; j<userData.length; j++){						
			var dataCnt = Number(userData[j].count);			
			//console.log("cnt:", cnt);
			//cnt = cnt+dataCnt;
			totalCnt += dataCnt;
			//console.log(cnt + " :: data cnt:" + dataCnt + " :: " + totalCnt);
			//console.log("cnt ====", dataCnt+" >> "+userData[j].type+" >> " + totalCnt+" :: "+cnt);
			if(!pflag){
				switch(userData[j].type){
					case 'tp':					
					tpCnt += dataCnt;
					break;
					case 'asset_media':					
					mediaCnt += dataCnt;
					break;
					case 'asset_print':
					printCnt += dataCnt;
					break;
					case 'activity':
					activityCnt += dataCnt;
					break;
					case 'question':
					questionCnt += dataCnt;
					break;
					case 'game':
					gameCnt += dataCnt;
					break;
					case 'presentation':
					presentationCnt += dataCnt;
					break;
					case 'whiteboard':
					whiteboardCnt += dataCnt;
					break;
					case 'asset_annotation':
					annotationCnt += dataCnt;				
					break;
					case 'dictionary':
					dictionaryCnt += dataCnt;
					break;					
				}
			} else {
				switch(userData[j].type){
					case 'presentation':
					presentationCnt += dataCnt;
					break;
					case 'whiteboard':
					whiteboardCnt += dataCnt;
					break;
					case 'asset_annotation':
					annotationCnt += dataCnt;				
					break;
					case 'dictionary':
					dictionaryCnt += dataCnt;
					break;
				}
			}
		}
		for(var j=0;j<addedResourceData.length; j++){
			var addedDataCnt = Number(addedResourceData[j].count);						
			addedTotalCnt += addedDataCnt;
			//console.log("added cnt ===> ",addedTotalCnt," : ", addedDataCnt);
			//console.log("cnt ====", dataCnt+" >> "+userData[j].type+" >> " + totalCnt+" :: "+cnt);
			//if(!pflag){
				switch(addedResourceData[j].type){
					case 'tp':					
					lpCnt_added += addedDataCnt;
					//console.log("tp:",lpCnt_added);
					break;					
					case 'asset','activity':					
					resourceCnt_added += addedDataCnt;
					//console.log("asset:",resourceCnt_added);
					break;
					case 'question':
					questionCnt_added += addedDataCnt;
					//console.log("question:",questionCnt_added);
					break;					
					case 'game':
					gameCnt_added += addedDataCnt;
					//console.log("game:",gameCnt_added);
					break;
					case 'notes':
					noteCnt_added += addedDataCnt;
					//console.log("notes:",noteCnt_added);
					break;
				}
			/*} else {
				switch(userData[j].type){
					case 'presentation':
					presentationCnt += dataCnt;
					break;
					case 'whiteboard':
					whiteboardCnt += dataCnt;
					break;
					case 'asset_annotation':
					annotationCnt += dataCnt;				
					break;
					case 'asset_dictionary':
					dictionaryCnt += dataCnt;
					break;
				}
			}*/
		}
		if(!pflag){
			userDataArr.push([userName, tpCnt, mediaCnt, printCnt, activityCnt, questionCnt, gameCnt, totalCnt]);
		} else {
			
		}
	}	
	if(!pflag){
		utilizationDataArr.push(["Presentation",presentationCnt]);
		utilizationDataArr.push(["Whiteboard",whiteboardCnt]);
		utilizationDataArr.push(["Annotation",annotationCnt]);
		utilizationDataArr.push(["Dictionary",dictionaryCnt]);
		drawUserTable();			
		//resources added
		resourceAddedDataArr.push(["Topics", lpCnt_added]);	
		resourceAddedDataArr.push(["Resources", resourceCnt_added]);
		resourceAddedDataArr.push(["Questions", questionCnt_added]);
		resourceAddedDataArr.push(["Games", gameCnt_added]);
		resourceAddedDataArr.push(["Notes", noteCnt_added]);
		//get monthly resource hit data
		getMonthlyResourceHitData();
		//then get // get prev month data
		
	} else {
		utilizationDataArr[0].push(presentationCnt);
		utilizationDataArr[1].push(whiteboardCnt);
		utilizationDataArr[2].push(annotationCnt);
		utilizationDataArr[3].push(dictionaryCnt);		
		drawFeatureUtilizationTable();
		//
		resourceAddedDataArr[0].push(lpCnt_added);	
		resourceAddedDataArr[1].push(resourceCnt_added);
		resourceAddedDataArr[2].push(questionCnt_added);
		resourceAddedDataArr[3].push(gameCnt_added);
		resourceAddedDataArr[4].push(noteCnt_added);
		drawContributionIndexTable();
	}	

	//console.log("userdata:", addedResourceData);
}

function drawUserTable(p_mthYr){
	//console.log("----->",userUtilizationArray[p_mthYr].tableData);
	//console.log("drawUserTable",p_mthYr);
	//console.log("user:", userDataArr);
	$('#userTable').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": false,
		"sPaginationType": "full_numbers",
		//"aaSorting": [[ 8, "desc" ]],
		//"iDisplayLength":12,
		"aaData": userUtilizationArray[p_mthYr].tableData,
		"aoColumns": [
			{ "sTitle": "Teacher Name","sWidth":"30%"},
			{ "sTitle": "Subjects", "sWidth":"10%"},
			{ "sTitle": "Login count", "sWidth":"10%"},
			{ "sTitle": "Total hits", "sWidth":"10%" },
			{ "sTitle": "Time spent (hrs)", "sWidth":"10%"},
			{ "sTitle": "Details", "sWidth":"30%"}			
		]		
	});
}
function drawUsageHeatmapTable(p_mthYr){
	$('#usageHeatmapTable').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": false,
		"aaData": heatmapTableData[p_mthYr],
		"aoColumns": [
			{ "sTitle": "","sWidth":"30%"},
			{ "sTitle": "Early Childhood Education", "sWidth":"10%"},
			{ "sTitle": "Grade 1 to 2", "sWidth":"10%"},
			{ "sTitle": "Grade 3 to 4", "sWidth":"10%"},
			{ "sTitle": "Grade 5 to 6", "sWidth":"10%" },
			{ "sTitle": "Grade 7 to 8", "sWidth":"10%" },
			{ "sTitle": "Grade 9 to 10", "sWidth":"10%" },
			{ "sTitle": "Grade 11 to 12", "sWidth":"10%" }
		]		
	});
}

function getFeatureUtilizationData(){	
	//for previous month
	loadXML(monthId-1,yearId,true);	
}

function drawFeatureUtilizationTable(p_mthYr){
	//console.log("feature:", utilizationDataArr);
	$('#featureUtilTable').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": true,
		//"aaData": utilizationDataArr,
		"aaData": featureUtilizationArray[p_mthYr].tableData,
		"aoColumns": [
			{ "sTitle": ""},
			{ "sTitle": "Current Month"},
			{ "sTitle": "No. of Users"}					
		]		
	});
	//{ "sTitle": "", "sCellType": "th", "sClass":"left"},
	//{ "sTitle": "Previous Month", "sClass": "center"},
}

function drawContributionIndexTable(p_mthYr){
	//console.log("drawContributionIndexTable *** ", p_mthYr, contributionIndexArray[p_mthYr]);
	$('#contributionIndexTable').dataTable({
		"bPaginate": false,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": true,
		//"aaData": resourceAddedDataArr,
		"aaData":contributionIndexArray[p_mthYr].tableData,
		"aoColumns": [
			{ "sTitle": ""},
			{ "sTitle": "Current Month"},
			{ "sTitle": "No. of Users"}
						
		]		
	});
	//{ "sTitle": "Total School Resources", "sClass": "center"},
}

function drawClassRoomCapacityUtilTable(p_mthYr){
	//console.log("drawContributionIndexTable *** ", p_mthYr, contributionIndexArray[p_mthYr]);
	$('#classCapacityUtilizationTable').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": true,
		"sPaginationType": "full_numbers",
		//"aaData": resourceAddedDataArr,
		"aaData":terminalUsageStatsData[p_mthYr].inClassRoom.tableData,
		"aoColumns": [
			{ "sTitle": "Location"},
			{ "sTitle": "IP Address"},
			{ "sTitle": "No. of Hrs"}
						
		]		
	});
	//{ "sTitle": "Total School Resources", "sClass": "center"},
}
function drawEdgeRoomCapacityUtilTable(p_mthYr){
	//console.log("drawContributionIndexTable *** ", p_mthYr, contributionIndexArray[p_mthYr]);
	$('#edgeCapacityUtilizationTable').dataTable({
		"bPaginate": true,
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": true,
		"sPaginationType": "full_numbers",
		//"aaData": resourceAddedDataArr,
		"aaData":terminalUsageStatsData[p_mthYr].inEdgeRoom.tableData,
		"aoColumns": [
			{ "sTitle": "Location"},
			{ "sTitle": "IP Address"},
			{ "sTitle": "No. of Hrs"}
						
		]		
	});
	//{ "sTitle": "Total School Resources", "sClass": "center"},
}


function getMonthlyResourceHitData(){
	//getMonthlyData(currMonth);
	// get prev month data
	//getFeatureUtilizationData();
}

function getTimeSpent(val,flag){	
	var tempTime;
	var timeSpent;	
	if(val > 0){
		tempTime = String(val).split(".");
		var hrs = tempTime[0].length>1?tempTime[0]:"0"+tempTime[0];
		var tempMins = "00";
		if(tempTime[1]){			
			tempMins = String(parseInt(Number("."+tempTime[1])*60));
		}
		var mins = tempMins.length>1?tempMins:"0"+tempMins;
		if(flag == "decimal"){
			timeSpent = hrs+"."+mins;
		} else {
			timeSpent = hrs+":"+mins;			
		}
	}else {
		if(flag == "decimal"){
			timeSpent = "00.00";
		} else {
			timeSpent = "00:00";
		}
	}	
	return timeSpent;
}
