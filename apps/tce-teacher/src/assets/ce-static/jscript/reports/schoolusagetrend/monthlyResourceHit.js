var monthData
var monthlyHit = [];

var mthTopicCnt = 0;
var mthMediaCnt = 0;
var mthPrintCnt = 0;
var mthActivityCnt = 0;
var mthQuestionCnt = 0;
var mthGameCnt = 0;		
var mthOthersCnt = 0;

var mthTopicArr = [];
var mthMediaArr = [];
var mthPrintableArr = [];
var mthActivityArr = [];
var mthGameArr = [];
var mthQuestionArr = [];
var mthOthersArr = [];

var monthlyLoginTrend = [];
var monthlyMeanData = [];
var meanValueData=[];
//server data
var mthServerUpTime = [];
var mthServerUpMeanTime = [];
var mthServerInternetTime = [];
var mthServerInternetMeanTime = [];

function getMonthUsageData(p_mthYrId){
	mthTopicArr = [];
	mthMediaArr = [];
	mthPrintableArr = [];
	mthActivityArr = [];
	mthGameArr = [];
	mthQuestionArr = [];
	mthOthersArr = [];

	monthlyLoginTrend = [];
	monthlyMeanData = [];
	meanValueData=[];
	
	mthServerUpTime = [];
	mthServerUpMeanTime = [];
	mthServerInternetTime = [];
	mthServerInternetMeanTime = [];
	
	//console.log("ALL DATA = ",yearwiseData,p_mthYrId);
	var monthJson = yearwiseData[p_mthYrId];
	monthData = jsonPath(monthJson, '$.viewed.usage');
	var loginData = jsonPath(monthJson,'$.usersMetadata.user');
	var daysInMth = daysInMonth(monthId, yearId);
	//console.log("LOGN DATA USER:", loginData);
	//monthlyUsageTrend[currMonth+""+currYear]={};
	for(var n=1; n<=daysInMth; n++){
		var totalHitCnt = 0;
		var totalLoginCnt = 0;
		mthTopicCnt = 0;
		mthMediaCnt = 0;
		mthPrintCnt = 0;
		mthActivityCnt = 0;
		mthQuestionCnt = 0;
		mthGameCnt = 0;		
		mthOthersCnt = 0;
		//
		var totalServerUpTime = 0;
		var totalServerInternetTime = 0;
		var day = jsonPath(monthData, '$.[?(@.logDay=="'+n+'")]');
		//console.log("GET MONTHLY DATA == ",day);
		if(day){
			for(var m=0; m<day.length; m++){
				var mCount = Number(day[m].count);				
				switch(day[m].type){							
					case 'tp':					
					case 'chapter':		
								
					mthTopicCnt += mCount;
					//console.log("Day:",n,"day[m].type",day[m].type,"Count:",mCount,"mthTopicCnt:",mthTopicCnt);
					break;
					case 'asset_media':					
					mthMediaCnt += mCount;
					break;
					case 'asset_print':
					mthPrintCnt += mCount;
					break;
					case 'activity':
					mthActivityCnt += mCount;
					break;
					case 'question':
					mthQuestionCnt += mCount;
					break;
					case 'game':
					mthGameCnt += mCount;
					break;	
					default:
					mthOthersCnt += mCount;				
				}
				//totalHitCnt += Number(day[m].count);
			}
		}
		//get server data here
		var tempyr = p_mthYrId.substr(-4,p_mthYrId.length);		
		var tempmth = "";
		var tempday = n;		
		if(p_mthYrId.length == 5){
			tempmth = p_mthYrId.substr(0,1);
		} else if(p_mthYrId.length == 6){
			tempmth = p_mthYrId.substr(0,2);
		}
		if(tempmth.length == 1){
			tempmth = "0"+tempmth;
		}
		if(tempday < 10){
			tempday = "0"+tempday;
		}
		//console.log("serverStatsData[p_mthYrId]",serverStatsData[p_mthYrId]);
		if(serverStatsData[p_mthYrId]){
			var tDate = tempyr + "-" + tempmth + "-" + tempday;
			//console.log("tDate:",tDate);
			var dayServerData = jsonPath(serverStatsData[p_mthYrId],"$.server_statistics.[?(@.pollDate == '"+tDate+"')]");
			//console.log("dayServerData = ",dayServerData);
			//console.log("Temp date:",tDate, dayServerData,serverStatsData[p_mthYrId]);
			//console.log("=>",((dayServerData[0].server)/60).toFixed(2),((dayServerData[0].internet)/60).toFixed(2));
			if(dayServerData){
				totalServerUpTime = Number(((dayServerData[0].server)/60).toFixed(2));
				totalServerInternetTime = Number(((dayServerData[0].internet)/60).toFixed(2));
			}
		}
		mthServerUpTime.push([n, totalServerUpTime] );
		mthServerInternetTime.push([n,totalServerInternetTime]);			
		//monthlyHit.push([n,totalHitCnt]);
		mthTopicArr.push([n,mthTopicCnt]);
		mthMediaArr.push([n,mthMediaCnt]);
		mthPrintableArr.push([n,mthPrintCnt]);
		mthActivityArr.push([n,mthActivityCnt]);
		mthGameArr.push([n,mthGameCnt]);;
		mthQuestionArr.push([n,mthQuestionCnt]);
		mthOthersArr.push([n,mthOthersCnt]);
		//GET LOGIN COUNT HERE
		var loginForDay = jsonPath(loginData,'$.[?(@.logDay=="'+n+'")]');
		//console.log("loginForDay",n,loginForDay);
		_.each(loginForDay,function(cnt){totalLoginCnt+=Number(cnt.count);});
		monthlyLoginTrend.push([n,totalLoginCnt]);
		//monthlyMeanData.push([n,dataMeanValue]);
		/*console.log("TOTAL LOGIN FOR THE DAY:",totalLoginCnt); 
		//console.log("LOGIN FOR THE DAY:",loginForDay);*/ 
		//console.log("day=",n," ::: ", totalHitCnt);
	}	
	var mCnt=0;
	meanValueData = $.map(monthlyLoginTrend, function(d){return d[1]});
	if(meanValueData){
		//var mData = _.filter(meanValueData, function(n){return n>0;})
		var mData = _.without(meanValueData, 0);
		var mTotal=0; 
		_.each(mData, function(m){mTotal+=m});
		var meanValue = parseInt(mTotal/mData.length);
		for(var n=1; n<=daysInMth; n++){
			monthlyMeanData.push([n,meanValue]);
		}
	}
	/*
	if(serverStatsData[p_mthYrId]){		
		meanValueData = $.map(mthServerUpTime, function(d){return d[1]});
		console.log("mean value calc:",meanValueData);
		if(meanValueData){
			//var mData = _.filter(meanValueData, function(n){return n>0;})
			var mData = _.without(meanValueData, 0);
			var mTotal=0; 
			_.each(mData, function(m){mTotal+=m});
			var meanValue = parseInt(mTotal/mData.length);
			for(var n=1; n<=daysInMth; n++){
				mthServerUpMeanTime.push([n,meanValue]);
			}
		}
		meanValueData = $.map(mthServerInternetTime, function(d){return d[1]});
		console.log("mean value calc:",meanValueData);
		if(meanValueData){
			//var mData = _.filter(meanValueData, function(n){return n>0;})
			var mData = _.without(meanValueData, 0);
			var mTotal=0; 
			_.each(mData, function(m){mTotal+=m});
			var meanValue = parseInt(mTotal/mData.length);
			for(var n=1; n<=daysInMth; n++){
				mthServerInternetMeanTime.push([n,meanValue]);
			}
		}
	}
	*/
	//console.log("meanValueData:::::::::",mTotal,">",meanValue);
	//console.log("monthlyHit",monthlyHit);
	drawMonthlyResourceHitChart();
	drawMonthlyLoginChart();
	if(isSummaryAvailable[p_mthYrId]){
		$('#serverHitChart>svg').css("display","block");
		drawMonthlyServerHitChart();
	} else {
		$('#serverHitChart>svg').empty();
		$('#serverHitChart>svg').css("display","none");
	}
	//draw complete year charts
	//getCompleteYearData();
}

function daysInMonth(month, year) {
	//console.log("days in month",month, year,":",new Date(year, month, 0).getDate())
    return new Date(year, month, 0).getDate();
}
function getDaysArr(p_val){
	//console.log("P_VAL",p_val);
	var arr = [];
	for(var i=1; i<=p_val; i++){
		arr.push(i);
	}
	return arr;
}

function drawMonthlyResourceHitChart(){
	//console.log("draw graph");
	$('#mthResourceChart>svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.stackedAreaChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .clipEdge(true);
	     chart.margin({left: 80});   
	    var daysInMth = daysInMonth(monthId, yearId);	             
		var tickmarks = getDaysArr(daysInMth);
		//console.log("TOTAL DAYS:",tickmarks);
		  chart.xAxis		  				  		
		  		.tickValues(tickmarks)
		  		.tickFormat(function(d){
		  			var dayName = new Date.parse(monthId+"-"+d+"-"+yearId).toString('ddd');
		  			if(dayName == "Sun"){
		  				return d+"("+dayName.substr(0,1)+")";
		  			} else {
		  				return d;
		  			}
		  			
		  		})		  		
		  		.showMaxMin(true);
		      
		
		  chart.yAxis
		      .axisLabel('Assets Used')
		      .tickFormat(d3.format('.0f'));
		
		  d3.select('#mthResourceChart svg')
		      .datum(getMthData())
		    .transition().duration(500)
		      .call(chart);
		//d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getMthData(){
	return [ 
		{ 
			"key" : "Topic" , 		
			"values" : mthTopicArr,		
		},
		{ 
			"key" : "Media" , 		
			"values" : mthMediaArr,		
		},
		{ 
			"key" : "Printable" , 		
			"values" : mthPrintableArr,		
		},
		{ 
			"key" : "Activity" , 		
			"values" : mthActivityArr,		
		},
		{ 
			"key" : "Game" , 		
			"values" : mthGameArr,		
		},		
		{ 
			"key" : "Question" , 		
			"values" : mthQuestionArr,		
		}
		,		
		{ 
			"key" : "Others" , 		
			"values" : mthOthersArr,		
		}
	]; 
}


function drawMonthlyLoginChart(){
	$('#loginChart>svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(false)
	                .clipEdge(true);
	      chart.margin({left: 80});
	    var daysInMth = daysInMonth(monthId, yearId);	             
		var tickmarks = getDaysArr(daysInMth);          	       
		//var tickmarks = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
		  chart.xAxis		  				  		
		  		.tickValues(tickmarks)
		  		.tickFormat(function(d){
		  			var dayName = new Date.parse(monthId+"-"+d+"-"+yearId).toString('ddd');
		  			if(dayName == "Sun"){
		  				return d+"("+dayName.substr(0,1)+")";
		  			} else {
		  				return d;
		  			}
		  			
		  		})	
		  		.showMaxMin(true)
		      .axisLabel('Date');
		
		  chart.yAxis
		      .axisLabel('Login count')
		      .tickFormat(d3.format('.0f'));
		
		  d3.select('#loginChart svg')
		      .datum(getMthLoginData())
		    .transition().duration(500)
		      .call(chart);
		    // console.log("Y LABEL",d3.select('.nv-y.nv-axis > g > g > text').attr('y'));
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  //console.log("Y LABEL",d3.select('.nv-y.nv-axis > g > g > text').attr('y'));
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getMthLoginData(){
	return [ 
		{ 
			"key" : "Number of logins" , 		
			"values" : monthlyLoginTrend,		
		},
		{
			"key" : "Mean",
			"values" : monthlyMeanData,
			"color": '#ff7f0e'
		}
	]; 
}

function drawMonthlyServerHitChart(){
	$('#serverHitChart>svg').empty();
	nv.addGraph(function() {
		var chart = nv.models.lineChart()
					.x(function(d) { return d[0] })
	                .y(function(d) { return d[1] })
	                .showLegend(true);	                
	                
	      chart.margin({left: 80});
	    var daysInMth = daysInMonth(monthId, yearId);	             
		var tickmarks = getDaysArr(daysInMth);          	       
		//var tickmarks = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
		  chart.xAxis		  				  		
		  		.tickValues(tickmarks)
		  		.tickFormat(function(d){
		  			var dayName = new Date.parse(monthId+"-"+d+"-"+yearId).toString('ddd');
		  			if(dayName == "Sun"){
		  				return d+"("+dayName.substr(0,1)+")";
		  			} else {
		  				return d;
		  			}
		  			
		  		})	
		  		.showMaxMin(true)
		      .axisLabel('Date');
		
		  chart.yAxis
		      .axisLabel('Facility uptime (hrs)')
		      .showMaxMin(true)
		      .tickFormat(d3.format('.0f'));
		
		  d3.select('#serverHitChart svg')
		      .datum(getServerHitData())
		    .transition().duration(500)
		      .call(chart);
		    // console.log("Y LABEL",d3.select('.nv-y.nv-axis > g > g > text').attr('y'));
		 // d3.select('.nv-y.nv-axis > g > g > text').attr('y','-35');
		  //console.log("Y LABEL",d3.select('.nv-y.nv-axis > g > g > text').attr('y'));
		  nv.utils.windowResize(chart.update);
		
		  return chart;
	 });
}

function getServerHitData(){
	//console.log("mthServerUpTime",mthServerUpTime,mthServerInternetTime);
	return [ 
		{ 
			"key" : "ClassEdge Server uptime (hrs)" , 		
			"values" : mthServerUpTime,		
		},
		{
			"key" : "Internet uptime (hrs)",
			"values" : mthServerInternetTime,
			"color": '#ff7f0e'
		}
		/*,
		{
			"key" : "Server usage (Mean)",
			"values" : mthServerUpMeanTime,			
		},
		{
			"key" : "Internet connection (Mean)",
			"values" : mthServerInternetMeanTime,			
		}
		*/
	]; 
}