var isAirApp = false;
var urlForAIR;
var selfUsageReportUrl;
var usersList;

var loaderHtml = "<table border='0' cellpadding='0' cellspacing='0' width='310px' height='50px'>"+
"<tr><td align='center' valign='middle'>"+
"<img src='/ce-static/images/common/loader_white.gif'/>"+
"</td></tr>"+
"</table>";

function setUserListBox(users){
	usersList = users;
	if(usersList.length > 0 && usersList.length == 1){
		$("#reportFor").append("<option value='"+usersList[0].userId+"'>"+usersList[0].fullName+"</option>");
	}else{
		$("#reportFor").append("<option value=''>Select a User</option>");
		$("#reportFor").append("<option value='all'>All</option>");
		for ( var userIndex = 0; userIndex < usersList.length; userIndex++) {
			$("#reportFor").append("<option value='"+usersList[userIndex].userId+"'>"+usersList[userIndex].fullName+"</option>");
		}
	}
}

function validate(formName) {
	var monthType = document.getElementById(formName).monthType.value;
	if(monthType==""){
		alert('Please Select Month Type');
		return false;
	}
	if(monthType == 'pMonth'){
		var yearId = document.getElementById(formName).yearId.value;
		var monthId = document.getElementById(formName).monthId.value;
		if(yearId == ''){
			alert('Please Select Year');
			return false;
		}
		if(monthId == ''){
			alert('Please Select Month');
			return false;
		}
	}else if(monthType == 'cMonth'){
		var startDate = document.getElementById(formName).startDate.value;
		var endDate = document.getElementById(formName).endDate.value;
		var stDate = convertDate(startDate);
		var enDate = convertDate(endDate);
		var date = new Date();
		if (startDate == "") {
			alert(" - Enter the Start Date .");
			return false;
		}
		if (endDate == "") {
			alert(" - Enter the End Date .");
			return false;
		}
		if (stDate.getTime() > enDate.getTime()) {
			alert(" - - Ensure the End Date is greater than or equal to the Start Date .");
			return false;
		}
	}
	var reportFor = document.getElementById(formName).reportFor;
	if (reportFor != null) {
		if (reportFor.value == "") {
			alert(" - Please select the Report For .");
			return false;
		}
	}
	return true;
}

function convertToDate(dateString) {
	var darry = splitDateString(dateString);
	var date = new Date(darry[2], darry[1], darry[0], 0, 0, 0, 0);
	return date;
}

function convertDate(dateString) {
	var darry = splitDateString(dateString);
	var date = new Date(darry[2], darry[0]-1, darry[1], 0, 0, 0, 0);
	return date;
}

function convertToSQLDate(dateString) {
	var darry = splitDateString(dateString);	
	var date = darry[2] + '/' + darry[0] + '/' +darry[1] ;
	return date;
}

function monthTypeDiv() {
	var monthType = document.getElementById("monthType").value;
	if (monthType == '') {
		alert("Please Select Month Type");
	} else if (monthType == 'cMonth') {
		$("#cMonthDiv").show();
		$("#pMonthDiv").hide();
		$("#reportDisplay").html('');
	} else {
		var currentYear = new Date().getFullYear();
		for ( var year = currentYear; year > 2010;year--) {
			$("#yearId").append(
					'<option value="'+year+'">' + year + '</option>');
		}
		$("#pMonthDiv").show();
		$("#cMonthDiv").hide();
		$("#reportDisplay").html('');
	}
}

function viewUsageReport(url, formName, selfUsageReportUrl) {
	if (validate(formName)) {
		var data = formData(formName);
		//add table
		var reportDisplayDiv=document.getElementById("reportDisplay");
		reportDisplayDiv.innerHTML =loaderHtml;		
		jQuery.getJSON(url, data, function(resData) {
			var aaData=getDataTableArr(resData,selfUsageReportUrl);
			//STEP-1 CLEAR inner data before init
			reportDisplayDiv.innerHTML="<table id='usersReportDataTable' class='display css_left'></table>";
			//STEP-2 populate now
			dataTableInit(formName,aaData);
		});
		
	}
}

function dataTableInit(formName,aaData){		
		jQuery('#usersReportDataTable').dataTable({
		"bPaginate": true,
		"sPaginationType": "full_numbers",
		"bLengthChange": false,
		"bFilter": false,
		"bSort": true,
		"bInfo": false,
		"bAutoWidth": false,		
		"aaData":aaData,
		"sScrollX": "100%",
		"sScrollXInner": "350%",
		"bScrollCollapse": true,
		"iDisplayLength": 15,
		"aoColumns": [
			{ "sTitle": "User Name","sWidth":"270px"},
			{ "sTitle": "Details", "sWidth":"90px"},
			{ "sTitle": "EdgeRoom Terminal Usage(mins.)", "sWidth":"130px" },
			{ "sTitle": "ClassRoom Terminal Usage(mins.)", "sWidth":"130px" },
			{ "sTitle": "LessonPlan Viewed", "sWidth":"90px"},
			{ "sTitle": "LessonPlan Added", "sWidth":"90px"},			
			{ "sTitle": "Multimedia Viewed [Edgeroom]", "sWidth":"130px" },
			{ "sTitle": "Multimedia Viewed [Classroom]", "sWidth":"130px" },
			{ "sTitle": "Activities Viewed [Edgeroom]", "sWidth":"130px" },
			{ "sTitle": "Activities Viewed [Classroom]", "sWidth":"130px" },
			{ "sTitle": "Activities Printed", "sWidth":"90px" },
			{ "sTitle": "Printables Printed", "sWidth":"90px" },
			{ "sTitle": "Printables Viewed [Edgeroom]", "sWidth":"130px" },
			{ "sTitle": "Printables Viewed [Classroom]", "sWidth":"130px" },
			{ "sTitle": "Questions Viewed", "sWidth":"90px" },
			{ "sTitle": "Notes Added", "sWidth":"90px" },
			{ "sTitle": "Resources Added", "sWidth":"90px" },
			{ "sTitle": "Questions Added", "sWidth":"90px" },
			{ "sTitle": "Game Viewed", "sWidth":"90px" },
			{ "sTitle": "Game Added", "sWidth":"90px" },
			{ "sTitle": "Tools Viewed", "sWidth":"90px" },
			{ "sTitle": "Default Presentation Viewed", "sWidth":"130px" },
			{ "sTitle": "Presentation created in Staffroom", "sWidth":"130px" },
			{ "sTitle": "Presentation Viewed", "sWidth":"90px" },
			{ "sTitle": "Whiteboard", "sWidth":"90px" },
			{ "sTitle": "Accessed Days", "sWidth":"90px" },
			{ "sTitle": "Classedge Utilization", "sWidth":"130px" }			
		]
	});
}
function formData(formName){
	var schoolId = document.getElementById(formName).schoolId.value;
	var monthType = document.getElementById(formName).monthType.value;
	var monthId = document.getElementById(formName).monthId.value;
	var yearId = document.getElementById(formName).yearId.value;
	var startDate = document.getElementById(formName).startDate.value;
	startDate = convertToSQLDate(startDate);
	var endDate = document.getElementById(formName).endDate.value;
	endDate = convertToSQLDate(endDate);
	var reportFor = document.getElementById(formName).reportFor.value;

	var data = {
			schoolId : schoolId,
			monthType : monthType,
			monthId : monthId,
			yearId : yearId,
			startDate : startDate,
			endDate : endDate,
			reportFor : reportFor
	};
	return data;
}
function getDataTableArr(resData, selfUsageReportUrl) {
	var reportFor = document.getElementById("reportFor").value;	
	var parentArr=new Array();
	if (reportFor != 'all') {
		for ( var i = 0; i < resData.length; i++) {
			if (reportFor == resData[i].userId) {
				parentArr.push(getChildData(resData[i],selfUsageReportUrl));				
			}
		}
	} else {
		for ( var i = 0; i < resData.length; i++) {
			for ( var userIndex = 0; userIndex < usersList.length; userIndex++) {
				if (usersList[userIndex].userId == resData[i].userId) {
					parentArr.push(getChildData(resData[i],selfUsageReportUrl));
				}
			}
		}
	}
	return parentArr;
}

function getChildData(report,selfUsageReportUrl) {
	var childArr=new Array();		
	childArr.push(report.userName);
	if (report.totalCount == 0) {		
		childArr.push("<a onclick=''>No details</a>");		
	} else {
		childArr.push("<a style='cursor: pointer;' onclick='javascript:showselfusageReport(\""+ selfUsageReportUrl+ "\",\""+ report.userId+ "\")'>Details</a>");		
	}
	childArr.push(report.edgeRoomUsage);
	childArr.push(report.classRoomUsage);
	childArr.push(report.lessonplanCount);
	childArr.push(report.lessonplanAddedCount);	
	childArr.push(report.multimediaEdgeRoomCount);
	childArr.push(report.multimediaClassRoomCount);
	childArr.push(report.activityEdgeRoomCount);
	childArr.push(report.activityClassRoomCount);
	childArr.push(report.activityPrintedCount);
	childArr.push(report.printPrintedCount);
	childArr.push(report.printableEdgeRoomCount);
	childArr.push(report.printableClassRoomCount);
	childArr.push(report.questionsViewedCount);
	childArr.push(report.notesAddedCount);
	childArr.push(report.resourceAaddedCount);
	childArr.push(report.questionsAddedCount);
	childArr.push(report.gameViewedCount);
	childArr.push(report.gameAddedCount);
	childArr.push(report.toolsViewedCount);
	childArr.push(report.defaultPresentationViewedCount);
	childArr.push(report.presentationCreatedClassroomCount);
	childArr.push(report.presentationViewedCount);
	childArr.push(report.whiteboardCount);
	childArr.push(report.accessedonCount);
	childArr.push(report.totalCount);
	return childArr;
}
function exportReport(formName, url) {
	if (validate(formName)) {
		var data = formData(formName);
		var reportFor = document.getElementById(formName).reportFor.value;
		var monthType = document.getElementById(formName).monthType.value;
		if( monthType == 'cMonth'){
			url = url + "&startDate=" + data.startDate + "&endDate=" + data.endDate;
		}else{
			url = url + "&monthId=" + data.monthId + "&yearId=" + data.yearId;
		}
		url = url + "&monthType=" + monthType + "&schoolId=" + data.schoolId + "&reportFor=" + reportFor;
		if (isAirApp) {
			urlForAIR = url;
			calledFromJSHandlerFunction('reports');
		} else {
			//var data = {startDate : startDate,
			//					endDate : filterValueVar};
			return download(url,null);
			//document.getElementById(formName).action = url;
			//document.getElementById(formName).submit();
		}
	}
}
function setAirApp() {
	isAirApp = true;
}
function getReportURL() {
	return urlForAIR;
}
function showselfusageReport(url, selectedUserId) {
	var monthType = document.getElementById("monthType").value;
	if(monthType == 'cMonth'){
		var startDate = document.getElementById('startDate').value;
		//startDate = convertToSQLDate(startDate);
		var endDate = document.getElementById('endDate').value;
		//endDate = convertToSQLDate(endDate);
		url += "&startDate="+startDate+"&endDate="+endDate;
	}else{
		var yearId = document.getElementById('yearId').value;
		//startDate = convertToSQLDate(startDate);
		var monthId = document.getElementById('monthId').value;
		//endDate = convertToSQLDate(endDate);
		url += "&monthId="+monthId+"&yearId="+yearId;
	}
	 document.getElementById('selectedUserId').value=selectedUserId;
	url += "&monthType="+monthType;
	document.getElementById("reportBaseForm").action = url;
	document.getElementById("reportBaseForm").submit();
}