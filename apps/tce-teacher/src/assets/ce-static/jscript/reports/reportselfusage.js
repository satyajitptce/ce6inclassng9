/**
 * 
 */


var statusHtml = "<table border='0' cellpadding='0' cellspacing='0' >"
	+ "<tr><td align='center' valign='middle'>"
	+ "<img src='/ce-static/images/common/loader_white.gif'/>"
	+ "</td></tr>" + "</table>";
function convertToSQLDate(dateString) {
	var darry = splitDateString(dateString);	
	var date = darry[2] + '/' + darry[0] + '/' +darry[1] ;
	return date;
}

function showPrevMonth(prevMonUsageUrl, selfUsageDetailedDataUrl) {
	var selectedUserId = document.getElementById("selectedUserId").value;
	var schoolId = document.getElementById("schoolId").value;
	var input1 = document.getElementById("input1").value;
	var input2 = document.getElementById("input2").value;
	var reqData = {
			selectedUserId : selectedUserId,
			schoolId : schoolId,
			monthType : 'pMonth',
			input1 : input1,
			input2 : input2
	};
	jQuery.getJSON(prevMonUsageUrl, reqData, function(reports) {
		for ( var i = 0; i < reports.length; i++) {
			var report = reports[i];
			var grdSubDiv = report.gradeId + report.subjectId;
			var divHtml = '<div id="menu-'+grdSubDiv+'">';
			divHtml += arrangeLeftData(report, selfUsageDetailedDataUrl);
			divHtml += '</div>';
			$('#dataDiv').append(divHtml);
			jQuery('#data-' + grdSubDiv).html(statusHtml);
			divHtml = arrangeRightData(report.details);
			jQuery('#data-' + grdSubDiv).html(divHtml);
			if (divHtml == "No Usage") {
				jQuery('#menu-' + grdSubDiv).remove();
			}
		}
	});
}

function showCurntMonth(curntMonUsageUrl, selfUsageDetailedDataUrl) {
	var selectedUserId = document.getElementById("selectedUserId").value;
	var schoolId = document.getElementById("schoolId").value;
	var input1 = document.getElementById("input1").value;
	var input2 = document.getElementById("input2").value;
	var reqData = {
			selectedUserId : selectedUserId,
			schoolId : schoolId,
			monthType : 'cMonth',
			input1 : convertToSQLDate(input1),
			input2 : convertToSQLDate(input2)
	};
	var leftDataList = new Array();
	jQuery.getJSON(curntMonUsageUrl, reqData, function(reports) {
		for ( var i = 0; i < reports.length; i++) {
			var row = {
					bookId : reports[i].bookId,
					gradeId : reports[i].gradeId,
					gradeTitle : reports[i].gradeTitle,
					subjectId : reports[i].subjectId,
					subjectTitle : reports[i].subjectTitle
			};
			leftDataList.push(row);
		}
		targetFunction(leftDataList, reqData, selfUsageDetailedDataUrl);
	});
}

function targetFunction(leftDataList, reqData, selfUsageDetailedDataUrl) {
	for ( var i = 0; i < leftDataList.length; i++) {
		var leftDataRow = leftDataList[i];
		var data = {
				bookId : leftDataRow.bookId,
				gradeId : leftDataRow.gradeId,
				subjectId : leftDataRow.subjectId,
				selectedUserId : reqData.selectedUserId,
				schoolId : reqData.schoolId,
				input1 : reqData.input1,
				input2 : reqData.input2
		};

		var grdSubDiv = leftDataRow.gradeId + leftDataRow.subjectId;
		var divHtml = '<div id="menu-'+grdSubDiv+'">';
		divHtml += arrangeLeftData(leftDataRow, selfUsageDetailedDataUrl);
		divHtml += '</div>';
		$('#dataDiv').append(divHtml);
		jQuery('#data-' + grdSubDiv).html(statusHtml);
		$.ajax({
			url : selfUsageDetailedDataUrl,
			dataType : 'json',
			data : data,
			async : false,
			success : function(rightData) {
				var divHtml = arrangeRightData(rightData);
				jQuery('#data-' + grdSubDiv).html(divHtml);
				if (divHtml == "No Usage") {
					jQuery('#menu-' + grdSubDiv).remove();
				}
			}
		});

	}
}

function arrangeLeftData(report, selfUsageDetailedDataUrl) {
	var html = '<table width="3150px"> <tr><td colspan="2" width="3050px" height="5px"></td></tr>';
	html += '<tr>';
	html += '<td width="350px" class="borderBT borderL" height="100%">';
	html += '<table width="350px" height="100%"><tr>';
	html += '<td width="100px" class="borderR text">' + report.gradeTitle
	+ '</td>';
	html += '<td width="150px" class="borderR text">' + report.subjectTitle
	+ '</td>';
	html += '<td width="100px" class="text"><a style="cursor: pointer;"';
	html += 'onclick="javascript:viewSelfUsageDetailedReport(\''
		+ selfUsageDetailedDataUrl + '\',\'' + report.bookId + '\',\''
		+ report.gradeId + '\',\'' + report.subjectId + '\');">';
	html += '<span id="toggle-'+report.gradeId+report.subjectId+'">hide</span> </a></td></tr></table></td>';
	html += '<td width="2800px" class="borderFull text">';
	html += '<div id="data-'+report.gradeId+report.subjectId+'"></div></td></tr></table>';
	return html;
}

function arrangeRightData(jsonContent) {
	var content = "";
	if (jsonContent.length == 0) {
		content += 'No Usage';
	} else {
		content += '<table width="2800px" height="100%">';
		jQuery
		.each(
				jsonContent,
				function(chapterIndex, chapter) {
					content += '<tr ';
					if (!(jsonContent.length == 1 || chapterIndex == (jsonContent.length - 1))) {
						content += '	class="borderB" ';
					}
					content += '>';
					if (chapterIndex == 0) {
						content += '	<td rowspan="'+jsonContent.length+'" width="100px" class="borderR">'
						+ jsonContent.length + '</td>';
					}
					content += '<td width="200px" class="borderR">'
						+ chapter.chapterTitle + '</td>';

					content += '<td width="200px" class="borderR"><table width="200px">';
					jQuery
					.each(
							chapter.tpTitles,
							function(tpIndex, tp) {
								content += '<tr><td width="200px" ';
								if (!(chapter.tpTitles.length == 1 || tpIndex == (chapter.tpTitles.length - 1))) {
									content += 'class="borderB"';
								}
								content += '>' + tp
								+ '</td></tr>';
							});
					content += '</table></td>';

					content += '<td width="400px"><table width="400px" height="100%"><tr>';
					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">';
					content +=  chapter.media.edgeRoom.count + '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.media.edgeRoom.data,
							function(meIndex, me) {
								content += '<tr><td width="150px"';
								if (!(chapter.media.edgeRoom.data.length == 1 || meIndex == chapter.media.edgeRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + me.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td>';

					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">';
					content +=  chapter.media.classRoom.count
						+ '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.media.classRoom.data,
							function(mcIndex, mc) {
								content += '<tr><td width="150px"';
								if (!(chapter.media.classRoom.data.length == 1 || mcIndex == chapter.media.classRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + mc.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td></tr></table></td>';

					content += '<td width="400px"><table width="400px" height="100%"><tr>';
					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">'
						+ chapter.printable.edgeRoom.count
						+ '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.printable.edgeRoom.data,
							function(peIndex, pe) {
								content += '<tr><td width="150px"';
								if (!(chapter.printable.edgeRoom.data.length == 1 || peIndex == chapter.printable.edgeRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + pe.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td>';

					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">'
						+ chapter.printable.classRoom.count
						+ '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.printable.classRoom.data,
							function(pcIndex, pc) {
								content += '<tr><td width="150px"';
								if (!(chapter.printable.classRoom.data.length == 1 || pcIndex == chapter.printable.classRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + pc.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td></tr></table></td>';

					content += '<td width="400px"><table width="400px" height="100%"><tr>';
					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">'
						+ chapter.activity.edgeRoom.count
						+ '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.activity.edgeRoom.data,
							function(aeIndex, ae) {
								content += '<tr><td width="150px"';
								if (!(chapter.activity.edgeRoom.data.length == 1 || aeIndex == chapter.activity.edgeRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + ae.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td>';

					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="50px" class="borderR">'
						+ chapter.activity.classRoom.count
						+ '</td>';
					content += '<td width="150px"><table width="150px" height="100%">';
					jQuery
					.each(
							chapter.activity.classRoom.data,
							function(acIndex, ac) {
								content += '<tr><td width="150px"';
								if (!(chapter.activity.classRoom.data.length == 1 || acIndex == chapter.activity.classRoom.data.length - 1)) {
									content += 'class="borderB"';
								}
								content += '>' + ac.title
								+ '</td></tr>';
							});
					content += '</table></td></tr></table></td></tr></table></td>';

					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="100px" class="borderR">'
						+ chapter.questViewed.edgeRoom.count
						+ '</td>';
					content += '<td width="100px">'
						+ chapter.questViewed.classRoom.count
						+ '</td>';
					content += '</tr></table></td>';

					content += '<td width="100px" class="borderR">'
						+ chapter.notesAdded.count + '</td>';
					content += '<td width="100px" class="borderR">'
						+ chapter.resourcesAdded.count
						+ '</td>';
					content += '<td width="100px" class="borderR">'
						+ chapter.questAdded.count + '</td>';
					content += '<td width="100px" class="borderR">'
					+ chapter.whiteboard.count + '</td>';

					content += '<td width="200px" class="borderR"><table width="200px" height="100%"><tr>';
					content += '<td width="100px" class="borderR">';

					if ((chapter.chapterId.substr(0, 5) == 'cchp-')
							&& chapter.defaultPresentationViewed.edgeRoom.count == 0) {
						content += 'NA';
					} else {
						content += chapter.defaultPresentationViewed.edgeRoom.count;
					}
					content += '</td>';

					content += '<td width="100px">';
					if ((chapter.chapterId.substr(0, 5) == 'cchp-')
							&& chapter.defaultPresentationViewed.classRoom.count == 0) {
						content += 'NA';
					} else {
						content += chapter.defaultPresentationViewed.classRoom.count;
					}
					content += '</td>';
					content += '</tr></table></td>';

					content += '<td width="100px" class="borderR">'
						+ chapter.presentationAdded.count
						+ '</td>';

					content += '<td width="200px"><table width="200px" height="100%"><tr>';
					content += '<td width="100px" class="borderR">';
					content += chapter.presentationViewed.edgeRoom.count
					+ '</td>';
					content += '<td width="100px">';
					content += chapter.presentationViewed.classRoom.count
					+ '</td>';
					content += '</tr></table></td>';
					content += '</tr>';

				});
		content += '</table>';
	}
	return content;
}


function viewSelfUsageDetailedReport(url, bookId, gradeId, subjectId) {
	var divDataId = '#data-' + gradeId + subjectId;
	var divToggleId = '#toggle-' + gradeId + subjectId;
	if ($(divDataId).is(':hidden')) {
		$(divDataId).show(300).css({
			visibility : "visible",
			display : "block"
		});
		jQuery(divToggleId).html('hide');
	} else {
		$(divDataId).hide(300).css({
			visibility : "hidden",
			display : "none"
		});
		jQuery(divToggleId).html('show');
	}
}

function backToReport(url,formId){
	url += "&isFromSelfUsage="+true;
	var monthType = document.getElementById('monthType').value;
	if(monthType == 'cMonth'){
		var input1 = document.getElementById('input1').value;
		var input2 = document.getElementById('input2').value;
		url += "&input1="+input1+"&input2="+input2;
	}
	document.getElementById(formId).action = url;
	document.getElementById(formId).submit();
}

//function convertToJSDate(dateString) {
//	var darry = splitDateString(dateString);
//	var date = darry[2] + '/' + darry[1] + '/' + darry[0];
//	return date;
//}

//OnScrollDiv() is for synchronizing two divs
function OnScrollDiv(dataDiv) {
	var titleDiv = document.getElementById("titleDiv");
	titleDiv.scrollLeft = dataDiv.scrollLeft;
}
