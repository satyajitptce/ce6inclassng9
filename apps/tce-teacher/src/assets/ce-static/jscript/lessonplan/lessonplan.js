/**
 * 
 */

function listLps(url) {
	var subjectId =  document.getElementById("subjectId").value;
	var gradeId =  document.getElementById("gradeId").value;
	var data = {subjectId : subjectId,
			gradeId : gradeId
	};
	document.getElementById("lpInfo").innerHTML = '';
	callAjax('#lpList', url, data);
}

function createCustomChapter(url) {
	var subjectId =  document.getElementById("subjectId").value;
	var gradeId =  document.getElementById("gradeId").value;
	url += "&subjectId="+subjectId+"&gradeId="+ gradeId;
	document.getElementById("tocForm").action = url;
	document.getElementById("tocForm").submit();
}

function editCustomChapter(chapterId, url) {
	var subjectId =  document.getElementById("subjectId").value;
	var gradeId =  document.getElementById("gradeId").value;
	url += "&mode=edit&chapterId="+ chapterId+"&subjectId="+ subjectId+"&gradeId="+ gradeId;
	document.getElementById("tocForm").action = url;
	document.getElementById("tocForm").submit();
}
function deleteCustomChapter(chapterId) {
	alert("under construction..");
	document.getElementById("tocForm").action = url;
	document.getElementById("tocForm").submit();
}
function getChapterInfo(chapterId) {
	var url = '${customChapterInfoUrl}';
	var bookId = document.getElementById("bookId").value;
	var data = {
			customChapterId : chapterId,
			bookId : bookId
	};
	callAjax('#lpInfo', url, data);
}
function loadLpPlayer(chapterId, url) {
	url +=  "&chapterId=" + chapterId;
	document.getElementById("tocForm").action = url;
	document.getElementById("tocForm").submit();
}