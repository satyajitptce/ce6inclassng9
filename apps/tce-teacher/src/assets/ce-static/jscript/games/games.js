/**
 *
 */
function showTopic(id, relId, dropID, relid1, url) {
	var temp = document.getElementById(id).innerHTML;
	document.getElementById('hsubjectId').value = id;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			subjectId : id
	};
	callAjax('#topicListDiv', url, data);
}

function showGames(id, relId, dropID, relid1, url) {
	var temp = document.getElementById(id).innerHTML;
	document.getElementById('htopicId').value = id;
	var subjectId = document.getElementById('hsubjectId').value;
	document.getElementById(relId).innerHTML = temp;
	document.getElementById(dropID).style.display = "none";
	var data = {
			subjectId : subjectId,
			topicId : id
	};
	callAjax('#gamesDiv', url, data);

}

function loadCpPlayer(gameContentId, mode,url) {
	var subjectId = document.getElementById('hsubjectId').value;
	var topicId = document.getElementById('htopicId').value;
	var formHandle = document.forms["_gameForm"];
	url += "&referenceId=" + gameContentId + "&mode=" + mode
	+ "&subjectId=" + subjectId + "&topicId=" + topicId+"&launchedFrom=game";
	formHandle.action = url;
	window
	.open(
			formHandle.action,
			'popUpWindow',
	'height=768,width=1024,left=10,top=10,resizable=no,scrollbars=no,toolbar=no,menubar=no,location=no,directories=no,status=no');
}


function popupDeleteGame(gameContentId, url) {

	var data = {
			gameContentId : gameContentId
	};
	Liferay.ClassedgePopup.displayPopup(url, data, "Delete Game");
}



function deleteGame(gameContentId, url) {
	var subjectId = document.getElementById('hsubjectId').value;
	var topicId = document.getElementById('htopicId').value;
	var data = {
			subjectId : subjectId,
			topicId : topicId,
			gameContentId : gameContentId
	};
	callAjax('#gamesDiv', url, data);
	Liferay.ClassedgePopup.closePopup();
}


function isSharedGame(gameContentId, url) {

	var subjectId = document.getElementById('hsubjectId').value;
	var topicId = document.getElementById('htopicId').value;
	var data = {
			subjectId : subjectId,
			topicId : topicId,
			gameContentId : gameContentId,
	};
	callAjax('#gamesDiv',url,data);

}

function isUnSharedGame(gameContentId, url) {
	var subjectId = document.getElementById('hsubjectId').value;
	var topicId = document.getElementById('htopicId').value;
	var data = {
			subjectId : subjectId,
			topicId : topicId,
			gameContentId : gameContentId,
	};
	callAjax('#gamesDiv',url,data);
}
