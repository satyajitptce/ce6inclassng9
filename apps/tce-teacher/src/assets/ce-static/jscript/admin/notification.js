function loadReportData(url,updateUrl,data) {
	var dataDiv=document.getElementById("dataDiv");
	dataDiv.innerHTML=loaderHtml;
	jQuery.getJSON(url,data,function(resData) {
		var aaData=getInventoryDataTableArr(resData,url,updateUrl);	
	    dataDiv.innerHTML ="<table id='inventoryReportDataTable' style=' margin: 0 auto; width: 100%;clear: both; border-collapse: collapse;table-layout: fixed; word-wrap:break-word;' class='display css_left'></table>";
		inventoryDataTableInit(aaData);
	});
}
function inventoryDataTableInit(aaData){		
	jQuery('#inventoryReportDataTable').dataTable({
	"bPaginate": true,
	"sPaginationType": "full_numbers",
	"bLengthChange": false,
	"bFilter": false,
	"bSort": true,
	"bInfo": false,
	"bAutoWidth": false,		
	"aaData":aaData,		
	"aoColumns": [
		{ "sTitle": "Message Id", "sWidth":"65px"},
		{ "sTitle": "Message Text", "sWidth":"255px"},
		{ "sTitle": "Message Settings", "sWidth":"255px"},
		{ "sTitle": "Role Type", "sWidth":"60px"},			
		{ "sTitle": "Message Type" , "sWidth":"60px"},
		{ "sTitle": "Terminal Type" , "sWidth":"60px"},			
		{ "sTitle": "Module Name" , "sWidth":"60px"}
		
	]
});
}
function getInventoryDataTableArr(resData,url,updateUrl) {
	var parentArr=new Array();
	for ( var i = 0; i < resData.length; i++) {
			parentArr.push(getInventoryChildData(resData[i],url,updateUrl));							
	}	
	return parentArr;
}
function getInventoryChildData(report,url,updateUrl) {
	var childArr=new Array();
	childArr.push("<a style='cursor: pointer;' onclick='javascript:updateServerMessage(\""+ updateUrl+ "\",\""+ report.messageId+ "\")'>"+report.messageId+"</a>");
	childArr.push(report.messageText.replace(/&/g, "&amp;").replace(/>/g, "&gt;").replace(/</g, "&lt;").replace(/"/g, "&quot;"));
	childArr.push(report.messageSettings);
	childArr.push(report.roleType);
	childArr.push(report.messageType);
	childArr.push(report.terminalType);
	childArr.push(report.moduleName);
	return childArr;
}



function addServerMessage(formName,urlVal){
	var messageId = document.getElementById("messageId").value;
	var messageText = document.getElementById("messageText").value;
	var messageSettings = document.getElementById("messageSettings").value;
	var roleType = document.getElementById("roleType").value;
	var messageType = document.getElementById("messageType").value;
	var terminalType = document.getElementById("terminalType").value;
	var moduleName = document.getElementById("moduleName").value;
	var fileParams = document.getElementById("fileParams").value;
	
	if (messageId == "") {
		alert("Check the messageId. \n (Can't be blank) \n");
		return;
	}
	
	if (messageText == "") {
		alert("Check the messageText. \n (Can't be blank) \n");
		return;
	}
	
	if (messageSettings == "") {
		alert("Check the messageSettings. \n (Can't be blank) \n");
		return;
	}
	
	if (roleType == "") {
		alert("Check the roleType. \n (Can't be blank) \n");
		return;
	}
	
	if (messageType == "") {
		alert("Check the messageType. \n (Can't be blank) \n");
		return;
	}
	
	if (terminalType == "") {
		alert("Check the terminalType. \n (Can't be blank) \n");
		return;
	}
	
	if (moduleName == "") {
		alert("Check the moduleName. \n (Can't be blank) \n");
		return;
	}
	
	if(messageSettings.indexOf("messageType") != -1)  {
		if (fileParams == "") {
			alert("Check the Parameter. \n (Can't be blank) \n");
			return;
		}
	}
	
	
	document.getElementById(formName).action=urlVal;
	document.getElementById(formName).submit();	
}


function updateServerMessage(urlVal,messgeId){
	document.getElementById("messageId").value=messgeId;
	document.getElementById('messageForm').action=urlVal;
	document.getElementById('messageForm').submit();	
}
