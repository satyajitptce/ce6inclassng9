////////////////function to show dialog////////////////////////////
AUI().use('aui-dialog', function(A) {

	Liferay.ClassedgePopup = {

			closePopup: function() {
				var instance = this;

				var popup = instance._getPopup();

				if (popup) {
					popup.hide();
				}
			},

			adjustPopup: function(width,height) {
				var instance = this;

				var popup = instance._getPopup();
				popup.set('width', width);
				popup.set('height', height);

			},

			displayPopup: function(url, data, title) {
				var instance = this;

				var popup = instance._getPopup();

				popup.show();

				popup.set('title', title);

				popup.io.set('uri', url);
				popup.io.set('data',data);
				popup.io.start();
			},

			_getPopup: function() {
				var instance = this;

				if (!instance._popup) {
					instance._popup = new A.Dialog(
							{
								resizable: false,
								width: 600,
								modal: true,
								centered: true
							}
					).plug(
							A.Plugin.IO,
							{autoLoad: false}
					).render();
				}

				return instance._popup;
			}
	};
});


function PopupCenter(pageURL,width,height) {
    var params = 'width='+width+', height='+height;
    params += ', directories=no';
	 params += ', location=no';
	 params += ', menubar=no';
	 params += ', resizable=yes';
	 params += ', scrollbars=yes';
	 params += ', status=no';
	 params += ', toolbar=no';
	window.open (pageURL," ",params);
}

/////////////////functions for context menu/////////////////////////
function HideContent(d) {
	if(d.length < 1) { return; }
	document.getElementById(d).style.display = "none";
}

function ShowContent(d) {
	if(d.length < 1) { return; }
	document.getElementById(d).style.display = "block";
}

///////////// button js functions ///////////////
function buttonOver(obj, textcolor) {
	obj.style.color = textcolor;
}

function buttonOut(obj, textcolor) {
	obj.style.color = textcolor;
}

////////////ajax js functions //////////////////
var loaderHtml = "<table border='0' cellpadding='0' cellspacing='0' width='310px' height='50px'>"+
"<tr><td align='center' valign='middle'>"+
"<img src='/ce-static/images/common/loader_white.gif'/>"+
"</td></tr>"+
"</table>";

var errorHtml = "<table border='0' cellpadding='0' cellspacing='0' width='310px' height='50px'>"+
"<tr><td align='center' valign='middle'> an error has occured"+
"</td></tr>"+
"</table>";

function callAjaxGet(targetDiv, url, data) {

	if(classedgeSessionTimedout==true){
		alert("Session is timedOut");
		location.href = themeDisplay.getURLHome();
		return;
	}

	var statusHtml = "<table border='0' cellpadding='0' cellspacing='0' >"+
	"<tr><td align='center' valign='middle'>"+
	"<img src='/ce-static/images/common/loader_white.gif'/>"+
	"</td></tr>"+
	"</table>";

	jQuery(targetDiv).html(statusHtml);
	jQuery.ajax({
		type : 'GET',
		url : url,
		data : data,
		success : function(content) {
			jQuery(targetDiv).html(content);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			jQuery(targetDiv).html(errorHtml); /*alert(errorThrown+"--"+textStatus);*/
		}
	});
}

function callAjax(targetDiv,url,data) {

	if(classedgeSessionTimedout==true){
		alert("Session is timedOut");
		location.href = themeDisplay.getURLHome();
		return;
	}

	jQuery(targetDiv).html(loaderHtml);

	jQuery.ajax({
		type : 'POST',
		url : url,
		data: data,
		success : function(content) {jQuery(targetDiv).html(content);},
		error : function(jqXHR, textStatus, errorThrown) {jQuery(targetDiv).html(errorHtml); /*alert(errorThrown+"--"+textStatus);*/}
	});
}

function callAjaxForFn(targetFn,url,data) {


	if(classedgeSessionTimedout==true){
		alert("Session is timedOut");
		location.href = themeDisplay.getURLHome();
		return;
	}

	jQuery.ajax({
		type : 'POST',
		url : url,
		data: data,
		success : function(content) {targetFn(content);},
		error : function(jqXHR, textStatus, errorThrown) {targetFn("error"); /*alert(errorThrown+"--"+textStatus);*/}
	});

}

function loadJsonDataIntoDropDown(urlVal,targetFunction,fieldVal) {
	if(classedgeSessionTimedout==true){
		alert("Session is timedOut");
		location.href = themeDisplay.getURLHome();
		return;
	}
	if (fieldVal == '') {
		alert("select filter type");
		return;
	}
	var data={filterTypeValue : fieldVal};
	jQuery.getJSON(urlVal,data,function(data) {
		targetFunction(data);
	});
}


function LoadDataIntoDropDown(urlVal,data,property) {

	this.baseUrl = urlVal;
	this.data = data;
	this.property = property; //{addselect : true/false, validateFilter : true/false, async : true/false}

	this.populate =function (id) {
		if(classedgeSessionTimedout==true){
			alert("Session is timedOut");
			location.href = themeDisplay.getURLHome();
			return;
		}



		var adddefaultSelect = this.property.addselect;
		var adddefaultFilter = this.property.validateFilter;
		var async = true;
		if(this.property.hasOwnProperty("async")){
			async = this.property.async;
		}
		jQuery.ajax({
			type : 'POST',
			url : this.baseUrl,
			data: data,
			async: async,
			success : function(retdata) {
							var bookDropDown = jQuery(id);
							bookDropDown.get(0).options.length = 0;
							var count = 0;
							if(adddefaultFilter==true&&data.filterTypeValue==''){
								alert("select filter type");
								return;
							}
							if(adddefaultSelect == true) {
								bookDropDown.get(0).options[count] = new Option("select","select");
								count++;
							}

							if (retdata.length === 0) {
								  bookDropDown.css("visibility", "visible");
							} else {
								jQuery.each(retdata, function(index, item) {
								    bookDropDown.get(0).options[count] = new Option(item.title,item.id);
									count++;
									bookDropDown.css("visibility", "visible");
								});
							}
						}
		});
	};
}





///////////////////// version history js functions ////////////////////////////
function getQueryVariable(variable) {
	var query = window.location.search.substring(1);
	var vars = query.split("&");
	for (var i=0;i<vars.length;i++) {
		var pair = vars[i].split("=");
		if (pair[0] == variable) {
			return pair[1];
		}
	}
	alert('Query Variable ' + variable + ' not found');
}



function VersionHandler(url) {
	this.baseUrl = url;
	this.namespace = "versionMgmtHandle";
	this.submitFnHandle = null;
	if(typeof(submitComments) == "function"){this.submitFnHandle =submitComments;};
	this.validateFn = null;
	if(typeof(validateForm) == "function"){this.validateFn =validateForm;};

	this.setCallbackFn=function(submitFn,validateFn) {
		this.submitFnHandle = submitFn;
		this.validateFn = validateFn;
	};

	this.setNamespace = function(namespace){
		this.namespace = namespace;
	};

	this.addVersionComment =function (formName,submitUrl) {
		var flag = this.validateFn(formName);
		if (flag == true) {
			var data = {
					submitUrl : submitUrl,
					formName : formName,
					namespace : this.namespace
			};
			Liferay.ClassedgePopup.displayPopup(this.baseUrl,data, "Add Comments");
		}
	};

	this.viewVersionHistory = function (assetId,moduleId){
		var data = {
				assetId : assetId,
				moduleId : moduleId
		};
		Liferay.ClassedgePopup.displayPopup(this.baseUrl,data,"Version History");
	};

	this.submitVersionComments = function(formName,submitUrl){
		var formElement = document.getElementById(formName);
		var comments = document.getElementById("versionComments").value;
		if(comments==""){
			alert("Please add your comments");
			return;
		}
		var inputElement = document.createElement("input");
	   inputElement.setAttributeNode(createHtmlAttribute("type", "hidden"));
	   inputElement.setAttributeNode(createHtmlAttribute("name", "comments"));
	   inputElement.setAttributeNode(createHtmlAttribute("value", comments));
	   formElement.appendChild(inputElement);
	   return this.submitFnHandle(formName,submitUrl);
	};

	this.showDetails = function(versionId){
		callAjax("#versionContent",this.baseUrl+"&Operation=viewVersionDetails&verionId="+versionId,null);
	};

	this.showVersions = function(){
		callAjax("#versionContent",this.baseUrl+"&Operation=viewVersionHistoryTable",null);
	};
}


function createHtmlAttribute(name, value) {
   var attribute = document.createAttribute(name);
   attribute.nodeValue = value;
   return attribute;
}


///////////////////// dynamic file upload js functions //////////////////////////
function uploadFile(divName) {

	if(document.getElementById("hidden_form").action == ''){
		alert('URL has not been set');
		return;
	}

	var theFile = document.getElementById("lpFile");
	var fileParent = theFile.parentNode;
	// create a div with a hidden iframe and form

	//This function should be called when the iframe has compleated loading
	document.getElementById("hidden_frame").onload = uploadDone;
	// That will happen when the file is completely uploaded and the server has returned the data we need.


	var hiddenForm = document.getElementById("hidden_form");
	fileParent.removeChild(theFile);
	// attach the file to the hidden form
	hiddenForm.appendChild(theFile);
	hiddenForm.submit();
	hiddenForm.removeChild(theFile);
	// put th file back where it came from
	fileParent.appendChild(theFile);
	// can't remove div yet, frame may not have loaded
	//document.body.removeChild(theDiv);
	document.getElementById("lpFile").value="";
}

function uploadDone() { //Function will be called when iframe is loaded
	var ret = frames['hidden_frame'].document.getElementsByTagName("body")[0].innerHTML;
	var theDiv = document.getElementById("uploadedDiv");
	theDiv.innerHTML = ret;
}


function download(downloadURL,data) {
	var settings = {
		width : 400,
		height : 100,
		title : "Download File"
	};
	downloadDialog(downloadURL,data,settings);
}

function downloadDialog(downloadURL,data,settings) {
	  $.modal("<div id='basic-modal-download'><div id='downloaddialog' title='"+settings.title+"'></div></div>");
	  $('#downloaddialog').dialog({
			autoOpen: false,
			width: settings.width,
			height: settings.height,
			modal: true,
			resizable: false,
			close: function (e) {
				$.modal.close();
				$('#downloaddialog').dialog( "close" );
			}
		});
	  $('#downloaddialog').dialog('open');
	  callAjax('#downloaddialog', downloadURL, data);
}

function showRollOverMenu(id,title,menuOptions){

	var header = '<div onmouseout="HideContent(\''+id+'uniquename\');" onmouseover="ShowContent(\''+id+'uniquename\'); return true;"> '+
				'<span style="FONT-FAMILY: Arial; font-size: 12px; font-weight: Regular; color: #000000;">'+title+'</span>'+
				'<div id="'+id+'uniquename" class="ce-popupmenu"><ul>';
	var menuItems = '';
	for(var i=0;i<menuOptions.length;i++){
		menuItems = menuItems + '<li><a href="'+menuOptions[i][1]+'" style="text-decoration: none;"><b>'+menuOptions[i][0]+'</b> </a></li>';
	}
	var footer = '</ul></div></div>';
	return header + menuItems + footer;

}

//extending the session and closing the warning message
function extendSession(){
try { if ('noWarning' == mySessionRenewAtWarning()) { mySessionRenew(); } } catch(e) {}
}

//Closes the warning by invoking the click event of extend button...
function mySessionRenewAtWarning(){
    var jqryWarn = new jQuery;
    var warnElm = jqryWarn.find('.popup-alert-notice');
    if (warnElm.length > 0) {
        // Match content of warning with session expiration warning language
        if (Liferay.Language.get('warning-your-session-will-expire').indexOf(warnElm[0].firstChild.nodeValue) == 0) {
            // click the extend button
            warnElm.find('.popup-alert-close').click();
            return true;
        }
    }
    return "noWarning";
}

// Extends the session and resets the client timer
function mySessionRenew() {
    if (Liferay.Session._stateCheck) {
        window.clearTimeout(Liferay.Session._stateCheck);
        Liferay.Session._stateCheck = null;
    }
    Liferay.Session.extend();
}

function fsetvaluewlen(id, relId, dropID, relid1,len){

	 var temp = document.getElementById(id).innerHTML;

	 if(temp.length>len){
	  temp= temp.substring( 0,len-2 ) .concat("..");
	 }
	 document.getElementById(relId).innerHTML = temp;
	 document.getElementById(dropID).style.display = "none";

}


//filter functions
function initializeFilter(selectedType,selectedValue) {
	if (selectedType != null){
		var selected = false;
		var selectedTypeValue = '';
		for(var i = 0;i < document.getElementById("filterType").length;i++){
	        if(document.getElementById("filterType").options[i].value.indexOf(selectedType) != -1){
	            document.getElementById("filterType").selectedIndex  = i;
	            selectedTypeValue = document.getElementById("filterType").options[i].value;
	            selected = true;
	            break;
	        }
	    }
		if(selected == false) {
			document.getElementById("filterType").selectedIndex  = 0;
		} else {
			$.when(changeFilter(selectedTypeValue)).done(function(){
				if(selectedTypeValue.indexOf('tt') == 0){
	            	document.getElementById('filterText').value=selectedValue;
	            } else if(selectedTypeValue.indexOf('dd') == 0){
	            	for(var i = 0;i < document.getElementById("filterDD").length;i++){
	            		 if(document.getElementById("filterDD").options[i].value == selectedValue){
	         	            document.getElementById("filterDD").selectedIndex  = i;
	         	            break;
	            		 }
	            	}
	            }
			});
		}
	}
}

function changeFilter(selValue) {
	var ddDiv =document.getElementById("ddDiv");
	var ttDiv =document.getElementById("ttDiv");
	var valParts = selValue.split('~');
	if(valParts[0]=='dd'){
		var data={filterTypeValue : valParts[2]	};
		var filterId=document.getElementById('filterDD');
		filterId.options.length=0;
		new LoadDataIntoDropDown(window[valParts[1]](),data,{addselect:true,validateFilter:true,async:false}).populate(filterId);
		ddDiv.style.display ='inline-block';
		ttDiv.style.display ='none';
	} else if(valParts[0]=='tt'){
		document.getElementById('filterText').value="";
		ddDiv.style.display ='none';
		ttDiv.style.display ='inline-block';
	} else if(valParts[0]=='all'){
		ddDiv.style.display ='none';
		ttDiv.style.display ='none';
	}

	return;
}

function validateFilter() {
	var valParts = document.getElementById("filterType").value.split('~');
	var filterId = '';
	if(valParts[0]=='dd'){
		filterId = document.getElementById('filterDD').value;
		if (filterId == 'empty'|| filterId=='select') {
			alert("Please select  filter value");
			return false;
		}
	} else if(valParts[0]=='tt'){
		filterId = document.getElementById('filterText').value;
		if (filterId == '') {
			alert("Filtered value can't be blank");
			return false;
		}

	} else {
		filterId = valParts[2];
	}

	document.getElementById('filterTypeId').value = valParts[2];
	document.getElementById('filterValueId').value = filterId;
	
	return true;
}


function retrieve(elementName){
	if(document.getElementById(elementName) != null) {
		return document.getElementById(elementName).value;
	}
	return "";
}

function retrieveForRadio(elementName){
	if(document.getElementsByName(elementName) != null) {
		var radioValue = $("input[name="+elementName+"]:checked").val();
		if(typeof radioValue !== "undefined" ) {
			return radioValue;
		}
	}
	//this is a workaround for firstime loading of the page as it seems the radio is not set
	if(document.getElementById("table"+elementName) != null) {
		return document.getElementById("table"+elementName).value;
	}
	//if no footer is there
	return "";
}

function getOperationFromURL(url,variable) {
	if(url.indexOf('/-/') > -1) {
		var query = url.split("/-/")[1];
		var vars = query.split("/");
		if(vars.length >= 3){
			if(vars[2].indexOf('?') > -1) {
				return vars[2].split("?")[0];
			} else {
				return vars[2];
			}
		}
	} else if(url.indexOf('?') > -1) {
		var query = url.split("?")[1];
		var vars = query.split("&");
		for (var i=0;i<vars.length;i++) {
			var pair = vars[i].split("=");
			if (pair[0].indexOf(variable) > -1) {
				return pair[1];
			}
		}
	}

	alert('Query Variable ' + variable + ' not found');
}

function loadReportData(url,columnDefs,extraProps){
	var tableProps = {tableId : "dTable", paginate : true, noOfRows : 15, serverside : true, showHeader : true, paginationType : "full_numbers"};
	try {
		if (Object.prototype.toString.call(extraProps) === '[object Object]') {
			jQuery.extend(tableProps, extraProps);
		} else {
			tableProps.paginate = extraProps;
		}
	} catch (e) {
		//this may happen in lower version
		tableProps.paginate = extraProps;
	}
	var tableId = tableProps.tableId;
	var paginate = tableProps.paginate;
	var serverside = tableProps.serverside;
	var noOfRows = tableProps.noOfRows;
	var paginationType = tableProps.paginationType;
	var operation = getOperationFromURL(url,"Operation");
	console.log(tableId+"~~"+paginate +"~~"+noOfRows +"--"+serverside +"--"+tableProps.showHeader +"--"+paginationType +"--"+operation);

    return $('#'+tableId).dataTable({
							"bSortable" : true,
							"aaSorting" : [ [ 0, 'asc' ]],
							"bPaginate" : paginate,
							"bLengthChange" : false,
					        "bFilter" : false,
					        "bInfo" : true,
					        "bAutoWidth" : true,
					        "bStateSave" : true,
					        "bProcessing" : true,
					        "bServerSide" : serverside,
					        "iDisplayLength" : noOfRows,
					        "sPaginationType" : paginationType,
					        "sAjaxSource" : url,
					        "fnServerParams": function ( aoData ) {

							       	 var sortIndex = -1;
							       	 var sortColumn = "";
							       	 for(var i = 0; i < aoData.length; i++){
								        	 if(aoData[i].name == "iSortCol_0"){
								        		 sortIndex = aoData[i].value;
								        	 }
							       	 }

							       	 if(sortIndex != -1){
							       		 for(var i = 0; i < aoData.length; i++){
									        	 if(aoData[i].name == "mDataProp_"+sortIndex){
									        		 sortColumn = aoData[i].value;
									        	 }
								        	 }

							       		 aoData.push( { "name": "sortColumn", "value": sortColumn });
							       	 }
								     aoData.push(
									        { "name": "filterName", "value":  retrieve('filterTypeId') },
									        { "name": "filterValue", "value": retrieve('filterValueId') },
									        { "name": "status", "value": retrieveForRadio('status') }
									  );

					        },
					        "fnDrawCallback": function() {
						        	if(tableProps.showHeader == false){
						            	$("#dTable thead").remove();
						        	}
					        },
					        "fnStateSave": function (oSettings, oData) {
					        	sessionStorage.setItem( 'DataTables_'+operation, JSON.stringify(oData) );
					        },
					        "fnStateLoad": function (oSettings) {
					        	return JSON.parse( sessionStorage.getItem('DataTables_'+operation) );
					        },
					        "aoColumnDefs" : columnDefs
	 			});
}


function commonAttachFile(divId,linkId){
	var mainDiv=document.getElementById(divId);
	var testFile =document.getElementById(divId+"File");
	if(testFile==null){
		var newdiv = document.createElement('div');
		newdiv.setAttribute("id",divId+'child');
		newdiv.innerHTML = "<input type=\"file\" name=\""+divId+"File"+"\" id=\""+divId+"File"+"\" size=\"20\"/> <input type=\"button\" value=\"X\" onclick=\" commonDeleteKeyword(\'"
				+ divId + "\',\'" + divId+'child'+ "\',\'" +linkId+ "\')\">";
		mainDiv.appendChild(newdiv);
		var linkDiv=document.getElementById(linkId);
		linkDiv.style.display='none';
	}
}

function commonDeleteKeyword(id,divno,linkid){
	var d = document.getElementById(id);
	var olddiv = document.getElementById(divno);
	d.removeChild(olddiv);
	var linkDiv=document.getElementById(linkid);
	linkDiv.style.display='block';
}

