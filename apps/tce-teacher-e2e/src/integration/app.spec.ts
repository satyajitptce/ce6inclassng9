import { getGreeting } from '../support/app.po';

describe('tce-teacher', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to tce-teacher!');
  });
});
