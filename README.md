# CE6InClass

This project was generated using [Nx](https://nx.dev).

## Quick Start & Documentation

## [Angular 8 - 9 upgrade](https://update.angular.io/)

## Angular CLI: 9.1.0-next.2

## Node: 10.13.0

- @angular-devkit/architect ---> 0.900.5
- @angular-devkit/build-angular ---> 0.901.0-next.2
- @angular-devkit/build-optimizer ---> 0.901.0-next.2
- @angular-devkit/build-webpack ---> 0.901.0-next.2
- @angular-devkit/core ---> 9.0.5
- @angular-devkit/schematics ---> 9.0.5
- @angular/cdk ---> 9.1.1
- @angular/cli ---> 9.1.0-next.2
- @ngtools/webpack ---> 9.1.0-next.2
- @schematics/angular ---> 9.0.5
- @schematics/update ---> 0.901.0-next.2
- rxjs ---> 6.5.4
- typescript ---> 3.7.5
- webpack ---> 4.42.0

## BUILD

Run `npm run build:prod-teacher071` to generate prod build.
