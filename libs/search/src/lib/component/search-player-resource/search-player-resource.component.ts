import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  ElementRef,
  ComponentRef
} from '@angular/core';
import { PanZoomService, PlayerAbstractComponent, Resource } from '@tce/core';
import { Subscription } from 'rxjs';
import { PlayerContainerService } from '@app-teacher/services';
import { PLAYER_OFFSET_TOP } from '../../../../../../apps/tce-teacher/src/app/shared/shared.constants';

@Component({
  selector: 'tce-search-player-resource',
  templateUrl: './search-player-resource.component.html',
  styleUrls: ['./search-player-resource.component.scss']
})
export class SearchPlayerResourceComponent implements OnInit {
  @ViewChild('playerRef', { static: true, read: ViewContainerRef })
  playerViewRef: ViewContainerRef;

  playerFactory;

  playerComponentRef: ComponentRef<PlayerAbstractComponent>;
  playerCloseSubscriber: Subscription;

  associatedResource: Resource;
  currentZoomScale = 1;

  subscriptions: Subscription = new Subscription();

  isPlayerResourceDrawerOpen: boolean;

  isPointerDisabled = false;
  focusMode = false;
  currentTool = null;
  currentActiveResourceId;
  constructor(
    private panZoomService: PanZoomService,
    private eleRef: ElementRef,
    private playerContainerService: PlayerContainerService
  ) {}

  ngOnInit() {}
  ngOnDestroy() {}
  moveToPlayer() {
    const {
      top,
      left,
      height,
      width
    } = this.eleRef.nativeElement.getBoundingClientRect();
    const playerMidX = left + width / 2;
    const playerMidY = top + height / 2;
    const middleOfWindow = {
      x: window.innerWidth / 2,
      y: window.innerHeight / 2
    };
    // Offset the player to be closer to the top of the screen.
    // 70px is subtracted as a buffer to allow for the logo to be above the players.
    // height / this.currentZoomScale gets the player's height at a zoom level of 1.
    // We then multiply by the zoom scale to scale the distance of the player from the middle of the screen.
    const playerOffset = Math.round(
      ((window.innerHeight - height / this.currentZoomScale) / 2 -
        PLAYER_OFFSET_TOP) *
        this.currentZoomScale
    );
    const panToPoint = {
      x: middleOfWindow.x - playerMidX,
      y: middleOfWindow.y - playerMidY - playerOffset
    };
    // 1. Pan center of player to center of screen.
    this.panZoomService.panToPoint(panToPoint);
    // 2. Zoom center of screen.
    // Zoom to center of screen needs to account for animation time.
    // This delay based on animation time should be a type variable.
    setTimeout(() => {
      this.panZoomService.setZoomToPointAndScale(middleOfWindow, 1);
    }, 10);
  }

  setPlayerResource() {
    if (this.playerComponentRef && this.associatedResource) {
      this.playerComponentRef.instance.resource = this.associatedResource;
    }
  }

  openPlayer() {
    if (!this.playerComponentRef) {
      this.playerComponentRef = this.playerViewRef.createComponent(
        this.playerFactory
      );
    }

    this.setPlayerResource();
    this.setPlayerCloseListener();
    this.isPointerDisabled = false;
    this.moveToPlayer();
  }
  setAssociatedResource(resource: Resource) {
    this.associatedResource = resource;
  }
  setPlayerCloseListener() {
    if (this.playerComponentRef) {
      this.playerCloseSubscriber = this.playerComponentRef.instance.playerCloseEmitter.subscribe(
        (nextResourceToOpen: Resource) => {
          this.playerContainerService.closeResource(this.associatedResource);
          if (nextResourceToOpen) {
            this.playerContainerService.openResource(nextResourceToOpen);
          }
        }
      );
      this.playerComponentRef.onDestroy(() => {
        this.playerCloseSubscriber.unsubscribe();
      });
    }
  }
  onLoadPlayerFactory(componentFactory, openPlayer: boolean) {
    if (!this.playerFactory) {
      this.playerFactory = componentFactory;
      if (openPlayer) {
        this.playerComponentRef = this.playerViewRef.createComponent(
          this.playerFactory
        );
      }
      this.setPlayerCloseListener();
      this.setPlayerResource();
      this.isPointerDisabled = !openPlayer;
    }
  }
}
