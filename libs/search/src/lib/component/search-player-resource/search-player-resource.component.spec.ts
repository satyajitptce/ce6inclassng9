import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPlayerResourceComponent } from './search-player-resource.component';

describe('SearchPlayerResourceComponent', () => {
  let component: SearchPlayerResourceComponent;
  let fixture: ComponentFixture<SearchPlayerResourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchPlayerResourceComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPlayerResourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
