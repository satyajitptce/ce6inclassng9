import {
  Component,
  OnInit,
  OnDestroy,
  AfterViewInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactoryResolver,
  ElementRef,
  HostBinding,
  Input
} from '@angular/core';
import { Subscription } from 'rxjs';
import { SearchPlayerResourceComponent } from '../search-player-resource/search-player-resource.component';
import { LibConfigService } from '@tce/lib-config';
import {
  Resource,
  AppConfigService,
  CommonService,
  CurriculumPlaylistService,
  KeyboardService,
  KeyboardState,
  KeyboardTheme,
  ToolbarService
} from '@tce/core';
import {
  FormControl,
  FormGroup,
  Validators,
  ValidatorFn,
  AbstractControl
} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ContentEditorService } from '@tce/content-library';
import { SearchFilterService } from '../../service/search-filter.service';
@Component({
  selector: 'tce-search-player-container',
  templateUrl: './search-player-container.component.html',
  styleUrls: ['./search-player-container.component.scss']
})
export class SearchPlayerContainerComponent implements OnInit, AfterViewInit {
  _searchresource: any;
  _planningAddResource: any;
  @Input('planningAddResource')
  set planningAddResource(val) {
    this._planningAddResource = val;
    //console.log('searchContainer', val);
  }

  get planningAddResource() {
    return this._planningAddResource;
  }
  @Input() currentPlanningResource;
  @Input() currentType;
  @Input('searchresource')
  set searchresource(data: any) {
    this._searchresource = data;
    if (this.playerResourceRef && this.playerOutlet) {
      //console.log('---if---');
      this.playerOutlet.clear();
      this.playerResourceRef.destroy();
      this.loadSearchResourceList(this.searchresource, true);
    } else {
      //console.log('---else---');
      this.loadSearchResourceList(this.searchresource, true);
    }
  }
  get searchresource() {
    return this._searchresource;
  }
  addToPlaylistResource: any;
  currentSelectedTopic: any;
  currentResources: any;
  showFav: any;
  @ViewChild('playerOutlet', { static: true, read: ViewContainerRef })
  playerOutlet: ViewContainerRef | undefined;

  @ViewChild('innerPlayerContainer', { static: true, read: ElementRef })
  innerPlayerContainer: ElementRef | undefined;

  @ViewChild('playerResource', { static: true, read: ViewContainerRef })
  playerResource: ViewContainerRef | undefined;
  playerResourceRef: ComponentRef<any> | null = null;
  subscriptions: Subscription = new Subscription();

  currentScale = 1;
  currentActiveResourceId: string;
  repositionAfterResizeTimeout;
  FavName: any;
  showBucket: any = false;
  isPointerDisabled = false;
  showBucketOptionsForm: FormGroup;
  myFavBucket = {
    title: ''
  };
  newResourceList: any;
  currentTopicId: any;
  @HostBinding('class.-disable-pointer-events') get disablePointerEvents() {
    return this.isPointerDisabled;
  }
  constructor(
    private libConfigService: LibConfigService,
    private factoryResolver: ComponentFactoryResolver,
    private appConfigService: AppConfigService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private keyboardService: KeyboardService,
    private toastrService: ToastrService,
    private toolbarService: ToolbarService,
    private commonService: CommonService,
    private contentEditorService: ContentEditorService,
    private searchFilterService: SearchFilterService
  ) {}

  ngOnInit() {
    this.curriculumPlaylistService.topicSelection$.subscribe(currentTopic => {
      if (currentTopic) {
        this.currentTopicId = currentTopic.topicId;
        this.currentSelectedTopic = currentTopic;
      }
    });

    this.curriculumPlaylistService.availableResources$.subscribe(
      (resources: Resource[]) => {
        let custResource = [];
        //("resources",resources)
        if (resources && resources.length > 0) {
          resources.forEach(element => {
            if (element.visibility && element.show) {
              custResource.push(element);
            }
          });
          this.currentResources = custResource;
        } else {
          this.currentResources = custResource;
        }
      }
    );

    this.FavName = [
      {
        name: 'Favorite Bucket 1'
      },
      {
        name: 'Favorite Bucket 2'
      },
      {
        name: 'Favorite Bucket 3'
      },
      {
        name: 'Favorite Bucket 4'
      }
    ];

    this.showBucketOptionsForm = new FormGroup({
      title: new FormControl(this.myFavBucket.title, Validators.required)
    });
  }
  ngAfterViewInit() {}
  loadSearchResourceList<T>(resource, openPlayer) {
    
    const playerResourceFactory = this.factoryResolver.resolveComponentFactory(
      SearchPlayerResourceComponent
    );
    this.playerResourceRef = this.playerResource.createComponent(
      playerResourceFactory
    );
    resource.setPlayerResourceRef(this.playerResourceRef);
    const playerResourceInstance = this.playerResourceRef.instance;
    playerResourceInstance.setAssociatedResource(resource);
    this.libConfigService
      .getComponentFactory<T>('player-' + resource.resourceType)
      .subscribe({
        next: componentFactory => {
          if (!this.playerOutlet) {
            return null;
          }
          playerResourceInstance.onLoadPlayerFactory(
            componentFactory,
            openPlayer
          );
          this.playerResourceRef.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn('err on componentload', err);
        }
      });
  }

  getResourceThumbnailIcon(resource: Resource, type: string) {
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }

  addAndOpenResourceToPlaylist(resource: any, type) {
    this.addToPlaylistResource = [];
    //console.log('addToPlayList', resource);
    if (resource.playerResourceRef) {
      //console.log('if ');
      delete resource.playerResourceRef;
      this.addToPlaylistResource.push(resource);
    } else {
      //console.log('else');
      this.addToPlaylistResource.push(resource);
    }
    if (this.addToPlaylistResource && this.addToPlaylistResource.length > 0) {
      //console.log('second if', this.addToPlaylistResource);
      this.createNewPlaylist(this.addToPlaylistResource, type);
    }
  }

  createNewPlaylist(data: any, type) {
    this.searchFilterService.closeSelectedResourceState.next(true);
    let concatArray: any;
    //console.log('current resources', this.currentResources);
    if (this.currentResources && this.currentResources.length > 0) {
      let filterResource = this.currentResources.filter(
        t => !!data.find(t1 => t.resourceId === t1.resourceId)
      );
      if (filterResource && filterResource.length > 0) {
        this.toastrService.error('same resource already exist in playlist');
        if (type === 'whiteboard') {
          this.commonService.setSearchResource(data[0]);
        }
      } else {
        concatArray = this.currentResources.concat(data);
      //console.log("createNewPlaylist -> filterResource 1st if --->else" , filterResource)

      }
    } else {
      concatArray = this.currentResources.concat(data);
      //console.log("createNewPlaylist -> filterResource else")

    }
    if (concatArray && concatArray.length > 0) {
     // console.log('concatArray================================>', concatArray);
      this.createJsonPlaylist(concatArray, type, data);
      this.toastrService.success('Resource Added successfully')
    }
    //console.log(data)
  }

  createJsonPlaylist(resourcelist, type, selectedData) {
    //console.log('createJsonPlaylist---->resourcelist', resourcelist);
    //console.log('type', type);
    //console.log('selectedData', selectedData);

    this.newResourceList = {};
    let newCollectionAssets: any = [];
    let newCollectionPractice: any;
    let newCollectionGallery: any;
    let currentResourceId = this.currentTopicId;
    let newCreationDate = Math.round(new Date().getTime() / 1000);
    let playlistJson = {
      creationDate: newCreationDate,
      asset: newCollectionAssets,
      gallery: newCollectionGallery,
      practice: newCollectionPractice
    };

    for (let index = 0; index < resourcelist.length; index++) {
      if (resourcelist[index].tcetype === 'quiz') {
        //console.log('if------------------------quiz');
        newCollectionAssets.push({ assets_type: 'practice' });
        newCollectionPractice = resourcelist[index].metaData.questionIds;
        playlistJson.practice = newCollectionPractice;
      }
      if (resourcelist[index].tcetype === 'gallery') {
        //console.log('if------------------------gallery');
        newCollectionAssets.push({ assets_type: 'gallery' });
        newCollectionGallery = resourcelist[index].metaData;
        playlistJson.gallery = newCollectionGallery;
      }
      if (
        resourcelist[index].tcetype !== 'quiz' &&
        resourcelist[index].tcetype !== 'gallery'
      ) {
        if (!currentResourceId) {
          //console.log('if------------------------currentResourceId');
          currentResourceId = resourcelist[index].tpId;
        }
        newCollectionAssets.push(resourcelist[index].metaData);
      }
    }
    if (playlistJson) {
      //console.log('createJsonPlaylist -> playlistJson', playlistJson);
      let string = JSON.stringify(playlistJson, function(k, v) {
        if (v !== undefined) {
          //console.log('k', k);
          return v;
        } else {
          //console.log('else v', k, v);
        }
      });
      //let string = playlistJson;
      //console.log('string', string);
      this.newResourceList = {
        id: currentResourceId,
        playlistJson: string,
        currentResource: resourcelist,
        type: 'search'
      };
      if (this.newResourceList.id) {
        
        //this.currentSelectedTopic.resources = resourcelist;
        this.commonService.setupdateResourcelist(this.newResourceList);
        // this.curriculumPlaylistService.setTopicSelection(
        //   this.currentSelectedTopic
        // );
        if (type === 'whiteboard') {
          //console.log(type);
          this.commonService.setSearchResource(selectedData[0]);
        }
      }
    }
  }

  openFavOption() {
    this.showBucket = false;
    this.showFav = !this.showFav;
  }

  openNewBucket() {
    this.showBucket = true;
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  updateFieldValue(Name: string, val) {
    this.showBucketOptionsForm.controls[Name].setValue(val);
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.DEFAULT
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }

  onSubmit() {
    this.showBucket = false;
    this.closeKeyboard();
  }

  closeBucket() {
    this.showBucket = false;
    this.closeKeyboard();
  }

  addResourcePlanning(resource: any) {
    const collectionData = [];
    let filterCurrentResource: any;
    if (resource.playerResourceRef) {
      delete resource.playerResourceRef;
      collectionData.push(resource);
    } else {
      collectionData.push(resource);
    }
    if (
      this.currentPlanningResource &&
      this.currentPlanningResource.length > 0
    ) {
      filterCurrentResource = this.currentResources.filter(
        t => !!collectionData.find(t1 => t.resourceId === t1.resourceId)
      );
      if (filterCurrentResource && filterCurrentResource.length > 0) {
        this.toastrService.error('same resource already exist in playlist');
      } else {
        //console.log('step1', this.planningAddResource);
        this.addResourcePlanningMode(collectionData, this.planningAddResource);
      }
    } else {
      if (collectionData && collectionData.length > 0) {
        this.addResourcePlanningMode(collectionData, this.planningAddResource);
      }
    }
  }

  addResourcePlanningMode(collectionData, planningAddResource) {
    this.searchFilterService.closeSelectedResourceState.next(true);
    // console.log("addResourcePlanningMode")
    let filterResource: any;
    let myArayFilter = [];
    if (planningAddResource && planningAddResource.length > 0) {
      for (let index = 0; index < planningAddResource.length; index++) {
        filterResource = planningAddResource[index].selectedResourceList.filter(
          t => !!collectionData.find(t1 => t.resourceId === t1.resourceId)
        );
        if (filterResource && filterResource.length > 0) {
          myArayFilter.push(filterResource);
        }
      }
      //console.log('myArayFilter', myArayFilter);
      if (myArayFilter && myArayFilter.length > 0) {
        this.toastrService.error('same resource already exist in playlist');
      } else {
        //console.log('step3');
        this.contentEditorService.setSelectedResourceData(collectionData);
      }
    } else {
      //console.log('step4');
      this.contentEditorService.setSelectedResourceData(collectionData);
    }
  }

  closeModal(){
    this.searchFilterService.setClosePreview('close')
    //this.searchresource.emit(null)
    //this.currentResource.emit(null);
  }
}
