import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPlayerContainerComponent } from './search-player-container.component';

describe('SearchPlayerContainerComponent', () => {
  let component: SearchPlayerContainerComponent;
  let fixture: ComponentFixture<SearchPlayerContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchPlayerContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPlayerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
