import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchMyfavoriteComponent } from './search-myfavorite.component';

describe('SearchMyfavoriteComponent', () => {
  let component: SearchMyfavoriteComponent;
  let fixture: ComponentFixture<SearchMyfavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchMyfavoriteComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchMyfavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
