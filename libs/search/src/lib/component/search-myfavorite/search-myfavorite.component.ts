import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tce-search-myfavorite',
  templateUrl: './search-myfavorite.component.html',
  styleUrls: ['./search-myfavorite.component.scss']
})
export class SearchMyfavoriteComponent implements OnInit {
  myFavoritesList: any;
  constructor() {}

  ngOnInit(): void {
    this.myFavoritesList = [
      {
        title: 'Favorites bucket 1',
        resource: [
          {
            resourceName: 'resource1'
          },
          {
            resourceName: 'resource2'
          }
        ]
      }
    ];
  }
}
