import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  Input,
  ChangeDetectorRef
} from '@angular/core';
import { KeyboardService, KeyboardState, KeyboardTheme } from '@tce/core';
import { SearchFilterService } from '../../service/search-filter.service';
import { ContentEditorService } from '@tce/content-library';
@Component({
  selector: 'tce-search-header',
  templateUrl: './search-header.component.html',
  styleUrls: ['./search-header.component.scss']
})
export class SearchHeaderComponent implements OnInit {
  showEditFavorites: any = false;
  hideCancelFavorites: any = false;
  _selectedResourceList: any = [];
  placeholderName = 'Search Resource(s)';
  @Input() currentPlanningResourceState;
  @Input('selectedResourceList')
  set selectedResourceList(val) {
    //console.log('SearchHeaderComponent', val);
    this._selectedResourceList = val;
  }
  get selectedResourceList() {
    return this._selectedResourceList;
  }
  @Input() currentSateView;
  @Input() currentType;
  @Input('currentUserText')
  set currentUserText(text: any) {
    if (text) {
      this.inputText = text;
    }
  }
  inputText: any;
  @Output() inputTextEmit = new EventEmitter<string>();
  @Output() inputClick = new EventEmitter<boolean>(false);

   
  @Output() currentViewState = new EventEmitter<string>();
  @Input() closeSearchSuggestion;
  filterList: any;
  searchSuggestion: any;
  showSuggestionPopup: any;
  fiterviewState = true;
  constructor(
    private keyboardService: KeyboardService,
    private searchFilterService: SearchFilterService,
    private contentEditorService: ContentEditorService,
    private cdr: ChangeDetectorRef
  ) {}

  ngOnInit(): void {
    this.showSuggestionPopup = this.closeSearchSuggestion;
    //console.log("closeSearchSuggestion",this.closeSearchSuggestion)
    this.filterList = [
      {
        id: 0,
        name: 'Resources'
      },
      {
        id: 1,
        name: 'Filter1'
      },
      {
        id: 2,
        name: 'Filter2'
      },
      {
        id: 3,
        name: 'Filter3'
      },
      {
        id: 4,
        name: 'Filter4'
      }
    ];
  }
  onKeyUp(e: any) {
    // console.log(
    //   'SearchInputComponent -> onKeyUp -> e.target.value',
    //   e.target.value
    // );
    if (this.currentSateView == 'favoritesView') {
    } else {
      if (this.fiterviewState) {
        this.currentViewState.emit('filterView');
        this.fiterviewState = false;
      }

      this.getInputText(e.target.value);
    }
  }
  setKeyboardFocus(e: FocusEvent) {
    this.inputClick.emit(true);
    const obj = {
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.DEFAULT
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }

  getInputText(text: any) {
    let getText: any;
    if (text) {
      getText = text.length;
    }
    if (getText >= 3) {
      this.showSuggestionPopup = true;
      this.getSuggetionResult(text);
    } else {
      this.showSuggestionPopup = false;
    }
    //console.log('SearchViewComponent -> getInputText -> text', text);
    if (this.currentSateView !== 'filterView') {
      this.currentSateView = 'filterView';
    }
    this.inputText = text;
    //this.inputTextEmit.emit(this.inputText);
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  setCurrentView(state: string) {
    if (this.currentType === 'teachingMode') {
      this.fiterviewState = true;
      this.showSuggestionPopup = false;
      this.searchSuggestion = [];
      if (state == 'favoritesView') {
        this.showEditFavorites = true;
        this.placeholderName = 'Search Favorites';
      } else {
        this.placeholderName = 'Search Resource(s)';
        this.showEditFavorites = false;
      }
      this.inputText = '';
      this.currentSateView = state;
      this.currentViewState.emit(state);
      this.inputTextEmit.emit(this.inputText);
    }
    if (this.currentType === 'planningMode') {
      this.contentEditorService.openSearchLib.next(false);
    }
  }

  editfavorites() {
    this.hideCancelFavorites = !this.hideCancelFavorites;
  }

  getSuggetionResult(text) {
    this.searchSuggestion = [];
    this.searchFilterService.getSearchSuggestionData(text).subscribe(res => {
      if (res) {
        this.searchSuggestion = res;
        this.cdr.detectChanges();
      }
    });
  }

  setSuggestionText(suggestionText) {
    this.showSuggestionPopup = false;
    this.searchSuggestion = [];
    this.inputText = suggestionText;
    this.setCurrentUserText();
  }

  setCurrentUserText() {
    this.showSuggestionPopup = !this.showSuggestionPopup
    this.inputTextEmit.emit(this.inputText);
  }
}
