import {
  Component,
  OnInit,
  HostListener,
  ElementRef,
  Input,
  ChangeDetectorRef,
  ViewChild
} from '@angular/core';

import {
  SubjectSelectorService,
  FullClassSelection,
  AppConfigService,
  Resource,
  ApiResource,
  TCEAnimations,
  FileUploadService,
  CurriculumPlaylistService
} from '@tce/core';
import { SearchFilterService } from '../../service/search-filter.service';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
import { ToastrService } from 'ngx-toastr';
import { ContentEditorService } from '@tce/content-library';
@Component({
  selector: 'tce-search-view',
  templateUrl: './search-view.component.html',
  styleUrls: ['./search-view.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ]),
    trigger('slideInOutX', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateX(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class SearchViewComponent implements OnInit {
  currentText: any;
  _currentPlanningResource: any;
  _currentSubject: any;
  _currentChapterTopic: any;
  pageNumber = 0;
  currentResourceLength: any = 20;
  animationFlag: any;
  planningResourceState = false;
  searchClick = false;
  @Input('currentChapterTopic')
  set currentChapterTopic(val) {
    if (val) {
      this._currentChapterTopic = val;
    }
  }
  get currentChapterTopic() {
    return this._currentChapterTopic;
  }
  @Input('currentPlanningResource')
  set currentPlanningResource(val) {
    if (val && val.length > 0) {
      this._currentPlanningResource = val;
    } else {
      this._currentPlanningResource = [];
    }
  }
  get currentPlanningResource() {
    return this._currentPlanningResource;
  }
  @Input() currentPlanningData;
  @Input() searchViewState;
  @Input('currentSubject')
  set currentSubject(value) {
    this._currentSubject = value;
    this.currentText = value;
    this.defaultText = value;
    if (this.currentResourceLength) {
      this.getUserText(value);
    }

    //this.getCurrentViewState('filterView')
  }
  get currentSubject() {
    return this._currentSubject;
  }
  showChapterResources: any = false;
  currentResource: Resource[] = [];
  defaultText: any;
  currentPreviewResource: any;
  totalPage = 0;
  collectText: any;
  myPlanningAddResource: any;
  collectionoFAllData: any;
  fileType: any;
  fileSize: any;
  converMBtoByte: any;
  fileIcons: any;
  closeSearchSuggestion;
  @Input() type;
  @ViewChild('fileDropRef')
  myInputVariable: ElementRef;
  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private searchFilterService: SearchFilterService,
    private appConfigService: AppConfigService,
    private fileUploadService: FileUploadService,
    private eRef: ElementRef,
    private toastrService: ToastrService,
    private cdr: ChangeDetectorRef,
    private contentEditorService: ContentEditorService,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {}

  ngOnInit(): void {
    this.currentResourceLength = this.appConfigService.getConfig(
      'global_setting'
    )['searchResourceLength'];
    this.fileSize = this.appConfigService.getConfig('global_setting')[
      'addResourceMaxFileSize'
    ];
    this.fileType = this.appConfigService.getConfig('general')[
      'customResourceData'
    ]['addResourceFileType'];
    // console.log(
    //   'SearchViewComponent -> ngOnInit -> this.currentResourceLength',
    //   this.currentResourceLength
    // );
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];
    this.fileIcons = this.appConfigService.getConfig('general')[
      'customResourceData'
    ]['resource'];
    this.contentEditorService.selectedResourceData$.subscribe((data: any) => {
      let newData: any;
      if (data && data != null) {
        this.collectionoFAllData = data;
        newData = data.filter(f => f.selectedResourceList.length > 0);
        if (newData && newData.length > 0) {
          this.planningResourceState = true;
          this.myPlanningAddResource = newData;
        } else {
          this.planningResourceState = false;
          this.myPlanningAddResource = [];
        }
      } else {
        this.myPlanningAddResource = [];
        this.collectionoFAllData = [];
      }
      this.cdr.detectChanges();
    });
    this.contentEditorService.currentPlanningList$.subscribe(data => {
      if (data) {
        //console.log('ResourceSelectionComponent -> ngOnInit -> data', data);
        this.currentPlanningData = data;
      }
    });
    // this.subjectSelectorService.fullClassSelection$.subscribe(
    //   (newClassSelection: FullClassSelection) => {
    //     if (newClassSelection) {
    //       this.currentText = newClassSelection.subject.title;
    //       this.getUserText(this.currentText);
    //     }
    //   }
    // );
  }
  getCurrentViewState(state: any) {
    
    this.currentResource = [];
    this.showChapterResources = false;
    this.totalPage = 0;
    this.currentPreviewResource = null;
    this.searchViewState = state;
    this.pageNumber = 0;
    if (state === 'libraryView' && this.currentResourceLength) {
      //console.log('libraryView');
      this.getUserText(this.currentText);
    }
  }
  getInputClick(event){
    this.searchClick = event
    this.searchFilterService.getSearchClick(this.searchClick);
//console.log("getInputClick",event)
  }
  searchContainerClick(event){
    //console.log("searchContainerClick event",event)
    if(event){
      this.closeSearchSuggestion = false;
    }

  }
  getUserText(text: any) {
    this.pageNumber = 0;
    this.collectText = '';
    this.collectText = text;
    //console.log(' this.currentResourceLength', this.currentResourceLength);
    if (this.collectText) {
      if (this.currentResourceLength) {
        this.getCurrentTextResource(
          this.collectText,
          this.pageNumber,
          this.currentResourceLength
        );
      }
    } else {
      if (this.searchViewState != 'libraryView') {
        this.toastrService.error('Input Filed Required');
      }
    }
  }

  getCurrentSuggestion(data: any) {
    //console.log(data);
    this.searchViewState = data.state;
    this.defaultText = data.text;
    this.pageNumber = 0;
    this.getCurrentTextResource(
      data.text,
      this.pageNumber,
      this.currentResourceLength
    );
  }
  getCurrentTextResource(userText, pageNumber, requiredRecoredLength) {
    //console.log('getCurrentTextResource');
    this.totalPage = 0;
    if (pageNumber === 0) {
      this.currentResource = [];
    }
    this.searchFilterService
      .getSearchData(userText, pageNumber, requiredRecoredLength)
      .subscribe(res => {
        if (res && res.totalPageCount) {
          this.totalPage = res.totalPageCount;
          // console.log(
          //   'SearchViewComponent -> getCurrentTextResource -> this.totalPage',
          //   this.totalPage
          // );
        }
        if (res && res.playlistJson) {
          let JsonParseResponce = JSON.parse(res.playlistJson);
          // console.log(
          //   'SearchViewComponent -> getCurrentTextResource -> res',
          //   JsonParseResponce
          // );
          if (
            JsonParseResponce &&
            JsonParseResponce.asset &&
            JsonParseResponce.asset.length > 0
          ) {
            if (pageNumber === 0) {
              //console.log(pageNumber);
              if (JsonParseResponce.asset.length > 8) {
                this.showChapterResources = true;
                //console.log('JsonParseResponce', this.showChapterResources);
              } else {
                this.showChapterResources = false;
              }
            } else {
              this.showChapterResources = true;
            }

            JsonParseResponce.asset.forEach(resource => {
              //console.log(resource);
              this.createResourceObject(resource);
            });
          } else {
            //console.log('else assets');
          }
        }
      });
  }

  createResourceObject(resource: any) {
    //console.log(resource);
    let converToResource = new Resource({
      assetId: resource['assetId'],
      tpId: resource['tpId'],
      title: resource['title'],
      mimeType: resource['mimeType'],
      assetType: resource['assetType'],
      thumbFileName: resource['thumbFileName'] ? resource['thumbFileName'] : '',
      fileName: resource['fileName'],
      subType: resource['subType'],
      encryptedFilePath: resource['encryptedFilePath'],
      metaData: {
        assetId: resource['assetId'],
        tpId: resource['tpId'],
        lcmsSubjectId: resource['lcmsSubjectId'],
        lcmsGradeId: resource['lcmsGradeId'],
        title: resource['title'],
        mimeType: resource['mimeType'],
        assetType: resource['assetType'],
        thumbFileName: resource['thumbFileName']
          ? resource['thumbFileName']
          : '',
        fileName: resource['fileName'],
        ansKeyId: resource['ansKeyId'],
        copyright: resource['copyright'],
        subType: resource['subType'],
        description: resource['description'],
        keywords: resource['keywords'],
        encryptedFilePath: resource['encryptedFilePath'],
        filePath:
          resource['encryptedFilePath'] +
          '/' +
          resource['assetId'] +
          '/' +
          resource['fileName']
      }
    });
    this.currentResource.push(converToResource);
    this.cdr.detectChanges();
    //console.log(' this.currentResource', this.currentResource);
  }

  getCurrentResourcePreviewPositionLib(position: any) {
    //console.log("position",position)
    const documentElement = document.documentElement;
    // documentElement.style.setProperty(
    //   `--${'previewRight'}`,
    //   `${position.top + 'px'}`
    // );
    // documentElement.style.setProperty(
    //   `--${'toolSubmenuPosition'}`,
    //   `${position.position + 'px'}`
    // );
  }

  getCurrentResourceLib(resource: any) {
    this.currentPreviewResource = resource;
  }

  getLoadMoreData(data) {
    this.pageNumber = data;
    this.getCurrentTextResource(
      this.collectText,
      data,
      this.currentResourceLength
    );
    //console.log('getLoadMoreData',this.pageNumber)
  }
  async getExtationFlag(arr, currentExtn) {
    let xyz = true;
    arr.forEach(element => {
      if (element == currentExtn) {
        xyz = false;
      }
    });
    return xyz;
  }
  async getCustomResourceThumbnailIcon(type: string) {
    let getCustomIcon: any;
    if (this.fileIcons) {
      for (let index = 0; index < this.fileIcons.length; index++) {
        for (
          let typeIndex = 0;
          typeIndex < this.fileIcons[index].type.length;
          typeIndex++
        ) {
          if (this.fileIcons[index].type[typeIndex] === type) {
            getCustomIcon = this.fileIcons[index].icon;
            console.log(
              'FormResourceComponent -> getCustomResourceThumbnailIcon -> getCustomIcon',
              getCustomIcon
            );
          }
        }
      }
    }
    if (getCustomIcon) {
      return getCustomIcon;
    }
  }
  onFileDropped($event) {
    this.prepareFilesList($event);
  }
  handleFileInput(files) {
    this.prepareFilesList(files);
  }
  async prepareFilesList(files: Array<any>) {
    //console.log('SearchViewComponent -> prepareFilesList -> files', files);
    let listOfResource = [];
    this.converMBtoByte = Number(this.fileSize) * 1048576;
    for (let index = 0; index < files.length; index++) {
      //check file
      if (files[index]) {
        const fileNameExtn = files[index].name
          .split('.')
          .pop()
          .toLowerCase();
        const arrFiletype = this.fileType.split(',');
        let showEtnMessage = await this.getExtationFlag(
          arrFiletype,
          '.' + fileNameExtn
        );
        //check extension
        if (!showEtnMessage) {
          //check size of file
          if (files[index].size >= this.converMBtoByte) {
            listOfResource = [];
            this.toastrService.error(
              'File Size should be less then or equal to ' + this.fileSize
            );
          } else {
            let customIcon = await this.getCustomResourceThumbnailIcon(
              fileNameExtn
            );
            //get icon
            if (customIcon) {
              let newdata = {
                type: fileNameExtn,
                item: files[index],
                icon: customIcon,
                currentChapterTopicSubject: this.currentChapterTopic
              };

              listOfResource.push(newdata);
            }
          }
        } else {
          listOfResource = [];
          this.toastrService.error('Invalid File Extention');
        }
      }
    }
    if (listOfResource && listOfResource.length > 0) {
      // console.log(
      //   'SearchViewComponent -> prepareFilesList -> listOfResource',
      //   listOfResource
      // );
      this.contentEditorService.chooseFileResourceList.next(listOfResource);
      this.contentEditorService.chooseFileResource.next(true);
      this.myInputVariable.nativeElement.value = '';
    }
  }
  // async handleFileInput(files: FileList) {
  //   this.converMBtoByte = Number(this.fileSize) * 1048576;
  //   console.log(
  //     'SearchViewComponent -> handleFileInput -> this.fileSize',
  //     this.fileSize
  //   );
  //   if (files && files[0]) {
  //     const fileNameExtn = files[0].name
  //       .split('.')
  //       .pop()
  //       .toLowerCase();
  //     const arrFiletype = this.fileType.split(',');
  //     let showEtnMessage = await this.getExtationFlag(
  //       arrFiletype,
  //       '.' + fileNameExtn
  //     );
  //     if (!showEtnMessage) {
  //       if (files[0].size >= this.converMBtoByte) {
  //         this.toastrService.error(
  //           'File Size should be less then or equal to ' + this.fileSize
  //         );
  //       } else {
  //         let customIcon = await this.getCustomResourceThumbnailIcon(
  //           fileNameExtn
  //         );
  //         if (customIcon) {
  //           let listOfResource = [];
  //           let newdata = {
  //             type: fileNameExtn,
  //             item: files,
  //             icon: customIcon,
  //             currentChapterTopicSubject: this.currentChapterTopic
  //           };

  //           listOfResource.push(newdata);
  //           if (listOfResource && listOfResource.length > 0) {
  //             this.contentEditorService.chooseFileResourceList.next(
  //               listOfResource
  //             );
  //             this.contentEditorService.chooseFileResource.next(true);
  //             console.log(
  //               'SearchViewComponent -> handleFileInput -> files',
  //               files.item(0)
  //             );
  //           }
  //         }
  //       }
  //     } else {
  //       this.toastrService.error('Invalid File Extention');
  //     }
  //   }
  // }
}
