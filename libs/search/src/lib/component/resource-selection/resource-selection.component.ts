import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { ContentEditorService } from '@tce/content-library';
import { Subscription } from 'rxjs';
import { CurriculumPlaylistService } from '@tce/core';
import { SearchFilterService } from '../../service/search-filter.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'resource-selection',
  templateUrl: './resource-selection.component.html',
  styleUrls: ['./resource-selection.component.scss']
})
export class ResourceSelectionComponent implements OnInit, OnDestroy {
  _currentPlanningResource: any;
  newResourceList: any = {};
  @Input() selectedResourceList;
  @Input() collectionoFAllData;
  @Input('currentPlanningResource')
  set currentPlanningResource(val) {
    //console.log('currentPlanningResource', val);
    if (val) {
      this._currentPlanningResource = val;
    } else {
      this._currentPlanningResource = [];
    }
  }
  get currentPlanningResource() {
    return this._currentPlanningResource;
  }
  _currentPlanningData: any;
  @Input('currentPlanningData')
  set currentPlanningData(value) {
     //console.log('ResourceSelectionComponent -> setcurrentPlanningData -> value', value );
    this._currentPlanningData = value;
  }
  get currentPlanningData() {
    return this._currentPlanningData;
  }
  resourceSelectionSubscription: Subscription = new Subscription();
  constructor(
    private contentEditorService: ContentEditorService,
    private searchFilterService: SearchFilterService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private toastrService: ToastrService
  ) {}

  ngOnInit(): void {}
  setCurrentView() {
    this.contentEditorService.setSelectedResourceData(null);
    this.contentEditorService.openSearchLib.next(false);
    this.contentEditorService.selectedResourceData.next(null);
  }
  selectedResourceRemove(id, name, type) {
    if (this.collectionoFAllData && this.collectionoFAllData.length > 0) {
      for (let index = 0; index < this.collectionoFAllData.length; index++) {
        if (this.collectionoFAllData[index].name === name) {
          if (type === 'selected') {
            if (
              this.collectionoFAllData[index].selectedResourceList &&
              this.collectionoFAllData[index].selectedResourceList.length > 0
            ) {
              for (
                let resourceIndex = 0;
                resourceIndex <
                this.collectionoFAllData[index].selectedResourceList.length;
                resourceIndex++
              ) {
                if (
                  this.collectionoFAllData[index].selectedResourceList[
                    resourceIndex
                  ].resourceId === id
                ) {
                  this.collectionoFAllData[index].selectedResourceList.splice(
                    resourceIndex,
                    1
                  );
                  this.contentEditorService.selectedResourceData.next(
                    this.collectionoFAllData
                  );
                }
              }
            }
          }
          if (type === 'all') {
            this.collectionoFAllData[index].selectedResourceList = [];
            this.contentEditorService.selectedResourceData.next(
              this.collectionoFAllData
            );
          }
        }
      }
    }
  }
  removeAll(name, type) {
    this.selectedResourceRemove('', name, type);
  }

  addResource() {
    //console.log('this.collectionoFAllData', this.collectionoFAllData);
    //console.log('currentPlanningResource', this.currentPlanningResource);
    let currentState = false;
    let currentresourceList: any = [];
    for (let index = 0; index < this.collectionoFAllData.length; index++) {
      if (
        this.collectionoFAllData[index].selectedResourceList &&
        this.collectionoFAllData[index].selectedResourceList.length > 0
      ) {
        for (
          let newIndex = 0;
          newIndex <
          this.collectionoFAllData[index].selectedResourceList.length;
          newIndex++
        ) {
          this.currentPlanningResource.push(
            this.collectionoFAllData[index].selectedResourceList[newIndex]
          );

          currentState = true;
        }
      }
    }
    if (currentState) {
      this.toastrService.success('Resource Added successfully')
      this.reArrangResource(this.currentPlanningResource);
      this.contentEditorService.currentPlanningResourceData.next(
        this.currentPlanningResource
      );
      for (
        let currentIndex = 0;
        currentIndex < this.collectionoFAllData.length;
        currentIndex++
      ) {
        this.collectionoFAllData[currentIndex].selectedResourceList = [];
        currentresourceList.push(this.collectionoFAllData[currentIndex]);
      }
      if (currentresourceList && currentresourceList.length > 0) {
        //console.log('currentresourceList', currentresourceList);
        this.contentEditorService.selectedResourceData.next(
          currentresourceList
        );
      }
    }
  }
  reArrangResource(currentResourceList) {
    this.newResourceList = {};
    let newCollectionAssets: any = [];
    let newCollectionPractice: any;
    let newCollectionGallery: any;
    let newCreationDate = Math.round(new Date().getTime() / 1000);
    let playlistJson = {
      creationDate: newCreationDate,
      asset: newCollectionAssets,
      gallery: newCollectionGallery,
      practice: newCollectionPractice
    };

    for (let index = 0; index < currentResourceList.length; index++) {
      if (currentResourceList[index].tcetype === 'quiz') {
        newCollectionAssets.push({ assets_type: 'practice' });
        newCollectionPractice = currentResourceList[index].metaData.questionIds;
        playlistJson.practice = newCollectionPractice;
      }
      if (currentResourceList[index].tcetype === 'gallery') {
        newCollectionAssets.push({ assets_type: 'gallery' });
        newCollectionGallery = currentResourceList[index].metaData;
        playlistJson.gallery = newCollectionGallery;
      }
      if (
        currentResourceList[index].tcetype !== 'quiz' &&
        currentResourceList[index].tcetype !== 'gallery'
      ) {
        newCollectionAssets.push(currentResourceList[index].metaData);
      }
    }
    if (playlistJson) {
      let string = JSON.stringify(playlistJson, function(k, v) {
        if (v !== undefined) {
          return v;
        }
      });

      //console.log("--this.currentPlanningData-->>",this.currentPlanningData)
      //console.log("--this.currentPlanningData.currentGradeId-->>",this.currentPlanningData.currentGradeId)
      //console.log("--this.currentPlanningData.currentSubjectId-->>",this.currentPlanningData.currentSubjectId)
      //console.log("--this.currentPlanningData.currentTopicId-->>",this.currentPlanningData.currentTopicId)
      this.newResourceList = {
        gradeId: this.currentPlanningData.currentGradeId,
        subjectId: this.currentPlanningData.currentSubjectId,
        playlistJson: {
          playlistJson: string,
          id: this.currentPlanningData.currentTopicId
        }
      };
      if (this.newResourceList) {
        //console.log('newResourceList', this.newResourceList);
        this.curriculumPlaylistService.UpdateResourceList(this.newResourceList);
      }
      //
      // this.currentTpoicResource.resources = this.resources;
      // this.curriculumPlaylistService.setTopicSelection(
      //   this.currentTpoicResource
      // );
    }

    //this.curriculumPlaylistService.setAvailableResources(this.resources);
  }
  ngOnDestroy() {
    this.resourceSelectionSubscription.unsubscribe();
  }
}
