import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { TCEAnimations } from '@tce/core';

@Component({
  selector: 'tce-search-library',
  templateUrl: './search-library.component.html',
  styleUrls: ['./search-library.component.scss'],
  animations: [TCEAnimations.resourceDisplay]
})
export class SearchLibraryComponent implements OnInit {
  resourceChange = false;
  _currentResourceList: any;
  @Input() currentPageNumber;
  @Input() currentType;
  @Input() currentView;
  @Input() currentPlanningResourceState;
  @Input('currentResourceList')
  set currentResourceList(resourceList) {
    this.resourceChange = !this.resourceChange;
    this._currentResourceList = resourceList;
  }
  get currentResourceList() {
    return this._currentResourceList;
  }
  @Input() showFooter;
  @Input() totalPageCount;

  @Output() currentSuggestion = new EventEmitter<any>();
  @Output() currentResourcePreviewPositionLib = new EventEmitter<any>();
  @Output() currentResourceLib = new EventEmitter<any>();
  @Output() loadMoreData = new EventEmitter<any>();
  staticSuggetion: any;
  constructor() {}

  ngOnInit(): void {
    this.staticSuggetion = [
      {
        id: 1,
        name: 'abc'
      },
      {
        id: 2,
        name: 'xyz'
      },
      {
        id: 3,
        name: 'pqr'
      }
    ];
  }
  filterSelectedSuggetion(text: any, state: any) {
    let customSuggetionJSON = {
      text,
      state
    };
    this.currentSuggestion.emit(customSuggetionJSON);
  }

  getCurrentResourcePreviewPosition(position: any) {
    this.currentResourcePreviewPositionLib.emit(position);
  }
  getCurrentResource(resource: any) {
    this.currentResourceLib.emit(resource);
  }

  getLoadMoreData(data) {
    this.loadMoreData.emit(data);
  }
}
