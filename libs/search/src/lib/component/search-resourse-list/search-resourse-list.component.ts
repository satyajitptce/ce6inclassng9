import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  ChangeDetectorRef
} from '@angular/core';
import { Resource, RequestApiService, AppConfigService } from '@tce/core';
import { SearchFilterService } from '../../service/search-filter.service';
@Component({
  selector: 'tce-search-resourse-list',
  templateUrl: './search-resourse-list.component.html',
  styleUrls: ['./search-resourse-list.component.scss']
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResourseListComponent implements OnInit {
  _totalPageCountResourceFlag: any;
  _totalPageCountResource: any;
  _chapterResources: any;
  _FooterState: any;
  _currentType: any;
  public scrollContainerRef: HTMLElement;
  @Input() currentPlanningResourceState;
  @Input('currentType')
  set currentType(val) {
    //console.log('SearchResourseListComponent -> setcurrentType -> val', val);
    this._currentType = val;
  }

  get currentType() {
    return this._currentType;
  }
  @Input() currentPage;
  @Input('chapterResources')
  set chapterResources(resourceList) {
    this._chapterResources = resourceList;
    this.changeDetectorRef.detectChanges();
  }
  get chapterResources() {
    return this._chapterResources;
  }
  @Input() receivedParentStatus;
  @Input('FooterState')
  set FooterState(scrollFlag) {
    this._FooterState = scrollFlag;
    //console.log("SearchResourseListComponent -> setFooterState -> scrollFlag", scrollFlag)
  }
  get FooterState() {
    return this._FooterState;
  }
  @Input('totalPageCountResource')
  set totalPageCountResource(count) {
    //console.log("SearchResourseListComponent -> settotalPageCountResource -> count", count)
    //console.log("SearchResourseListComponent -> settotalPageCountResource -> this.currentPage", this.currentPage)
    if (count > 1 && this.currentPage + 1 < count) {
      this._totalPageCountResourceFlag = true;
    } else {
      this._totalPageCountResourceFlag = false;
    }
    this.changeDetectorRef.detectChanges();
  }
  get totalPageCountResource() {
    return this._totalPageCountResource;
  }
  @Output() currentResourcePreviewPosition = new EventEmitter<any>();
  @Output() currentResource = new EventEmitter<any>();
  @Output() loadData = new EventEmitter<any>();
  selectedResourceId: any;
  constructor(
    private appConfigService: AppConfigService,
    private requestApiService: RequestApiService,
    private changeDetectorRef: ChangeDetectorRef,
    private searchFilterService: SearchFilterService
  ) {}

  ngOnInit() {
    this.searchFilterService.closeSelectedResourceState$.subscribe(state => {
      this.closePreview();
    });

    this.searchFilterService.searchClick$.subscribe(value => {
      if(value){
        this.closePreview();
      }
    });
    this.searchFilterService.closePreview$.subscribe(value=>{
      this.closePreview();
    })


  }
  getResourceThumbnailIcon(resource: Resource, type: string) {
    //console.log('getResourceThumbnailIcon', resource, type);
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }

  getResourceThumbnail(thumbnailParams: any) {
    //console.log('getResourceThumbnail', thumbnailParams);
    if (thumbnailParams) {
      return this.requestApiService.getUrl('getFile') + thumbnailParams;
    } else {
      return '';
    }
  }

  openPreview(event: any, resource: any) {
    this.selectedResourceId = resource.resourceId;
    const targetElement = event.target;
    const targetElementTop = targetElement.getClientRects()[0].top;
    // console.log(
    //   'SearchResourseListComponent -> openPreview -> targetElementTop',
    //   targetElement.getClientRects()[0]
    // );
    const searchResourceRef: any = document.getElementsByClassName(
      'row col-12 resource-card-wrapper'
    );
    //console.log("SearchResourseListComponent -> openPreview -> searchResourceRef", searchResourceRef)
    const searchResourceTop = searchResourceRef[0].getClientRects()[0].top;
    const getsearchResourceRef = searchResourceRef[0].getClientRects()[0];
    //console.log("SearchResourseListComponent -> openPreview -> getsearchResourceRef", getsearchResourceRef)
    const final = targetElementTop - searchResourceTop;
    const submenuPosition =
      getsearchResourceRef.left + getsearchResourceRef.width;
    const newposition = {
      top: final,
      position: submenuPosition
    };
    if (newposition) {
      //console.log('newposition', newposition);
      this.currentResourcePreviewPosition.emit(newposition);
      this.currentResource.emit(resource);
    }
  }
  closePreview() {
    this.selectedResourceId = '';
    this.currentResource.emit(null);
  }

  triggerScroll(emittedObject: {
    
    eventName: string;
    type: string;
    cachedCurrentTarget: any;
  }) {
    this.closePreview()
    
    if (
      emittedObject.eventName === 'mousedown' ||
      emittedObject.eventName === 'touchstart'
    ) {
      //console.log("emittedObject",emittedObject)
      const pdfElement =
        emittedObject.cachedCurrentTarget.parentNode.parentNode.parentNode
          .parentNode.parentNode.parentNode.previousElementSibling
          .firstElementChild.firstElementChild.firstElementChild;
      if (emittedObject.type === 'up') {
        pdfElement.scrollTop -= 20;
      } else {
        pdfElement.scrollTop += 20;
      }
    }
  }

  loadMore() {
    this.loadData.emit(this.currentPage + 1);
  }
}
