import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchResourseListComponent } from './search-resourse-list.component';

describe('SearchResourseListComponent', () => {
  let component: SearchResourseListComponent;
  let fixture: ComponentFixture<SearchResourseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [SearchResourseListComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchResourseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
