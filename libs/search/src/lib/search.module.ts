import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchViewComponent } from './component/search-view/search-view.component';
import { ResourceType, CoreModule } from '@tce/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { SearchHeaderComponent } from './component/search-header/search-header.component';
import { SearchLibraryComponent } from './component/search-library/search-library.component';
import { SearchMyfavoriteComponent } from './component/search-myfavorite/search-myfavorite.component';
import { SearchResourseListComponent } from './component/search-resourse-list/search-resourse-list.component';
import { SearchPlayerResourceComponent } from './component/search-player-resource/search-player-resource.component';
import { SearchPlayerContainerComponent } from './component/search-player-container/search-player-container.component';
import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ResourceSelectionComponent } from './component/resource-selection/resource-selection.component';
import { DndDirective } from './directive/dnd.directive';
const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'player-' + ResourceType.TCEVIDEO,
    path: 'player-' + ResourceType.TCEVIDEO, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-tce-video').then(mod => mod.PlayerTceVideoModule)
  },
  {
    componentId: 'player-' + ResourceType.WORKSHEET,
    path: 'player-' + ResourceType.WORKSHEET, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-worksheet').then(mod => mod.PlayerWorksheetModule)
  },
  {
    componentId: 'player-' + ResourceType.EBOOK,
    path: 'player-' + ResourceType.EBOOK, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-ebook').then(mod => mod.PlayerEbookModule)
  },
  {
    componentId: 'player-' + ResourceType.QUIZ,
    path: 'player-' + ResourceType.QUIZ, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-quiz').then(mod => mod.PlayerQuizModule)
  },
  {
    componentId: 'player-' + ResourceType.GALLERY,
    path: 'player-' + ResourceType.GALLERY, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-gallery').then(mod => mod.PlayerGalleryModule)
  },
  {
    componentId: 'player-' + ResourceType.UNSUPPORT,
    path: 'player-' + ResourceType.UNSUPPORT, // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/player-unsupport').then(mod => mod.PlayerUnsupportModule)
  }
];
@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    AngularSvgIconModule,
    FormsModule,
    ReactiveFormsModule,
    LibConfigModule.forRoot(manifests),
    LibConfigModule.forChild(SearchViewComponent)
  ],
  declarations: [
    SearchViewComponent,
    SearchHeaderComponent,
    SearchLibraryComponent,
    SearchMyfavoriteComponent,
    SearchResourseListComponent,
    SearchPlayerContainerComponent,
    SearchPlayerResourceComponent,
    ResourceSelectionComponent,
    DndDirective
  ],
  exports: [
    SearchViewComponent,
    SearchHeaderComponent,
    SearchLibraryComponent,
    SearchMyfavoriteComponent,
    SearchResourseListComponent,
    SearchPlayerContainerComponent,
    SearchPlayerResourceComponent,
    ResourceSelectionComponent
  ],
  entryComponents: [
    SearchViewComponent,
    SearchHeaderComponent,
    SearchLibraryComponent,
    SearchMyfavoriteComponent,
    SearchResourseListComponent,
    SearchPlayerContainerComponent,
    SearchPlayerResourceComponent,
    ResourceSelectionComponent
  ]
})
export class SearchModule {}
