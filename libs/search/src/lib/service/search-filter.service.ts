import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { RequestApiService } from '@tce/core';
import { Resource } from '@tce/core';
@Injectable({
  providedIn: 'root'
})
export class SearchFilterService {
  private filterData = new Subject();
  public filterData$ = this.filterData.asObservable();

  private searchClick = new Subject();
  public searchClick$ = this.searchClick.asObservable();

  private closePreview = new Subject();
  public closePreview$ = this.closePreview.asObservable();

  public closeSelectedResourceState = new Subject();
  public closeSelectedResourceState$ = this.closeSelectedResourceState.asObservable();
  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService
  ) {}

  getSearchData(text, pageNumber, length): Observable<any> {
    const requestUrl = this.requestApiService.getUrl('searchFilter').replace(
      '@searchItem@',
      'searchTerm=' + text + '&page=' + pageNumber + '&size=' + length
      //text + '&hl=on&facet=on&facet.field=resourceType&rows=10'
    );
    if (text) {
      return this.http.get(requestUrl).pipe(
        map(res => {
          return res;
        })
      );
    }
  }

  getSearchClick(value){
    this.searchClick.next(value);

  }

  setClosePreview(value){
    this.closePreview.next(value);
  }

  getSearchSuggestionData(text): Observable<any> {
    const requestUrl = this.requestApiService
      .getUrl('searchSuggestion')
      .replace(
        '@searchSuggestion@',
        'spellTerm=' + text
        //text + '&hl=on&facet=on&facet.field=resourceType&rows=10'
      );
    if (text) {
      return this.http.get(requestUrl).pipe(
        map(res => {
          return res;
        })
      );
    }
  }
  setFilterData(response) {
    this.filterData.next(response);
  }
}
