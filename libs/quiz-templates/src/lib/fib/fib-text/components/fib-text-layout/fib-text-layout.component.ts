import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  Renderer2,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  TemplateMcqData,
  TemplateMcqOption,
  FibTextDropdown
} from '../../../../core/interface/quiz-player-template.interface';
import { BehaviorSubject, Subject, Subscription, of } from 'rxjs';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { QuestionEditorService } from '@tce/template-editor';
import Quill from 'quill';
import { QuillEditorComponent } from 'libs/quiz-templates/src/lib/sharedEditors/quill-component/components/quill-component-layout/quill-editor.component';
import { TemplateMarkupLayoutComponent } from 'libs/quiz-templates/src/lib/sharedComponents/template-markup/components/template-markup-layout/template-markup-layout.component';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-fib-text-layout',
  templateUrl: './fib-text-layout.component.html',
  styleUrls: ['./fib-text-layout.component.scss']
})
export class FibTextLayoutComponent implements OnInit {
  @Input() public templateData: FibTextDropdown;
  @Input() public previewState: boolean;
  @Input() public submit: Subject<void>;
  @Input() public save: Subject<void>;
  @Output() public sourceStateChange = new EventEmitter();
  @Output() public showAnswers = new EventEmitter();
  @Output() public getAnswers = new EventEmitter();
  @Output() public editQuestion: BehaviorSubject<object> = new BehaviorSubject<
    object
  >({});
  private showAnsSubscription: Subscription;
  public updateResponse: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(false);
  public points: number;
  public previewShow: boolean = true;
  public sourceData: FibTextDropdown;
  private submitSubscription: Subscription;
  private previewSubscription: Subscription;
  private sourceSubscription: Subscription;
  private metadataSubscription: Subscription;
  public qstem: object = {};
  public layout: string = 'horizontal';
  public inputName: string;
  public selectedAnswers: Array<string> = [];
  public selectedAnswersLabels: Array<string> = [];
  public templateMarkUpData: string;
  public templateType: string;
  public dashboardPreviewShow: boolean = true;
  private dashboardPreviewSubscription: Subscription;
  @Input() public dashboardPreviewState: boolean;
  public getShowAnsState: object = {};
  public showAnsStateFlag: boolean;
  public selectedAnswersPreview: Array<string> = [];
  public correctAnsPoints: number = 0;

  public deviceView: string = 'laptop';
  private saveSubscription: Subscription;
  public possibleResponses: Array<Array<object>>;
  public responseCount: number;
  public navbarOpen: boolean = false;
  public metaData: object = {};
  public responseIndex: number;
  public clickedResponse: string;
  public tempData: string;
  public clickComponentStatus: boolean;
  public templateName: string;
  public quillInstance: any;
  rangeIndex: number;
  public qstemData: string = '';
  public quillLoaded: boolean;
  public responses: Object = {};
  public responseIds: Array<any> = [];
  @ViewChild(TemplateMarkupLayoutComponent)
  public tempMarkup: TemplateMarkupLayoutComponent;
  destroy$: Subject<boolean> = new Subject<boolean>();
  allowAddResponse: boolean = false;

  @ViewChild('qstemRef', { static: false })
  public qstemRef: ElementRef;
  @ViewChild('optionsPreviewDivHeight', { static: false })
  public optionsPreviewDivHeight: ElementRef;
  @ViewChild('answers', { static: false })
  public answers: ElementRef;
  @ViewChild('optionsDivMaxHt', { static: false })
  public optionsDivMaxHt: ElementRef;

  constructor(
    public renderer: Renderer2,
    private questionEditorService: QuestionEditorService
  ) {}

  ngOnInit() {
    this.previewShow = this.previewState;
    // this.submitSubscription = this.submit.subscribe(() => this.onSubmit());

    this.saveSubscription = this.save
      .pipe(takeUntil(this.destroy$))
      .subscribe(() => this.saveData());

    this.questionEditorService
      .getInstance()
      .pipe(takeUntil(this.destroy$))
      .subscribe(instance => {
        this.quillInstance = instance;
      });

    this.questionEditorService.getQuillLoading().subscribe(bool => {
      this.quillLoaded = bool;
    });

    if (this.dashboardPreviewState) {
    }

    this.questionEditorService
      .getAllowAddResponse()
      .pipe(takeUntil(this.destroy$))
      .subscribe(bool => {
        this.allowAddResponse = bool;
      });

    this.questionEditorService
      .getSubmitAnsShow()
      .pipe(takeUntil(this.destroy$))
      .subscribe(state => {
        this.showAnsStateFlag = state;
        console.log('showAnsStateFlag ', this.showAnsStateFlag);
      });

    this.initState();
    this.emitAns();
  }

  ngAfterViewInit() {
    this.calculateOptionsDivHeight();
  }

  emitAns() {
    if (!this.dashboardPreviewState) {
      this.questionEditorService.updateAnswerStateObject({
        selectedAnswersPreview: this.selectedAnswersPreview,
        selectedAnswers: this.selectedAnswers,
        state: this.showAnsStateFlag,
        points: this.points,
        correctAnsPoints: this.correctAnsPoints
      });
      this.questionEditorService
        .getAnsStateObject()
        .pipe(takeUntil(this.destroy$))
        .subscribe(data => {
          this.getShowAnsState = data;
        });
    }
  }

  //Function to initial all the variables
  initState(calledFromDropped: boolean = false) {
    this.questionEditorService
      .getQstem()
      .pipe(takeUntil(this.destroy$))
      .subscribe(stem => {
        console.log('qstem from ser ', stem);
        this.qstemData = stem;
      });
    var stemLable = '';
    if (this.qstemData) stemLable = this.qstemData;
    else stemLable = this.templateData.data.stimulus.label;
    console.log('Called from dropped', calledFromDropped);
    console.log('template Data:', this.templateData);
    this.qstem = {
      text: stemLable,
      value: this.templateData.data.stimulus.value
    };
    this.templateMarkUpData = this.templateData.data.template;
    this.layout = this.templateData.data.ui_style.type;
    this.inputName = this.templateData.reference;
    this.points = this.templateData.data.validation.valid_response.score;
    this.sourceData = this.templateData;
    this.templateType = this.templateData.type;
    this.metaData = this.templateData.data.metadata;
    this.selectedAnswers = this.templateData.data.validation.valid_response.value;
    this.templateName = this.templateData.name;

    if (
      this.templateType == 'fib-dropdown' ||
      this.templateType == 'fib-drag-drop'
    ) {
      this.possibleResponses = this.templateData.data.possible_responses;
    } else {
      console.log('WRONG TAMPLATE');
    }
    console.log('possible: ', this.possibleResponses);

    if (this.templateType == 'fib-text') {
      this.questionEditorService
        .getResponses()
        .pipe(takeUntil(this.destroy$))
        .subscribe(responses => {
          this.responses = responses;
        });
      this.questionEditorService
        .getResponseIds()
        .pipe(takeUntil(this.destroy$))
        .subscribe(ids => {
          this.responseIds = ids;
        });
    }
  }

  labels() {
    this.selectedAnswersLabels = [];
    if (this.possibleResponses) {
      for (let i = 0; i < this.possibleResponses.length; i++) {
        for (let j = 0; j < this.possibleResponses[i].length; j++)
          if (
            this.selectedAnswers &&
            this.selectedAnswers[i] === this.possibleResponses[i][j]['value']
          ) {
            this.selectedAnswersLabels.push(
              this.possibleResponses[i][j]['label']
            );
          }
      }
    }
  }

  //Output function which retrieves the points entered from the app-set-correct-ans-layout component
  getPointsValue(event) {
    this.points = event;
    this.changeTemplateData();
  }

  //Output function which retrieves the source json from the app-source-json-layout component
  changeSourceState(sourceJson) {
    this.templateData = sourceJson as FibTextDropdown;
    console.log('SOUrceData: ', this.templateData);
    let oldValues = this.templateData.data.validation.valid_response.value;
    let newValues = sourceJson.data.validation.valid_response.value;
    if (
      this.templateType == 'fib-dropdown' &&
      newValues.length > sourceJson.data.possible_responses.length
    ) {
      newValues.pop();
    }
    this.selectedAnswers = sourceJson.data.validation.valid_response.value;
    // this.labels();
    this.templateData = sourceJson as FibTextDropdown;
    this.updateResponse.next(true);
    this.initState();
    this.sourceStateChange.emit(false);
  }

  /**
   * @description To toggle side bar for metadata
   */
  toggleSidebar(metadata?): void {
    if (metadata) {
      this.metaData = this.templateData.data.metadata = metadata;
    }
    this.navbarOpen = !this.navbarOpen;
  }

  rIndex(index) {
    console.log(index);
    this.responseIndex = index;
  }

  /**
   * When dragged element is dropped
   * @param event contains the position of previous index and dragged index
   */
  dropped(event: CdkDragDrop<string[]>, index): void {
    console.log('Event: ', event, index);
    moveItemInArray(
      this.possibleResponses[index],
      event.previousIndex,
      event.currentIndex
    );
    this.sourceData = this.templateData;
    this.updateResponse.next(true);
    this.initState();

    console.log('this.possibleResponses: ', this.possibleResponses);
  }

  onOptUpdate(event, rowIndex, choiceIndex?) {
    console.log(event, rowIndex, choiceIndex);
    if (this.templateType == 'fib-dropdown') {
      let div = this.renderer.createElement('div');
      this.renderer.setProperty(div, 'innerHTML', event.label);
      let newVar = div.textContent || div.innerText || '';
      console.log('newVar: ', newVar);
      console.log('div: ', div, newVar);
      let oldPossibleResponse = this.possibleResponses[rowIndex][choiceIndex][
        'value'
      ];
      console.log('rowUpdate: ', event, rowIndex, choiceIndex);
      this.possibleResponses[rowIndex][choiceIndex] = event;
    }
    if (this.templateData.data.validation.valid_response.value)
      this.templateData.data.possible_responses = this.possibleResponses;
    this.sourceData = this.templateData;
    this.initState();
    this.updateResponse.next(true);
    console.log('This is source data ', this.sourceData);
  }

  trackByFn(index: any, item: any) {
    return index;
  }

  saveData() {
    this.selectedAnswersPreview = [];
    this.getAnswers.next(this.sourceData);
  }

  editRedirect(): void {
    this.editQuestion.next({
      category: this.templateData.data.type,
      subcategory: this.templateData.type,
      id: this.templateData['id']
    });
  }

  // Change value and score of the options in the templateData
  changeTemplateData() {
    this.templateData.data.validation.valid_response = {
      score: this.points,
      value: this.selectedAnswers
    };
  }

  // Get the answers selected in the app-fib-set-correct-ans-options-layout component
  getSelectedAns(pushedanswers) {
    // console.log(pushedanswers)
    this.selectedAnswers = pushedanswers;
    // this.labels();
    this.changeTemplateData();
  }

  correctPoints(points) {
    console.log('points ', points);

    if (!this.dashboardPreviewState) {
      if (points) {
        this.correctAnsPoints = this.points;
      } else {
        this.correctAnsPoints = 0;
      }
      this.emitPoints();
    }
  }

  emitPoints() {
    if (!this.dashboardPreviewState) {
      this.showAnswers.emit({
        points: this.points,
        selectedAnswersPreview: this.selectedAnswersPreview,
        correctAnsPoints: this.correctAnsPoints
      });
    }
  }

  addResponse(rowIndex, response) {
    if (this.templateType == 'fib-dropdown') {
      let incrementedVal = '0';
      if (response.length > 0) {
        incrementedVal = (
          parseInt(response[response.length - 1]['value']) + 1
        ).toString();
      }
      this.templateData.data.possible_responses[rowIndex].push({
        label: 'New Choice',
        value: incrementedVal
      });
    }
    this.updateResponse.next(true);
  }

  deleteResponse(response, rowIndex, choiceIndex?) {
    le: console.log('Delete: ', response, rowIndex, choiceIndex);
    if (this.templateType == 'fib-dropdown') {
      this.templateData.data.possible_responses[rowIndex].splice(
        choiceIndex,
        1
      );
      if (
        this.templateData.data.validation.valid_response.value[rowIndex] ==
        response.value
      ) {
        this.templateData.data.validation.valid_response.value[rowIndex] = '';
      }
    } else {
      this.templateData.data.possible_responses.splice(rowIndex, 1);
    }
    this.templateData.data.possible_responses = this.possibleResponses;
    this.sourceData = this.templateData;
    this.initState();
    console.log('this.possibleResponses: ', this.possibleResponses);
    this.updateResponse.next(true);
  }

  // Handle the form on submit
  onSubmit() {
    console.log('fib-text', 'Submit Pressed');
  }

  //Function to get updated content from the app-template-markup-layout
  onContentUpdate(updatedContent) {
    console.log('updated: ', updatedContent);
    this.templateMarkUpData = updatedContent.rdata;
    this.templateData.data.template = this.templateMarkUpData;
    console.log('DATA: ', updatedContent.rdata);
    if (updatedContent.rdata) {
      let matchedArray = updatedContent.rdata.match(/{{response}}/g) || [];
      if (this.templateType == 'fib-dropdown') {
        // console.log('MATCHARRAY: ', matchedArray, matchedArray.length);
        if (
          matchedArray.length < this.templateData.data.possible_responses.length
        ) {
          this.templateData.data.possible_responses.pop();
        }
      }

      if (
        matchedArray.length <
        this.templateData.data.validation.valid_response.value.length
      ) {
        this.templateData.data.validation.valid_response.value.pop();
      }
    }
    this.calculateOptionsDivHeight();
  }

  clickOpt(res: string): void {
    console.log('Temp Value', res);
    this.tempData = res;
    this.clickComponentStatus = false;
  }

  //Function to retrieve the updated value from the dc-opt component and update the source json
  onResponseClick(updatedContent: string): void {
    this.clickedResponse = updatedContent;
    console.log(this.clickedResponse);
  }

  //Function to get updated content from the dc-qstem
  onQstemContentUpdate(updatedContent) {
    console.log(updatedContent);
    this.templateData.data.stimulus.label = updatedContent.text;
    this.initState();
    console.log(this.qstem);
    this.calculateOptionsDivHeight();
  }

  // Validate the form on submit
  onValidation() {}

  /**
   * When dragged element is dropped
   * @param event contains the position of previous index and dragged index
   */
  responseDropped(event: CdkDragDrop<string[]>, i: number): void {
    moveItemInArray(
      this.possibleResponses[i],
      event.previousIndex,
      event.currentIndex
    );
    this.updateResponse.next(true);
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    // this.submitSubscription.unsubscribe();
    // this.showAnsSubscription.unsubscribe();
    // this.previewSubscription.unsubscribe();
    this.selectedAnswers = [];
    this.selectedAnswersPreview = [];
  }

  feedbackStemUpdate(evt) {
    this.qstem['feedback'] = evt;
    // this.templateData.data.stimulus.feedbackInline = this.qstem['feedback'];
  }

  addResponseTag(tagName: string) {
    if (this.quillInstance && this.allowAddResponse) {
      console.log(
        'templateMarkupdata',
        this.responses,
        this.templateMarkUpData,
        this.quillInstance.scrollingContainer.innerHTML
      );

      // code for response tag START
      // let quillInstance = this.quillInstance.editor;
      let range = this.quillInstance.getSelection(true);
      let responseSpan = this.renderer.createElement('code');
      let spanText = this.renderer.createText(` RESPONSE`);
      this.renderer.appendChild(responseSpan, spanText);
      let spanOuterHtml = responseSpan.outerHTML;

      range.index;
      // var clipboard = quillInstance.getModule('clipboard');

      this.quillInstance.insertText(range.index, ' ', Quill.sources.API);

      this.quillInstance.clipboard.dangerouslyPasteHTML(
        range.index + 1,
        spanOuterHtml
      );
      let range1 = this.quillInstance.getSelection(true);
      console.log('quillInstance ', this.quillInstance, range1.index);

      // Add a space after the marker

      this.quillInstance.setSelection(range.index + 9);
      //Take the cursor to the end of the inserted TemplateMarker
      this.rangeIndex = range.index;

      this.quillInstance.insertText(range.index + 11, '       ');

      console.log('focus', this.quillInstance.focus());

      let subString = this.templateMarkUpData.replace('<p>', '');
      subString = subString.substring(0, range.index);
      // subString
      let resmatch =
        subString && subString.match(/{{res/g) ? subString.match(/{{res/g) : [];
      let resCount = resmatch.length;

      console.log('substring before', this.responses, this.responseIds);

      this.questionEditorService.updateNewResponseFlag(true);
    }
  }

  calculateOptionsDivHeight() {
    var optionDivHeight;
    if (this.previewShow) {
      if (document.getElementById('qb-preview-submit')) {
        optionDivHeight =
          document.getElementById('qb-preview-submit').offsetTop -
          this.qstemRef.nativeElement.offsetHeight -
          215;
        this.optionsDivMaxHt.nativeElement.style.maxHeight =
          optionDivHeight + 'px';
      }
    } else {
      if (document.getElementById('qb-preview-submitQuesWrapper')) {
        optionDivHeight =
          document.getElementById('qb-preview-submitQuesWrapper').offsetTop -
          this.answers.nativeElement.offsetTop -
          35;
        this.optionsDivMaxHt.nativeElement.style.maxHeight =
          optionDivHeight + 'px';
        this.optionsDivMaxHt.nativeElement.style.minHeight =
          optionDivHeight + 'px';
      }
    }
  }
}
