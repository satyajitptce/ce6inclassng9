import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  Renderer2,
  AfterViewInit,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { SharedComponentService } from '@tce/quiz-templates';
import Quill from 'quill';

declare const openEditor: any;
declare const closeEditor: any;

import ImageResize from 'quill-image-resize';
import { TemplateMarker } from '../../template-maker.class';
import { QuestionEditorService } from '@tce/template-editor';
import { read } from 'fs';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

// Code for response tag start

let customeIcons = Quill.import('ui/icons');
customeIcons['addResponseTag'] =
  '<i class="fa fa-reply" style="color:lightgrey" title="Single Response" aria-hidden="true"></i>';
customeIcons['addMultipleResponseTag'] =
  '<i class="fa fa-reply-all" style="color:lightgrey" title="Multiple Questions" aria-hidden="true"></i>';
customeIcons['math'] =
  '<i class="fa fa-superpowers" style="color:lightgrey" title="Math Calculator" aria-hidden="true"></i>';

Quill.register('modules/imageResize', ImageResize);

@Component({
  selector: 'quiz-quill-editor',
  templateUrl: './quill-editor.component.html',
  styleUrls: ['./quill-editor.component.scss']
})
export class QuillEditorComponent implements OnInit, OnDestroy {
  @Input() public quillConfig;
  @Input() public quillHtmlData;
  @Input() public allowResponseTag: boolean;
  @Input() public formattingOptions: Array<string>;
  @Input() public templateType: string;
  @Input() public templateMode: string;
  @Input() public multipleQuestionToolbarOption: boolean;

  @Output() getUpdatedContent = new EventEmitter();
  @Output() getQuillUpdated = new EventEmitter();
  @Output() getUpdatedContentFocus = new EventEmitter();
  @Output() getIndex = new EventEmitter();
  @Output() addTag = new EventEmitter();
  @Output() deleteTag = new EventEmitter();
  public quillTools: any;
  public updateQuillData: any;
  public focus: boolean = false;
  public readOnly: boolean = false;
  public responses: Object = {};
  public responseIds: Array<any> = [];
  private newResFlag: boolean = false;
  private highlightedTexts: Array<object>;
  private highlightedTextsIds: Array<string>;
  destroy$: Subject<boolean> = new Subject<boolean>();

  quillInstance: any;

  rangeIndex: number;

  showEditor = false;
  activeObj;
  public modules = {
    imageResize: {
      displaySize: true
    },
    toolbar: false,
    clipboard: true
    //kept for future references
    // {
    //   container: [
    //     [{ placeholder: ['[GuestName]', '[HotelName]'] }], // my custom dropdown
    //     ['bold', 'italic', 'underline', 'strike'], // toggled buttons
    //     ['blockquote', 'code-block'],
    //     [{ header: 1 }, { header: 2 }], // custom button values
    //     [{ list: 'ordered' }, { list: 'bullet' }],
    //     [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
    //     [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
    //     [{ direction: 'rtl' }], // text direction
    //     [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    //     [{ font: [] }],
    //     [{ align: [] }],
    //     ['link', 'image', 'math'],
    //     ['clean'] // remove formatting button
    //   ],
    //   handlers: {
    //     /**
    //      * @description
    //      * This function handles the function on image button click in the quill toolbar
    //      * @returns void
    //      */
    //     image: () => {
    //       console.log('Image Modal Quill', this.quillConfig);
    //       // this.imageModalOpen.emit({type: 'editor', state:true});

    //       this.sharedComponentService.imageUploadModalService({
    //         type: 'editor',
    //         state: true,
    //         name: this.quillConfig.name
    //       });
    //     },
    //     math: e => {
    //       console.log(e);
    //       this.activeObj = {
    //         elem: e
    //       };
    //       this.showEditor = true;
    //     },
    //     addResponseTag: e => {
    //       this.addResponseTag('RESPONSE');
    //     },
    //     addMultipleResponseTag: e => {
    //       this.addResponseTag('VARIABLE');
    //     }
    //   }
    // }
  };

  public selectedModules = {
    toolbar: {
      // container: [this.formattingOptions]
    }
  };

  constructor(
    public renderer: Renderer2,
    public sanitizer: DomSanitizer,
    public sharedComponentService: SharedComponentService,
    private readonly questionEditorService: QuestionEditorService,
    private elm: ElementRef
  ) {}

  ngOnInit() {
    //console.log('instance loaded');

    this.questionEditorService.getInstance().subscribe(instance => {
      //console.log('instance ', instance);
    });
    if (this.templateType === 'fib-text') {
      this.questionEditorService
        .getResponses()
        .pipe(takeUntil(this.destroy$))
        .subscribe(res => {
          this.responses = res;
        });
      this.questionEditorService
        .getResponseIds()
        .pipe(takeUntil(this.destroy$))
        .subscribe(ids => {
          this.responseIds = ids;
        });
      setTimeout(() => {
        this.addUids();
      });
    }

    if (this.templateType === 'token-highlight') {
      this.questionEditorService
        .getHighlightedTexts()
        .pipe(takeUntil(this.destroy$))
        .subscribe(texts => {
          this.highlightedTexts = texts;
        });

      this.questionEditorService
        .getHighlightedTextsIds()
        .pipe(takeUntil(this.destroy$))
        .subscribe(ids => {
          this.highlightedTextsIds = ids;
        });
      setTimeout(() => {
        this.addHighlightTextsId();
      });
    }

    //console.log('tempType ', this.templateType);

    this.questionEditorService.getNewResponseFlag().subscribe(flag => {
      this.newResFlag = flag;
    });

    if (this.templateType === 'mcq-tf') this.readOnly = true;
    //kept for future reference

    // if (this.templateType == 'rich-text') {
    //   this.quillTools = [this.formattingOptions];
    //   this.modules.toolbar.container = this.quillTools;
    // } else {
    // this.quillTools = [
    //   [{ placeholder: ['[GuestName]', '[HotelName]'] }], // my custom dropdown
    //   ['bold', 'italic', 'underline', 'strike'], // toggled buttons
    //   ['blockquote', 'code-block'],
    //   [{ header: 1 }, { header: 2 }], // custom button values
    //   [{ list: 'ordered' }, { list: 'bullet' }],
    //   [{ script: 'sub' }, { script: 'super' }], // superscript/subscript
    //   [{ indent: '-1' }, { indent: '+1' }], // outdent/indent
    //   [{ direction: 'rtl' }], // text direction
    //   [{ color: [] }, { background: [] }], // dropdown with defaults from theme
    //   [{ font: [] }],
    //   [{ align: [] }],
    //   ['link', 'image', 'math'],
    //   ['clean'] // remove formatting button
    // ];
    // this.modules.toolbar.container = this.quillTools;
    // }

    // check if response tag is allowed -- rohit
    // kept for future reference -- usman

    // if (this.allowResponseTag) {
    //   this.modules.toolbar.container['10'].push('addResponseTag');
    //   TemplateMarker.tagName = 'RESPONSE';
    // }

    // if (this.multipleQuestionToolbarOption) {
    //   this.modules.toolbar.container['10'].push('addMultipleResponseTag');
    //   TemplateMarker.tagName = 'VARIABLE';
    // }

    TemplateMarker.blotName = 'TemplateMarker';
    Quill.register({
      'formats/TemplateMarker': TemplateMarker
    });

    // this.sharedComponentService.getImageData.subscribe(imageData => {
    //   if (
    //     imageData['componentType'] == 'editor' &&
    //     imageData['name'] == this.quillConfig.name
    //   ) {
    //     let image = this.renderer.createElement('img');
    //     this.renderer.setAttribute(image, 'src', imageData['imageUrl']);
    //     if (!this.quillHtmlData) this.quillHtmlData = '';
    //     this.quillHtmlData += image.outerHTML;
    //   }
    // });
  }

  addUids() {
    //console.log('uuids', this.responses, this.responseIds);

    let elementRef = this.elm.nativeElement.querySelectorAll('code');
    for (let i = 0; i < elementRef.length; i++) {
      let uid: any;
      if (this.responseIds[i]) {
        uid = this.responseIds[i];
      } else {
        uid = Math.floor(100000 + Math.random() * 900000);
        this.responseIds.push(uid);
      }
      this.renderer.setAttribute(elementRef[i], 'id', uid);
      this.responses[uid] = elementRef[i].innerText;
    }
    this.questionEditorService.updateResponseIds(this.responseIds);
    this.questionEditorService.updateResponses(this.responses);
  }

  addHighlightTextsId() {
    let elementRef = this.elm.nativeElement.querySelectorAll('code');
    for (let i = 0; i < elementRef.length; i++) {
      let uid: any;
      if (this.highlightedTextsIds[i]) {
        uid = this.highlightedTextsIds[i];
      } else {
        uid = Math.floor(100000 + Math.random() * 900000);
        this.highlightedTextsIds.push(uid);
      }
      this.renderer.setAttribute(elementRef[i], 'id', uid);
      this.highlightedTexts[uid] = elementRef[i].innerText;
    }
    this.questionEditorService.updateHighlightedTextsIds(
      this.highlightedTextsIds
    );
    this.questionEditorService.updateHighlightedTexts(this.highlightedTexts);
    //console.log('highlight', this.highlightedTexts, this.highlightedTextsIds);
  }

  ngOnChanges(changes) {}

  onEditorCreated(editorInstance) {
    //console.log('editorInstance', editorInstance, this.quillConfig);
    this.quillInstance = editorInstance;
    if (this.quillConfig.quillLoc === 'qstem') {
      this.quillInstance.focus();
    }
  }

  /**
   * @description
   * This function strips the tags from the data obtainer from the quill editor and emits it to the parent component
   * @param changedContent
   * @returns void
   */
  onContentChanged(changedContent): void {
    // console.log('changedContent ', changedContent);

    if (this.focus) {
      let elem = changedContent.editor.root.parentElement.querySelectorAll(
        '.ql-tooltip'
      )[0];
    }
    this.quillHtmlData = changedContent.html;
    var div = document.createElement('div');
    div.innerHTML = changedContent.html;
    changedContent.text = div.innerText;
    var parser = new DOMParser();
    var doc = parser.parseFromString(
      this.elm.nativeElement.innerHTML,
      'text/html'
    );
    var codes = doc.querySelectorAll('code');
    if (this.quillConfig.name === 'fib-text') this.addUids();
    if (this.templateType === 'token-highlight') this.addHighlightTextsId();
    let count = 0;
    if (codes.length > 0) {
      codes.forEach(element => {
        if (this.newResFlag && count < codes.length) {
          this.getUpdatedContent.emit(changedContent.html);
          this.questionEditorService.updateNewResponseFlag(false);
        } else if (element.id) {
          count++;
          if (count === codes.length) {
            this.getUpdatedContent.emit(changedContent.html);
          }
        }
      });
    } else {
      this.getUpdatedContent.emit(changedContent.html);
    }
    // }
    // this.getUpdatedContent.emit(changedContent.html);
    if (this.rangeIndex) {
      let textRange = changedContent.text.length;
      this.quillInstance.setSelection(this.rangeIndex + 2, Quill.sources.API);
      this.rangeIndex = undefined;
    }
  }

  previewQuillChanged(changedContent): void {
    this.updateQuillData = changedContent.html;
    var div = document.createElement('div');
    div.innerHTML = changedContent.html;
    changedContent.text = div.innerText;
    this.getQuillUpdated.emit(changedContent.html);
  }

  setFocus(editor) {
    editor.focus();
  }

  onFocusInput(e?) {
    //console.log('focus ', e, this.quillConfig.name);
    if (this.quillConfig.name === 'fib-text') {
      this.questionEditorService.updateAllowAddResponse(true);
    } else {
      this.questionEditorService.updateAllowAddResponse(false);
    }
    let index = e.editor.getSelection().index;
    let elem = e.editor.root.parentElement.querySelectorAll('.ql-tooltip')[0];
    this.focus = true;
    this.getIndex.emit(index);
    this.questionEditorService.setInstance(e.editor);
    this.questionEditorService.setComponentName(this.quillConfig.name);
  }

  onBlur(e) {
    this.getUpdatedContentFocus.emit(this.quillHtmlData);
  }

  onBlurInput(e) {
    closeEditor();
    this.focus = false;
  }

  onEquationAdd(e) {
    this.activeObj = {
      elem: e
    };
    this.showEditor = true;
  }

  onCloseEditor() {
    this.showEditor = false;
  }

  clickKey(e) {
    //console.log('clickkey ', e);
    if (
      this.templateType === 'token-highlight' &&
      e.target.localName === 'code'
    ) {
      alert(e.target.id);
    }
  }

  ngOnDestroy() {
    this.questionEditorService.updateResponseIds([]);
    this.questionEditorService.updateResponses({});
    this.destroy$.next(true);
  }
}
