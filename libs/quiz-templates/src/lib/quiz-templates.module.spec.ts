import { async, TestBed } from '@angular/core/testing';
import { QuizTemplatesModule } from './quiz-templates.module';

describe('QuizTemplatesModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [QuizTemplatesModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(QuizTemplatesModule).toBeDefined();
  });
});
