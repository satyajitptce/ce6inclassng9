import {
  Component,
  OnInit,
  Output,
  Renderer2,
  Input,
  EventEmitter,
  ViewChild,
  ElementRef,
  AfterViewInit,
  QueryList,
  ChangeDetectorRef,
  SimpleChanges,
  OnDestroy
} from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import { TemplateMcqData } from 'libs/quiz-templates/src/lib/core/interface/quiz-player-template.interface';
import { SharedComponentService } from '../../../core/services/shared-component.service';
import { QuestionEditorService } from '@tce/template-editor';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-fib-set-correct-ans-options-layout',
  templateUrl: './fib-set-correct-ans-options-layout.component.html',
  styleUrls: ['./fib-set-correct-ans-options-layout.component.scss']
})
export class FibSetCorrectAnsOptionsLayoutComponent
  implements OnInit, OnDestroy {
  @Input() public optData: object;
  @Input() public previewState: boolean;
  @Input() public inputName: string;
  @Input() public templateMarkUpData: string;
  @Input() public optValue: Array<string>;
  @Input() public type: string;
  @Input() public sourceData: TemplateMcqData;
  @Input() public showAnsState: object;
  @Input() public possibleResponses: Array<Array<string>>;
  // @Input() public sourceState: BehaviorSubject<boolean>;
  @Input() public updateResponse: BehaviorSubject<boolean>;
  @ViewChild('correctAnsContainer', { static: true }) container: ElementRef;
  public defaultHtml: any = '';
  public editorState: boolean = false;
  // public quillLoaded: boolean = false;
  public mode: boolean;
  // public comp: ComponentRef<any>;
  @Output() pushSelectedAns = new EventEmitter();
  @Output() correctPoints = new EventEmitter();
  // @Output() correctAnswer = new EventEmitter();
  public templateData: any;
  public templateMatchArr = [];
  public fibTextValue: Array<string> = [];
  public fibTextValuePreview: Array<any> = [];
  public response: any;
  public selectedPreview: Array<string>;
  public getShowAnsState: BehaviorSubject<object> = new BehaviorSubject<object>(
    {}
  );

  private inputText: string;
  public showCorrectAnswer: boolean;
  public correctAnswer: Array<string>;
  public selected: Array<string>;
  public responses: Object;
  public responseIds: Array<any>;
  destroy$: Subject<boolean> = new Subject<boolean>();
  private listenFunc: Function;

  constructor(
    private renderer: Renderer2,
    private sanitizer: DomSanitizer,
    public changeRef: ChangeDetectorRef,
    public sharedComponentService: SharedComponentService,
    private questionEdtorService: QuestionEditorService,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    // console.log('Random 1', this.random);
    console.log('on init in set correct', this.showAnsState);

    this.questionEdtorService
      .getResponses()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.responses = res;
        console.log('responses in fib', this.responses, res);
      });
    this.questionEdtorService
      .getResponseIds()
      .pipe(takeUntil(this.destroy$))
      .subscribe(ids => {
        this.responseIds = ids;
        console.log('res Ids', this.responseIds, ids);

        this.render();
      });

    this.showAnsState = this.questionEdtorService
      .getSubmitAnsShow()
      .pipe(takeUntil(this.destroy$))
      .subscribe(state => {
        this.showCorrectAnswer = state;
        this.correctIncorrect();
      });

    console.log('TYPE: ', this.type);
    setTimeout(() => {
      console.log('responses in fib', this.responseIds);
    }, 5000);

    this.inputText = this.templateMarkUpData;
    console.log('possible Responses: ', this.possibleResponses);
    if (this.sourceData) {
      console.log('sourceData ', this.sourceData);

      this.fibTextValue = this.sourceData.data.validation.valid_response.value;
    }
    this.selectedPreview = [...this.fibTextValue];
    console.log('SelPreview: ', this.selectedPreview);

    // this.previewState.subscribe((mode: boolean) => {
    this.mode = this.previewState;
    if (!this.mode) {
      this.showCorrectAnswer = false;
      // this.correctIncorrect();
    }
    this.fibTextValuePreview = [];
    console.log('mode: ', this.mode);
    // });

    // this.showAnsState.subscribe((data: object) => {

    // console.log('ansState: ', data['state']);
    this.correctAnswer = this.showAnsState['selectedAnswers'];
    // });

    this.updateResponse.pipe(takeUntil(this.destroy$)).subscribe(data => {
      this.render();
    });

    // this.render();
    // this.fibTextValue = [];
    this.listenFunc = this.renderer.listen(
      this.elementRef.nativeElement,
      'keyup',
      (e: any) => {
        if (this.type == 'fib-text') {
          console.log('eventlistener');
          console.log(
            'all responses',
            this.responses,
            this.responseIds,
            this.fibTextValuePreview
          );

          // if (e.target.id.indexOf('-fib') > -1) {
          let tagId = e.target.id;
          console.log(
            'tagId ',
            tagId,
            this.responseIds,
            this.responseIds.findIndex(id => id == e.target.id)
          );

          let indexPosition = this.responseIds.findIndex(
            id => id == e.target.id
          );
          console.log('indexPosition ', indexPosition);
          this.fibTextValuePreview[indexPosition] = e.target.value;
          console.log(
            'fibfibfib ',
            this.fibTextValue,
            this.fibTextValuePreview,
            indexPosition
          );

          this.templateMatchArr = this.inputText.match(/{{response}}/g) || [];
          let totalOptions = this.fibTextValue.length;
          let lowerFibTextValue = [];
          let lowerFibTextValuePreview = [];
          for (let i = 0; i < this.fibTextValuePreview.length; i++) {
            console.log(
              'fibfibfib inside',
              i,
              this.fibTextValue[i],
              this.fibTextValuePreview.length
            );

            lowerFibTextValue.push(
              this.fibTextValue[i].toLocaleLowerCase().trim()
            );
            if (this.fibTextValuePreview[i]) {
              lowerFibTextValuePreview.push(
                this.fibTextValuePreview[i].toLocaleLowerCase().trim()
              );
            }
          }

          let stringDifference = this.sharedComponentService.getDifferenceofStrings(
            lowerFibTextValue,
            lowerFibTextValuePreview,
            totalOptions
          );
          console.log(
            'stringDifference',
            stringDifference,
            lowerFibTextValue,
            lowerFibTextValuePreview
          );

          this.correctPoints.emit(stringDifference);
        }
        this.correctIncorrect();
        // }
        // }
      }
    );
  }

  //Function to render the html
  render() {
    console.log('Random 2', this.responseIds);

    console.log('Template Markup Data', this.templateMarkUpData);
    if (this.templateMarkUpData) {
      this.response = this.sourceData.data.possible_responses;
      this.templateMarkUpData = this.sourceData.data.template;
      this.templateMatchArr =
        this.templateMarkUpData.match(/{{response}}/g) || [];
      console.log('ResponseFIB', this.response);
      this.fibTextValue = this.sourceData.data.validation.valid_response.value;

      if (this.templateMatchArr && this.templateMatchArr.length > 0) {
        this.templateMatchArr.forEach((template, index) => {
          if (!this.fibTextValue[index]) {
            this.fibTextValue[index] = '';
          }

          if (this.type == 'fib-text') {
            // this.fibTextValue.push(null);
            let span = this.renderer.createElement('span');
            this.renderer.setStyle(span, 'position', 'relative');
            let textInput = this.renderer.createElement('input');
            this.renderer.setAttribute(textInput, 'type', 'text');
            let span1 = this.renderer.createElement('span');
            if (this.responseIds) {
              console.log('inside if ', this.responses, this.responseIds);

              // this.renderer.setAttribute(textInput, 'id', `${index + 1}-fib`);
              this.renderer.setAttribute(
                textInput,
                'id',
                this.responseIds[index]
              );

              this.renderer.setAttribute(
                span1,
                'id',
                `${this.responseIds[index]}-fib-span`
              );
            }
            this.renderer.appendChild(span, textInput);

            this.renderer.appendChild(span, span1);
            let spanOuterHtml = span.outerHTML;
            this.templateMarkUpData = this.templateMarkUpData.replace(
              '{{response}}',
              spanOuterHtml
            );
          }
          if (this.type == 'fib-dropdown') {
            console.log('responses: ', this.response, this.templateMarkUpData);

            let span = this.renderer.createElement('span');
            this.renderer.setStyle(span, 'position', 'relative');
            let select = this.renderer.createElement('select');
            this.renderer.setAttribute(select, 'id', `${index}`);
            this.renderer.appendChild(span, select);
            let opt1 = this.renderer.createElement('option');
            this.renderer.setAttribute(opt1, 'value', '');
            let selectText = this.renderer.createText('');
            this.renderer.appendChild(opt1, selectText);
            let span1 = this.renderer.createElement('span');
            this.renderer.setAttribute(
              span1,
              'id',
              `${index + 1}-fib-drop-span`
            );
            this.renderer.appendChild(span, span1);
            // this.renderer.setValue(opt1, 'select');
            this.renderer.appendChild(select, opt1);
            if (this.response) {
              if (this.response[index]) {
                this.response[index].forEach((o, rindex) => {
                  console.log('options --->', o);
                  // let opt = `opt-${index}`;
                  let opt = this.renderer.createElement('option');
                  let optText = this.renderer.createText(o['label']);
                  this.renderer.setProperty(opt, 'innerHTML', o['label']);
                  this.renderer.setAttribute(opt, 'id', `index`);
                  this.renderer.setAttribute(opt, 'value', o['value']);
                  console.log('fibText -->', this.fibTextValue[index]);
                  if (!this.mode && o['value'] === this.fibTextValue[index]) {
                    this.renderer.setAttribute(opt, 'selected', 'true');
                  }
                  console.log('option 2 ---> ', optText);
                  this.renderer.appendChild(select, opt);
                });
              }
            }

            let spanOuterHtml = span.outerHTML;
            this.templateMarkUpData = this.templateMarkUpData.replace(
              '{{response}}',
              spanOuterHtml
            );
          }

          if (this.type == 'fib-drag-drop') {
            let div = this.renderer.createElement('div');
            this.renderer.addClass(div, 'fibDiv');
            this.renderer.setAttribute(div, 'id', `${index}`);
            this.renderer.listen(div, 'click', event => {
              this.clickComp(event);
            });
            let spanOuterHtml = div.outerHTML;
            this.templateMarkUpData = this.templateMarkUpData.replace(
              '{{response}}',
              spanOuterHtml
            );
          }
        });
      } else {
      }

      this.templateData = this.sanitizer.bypassSecurityTrustHtml(
        this.templateMarkUpData
      );
    }
  }

  clickComp(event) {
    alert(event);
  }

  textClick(event?) {
    console.log('localName ', event);

    if (event && event.target.localName === 'p') {
      this.questionEdtorService.updateQuillLoading(false);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('Changes', changes);
    if (this.showCorrectAnswer) this.correctIncorrect();
    if (changes.templateMarkUpData) {
      this.templateData = changes.templateMarkUpData.currentValue;
      this.inputText = this.templateData;
      this.render();
    }
  }
  ngOnDestroy() {
    this.destroy$.next(true);
    this.listenFunc();
    console.log('fibdestroy');
    // this.previewState.unsubscribe();
    // this.showAnsState.unsubscribe();
    // window.removeEventListener('change',()=>{
    //   console.log('EVent removed!')
    // })
  }

  correctIncorrect() {
    if (this.mode) {
      // if (this.type == 'fib-text') {
      if (this.fibTextValue) {
        console.log('correct incorrect if 1');

        this.fibTextValue.forEach((value, index) => {
          let text: any;
          let span: any;
          if (this.type == 'fib-text') {
            text = document.getElementById(this.responseIds[index]);
            span = document.getElementById(
              `${this.responseIds[index]}-fib-span`
            );
            console.log(
              'text span ',
              text,
              span,
              this.showCorrectAnswer,
              this.correctAnswer
            );
          } else {
            text = document.getElementById(this.responseIds[index]);
            span = document.getElementById(
              `${this.responseIds[index]}-fib-drop-span`
            );
          }
          if (this.showCorrectAnswer) {
            console.log('text span 1', text, span);

            if (
              this.fibTextValuePreview[index] &&
              value.toLocaleLowerCase().trim() ===
                this.fibTextValuePreview[index].toLocaleLowerCase().trim()
            ) {
              // console.log('text span 1', text, span);
              if (text && text.classList.contains('incorrect')) {
                this.renderer.removeClass(text, 'incorrect');
              }
              if (span && span.classList.contains('incorrect-div')) {
                this.renderer.removeClass(span, 'incorrect-div');
              }
              if (text) {
                this.renderer.addClass(text, 'correct');
              }
              if (span) {
                this.renderer.addClass(span, 'correct-div');
              }
            } else {
              if (text && text.classList.contains('correct')) {
                this.renderer.removeClass(text, 'correct');
              }
              if (span && span.classList.contains('correct-div')) {
                this.renderer.removeClass(span, 'correct-div');
              }
              if (text) {
                this.renderer.addClass(text, 'incorrect');
              }
              if (span) {
                this.renderer.addClass(span, 'incorrect-div');
              }
            }
          } else {
            if (text && text.classList.contains('incorrect')) {
              this.renderer.removeClass(text, 'incorrect');
            }
            if (span && span.classList.contains('incorrect-div')) {
              this.renderer.removeClass(span, 'incorrect-div');
            }
            if (text && text.classList.contains('correct')) {
              this.renderer.removeClass(text, 'correct');
            }
            if (span && span.classList.contains('correct-div')) {
              this.renderer.removeClass(span, 'correct-div');
            }
          }
          console.log('text span 2', text, span);
        });
      }
    } else {
      this.fibTextValue.forEach((value, index) => {
        let text: any;
        let span: any;
        if (this.type == 'fib-text') {
          text = document.getElementById(this.responseIds[index]);
          span = document.getElementById(`${this.responseIds[index]}-fib-span`);
        } else {
          text = document.getElementById(this.responseIds[index]);
          span = document.getElementById(
            `${this.responseIds[index]}-fib-drop-span`
          );
        }
        if (text && text.classList.contains('incorrect')) {
          this.renderer.removeClass(text, 'incorrect');
        }
        if (span && span.classList.contains('incorrect-div')) {
          this.renderer.removeClass(span, 'incorrect-div');
        }
        if (text && text.classList.contains('correct')) {
          this.renderer.removeClass(text, 'correct');
        }
        if (span && span.classList.contains('correct-div')) {
          this.renderer.removeClass(span, 'correct-div');
        }
      });
    }
  }
}
