import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddStemsLayoutComponent } from './add-stems-layout/add-stems-layout.component';

@NgModule({
  declarations: [AddStemsLayoutComponent],
  imports: [CommonModule],
  exports: [AddStemsLayoutComponent]
})
export class AddStemsModule {}
