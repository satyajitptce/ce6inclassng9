import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TagResponseLayoutComponent } from './tag-response-layout.component';

describe('TagResponseLayoutComponent', () => {
  let component: TagResponseLayoutComponent;
  let fixture: ComponentFixture<TagResponseLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TagResponseLayoutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagResponseLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
