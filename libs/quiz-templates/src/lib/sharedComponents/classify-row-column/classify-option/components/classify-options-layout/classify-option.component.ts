import {
  Component,
  OnInit,
  ComponentRef,
  SimpleChanges,
  EventEmitter,
  Output,
  ViewContainerRef,
  Renderer2,
  ChangeDetectorRef,
  Input,
  ViewChild,
  ElementRef
} from '@angular/core';
import { ClassifyMatchOption } from 'libs/quiz-templates/src/lib/core/interface/quiz-player-template.interface';
import { BehaviorSubject } from 'rxjs';
import { SharedComponentService } from '../../../../core/services/shared-component.service';
@Component({
  selector: 'app-classify-option',
  templateUrl: './classify-option.component.html',
  styleUrls: ['./classify-option.component.scss']
})
export class ClassifyOptionComponent implements OnInit {
  @Input() public optData: ClassifyMatchOption;
  @Input() public previewState: BehaviorSubject<boolean>;
  @Input() public showAnsState: BehaviorSubject<object>;
  @Input() public optValue: Array<any>;
  @Input() public tempData: object;
  @Input() public type: string;
  // @Input() public colIndex: any;
  // @Input() public rowIndex: any;
  @Input() public previewTemplateOptionValue: Array<any>;
  @Input() public optArray: Array<ClassifyMatchOption>;
  @ViewChild('myContent', { static: true }) public myContent: ElementRef;
  @Output() onContentUpdate = new EventEmitter();
  @Output() onSelectedAnswersPreview = new EventEmitter();
  public mode: boolean;
  public showCorrectAnswer: boolean;

  public optionRowIndex: any;
  public optionColIndex: any;

  public receivedData: any;
  constructor(
    private renderer: Renderer2,
    private sharedComponentService: SharedComponentService,
    private cdr: ChangeDetectorRef
  ) {}
  ngOnInit() {
    if (!this.previewTemplateOptionValue) {
      this.previewTemplateOptionValue = [];
    }

    // if(this.tempData)
    this.previewState.subscribe((mode: boolean) => {
      this.mode = mode;
    });
    this.showAnsState.subscribe((data: object) => {
      this.showCorrectAnswer = data['state'];
      // this.correctAnswer = data['selectedAnswers'];
      // console.log("Class opt", this.optData, this.rowIndex, this.colIndex)
      // this.optionColIndex = this.colIndex;
      // this.optionRowIndex = this.rowIndex;
    });
  }

  getClickStatus(): boolean {
    if (
      this.tempData &&
      this.tempData['value'] &&
      this.tempData['value'].length > 0 &&
      this.tempData['value'] == this.optData.value
    ) {
      return true;
    } else {
      return false;
    }
  }

  selectPoints(): boolean {
    let optionArray = [];
    this.optArray.forEach(async optArr => {
      optionArray.push(optArr.value);
    });
    // console.log(this.sharedComponentService.getDifferenceOfArray(optionArray,this.optValue));
    let arr1 = String(optionArray);
    let arr2 = String(this.optValue);
    if (arr1 == arr2) {
      // this.onSelectedAnswersPreview.emit(true);
      return true;
    } else {
      // this.onSelectedAnswersPreview.emit(false);
      return false;
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    // if(this.mode) {
    //   if(changes.optValue) {
    //     this.correctAnswer = changes.optValue.currentValue;
    //   }
    //   this.onSelectedAnswersPreview.emit(this.selectPoints());
    // }
  }

  ngAfterViewInit(): void {
    // this.renderLabel();
    this.cdr.detectChanges();
  }
  /**
   * @description This function checks whether preview mode is on or off & accordingly sets the editorMode
   * @returns void
   */
  onContainerClick(): void {
    this.onContentUpdate.emit(this.optData.value);
  }

  count: 0;
  /**
   * @description This function returns a particular class to the html
   * @returns string;
   */
  getOptionClass(): string {
    let value;

    // if(this.optArray.length <= this.count) {
    if (this.getClickStatus()) {
      value = 'select';
    }
    if (this.showCorrectAnswer) {
      let check = 0;
      let emptyCheck = 0;
      let existCheck = 0;

      if (this.type !== 'fib') {
        for (let rowIndex = 0; rowIndex < this.optValue.length; rowIndex++) {
          let opt = this.optValue[rowIndex];

          for (let colIndex = 0; colIndex < opt.length; colIndex++) {
            let row = opt[colIndex];

            if (row.length > 0) {
              emptyCheck = 1;
              for (let index = 0; index < row.length; index++) {
                let col = row[index];

                if (col == this.optData.value) {
                  existCheck = 1;
                  check += this.sharedComponentService.getDifferenceOfArray(
                    this.optValue[rowIndex][colIndex],
                    this.previewTemplateOptionValue[rowIndex][colIndex]
                  ).length;

                  if (check > 0) {
                    let result = this.optValue[rowIndex][colIndex].filter(opt =>
                      this.previewTemplateOptionValue[rowIndex][colIndex].some(
                        prev => opt === prev
                      )
                    );

                    result.forEach(res => {
                      if (res == this.optData.value) {
                        check = 0;
                      }
                    });
                  }
                }
              }
            }
          }
        }

        // console.log("Check options", check, emptyCheck, existCheck, this.optData)
        if (check > 0 || emptyCheck == 0 || existCheck == 0) {
          value = 'incorrect-ans';
        } else {
          value = 'correct-ans';
        }
      }
    }

    return value;
  }
  ngOnDestroy() {}
}
