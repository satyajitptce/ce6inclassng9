import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QstemComponent } from './qstem.component';

describe('QstemComponent', () => {
  let component: QstemComponent;
  let fixture: ComponentFixture<QstemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QstemComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QstemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
