import {
  Component,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ElementRef,
  Renderer2,
  ComponentRef,
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { DomSanitizer } from '@angular/platform-browser';
import {
  TemplateMcqData,
  FibTextDropdown
} from 'libs/quiz-templates/src/lib/core/interface/quiz-player-template.interface';
import { SharedComponentService } from '../../../core/services/shared-component.service';
import { QuestionEditorService } from '@tce/template-editor';
import { QuillEditorComponent } from 'libs/quiz-templates/src/lib/sharedEditors/quill-component/components/quill-component-layout/quill-editor.component';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-template-markup-layout',
  templateUrl: './template-markup-layout.component.html',
  styleUrls: ['./template-markup-layout.component.scss']
})
export class TemplateMarkupLayoutComponent implements OnInit {
  @ViewChild('quillMarkUpContainer', { static: false, read: ViewContainerRef })
  public quillMarkUpContainer: ViewContainerRef;
  @ViewChild('myContentMarkUp', { static: true })
  public myContentMarkUp: ElementRef;
  @Input() public templateData: string;
  @Input() public templateType: string;
  @Input() public previewState: boolean;
  @Input() public inputType: string;
  @Input() public source: FibTextDropdown;
  @Input() public inputName: string;
  @Input() public type: string;
  @Output() public onContentUpdate = new EventEmitter();
  public defaultHtml: any = '';
  public editorState: boolean = false;
  public quillLoaded: boolean = false;
  public mode: boolean = true;
  public comp: ComponentRef<any>;
  public matchedArray: Array<string> = [];
  public response: Array<Array<string>> = [];
  public indexPosition: number = 0;
  public inputData: string;
  public responseSpan: any;
  public responses: Object = {};
  public responseIds: Array<any> = [];
  @ViewChild(QuillEditorComponent, { static: false })
  public quillComp: QuillEditorComponent;
  @Output() pushSelectedAns = new EventEmitter();
  public correctAnswers: Array<any> = [];
  destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private renderer: Renderer2,
    private sharedComponentService: SharedComponentService,
    private elementRef: ElementRef,
    public sanitizer: DomSanitizer,
    private questionEdtorService: QuestionEditorService,
    private elm: ElementRef
  ) {}
  ngOnInit() {
    this.correctAnswers = this.source.data.validation.valid_response.value;

    this.questionEdtorService
      .getResponses()
      .pipe(takeUntil(this.destroy$))
      .subscribe(res => {
        this.responses = res;
      });
    this.questionEdtorService
      .getResponseIds()
      .pipe(takeUntil(this.destroy$))
      .subscribe(ids => {
        this.responseIds = ids;
      });
    this.inputData = this.templateData;

    window.addEventListener('click', e => {
      this.renderData();
    });

    // window.addEventListener('keyup', e => {
    //   console.log('window typed ', e);
    // });
  }

  ngAfterViewInit(): void {
    this.mode = this.previewState;
    this.renderData();
    this.labelUpdate();
  }

  /**
   * @description This function replaces the Response tag in the string and replaces it with a input text in the html
   * @returns void
   */
  renderData() {
    if (this.templateData) {
      this.templateData = this.inputData;
      this.matchedArray = this.templateData.match(/{{response}}/g) || [];
      let cAnswers: Array<string> = [];
      this.matchedArray.forEach((matchEl, index) => {
        let responseSpan = this.renderer.createElement('code');
        let spanText = '';
        if (this.correctAnswers.length > 0) {
          spanText = this.renderer.createText(
            ` ${this.correctAnswers[index]} `
          );
        } else {
          spanText = this.renderer.createText(` RESPONSE `);
          cAnswers.push(` RESPONSE `);
        }
        this.renderer.appendChild(responseSpan, spanText);
        let spanOuterHtml = responseSpan.outerHTML;
        this.templateData = this.templateData.replace(
          '{{response}}',
          spanOuterHtml
        );
      });
    }
  }

  addTags(e) {
    if (this.inputData) {
      let match = this.inputData.match(/{{response}}/g) || [];
      let length = match.length - 1;
      let choice = [
        { label: 'Choice A', value: '0' },
        { label: 'Choice B', value: '1' }
      ];
      if (this.type == 'fib-dropdown') {
        this.source.data.possible_responses.splice(length, 0, choice);
      }

      this.renderData();
      this.labelUpdate();
    }
  }

  deleteTag() {}

  /**
   * @description Function to update label in the html
   * @param updatedTemplate Type = string
   */
  labelUpdate() {
    if (this.mode && this.myContentMarkUp) {
      this.renderer.setProperty(
        this.myContentMarkUp.nativeElement,
        'innerHTML',
        this.templateData
      );

      this.defaultHtml = this.myContentMarkUp.nativeElement;
      this.defaultHtml = this.defaultHtml.outerHTML;
    } else {
      let div = this.renderer.createElement('div');
      this.renderer.setProperty(div, 'innerHTML', this.templateData);
      this.defaultHtml = div.outerHTML;

      // prompt('Template Data', JSON.stringify(this.templateData));
      this.onContainerClick();
    }
  }

  /**
   * @description This function checks whether preview mode is on or off & accordingly sets the editorMode
   * @returns void
   */
  onContainerClick() {
    if (!this.mode) {
      this.editorState = true;
      this.loadQuill();
    }
  }

  /**
   * @description Gets random number within a range
   * @param min Type = number
   * @param max Type = number
   * @returns number
   */
  randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
  }

  selectedIndex: any;
  /**
   * @description This function loads the quill component with @Input & @Output
   * @returns void
   */
  async loadQuill() {
    // alert(this.quillLoaded);

    if (!this.quillLoaded) {
      this.comp = await this.sharedComponentService.loadDynamicEditorModule(
        'quillLoader',
        this.quillMarkUpContainer
      );
      if (this.comp instanceof ComponentRef) {
        this.quillLoaded = true;
        this.comp.instance.quillConfig = { name: 'fib-text' };
        this.comp.instance.quillHtmlData = this.defaultHtml;
        this.comp.instance.templateType = this.templateType;
        this.comp.instance.allowResponseTag = true;
        this.comp.instance.getUpdatedContent
          .pipe(takeUntil(this.destroy$))
          .subscribe(data => {
            let matches =
              data && data.match(/<code/g) ? data.match(/<code/g) : [];
            var parser = new DOMParser();
            var doc = parser.parseFromString(data, 'text/html');
            let codeRef = doc.querySelectorAll('code');
            let correctAnswers = [];
            codeRef.forEach(code => {
              correctAnswers.push(code.innerText);
            });
            this.pushSelectedAns.emit(correctAnswers);

            this.matchedArray =
              data && data.match(/RESPONSE/g) ? data.match(/RESPONSE/g) : [];
            if (matches) {
              matches.forEach((matchEl, index) => {
                if (codeRef.length < this.responseIds.length) {
                  let newcodeRef = this.elm.nativeElement.querySelectorAll(
                    'code'
                  );

                  let codeRefIds = [];
                  this.responses = {};
                  codeRef.forEach(code => {
                    codeRefIds.push(code.id);
                    this.responses[code.id] = code.innerText;
                  });
                  this.responseIds = codeRefIds;

                  this.questionEdtorService.updateResponseIds(this.responseIds);
                  this.questionEdtorService.updateResponses(this.responses);
                }
                if (this.responses[this.responseIds[index]]) {
                  this.responses[this.responseIds[index]] =
                    codeRef[index].innerText;
                }
                let currentText = codeRef[index].innerText;

                data = data.replace(
                  `<code id="${this.responseIds[index]}">${currentText}</code>`,
                  '{{response}}'
                );
                data = data.replace(
                  `<code id="${this.responseIds[index]}" class="">${currentText}</code>`,
                  '{{response}}'
                );
                data = data.replace(
                  `<code>${currentText}</code>`,
                  '{{response}}'
                );
              });

              let totalData = {
                rdata: data,
                response: this.source.data.possible_responses
              };
              this.templateData = data;
              this.onContentUpdate.emit(totalData);
            }
            this.inputData = data;
            this.renderData();
            this.labelUpdate();
          });

        this.comp.instance.getIndex
          .pipe(takeUntil(this.destroy$))
          .subscribe(index => {
            this.indexPosition = index;
          });

        this.comp.instance.addTag
          .pipe(takeUntil(this.destroy$))
          .subscribe(e => {
            this.addTags(e);
          });
        // this.comp.addUids();
      }
    }
  }

  ngOnDestroy() {
    this.destroy$.next(true);
  }

  iterateText() {
    this.comp.instance.onContentChanged();
  }
}
