import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateMarkupLayoutComponent } from './template-markup-layout.component';

describe('TemplateMarkupLayoutComponent', () => {
  let component: TemplateMarkupLayoutComponent;
  let fixture: ComponentFixture<TemplateMarkupLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateMarkupLayoutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateMarkupLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
