import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointsLayoutComponent } from './points-layout.component';

describe('PointsLayoutComponent', () => {
  let component: PointsLayoutComponent;
  let fixture: ComponentFixture<PointsLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PointsLayoutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointsLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
