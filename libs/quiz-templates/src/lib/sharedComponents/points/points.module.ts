import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PointsLayoutComponent } from './points-layout/points-layout.component';

@NgModule({
  declarations: [PointsLayoutComponent],
  imports: [CommonModule],
  exports: [PointsLayoutComponent]
})
export class PointsModule {}
