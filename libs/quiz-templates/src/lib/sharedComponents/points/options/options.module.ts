import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptionsLayoutComponent } from './options-layout/options-layout.component';

@NgModule({
  declarations: [OptionsLayoutComponent],
  imports: [CommonModule]
})
export class OptionsModule {}
