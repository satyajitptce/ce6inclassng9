import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild
} from '@angular/core';
import {
  AppStateStorageService,
  AuthenticationService,
  PanZoomService,
  ScriptLoaderService,
  ToolbarService,
  ToolType,
  WhiteboardService,
  AppConfigService,
  FileUploadService,
  AuthStateEnum
} from '@tce/core';
import { fromEvent, Observable } from 'rxjs';
import * as svgPanZoom from 'svg-pan-zoom';
import { TimelineLite, TweenMax } from 'gsap';
import { WidgetsSharedService } from '@tce/player-widgets';

declare let TweenLite: any;
declare let erase: any;
declare let Snap: any;
declare let simplify: any;
declare let SVG: any;
declare let event: Event;
declare let stopAllTCEMedia: any;
declare let self: any;
declare var $;

@Component({
  selector: 'tce-board',
  templateUrl: './whiteboard.component.html',
  styleUrls: ['./whiteboard.component.scss']
})
export class WhiteboardComponent implements OnInit {
  self = this;
  panZoom: SvgPanZoom.Instance;
  bgPanZoom: SvgPanZoom.Instance;
  
  @Input() states = 'path';
  @Output() myEvent = new EventEmitter();

  @ViewChild('panGroup', { static: true }) panGroupElementRef: ElementRef;

  public wbEvent: any;
  public selectedTool = 'path';
  public xmlns = 'http://www.w3.org/2000/svg';
  public xhtml = 'http://www.w3.org/1999/xhtml';
  public drawingboard = document.getElementById('drawingBoard');
  public panGroup = document.getElementById('panGroup');
  public lastSelectedState = 'path';
  public tweenLite: TweenLite;
  //points and offsets
  public point = { x: 0, y: 0 };
  public startX: any;
  public startY: any;
  public offsetX = 0;
  public offsetY = 0;
  //touch
  public currentTouches = [];
  public multitouch = false;
  //pan
  public isPanning = false;
  //selector
  public selectedElement = null;
  //drawing
  public drawingMode = false;
  public currentDrawing = null;
  public opacity = 0.8;
  public strokeWidth: any;
  public strokeColor: string;
  public strPath = '';
  //eraser
  public erasingMode = false;
  public eraserSize = 50;
  public EraseByElementSelection: any;
  public eraser = null;
  public eraserCursorPath = null;
  public eraserCursorPathOpacity = 0;
  //laser
  public laserMode = false;
  public laserCursorPath = null;
  public laserTimer = null;
  public laserLength = 0;
  public laserDrawFPS = 60;
  public laserPathLength = 0;
  public laserDistancePerPoint = 2;
  public laserStrokeSize = 10;
  public myline = null;
  public isMouseDown = false;
  public whiteBoardData = [];
  //eraser visula
  public eraserCursorIcon = null;
  public eraserCursorIconWidth = 50;
  public eraserCursorIconHeight = 100;
  public eraserCursorIconColor = '#cccccc';
  public eraserCursorOuter = null;
  public eraserCursorInner1 = null;
  public eraserCursorInner2 = null;
  // eraser.js ------------------
  public elementsToBeErased = [];
  public erasePath = [];
  public pathColorsArr = [];
  public pathStrokeWidthArr = [];
  public pathOpacityArr = [];
  // end of eraser.js ----------------------
  public lastMouseX: number;
  public lastMouseY: number;
  public myLastMouseX: number;
  public myLastMouseY: number;
  public currentMousePosition: SvgPanZoom.Point;
  public whiteboardContainer: HTMLDivElement;

  //smooth path
  bufferSize = 8;
  buffer = [];

  public wbUsage = {
    state: false,
    start: null
  };

  private _defaultStrokeColor = '';
  public selectedPenSize: any = 1;
  isLoggedIn: any = false;
  constructor(
    private elRef: ElementRef,
    private toolbarService: ToolbarService,
    private scriptLoader: ScriptLoaderService,
    private panZoomService: PanZoomService,
    private appStateStorageService: AppStateStorageService,
    private authService: AuthenticationService,
    private whiteBoardService: WhiteboardService,
    private appConfigService: AppConfigService,
    private fileUploadService: FileUploadService,
    private widgetsSharedService: WidgetsSharedService
  ) {}

  ngOnInit() {
    
    const defaultpensize = this.appConfigService.getConfig('global_setting')[
      'default-stroke-size'
    ];
    this.EraseByElementSelection = this.appConfigService.getConfig(
      'global_setting'
    )['EraseByElementSelection'];
    this.strokeWidth = defaultpensize;
    this.whiteboardContainer = document.getElementById(
      'whiteboardContainer'
    ) as HTMLDivElement;
    this.addEvents();
    this.authService.userState$.subscribe(userState => {
      this.isLoggedIn = userState.authStatus === AuthStateEnum.AUTH;
    });
    
    this.toolbarService.currentSelectedTool$.subscribe(newToolType => {
      this.panZoom.disablePan();
      //  console.log('newToolType-->> ', newToolType);

      this.selectedTool = newToolType;
      if (
        this.selectedTool === 'path' ||
        this.selectedTool === 'circle' ||
        this.selectedTool === 'rect' ||
        this.selectedTool === 'line'
      ) {
        this.lastSelectedState = this.selectedTool;
      }
    });
    this.toolbarService.currentPenSize.subscribe(data => {
      if (data) {
        this.selectedPenSize = data;
        this.strokeWidth = this.selectedPenSize;
      }
    });
    this.toolbarService.selectedToolColor$.subscribe(color => {
      this.strokeColor = color;
      this.selectedTool = this.lastSelectedState;
      this.toolbarService.selectTool(ToolType.Pen);
    });

    this.toolbarService.penColors$.subscribe(newPenColors => {
      this.setDefaultStrokeColors(newPenColors);
      this.selectedTool = this.lastSelectedState;
      this.toolbarService.selectTool(ToolType.Pen);
    });

    this.toolbarService.clearAnnotationsListener$.subscribe(
      loadNewWhiteboard => {
        this.clearAnnotations();
        if (loadNewWhiteboard) {
          this.checkSavedWhiteboards();
        }
      }
    );

    this.authService.logoutTriggered$.subscribe(() => {
      //console.log("authservice")
      this.saveDrawings(this.whiteBoardData, 'logoutTrigger');
    });

    this.appStateStorageService.saveToWhiteBoard$.subscribe(() => {
      // console.log("appstorage")
      this.saveDrawings(this.whiteBoardData, 'appStorage');
    });

    this.toolbarService.clearAllAnnotationsBroadcaster$.subscribe(() => {
      //console.log("toolbar-clearAllAnnotation")
      this.clearAnnotations();
      this.saveDrawings(this.whiteBoardData, 'clearAnnotation');
    });

    this.toolbarService.clearWhiteboardBroadcaster$.subscribe(() => {
      // console.log("toolbar-clearWhiteboard")
      this.clearAnnotations();
      this.saveDrawings(this.whiteBoardData, 'clearWhiteBoard');
    });
    this.toolbarService.clearWhiteboardSetting$.subscribe(response => {
      this.EraseByElementSelection = response;
    });
    this.widgetsSharedService.getDrawLayout().subscribe(
      response => {
        // console.log('WhiteboardComponent -> ngOnInit -> response', response);
        if (response) {
          this.drawPathFromWidget(response);
        }
      },
      error => {
        console.log('WhiteboardComponent -> saveDrawings -> error', error);
      }
    );
    this.toolbarService.globalPanZoom = this.panZoom;
  }

  saveDrawings(whiteboardData, type) {
    //console.log('WhiteboardComponent -> saveDrawings -> type', type);
    //console.log('TCL: WhiteboardComponent');
    if (this.isLoggedIn && type != 'onMouseLeave') {
      let attributes;
      if (whiteboardData) {
        attributes = whiteboardData.map(data => {
          if (data.element !== null) {
            const elemArrtibutes = data.element.attributes;
            const svgPathAttributes = {};
            for (let i = 0; i < elemArrtibutes.length; i++) {
              if (elemArrtibutes[i].specified) {
                svgPathAttributes[elemArrtibutes[i].nodeName] =
                  elemArrtibutes[i].nodeValue;
              }
            }
            return svgPathAttributes;
          }
        });
      }
      attributes = this.getUniqueItemsFromArray(attributes);
      if (attributes) {
        this.appStateStorageService.saveToLocalStorage({
          drawings: attributes
        });
      }

      this.whiteBoardData = this.getUniqueItemsFromArray(this.whiteBoardData);

      if(!this.wbUsage.state){
        this.wbUsage.state = true;
        this.wbUsage.start = new Date().getTime();
      }else{
        const end = new Date().getTime();
        const duration = Math.round((end-this.wbUsage.start)/1000);
        console.log("🚀 ~ file: whiteboard.component.ts ~ line 269 ~ WhiteboardComponent ~ saveDrawings ~ duration", duration)
      }
    }
  }

  clearAnnotations() {
    if (this.panGroup) {
      while (this.panGroup.firstChild) {
        this.panGroup.removeChild(this.panGroup.firstChild);
      }
    }
    this.whiteBoardData = [];
    //this.saveDrawings(this.whiteBoardData);
  }

  lazyLoadLibs() {
    const fileref = document.createElement('link');
    fileref.setAttribute('rel', 'stylesheet');
    fileref.setAttribute('type', 'text/css');
    fileref.setAttribute('href', 'assets/vendor/svg.select.css');
    document.getElementsByTagName('head')[0].appendChild(fileref);

    this.scriptLoader.loadScript('assets/vendor/svg.js').subscribe(() => {
      this.scriptLoader
        .loadScripts([
          'assets/vendor/erase.js',
          'assets/vendor/simplify.js',
          'assets/vendor/svg.select.js',
          'assets/vendor/svg.resize.js',
          'assets/vendor/svg.draggable.js'
        ])
        .subscribe(() => {
          this.addEvents();
        });
    });
  }

  addEvents() {
    // Listening for mouse move to find mouse position for zoom to mouse point.
    fromEvent(document, 'mousemove').subscribe((e: MouseEvent) => {
      // Improvement: Instead of using this event
      // We might be able to disable the default mousewheel zoom in SvgPanZoom
      // then we could use our own listener on the whiteboard for mousewheel
      // and then fire the SvgPanZoom.zoomToPoint method.
      this.panZoomService.setCurrentMousePosition(this.getMousePosition(e));
    });

    const element = this.elRef.nativeElement.querySelector(
      'div'
    ) as HTMLDivElement;

    element.addEventListener('mousedown', this.onMouseDown.bind(this));
    element.addEventListener('mousemove', this.onMouseMove.bind(this), false);
    element.addEventListener('mouseup', this.onMouseUp.bind(this));

    element.addEventListener('mouseleave', this.onMouseLeave.bind(this));
    element.addEventListener('touchstart', this.onMouseDown.bind(this));
    element.addEventListener('touchmove', this.onMouseMove.bind(this));
    element.addEventListener('touchend', this.onMouseUp.bind(this));

    this.setPanZoom();

    this.panZoom.setBeforeZoom((oldScale, newScale) => {
      const zoomFactor = newScale / oldScale;
      this.panZoomService.movePlayersInRelationToZoomPoint(zoomFactor);
    });

    this.panZoom.setOnZoom(scale => {
      this.panZoomService.setCurrentZoomScale(scale);
      //const zoom = this.panZoom.getZoom();
      this.bgPanZoom.zoom(scale);
      this.bgPanZoom.pan(this.panZoom.getPan());
    });

    this.panZoomService.zoomToPointAndScale$.subscribe(({ point, scale }) => {
      this.panZoom.zoomAtPoint(scale, point);
      //console.log('s--', scale);
      // console.log('p--', point);
    });

    this.panZoom.setBeforePan((oldPoint, newPoint) => {
      this.panZoomService.panCurrentMatrixBy({
        x: newPoint.x - oldPoint.x,
        y: newPoint.y - oldPoint.y
      });
    });

    this.panZoomService.panToPoint$.subscribe(point => {
      this.panZoom.panBy(point);
    });
  }

  getMousePosition(e) {
    return {
      x: e.clientX,
      y: e.clientY
    };
  }

  onMouseDown(e: MouseEvent) {
    this.fileUploadService.setAddResourceFlagBroadcaster('');
    if (this.selectedTool === ToolType.Pan) {
      this.lastMouseX = e.clientX;
      this.lastMouseY = e.clientY;
    }

    this.wbEvent = e;
    this.isMouseDown = true;
    if (this.selectedTool === 'pan') {
      if (typeof stopAllTCEMedia === 'function') {
        stopAllTCEMedia();
      }
    }
    this.defaults();
  }

  onMouseMove(e: any) {
    if (this.isMouseDown) {
      this.wbEvent = e;
      this.continueDrawing(e);
    }
  }

  onMouseUp(e: any) {
    this.isMouseDown = false;
    this.wbEvent = e;
    //console.log("onMouseUp")
    this.endDrawing(e, 'onMouseUp');
  }

  onMouseLeave() {
    //console.log("onMouseLeave")
    this.isPanning = false;
    this.isMouseDown = false;
    if (this.wbEvent) {
      this.endDrawing(this.wbEvent, 'onMouseLeave');
    }
  }

  defaults() {
    this.removeEraser('default');
    this.selectElement();
    this.wbActions();

    /*

    const str: string = this.selectedElement.id.split('_');
    const type: string = str[0];
    if (
      type === 'circle' ||
      type === 'rect' ||
      type === 'line' ||
      type === 'path'
    ) {
      if (this.selectedTool !== 'erase') {
        this.lastSelectedState = type;
        this.createShapeSelection();
      }
    } else {
      this.removeShapeSelection();
      this.wbActions();
    }
    */
  }

  setTouchGestures() {
    const myTouch = this.wbEvent.touches[0];
    const rX = myTouch.radiusX;
    const rY = myTouch.radiusY;
    let brushSize = rX + rY;

    if (brushSize === 0) {
      brushSize = 2;
    }

    brushSize = Math.round(brushSize);
    this.eraserSize = brushSize;
    this.strokeWidth = brushSize / 2;
    let maxBrushSize = 20;

    const is_iPad = navigator.userAgent.match(/iPad/i) != null;
    if (is_iPad) {
      maxBrushSize = 50;
    }
    if (this.selectedTool !== 'pan') {
      if (brushSize > maxBrushSize) {
        this.selectedTool = 'erase';
      } else {
        this.selectedTool = this.lastSelectedState;
      }
    }
  }

  selectElement() {
    if (this.wbEvent.touches) {
      const location = this.wbEvent.changedTouches[0];
      const realTarget = document.elementFromPoint(
        location.pageX,
        location.pageY
      );
      this.selectedElement = realTarget;
    } else {
      this.selectedElement = event.target;
    }
  }

  wbActions() {
    this.point = this.getMousePoint();
    this.startX = this.getPageCordinates().pgX - this.offsetX;
    this.startY = this.getPageCordinates().pgY - this.offsetY;
    switch (this.selectedTool) {
      case 'select':
        this.createShapeSelection();
        break;
      case 'pan':
        this.isPanning = true;
        this.toolbarService.onToolBarActionDone(false);
        break;
      case 'laser':
        this.laserMode = true;
        this.drawLaserByPath();
        break;
      case 'erase':
        this.erasingMode = true;
        if (this.EraseByElementSelection) {
          this.drawEraseBySelection();
        } else {
          this.drawEraseByPath();
        }
        break;
      default:
        this.startDrawing();
    }
  }

  startDrawing() {
    this.whiteBoardService.setWhiteboardActive(true);
    this.drawingMode = true;
    if (this.isMultiTouch()) {
      //console.log("IS-TOUCH")
      this.drawPathByMultiTouch();
    } else {
      this.drawPathByMouse();
    }
    this.drawDefaultsForTouchAndMouse();
  }

  drawPathByMultiTouch() {
    //let event: Event;
    //console.log("1 - event -->> ",event)
    this.panGroup = document.getElementById('panGroup');
    const touches = this.wbEvent.changedTouches;
    for (let i = 0; i < touches.length; i++) {
      const touch = touches[i];
      this.currentDrawing = document.createElementNS(this.xmlns, 'path');
      if (this.selectedTool === 'path') {
        this.currentDrawing.setAttributeNS(null, 'fill', 'none');
      }
      const x = this.getMultiTouchPoint(touch).x;
      const y = this.getMultiTouchPoint(touch).y;
      //  console.log('down-' + x + ' ' + y);
      this.strPath = 'M' + x + ' ' + y;
      this.panGroup.appendChild(this.currentDrawing);
      this.currentTouches.push({
        id: touch.identifier,
        pageX: x,
        pageY: y,
        strPath: this.strPath,
        element: this.currentDrawing
      });
      this.saveData(this.currentDrawing);
    }
  }
  drawPathByMouse = function() {
    this.panGroup = document.getElementById('panGroup');
    this.currentDrawing = document.createElementNS(this.xmlns, 'path');
    this.currentDrawing.setAttributeNS(null, 'fill', 'none');
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.panGroup.appendChild(this.currentDrawing);
    if (typeof stopAllTCEMedia === 'function') {
      stopAllTCEMedia();
    }
  };

  drawDefaultsForTouchAndMouse() {
    this.currentDrawing.setAttributeNS(null, 'stroke-width', this.strokeWidth);
    this.currentDrawing.setAttributeNS(
      null,
      'id',
      'path_' + this.getRandomID()
    );
    this.currentDrawing.setAttributeNS(null, 'class', 'draggable');
    this.currentDrawing.setAttributeNS(null, 'opacity', this.opacity);
    this.currentDrawing.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.currentDrawing.setAttributeNS(null, 'stroke-linecap', 'round');
    this.currentDrawing.setAttributeNS(null, 'stroke', this.strokeColor);
    this.currentDrawing.setAttributeNS(null, 'fill', 'none');
    this.currentDrawing.setAttributeNS(null, 'fill-opacity', '0');
    this.currentDrawing.setAttributeNS(null, 'myStrokeWidth', this.strokeWidth);
    this.currentDrawing.setAttributeNS(null, 'myStrokeColor', this.strokeColor);
    this.currentDrawing.setAttributeNS(null, 'myStrokeOpacity', this.opacity);
    this.panGroup.appendChild(this.currentDrawing);

    this.buffer = [];
    this.appendToBuffer(this.getMousePoint());
    // add a dot on mousedown
    this.strPath +=
      ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
    this.currentDrawing.setAttribute('d', this.strPath);
  }

  drawEraseBySelection() {
    this.eraserSize = 2;
    this.panGroup = document.getElementById('panGroup');
    this.eraserCursorPath = document.createElementNS(this.xmlns, 'path');
    this.eraserCursorPath.setAttributeNS(null, 'id', 'eraserCursorPath');
    this.eraserCursorPath.setAttributeNS(null, 'class', 'eraserCursorPath');
    this.eraserCursorPath.setAttributeNS(null, 'stroke', 'gray');
    this.eraserCursorPath.setAttributeNS(null, 'opacity', '0.6');
    this.eraserCursorPath.setAttributeNS(null, 'fill', 'transparent');
    this.eraserCursorPath.setAttributeNS(null, 'stroke-width', this.eraserSize);
    this.eraserCursorPath.setAttributeNS(null, 'pointer-events', 'none');
    this.eraserCursorPath.setAttributeNS(null, 'stroke-dasharray', '10');
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.panGroup.append(this.eraserCursorPath);
    this.erasePath = [];
    this.elementsToBeErased = [];
  }

  drawLaserByPath() {
    this.eraserSize = this.eraserCursorIconHeight; //* this.eraserCursorIconWidth
    this.panGroup = document.getElementById('panGroup');
    this.laserCursorPath = document.createElementNS(this.xmlns, 'polyline');
    this.laserCursorPath.setAttributeNS(null, 'id', 'laserCursorPath');
    this.laserCursorPath.setAttributeNS(null, 'class', 'mylaserCursorPath');
    this.laserCursorPath.setAttributeNS(null, 'stroke', 'red');
    this.laserCursorPath.setAttributeNS(null, 'opacity', '0.6');

    this.laserCursorPath.setAttributeNS(null, 'fill', 'none');
    this.laserCursorPath.setAttributeNS(null, 'stroke-width', 10);
    this.laserCursorPath.setAttribute('stroke-linejoin', 'round');
    this.laserCursorPath.setAttribute('stroke-linecap', 'round');
    this.laserCursorPath.setAttributeNS(null, 'pointer-events', 'none');
    this.strPath = this.point.x + ' ' + this.point.y + ',';
    this.panGroup.append(this.laserCursorPath);
    this.whiteboardContainer.style.zIndex = '9999';
  }

  drawEraseByPath() {
    this.eraserSize = this.eraserCursorIconHeight; //* this.eraserCursorIconWidth
    this.panGroup = document.getElementById('panGroup');
    this.eraserCursorPath = document.createElementNS(this.xmlns, 'path');
    this.eraserCursorPath.setAttributeNS(null, 'id', 'eraserCursorPath');
    this.eraserCursorPath.setAttributeNS(null, 'class', 'eraserCursorPath');
    this.eraserCursorPath.setAttributeNS(null, 'stroke', 'gray');
    this.eraserCursorPath.setAttributeNS(
      null,
      'opacity',
      this.eraserCursorPathOpacity
    );
    this.eraserCursorPath.setAttributeNS(null, 'fill', 'none');
    this.eraserCursorPath.setAttributeNS(null, 'stroke-width', this.eraserSize);
    this.eraserCursorPath.setAttribute('stroke-linejoin', 'round');
    this.eraserCursorPath.setAttribute('stroke-linecap', 'round');
    this.eraserCursorPath.setAttributeNS(null, 'pointer-events', 'none');
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.panGroup.append(this.eraserCursorPath);
    this.erasePath = [];
    this.elementsToBeErased = [];
    this.drawEraserIcon();
  }

  drawEraserIcon() {
    const centerX = this.getMousePoint().x - this.eraserCursorIconWidth / 2;
    const centerY = this.getMousePoint().y - this.eraserCursorIconHeight / 2;
    this.eraserCursorIcon = document.createElementNS(this.xmlns, 'g');
    this.eraserCursorIcon.setAttributeNS(null, 'id', 'eraserCursorIcon');

    this.eraserCursorOuter = document.createElementNS(this.xmlns, 'rect');
    this.eraserCursorOuter.setAttributeNS(null, 'id', 'eraserCursorOuter');
    this.eraserCursorOuter.setAttributeNS(
      null,
      'width',
      String(this.eraserCursorIconWidth)
    );
    this.eraserCursorOuter.setAttributeNS(
      null,
      'height',
      String(this.eraserCursorIconHeight)
    );
    this.eraserCursorOuter.setAttributeNS(null, 'x', String(centerX));
    this.eraserCursorOuter.setAttributeNS(null, 'y', String(centerY));
    this.eraserCursorOuter.setAttributeNS(
      null,
      'fill',
      this.eraserCursorIconColor
    );
    this.eraserCursorIcon.appendChild(this.eraserCursorOuter);

    this.eraserCursorInner1 = document.createElementNS(this.xmlns, 'rect');
    this.eraserCursorInner1.setAttributeNS(null, 'id', 'eraserCursorOuter');
    this.eraserCursorInner1.setAttributeNS(
      null,
      'width',
      String(this.eraserCursorIconWidth / 4)
    );
    this.eraserCursorInner1.setAttributeNS(
      null,
      'height',
      String(this.eraserCursorIconHeight / 2)
    );
    const eraserCursorInnerWidth = this.eraserCursorInner1.getAttribute(
      'width'
    );
    const eraserCursorInnerHeight = this.eraserCursorInner1.getAttribute(
      'height'
    );
    const eraserCursorInnerCenterX =
      this.getMousePoint().x - eraserCursorInnerWidth / 2;
    const eraserCursorInnerCenterY =
      this.getMousePoint().y - eraserCursorInnerHeight / 2;
    this.eraserCursorInner1.setAttributeNS(
      null,
      'x',
      String(eraserCursorInnerCenterX + 5)
    );
    this.eraserCursorInner1.setAttributeNS(
      null,
      'y',
      String(eraserCursorInnerCenterY)
    );
    this.eraserCursorInner1.setAttributeNS(null, 'fill', '#666666');
    this.eraserCursorIcon.appendChild(this.eraserCursorInner1);

    this.eraserCursorInner2 = document.createElementNS(this.xmlns, 'rect');
    this.eraserCursorInner2.setAttributeNS(null, 'id', 'eraserCursorOuter');
    this.eraserCursorInner2.setAttributeNS(
      null,
      'width',
      String(this.eraserCursorIconWidth / 4)
    );
    this.eraserCursorInner2.setAttributeNS(
      null,
      'height',
      String(this.eraserCursorIconHeight / 2)
    );
    this.eraserCursorInner2.setAttributeNS(
      null,
      'x',
      String(eraserCursorInnerCenterX - 5)
    );
    this.eraserCursorInner2.setAttributeNS(
      null,
      'y',
      String(eraserCursorInnerCenterY)
    );
    this.eraserCursorInner2.setAttributeNS(null, 'fill', '#666666');
    this.eraserCursorIcon.appendChild(this.eraserCursorInner2);
    this.panGroup.append(this.eraserCursorIcon);
  }
  // CONTINUE DRAWING - MOUSE MOVE ----------------------------------------------

  continueDrawing(e: any) {
    if (this.isMouseDown) {
      this.wbEvent = e;
      //console.log('this.selectedTool-->>', this.selectedTool);
      switch (this.selectedTool) {
        case ToolType.Erase:
          if (this.EraseByElementSelection) {
            this.continueEraseBySelection();
          } else {
            this.continueEraseByPath();
          }
          break;
        case ToolType.Rectangle:
          this.continueRectDrawing();
          break;
        case ToolType.Circle:
          this.continueCircleDrawing();
          break;
        case ToolType.Line:
          this.continueLineDrawing();
          break;
        case ToolType.Pan:
          this.continuePanning();
          break;
        case ToolType.Laser:
          this.continueLaser();
          break;
        default:
          if (this.isMultiTouch()) {
            this.continueMultiTouchPathDrawing(e);
          } else {
            this.continueMousePathDrawing();
          }
      }
    }
  }

  continueMultiTouchPathDrawing(e) {
    this.wbEvent = e;
    const touches = this.wbEvent.changedTouches;
    for (let i = 0; i < touches.length; i++) {
      const touch = touches[i],
        currentTouchIndex = this.getCurrentTouchIndex(touch.identifier);
      if (currentTouchIndex >= 0) {
        const x = this.getMultiTouchPoint(touch).x;
        const y = this.getMultiTouchPoint(touch).y;
        const currentTouch = this.currentTouches[currentTouchIndex];
        currentTouch.strPath +=
          ' L' + currentTouch.pageX + ' ' + currentTouch.pageY;
        currentTouch.element.setAttribute('d', currentTouch.strPath);
        currentTouch.pageX = x;
        currentTouch.pageY = y;
        this.currentTouches.splice(currentTouchIndex, 1, currentTouch);
      }
    }
  }

  continueLaser() {
    this.drawingMode = false;

    if (this.laserMode) {
      this.strPath +=
        this.getMousePoint().x + ' ' + this.getMousePoint().y + ',';
      const newStr = this.strPath.substring(0, this.strPath.length - 1);
      this.laserCursorPath.setAttribute('points', newStr);
    }

    /*
    const total = 100;
    const leader = this.getMousePoint();
    for (let i = 0; i < total; i++) {
      this.createLaserLine(leader, i);
    }*/
  }

  // --- motion trail
  createLaserLine(leader, i) {
    const total = 100;
    const ease = 0.75;
    this.panGroup = document.getElementById('panGroup');
    this.myline = document.createElementNS(this.xmlns, 'line');
    this.panGroup.appendChild(this.myline);

    const pos = this.myline._gsTransform;
    //this.tweenLite.set(this)
    //this.tweenLite.set(this.myline, { x: -15, y: -15, alpha: (total - i) / total });
    TweenLite.set(this.myline, { x: -15, y: -15, alpha: (total - i) / total });
    //console.log('myline--', this.myline);
    //console.log('pos--', pos);
    TweenMax.to(this.myline, 1000, {
      x: '+=1',
      y: '+=1',
      repeat: -1,

      modifiers: {
        x() {
          //console.log("pos--",pos)
          //console.log("leader--",leader)
          //const myx = pos.x + (leader.x - pos.x) * ease;
          //this. myline.setAttribute("x2", this.getMousePoint().x);
          //return myx;
        },
        y() {
          //const myy = pos.y + (leader.y - pos.y) * ease;
          //this.myline.setAttribute("y2", this.getMousePoint().y);
          //return myy;
        }
      }
    });

    return pos;
  }

  // ---end of motion trail

  continueMousePathDrawing() {
    if (!this.drawingMode) {
      return;
    }
    /*
    const currentMousePoint = this.getMousePoint();
    this.strPath += ' L' + currentMousePoint.x + ' ' + currentMousePoint.y;
    this.currentDrawing.setAttribute('type', 'path');
    this.currentDrawing.setAttribute('d', this.strPath);
    */
    //satyajit -- smooth path
    this.appendToBuffer(this.getMousePoint());
    this.updateSvgPath();
  }

  continueEraseBySelection() {
    this.drawingMode = false;
    if (this.erasingMode) {
      this.eraserCursorPath.setAttribute('x', this.getMousePoint().x);
      this.eraserCursorPath.setAttribute('y', this.getMousePoint().y);
      this.strPath +=
        ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
      this.eraserCursorPath.setAttribute('d', this.strPath);
      //const arr = [this.getMousePoint().x, this.getMousePoint().y];
      //this.erasePath.push(arr);
      const targetElements = this.getIntersectedElement();
      // if (targetElements !== undefined) {
      //   if (this.elementsToBeErased.indexOf(targetElements.node) === -1) {
      //     this.elementsToBeErased.push(targetElements.node);
      //   }
      // }
    }
  }

  continueEraseByPath() {
    this.drawingMode = false;
    if (this.erasingMode) {
      this.eraserCursorPath.setAttribute('x', this.getMousePoint().x);
      this.eraserCursorPath.setAttribute('y', this.getMousePoint().y);
      this.strPath +=
        ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
      this.eraserCursorPath.setAttribute('d', this.strPath);
      const arr = [this.getMousePoint().x, this.getMousePoint().y];
      this.erasePath.push(arr);

      for (let i = 0; i < this.whiteBoardData.length; i++) {
        if (this.whiteBoardData[i].element !== null) {
          const targetElements = this.whiteBoardData[i].element;
          if (targetElements.id !== 'drawingBoard') {
            if (this.elementsToBeErased.indexOf(targetElements) === -1) {
              this.elementsToBeErased.push(targetElements);
            }
          }
        }
      }

      this.continueEraserIcon();
    }
  }

  continueEraserIcon() {
    //var xPosition = e.clientX - parentPosition.x - (theThing.clientWidth / 2);
    //var yPosition = e.clientY - parentPosition.y - (theThing.clientHeight / 2);
    const centerX = this.getMousePoint().x - this.eraserCursorIconWidth / 2;
    const centerY = this.getMousePoint().y - this.eraserCursorIconHeight / 2;
    const eraserCursorInnerWidth = this.eraserCursorInner1.getAttribute(
      'width'
    );
    const eraserCursorInnerHeight = this.eraserCursorInner1.getAttribute(
      'height'
    );
    const eraserCursorInnerCenterX =
      this.getMousePoint().x - eraserCursorInnerWidth / 2;
    const eraserCursorInnerCenterY =
      this.getMousePoint().y - eraserCursorInnerHeight / 2;
    this.eraserCursorOuter.setAttributeNS(null, 'x', centerX);
    this.eraserCursorOuter.setAttributeNS(null, 'y', centerY);
    this.eraserCursorInner1.setAttributeNS(
      null,
      'x',
      eraserCursorInnerCenterX - 10
    );
    this.eraserCursorInner1.setAttributeNS(null, 'y', eraserCursorInnerCenterY);
    this.eraserCursorInner2.setAttributeNS(
      null,
      'x',
      eraserCursorInnerCenterX + 10
    );
    this.eraserCursorInner2.setAttributeNS(null, 'y', eraserCursorInnerCenterY);
  }

  continueRectDrawing() {
    if (!this.drawingMode) {
      return;
    }
    const currentMousePoint = this.getMousePoint();
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.strPath += ' L' + currentMousePoint.x + ' ' + this.point.y;
    this.strPath += ' L' + currentMousePoint.x + ' ' + currentMousePoint.y;
    this.strPath += ' L' + this.point.x + ' ' + currentMousePoint.y;
    this.strPath += ' L' + this.point.x + ' ' + this.point.y;
    this.currentDrawing.setAttribute('d', this.strPath);
  }

  continueCircleDrawing() {
    if (!this.drawingMode) {
      return;
    }
    this.strPath = '';
    const radius = Math.abs(
      this.getPageCordinates().pgX - this.offsetX - this.startX
    );
    this.strPath += this.helperGetArc(
      this.point.x,
      this.point.y,
      radius,
      0,
      359
    );
    this.currentDrawing.setAttribute('d', this.strPath);
  }
  continueLineDrawing() {
    if (!this.drawingMode) {
      return;
    }
    const tempPt = this.getMousePoint();
    const startX = this.point.x,
      startY = this.point.y,
      mouseX = tempPt.x,
      mouseY = tempPt.y;
    this.strPath = 'M' + startX + ' ' + startY;
    this.strPath += ' L' + mouseX + ' ' + mouseY;
    const tmpPath = '';
    this.currentDrawing.setAttribute('d', this.strPath + tmpPath);
  }

  continuePanning() {
    this.panZoom.enablePan();
    this.panZoomService.setExplicitPan();
    this.bgPanZoom.pan(this.panZoom.getPan());

    const deltaX = this.panZoom.getPan().x - this.myLastMouseX;
    const deltaY = this.panZoom.getPan().y - this.myLastMouseY;
    this.myLastMouseX = this.panZoom.getPan().x;
    this.myLastMouseY = this.panZoom.getPan().y;

    if (this.isPanning) {
      $('.canvas-bg-container').css({
        'background-position-x': '+=' + deltaX,
        'background-position-y': '+=' + deltaY
      });
    }
  }

  // END DRAWING - MOUSE UP
  endDrawing(e: any, type: string) {
    //console.log('endDrawing -> type', type);
    // console.log("--MOUSE UP--")
    this.wbEvent = e;
    this.isMouseDown = false;
    switch (this.selectedTool) {
      case 'erase':
        this.erasingMode = false;
        this.strPath = '';
        if (this.EraseByElementSelection) {
          //if (this.elementsToBeErased.length > 0)
          {
            if (type != 'onMouseLeave') {
              this.doErasingByElement();
              this.elementsToBeErased = [];
            }
            //console.log('END DRAWING REMOVE SELECTED ELEMENTS');
          }
        } else {
          if (this.elementsToBeErased.length > 0) {
            this.erasePath = this.getUniqueItemsFromArray(this.erasePath);
            if (this.erasePath.length > 300) {
              this.erasePath.length = 300;
            }
            const tempArr = this.elementsToBeErased;
            this.elementsToBeErased = [];
            for (let i = 0; i < tempArr.length; i++) {
              this.elementsToBeErased.push(tempArr[i]);
              this.doErasing();
            }
          }
          this.removeEraser('enddrawing-erace');
        }

        break;
      case 'laser':
        this.laserMode = false;
        this.whiteboardContainer.style.zIndex = '100';
        //this.toolbarService.selectTool(ToolType.Pen);
        this.removeLaser();
        break;
      case 'pan':
        this.isPanning = true;
        //this.toolbarService.selectTool(ToolType.Select);
        this.toolbarService.onToolBarActionDone(true);
        break;
      default:
        if (this.drawingMode) {
          if (!this.isMultiTouch()) {
            this.saveData(this.currentDrawing);
          }
          this.drawingMode = false;
          this.selectedElement = this.currentDrawing;
        }
        if (type != 'onMouseLeave') {
          this.removeEraser('enddrawing-default');

          this.storeMultTouchData();
          //console.log("endDrawing")
          //this.saveDrawings(this.whiteBoardData,type);
        }
    }
  }

  removeLaser() {
    if (this.laserCursorPath !== null) {
      this.laserCursorPath.remove();
      //this.removeLaserPolyLine();
      /*
      clearInterval(this.laserTimer);
      this.laserTimer = null;
      this.laserLength = 0;
      this.laserTimer = setInterval(this.onPlayBackProgress.bind(this),   10 / this.laserDrawFPS   );
      */
    }
  }

  removeLaserPolyLine() {
    const str = this.laserCursorPath.getAttribute('points');
    const arr = str.split(',');
    let newArr = [];

    let newStr = '';
    for (let i = 0; i < arr.length; i++) {
      newArr = this.removeNum(arr[i], arr[i]);
      //console.log('final-->> ', newArr);
    }
    //console.log('newArr--length--', newArr.length);
    if (newArr.length > 0) {
      this.laserCursorPath.setAttribute('points', newArr + ',');
    }
  }

  removeNum(string, val) {
    const arr = string.split(',');
    for (let i = 0; i < arr.length; i++) {
      //console.log('arr[i] ', arr[i] + ' --- ' + 'val ', val);

      if (arr[i] === val) {
        //console.log('-ok-');
        arr.splice(i);
        i--;
      }
    }
    return arr.join(',');
  }

  onPlayBackProgress() {
    this.laserStrokeSize--;
    //console.log('this.laserStrokeSize--', this.laserStrokeSize);
    //this.laserCursorPath.setAttributeNS(null, 'stroke-width', this.laserStrokeSize);

    /*
    this.laserPathLength = this.laserCursorPath.getTotalLength();    
    this.laserLength = this.laserLength + this.laserDistancePerPoint;    
    this.laserCursorPath.style.strokeDasharray = [this.laserLength,this.laserPathLength].join(' ');
    if (this.laserLength >= this.laserPathLength) {
        this.laserStopDrawing();
    } 
    */

    this.laserPathLength = this.laserCursorPath.getTotalLength();
    this.laserLength = this.laserLength + this.laserDistancePerPoint;
    this.laserCursorPath.style.strokeDasharray = [
      this.laserLength,
      this.laserPathLength
    ].join(' ');
    if (this.laserLength >= this.laserPathLength) {
      this.laserStopDrawing();
    }
  }

  laserStopDrawing() {
    if (this.laserTimer != null) {
      clearInterval(this.laserTimer);
      //this.laserCursorPath.remove();
    }
  }

  removeEraser(type) {
    //console.log('removeEraser -> type', type);
    if (this.eraserCursorPath !== null) {
      this.eraserCursorPath.remove();
    }
    if (this.eraserCursorIcon !== null) {
      this.eraserCursorIcon.remove();
    }
    //console.log("remove erase")
    if (type != 'default') {
      this.saveDrawings(this.whiteBoardData, 'removeEraser');
    }
  }

  getIntersectedElement() {
    for (let i = 0; i < this.whiteBoardData.length; i++) {
      const targetPath = SVG.get(this.whiteBoardData[i].element.id);
      const myEraserPath = SVG.get(this.eraserCursorPath.id);
      const intersectionsPoint = myEraserPath.intersectsPath(targetPath);
      if (intersectionsPoint.length > 0) {
        return targetPath;
      }
    }
  }
  setPanZoom() {
    this.panZoom = svgPanZoom(document.getElementById('drawingBoard'), {
      zoomEnabled: true,
      dblClickZoomEnabled: false,
      controlIconsEnabled: false,
      fit: false,
      center: false,
      // These values should be added to types and used to calculate zoom related tools.
      minZoom: 0.4,
      maxZoom: 3
    });

    this.panZoom.disablePan();
    this.panZoom.setZoomScaleSensitivity(0.2);

    this.bgPanZoom = svgPanZoom(document.getElementById('canvasBgContainer'), {
      zoomEnabled: true,
      dblClickZoomEnabled: false,
      controlIconsEnabled: false,
      fit: false,
      center: false,
      // These values should be added to types and used to calculate zoom related tools.
      minZoom: 0.4,
      maxZoom: 3
    });

    this.bgPanZoom.setZoomScaleSensitivity(0.2);
  }

  createShapeSelection() {
    this.removeShapeSelection();
    const element = this.wbEvent.target;
    if (element !== null && element.id !== 'drawingBoard') {
      const ele = document.getElementById(element.id);
      if (ele !== null) {
        const selectElement = SVG.get(ele.id)
          .selectize({ deepSelect: true })
          .resize()
          .draggable();
      }
    }
  }

  removeShapeSelection() {
    for (let i = 0; i < this.whiteBoardData.length; i++) {
      const element = this.whiteBoardData[i].element;
      if (element !== null) {
        const ele = document.getElementById(element.id);
        if (ele !== null) {
          if (typeof SVG.get(ele.id) === 'function') {
            const selectElement = SVG.get(ele.id)
              .selectize(false)
              .resize(false)
              .draggable(false);
          }
        }
      }
    }
  }

  storeMultTouchData() {
    if (this.isMultiTouch()) {
      const touches = this.wbEvent.changedTouches;
      for (let i = 0; i < touches.length; i++) {
        const touch = touches[i];
        const currentTouchIndex = this.getCurrentTouchIndex(touch.identifier);
        if (currentTouchIndex >= 0) {
          const currentTouch = this.currentTouches[currentTouchIndex];
          this.currentTouches.splice(currentTouchIndex, 1);
        } else {
          console.log('Touch was not found!');
        }
      }
    }
  }

  helperPolarToCartesian(
    centerX: any,
    centerY: any,
    radius: any,
    angleInDegrees: any
  ) {
    const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
    return {
      x: centerX + radius * Math.cos(angleInRadians),
      y: centerY + radius * Math.sin(angleInRadians)
    };
  }

  helperGetArc(x: any, y: any, radius: any, startAngle: any, endAngle: any) {
    const start = this.helperPolarToCartesian(x, y, radius, endAngle);
    const end = this.helperPolarToCartesian(x, y, radius, startAngle);
    const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';
    const d = [
      'M',
      start.x,
      start.y,
      'A',
      radius,
      radius,
      0,
      largeArcFlag,
      0,
      end.x,
      end.y
    ].join(' ');
    return d;
  }
  getMultiTouchPoint(touch) {
    // let event: any;
    const rect: ClientRect = this.wbEvent.target.getBoundingClientRect();
    let xx = touch.pageX - rect.left - this.panZoom.getPan().x;
    let yy = touch.pageY - rect.top - this.panZoom.getPan().y;
    xx = xx / this.panZoom.getSizes().realZoom;
    yy = yy / this.panZoom.getSizes().realZoom;
    return { x: xx, y: yy };
  }

  getMousePoint() {
    const pgX = this.getPageCordinates().pgX;
    const pgY = this.getPageCordinates().pgY;

    const myX = pgX - this.offsetX - this.panZoom.getPan().x;
    const myY = pgY - this.offsetY - this.panZoom.getPan().y;
    const newX = myX / this.panZoom.getSizes().realZoom;
    const newY = myY / this.panZoom.getSizes().realZoom;
    return { x: newX, y: newY };
  }

  getPageCordinates() {
    let pggX, pggY;
    //let event: any;
    //console.log("TouchEvent-->> ", TouchEvent);
    //console.log("2 -- event-->> ", event.changedTouches);

    if (this.wbEvent.touches) {
      const touches = this.wbEvent.changedTouches;
      for (let i = 0; i < touches.length; i++) {
        const touch = touches[i];
        pggX = touch.pageX;
        pggY = touch.pageY;
      }
      pggX = this.wbEvent.targetTouches[0].clientX;
      pggY = this.wbEvent.targetTouches[0].clientY;
    } else {
      pggX = this.wbEvent.pageX;
      pggY = this.wbEvent.pageY;
    }
    return { pgX: pggX, pgY: pggY };
  }

  getCurrentTouchIndex(id: any) {
    for (let i = 0; i < this.currentTouches.length; i++) {
      if (this.currentTouches[i].id === id) {
        return i;
      }
    }
    // Touch not found! Return -1.
    return -1;
  }

  isMultiTouch() {
    let flag;
    //let event: any;
    if (this.wbEvent) {
      if (this.wbEvent.touches) {
        if (this.multitouch) {
          flag = true;
        }
      }
    }

    return flag;
  }

  getRandomID() {
    return (
      Math.random()
        .toString(36)
        .substring(2, 15) +
      Math.random()
        .toString(36)
        .substring(2, 15)
    );
  }

  getUniqueItemsFromArray(arr: any) {
    const uniques = [];
    const itemsFound = {};
    //console.log('get unique input arr -->>', arr);
    for (let i = 0, l = arr.length; i < l; i++) {
      let uniquenessBasis = '';
      if (arr[i].id) {
        uniquenessBasis = arr[i].id;
      } else if (arr[i].element && arr[i].element.id) {
        uniquenessBasis = arr[i].element.id;
      } else {
        uniquenessBasis = JSON.stringify(arr[i]);
      }

      if (itemsFound[uniquenessBasis]) {
        continue;
      }
      uniques.push(arr[i]);
      itemsFound[uniquenessBasis] = true;
    }
    //console.log('uniques ', uniques);
    return uniques;
  }

  // ALL ERASER FUNCTIONS ---------------------------------------------
  doErasingByElement() {
    let elementsToBeErased = [];
    let that = this;
    for (let i = 0; i < this.whiteBoardData.length; i++) {
      const ele = this.whiteBoardData[i].element;
      const flag = this.intersectRect(this.eraserCursorPath, ele);
      //console.log("flag-->> ",flag);
      if (flag) {
        //this.removeData(ele);
        //ele.remove();
        ele.setAttributeNS(null, 'stroke', 'yellow');
        ele.setAttributeNS(null, 'stroke-width', this.strokeWidth * 3);
        ele.setAttributeNS(null, 'opacity', '0.8');
        elementsToBeErased.push(ele);
      }
    }

    if (elementsToBeErased.length > 0) {
      for (let i = 0; i < elementsToBeErased.length; i++) {
        const element = elementsToBeErased[i];
        setTimeout(function() {
          //console.log('Test');
          that.removeData(element);
          element.remove();
        }, 500);
      }
    }

    //console.log('whiteBoardData-->> ', this.whiteBoardData);
    this.removeEraser('doErasingByElement');
  }

  intersectRect(rect1, rect2) {
    if (rect1 !== null && rect2 !== null) {
      const r1 = rect1.getBoundingClientRect(); //BOUNDING BOX OF THE FIRST OBJECT
      const r2 = rect2.getBoundingClientRect(); //BOUNDING BOX OF THE SECOND OBJECT

      //CHECK IF THE TWO BOUNDING BOXES OVERLAP
      return !(
        r2.left > r1.right ||
        r2.right < r1.left ||
        r2.top > r1.bottom ||
        r2.bottom < r1.top
      );
    }
  }

  doErasing() {
    for (let i = 0; i < this.elementsToBeErased.length; i++) {
      this.removeData(this.elementsToBeErased[i]);
      const element = this.elementsToBeErased[i];
      if (element.nodeName === 'path' || element.nodeName === 'eraser') {
        const polyLinesData = this.convertToPolylines(element);
        const sourcePaths = [];
        sourcePaths.push(polyLinesData);

        if (sourcePaths.length > 0) {
          const lowPolyErasePath = simplify(this.erasePath, 1, true);
          const computedErasingPath = erase(
            sourcePaths,
            lowPolyErasePath,
            this.eraserSize / 2
          );
          this.redrawAfterErasing(computedErasingPath);
        } else {
          console.log('do nothing!');
        }
      }
    }
  }

  convertToPolylines(shapeElement: any) {
    if (!shapeElement || shapeElement.nodeName !== 'path') return;
    const dataFromPath = shapeElement.getAttribute('d');
    let pathpointsArr: any;
    if (dataFromPath.includes(',')) {
      pathpointsArr = dataFromPath.split(',');
    } else {
      pathpointsArr = dataFromPath.split(' ');
    }
    const extractedPoints: any = [];
    //  console.log('START-->> POLY');
    for (let i = 0; i < shapeElement.getTotalLength(); i++) {
      const pointsFromElement = shapeElement.getPointAtLength(
        (i * shapeElement.getTotalLength()) / shapeElement.getTotalLength()
      );
      const polycordinates = [];
      polycordinates.push(pointsFromElement.x);
      polycordinates.push(pointsFromElement.y);
      extractedPoints.push(polycordinates);
    }
    //  console.log('END-->> POLY');

    /*  simplify(points, tolerance, highestQuality) */
    const lowPolyPoints = simplify(extractedPoints, 1, true);
    return lowPolyPoints;
  }

  // REDRAW AFTER ERASING
  redrawAfterErasing(paths: any) {
    this.pathColorsArr = [];
    this.pathStrokeWidthArr = [];
    this.pathOpacityArr = [];
    for (let i = 0; i < this.elementsToBeErased.length; i++) {
      const element = this.elementsToBeErased[i];
    }
    const iteration = paths.length / this.elementsToBeErased.length;
    let count = 0;
    for (let i = 1; i <= paths.length; i++) {
      const element = this.elementsToBeErased[count];
      this.pathColorsArr.push(element.getAttribute('myStrokeColor'));
      this.pathStrokeWidthArr.push(element.getAttribute('myStrokeWidth'));
      this.pathOpacityArr.push(element.getAttribute('myStrokeOpacity'));
      if (i % iteration === 0) {
        count++;
      }
    }

    let newDrawing = null;
    for (let i = 0; i < paths.length; i++) {
      let data = 'M' + paths[i][0] + ' ' + paths[i][1];
      for (let j = 0; j < paths[i].length; j++) {
        data += ' L' + paths[i][j][0] + ' ' + paths[i][j][1];
      }
      newDrawing = document.createElementNS(this.xmlns, 'path');
      newDrawing.setAttributeNS(
        null,
        'stroke-width',
        this.pathStrokeWidthArr[i]
      );
      newDrawing.setAttributeNS(
        null,
        'myStrokeWidth',
        this.pathStrokeWidthArr[i]
      );
      newDrawing.setAttributeNS(
        null,
        'id',
        'path_' + i + '_' + this.getRandomID()
      );
      newDrawing.setAttributeNS(null, 'class', 'draggable');
      newDrawing.setAttribute('stroke-linejoin', 'round');
      newDrawing.setAttribute('stroke-linecap', 'round');
      newDrawing.setAttributeNS(null, 'opacity', this.pathOpacityArr[i]);
      newDrawing.setAttributeNS(
        null,
        'myStrokeOpacity',
        this.pathOpacityArr[i]
      );
      newDrawing.setAttributeNS(null, 'fill', 'none');
      newDrawing.setAttributeNS(null, 'fill-opacity', '0');
      newDrawing.setAttributeNS(null, 'stroke', this.pathColorsArr[i]);
      newDrawing.setAttributeNS(null, 'myStrokeColor', this.pathColorsArr[i]);
      newDrawing.setAttribute('d', data);
      this.panGroup.append(newDrawing);
      this.selectedTool = 'path';
      this.saveData(newDrawing);
      this.selectedTool = 'erase';
    }

    for (let i = 0; i < this.elementsToBeErased.length; i++) {
      const elements = this.elementsToBeErased[i];
      this.removeData(elements);
      elements.remove();
    }
    this.elementsToBeErased = [];
  }
  // END OF ERASER FUNCTIONS -------------------------------------------

  // DATA CONTROLLER FUNCTIONS
  saveData(item: any) {
    if (item !== null) {
      const obj = {
        type: 'path',
        element: item
      };
      const foundItem = this.whiteBoardData.find(wbItem => {
        return obj.element.id === wbItem.element.id;
      });
      if (!foundItem) {
        this.whiteBoardData.push(obj);
        // console.log(
        //   'TCL: saveData -> this.whiteBoardData',
        //   this.whiteBoardData
        // );
      }
      //console.log('saveData whiteboard data', this.whiteBoardData);
      //this.whiteBoardData =  this.getUniqueItemsFromArray(this.whiteBoardData);
    }
  }

  removeData(item: any) {
    for (let i = 0; i < this.whiteBoardData.length; i++) {
      if (this.whiteBoardData[i].element !== null) {
        const searchID = this.whiteBoardData[i].element.id;
        if (item.id === searchID) {
          this.whiteBoardData.splice(i, 1);
          break;
        }
      }
    }
  }
  // End Of DATA CONTROLLER FUNCTIONS

  doLog(message: any) {
    const myElement = document.getElementById('debug_txt');
    //myElement.innerHTML += " : "+message + '<br>';
    //myElement.scroll(0, 10000000)
  }

  //CHANGE STATE
  changeState(btnState: any) {
    this.panZoom.disablePan();
    this.selectedTool = btnState;
    this.removeShapeSelection();
  }

  setDefaultStrokeColors(newPenColors) {
    let newDefaultStrokeColor: any;
    newPenColors.forEach(element => {
      if (element.value === '#FFFFFF') {
        newDefaultStrokeColor = element.value;
      }
      if (element.value === '#000000') {
        newDefaultStrokeColor = element.value;
      }
    });
    for (let i = 0; i < this.whiteBoardData.length; i++) {
      const element = this.whiteBoardData[i].element;
      const currentStrokeColor = element.getAttribute('myStrokeColor');

      // Only change white or black stroke colors on theme change,
      // colors should remain the same
      if (currentStrokeColor === this._defaultStrokeColor) {
        element.setAttributeNS(null, 'stroke', newDefaultStrokeColor);
        element.setAttributeNS(null, 'myStrokeColor', newDefaultStrokeColor);
      }
    }
    this._defaultStrokeColor = newDefaultStrokeColor;
  }
  //new code
  checkSavedWhiteboards() {
    this.clearAnnotations();
    let whiteboardData: any;
    //const whiteboardData:any = this.appStateStorageService.getAppStorageByCurrentStorageKey();
    this.appStateStorageService
      .getAppStorageByCurrentStorageKey()
      .subscribe(data => {
        if (data && data['wbContent']) {
          // whiteboardData = []
          whiteboardData = JSON.parse(data['wbContent']);
          if (!whiteboardData || !whiteboardData.hasOwnProperty('drawings')) {
            return;
          }
          this.panGroup = document.getElementById('panGroup');
          if (whiteboardData.drawings.length > 0) {
            whiteboardData.drawings.forEach(drawing => {
              const drawingElem = document.createElementNS(this.xmlns, 'path');
              if (drawing !== null) {
                Object.keys(drawing).forEach((key, index) => {
                  drawingElem.setAttributeNS(null, key, drawing[key]);
                });

                this.panGroup.appendChild(drawingElem);
                const obj = {
                  type: 'path',
                  element: drawingElem
                };
                this.whiteBoardData.push(obj);
              }
            });
            //this.whiteBoardData =  this.getUniqueItemsFromArray(this.whiteBoardData);
          }
        }
      });
    //  console.log('whiteboardData.drawings -->> ', whiteboardData.drawings);
  }

  //old code
  //  checkSavedWhiteboards() {
  //   this.clearAnnotations();
  //   const whiteboardData:any = this.appStateStorageService.getAppStorageByCurrentStorageKey();
  //   console.log("TCL: checkSavedWhiteboards -> whiteboardData", whiteboardData)
  //   if (!whiteboardData || !whiteboardData.hasOwnProperty('drawings')) {
  //     return;
  //   }
  //   this.panGroup = document.getElementById('panGroup');
  //   //  console.log('whiteboardData.drawings -->> ', whiteboardData.drawings);

  //   if (whiteboardData.drawings.length > 0) {
  //     whiteboardData.drawings.forEach(drawing => {
  //       const drawingElem = document.createElementNS(this.xmlns, 'path');
  //       if (drawing !== null) {
  //         Object.keys(drawing).forEach((key, index) => {
  //           drawingElem.setAttributeNS(null, key, drawing[key]);
  //         });

  //         this.panGroup.appendChild(drawingElem);
  //         const obj = {
  //           type: 'path',
  //           element: drawingElem
  //         };
  //         this.whiteBoardData.push(obj);
  //       }
  //     });
  //     //this.whiteBoardData =  this.getUniqueItemsFromArray(this.whiteBoardData);
  //   }
  // }
  get toolType() {
    return ToolType;
  }

  drawPathFromWidget(d: any) {
    //const lowPolyErasePath = simplify(d, 1, true);
    this.panGroup = document.getElementById('panGroup');
    this.currentDrawing = document.createElementNS(this.xmlns, 'path');
    this.currentDrawing.setAttributeNS(null, 'stroke-width', this.strokeWidth);
    this.currentDrawing.setAttributeNS(
      null,
      'id',
      'path_' + this.getRandomID()
    );
    this.currentDrawing.setAttributeNS(null, 'class', 'draggable');
    this.currentDrawing.setAttributeNS(null, 'opacity', this.opacity);
    this.currentDrawing.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.currentDrawing.setAttributeNS(null, 'stroke-linecap', 'round');
    this.currentDrawing.setAttributeNS(null, 'stroke', this.strokeColor);
    this.currentDrawing.setAttributeNS(null, 'fill', 'none');
    this.currentDrawing.setAttributeNS(null, 'fill-opacity', '0');
    this.currentDrawing.setAttributeNS(null, 'myStrokeWidth', this.strokeWidth);
    this.currentDrawing.setAttributeNS(null, 'myStrokeColor', this.strokeColor);
    this.currentDrawing.setAttributeNS(null, 'myStrokeOpacity', this.opacity);
    this.panGroup.appendChild(this.currentDrawing);
    // add a dot on mousedown

    this.currentDrawing.setAttribute('d', d);
    this.saveData(this.currentDrawing);
  }

  appendToBuffer (pt) {
        this.buffer.push(pt);
        while (this.buffer.length > this.bufferSize) {
            this.buffer.shift();
        }
  }

    // Calculate the average point, starting at offset in the buffer
  getAveragePoint (offset) {
      const len = this.buffer.length;
      if (len % 2 === 1 || len >= this.bufferSize) {
          let totalX = 0;
          let totalY = 0;
          let pt, i;
          let count = 0;
          for (i = offset; i < len; i++) {
              count++;
              pt = this.buffer[i];
              totalX += pt.x;
              totalY += pt.y;
          }
          return {
              x: totalX / count,
              y: totalY / count
          }
      }
      return null;
  }

  updateSvgPath () {
    let pt = this.getAveragePoint(0);

    if (pt) {
        // Get the smoothed part of the path that will not change
        this.strPath += " L" + pt.x + " " + pt.y;

        // Get the last part of the path (close to the current mouse position)
        // This part will change if the mouse moves again
        let tmpPath = "";
          for (let offset = 2; offset < this.buffer.length; offset += 2) {
              pt = this.getAveragePoint(offset);
              tmpPath += " L" + pt.x + " " + pt.y;
          }

          // Set the complete current path coordinates
          this.currentDrawing.setAttribute("d", this.strPath + tmpPath);
      }
  }
}
