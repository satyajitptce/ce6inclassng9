import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibConfigModule } from '@tce/lib-config';
import { WhiteboardComponent } from './components/whiteboard/whiteboard.component';
@NgModule({
  imports: [CommonModule, LibConfigModule.forChild(WhiteboardComponent)],
  declarations: [WhiteboardComponent],
  exports: [WhiteboardComponent]
})
export class WhiteboardModule {}
