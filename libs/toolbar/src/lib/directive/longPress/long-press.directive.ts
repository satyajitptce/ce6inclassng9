import {
  Directive,
  Output,
  EventEmitter,
  HostBinding,
  HostListener
} from '@angular/core';
import { AppConfigService } from '@tce/core';
@Directive({
  selector: '[tceLongPress]'
})
export class LongPressDirective {
  pressing: boolean;
  selectedtoolindex: boolean;
  timeout: any;
  interval: any;
  constructor(private appConfigService: AppConfigService) {}
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onLongPress = new EventEmitter();

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onLongPressing = new EventEmitter();

  @HostBinding('class.press')
  get press() {
    return this.pressing;
  }

  @HostBinding('class.longpress')
  get longPress() {
    return this.selectedtoolindex;
  }

  @HostListener('mousedown', ['$event'])
  @HostListener('touchstart', ['$event'])
  startInteraction(event) {
    var speed;
    if (event.touches) {
      speed = this.appConfigService.getConfig('global_setting')[
        'toolbarSwipeScrollSpeed'
      ];
    } else {
      speed = this.appConfigService.getConfig('global_setting')[
        'toolbarLongPressScrollSpeed'
      ];
    }
    this.pressing = true;
    this.selectedtoolindex = false;
    this.timeout = setTimeout(() => {
      this.selectedtoolindex = true;
      this.onLongPress.emit(event);
      this.interval = setInterval(() => {
        this.onLongPressing.emit(event);
      }, 150);
    }, speed);
  }

  @HostListener('touchend')
  @HostListener('mouseup')
  @HostListener('mouseleave')
  endPress() {
    clearTimeout(this.timeout);
    clearInterval(this.interval);
    this.selectedtoolindex = false;
    this.pressing = false;
  }
}
