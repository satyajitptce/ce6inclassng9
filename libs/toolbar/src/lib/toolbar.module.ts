import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { ToolsComponent } from './components/tools/tools.component';
import { LibConfigModule } from '@tce/lib-config';
import { LongPressDirective } from './directive/longPress/long-press.directive';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { CoreModule } from '@tce/core';
import { UserOptionsComponent } from './components/user-options/user-options.component';
import { SubToolsComponent } from './components/sub-tools/sub-tools.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    LibConfigModule.forChild(ToolbarComponent),
    AngularSvgIconModule
  ],
  declarations: [
    ToolbarComponent,
    ToolsComponent,
    LongPressDirective,
    UserOptionsComponent,
    SubToolsComponent
  ],
  exports: [
    ToolbarComponent,
    SubToolsComponent,
    ToolsComponent,
    UserOptionsComponent
  ],
  entryComponents: [SubToolsComponent]
})
export class ToolbarModule {}
