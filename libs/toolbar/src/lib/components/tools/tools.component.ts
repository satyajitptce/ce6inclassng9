import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener,
  OnDestroy,
  ChangeDetectorRef
} from '@angular/core';
import {
  trigger,
  style,
  animate,
  transition,
  group
} from '@angular/animations';
import { ToolbarService, ToolType } from '@tce/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { PlayerDrawerService } from '@app-teacher/services';
export const righttoolbar = {
  rotate0: 'rotate(0deg)',
  rotate270: 'rotate(0deg)',
  mf: '24px'
};
export const lefttoolbar = {
  rotate0: 'rotate(90deg)',
  rotate270: 'rotate(270deg)',
  mf: '48px'
};
@Component({
  selector: 'tce-tools',
  templateUrl: './tools.component.html',
  styleUrls: ['./tools.component.scss'],
  animations: [
    trigger('fadeInOut', [
      transition('void => fly', [
        group([
          style({
            transform: 'translateY(-2px)',
            opacity: '0',
            background: 'red'
          }),
          animate('1s ease-out', style('*'))
        ])
      ])
    ])
  ]
})
export class ToolsComponent implements OnInit, OnDestroy {
  _toolsData: any;
  _multiColumnLayout: any;
  _appendSubjectClass: any = false;
  _previousTool: any;
  _defaultTool: any;
  toolsSubscription: Subscription = new Subscription();
  @Input() currentTool;
  @Input('toolsData')
  set toolsData(toolsData) {
    this._toolsData = toolsData;
    //console.log('toolsData', toolsData);
    //this.cdf.detectChanges();
  }
  get toolsData() {
    return this._toolsData;
  }
  @Input('multiColumnLayout')
  set multiColumnLayout(value) {
    this._multiColumnLayout = value;
    //this.cdf.detectChanges();
  }
  get multiColumnLayout() {
    return this._multiColumnLayout;
  }
  @Input() toolbarSideState;
  @Input('defaultTool')
  set defaultTool(val) {
    //console.log("ToolsComponent -> setdefaultTool -> val", val)
    if (val) {
      this._defaultTool = val;
    } else {
      this._defaultTool = this.currentTool;
    }
    //this.cdf.detectChanges()
  }
  get defaultTool() {
    return this._defaultTool;
  }
  @Input('previousTool')
  set previousTool(val) {
     //console.log("ToolsComponent -> setpreviousTool -> val", val)
    this._previousTool = val;
    //this.cdf.detectChanges();
  }
  get previousTool() {
    return this._previousTool;
  }
  @Input() focusToolState;
  @Input() getSelectedTool;
  @Input('appendSubjectClass')
  set appendSubjectClass(val) {
    //console.log("ToolsComponent -> setappendSubjectClass -> val", val)
    this._appendSubjectClass = val;
    this.cdf.detectChanges();
  }
  get appendSubjectClass() {
    return this._appendSubjectClass;
  }
  @Output() menuselected = new EventEmitter();
  tracker: any;
  clickToScrollPosition: any = 40;
  constructor(
    private toolbarService: ToolbarService,
    private sanitizer: DomSanitizer,
    private playerDrawerService: PlayerDrawerService,
    private cdf: ChangeDetectorRef
  ) {}
  @HostListener('click', ['$event'])
  handleClickInside(event: Event) {
    event.stopPropagation();
  }
  ngOnInit(): void {
    this.tracker = document.getElementById('myTool');
    this.toolsSubscription.add(
      this.toolbarService.toolbarScroll$.subscribe(value => {
        this.clickToScrollPosition += 40;
        this.tracker.scrollTop = this.clickToScrollPosition;
      })
    );
    if (!this._multiColumnLayout) {
      this.toolsSubscription.add(
        this.playerDrawerService.drawerOpenStatus$.subscribe(newOpenStatus => {
          if (newOpenStatus) {
            for (let index = 0; index < this._toolsData.length; index++) {
              if (
                (this.toolsData[index].event ||
                  this.toolsData[index].subtilte) &&
                (this.toolsData[index].event ||
                  this.toolsData[index].subtilte) == this.getSelectedTool
              ) {
                this.tracker.scrollTop = 100;
              }
            }
            // this.cdf.detectChanges();
          }
        })
      );
    }
  }
  transform(html) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  onScroll($event) {
    let obj = {
      state: '',
      clickedstatus: false,
      submenustatus: false,
      bodyclick: true
    };
    this.menuselected.emit(obj);
    this.toolbarService.toolbarUserFlag.next(false);
    this.tracker = $event.target;

    const scrollTop = this.tracker.scrollTop;
    const scrollHeight = this.tracker.scrollHeight;
    const clientHeight = this.tracker.clientHeight;
    let limit = scrollHeight - clientHeight;
    this.clickToScrollPosition = scrollTop;

    //this.tracker[0].style['scroll-behavior'] = 'auto';
    this.tracker.style['scrollBehavior'] = 'smooth';
    if (scrollTop === limit) {
      this.tracker.style['scrollBehavior'] = 'auto';
      // console.log('end reached');
      //this.clickToScrollPosition = 40;
      this.tracker.scrollTop = 80;
      //limit = limit * 2
    }
    this.cdf.detectChanges();
  }

  onselectState(
    state: any,
    clickedstatus: boolean,
    submenustatus: boolean,
    bodyclick: boolean,
    $event
  ) {
    let submenuPosition: any;

    // position submenu based on current tool selection
    const targetElement = $event.target;
    const targetElementTop = targetElement.getClientRects()[0].top;
    const toolBarRef: any = document.getElementsByClassName('mytoolbar');
    const getToolbarRef = targetElement.getClientRects()[0];
    const toolBarTop = toolBarRef[0].getClientRects()[0].top;
    const final = targetElementTop - toolBarTop - 10;
    //console.log('this.toolbarSideState-->> ', this.toolbarSideState);
    if (this.toolbarSideState) {
      submenuPosition = getToolbarRef.left + getToolbarRef.width + 10;
    } else {
      submenuPosition =
        window.innerWidth - getToolbarRef.right + getToolbarRef.width + 10;
    }
    let newPositionArray = {
      top: final,
      position: submenuPosition,
      userTop: ''
    };
    this.subMenuPosition(newPositionArray);
    // end of position submenu based on current tool selection

    this.toolbarService.toolbarUserFlag.next(false);
    const obj = {
      state: state,
      clickedstatus: clickedstatus,
      submenustatus: submenustatus,
      bodyclick: bodyclick
    };

    this.menuselected.emit(obj);
  }

  subMenuPosition(dd) {
    this.toolbarService.toolbarSubmenuTop.next(dd);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.toolsSubscription.unsubscribe();
  }
}
