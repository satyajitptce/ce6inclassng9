import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import {
  ToolbarService,
  AppConfigService,
  PanZoomService,
  ThemeService,
  GlobalConfigState,
  AuthenticationService,
  AuthStateEnum,
  ToolType,
  CommonService,
  UserProfileService
} from '@tce/core';
import { Subscription } from 'rxjs';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
@Component({
  selector: 'tce-sub-tools',
  templateUrl: './sub-tools.component.html',
  styleUrls: ['./sub-tools.component.scss']
})
export class SubToolsComponent implements OnInit, OnDestroy {
  subToolSubscription: Subscription = new Subscription();
  @Input() showProfileDropdown;
  @Input() isLoggedIn;
  @Input() userData;
  @Input() toolbarSideState;
  @Input() toolsData;
  @Input() selectedTool;
  @Input() toolState;
  @Input() currentSubTool;
  @Input() eraseByElementSelection;
  @Input() themesOption;
  @Output() subMenuSelected = new EventEmitter();
  @Output() OnChangeColorState = new EventEmitter();
  @Output() OnChangePenState = new EventEmitter();
  zoomScale = 100;
  constructor(
    private toolbarService: ToolbarService,
    public panZoomService: PanZoomService,
    private sanitizer: DomSanitizer,
    private commonService:CommonService
  ) {}

  ngOnInit(): void {
    this.commonService.classroomModeState$.subscribe(flag => {
      if(!flag){
        this.showProfileDropdown = false
      }
    })

  }
  onsubSelectedStatus(tool, state) {
    // console.log("tool",tool,"state",state)
    const currentToolState = {
      tool: tool,
      state: state
    };
    this.subMenuSelected.emit(currentToolState);
    if(tool === 'gtEraseWhiteboard'){
      this.commonService.setLessonPlayerMode(false);
    }
  }
  toggleshare($event) {
    this.eraseByElementSelection = !this.eraseByElementSelection;
    this.toolbarService.settingClearWhiteboard(this.eraseByElementSelection);
  }
  zoomIn() {
    this.zoomScale += 0.25;
    this.zoomToCenter();
  }

  zoomEnd() {
    this.toolbarService.selectTool(ToolType.Pen);
  }
  zoomOut() {
    this.zoomScale -= 0.25;
    this.zoomToCenter();
  }

  handleZoomSlide(newZoom: string) {
    this.zoomScale = +newZoom;
    this.zoomToCenter();
  }

  setZoomScale(newScale) {
    this.zoomScale = newScale;
  }

  zoomToCenter() {
    this.panZoomService.setZoomToPointAndScale(
      { x: window.innerWidth / 2, y: window.innerHeight / 2 },
      this.zoomScale
    );
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subToolSubscription.unsubscribe();
  }

  onChangeColor(color, state) {
    const currentColorState = {
      color: color,
      state: state
    };
    this.OnChangeColorState.emit(currentColorState);
  }

  onChangePen(size, state) {
    const currentPenState = {
      size: size,
      state: state
    };
    this.OnChangePenState.emit(currentPenState);
  }

  transform(html: any) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
}
