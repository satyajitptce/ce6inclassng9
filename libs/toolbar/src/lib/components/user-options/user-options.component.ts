import { Component, OnInit, Input, NgModule } from '@angular/core';
import {
  KeyboardService,
  AuthenticationService,
  ThemeService,
  ToolbarService,
  CommonService,
  AppConfigService,
  UserProfileService
} from '@tce/core';

import {
  PlayerContainerService,
  PlayerDrawerService
} from '@app-teacher/services';
import { PlanningModeService } from '../../../../../planning-mode/src/lib/services/planning-mode.service';
import { Subscription } from 'rxjs';
import { Route, Router } from '@angular/router';
import { ApplicationMode } from 'libs/core/src/lib/enums/app-mode.enum';
@Component({
  selector: 'tce-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.scss']
})
export class UserOptionsComponent implements OnInit {
  _isLoggedIn = false;
  currentUserState: any;
  userOptionSubscription: Subscription = new Subscription();

  @Input() type;
  @Input() userData;
  @Input() userProfileImg;
  @Input('isLoggedIn')
  set isLoggedIn(value: boolean) {
    //console.log('loggin vlue', value);
    this._isLoggedIn = value;

    const documentElement = document.documentElement;
    if (value) {
      this.userProfileImg = this.userProfileService.userProfileImg;
      documentElement.style.setProperty(`--${'userMenuTop'}`, `${'-280px'}`);
    } else {
      documentElement.style.setProperty(`--${'userMenuTop'}`, `${'-100px'}`);
    }
  }
  get isLoggedIn() {
    return this._isLoggedIn;
  }
  showKeyboard = true;
  darkTheme = true;

  constructor(
    private keyboardService: KeyboardService,
    private authService: AuthenticationService,
    private themeService: ThemeService,
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService,
    private commonService: CommonService,
    private appConfigService: AppConfigService,
    private planningModeService: PlanningModeService,
    private userProfileService: UserProfileService,
    private router: Router
  ) {}

  ngOnInit() {
    
    console.log("this.userData-->> ", this.userData)
    this.keyboardService.keyboardUserState$.subscribe(showKeyboard => {
      this.showKeyboard = showKeyboard;
    });
    let getCurrentUserState = this.appConfigService.getConfig('global_setting')[
      'classRoomMode'
    ];
    // if (getCurrentUserState) {
    //   this.currentUserState = 'teaching';
    // } else {
    //   this.currentUserState = 'planning';
    // }
    this.currentUserState = this.type;
    
    this.themeService.currentThemeNameSubject$.subscribe(themeName => {
      this.darkTheme = themeName === 'dark';
    });

    this.commonService.classroomModeState$.subscribe(state => {
      //console.log('user-options-classroom mode', state);
      if (state) {
        this.currentUserState = ApplicationMode.TEACHING;
      } else {
        this.currentUserState = ApplicationMode.PLANNING;
      }
    });
  }

  toggleKeyboard(e: Event) {
    this.keyboardService.toggleUserState();
  }

  logOut(event, callee: string) {
    this.toolbarService.toolbarUserFlag.next(false);
    this.playerDrawerService.setDrawerOpenStatus(false);
    this.toolbarService.setSelectedSubjectId('');
    this.authService.logoutUser(false, 'user logOut');
  }

  toggleTheme(e: Event) {
    if (this.themeService.currentThemeName === 'light') {
      this.themeService.setTheme('dark');
    } else {
      this.themeService.setTheme('light');
    }
  }

  profileDetail(type: string) {
    const userStae = {
      userView: 'userProfile',
      currentState: 'account'
    };
    if (this.type === 'teaching') {
      this.toolbarService.toolbarUserFlag.next(false);
      this.commonService.setUserProfileState(userStae);
    }
    if (this.type === 'planning') {
      this.planningModeService.setUserProfileState(userStae);
    }
  }

  setUserMode(state: string) {
    this.currentUserState = state;
    //console.log('setUserMode', this.currentUserState);
    if (state === ApplicationMode.TEACHING) {
      this.commonService.applicationMode = state;
      this.router.navigateByUrl('/canvas');
      this.commonService.setClassroomModeState(true);
      this.keyboardService.teachModeKeyBoardstate();
    } else {
      this.commonService.setClassroomModeState(false);
      this.keyboardService.planModeKeyBoardState();

    }
  }

  openAdmin() {
    this.commonService.setUserAdmin('admin');
  }
}
