import {
  Input,
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  HostListener,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnDestroy
} from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import {
  ToolbarService,
  AppConfigService,
  PanZoomService,
  ThemeService,
  GlobalConfigState,
  AuthenticationService,
  AuthStateEnum,
  ToolType,
  CommonService,
  UserProfileService,
  KeyboardService
} from '@tce/core';
import { PlayerDrawerService } from '@app-teacher/services';
import { Subscription } from 'rxjs';
@Component({
  selector: 'tce-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarComponent implements OnInit, OnDestroy {
  @Input() userData;

  toolbarSubscription: Subscription = new Subscription();
  gtAddtoolbar: any = {};
  selectedTool: string = '';
  gtBackgroundSubMenuTool: string = '';
  currentToolState: boolean = false;
  toolState: boolean = false;
  defaultTool: string = '';
  currentSubTool: string = '';
  previousTool: string = '';
  focusToolState: boolean = false;
  getSelectedTool: string = '';
  selectedlasttool: string = '';
  toolbarSideState: any = false;
  showProfileDropdown: any = false;
  userProfileImg: string = '';
  currentBackground: any;
  selectedTheme: any;
  eraseByElementSelection: any;
  defaultColor: any;
  SelectedPenSize: any;
  toolsData: any = [];
  minimumScrollNumber: number;
  multiColumnLayout: boolean;
  isLongPressed: boolean;
  toolbarListData: any;
  toolsData_contextual: any;
  theme: any;
  subjecticon: any;
  whiteboardp: any;
  authmode: any;
  modetype: any;
  selectedColor: any;
  isLoggedIn: any;
  themesOption: any;
  itemToDisplay: any;
  toolAvailableHeightReduction: any = 0;
  selectedtoolindex: any;
  selectedsubject: any;
  appendSubjectClass: any;
  currentTool: any;
  constructor(
    private sanitizer: DomSanitizer,
    private toolbarService: ToolbarService,
    private eRef: ElementRef,
    private appConfigService: AppConfigService,
    public panZoomService: PanZoomService,
    private themeService: ThemeService,
    private cdr: ChangeDetectorRef,
    private authService: AuthenticationService,
    private playerDrawerService: PlayerDrawerService,
    private commonService: CommonService,
    private userProfileService: UserProfileService,
    private keyboardService: KeyboardService
  ) {}
  @HostListener('window:resize', ['$event'])
  onResize(event: Event) {
    this.setToolBarHeight();
  }
  setToolBarHeight() {
    const toolbarHeight = window.innerHeight;
    const iconHeight = 24;
    const iconPadding = 8;
    this.itemToDisplay =
      (toolbarHeight - this.toolAvailableHeightReduction * 1.3) /
        (iconHeight + iconPadding) -
      this.toolsData.length;
    this.itemToDisplay = Math.round(this.itemToDisplay);
    if (this.itemToDisplay > this.toolsData.length) {
      this.itemToDisplay = this.toolsData.length;
    }
    if (this.itemToDisplay <= 0) {
      this.itemToDisplay = 1;
    }
    let customHeight: any;
    if (this.multiColumnLayout) {
      customHeight = 'auto';
    } else {
      customHeight = this.itemToDisplay * 40 + 'px';
    }
    this.selectedtoolindex = this.itemToDisplay;
    const documentElement = document.documentElement;
    documentElement.style.setProperty(`--${'toolHeight'}`, `${customHeight}`);
    //this.cdr.detectChanges()
  }

  @HostListener('document:touchstart', ['$event'])
  @HostListener('document:mousedown', ['$event'])
  @HostListener('document:click', ['$event'])
  clickout(event) {
    // console.log("event--toolbar",event.target.id)
    if (this.eRef.nativeElement.contains(event.target)) {
      
      //console.log("event--toolbar")

      //this.submenuIndex = null;
      //this.selectstate = true;
    } else {
      this.toolState = false;
      if (this.currentToolState) {
        this.defaultTool = this.selectedlasttool;
        this.onSelectTool(this.selectedlasttool, true, true, true);
        this.currentToolState = false;
      }
    }
    if (event.target.id === 'clicked') {
      
      this.toolState = false;
      if (this.currentToolState) {
        this.defaultTool = this.selectedlasttool;
        this.onSelectTool(this.selectedlasttool, true, true, true);
        this.currentToolState = false;
      }
    }

    if(event.target.id==="drawingBoard"){
    this.showProfileDropdown = false;
    this.keyboardService.closeKeyboard()
    this.commonService.setAddResourceState(true);
    this.commonService.setLessonPlayerMode(false);
    this.commonService.setAddResourcePanel(false)
    //this.playerDrawerService.setDrawerOpenStatus(false);

    const userStae = {
      userView: '',
      currentState: ''
    };
    this.commonService.setUserProfileState(userStae);

    }
    if(event.target.id=== "drawingBoard" || "cdk-drop-list-1" || "playerdrawer"){
      //set addResources close;


    }
    

  }
  
  ngOnInit(): void {
    this.minimumScrollNumber = this.appConfigService.getConfig(
      'global_setting'
    )['minimum-scroll-tool'];

    this.eraseByElementSelection = this.appConfigService.getConfig(
      'global_setting'
    )['EraseByElementSelection'];
    this.selectedTheme = this.themeService.currentThemeName;
    this.defaultColor = this.appConfigService.getConfig('global_setting')[
      'defaultcolor'
    ];
    this.multiColumnLayout =
      this.appConfigService.getConfig('global_setting')[
        'toolbarMultiColumn'
      ] === GlobalConfigState.ENABLED;
    this.SelectedPenSize = this.appConfigService.getConfig('global_setting')[
      'default-stroke-size'
    ];
    const Cmodeconfigdata = this.appConfigService.getCmodeConfig();
    this.toolbarListData = [];
    this.toolsData = [];
    this.toolsData_contextual = [];
    const allCmodeData = this.appConfigService.getConfig('cmodes');
    Object.keys(allCmodeData).forEach(key => {
      this.toolbarListData.push(allCmodeData[key]);
    });
    this.theme = this.appConfigService.getConfig('themes');
    this.subjecticon = this.appConfigService.getConfig('general')['icons'];
    this.whiteboardp = this.appConfigService.getGlobalSettingConfig(
      'modulewhiteboard'
    );
    if (this.defaultColor) {
      this.colorpicker({ dynamic: this.defaultColor });
      this.onChangeColor(this.defaultColor, true);
    }
    if (Cmodeconfigdata) {
      this.authmode = Cmodeconfigdata.loginroute;
    }
    if (this.whiteboardp) {
      this.modetype = 'modulewhiteboard';
    } else {
      this.modetype = 'moduleplayer';
    }

    if (this.toolbarListData) {
      this.toolbarListData.forEach(e => {
        if (this.authmode === e.loginroute) {
          for (let z = 0; z < e.toolbar.length; z++) {
            if (e.toolbar[z].event === 'gtAdd') {
              if (e.toolbar[z].enabled) {
                this.gtAddtoolbar = e.toolbar[z];
              }
            }
            if (e.toolbar[z].event !== 'gtAdd') {
              if (e.toolbar[z].enabled) {
                if (e.toolbar[z].onlyif) {
                  for (let yy = 0; yy < e.toolbar[z].onlyif.length; yy++) {
                    for (
                      let ww = 0;
                      ww < e.toolbar[z].onlyif[yy].hasmodule.length;
                      ww++
                    ) {
                      if (
                        e.toolbar[z].onlyif[yy].hasmodule[ww] === this.modetype
                      ) {
                        this.toolsData.push(e.toolbar[z]);
                      }
                    }
                  }
                }
                if (!e.toolbar[z].onlyif) {
                  this.toolsData.push(e.toolbar[z]);
                }
              }
              if (!e.toolbar[z].enabled) {
                this.toolsData_contextual.push(e.toolbar[z]);
              }
            }
          }
        }
        //this.cdr.detectChanges();
      });
    }

    this.toolbarSubscription.add(
      this.authService.userState$.subscribe(userState => {
        this.isLoggedIn = userState.authStatus === AuthStateEnum.AUTH;
        // this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.userProfileService.getJSON().subscribe((data: any) => {
        this.userProfileImg = data.user_img;
      })
    );

    /*
    this.userProfileService.userProfileAPIData$.subscribe(data => {
      this.userData = data;
    });
    */

    this.toolbarSubscription.add(
      this.themeService.currentThemeNameSubject$.subscribe(data => {
        this.selectedTheme = data;

        this.themecolor(this.theme, this.selectedTheme);
        if (this.currentBackground !== undefined) {
          this.applyBackgroundImage(this.currentBackground);
        }
        //this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.toolbarService.toolbarAvailableHeightReduction$.subscribe(
        newValue => {
          this.toolAvailableHeightReduction = newValue;
          this.setToolBarHeight();
          // this.cdr.detectChanges();
        }
      )
    );

    this.toolbarSubscription.add(
      this.toolbarService.toolbarUserFlag$.subscribe(flag => {
        this.showProfileDropdown = flag;
        //this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
        this.toolState = false;
        this.showProfileDropdown = false;
        this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.toolbarService.toolbarSubmenuTop$.subscribe((top: any) => {
        if (top) {
          const dynamicTop = top.top;
          const dynamicPosition = top.position;
          const documentElement = document.documentElement;
          documentElement.style.setProperty(
            `--${'toolSubmenuTop'}`,
            `${dynamicTop + 'px'}`
          );
          documentElement.style.setProperty(
            `--${'toolSubmenuPosition'}`,
            `${dynamicPosition + 'px'}`
          );
        }
        // this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.toolbarService.currentSelectedTool.subscribe(newtoolbar => {
        this.currentTool =
          'gt' + newtoolbar.charAt(0).toUpperCase() + newtoolbar.slice(1);
        if (newtoolbar) {
          this.defaultTool =
            'gt' + newtoolbar.charAt(0).toUpperCase() + newtoolbar.slice(1);
          this.previousTool = this.toolbarService.previousSelectedTool;
          if (this.previousTool) {
            this.selectedlasttool = this.previousTool;
          } else {
            this.selectedlasttool = this.defaultTool;
          }
          if (this.getSelectedTool) {
            this.getSelectedTool = this.getSelectedTool;
          } else {
            this.getSelectedTool = this.defaultTool;
          }
          this.cdr.detectChanges();
        }
      })
    );
    this.setToolBarHeight();
    this.toolbarSubscription.add(
      this.toolbarService.gettoolbarside().subscribe(res => {
        this.toolbarSideState = res;
        this.showProfileDropdown = false;
        //this.cdr.detectChanges();
      })
    );
    this.toolbarSubscription.add(
      this.toolbarService.selectedSubjectObject$.subscribe(data => {
        this.toolsData.forEach((item, index) => {
          if (item.subid) {
            this.toolsData.splice(index, 1);
          }
        });
        this.toolsData.push(data);
        //this.scrolltoolbarlist.splice(8, 0, data);
        //this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.toolbarService.defaultToolColor.subscribe(color => {
        if (color) {
          this.currentSubTool = '';
          this.selectedTool = '';
          this.previousTool = '';
          this.defaultColor = color;
          this.colorpicker({ dynamic: this.defaultColor });
          this.onChangeColor(this.defaultColor, true);
        }
        this.cdr.detectChanges();
      })
    );

    this.toolbarSubscription.add(
      this.toolbarService.selectedSubjectId$.subscribe(newSubjectId => {
        this.appendSubjectClass = false;
        let customObjSubject = {
          enabled: 1,
          subid: '',
          event: '',
          tooltip: '',
          onlyif: '',
          icon: '',
          submenu: { instruction: '', options: [] }
        };

        this.toolsData.forEach((item, index) => {
          if (item.subid) {
            this.toolsData.splice(index, 1);
          }
        });
        if (!newSubjectId) {
          this.toolsData = this.toolsData.filter(function(
            element,
            index,
            array
          ) {
            return element !== element.subid;
          });
        }

        if (newSubjectId) {
          this.selectedsubject = [newSubjectId];
        }
        if (
          this.subjecticon &&
          this.subjecticon.length > 0 &&
          this.selectedsubject &&
          this.selectedsubject.length > 0
        ) {
          this.appendSubjectClass = true;
          this.subjecticon.forEach(m => {
            for (let s = 0; s < this.selectedsubject.length; s++) {
              if (this.selectedsubject[s] === m.subid) {
                customObjSubject.event = 'gt' + m.subtilte;
                customObjSubject.tooltip = m.subtilte;
                customObjSubject.icon = m.icon;
                customObjSubject.subid = m.subid;
                if (
                  this.toolsData_contextual &&
                  this.toolsData_contextual.length > 0
                ) {
                  for (
                    let sc = 0;
                    sc < this.toolsData_contextual.length;
                    sc++
                  ) {
                    customObjSubject.onlyif = this.toolsData_contextual[
                      sc
                    ].onlyif;
                    customObjSubject.submenu.instruction = this.toolsData_contextual[
                      sc
                    ].submenu.instruction;
                    let subjectSubmenuOption = this.toolsData_contextual[sc]
                      .submenu.options;
                    if (
                      subjectSubmenuOption &&
                      subjectSubmenuOption.length > 0
                    ) {
                      for (
                        let cso = 0;
                        cso < subjectSubmenuOption.length;
                        cso++
                      ) {
                        if (subjectSubmenuOption[cso].enabled) {
                          if (
                            subjectSubmenuOption[cso].onlyif &&
                            subjectSubmenuOption[cso].onlyif.length > 0
                          ) {
                            for (
                              let sIf = 0;
                              sIf < subjectSubmenuOption[cso].onlyif.length;
                              sIf++
                            ) {
                              let customhash =
                                subjectSubmenuOption[cso].onlyif[sIf].hasmodule;
                              let customSubjectId =
                                subjectSubmenuOption[cso].onlyif[sIf].subids;
                              if (
                                customhash.hasmodule &&
                                customhash.hasmodule.length > 0
                              ) {
                                for (
                                  let cmod = 0;
                                  cmod < customhash.hasmodule.length;
                                  cmod++
                                ) {
                                  if (
                                    customhash.hasmodule[cmod] === this.modetype
                                  ) {
                                    if (
                                      customSubjectId &&
                                      customSubjectId.length > 0
                                    ) {
                                      for (
                                        let subId = 0;
                                        subId < customSubjectId.length;
                                        subId++
                                      ) {
                                        if (
                                          customSubjectId[subId] ===
                                          customObjSubject.subid
                                        ) {
                                          customObjSubject.submenu.options.push(
                                            subjectSubmenuOption[cso]
                                          );
                                        }
                                      }
                                    } else {
                                      customObjSubject.submenu.options.push(
                                        subjectSubmenuOption[cso]
                                      );
                                    }
                                  }
                                }
                              } else {
                                if (
                                  customSubjectId &&
                                  customSubjectId.length > 0
                                ) {
                                  for (
                                    let subId = 0;
                                    subId < customSubjectId.length;
                                    subId++
                                  ) {
                                    if (
                                      customSubjectId[subId] ===
                                      customObjSubject.subid
                                    ) {
                                      customObjSubject.submenu.options.push(
                                        subjectSubmenuOption[cso]
                                      );
                                    }
                                  }
                                } else {
                                  customObjSubject.submenu.options.push(
                                    subjectSubmenuOption[cso]
                                  );
                                }
                              }
                            }
                          } else {
                            //customObjSubject.submenu.options.push(subjectSubmenuOption[cso])
                          }
                        }
                      }
                    }
                  }
                  this.toolbarService.selectedSubjectObject.next(
                    customObjSubject
                  );
                }
                //this.scrolltoolbarlist.push(m);
                //this.scrolltoolbarlist.splice(8, 0, m);
              }
            }
          });
        }
        //this.cdr.detectChanges();
      })
    );
  }
  // image convert start
  transform(html: any) {
    return this.sanitizer.bypassSecurityTrustHtml(html);
  }
  //image convert end

  //submenu position start
  onMenuSelected(val: any) {
    this.onSelectTool(
      val.state,
      val.clickedstatus,
      val.submenustatus,
      val.bodyclick
    );
  }
  //submenu position end

  //selected tool start
  onSelectTool(
    state: any,
    clickedstatus: boolean,
    submenustatus: boolean,
    bodyclick: boolean
  ) {
    this.gtBackgroundSubMenuTool = state;
    //this.playerDrawerService.setDrawerOpenStatus(false);
    if (clickedstatus) {
      this.toolState = true;
    } else {
      this.toolState = false;
    }
    if (state !== 'gtPen' || state !== 'gtZoom') {
      this.currentToolState = true;
    }
    if (state === 'gtPen') {
      submenustatus = false;
    }
    if (bodyclick) {
      this.toolState = false;
    }
    this.selectedTool = state;

    if (state === 'gtErase') {
      this.onSubSelectTool(state, true);
    }

    if (state === 'gtBackground') {
      //this.onsubSelectedStatus(state, true);
    }

    if (state === 'gtColor') {
      this.toolbarService.selectTool(ToolType.Pen);
    }

    if (state !== 'gtColor') {
      this.getSelectedTool = state;
      this.defaultTool = this.selectedTool;
      this.currentSubTool = state;
      this.previousTool = state;
      if (state === 'gtFocus') {
        this.focusToolState = !this.focusToolState;
        this.toolbarService.setFocusMode(this.focusToolState);
      }
      if (state && !submenustatus) {
        const newstate = state.substring(2).toLowerCase();
        this.toolbarService.selectTool(newstate);
      }
    }
  }
  //selected tool end

  shownUserData() {
    if (this.isLoggedIn) {
      this.userData = this.userProfileService.userData;
    console.log("A userProfile---->",this.userData)

    if(this.userData.firstName === '' && this.userData.middleName === ''  && this.userData.lastName === ''){
      this.userData.firstName = 'user name'
      }
      this.userProfileImg = 'assets/images/user.png';
      this.toolbarService.userProfileImg = this.userProfileImg;
    } else {
      this.userProfileService.getJSON().subscribe(data => {
        this.userData = data;
        this.userProfileImg = data.user_img;
        console.log("B userProfile---->",this.userData)
      });
    }
  }

  onSubMenuSelected(val: any) {
    this.onSubSelectTool(val.tool, val.state);
  }
  //selected sub tool start
  onSubSelectTool(tool: any, state: boolean) {
    this.selectedlasttool = tool;
    this.currentToolState = false;
    this.previousTool = this.selectedTool;
    this.toolbarService.setprevTool(this.selectedTool);
    this.currentSubTool = tool;
    this.selectedTool = tool;
    const newstate = tool.substring(2).toLowerCase();
    if (tool !== 'gtEraseWhiteboard' && tool !== 'gtEraseAnnotations') {
      this.toolbarService.selectTool(newstate);
    }
    if (tool === 'gtEraseWhiteboard') {
      tool = 'gtPen';
      this.toolbarService.setprevTool(tool);
      this.onSelectTool(tool, false, false, true);
      this.toolbarService.broadcastClearWhiteboard(true);
    }
    if (tool === 'gtEraseAnnotations') {
      tool = 'gtPen';
      this.toolbarService.setprevTool(tool);
      this.onSelectTool(tool, false, false, true);
      this.toolbarService.broadcastClearAllAnnotations(true);
    }

    if (this.gtBackgroundSubMenuTool === 'gtBackground') {
      this.applyBackgroundImage(tool);
    }
  }
  //selected sub tool end

  //getBackground svg start
  applyBackgroundImage(background: any) {
    this.currentBackground = background;
    const themeData = this.appConfigService.getConfig('themes');
    const currentTheme = themeData[this.selectedTheme];
    const svgPattern = currentTheme.default[background]['svgPattern'];
    //remove existing SVG
    const bgPattern = document.getElementById('bgPattern');
    if (bgPattern !== null) {
      bgPattern.remove();
    }
    // apply css background
    const documentElement = document.documentElement;
    for (const [key, value] of Object.entries(
      currentTheme.default[background]
    )) {
      documentElement.style.setProperty(`--${key}`, `${value}`);
    }
    // apply svg background
    if (svgPattern !== undefined) {
      this.renderSVG('bgPanGroup', svgPattern);
    }
  }
  //getBackground svg end

  //svg rendring start
  renderSVG(id, xml_string) {
    // add new SVG
    const doc = new DOMParser().parseFromString(xml_string, 'application/xml');
    const element = document.getElementById(id);
    const renderedStr = element.ownerDocument.importNode(
      doc.documentElement,
      true
    );
    element.appendChild(renderedStr);
  }
  //svg rendring end

  // user profile start

  clicktoshowProfile(event) {
    this.commonService.setLessonPlayerMode(false);
    this.playerDrawerService.setDrawerOpenStatus(false);
    // position submenu based on current tool selection
    const userStae = {
      userView: '',
      currentState: ''
    };
    this.commonService.setUserProfileState(userStae);
    const targetElement = event.target;
    let submenuPosition: any;
    const targetElementTop = targetElement.getClientRects()[0].top;
    const toolBarRef: any = document.getElementsByClassName('mytoolbar');
    const toolBarTop = toolBarRef[0].getClientRects()[0].top;
    const getToolbarRef = targetElement.getClientRects()[0];
    const final = targetElementTop - toolBarTop - 20;
    const userTopcal = getToolbarRef.top;
    if (this.toolbarSideState) {
      submenuPosition = getToolbarRef.left + getToolbarRef.width + 20;
    } else {
      submenuPosition =
        window.innerWidth - getToolbarRef.right + getToolbarRef.width + 20;
    }
    // end position submenu based on current tool selection
    let newPositionArray = {
      top: final,
      position: submenuPosition,
      userTop: userTopcal
    };
    this.toolbarService.toolbarSubmenuTop.next(newPositionArray);
    //this.showProfileDropdown = !this.showProfileDropdown;
    
    this.toolState = false;
    this.cdr.detectChanges();
    if(!this.showProfileDropdown){
      this.showProfileDropdown = true;
    }else{
      this.showProfileDropdown = false;
    }
    //console.log("--showProfileDropdown--",this.showProfileDropdown)
  }
  // user profile end

  doScroll(event) {
    this.showProfileDropdown = false;
    this.toolState = false;
    this.toolbarService.toolbarScroll.next(event);
  }
  onLongPressing(event) {
    if (this.multiColumnLayout) {
      return false;
    }
    this.doScroll(event);
  }
  onLongPress() {
    this.isLongPressed = !this.isLongPressed;
  }

  private colorpicker(theme: {}) {
    Object.keys(theme).forEach(k =>
      document.documentElement.style.setProperty(`--${k}`, theme[k])
    );
  }
  emitOnChangeColor(val) {
    //console.log("onChangePenEmit",val)

    this.onChangeColor(val.color, val.state);
  }
  onChangeColor(color: any, clickedstatus: boolean) {
    if (clickedstatus) {
      this.toolState = true;
    } else {
      this.toolState = false;
    }
    this.selectedTool = color;
    this.colorpicker({ dynamic: color });
    this.selectedColor = color;
    this.toolbarService.setToolColor(color);
  }

  themecolor(data, theme) {
    if (data) {
      this.themesOption = [];
      const keyNames = Object.keys(data);
      if (keyNames.length > 0) {
        for (let index = 0; index < keyNames.length; index++) {
          if (theme === keyNames[index]) {
            this.themesOption = data[keyNames[index]].toolbar.pen;
          }
        }
      }
    }
  }
  onChangePenEmit(val) {
    //console.log("onChangePenEmit",val)
    this.onChangePen(val.size, val.state);
  }
  onChangePen(size: any, clickedstatus: boolean) {
    this.toolState = !this.toolState;
    this.SelectedPenSize = size;
    this.toolbarService.setPenSize(size);
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    //this.userProfileService.userProfileAPIData.unsubscribe();
    this.toolbarSubscription.unsubscribe();
  }
}
