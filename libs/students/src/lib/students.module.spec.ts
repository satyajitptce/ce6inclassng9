import { async, TestBed } from '@angular/core/testing';
import { StudentsModule } from './students.module';

describe('StudentsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StudentsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StudentsModule).toBeDefined();
  });
});
