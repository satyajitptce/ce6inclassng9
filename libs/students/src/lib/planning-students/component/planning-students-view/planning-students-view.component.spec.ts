import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningStudentsViewComponent } from './planning-students-view.component';

describe('PlanningStudentsViewComponent', () => {
  let component: PlanningStudentsViewComponent;
  let fixture: ComponentFixture<PlanningStudentsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningStudentsViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningStudentsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
