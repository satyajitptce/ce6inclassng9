import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningStudentsViewComponent } from './component/planning-students-view/planning-students-view.component';
import { NbCardModule, NbIconModule, NbThemeModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

@NgModule({
  declarations: [PlanningStudentsViewComponent],
  imports: [CommonModule, NbCardModule, NbEvaIconsModule, NbIconModule],
  exports: [PlanningStudentsViewComponent],
  entryComponents: [PlanningStudentsViewComponent]
})
export class PlanningStudentsModule {}
