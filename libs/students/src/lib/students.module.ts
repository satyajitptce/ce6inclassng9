import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';

import { PlanningStudentsViewComponent } from './planning-students/component/planning-students-view/planning-students-view.component';
import { PlanningStudentsModule } from './planning-students/planning-students.module';

@NgModule({
  imports: [
    CommonModule,
    PlanningStudentsModule,

    LibConfigModule.forChild(PlanningStudentsViewComponent)
  ]
})
export class StudentsModule {}
