module.exports = {
  name: 'students',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/students',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
