import {
  Component,
  ElementRef,
  HostBinding,
  HostListener,
  Input,
  OnInit,
  ViewChild
} from '@angular/core';
import {
  AppConfigService,
  ContentResizeService,
  PanZoomService,
  ResourceType,
  ToolbarService,
  ToolType,
  CurriculumPlaylistService
} from '@tce/core';
import Konva from 'konva';
import { KonvaEventObject } from 'konva/types/Node';
import { ShapeConfig, Shape } from 'konva/types/Shape';
import { PlayerDrawerService } from '@app-teacher/services/player-drawer.service';

declare let testFactor: any;
declare let currentScaleFactor: any;
@Component({
  selector: 'tce-annotation',
  templateUrl: './annotation.component.html',
  styleUrls: ['./annotation.component.scss']
})
export class AnnotationComponent implements OnInit {
  @Input() canvasContext: ResourceType;
  @Input() ignoreScale = false;
  @Input() set stageId(stageId: string) {
    if (stageId && stageId !== this.activeStageId) {
      this.activeStageId = stageId;
      if (this.canvasContext !== ResourceType.TCEVIDEO) {
        this.sizingReferenceElement = this.elRef.nativeElement.parentElement;
        this.createOrLoadKonvaStage();
      }
    }
  }

  sizingReferenceElement: HTMLElement;
  sizingReferenceInterval;

  activeStageId: string;
  isAnnotationLayerHidden = false;
  isAnnotationLayerNonBlocking = false;

  isDrawing = false;
  konvaStage: Konva.Stage;
  konvaLayer: Konva.FastLayer;
  previousLine: Konva.Line;
  annotationTimeout: NodeJS.Timer;

  currentScale = 1;

  currentTool: string;
  currentToolColor: string;
  currentToolStroke: number;
  public strokeWidth: number;
  public selectedPenSize: any = 1;
  annotationBehavior: 'fade' | 'overlay' = 'overlay';
  boundPostFade = this.postFade.bind(this);
  newZoomValue: number = 1;
  isPlaylistDrawerOpen = false;

  @HostBinding('class.-hide-annotation-layer') get annotationLayerVisible() {
    return this.isAnnotationLayerHidden;
  }

  @HostBinding('class.-non-blocking') get annotationLayerNonBlocking() {
    return this.isAnnotationLayerNonBlocking;
  }

  @HostBinding('class.position-center') get positionCenter() {
    return this.canvasContext === ResourceType.TCEVIDEO;
  }

  @ViewChild('konvaContainer', { static: true }) konvaContainer: ElementRef<
    HTMLDivElement
  >;

  constructor(
    private elRef: ElementRef<HTMLDivElement>,
    private toolbarService: ToolbarService,
    private contentResizeService: ContentResizeService,
    private appConfigService: AppConfigService,
    private panzoomService: PanZoomService,
    private playerDrawerService: PlayerDrawerService,
  ) {
    this.toolbarService.clearAllAnnotationsBroadcaster$.subscribe(() => {
      this.clearLayer();
    });
  }

  @HostListener('contextmenu', ['$event'])
  private preventContextMenu(evnt: MouseEvent) {
    evnt.preventDefault();
    evnt.stopPropagation();
  }

  ngOnInit() {
    
    const defaultpensize = this.appConfigService.getConfig('global_setting')[
      'default-stroke-size'
    ];
    this.strokeWidth = parseInt(defaultpensize, 5);
    this.toolbarService.currentPenSize.subscribe(data => {
      if (data) {
        this.selectedPenSize = data;
        this.strokeWidth = parseInt(this.selectedPenSize, 5);
      }
    });

    this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
        //console.log("drawerOpenState-->>", flag);
        this.isPlaylistDrawerOpen = flag;
    });



    if (
      this.canvasContext === ResourceType.TCEVIDEO ||
      this.canvasContext === ResourceType.QUIZ
    ) {
      this.annotationBehavior = 'fade';
      if (
        this.canvasContext === ResourceType.QUIZ &&
        this.appConfigService.getGlobalSettingConfig('quizannotationbehaviour')
      ) {
        this.annotationBehavior = this.appConfigService.getGlobalSettingConfig(
          'quizannotationbehaviour'
        );
      }
    }
    this.toolbarService.currentSelectedTool$.subscribe(tool => {
      this.currentTool = tool;
      this.toggleVisiblity();
    });
    this.toolbarService.selectedToolColor$.subscribe(
      toolColor => (this.currentToolColor = toolColor)
    );

    if (!this.ignoreScale) 
    {
      this.panzoomService.currentZoomScale$.subscribe(
        newScale => {
          this.currentScale = newScale
          //console.log("1. current scale-->>",this.currentScale);
          //let tempStr = this.currentScale.toFixed(2)
          this.newZoomValue = Number(this.currentScale.toFixed(2));
          //console.log("newZoomValue-->>",Number(this.newZoomValue))
          //this.resizeStage()
      });
      
    }

    this.toggleVisiblity();

    if (this.canvasContext === ResourceType.TCEVIDEO) {
      //console.log("2. ResourceType.TCEVIDEO--");
      this.sizingReferenceInterval = setInterval(() => {
        const foundVideoIframe = this.elRef.nativeElement.parentElement.querySelector(
          'iframe'
        );
        if (foundVideoIframe) {
          clearInterval(this.sizingReferenceInterval);
          this.sizingReferenceElement = foundVideoIframe;
          setTimeout(() => {
            this.createOrLoadKonvaStage();
            //console.log("3. this.initListeners()--");
            this.initListeners();
          }, 500);
        }
      }, 1000);
    } else {
      //console.log("4. else this.initListeners()--");
      this.initListeners();
      
    }
  }

  initListeners() {
    const parentElement = this.sizingReferenceElement;
    window.addEventListener('resize', this.resizeStage.bind(this));
    parentElement.addEventListener('scroll', this.repositionStage.bind(this));
    this.resizeStage()
    this.contentResizeService.resizeContent$.subscribe(() => {
      //console.log("5. initListeners --- ")
      this.resizeStage();
    });
  }

  createOrLoadKonvaStage() {
    const stageJSON = sessionStorage.getItem(
      `${this.activeStageId}-annotations`
    );
      //console.log("--stageJSON-->>",stageJSON)
    if (stageJSON) {
      this.loadStage(stageJSON);
    } else {
      this.createStage();
    }

    this.konvaStage.on('mousedown touchstart', this.beginDrawing.bind(this));
    this.konvaStage.on('mousemove touchmove', this.handleDraw.bind(this));
    this.konvaStage.on(
      'mouseup touchend mouseleave',
      this.handleDrawingComplete.bind(this)
    );
  }

  private loadStage(stageJSON: string) {
    const parentDimensions = this.getParentDimensions();

    try {
      // Reset the stage y prior to loading, dynamically setting it after
      // an already loaded stage doesn't update the position
      const parsedStage = JSON.parse(stageJSON);
      parsedStage.attrs.y = 0;

      this.konvaStage = Konva.Stage.create(
        parsedStage,
        this.konvaContainer.nativeElement
      );
      this.konvaStage.width(parentDimensions.width);
      this.konvaStage.height(parentDimensions.height);
      this.konvaLayer = this.konvaStage.getLayers()[0] as Konva.FastLayer;
    } catch (err) {
      //console.error('Error loading existing konva stage: ', err);
      this.createStage();
    }
  }

  private createStage() {
    const container = this.konvaContainer.nativeElement;
    const parentDimensions = this.getParentDimensions();

    this.konvaStage = new Konva.Stage({
      container,
      width: parentDimensions.width,
      height: parentDimensions.height
    });
    this.konvaLayer = new Konva.FastLayer();
    this.konvaStage.add(this.konvaLayer);
  }

  private saveKonvaStage() {
    if (this.konvaStage) {
      sessionStorage.setItem(
        `${this.activeStageId}-annotations`,
        this.konvaStage.toJSON()
      );
    }
  }

  private beginDrawing({ evt }: KonvaEventObject<MouseEvent | TouchEvent>) {
    const PEN_STROKE_WIDTH = this.strokeWidth;
    const ERASER_STROKE_WIDTH = 20;
    const isPen = this.currentTool === ToolType.Pen;
    const isErase = this.currentTool === ToolType.Erase;

    if (!isPen && !isErase) {
      return;
    }
    evt.preventDefault();
    if (this.konvaContainer.nativeElement.classList.contains('fadeout')) {
      this.cancelFade();
    }
    if (this.annotationTimeout) {
      clearTimeout(this.annotationTimeout);
    }

    this.isDrawing = true;

    const pointerPosition = this.konvaStage.getPointerPosition();
    const stagePosition = this.konvaStage.getAbsolutePosition();
    //console.log("KINVA--handle draw this.currentScale--",this.currentScale)
    //console.log("KINVA--pointerPosition--",pointerPosition)

    this.previousLine = new Konva.Line({
      stroke: this.currentToolColor,
      lineCap: 'round',
      lineJoin: 'round',
      strokeWidth: isPen ? PEN_STROKE_WIDTH : ERASER_STROKE_WIDTH,
      globalCompositeOperation: isPen ? 'source-over' : 'destination-out',
      //points: [(pointerPosition.x - stagePosition.x) / this.currentScale, pointerPosition.y / this.currentScale - stagePosition.y]
      points: [
        (pointerPosition.x - stagePosition.x) - this.currentScale, 
        (pointerPosition.y - stagePosition.y) - this.currentScale]
      
    });

    this.konvaLayer.add(this.previousLine);
    this.handleDraw({
      evt: evt,
      target: <any>evt.target,
      currentTarget: <any>evt.currentTarget,
      cancelBubble: evt.cancelBubble
    });
  }

  private stopDrawing({ evt }: KonvaEventObject<MouseEvent | TouchEvent>) {
    this.isDrawing = false;
  }

  private handleDraw({ evt }: KonvaEventObject<MouseEvent | TouchEvent>) {
    //console.log("BB HANDLE DRAW")
    if (!this.isDrawing) {
      return;
    }

    evt.preventDefault();
    

    const pointerPosition = this.konvaStage.getPointerPosition();
    const stagePosition = this.konvaStage.getAbsolutePosition();
    //console.log("KINVA--this.currentScale--",this.currentScale)
    // const newPoints = this.previousLine
    //   .points()
    //   .concat([
    //     (pointerPosition.x - stagePosition.x) / this.currentScale,
    //     pointerPosition.y / this.currentScale - stagePosition.y
    //   ]);
    const newPoints = this.previousLine
      .points()
      .concat([
        (pointerPosition.x - stagePosition.x) - this.currentScale,
        (pointerPosition.y - stagePosition.y) - this.currentScale
      ]);

    this.previousLine.points(newPoints);
    this.konvaLayer.batchDraw();
  }

  private handleDrawingComplete() {
    //console.log("--handleDrawingComplete--")
    this.isDrawing = false;

    if (this.annotationBehavior === 'fade') {
      const fadeOutDelay = this.appConfigService.getConfig('global_setting')[
        'annotationFadeOutDelay'
      ];

      this.annotationTimeout = setTimeout(() => {
        this.fadeLayer();
      }, fadeOutDelay);
    } else {
      //console.log("BB ELSE--")
      this.saveKonvaStage();
    }
  }

  private cancelFade() {
    this.konvaContainer.nativeElement.removeEventListener(
      'transitionend',
      this.boundPostFade
    );
    this.konvaContainer.nativeElement.classList.add('block-transition');
    this.konvaContainer.nativeElement.classList.remove('fadeout');
    setTimeout(() => {
      this.konvaContainer.nativeElement.classList.remove('block-transition');
    }, 500);
  }
  private fadeLayer() {
    this.konvaContainer.nativeElement.removeEventListener(
      'transitionend',
      this.boundPostFade
    );
    this.konvaContainer.nativeElement.addEventListener(
      'transitionend',
      this.boundPostFade
    );
    this.konvaContainer.nativeElement.classList.add('fadeout');
    //satyajit added fixed issue where after fadeout lines were reappearing
    this.clearLayer();
  }

  private postFade() {
    this.clearLayer();
    this.konvaContainer.nativeElement.classList.add('block-transition');
    this.konvaContainer.nativeElement.classList.remove('fadeout');
    setTimeout(() => {
      this.konvaContainer.nativeElement.classList.remove('block-transition');
    }, 500);
  }

  private clearLayer() {
    this.konvaLayer.destroyChildren();
    this.konvaLayer.clear();
  }

  private repositionStage() {
    const dy = this.sizingReferenceElement.scrollTop;
    this.konvaStage.container().style.transform = 'translateY(' + dy + 'px)';
    this.konvaStage.y(-dy);
    this.konvaStage.batchDraw();
  }

  private getParentDimensions(): {
    width: number;
    height: number;
  } {
    const parent = this.sizingReferenceElement;
    if (this.canvasContext === ResourceType.TCEVIDEO) {
      //console.log("PARENT--TCEVIDEO-->",parent.id)
      const parentBoundingClientRect = parent.getBoundingClientRect();
      //console.log("7. getParentDimensions-->W--",parent.getBoundingClientRect().width, "--H: ",parent.getBoundingClientRect().height);
      //console.log("CW->",parent.clientWidth, "CH->",parent.clientHeight)
      return {
        width: parentBoundingClientRect.width,
        height: parentBoundingClientRect.height
      };
    }else{
      return {
        width: parent.clientWidth,
        height: parent.clientHeight
      };
    }
    
  }

  private resizeStage() {        
    setTimeout(() => {
    
      const parentDimensions = this.getParentDimensions();   
      /*   
      if(typeof testFactor !== undefined){
          //console.log("external testFactor-->>", testFactor)
      }
      if(typeof currentScaleFactor !== undefined){
        currentScaleFactor = this.currentScale;
        //console.log("external currentScaleFactor-->>", currentScaleFactor)
      }*/

      

      let bottomDifferenceFactor = 1;
      if(this.isPlaylistDrawerOpen){
        bottomDifferenceFactor = 1.05;
      }else{
        bottomDifferenceFactor = 1.6;
      }

      this.konvaStage.width(parentDimensions.width * this.getZoomFactor());
      this.konvaStage.height((parentDimensions.height - (35 * (this.currentScale * bottomDifferenceFactor))) * this.getZoomFactor());
    }, 200);
  }

  // Added 15th March 2021 - satyajit, arijit
  // to resolve factorial difference between konva stage with tce player while zooming and resizing
  getZoomFactor(){
    let val = 1;
    let expectedZoomValue: any = [
      {"zoomVal":0.4, "factor":2.5},
      {"zoomVal":0.5, "factor":2},
      {"zoomVal":0.75, "factor":1.34},
      {"zoomVal":1, "factor":1},
      {"zoomVal":1.25, "factor":0.8},
      {"zoomVal":1.5, "factor":0.67},
      {"zoomVal":1.75, "factor":0.57},
      {"zoomVal":2, "factor":0.5},
    ]
    for(let i = 0; i < expectedZoomValue.length; i++){
      if(this.newZoomValue === expectedZoomValue[i].zoomVal){
        val = expectedZoomValue[i].factor;
      }
    }
    return val;
  }

  private toggleVisiblity() {
    this.currentTool === ToolType.Pen || this.currentTool === ToolType.Erase
      ? (this.isAnnotationLayerNonBlocking = false)
      : (this.isAnnotationLayerNonBlocking = true);
  }

   /*
      let mytestFactor = 1;
      if(this.newZoomValue === 0.4){
        mytestFactor = 2.5;
      }else if(this.newZoomValue === 0.5){
        mytestFactor = 2;
      }else if(this.newZoomValue === 0.75){
        mytestFactor = 1.34;
      }else if(this.newZoomValue === 1){
        mytestFactor = 1;
      }else if(this.newZoomValue === 1.25){
        mytestFactor = 0.8;
      }else if(this.newZoomValue === 1.5){
        mytestFactor = 0.67;
      }else if(this.newZoomValue === 1.75){
        mytestFactor = 0.57;
      }else if(this.newZoomValue === 2){
        mytestFactor = 0.5;
      }*/
}
