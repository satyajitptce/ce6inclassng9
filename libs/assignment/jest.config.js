module.exports = {
  name: 'assignment',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/assignment',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
