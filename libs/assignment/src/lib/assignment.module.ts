import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { PlanningAssignmentViewComponent } from './planning-assignment/component/planning-assignment-view/planning-assignment-view.component';

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(PlanningAssignmentViewComponent)
  ]
})
export class AssignmentModule {}
