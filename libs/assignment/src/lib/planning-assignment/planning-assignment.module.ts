import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningAssignmentViewComponent } from './component/planning-assignment-view/planning-assignment-view.component';
import { AssignmentModule } from '../assignment.module';
import {
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbTabsetModule
} from '@nebular/theme';

@NgModule({
  declarations: [PlanningAssignmentViewComponent],
  imports: [
    CommonModule,
    NbTabsetModule,
    NbSelectModule,
    NbCardModule,
    NbIconModule
  ],
  exports: [PlanningAssignmentViewComponent],
  entryComponents: [PlanningAssignmentViewComponent]
})
export class PlanningAssignmentModule {}
