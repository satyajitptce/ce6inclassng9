import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningAssignmentViewComponent } from './planning-assignment-view.component';

describe('PlanningAssignmentViewComponent', () => {
  let component: PlanningAssignmentViewComponent;
  let fixture: ComponentFixture<PlanningAssignmentViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningAssignmentViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningAssignmentViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
