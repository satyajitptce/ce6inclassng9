import { async, TestBed } from '@angular/core/testing';
import { AssignmentModule } from './assignment.module';

describe('AssignmentModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AssignmentModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(AssignmentModule).toBeDefined();
  });
});
