module.exports = {
  name: 'player-widgets',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-widgets',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
