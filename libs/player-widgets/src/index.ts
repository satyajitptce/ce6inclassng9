export * from './lib/player-widgets.module';

export {
  WidgetsConfigService
} from './lib/core/services/widgets-config.service';

export {
  WidgetsSharedService
} from './lib/core/services/widgets-shared.service';

export {
  PlayerWidgetsComponent
} from './lib/player-widgets/player-widgets.component';

export { WidgetSharedModule } from './lib/core/modules/widgets-shared.module';