import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DynamicWidgetsModules } from './core/interfaces/widgets.interface';
import { WidgetSharedModule } from './core/modules/widgets-shared.module';
import { WidgetsConfigModule } from './core/modules/widgets-config.module';
import { PlayerWidgetsComponent } from './player-widgets/player-widgets.component';

const Widgets: DynamicWidgetsModules[] = [
  {
    widgetId: 'toolcompass',
    loadChildren: () => import('./widgets/maths/compass/compass.module').then(mod => mod.CompassModule)
  },
  {
    widgetId: 'toolruler',
    loadChildren: () =>
      import('./widgets/maths/ruler/ruler.module').then(mod => mod.RulerModule)
  }
]

@NgModule({
  imports: [
    CommonModule,
    WidgetsConfigModule.forRoot(Widgets),
    WidgetSharedModule.forRoot()
  ],
  declarations: [PlayerWidgetsComponent]
})
export class PlayerWidgetsModule {}
