import { NgModule, ModuleWithProviders } from '@angular/core';
import { WidgetsSharedService } from './../services/widgets-shared.service';

@NgModule({})
export class WidgetSharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: WidgetSharedModule,
      providers: [WidgetsSharedService]
    };
  }
}
