import { ModuleWithProviders, NgModule } from '@angular/core';
import { WIDGETS_MODULE } from '../tokens/widgets-config-token';
import { WidgetsConfigService } from '../services/widgets-config.service';

@NgModule({})
export class WidgetsConfigModule {
  static forRoot(widgets: any): ModuleWithProviders<WidgetsConfigModule> {
    return {
      ngModule: WidgetsConfigModule,
      providers: [
        WidgetsConfigService,
        {
          provide: WIDGETS_MODULE,
          useValue: widgets
        }
      ]
    };
  }
}
