import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class WidgetsSharedService {
  public widgetListner = new BehaviorSubject(null);

  constructor() {}

  setDrawLayout(data) {
    this.widgetListner.next(data);
  }

  getDrawLayout() {
    return this.widgetListner.asObservable();
  }
}
