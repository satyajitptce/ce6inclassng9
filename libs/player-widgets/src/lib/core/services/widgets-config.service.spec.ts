import { TestBed } from '@angular/core/testing';

import { WidgetsConfigService } from './widgets-config.service';

describe('WidgetsConfigService', () => {
  let service: WidgetsConfigService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WidgetsConfigService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
