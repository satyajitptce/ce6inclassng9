import {
  Injectable,
  Inject,
  Compiler,
  Injector,
  NgModuleRef,
  ModuleWithComponentFactories
} from '@angular/core';
import { WIDGETS_MODULE } from './../tokens/widgets-config-token';
import { DynamicWidgetsModules } from './../interfaces/widgets.interface';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WidgetsConfigService {
  public widgestMapId = {
    toolcompass: {
      loaded: false
    },
    toolruler: {
      loaded: false
    }
  };
  constructor(
    @Inject(WIDGETS_MODULE) private widgets: DynamicWidgetsModules[],
    private compiler: Compiler,
    private injector: Injector
  ) {}

  getComponentFactoryResolver(widgetId): Observable<any> {
    return new Observable(observe => {
      const selectedWidget = this.widgets.filter(
        widget => widget.widgetId === widgetId
      );
      // console.log(
      //   'WidgetsConfigService -> getWidgetModule -> selectedWidget',
      //   selectedWidget
      // );
      if (selectedWidget.length !== 0) {
        this.getWidgetModule(selectedWidget[0]).then(
          ComponentFactory => observe.next(ComponentFactory),
          error => {
            throw throwError(error);
          }
        );
      } else {
        throw throwError(`Widget Id: ${widgetId} not found!`);
      }
    });
  }

  async getWidgetModule(selectedWidget: DynamicWidgetsModules) {
    const module = await selectedWidget.loadChildren().then(mod => mod);
    const component = module.default;
    const moduleFactory: ModuleWithComponentFactories<
      any
    > = this.compiler.compileModuleAndAllComponentsSync(module);
    const moduleFactoryRef: NgModuleRef<
      any
    > = moduleFactory.ngModuleFactory.create(this.injector);
    const componentFactory = await moduleFactoryRef.componentFactoryResolver.resolveComponentFactory(
      component
    );
    return componentFactory;
  }

  checkSelectedToolExsitsInWidget(widgetId) {
    return (
      Object.keys(this.widgestMapId).filter(widget => widget === widgetId)
        .length > 0
    );
  }

  isWidgetLoaded(widgetId) {
    return this.widgestMapId[widgetId].loaded;
  }

  widgetLoadedSuccess(widgetId) {
    this.widgestMapId[widgetId].loaded = true;
  }

  widgetDestroyedSuccess(widgetId) {
    //console.log(widgetId);
    this.widgestMapId[widgetId].loaded = false;
  }
}
