export interface DynamicWidgetsModules {
  /** Unique identifier, used in the application to retrieve a ComponentFactory. */
  widgetId: string;
  /** Path to component module. */
  loadChildren: () => Promise<any>;
}
