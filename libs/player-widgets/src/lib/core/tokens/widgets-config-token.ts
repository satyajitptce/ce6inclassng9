import { InjectionToken } from '@angular/core';

export const WIDGETS_MODULE = new InjectionToken<any>('WIDGETS_MODULE');
