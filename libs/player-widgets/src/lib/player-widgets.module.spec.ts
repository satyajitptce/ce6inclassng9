import { async, TestBed } from '@angular/core/testing';
import { PlayerWidgetsModule } from './player-widgets.module';

describe('PlayerWidgetsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerWidgetsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerWidgetsModule).toBeDefined();
  });
});
