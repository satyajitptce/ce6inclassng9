import { Component, OnInit, ViewChild, ElementRef, Renderer2, Output, Input, EventEmitter, OnDestroy } from '@angular/core';
import { WidgetsSharedService } from './../../../core/services/widgets-shared.service';
import { ToolbarService } from '@tce/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PlayerDrawerService } from '@app-teacher/services';

@Component({
  selector: 'tce-compass',
  templateUrl: './compass.component.html',
  styleUrls: ['./compass.component.scss']
})
export class CompassComponent implements OnInit {
  @Input() widgetId: string;
  @Output() move: EventEmitter<ElementRef> = new EventEmitter<ElementRef>();
  @Output() destroyPlayer: EventEmitter<boolean> = new EventEmitter<boolean>();
  private destroy$: Subject<boolean> = new Subject<boolean>();
  
  wbEvent: any;
  selectedTool;
  xmlns = 'http://www.w3.org/2000/svg';
  xhtml = 'http://www.w3.org/1999/xhtml';

  @ViewChild('widget', { static: true }) widgetRef: ElementRef;
  //@ViewChild('rulerBoard', { static: true }) mySvg: ElementRef;
  @ViewChild('myPanGroup', { static: true }) myPanGroup: ElementRef;
  
  public offsetX = 0;
  public offsetY = 0;
  //start1;
  centerPtX;
  centerPtY;
  radius = 100
  anglex
  angley
  degreeVal = 0;
  strPath;
  outerStrPath;
  pointWithoutRect = { x: 0, y: 0 };
  angle = 0;
  mysvg
  line
  path
  arc
  currentDrawing
  centerHandle
  radiusHandle
  plotterHandle
  startPositionHandle
  pt
  c1
  c2
  centerp
  d3
  p1
  p2
  isCenterHandleSelected
  isRadiusHandleSelected
  isStartPositionHandleSelected
  isPlotterHandleSelected
  currentControl
  isControlMousedDown
  //arcx = 0
  //arcy = 0
  radians = 0
  degrees = 0
  currentAngle = 0
  meX = 0
  meY = 0
  //pointer1
  //cx1
  //cy1
  lineStartx
  lineStarty
  lineEndx
  lineEndy
  strokeColor
  strokeWidth
  panZoom

  
  isPlayerMove;
  widgetPlayerRef;
  dragOffset = [0, 0];
  radiusToCm = ""
  constructor(
    private widgetsSharedService: WidgetsSharedService,
    private toolbarService: ToolbarService,
    private renderer: Renderer2,
    private playerDrawerService: PlayerDrawerService
  ) {}

  ngOnInit(): void {
    this.toolbarService.currentSelectedTool$
    .pipe(takeUntil(this.destroy$))
    .subscribe(newToolType => {
      this.selectedTool = newToolType;
      //console.log("newToolType-->> ",newToolType)
    });
    this.toolbarService.selectedToolColor$.subscribe(color => {
      this.strokeColor = color;
    });
    this.toolbarService.currentPenSize.subscribe(penSize => {
        this.strokeWidth = penSize;
    });
    this.toolbarService.clearWhiteboardBroadcaster$.subscribe(() => {
      this.destroyPlayer.emit()
    });

    this.panZoom = this.toolbarService.globalPanZoom;
    
    
    this.mysvg = document.getElementById("svgDraw");
    
		
		this.line = document.createElementNS('http://www.w3.org/2000/svg', "path");
		this.path = document.createElementNS('http://www.w3.org/2000/svg', "path");
		this.arc = document.createElementNS('http://www.w3.org/2000/svg', "path");
	
		
		this.centerHandle  = document.getElementById("centerHandle");
		this.radiusHandle  = document.getElementById("radiusHandle");
		this.plotterHandle  = document.getElementById("plotterHandle");
		this.startPositionHandle  = document.getElementById("startPositionHandle");
		
    
    const element = this.widgetRef.nativeElement;
    element.addEventListener('mousedown', this.onControlsDown.bind(this));
    element.addEventListener('mousemove', this.onMouseMove.bind(this), false);
    element.addEventListener('mouseup', this.onEndDrawing.bind(this));
    element.addEventListener('mouseleave', this.onEndDrawing.bind(this));
    
    this.createStage(300,300);
    
  }

  createStage(w: any, h: any){
    this.mysvg.setAttribute('width', w +'px')
    this.mysvg.setAttribute('height', h +'px')

		const svgW = this.mysvg.getAttribute('width').split('px');
		const svgH = this.mysvg.getAttribute('height').split('px');
    //console.log("svg w & h -->>",svgW[0], svgH[0])
    this.centerPtX = svgW[0] / 2;
    this.centerPtY = svgH[0] / 2;
		this.anglex = this.centerPtX - this.radius;
    this.angley = this.centerPtY;

		this.c1 = "a" + " " +this.radius+ " " + ","+ " " +this.radius + " " +0 + " " +1+ " " +","+ " " +0+ " " +(this.radius*2)+ " " +","+ " " +0; 
		this.c2 = "a" + " " + this.radius+ " " + ","+ " " +this.radius + " " +0 + " " +1+ " " +","+ " " +0+ " " +-(this.radius*2)+ " " +","+ " " +0; 
		this.centerp = "M"+" "+ (this.centerPtX - this.radius) +" "+ (this.centerPtY);
		this.d3 = this.centerp +" " + this.c1 + this.c2; 
		this.path.setAttribute("d",this.d3);
		this.path.setAttribute("fill",'none');
		this.path.setAttribute("stroke",'gray');
		this.path.setAttribute("stroke-width",2);
		this.path.setAttribute("opacity",1);
		this.path.setAttribute("stroke-linecap",'round');
		this.path.setAttribute("stroke-dasharray",'0,4');
		this.mysvg.appendChild(this.path);
		
		
		//line
		this.p1 = "M"+" "+ (this.centerPtX - this.radius) +" "+ (this.centerPtY);
		this.p2 = "L"+" "+ (this.centerPtX) +" "+ (this.centerPtY);
		this.line.setAttribute("d",this.p1 + this.p2);
		this.line.setAttribute("fill",'none');
		this.line.setAttribute("stroke",'#979797');
		this.line.setAttribute("stroke-width", 3);
    this.line.setAttribute("stroke-linecap",'square');
		this.mysvg.appendChild(this.line);

    this.centerHandle.setAttribute("cx",this.centerPtX);
		this.centerHandle.setAttribute("cy",this.centerPtY);
		this.centerHandle.appendChild(this.radiusHandle);
    
    this.radiusHandle.setAttribute("cx",this.centerPtX- this.radius + 60);
		this.radiusHandle.setAttribute("cy",this.centerPtY);
		this.mysvg.appendChild(this.radiusHandle);
		
		this.plotterHandle.setAttribute("cx", this.centerPtX - this.radius);
		this.plotterHandle.setAttribute("cy", this.centerPtY);
		this.mysvg.appendChild(this.plotterHandle);
		
		
		this.startPositionHandle.setAttribute("cx",this.centerPtX- this.radius + 30);
		this.startPositionHandle.setAttribute("cy",this.centerPtY);
		this.mysvg.appendChild(this.startPositionHandle);
    
    
    


    this.widgetPlayerRef = document.getElementById('player-widgets');
    // const playerMoveBtn = document.getElementById("mymove");  
    // playerMoveBtn.style.top = (this.centerPtY - 17) + "px";
    // playerMoveBtn.style.left = (this.centerPtX - 17) + "px";

    const playerCloseBtn = document.getElementById("myclose");
    playerCloseBtn.style.top = (this.centerPtY + 5) + "px";
    playerCloseBtn.style.left = (this.centerPtX + 15) + "px";
    this.mysvg.appendChild(this.centerHandle);

    const radusValueDisplay = document.getElementById("radusValue");
    radusValueDisplay.style.top = (this.centerPtY + 30) + "px";
    radusValueDisplay.style.left = (this.centerPtX - 30) + "px";

    
  }

  


  /// compasss functions
  
  onControlsDown(e: any){
    e.preventDefault();
    
    
    this.wbEvent = e;
    this.currentControl = e.target.id;
    this.isControlMousedDown = true;
    switch (this.currentControl) {
      case "radiusHandle":
        this.isRadiusHandleSelected = true;
        this.playerDrawerService.setDrawerOpenStatus(false);
        break;
      case "startPositionHandle":
        this.isStartPositionHandleSelected = true;
        this.playerDrawerService.setDrawerOpenStatus(false);
        break;
      case "plotterHandle":
        this.isPlotterHandleSelected = true;
        this.pointWithoutRect = this.getMousePointWithoutRect(e);
        this.onPlotterDown(e);
        this.playerDrawerService.setDrawerOpenStatus(false);
        break;
      case "centerHandle":
        this.playerDrawerService.setDrawerOpenStatus(false);
        this.isCenterHandleSelected = true;
        this.isPlayerMove = true;
        this.dragOffset = [
          this.widgetPlayerRef.offsetLeft - e.clientX,
          this.widgetPlayerRef.offsetTop - e.clientY
        ];
        break;
      default:
        
        
    }
  }
 
  
  onPlotterDown(e: any){
    //console.log("this.panZoom--->>",this.panZoom)
    //e.preventDefault();
    this.wbEvent = e;
    this.currentDrawing = document.createElementNS('http://www.w3.org/2000/svg', "path");
    this.currentDrawing.setAttribute("id",'plottedPath');
    this.currentDrawing.setAttribute("fill",'none');
    this.currentDrawing.setAttribute("stroke",this.strokeColor);
    this.currentDrawing.setAttribute("stroke-width", this.strokeWidth);
    this.currentDrawing.setAttribute("stroke-linecap", "square");
    this.currentDrawing.setAttribute("stroke-linejoin", "square");
    //this.degreeVal = Math.round(this.degrees + 180 + this.currentAngle);
    //this.radians = this.degreeVal * (Math.PI / 180);  // convert degree to radians
    //this.meX = this.centerPtX + Math.cos(this.radians) * this.radius;  
    //this.meY = this.centerPtY + Math.sin(this.radians) * this.radius;
    //console.log("mousePoint-->>",this.getMousePoint().x, this.getMousePoint().y)

    //this.strPath = " M " + this.getMousePoint().x + " " + this.getMousePoint().y;
    //this.outerStrPath = " M " + this.pointWithoutRect.x + " " + this.pointWithoutRect.y;
    //-----

    this.degreeVal = Math.round(this.degrees + 180);
    this.radians = this.degreeVal * (Math.PI / 180);  // convert degree to radians
    //console.log("this.degreeVal-->>",this.degreeVal)
    //console.log("this.radians-->>",this.radians)
    this.meX = this.centerPtX + Math.cos(this.radians) * this.radius;  
    this.meY = this.centerPtY + Math.sin(this.radians) * this.radius;

    // satyajit calc for circle in whiteboard
    let nn = this.centerHandle.getBoundingClientRect()
    const newCenterPTX = nn.left + (nn.width/2) - this.panZoom.getPan().x;
    const newCenterPTY = nn.top + (nn.height/2) - this.panZoom.getPan().y;

    const newX = newCenterPTX / this.panZoom.getSizes().realZoom;
    const newY = newCenterPTY / this.panZoom.getSizes().realZoom;
    
    const newMeX = newX + Math.cos(this.radians) * this.radius;  
    const newMeY = newY + Math.sin(this.radians) * this.radius;
    //-----

    this.strPath = " M "+ this.meX + " " + this.meY;
    this.outerStrPath = " M "+ newMeX + " " + newMeY;

    //this.strPath = " M " + this.getMousePoint().x + " " + this.getMousePoint().y;
    //this.outerStrPath = " M " + this.pointWithoutRect.x + " " + this.pointWithoutRect.y;
    this.currentDrawing.setAttribute("d", this.strPath);
    //console.log("this.plotterHandle-->>", this.plotterHandle)
  }
  
  

  onMouseMove(e: any){
    this.wbEvent = e;   
     e.preventDefault(); 
     
    if(this.isControlMousedDown){
      if(this.isRadiusHandleSelected){
        this.handleRadiusControl();
      }    
      if(this.isStartPositionHandleSelected){
        this.handleStartPoitionControl();
      }    
      if(this.isPlotterHandleSelected){
        this.handlePlotterControl(e);   
      }
      if(this.isCenterHandleSelected){
        this.handleCenterControl(e);
      }
    }



    
    
    /*
    if(this.isControlMousedDown){
      this.currentControl = e.target.id;
      console.log("this.currentControl-->>",this.currentControl)
      switch (this.currentControl) {
        case "radiusHandle":
          this.isRadiusHandleSelected = true;
          this.handleRadiusControl();
          break;
        case "startPositionHandle":
          this.handleStartPoitionControl();
          break;
        case "plotterHandle":
          this.isPlotterHandleSelected = true;
          this.handlePlotterControl(e);   
          break;
        case "centerHandle":
          this.handleCenterControl(e); 
          break;
        default:     
      }
    }
    */



  }

 

  

  handleRadiusControl(){
    this.radius = this.getRadius(this.getMousePoint().x,this.getMousePoint().y,this.centerPtX,this.centerPtX) + 60;
    
    //console.log("r--->> ",Math.round(this.radius))
    var whVal = Math.round(this.radius * 2) + 50;
    this.createStage(whVal, whVal);

    //if(this.radius < 301)
    {
      const onePX = 0.0264583333 // cm
      const oneCM = 37.7952755906 // px
      const px = 1.3333333333 // px
      const totalPx = this.radius * onePX; 
      //console.log("totalPx--",totalPx)
      this.radiusToCm = "r = "+totalPx.toFixed(2) + " cm"
       
       this.c1 = "a" + " " +this.radius+ " " + ","+ " " +this.radius + " " +0 + " " +1+ " " +","+ " " +0+ " " +(this.radius * 2)+ " " +","+ " " + 0; 
       this.c2 = "a" + " " + this.radius+ " " + ","+ " " +this.radius + " " +0 + " " +1+ " " +","+ " " +0+ " " +-(this.radius * 2)+ " " +","+ " " + 0; 
       this.centerp = "M"+" "+ (this.centerPtX - this.radius) +" "+ (this.centerPtY);
       this.d3 = this.centerp +" " + this.c1 + this.c2; 
       this.path.setAttribute("d", this.d3);
       //this.mysvg.appendChild(this.path);
       
       
       //line
       this.p1 = "M"+" "+ (this.centerPtX - this.radius) +" "+ (this.centerPtY);
       this.p2 = "L"+" "+ (this.centerPtX) +" "+ (this.centerPtY);
       this.line.setAttribute("d",this.p1+this.p2);
  
       
       //this.mysvg.appendChild(this.line);
       
       //plotterHandle
       this.plotterHandle.setAttribute("cx",this.centerPtX - this.radius);
       this.plotterHandle.setAttribute("cy",this.centerPtY);
       //this.mysvg.appendChild(this.plotterHandle);
       
       //radiusHandle
       this.radiusHandle.setAttribute("cx",this.centerPtX- this.radius+60);
       this.radiusHandle.setAttribute("cy",this.centerPtY);
       //this.mysvg.appendChild(this.radiusHandle);
       
       //startPositionHandle
       this.startPositionHandle.setAttribute("cx", this.centerPtX - this.radius+30);
       this.startPositionHandle.setAttribute("cy", this.centerPtY);
       //this.mysvg.appendChild(this.startPositionHandle);
       this.handleStartPoitionControl();
    }

    
  }

  handleStartPoitionControl(){
    this.degreeVal = Math.round(this.degrees + 180);
    this.anglex = this.getMousePoint().x;
    this.angley = this.getMousePoint().y;
    this.rotateElement(this.line,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.radiusHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.plotterHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.startPositionHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
  }

  handlePlotterControl(e){
    //console.log(e.target.id)
    this.wbEvent = e;
    e.preventDefault();
    this.rotateElement(this.radiusHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.line,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.plotterHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);
    this.rotateElement(this.startPositionHandle,this.centerPtX,this.centerPtY,this.getMousePoint().x,this.getMousePoint().y);


    this.degreeVal = Math.round(this.degrees + 180);
    this.radians = this.degreeVal * (Math.PI / 180);  // convert degree to radians
    
    this.meX = this.centerPtX + Math.cos(this.radians) * this.radius;  
    this.meY = this.centerPtY + Math.sin(this.radians) * this.radius;

    // satyajit calc for circle in whiteboard
    let nn = this.centerHandle.getBoundingClientRect()
    const newCenterPTX = nn.left + (nn.width/2) - this.panZoom.getPan().x;
    const newCenterPTY = nn.top + (nn.height/2) - this.panZoom.getPan().y;

    const newX = newCenterPTX / this.panZoom.getSizes().realZoom;
    const newY = newCenterPTY / this.panZoom.getSizes().realZoom;
    
    const newMeX = newX + Math.cos(this.radians) * this.radius;  
    const newMeY = newY + Math.sin(this.radians) * this.radius;
    
    
    

    this.strPath += " L "+ this.meX + " " + this.meY;
    this.outerStrPath += " L "+ newMeX + " " + newMeY;
    
    this.currentDrawing.setAttribute("d", this.strPath);
    this.mysvg.appendChild(this.currentDrawing);
    
    this.currentAngle = this.degrees;
     
     
     
     
     this.mysvg.appendChild(this.plotterHandle);
  }

  rotateElement(el, originX, originY, towardsX, towardsY){
    this.degrees = Math.atan2(towardsY - originY, towardsX - originX) * 180 / Math.PI + 180;
    this.degrees = Math.round(this.degrees)
    console.log("degrees-->",this.degrees)
    el.setAttribute(
      'transform',
      'translate('
      +originX+','+originY+
      ') rotate('+
      this.degrees
      +') translate('
      +(-originX)+','+(-originY)+')'
    );
  }

  
 handleCenterControl(e){
  const mousePosition = { x: e.clientX,  y: e.clientY};
  let leftPos = mousePosition.x + this.dragOffset[0] + 'px';
  let topPos = mousePosition.y + this.dragOffset[1] + 'px';
    if(this.isPlayerMove)
    {
      //e.preventDefault();
      
      this.widgetPlayerRef.style.left = leftPos;//mousePosition.x + this.dragOffset[0] + 'px';
      this.widgetPlayerRef.style.top = topPos;//mousePosition.y + this.dragOffset[1] + 'px';
    }
 }
  
  getRadius(xA,yA,xB,yB){
    let rx = xA - xB;
    let ry = yA - yB;
    return Math.sqrt(rx *rx + ry * ry);
    }
  
  
  
  onEndDrawing(){
    //console.log("--END PLOTTING--")
    this.radiusToCm = "";
    this.isControlMousedDown = false;
    this.isCenterHandleSelected = false;
    this.isRadiusHandleSelected = false;
    
    this.isStartPositionHandleSelected = false;
    this.isPlayerMove = false;

    if(this.isPlotterHandleSelected){
        this.setDrawLayout();
        if(this.currentDrawing !== null){
          this.currentDrawing.remove();
        }
        this.isPlotterHandleSelected = false;
    }
    

    
  }

  // Get point in global SVG space
  cursorPoint(){
    //this.pt.x = evt.clientX; this.pt.y = evt.clientY;
    return this.getMousePoint(); //this.pt.matrixTransform(this.mysvg.getScreenCTM().inverse());
  }
  
  wbgetMousePoint() {
    const pgX = this.getPageCordinates().pgX;
    const pgY = this.getPageCordinates().pgY;

    const myX = pgX - this.offsetX - this.panZoom.getPan().x;
    const myY = pgY - this.offsetY - this.panZoom.getPan().y;
    const newX = myX / this.panZoom.getSizes().realZoom;
    const newY = myY / this.panZoom.getSizes().realZoom;
    return { x: newX, y: newY };
  }
  getMousePoint() {
    const rect = this.widgetRef.nativeElement.getBoundingClientRect();
    const myX = this.wbEvent.clientX - rect.left;
    const myY = this.wbEvent.clientY - rect.top;
    return { x: myX, y: myY };
  }
  
  getMousePointWithoutRect(e) {
    this.wbEvent = e;
    const myX = this.wbEvent.clientX - this.panZoom.getPan().x;
    const myY = this.wbEvent.clientY - this.panZoom.getPan().y;
    const newX = myX / this.panZoom.getSizes().realZoom;
    const newY = myY / this.panZoom.getSizes().realZoom;
    return { x: newX, y: newY };
  }

  getPageCordinates() {
    let pggX, pggY;
    pggX = this.wbEvent.pageX;
    pggY = this.wbEvent.pageY;
    return { pgX: pggX, pgY: pggY };
  }

  setDrawLayout(){
    this.widgetsSharedService.setDrawLayout(this.outerStrPath);
  }
}