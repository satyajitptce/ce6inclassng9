import { Component, OnInit, ViewChild, ElementRef, Renderer2, Output, EventEmitter } from '@angular/core';
import { WidgetsSharedService } from './../../../core/services/widgets-shared.service';
import { ToolbarService } from '@tce/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'tce-compass',
  templateUrl: './compass.component.html',
  styleUrls: ['./compass.component.scss']
})
export class CompassComponent implements OnInit {
  @Output() move: EventEmitter<ElementRef> = new EventEmitter<ElementRef>();
  private destroy$: Subject<boolean> = new Subject<boolean>();
  private path;
  wbEvent: any;
  selectedTool;
  xmlns = 'http://www.w3.org/2000/svg';
  xhtml = 'http://www.w3.org/1999/xhtml';
  currentCircleDrawing = null;
  currentRadiusDrawing = null;
  centerToRadiusLine = null;
  isMouseDownCircle = false;
  isMouseDownRadius = false;
  garbageData = [];

  //drawing
  opacity = 1;
  strokeWidth: any = '5';
  strokeColor: string = 'red';
  strPath = '';
  lineStrPath = '';
  strRadiusPath = '';

  //points and offsets
  point = { x: 0, y: 0 };
  startX: any;
  startY: any;
  offsetX = 0;
  offsetY = 0;
  drawRadius = true;
  radiusDetails = {
    radius: 0,
    x: 0,
    y: 0
  }

  //element = dom reference div

  @ViewChild('widget', { static: true }) widgetRef: ElementRef;
  @ViewChild('myCompassBoard', { static: true }) myCompassBoard: ElementRef;
  @ViewChild('myPanGroup', { static: true }) myPanGroup: ElementRef;


  constructor(
    private widgetsSharedService: WidgetsSharedService,
    private toolbarService: ToolbarService,
    private renderer: Renderer2
  ) {}

  ngOnInit(): void {
    this.toolbarService.currentSelectedTool$
    .pipe(takeUntil(this.destroy$))
    .subscribe(newToolType => {
      this.selectedTool = newToolType;
    });
    
    // this.path = this.svgPath.nativeElement.attributes.d.value;
    // this.drawLayout();

    const element = this.widgetRef.nativeElement;

    element.addEventListener('mousedown', this.onMouseDown.bind(this));
    element.addEventListener('mousemove', this.onMouseMove.bind(this), false);
    element.addEventListener('mouseup', this.onMouseUp.bind(this));

    element.addEventListener('mouseleave', this.onMouseLeave.bind(this));
    element.addEventListener('touchstart', this.onMouseDown.bind(this));
    element.addEventListener('touchmove', this.onMouseMove.bind(this));
    element.addEventListener('touchend', this.onMouseUp.bind(this));
  }

  // drawLayout() {
  //   this.path = this.widgetsConfigService.setDrawLayout(this.path);
  // }

  onMouseDown(e: MouseEvent) {
    this.wbEvent = e;
    this.point = this.getMousePoint();
    if(this.drawRadius){
      this.isMouseDownCircle = true;
      this.startCircleDrawing();
    }else{
      this.isMouseDownRadius = true;
      this.startRadiusDrawing();
    }
  }

  onMouseMove(e: MouseEvent) {
    this.wbEvent = e;
    if (this.isMouseDownCircle && this.drawRadius){
      this.continueCircleDrawing();
      this.createLineFromCenterToRadius();
    }
    if (this.isMouseDownRadius && !this.drawRadius){
      this.continueRadiusDrawing();
    }
  }

  onMouseUp(e: MouseEvent) {
    this.wbEvent = e;
    if(this.drawRadius){
      //this.widgetsSharedService.setDrawLayout(this.strPath);
      this.endCircleDrawing();
      this.toggle();
    }else{
      this.endRadiusDrawing();
      this.toggle();
    }
  }

  onMouseLeave(e: MouseEvent) {
     this.wbEvent = e;
    //  this.endCircleDrawing();
  }

  toggle(){
    this.drawRadius = !this.drawRadius;
  }

  startCircleDrawing() {
    this.startX = this.getPageCordinates().pgX - this.offsetX;
    this.startY = this.getPageCordinates().pgY - this.offsetY;
    this.currentCircleDrawing = document.createElementNS(this.xmlns, 'path');
    this.currentCircleDrawing.setAttributeNS(null, 'fill', 'none');
    this.currentCircleDrawing.setAttributeNS(null, 'stroke-width', 1);
    this.currentCircleDrawing.setAttributeNS(
      null,
      'id',
      'path_' + this.getRandomID()
    );
    this.currentCircleDrawing.setAttributeNS(null, 'opacity', "0.5");
    this.currentCircleDrawing.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.currentCircleDrawing.setAttributeNS(null, 'stroke-linecap', 'round');
    this.currentCircleDrawing.setAttributeNS(null, 'stroke-dasharray', '5,5');
    this.currentCircleDrawing.setAttributeNS(null, 'stroke', "white");
    this.currentCircleDrawing.setAttributeNS(null, 'fill', 'none');

    this.myPanGroup.nativeElement.appendChild(this.currentCircleDrawing);
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.strPath +=
      ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
    this.currentCircleDrawing.setAttribute('d', this.strPath);

    

    this.currentRadiusDrawing = document.createElementNS(this.xmlns, 'path');
    this.currentRadiusDrawing.setAttributeNS(null, 'fill', 'none');
    this.currentRadiusDrawing.setAttributeNS(null, 'stroke-width', 5);
    this.currentRadiusDrawing.setAttributeNS(
      null,
      'id',
      'path_' + this.getRandomID()
    );
    this.currentRadiusDrawing.setAttributeNS(null, 'opacity', "1");
    this.currentRadiusDrawing.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.currentRadiusDrawing.setAttributeNS(null, 'stroke-linecap', 'round');
    this.currentRadiusDrawing.setAttributeNS(null, 'stroke', "red");
    this.currentRadiusDrawing.setAttributeNS(null, 'fill', 'none');




    //line
    this.centerToRadiusLine = document.createElementNS(this.xmlns, 'path');
    this.centerToRadiusLine.setAttributeNS(null, 'fill', 'none');
    this.centerToRadiusLine.setAttributeNS(null, 'stroke-width', 2);
    this.centerToRadiusLine.setAttributeNS(null, 'id', 'line');
    this.centerToRadiusLine.setAttributeNS(null, 'opacity', "1");
    this.centerToRadiusLine.setAttributeNS(null, 'stroke-linejoin', 'round');
    this.centerToRadiusLine.setAttributeNS(null, 'stroke-linecap', 'round');
    this.centerToRadiusLine.setAttributeNS(null, 'stroke', "red");    

    this.lineStrPath = 'M' + this.point.x + ' ' + this.point.y;
    this.lineStrPath += ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
    this.centerToRadiusLine.setAttribute('d', this.lineStrPath);
    this.myPanGroup.nativeElement.appendChild(this.centerToRadiusLine);
    //line
  }

  startRadiusDrawing() {
    this.myPanGroup.nativeElement.appendChild(this.currentRadiusDrawing);
    this.strPath = 'M' + this.point.x + ' ' + this.point.y;
    this.strPath +=
      ' L' + this.getMousePoint().x + ' ' + this.getMousePoint().y;
    this.currentRadiusDrawing.setAttribute('d', this.strPath);
  }

  // continuePathDrawing() {
  //   const currentMousePoint = this.getMousePoint();
  //   this.strPath += ' L' + currentMousePoint.x + ' ' + currentMousePoint.y;
  //   this.currentCircleDrawing.setAttribute('type', 'path');
  //   this.currentCircleDrawing.setAttribute('d', this.strPath);
  //   // this.widgetsSharedService.setDrawLayout(this.strPath);
  // }

  continueRadiusDrawing(){
    this.strPath += this.helperGetArc(
      this.radiusDetails.x,
      this.radiusDetails.y,
      this.radiusDetails.radius,
      0,
      180
    );
    this.currentRadiusDrawing.setAttribute('d', this.strPath);
  }

  createLineFromCenterToRadius(){

    const tempPt = this.getMousePoint();
    const startX = this.point.x, startY = this.point.y,
      mouseX = tempPt.x,
      mouseY = tempPt.y;
    this.lineStrPath = 'M' + startX + ' ' + startY;
    this.lineStrPath += ' L' + mouseX + ' ' + mouseY;
    console.log("--lineStrPath-->>",this.lineStrPath)
    this.centerToRadiusLine.setAttribute('d', this.lineStrPath);
  }

  continueCircleDrawing() {
    this.strPath = '';
    const radius = Math.abs(
      this.getPageCordinates().pgX - this.offsetX - this.startX
    );
    this.radiusDetails = {
      radius: radius,
      x: this.point.x,
      y: this.point.y
    }
    this.strPath += this.helperGetArc(
      this.point.x,
      this.point.y,
      radius,
      0,
      359
    );
    this.currentCircleDrawing.setAttribute('d', this.strPath);
  }

  endCircleDrawing() {
    // drawing is completed
    this.isMouseDownCircle = false;
    this.garbageData.push(this.currentCircleDrawing);
    this.currentCircleDrawing = null;
  }


  endRadiusDrawing() {
    this.isMouseDownRadius = false;
    this.currentRadiusDrawing = null;
    this.clearDrawingBoard();
  }

  clearDrawingBoard() {
    for (let i = 0; i < this.garbageData.length; i++) {
      const element = this.garbageData[i];
      if (element !== null) {
        element.remove();
      }
    }
    this.garbageData = [];
  }

  getMousePoint() {
    const rect = this.myCompassBoard.nativeElement.getBoundingClientRect();
    const newX = this.wbEvent.clientX - rect.left;
    const newY = this.wbEvent.clientY - rect.top;
    return { x: newX, y: newY };
  }
  
  getPageCordinates() {
    let pggX, pggY;
    //let event: any;
    //console.log("TouchEvent-->> ", TouchEvent);
    //console.log("2 -- event-->> ", event.changedTouches);

    if (this.wbEvent.touches) {
      const touches = this.wbEvent.changedTouches;
      for (let i = 0; i < touches.length; i++) {
        const touch = touches[i];
        pggX = touch.pageX;
        pggY = touch.pageY;
      }
      pggX = this.wbEvent.targetTouches[0].clientX;
      pggY = this.wbEvent.targetTouches[0].clientY;
    } else {
      pggX = this.wbEvent.pageX;
      pggY = this.wbEvent.pageY;
    }
    return { pgX: pggX, pgY: pggY };
  }

  getRandomID() {
    return (
      Math.random()
        .toString(36)
        .substring(2, 15) +
      Math.random()
        .toString(36)
        .substring(2, 15)
    );
  }

  helperGetArc(x: any, y: any, radius: any, startAngle: any, endAngle: any) {
    const start = this.helperPolarToCartesian(x, y, radius, endAngle);
    const end = this.helperPolarToCartesian(x, y, radius, startAngle);
    const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1';
    const d = [
      'M',
      start.x,
      start.y,
      'A',
      radius,
      radius,
      0,
      largeArcFlag,
      0,
      end.x,
      end.y
    ].join(' ');
    return d;
  }

  helperPolarToCartesian(
    centerX: any,
    centerY: any,
    radius: any,
    angleInDegrees: any
  ) {
    const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0;
    return {
      x: centerX + radius * Math.cos(angleInRadians),
      y: centerY + radius * Math.sin(angleInRadians)
    };
  }

  polarToCartesian(centerX, centerY, radius, angleInDegrees) {
    var angleInRadians = (angleInDegrees-90) * Math.PI / 180.0;
  
    return {
      x: centerX + (radius * Math.cos(angleInRadians)),
      y: centerY + (radius * Math.sin(angleInRadians))
    };
  }
}