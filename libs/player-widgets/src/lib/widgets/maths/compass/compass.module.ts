import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CompassComponent } from './compass.component';

@NgModule({
  declarations: [
    CompassComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CompassComponent
  ]
})

export class CompassModule {
  static default = CompassComponent;
}
