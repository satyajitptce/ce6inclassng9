import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RulerComponent } from './ruler.component';

@NgModule({
  declarations: [
    RulerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RulerComponent
  ]
})
export class RulerModule {
  static default = RulerComponent;
}
