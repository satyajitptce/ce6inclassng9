import { Component, OnInit, ViewChild, ElementRef, Renderer2, Output, Input, EventEmitter, OnDestroy } from '@angular/core';
import { WidgetsSharedService } from './../../../core/services/widgets-shared.service';
import { ToolbarService } from '@tce/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { PlayerDrawerService } from '@app-teacher/services';

@Component({
  selector: 'tce-ruler',
  templateUrl: './ruler.component.html',
  styleUrls: ['./ruler.component.scss']
})
export class RulerComponent implements OnInit {
  @Input() widgetId: string;
  @Output() move: EventEmitter<ElementRef> = new EventEmitter<ElementRef>();
  @Output() destroyPlayer: EventEmitter<boolean> = new EventEmitter<boolean>();
  private destroy$: Subject<boolean> = new Subject<boolean>();

  private panZoom
  private path;
  wbEvent: any;
  selectedTool = 'path';
  xmlns = 'http://www.w3.org/2000/svg';
  xhtml = 'http://www.w3.org/1999/xhtml';
  currentDrawing = null;
  isMouseDown = false;
  garbageData = [];

  //drawing
  opacity = 0.8;
  strokeWidth: any = '2';
  strokeColor: string = 'red';
  strPath = '';
  outerStrPath = '';

  //points and offsets
  point = { x: 0, y: 0 };
  pointWithoutRect = { x: 0, y: 0 };
  startX: any;
  startY: any;
  offsetX = 0;
  offsetY = 0;

  //element = dom reference div


  // from ruler.js
  //flag    
  isRulerMouseDown = false;
  isRulerExtended = false;
  isDrawLine = false;
  isLeftStripDown = false;
  isRightStripDown = false;
  
  // calculationa variables
  rular_width = 380;
  rulerStartX = 0;
  rulerStartY = 10;
  rangeRightX = 40;
  rangeLeftX = 30;
  extenedStripX = 0;//rulerStartX  + rular_width;


  //Element variable
  mysvg;
  rulerExtendButton;
  mainRuler;
  rulerRotateButton;
  drawLineSelector;    
  rangeRight;
  rangeLeft;
  currentControl;

  // only for draw line
  lineStartX;
  lineStartY;
    distance;
  isStart =  false;
  strPathEnd;
  strPathStart;

  outerLineStartX;
  outerLineStartY;
  outerStrPathStart;
  outerStrPathEnd;



  @ViewChild('widget', { static: true }) widgetRef: ElementRef;

  //@ViewChild('rulerBoard', { static: true }) drawingBoard: ElementRef;
  
  isPlayerMove;
  widgetPlayerRef;
  dragOffset = [0, 0];
  constructor(
    private widgetsSharedService: WidgetsSharedService,
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService
  ) {}

  ngOnInit(): void {
    // this.path = this.svgPath.nativeElement.attributes.d.value;
    // this.drawLayout();

    this.toolbarService.currentSelectedTool$.pipe(takeUntil(this.destroy$)).subscribe(newToolType => {
      this.selectedTool = newToolType; 
    });
    this.toolbarService.selectedToolColor$.subscribe(color => {
      this.strokeColor = color;
    });
    this.toolbarService.currentPenSize.subscribe(penSize => {
        this.strokeWidth = penSize;
    });
    this.toolbarService.clearWhiteboardBroadcaster$.subscribe(() => {
      this.destroyPlayer.emit()
    });

    this.panZoom = this.toolbarService.globalPanZoom;
    this.widgetPlayerRef = document.getElementById('player-widgets');
    
    this.mysvg = document.getElementById("svgDraw");   
    
    const element = this.widgetRef.nativeElement; 
    element.addEventListener('mousedown', this.onMouseDown.bind(this));
    element.addEventListener('mousemove', this.onMouseMove.bind(this), false);
    element.addEventListener('mouseup', this.onMouseUp.bind(this));
    element.addEventListener('mouseleave', this.onMouseUp.bind(this));
    
    this.mysvg = document.getElementById("svgDraw");
    this.rulerExtendButton = document.getElementById("rulerExtendButton");
    this.mainRuler = document.getElementById("mainRuler");
    this.rulerRotateButton = document.getElementById("rulerRotateButton");
    this.drawLineSelector = document.getElementById("drawLineSelector");
    this.rangeRight = document.getElementById("rangeRight");
    this.rangeLeft = document.getElementById("rangeLeft");

    this.createStage();   
  }


    createStage(){                        
        this.extenedStripX = this.rulerStartX + this.rular_width;                    
        this.mainRuler.setAttribute("width", this.rular_width);
        this.rulerExtendButton.setAttribute("x", this.extenedStripX);
        this.rulerRotateButton.setAttribute("x", this.extenedStripX);
        this.drawLineSelector.setAttribute("x", this.extenedStripX);
        this.rangeRight.setAttribute("x", this.rangeRightX);
        this.rangeLeft.setAttribute("x", this.rangeLeftX);

        this.mysvg.appendChild(this.rangeRight);
        this.mysvg.appendChild(this.rangeLeft);
    }

  

  onMouseDown(e: any) {
    this.wbEvent = e;
    this.isMouseDown = true;
    this.point = this.getMousePoint();
    this.pointWithoutRect = this.getMousePointWithoutRect();

    this.currentControl = e.target.id;
        this.isMouseDown = true;
        console.log(this.currentControl);

        switch (this.currentControl) {
        case "rangeRight":
            this.isRightStripDown = true;
            break;
        case "rangeLeft":
            this.isLeftStripDown = true;
            break;
        case "drawLineSelector":
            this.isDrawLine = true;
            break;
        case "rulerExtendButton":
            this.isRulerExtended = true
            break;
        case "mainRuler":
            this.isRulerMouseDown = true;
            this.isPlayerMove = true;
            this.dragOffset = [
              this.widgetPlayerRef.offsetLeft - e.clientX,
              this.widgetPlayerRef.offsetTop - e.clientY
            ];
            break;
        default:
        }
        if(this.isDrawLine && this.isMouseDown){
          this.generateLine();
        }
    
  }

  onMouseMove(e: MouseEvent) {
    this.wbEvent = e;
    //if (this.isMouseDown) 
    {      
      const mousePosition = { x: e.clientX,  y: e.clientY};
    let leftPos = mousePosition.x + this.dragOffset[0] + 'px';
    let topPos = mousePosition.y + this.dragOffset[1] + 'px';
      if(this.isPlayerMove)
      {
        this.widgetPlayerRef.style.left = leftPos;//mousePosition.x + this.dragOffset[0] + 'px';
        this.widgetPlayerRef.style.top = topPos;//mousePosition.y + this.dragOffset[1] + 'px';
      }
      if(this.isRulerExtended) {
        this.rular_width  = this.getLength(this.getMousePoint().x, this.getMousePoint().y, this.rulerStartX, this.rulerStartY)
        if(this.rular_width > 0){
          this.mysvg.setAttribute("width", this.rular_width + 20);
          this.createStage();
        }
      }
    
      if(this.getMousePoint().x > this.rulerStartX && this.getMousePoint().x < this.extenedStripX){
        if(this.isLeftStripDown){
          var getRangeRightX = this.rangeRight.getAttribute("x");
          if(this.getMousePoint().x < getRangeRightX ) {
              this.rangeLeft.setAttribute("x", this.getMousePoint().x);
              this.rangeLeftX = this.getMousePoint().x;
          }            
        }
        if(this.isRightStripDown){
          var getRangeLeftX = this.rangeLeft.getAttribute("x")
          if(this.getMousePoint().x > getRangeLeftX){
            this.rangeRight.setAttribute("x", this.getMousePoint().x);
            this.rangeRightX = this.getMousePoint().x;
          }
        }
    }
      if(this.isDrawLine && this.isMouseDown){    
        this.drawLine();
      }
    }
  }

  onMouseUp(e: MouseEvent) {
    this.wbEvent = e;    
    this.endDrawing();
  }

  onMouseLeave(e: MouseEvent) {
    this.wbEvent = e;
    this.endDrawing();
  }

  

  endDrawing() {
    // drawing is completed
    if (this.isMouseDown) {

      //if(this.wbEvent.target.id === 'svgDraw'){
       // this.outerStrPathEnd = " L" + this.getMousePointWithoutRect().x + " " + this.getMousePointWithoutRect().y;
       // this.outerStrPath =  this.outerStrPathStart + this.outerStrPathEnd;
        this.widgetsSharedService.setDrawLayout(this.outerStrPath);
     // }
      

      this.outerStrPath = "";
      this.strPath = "";
      
      this.isPlayerMove = false;
      this.isMouseDown = false;
      this.isRulerMouseDown = false;
      this.isRulerExtended = false;
      
      this.isLeftStripDown = false;
      this.isRightStripDown = false;
      this.garbageData.push(this.currentDrawing);
      this.clearDrawingBoard();
      this.currentDrawing = null;
    }
  }

  clearDrawingBoard() {
    console.log('this.garbageData-->> ', this.garbageData);
    for (let i = 0; i < this.garbageData.length; i++) {
      const element = this.garbageData[i];
      if (element !== null) {
        element.remove();
      }
    }
    this.garbageData = [];
  }

  getLength(xA,yA,xB,yB){
    let rx = xA - xB;
    let ry = yA - yB;
    return Math.sqrt(rx *rx + ry * ry);    
  }


  
  generateLine(){
    this.isStart = true;
    this.lineStartX = this.getMousePoint().x; 
    this.lineStartY = this.getMousePoint().y;

    this.outerLineStartX = this.getMousePointWithoutRect().x; 
    this.outerLineStartY = this.getMousePointWithoutRect().y;

    
    this.currentDrawing = document.createElementNS('http://www.w3.org/2000/svg', "path");
  }
//test
  drawLine(){
    if(this.wbEvent.target.id === "svgDraw"){
       // if(this.lineStartX < this.getMousePoint().x){              
        //  if(this.lineStartY < 50 || this.lineStartY > 80){
        if(this.lineStartY < 50){
          this.lineStartY = 5;

        }if(this.lineStartY >80){
          this.lineStartY = 94;
        }
        //  }
        if(this.isStart){
          this.strPathStart = "M"+" "+ this.lineStartX +" "+ this.lineStartY +" ";
          this.isStart =  false;
          this.strPathEnd = " L" + this.getMousePoint().x + " " + this.lineStartY;
          this.strPath =  this.strPathStart + this.strPathEnd;
            //--- PATH STR for whiteboard
          this.outerStrPathStart = "M"+" "+ this.outerLineStartX +" "+ this.outerLineStartY +" ";
          // this.outerStrPathEnd = " L" + this.getMousePointWithoutRect().x + " " + this.getMousePointWithoutRect().y;
          // this.outerStrPath =  this.outerStrPathStart + this.outerStrPathEnd;
          
        }else {
          this.strPathEnd += " L" + this.getMousePoint().x + " " + this.lineStartY;
          this.strPath =  this.strPathStart + this.strPathEnd;
          //--- PATH STR for whiteboard
            this.outerStrPathEnd = " L" + this.getMousePointWithoutRect().x + " " + this.getMousePointWithoutRect().y;
            this.outerStrPath =  this.outerStrPathStart + this.outerStrPathEnd;
        }		
        this.currentDrawing.setAttribute("id", "line_"+this.getRandomID());
        this.currentDrawing.setAttribute("d", this.strPath);

        this.currentDrawing.setAttribute("stroke", this.strokeColor);
        this.currentDrawing.setAttribute("stroke-width", this.strokeWidth);
        this.mysvg.appendChild(this.currentDrawing);

              //this.garbageData.push(this.currentDrawing);
          //}
     // }
    }
    
   
}
  getMousePoint() {
    const rect = this.widgetRef.nativeElement.getBoundingClientRect();
    const myX = this.wbEvent.clientX - rect.left;
    const myY = this.wbEvent.clientY - rect.top;
    return { x: myX, y: myY };
  }
  
  getMousePointWithoutRect(){    
    const myX = this.wbEvent.clientX - this.panZoom.getPan().x;
    const myY = this.wbEvent.clientY - this.panZoom.getPan().y;
    const newX = myX / this.panZoom.getSizes().realZoom;
    const newY = myY / this.panZoom.getSizes().realZoom;
    return { x: newX, y: newY };
  }

  getPageCordinates() {
    let pggX, pggY;
    //let event: any;
    //console.log("TouchEvent-->> ", TouchEvent);
    //console.log("2 -- event-->> ", event.changedTouches);

    if (this.wbEvent.touches) {
      const touches = this.wbEvent.changedTouches;
      for (let i = 0; i < touches.length; i++) {
        const touch = touches[i];
        pggX = touch.pageX;
        pggY = touch.pageY;
      }
      pggX = this.wbEvent.targetTouches[0].clientX;
      pggY = this.wbEvent.targetTouches[0].clientY;
    } else {
      pggX = this.wbEvent.pageX;
      pggY = this.wbEvent.pageY;
    }
    return { pgX: pggX, pgY: pggY };
  }
  getRandomID() {
    return (
      Math.random()
        .toString(36)
        .substring(2, 15) +
      Math.random()
        .toString(36)
        .substring(2, 15)
    );
  }

  // drawLayout() {
  //   this.path = this.widgetsConfigService.setDrawLayout(this.path);
  // }

}
