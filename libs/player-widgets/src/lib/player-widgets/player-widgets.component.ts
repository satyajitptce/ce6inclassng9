import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  Renderer2,
  Input,
  ViewContainerRef,
  Output,
  EventEmitter,
  OnDestroy
} from '@angular/core';
import { AuthenticationService, AuthStateEnum, ToolbarService, ToolType } from '@tce/core';
import { WidgetsConfigService } from './../core/services/widgets-config.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'tce-player-widgets',
  templateUrl: './player-widgets.component.html',
  styleUrls: ['./player-widgets.component.scss']
})
export class PlayerWidgetsComponent implements OnInit, OnDestroy {
  @Input() widgetId: string;
  @Output() destroyPlayer: EventEmitter<boolean> = new EventEmitter<boolean>();
  @ViewChild('widgetRef', { read: ViewContainerRef }) private widgetRef: ViewContainerRef;
  @ViewChild('move', { static: true }) move: ElementRef;
  @ViewChild('player', { static: true }) player: ElementRef;
  private destroy$: Subject<boolean> = new Subject<boolean>();
  public selectedTool;
  public selectedWidget;
  public lastMouseX: number;
  public lastMouseY: number;
  public dragActive = false;
  public dragOffset = [0, 0];
  public isPanning;
  public toolbarAction;

  constructor(
    private renderer: Renderer2,
    private toolbarService: ToolbarService,
    private widgetsConfigService: WidgetsConfigService,
    private authService: AuthenticationService
  ) { }

  ngOnInit(): void {
    if (this.widgetId) {
      this.selectedWidget = this.widgetId;
      this.loadWidget();
    }
    this.moveCapabilities();
    this.toolbarService.currentSelectedTool$
    .pipe(takeUntil(this.destroy$))
    .subscribe(newToolType => {
      console.log("toolType--",newToolType);
      this.selectedTool = newToolType;
      if(this.selectedTool === "pan"){
        this.isPanning = true;
      }else{
        this.isPanning = false;
      }
    });

    this.toolbarService.toolBarActionDone$.subscribe(value => {
      console.log('value from wb toolbar-',value);
      
      //this.isPanning = value;
      
    })

    this.authService.userState$.subscribe(authState => {
      if(authState.authStatus === AuthStateEnum.NO_AUTH){
        this.closePlayer();
      }
    });
  }

  loadWidget() {
    this.widgetsConfigService
      .getComponentFactoryResolver(this.selectedWidget)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        response => {
          const componentRef = this.widgetRef.createComponent(response);
          const instance = componentRef.instance as any;
          this.renderer.addClass(componentRef.location.nativeElement, 'widget');
          instance.move.subscribe((elementRef: ElementRef) => {
            this.move = elementRef;
            this.moveCapabilities();
          });
          instance.destroyPlayer.subscribe(() => {
            this.closePlayer();
          })
          this.widgetsConfigService.widgetLoadedSuccess(this.selectedWidget);
        },
        error => {
          console.log('PlayerWidgetsComponent -> loadWidget -> error', error);
        }
      );
  }

  closePlayer() {
    this.destroyPlayer.emit(true);
  }

  clearDataOfWb() {}

  sendDataOfWb() {}

  moveCapabilities(){
    this.renderer.listen(
      this.move.nativeElement,
      'mousedown',
      (e: MouseEvent) => {
        if(this.selectedTool === ToolType.Pan)
        {
          this.dragActive = true;
          this.dragOffset = [
            this.player.nativeElement.offsetLeft - e.clientX,
            this.player.nativeElement.offsetTop - e.clientY
          ];
        }
      }
    );
    this.renderer.listen(
      this.move.nativeElement,
      'mousemove',
      (e: MouseEvent) => {
        if (this.dragActive && this.selectedTool === ToolType.Pan) {
          e.preventDefault();
          var mousePosition = {
            x: e.clientX,
            y: e.clientY
          };
          this.player.nativeElement.style.left =
            mousePosition.x + this.dragOffset[0] + 'px';
          this.player.nativeElement.style.top =
            mousePosition.y + this.dragOffset[1] + 'px';
        }
      }
    );
    this.renderer.listen(
      this.move.nativeElement,
      'mouseup',
      (e: MouseEvent) => {
        if(this.selectedTool === ToolType.Pan){
          this.dragActive = false;
        }
      }
    );
    this.renderer.listen(
      this.move.nativeElement,
      'mouseleave',
      (e: MouseEvent) => {
        this.dragActive = false;
      }
    );
  }

  ngOnDestroy(): void {
    //console.log(this.widgetId);
    this.widgetsConfigService.widgetDestroyedSuccess(this.widgetId);
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }

}
