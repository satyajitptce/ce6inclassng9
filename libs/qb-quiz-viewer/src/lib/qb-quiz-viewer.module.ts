import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QbQuizViewerComponent } from './components/qb-quiz-viewer/qb-quiz-viewer.component';
import { QbQuizStatusBarComponent } from './components/qb-quiz-status-bar/qb-quiz-status-bar.component';
import { QbQuestionCardListModule } from '@tce/qb-question-card-list';
import { CoreModule } from '@tce/core';
import { CommonSelectorModule } from '@app-teacher/features/common-selector/common-selector.module';
import { FormsModule } from '@angular/forms';
// import { CommonSelectorModule } from '@app-teacher/features/common-selector/common-selector.module';
@NgModule({
  imports: [
    CommonModule,
    QbQuestionCardListModule,
    CoreModule,
    CommonSelectorModule,
    FormsModule
  ],
  declarations: [QbQuizViewerComponent, QbQuizStatusBarComponent],
  exports: [QbQuizViewerComponent, QbQuizStatusBarComponent]
})
export class QbQuizViewerModule {}
