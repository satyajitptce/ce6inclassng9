import { async, TestBed } from '@angular/core/testing';
import { QbQuizViewerModule } from './qb-quiz-viewer.module';

describe('QbQuizViewerModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [QbQuizViewerModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(QbQuizViewerModule).toBeDefined();
  });
});
