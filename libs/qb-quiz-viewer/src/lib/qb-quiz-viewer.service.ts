import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RequestApiService } from '@tce/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QbQuizViewerService {
  public createQuiz = new Subject();
  public createQuiz$ = this.createQuiz.asObservable();

  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private toastr: ToastrService,

  ) {}

  createCustomQuiz(quizData,toastrData) {
    return this.http
      .post<any>(this.requestApiService.getUrl('customQuiz'), quizData)
      .subscribe(
        data => {
          this.createQuiz.next('success');
          this.toastr.success(
            'Quiz added successfully in ' +
            toastrData.grade +
              ', ' +
              toastrData.subject +
              ', ' +
              toastrData.chapter +
              ', ' +
              toastrData.topic +
              '!'
          );
          console.log('quizData ', data);
          // this.planModeService.setQuizQuestionList([]);
          // this.planModeService.setSelectQuiz(false);
        },
        err => {
          console.log(err);
        }
      );
  }
}
