import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbQuizStatusBarComponent } from './qb-quiz-status-bar.component';

describe('QbQuizStatusBarComponent', () => {
  let component: QbQuizStatusBarComponent;
  let fixture: ComponentFixture<QbQuizStatusBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QbQuizStatusBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbQuizStatusBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
