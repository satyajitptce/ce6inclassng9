import {
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit
} from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { PlanningModeService } from 'libs/planning-mode/src/lib/services/planning-mode.service';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';

@Component({
  selector: 'tce-qb-quiz-status-bar',
  templateUrl: './qb-quiz-status-bar.component.html',
  styleUrls: ['./qb-quiz-status-bar.component.scss']
})
export class QbQuizStatusBarComponent implements OnInit, OnChanges {
  @Input() quizQuestionList: Array<string>;
  public showQuestions: boolean;
  public questionIndex: number;

  constructor(
    private planModeService: PlanningModeService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit(): void {}

  ngOnChanges() {
    console.log('quizQuestionList ', this.quizQuestionList);
  }

  viewSelectedQuestions() {
    this.showQuestions = !this.showQuestions;
  }

  addCancelQuiz() {
    this.showQuestions = false;
  }

  openFlyout(i) {
    // console.log('index ', i);
    if (this.questionIndex === i) this.questionIndex = null;
    else this.questionIndex = i;
  }

  removeQuestion(question) {
    console.log('quizQuestions event', question);
    this.questionIndex = null;
    let index = this.quizQuestionList.indexOf(question);
    this.quizQuestionList.splice(index, 1);
    this.planModeService.setQuizQuestionList(this.quizQuestionList);
    console.log('quizQuestionList ', this.quizQuestionList);
  }

  // previewQuestion(i) {}

  previewQuestion(questionData) {
    console.log('questionData ', questionData);
    this.questionIndex = null;
    let qData: any;
    let qType: any;

    if (questionData.questionSourceType) {
      if (questionData.mode === 'edit') {
        qType = 'custom'; //changes done by usman. To load custom question in preview mode
        qData = JSON.parse(JSON.stringify(questionData.data));
      } else {
        questionData.questionDetails['questionId'] = questionData.questionId;
        qType = 'TCE'; //changes done by usman. To load custom question in preview mode
        qData = questionData.questionDetails;
      }
    } else {
      qType = 'TCE';
      qData = questionData.questionDetails;
    }
    this.open(false, false, qData, qType);
  }

  protected open(
    closeOnBackdropClick: boolean,
    closeOnEsc: boolean,
    qData,
    qType
  ) {
    this.dialogService
      .open(QuestionEditorContainerComponent, {
        closeOnBackdropClick,
        closeOnEsc,
        context: { questionData: qData, questionType: qType }
      })
      .onClose.subscribe(save => {
        if (save) {
          this.planModeService.qbChangeChapterTopicSet(true);
        }
      });
  }

  // @HostListener('document:click', ['$event'])
  // clickout(event) {
  //   if (event.target.id === 'viewQuestions') {
  //     //console.log('question-more-option---selected');
  //     this.showQuestions = true;
  //   } else {
  //     this.showQuestions = false;
  //   }
  // }
}
