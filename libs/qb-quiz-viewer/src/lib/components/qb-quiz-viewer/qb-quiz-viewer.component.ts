import {
  animate,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnInit,
  Output
} from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import {
  ApiSubject,
  Chapter,
  CurriculumPlaylistService,
  GradeLevel,
  SubjectSelectorService
} from '@tce/core';
import { QuestionbankserviceService } from '@tce/question-bank';
import { QuestionEditorService } from '@tce/template-editor';
import { PlanningModeService } from 'libs/planning-mode/src/lib/services/planning-mode.service';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { QbQuizViewerService } from '../../qb-quiz-viewer.service';

// @Component({
//   selector: 'tce-question-editor-container',
//   templateUrl: './question-editor-container.component.html',
//   styleUrls: ['./question-editor-container.component.scss'],
//   animations: [
//     trigger('slideInOut', [
//       state('in', style({ opacity: 1 })),
//       transition(':enter', [
//         style({ transform: 'translateY(5%)', opacity: 0 }),
//         animate(
//           '300ms ease-in',
//           style({ transform: 'translateY(0%)', opacity: 1 })
//         )
//       ]),
//       transition(':leave', [
//         style({ transform: 'translateY(0%)', opacity: 1 }),
//         animate(
//           '300ms ease-in',
//           style({ transform: 'translateY(5%)', opacity: 0 })
//         )
//       ])
//     ]),
//     trigger('slideInOutX', [
//       state('in', style({ opacity: 1 })),
//       transition(':enter', [
//         style({ transform: 'translateX(5%)', opacity: 0 }),
//         animate(
//           '300ms ease-in',
//           style({ transform: 'translateX(0%)', opacity: 1 })
//         )
//       ]),
//       transition(':leave', [
//         style({ transform: 'translateX(0%)', opacity: 1 }),
//         animate(
//           '300ms ease-in',
//           style({ transform: 'translateX(5%)', opacity: 0 })
//         )
//       ])
//     ])
//   ]
// })
@Component({
  selector: 'tce-qb-quiz-viewer',
  templateUrl: './qb-quiz-viewer.component.html',
  styleUrls: ['./qb-quiz-viewer.component.scss']
})
export class QbQuizViewerComponent implements OnInit {
  @Input() quizQuestionList;
  availableTopics: any;
  selectedChapter;
  selectedTopic;
  availableGrades: any;
  availableSubjects: ApiSubject[] = [];
  availableDivisions: any;
  selectedGradeLevel: any;
  selectedSubject: any;
  selectedGradeSub: string;
  selectedChapterTopic: string;
  availableChapters: any;
  gradeSubjectvalue: string;
  topicChaptervalue: string;
  public quizTitle: any;
  public showCheckBox: boolean;

  public showGradeSubject: boolean = false;
  public showChapterTopic: boolean = false;
  subjectSelectorSubscription: Subscription = new Subscription();
  @Output() addCancelQuiz = new EventEmitter();

  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private questionBankService: QuestionbankserviceService,
    private cdr: ChangeDetectorRef,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private questionEditorService: QuestionEditorService,
    private quizService: QbQuizViewerService,
    private planModeService: PlanningModeService,
    private toastr: ToastrService,
    private dialogService: NbDialogService
  ) {}

  ngOnInit(): void {
    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.availableGrades$.subscribe(
        (gradeLevels: GradeLevel[]) => {
          this.availableGrades = gradeLevels;
          console.log('selectedGrade', this.availableGrades);
        }
      )
    );

    this.planModeService.getSelectQuiz().subscribe(select => {
      this.showCheckBox = select;
      console.log('checkbox', this.showCheckBox);
    });

    this.subjectSelectorSubscription.add(
      this.questionEditorService.getSelectedGrade().subscribe(gradeLevel => {
        this.selectedGradeLevel = gradeLevel;
        //console.log('selectedGrade ', this.selectedGradeLevel);
        this.selectedGradeSubChapTopic();
        if (gradeLevel) {
          this.availableDivisions = gradeLevel['divisions'] || [];
          this.availableSubjects = gradeLevel['subjects'];
        }
        this.cdr.detectChanges();
      })
    );
    this.subjectSelectorSubscription.add(
      this.questionEditorService.getSelectedSubject().subscribe(subject => {
        if (subject) {
          this.selectedSubject = subject;
          //console.log('selectedSub ', this.selectedSubject);
          this.selectedGradeSubChapTopic();
        }
      })
    );

    // this.curriculumPlaylistService.availableChapters$.subscribe(
    //   (chapters: Chapter[]) => {
    //     this.availableChapters = chapters;
    //   }
    // );

    console.log('subjectBookID quiz viewer');

    this.questionEditorService.getAllChapters().subscribe(chapters => {
      this.availableChapters = chapters;
      console.log('chapters in qw', chapters);

      if (this.availableChapters.length > 0) {
        this.selectedChapter = this.availableChapters[0];
        if (this.selectedChapter.topics.length > 0) {
          this.selectedTopic = this.selectedChapter.topics[0];
          this.selectedGradeSubChapTopic();
        }
      }
    });
    this.curriculumPlaylistService.selectedSubjectChapterQuizList$.subscribe(
      (data: any) => {
        console.log('PlanningModeViewComponent -> quiz', data);

        if (data) {
          this.selectedGradeSub =
            data.currentGradeId.title +
            (data.currentGradeId.divisions && data.currentGradeId.divisions[0]
              ? data.currentGradeId.divisions[0].divisionTitle
              : '') +
            '|' +
            data.currentSubjectId.title;
          //console.log('PlanningModeViewComponent -> data', data);
          // this.contenteditorData = data;
          console.log('this.contenteditorData -> data', data);
          this.availableChapters = data.cueerntChapterTopic;
          // this.selectedChapter = data.cueerntChapterTopic[0];
          if (data.cueerntChapterTopic.length > 0) {
            this.selectedChapter = data.cueerntChapterTopic[0];
            this.availableTopics = data.cueerntChapterTopic[0].topics;
            if (this.selectedChapter.topics.length > 0) {
              this.selectedTopic = this.selectedChapter.topics[0];
            }
            this.selectedGradeSubChapTopic();
            console.log(
              'this.contenteditorData -> data if',
              data.cueerntChapterTopic[0],
              this.selectedChapter
            );

            // this.displayvalueGradeSubject(data.cueerntChapterTopic[0]);
          }
        }
      }
    );

    this.questionEditorService.getSelectedChapter().subscribe(chapter => {
      if (chapter) {
        this.selectedChapter = chapter;
        // console.log('selectedChap ', this.selectedChapter);

        this.selectedGradeSubChapTopic();
        this.availableTopics = chapter['topics'];
      }
      // console.log('chapter1111', chapter);
    });

    this.questionEditorService.getSelectedTopic().subscribe(topic => {
      // console.log('topic1111', topic);
      this.selectedTopic = topic;
      // console.log('selectedTopic ', this.selectedTopic);

      this.selectedGradeSubChapTopic();
    });
  }

  showOpenGrade(bool) {
    if (this.showGradeSubject) this.showGradeSubject = false;
    else this.showGradeSubject = bool;
    // this.showChapterTopic = topic;
    if (this.showChapterTopic) this.showOpenSubject(false);
  }

  showOpenSubject(bool) {
    // this.showGradeSubject = bool;
    this.showChapterTopic = !this.showChapterTopic;
    if (this.showGradeSubject) this.showOpenGrade(false);
  }

  selectedGradeSubChapTopic() {
    console.log('selectedGradeSub ', this.selectedChapter, this.selectedTopic);

    if (this.selectedGradeLevel && this.selectedSubject) {
      this.selectedGradeSub =
        this.selectedGradeLevel.title + ' | ' + this.selectedSubject.title;
    }
    if (this.selectedChapter && this.selectedTopic) {
      let topic = '';
      if (this.selectedTopic === 'none') {
        topic = 'no topic selected';
      } else {
        topic =
          this.selectedTopic.topicNumber + '. ' + this.selectedTopic.topicTitle;
      }
      this.selectedChapterTopic =
        this.selectedChapter.chapterTitle + ' | ' + topic;
    } else {
      this.selectedChapterTopic = 'No chapter, topic selected';
    }
  }

  displayvalueGradeSubject(subjectGrade) {
    // console.log('subjectGrade', subjectGrade, this.showGradeSubject);

    this.selectedChapter = null;
    this.selectedTopic = null;
    // this.curriculumPlaylistService.setselectedSubjectChapter(subjectGrade);
    this.curriculumPlaylistService.getChaptersTopicsQuizEditor(subjectGrade);
    this.selectedGradeLevel = subjectGrade.gradeLevel;
    this.selectedSubject = subjectGrade.subject;
    this.gradeSubjectvalue =
      subjectGrade.gradeLevel.title + ' | ' + subjectGrade.subject.title;
    this.showGradeSubject = false;
    console.log('subjectGrade', subjectGrade, this.showGradeSubject);

    this.cdr.detectChanges();
    this.selectedGradeSubChapTopic();
  }

  displayvalueChapterTopic(ChapterTopic) {
    this.selectedChapter = ChapterTopic.chapter;
    this.availableTopics = ChapterTopic.chapter.topics;
    this.selectedTopic = ChapterTopic.topic;
    this.topicChaptervalue =
      ChapterTopic.chapter.chapterNumber +
      '.' +
      ChapterTopic.topic.topicNumber +
      ' | ' +
      ChapterTopic.topic.topicTitle;
    this.showChapterTopic = false;
    this.cdr.detectChanges();
    this.selectedGradeSubChapTopic();
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    // console.log('clickout viewer', event);
    // if (event.target.id === 'grade-selector') {
    //   // if (event.target.id === 'gradeSubject') {
    //   //   //console.log('question-more-option---selected');
    //   this.showGradeSubject = true;
    // }
    // else {
    //   this.showGradeSubject = false;
    // }
    // if (event.target.id === 'chapterTopic') {
    //   //console.log('question-more-option---selected');
    //   this.showChapterTopic = true;
    // } else {
    //   this.showChapterTopic = false;
    // }
  }

  addQuiz() {
    console.log(
      'detaiils ',
      this.selectedGradeLevel,
      this.selectedSubject,
      this.selectedChapter,
      this.selectedTopic
    );
    if (this.quizQuestionList.length > 0) {
      let qIds: Array<string> = [];
      this.quizQuestionList.forEach(question => {
        qIds.push(question.questionId);
      });
      if (
        this.quizTitle &&
        this.selectedGradeLevel &&
        this.selectedSubject &&
        this.selectedChapter.chapterId &&
        this.selectedTopic.topicId
      ) {
        let quizData = {
          title: this.quizTitle,
          gradeId: this.selectedGradeLevel.id,
          subjectId: this.selectedSubject.subjectId,
          chapterId: this.selectedChapter.chapterId,
          tpId: this.selectedTopic.topicId,
          data: JSON.stringify(qIds)
        };
        let toastrData = {
          grade: this.selectedGradeLevel.title,
          subject:this.selectedSubject.title,
          chapter:this.selectedChapter.chapterTitle,
          topic:this.selectedTopic.topicTitle 
        }
        console.log('quizData ', quizData,toastrData);

        this.quizService.createCustomQuiz(quizData,toastrData);
        this.quizService.createQuiz$.subscribe(data => {
          console.log('createQuiz', data);
          if (data === 'success') {
            this.planModeService.setQuizQuestionList([]);
            this.planModeService.setSelectQuiz(false);
           
          }
        });
        // console.log('createQuiz', createQuiz);
        // this.planModeService.setQuizQuestionList([]);
        // this.planModeService.setSelectQuiz(false);
        this.cancel();
      } else {
        this.toastr.error('please fill all details');
      }
    } else {
      this.toastr.error('Please add atleast one question');
    }
    // console.log('quizData ', quizData);
  }

  cancel() {
    this.addCancelQuiz.emit();
  }

  openPopup(questionData) {
    //console.log('questionData', questionData);
    let qData: any;
    let qType: any;

    if (questionData.data) {
      if (questionData.data.questionSourceType) {
        if (questionData.mode === 'edit') {
          qType = 'custom'; //changes done by usman. To load custom question in preview mode
          qData = JSON.parse(JSON.stringify(questionData.data));
        } else {
          questionData.data.questionDetails['questionId'] =
            questionData.data.questionId;
          qType = 'TCE'; //changes done by usman. To load custom question in preview mode
          qData = questionData.data.questionDetails;
        }
      } else {
        qType = 'TCE';
        qData = questionData.data.questionDetails;
      }
      this.open(false, false, qData, qType);
    } else {
      qType = 'custom';
      this.open(false, false, qData, qType);
    }
    // this.showEdit = !this.showEdit;
  }

  protected open(
    closeOnBackdropClick: boolean,
    closeOnEsc: boolean,
    qData,
    qType
  ) {
    this.dialogService
      .open(QuestionEditorContainerComponent, {
        closeOnBackdropClick,
        closeOnEsc,
        context: { questionData: qData, questionType: qType }
      })
      .onClose.subscribe(save => {
        if (save) {
          this.planModeService.qbChangeChapterTopicSet(true);
        }
      });
  }

  // deleteQuestion(questionId) {
  //   this.questionBankService.deleteQuestion(questionId);
  //   this.questionBankService.deleteQuestionBroadcast$.subscribe(data => {
  //     console.log('delete Question---------->', data);
  //     if (data && data['status'] === 'success') {
  //       this.toastr.success('Question deleted successfully!');
  //       this.planModeService.qbChangeChapterTopicSet(true);
  //     }
  //   });
  // }
}
