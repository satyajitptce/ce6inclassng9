import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbQuizViewerComponent } from './qb-quiz-viewer.component';

describe('QbQuizViewerComponent', () => {
  let component: QbQuizViewerComponent;
  let fixture: ComponentFixture<QbQuizViewerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QbQuizViewerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbQuizViewerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
