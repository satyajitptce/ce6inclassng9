import { TestBed } from '@angular/core/testing';

import { QbQuizViewerService } from './qb-quiz-viewer.service';

describe('QbQuizViewerService', () => {
  let service: QbQuizViewerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QbQuizViewerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
