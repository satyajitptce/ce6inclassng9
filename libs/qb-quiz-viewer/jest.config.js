module.exports = {
  name: 'qb-quiz-viewer',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/qb-quiz-viewer',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
