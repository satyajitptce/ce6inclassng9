module.exports = {
  name: 'template-editor',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/template-editor',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
