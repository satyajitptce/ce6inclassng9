import { async, TestBed } from '@angular/core/testing';
import { TemplateEditorModule } from './template-editor.module';

describe('TemplateEditorModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [TemplateEditorModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(TemplateEditorModule).toBeDefined();
  });
});
