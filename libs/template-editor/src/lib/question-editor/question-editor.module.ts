import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { QuestionEditorViewComponent } from './component/question-editor-view/question-editor-view.component';
import { QuestionEditorContainerComponent } from './component/question-editor-container/question-editor-container.component';
import { QuestionEditorControllerComponent } from './component/question-editor-controller/question-editor-controller.component';
import { QuestionEditorPreviewComponent } from './component/question-editor-preview/question-editor-preview.component';
import { QbTemplatePreviewModule } from '@tce/qb-template-preview';
import {
  NbCardModule,
  NbDialogModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbLayoutModule,
  NbSidebarModule,
  NbInputModule
} from '@nebular/theme';
import { CommonSelectorModule } from '../../../../../apps/tce-teacher/src/app/features/common-selector/common-selector.module';
@NgModule({
  declarations: [
    QuestionEditorViewComponent,
    QuestionEditorContainerComponent,
    QuestionEditorControllerComponent,
    QuestionEditorPreviewComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    NbCardModule,
    NbDialogModule.forChild(),
    NbButtonModule,
    NbSelectModule,
    NbIconModule,
    NbLayoutModule,
    NbSidebarModule,
    NbInputModule,
    QbTemplatePreviewModule,
    CommonSelectorModule
  ],

  exports: [
    QuestionEditorViewComponent,
    QuestionEditorContainerComponent,
    QuestionEditorControllerComponent,
    QuestionEditorPreviewComponent
  ],
  entryComponents: [
    QuestionEditorViewComponent,
    QuestionEditorContainerComponent,
    QuestionEditorControllerComponent,
    QuestionEditorPreviewComponent
  ]
})
export class QuestionEditorModule {}
