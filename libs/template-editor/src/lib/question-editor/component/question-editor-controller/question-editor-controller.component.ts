import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { BehaviorSubject, from, Subject, Subscription } from 'rxjs';
import { QuestionbankserviceService } from 'libs/question-bank/src/lib/services/questionbankservice.service';
import { QuestionEditorPreviewComponent } from '../question-editor-preview/question-editor-preview.component';
import {
  ApiDivision,
  ApiSubject,
  AppConfigService,
  Chapter,
  CurriculumPlaylistService,
  FullClassSelection,
  FullContentSelection,
  GradeLevel,
  SubjectSelectorService
} from '@tce/core';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';

@Component({
  selector: 'tce-question-editor-controller',
  templateUrl: './question-editor-controller.component.html',
  styleUrls: ['./question-editor-controller.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ]),
    trigger('slideInOutX', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateX(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class QuestionEditorControllerComponent implements OnInit, OnChanges {
  questionList: any;
  selectedQuestionType: any;
  @Input() questionType: string;
  @Input() editMode: boolean;
  @Output() selectedTemplate = new EventEmitter();
  @Output() openGradeSubChapterTopic = new EventEmitter();
  @Input() selectedGradeSub: any;
  @Input() selectedChapterTopic: any;
  public preview: BehaviorSubject<boolean>;
  public questionTypeList = [
    {
      id: 11,
      title: 'Multiple Choice - Standard',
      path: 'SCQ'
    },
    {
      id: 12,
      title: 'Multiple Choice - Multiple response',
      path: 'MCQ'
    },
    {
      id: 13,
      title: 'Multiple Choice - True or false',
      path: 'mcq-tf'
    },
    {
      id: 36,
      title: 'Open Ended Question',
      path: 'plain-text'
    }
  ];
  public questionTypeDropdown: boolean;

  constructor(
    private questionBankService: QuestionbankserviceService,
    private dialogService: NbDialogService,
    private appConfigService: AppConfigService,
    private subjectSelectorService: SubjectSelectorService,
    private cdr: ChangeDetectorRef,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {
    this.questionBankService.getQuestionType().subscribe(data => {
      console.log('datadata ', data);

      this.questionList = data.qbQuestionType;
      if (this.questionList)
        this.questionTypeList.forEach(question => {
          if (question.path === this.questionType) {
            this.selectedQuestionType = question;
          }
        });
      // this.selectedQuestionType = this.questionType;
    });
  }

  ngOnChanges(changes: SimpleChanges): void {}

  ngOnInit(): void {
    console.log('allallllalll ', this.questionType, this.selectedQuestionType);
  }

  selectedQuestionTypeEvent(data: any) {
    console.log('selected type ', data);

    this.selectedQuestionType = data;
    this.selectedTemplate.emit(data.path);
  }

  protected open(
    closeOnBackdropClick: boolean,
    closeOnEsc: boolean,
    templateData: any
  ) {
    this.dialogService.open(QuestionEditorPreviewComponent, {
      context: { templateData: templateData },
      closeOnBackdropClick,
      closeOnEsc
    });
  }

  showOpengradeSubject(grade, topic) {
    let openGradeTopic = {
      grade: grade,
      topic: topic
    };
    this.openGradeSubChapterTopic.emit(openGradeTopic);
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (!this.editMode) {
      if (event.target.id === 'questionTypeId') {
        this.questionTypeDropdown = true;
      } else {
        this.questionTypeDropdown = false;
      }
    }
  }
}
