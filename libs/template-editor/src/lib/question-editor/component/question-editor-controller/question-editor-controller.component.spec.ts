import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionEditorControllerComponent } from './question-editor-controller.component';

describe('QuestionEditorControllerComponent', () => {
  let component: QuestionEditorControllerComponent;
  let fixture: ComponentFixture<QuestionEditorControllerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionEditorControllerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionEditorControllerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
