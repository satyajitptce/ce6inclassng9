import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionEditorPreviewComponent } from './question-editor-preview.component';

describe('QuestionEditorPreviewComponent', () => {
  let component: QuestionEditorPreviewComponent;
  let fixture: ComponentFixture<QuestionEditorPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionEditorPreviewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionEditorPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
