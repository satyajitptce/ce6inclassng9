import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
@Component({
  selector: 'tce-question-editor-preview',
  templateUrl: './question-editor-preview.component.html',
  styleUrls: ['./question-editor-preview.component.scss']
})
export class QuestionEditorPreviewComponent implements OnInit {
  @Input() templateData: any;
  @Input() questionType: any;
  @Output() public dismissDialog = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
    //console.log('templateData in preview', this.templateData);
  }

  dismiss() {
    this.dismissDialog.emit();
  }
}
