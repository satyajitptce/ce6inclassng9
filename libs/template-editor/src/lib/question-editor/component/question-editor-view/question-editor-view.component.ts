import {
  Component,
  Inject,
  Injector,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ElementRef,
  NgModuleFactory,
  NgModuleRef,
  ComponentFactory,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  ComponentFactoryResolver,
  AfterViewInit,
  OnChanges
} from '@angular/core';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { Location } from '@angular/common';
import { QuestionbankserviceService } from 'libs/question-bank/src/lib/services/questionbankservice.service';
import { McqSingleSelectLayoutComponent } from '@tce/quiz-templates';
import { FibTextLayoutComponent } from '@tce/quiz-templates';
import { HotspotLayoutComponent } from '@tce/quiz-templates';
import { EssayTextLayoutComponent } from '@tce/quiz-templates';
import { OrderListComponent } from '@tce/quiz-templates';
import { FibImageDragDropLayoutComponent } from '@tce/quiz-templates';
import { TokenHighlightLayoutComponent } from '@tce/quiz-templates';
import { MatchListComponent } from '@tce/quiz-templates';
import { QuestionEditorService } from '../../services/question-editor.service';
@Component({
  selector: 'tce-question-editor-view',
  templateUrl: './question-editor-view.component.html',
  styleUrls: ['./question-editor-view.component.scss']
})
export class QuestionEditorViewComponent implements OnInit, OnChanges {
  @ViewChild('targetRefView', { read: ViewContainerRef, static: true })
  viewRef: ViewContainerRef;
  questionData: any;
  @Input() templateData: any;
  @Input() templateComponent: any;
  public previewState: boolean = false;
  private showAnsState: boolean = false;
  public globalPreviewState: boolean = false;
  private submit: Subject<void> = new Subject<void>();
  private save: Subject<void> = new Subject<void>();
  private metadata: Subject<void> = new Subject<void>();
  private viewDevice: Subject<void> = new Subject<void>();
  private layoutView: Subject<void> = new Subject<void>();
  private preview: Subscription;
  private componentHashmap = {
    mcq: McqSingleSelectLayoutComponent,
    MCQ: McqSingleSelectLayoutComponent,
    SCQ: McqSingleSelectLayoutComponent,
    'mcq-tf': McqSingleSelectLayoutComponent,
    fib: FibTextLayoutComponent,
    hotspot: HotspotLayoutComponent,
    essay: EssayTextLayoutComponent,
    classify: OrderListComponent,
    fibImage: FibImageDragDropLayoutComponent,
    token: TokenHighlightLayoutComponent,
    classifyMatchList: MatchListComponent
  };

  @ViewChild(MatchListComponent)
  public matchListComponent: MatchListComponent;

  constructor(
    public questionBankService: QuestionbankserviceService,
    private componentFactoryResolver: ComponentFactoryResolver,
    private questionEditorService: QuestionEditorService
  ) {}

  ngOnInit(): void {
    console.log('globalState ', this.globalPreviewState, this.templateData);
    this.questionEditorService.updatePreviewState(false);
    this.loadTemplate(this.templateData);
  }

  ngOnChanges(changes): void {
    this.loadTemplate(this.templateData);
  }

  loadTemplate(templateData) {
    //console.log('select Temp ', this.templateData, this.templateComponent);
    // this.vcRef = (this.currentAdIndex + 1) % this.ads.length;
    // const adItem = this.ads[this.currentAdIndex];
    let componentTemplate: any;
    if (templateData.data.type) {
      componentTemplate = this.componentHashmap[templateData.data.type];
    } else {
      componentTemplate = this.componentHashmap[templateData.type];
    }
    this.viewRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      componentTemplate
    );

    const componentRef = this.viewRef.createComponent<any>(componentFactory);

    componentRef.instance.templateData = templateData;
    componentRef.instance.previewState = this.previewState;
    componentRef.instance.showAnsStateFlag = this.showAnsState;
    componentRef.instance.submit = this.submit;
    componentRef.instance.save = this.save;
    componentRef.instance.metadataSidebar = this.metadata;
    componentRef.instance.viewDevice = this.viewDevice;
    componentRef.instance.layoutView = this.layoutView;
  }
}
