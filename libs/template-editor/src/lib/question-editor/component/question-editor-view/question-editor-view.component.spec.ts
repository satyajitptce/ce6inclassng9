import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionEditorViewComponent } from './question-editor-view.component';

describe('QuestionEditorViewComponent', () => {
  let component: QuestionEditorViewComponent;
  let fixture: ComponentFixture<QuestionEditorViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionEditorViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionEditorViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
