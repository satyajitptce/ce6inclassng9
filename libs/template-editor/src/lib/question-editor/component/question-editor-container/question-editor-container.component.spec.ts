import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionEditorContainerComponent } from './question-editor-container.component';

describe('QuestionEditorContainerComponent', () => {
  let component: QuestionEditorContainerComponent;
  let fixture: ComponentFixture<QuestionEditorContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QuestionEditorContainerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionEditorContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
