import {
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnInit
} from '@angular/core';
import { NbDialogRef, NbDialogService, NbSidebarService } from '@nebular/theme';
import { QuestionEditorViewComponent } from '../question-editor-view/question-editor-view.component';
import { QuestionEditorPreviewComponent } from '../question-editor-preview/question-editor-preview.component';
import { QuestionbankserviceService } from 'libs/question-bank/src/lib/services/questionbankservice.service';
import { BehaviorSubject, Subscription } from 'rxjs';
import { QuestionEditorService } from '../../services/question-editor.service';
import { ToastrService } from 'ngx-toastr';
import { UtilityConfigService } from '@tce/question-bank';
import {
  ApiDivision,
  ApiSubject,
  AppConfigService,
  Chapter,
  CurriculumPlaylistService,
  FullClassSelection,
  FullContentSelection,
  GradeLevel,
  RequestApiService,
  SubjectSelectorService
} from '@tce/core';
import {
  trigger,
  transition,
  animate,
  style,
  state
} from '@angular/animations';
import { SharedComponentService } from '@tce/quiz-templates';

@Component({
  selector: 'tce-question-editor-container',
  templateUrl: './question-editor-container.component.html',
  styleUrls: ['./question-editor-container.component.scss'],
  animations: [
    trigger('slideInOut', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateY(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateY(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateY(5%)', opacity: 0 })
        )
      ])
    ]),
    trigger('slideInOutX', [
      state('in', style({ opacity: 1 })),
      transition(':enter', [
        style({ transform: 'translateX(5%)', opacity: 0 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(0%)', opacity: 1 })
        )
      ]),
      transition(':leave', [
        style({ transform: 'translateX(0%)', opacity: 1 }),
        animate(
          '300ms ease-in',
          style({ transform: 'translateX(5%)', opacity: 0 })
        )
      ])
    ])
  ]
})
export class QuestionEditorContainerComponent implements OnInit {
  public getEditQuestion: any;
  public selectedTemplate: string;
  public templateComponent: string;
  public showcontroller: boolean = false;
  public showPreview: boolean = false;
  public globalPreviewState: boolean = false;
  public editMode: boolean = false;
  @Input() questionData: any;
  @Input() questionType: any;
  public questionTemplateData: any;
  public xmlQuestion: boolean = false;
  public templates = [
    {
      reference: 'c3805d10-0cd7-4057-a85c',
      data: {
        options: [
          {
            label: '',
            value: '0',
            feedbackInline: '',
            image: ''
          }
        ],
        stimulus_audio: '',
        penalty_score: 0,
        _comment: '',
        response_id: '',
        feedback_attempts: 1,
        instant_feedback: false,
        multiple_responses: false,
        stimulus: {
          label: '',
          value: '0',
          feedbackInline: '',
          imgMode: 'medium',
          image: ''
        },
        type: 'mcq',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          },
          penalty: 0
        },
        ui_style: {
          type: 'vertical',
          theme: 'light',
          _comment: ''
        },
        metadata: {
          name: 'Multiple choice – standard',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        },
        media: {
          src: ''
        }
      },
      type: 'SCQ',
      name: 'Multiple Choice - single select',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e2',
      data: {
        options: [
          {
            label: '',
            value: '0',
            feedbackInline: '',
            image: ''
          }
        ],
        stimulus: {
          label: '',
          value: '0',
          feedbackInline: '',
          imgMode: 'medium'
        },
        stimulus_audio: '',
        penalty_score: 0,
        _comment: '',
        response_id: '',
        feedback_attempts: 1,
        instant_feedback: false,
        multiple_responses: false,
        type: 'mcq',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          },
          penalty: 0
        },
        ui_style: {
          type: 'vertical',
          theme: 'light',
          _comment: ''
        },
        metadata: {
          name: 'Multiple choice – standard',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        },
        media: {
          src: ''
        }
      },
      type: 'MCQ',
      name: 'Multiple Choice - multiple select',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e4',
      data: {
        options: [
          {
            label: 'True',
            value: '0',
            feedbackInline: ''
          },
          {
            label: 'False',
            value: '1',
            feedbackInline: ''
          }
        ],
        stimulus: {
          label: '',
          value: '0',
          feedbackInline: '',
          placeholder: 'Compose The Question...',
          imgMode: 'medium',
          image: ''
        },
        stimulus_audio: '',
        penalty_score: 0,
        _comment: '',
        response_id: '',
        feedback_attempts: 1,
        instant_feedback: false,
        multiple_responses: false,
        type: 'mcq',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          },
          penalty: 0
        },
        ui_style: {
          type: 'horizontal',
          theme: 'light',
          _comment: ''
        },
        metadata: {
          name: 'Multiple choice – standard',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        }
      },
      type: 'mcq-tf',
      name: 'Multiple choice - true or false',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e1',
      data: {
        stimulus: {
          label: '',
          value: '0',
          placeholder: 'Compose The Question...'
        },
        template: '',
        type: 'fib',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          }
        },
        ui_style: {
          type: 'horizontal'
        }
      },
      metadata: {
        name: 'Fill in the blanks – standard',
        template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163'
      },
      type: 'fib-text',
      name: 'FIB - text',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e1',
      data: {
        stimulus: {
          label: '',
          value: '0',
          placeholder: 'Compose The Question...'
        },
        type: 'hotspot',
        image: {
          src:
            '//assets.learnosity.com/organisations/1/bead7655-fb71-41af-aeea-9e08a47eac68.png',
          width: 700,
          height: 370
        },
        areas: [
          [
            {
              x: 167,
              y: 22
            },
            {
              x: 132,
              y: 24
            },
            {
              x: 118,
              y: 32
            },
            {
              x: 61,
              y: 33
            },
            {
              x: 37,
              y: 38
            },
            {
              x: 20,
              y: 59
            },
            {
              x: 14,
              y: 76
            },
            {
              x: 33,
              y: 85
            },
            {
              x: 58,
              y: 71
            },
            {
              x: 71,
              y: 70
            },
            {
              x: 67,
              y: 82
            },
            {
              x: 60,
              y: 100
            },
            {
              x: 56,
              y: 126
            },
            {
              x: 66,
              y: 147
            },
            {
              x: 79,
              y: 165
            },
            {
              x: 117,
              y: 184
            },
            {
              x: 138,
              y: 200
            },
            {
              x: 130,
              y: 218
            },
            {
              x: 141,
              y: 248
            },
            {
              x: 161,
              y: 264
            },
            {
              x: 160,
              y: 294
            },
            {
              x: 163,
              y: 318
            },
            {
              x: 177,
              y: 342
            },
            {
              x: 207,
              y: 358
            },
            {
              x: 217,
              y: 348
            },
            {
              x: 203,
              y: 337
            },
            {
              x: 198,
              y: 317
            },
            {
              x: 213,
              y: 303
            },
            {
              x: 229,
              y: 277
            },
            {
              x: 247,
              y: 263
            },
            {
              x: 251,
              y: 245
            },
            {
              x: 257,
              y: 225
            },
            {
              x: 238,
              y: 208
            },
            {
              x: 214,
              y: 193
            },
            {
              x: 195,
              y: 181
            },
            {
              x: 177,
              y: 173
            },
            {
              x: 151,
              y: 170
            },
            {
              x: 194,
              y: 164
            },
            {
              x: 182,
              y: 155
            },
            {
              x: 163,
              y: 149
            },
            {
              x: 158,
              y: 132
            },
            {
              x: 188,
              y: 104
            },
            {
              x: 210,
              y: 102
            },
            {
              x: 228,
              y: 95
            },
            {
              x: 226,
              y: 80
            },
            {
              x: 220,
              y: 61
            },
            {
              x: 227,
              y: 52
            },
            {
              x: 242,
              y: 64
            },
            {
              x: 263,
              y: 66
            },
            {
              x: 285,
              y: 62
            },
            {
              x: 312,
              y: 57
            },
            {
              x: 319,
              y: 44
            },
            {
              x: 303,
              y: 38
            },
            {
              x: 324,
              y: 26
            },
            {
              x: 325,
              y: 13
            },
            {
              x: 309,
              y: 11
            },
            {
              x: 278,
              y: 11
            },
            {
              x: 256,
              y: 9
            },
            {
              x: 225,
              y: 9
            },
            {
              x: 196,
              y: 13
            }
          ],
          [
            {
              x: 425,
              y: 278
            },
            {
              x: 440,
              y: 277
            },
            {
              x: 445,
              y: 260
            },
            {
              x: 450,
              y: 238
            },
            {
              x: 436,
              y: 234
            },
            {
              x: 417,
              y: 259
            }
          ],
          [
            {
              x: 614,
              y: 238
            },
            {
              x: 598,
              y: 239
            },
            {
              x: 571,
              y: 259
            },
            {
              x: 556,
              y: 270
            },
            {
              x: 555,
              y: 303
            },
            {
              x: 590,
              y: 303
            },
            {
              x: 609,
              y: 302
            },
            {
              x: 621,
              y: 315
            },
            {
              x: 646,
              y: 311
            },
            {
              x: 658,
              y: 297
            },
            {
              x: 665,
              y: 278
            },
            {
              x: 652,
              y: 252
            },
            {
              x: 638,
              y: 233
            },
            {
              x: 630,
              y: 245
            }
          ],
          [
            {
              x: 648,
              y: 321
            },
            {
              x: 674,
              y: 309
            },
            {
              x: 684,
              y: 291
            },
            {
              x: 698,
              y: 297
            },
            {
              x: 697,
              y: 312
            },
            {
              x: 658,
              y: 335
            }
          ]
        ],
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          }
        },
        ui_style: {
          type: 'horizontal'
        }
      },
      metadata: {
        name: 'Hotspot',
        template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163'
      },
      type: 'highlight',
      name: 'Hotspot',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e1',
      data: {
        type: 'essay',
        metadata: {
          name: 'Essay with Plain Text',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        },
        stimulus: {
          label: '',
          value: '0',
          feedbackInline: ''
        }
      },
      show_copy: true,
      show_cut: true,
      show_paste: true,
      show_word_limit: 'visible',
      max_length: '10000',
      showSampleAnswer: false,
      spellcheck: true,
      type: 'plain-text',
      name: 'Plain text',
      sample_answer: ''
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e5',
      data: {
        options: [
          {
            label: '',
            value: '0',
            feedbackInline: ''
          }
        ],
        stimulus: {
          label: '',
          value: '0',
          placeholder: 'Compose The Question...'
        },
        stimulus_audio: '',
        penalty_score: 0,
        _comment: '',
        response_id: '',
        feedback_attempts: 1,
        instant_feedback: false,
        multiple_responses: false,
        type: 'classify',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          },
          penalty: 0
        },
        ui_style: {
          type: 'horizontal',
          theme: 'light',
          _comment: ''
        },
        metadata: {
          name: 'Order-list',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        }
      },
      type: 'order-list',
      name: 'Sequencing Question',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e613',
      data: {
        options: [
          {
            label: '',
            value: '0',
            feedbackInline: ''
          }
        ],
        image: {
          src: 'https://assets.learnosity.com/demos/docs/colored_world_map.png',
          width: 600,
          height: 600
        },
        stimulus: {
          label: '',
          value: '0',
          placeholder: 'Compose The Question...'
        },
        possible_responses: [
          {
            x: 11.56,
            y: 24.33,
            width: 72,
            height: 66,
            id: 'abc1'
          },
          {
            x: 22.57,
            y: 44.81,
            width: 178,
            height: 73,
            id: 'abc2'
          },
          {
            x: 39.63,
            y: 67.66,
            width: 70,
            height: 73,
            id: 'abc3'
          }
        ],
        type: 'fibImage',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: [null, null, null]
          }
        },
        ui_style: {
          type: 'horizontal'
        }
      },
      metadata: {
        name: 'Fill in the blanks – Image Text',
        template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9166',
        subjects: []
      },
      type: 'fib-image-text',
      name: 'FIB - image text',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e1',
      data: {
        stimulus: {
          label: '',
          value: '0',
          feedbackInline: '',
          placeholder: 'Compose The Question...'
        },
        type: 'token',
        template:
          'Risus et tincidunt turpis <strong>facilisis</strong>.\n\nCurabitur eu nulla justo.Curabitur vulputate ut nisl et bibendum. Nunc diam enim, porta sed eros vitae. dignissim, et tincidunt turpis facilisis.\n\nCurabitur eu nulla justo. Curabitur vulputate ut nisl et bibendum.\n',
        templateText: '',
        tokens:
          'Risus et tincidunt turpis <strong>facilisis</strong>.\n\nCurabitur eu nulla justo.Curabitur vulputate ut nisl et bibendum. Nunc diam enim, porta sed eros vitae. dignissim, et tincidunt turpis facilisis.\n\nCurabitur eu nulla justo. Curabitur vulputate ut nisl et bibendum.\n',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          }
        },
        ui_style: {
          type: 'horizontal'
        }
      },
      metadata: {
        name: 'Token-Highlight',
        template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163'
      },
      type: 'token-highlight',
      name: 'Token',
      widget_type: 'response'
    },
    {
      reference: 'c3805d10-0cd7-4057-a85c-4061ef2cc29e5',
      data: {
        options: [
          {
            label: '',
            value: '0',
            feedbackInline: ''
          }
          // {
          //   label: '[Choice Q]',
          //   value: '1',
          //   feedbackInline: ''
          // },
          // {
          //   label: '[Choice R]',
          //   value: '2',
          //   feedbackInline: ''
          // },
          // {
          //   label: '[Choice S]',
          //   value: '3',
          //   feedbackInline: ''
          // }
        ],
        matches: [],
        stems: [
          {
            text: '',
            value: '0'
          }
          // {
          //   text: '<p>[This is the stem.]</p>',
          //   value: '1'
          // },
          // {
          //   text: '<p>[This is the stem.]</p>',
          //   value: '2'
          // },
          // {
          //   text: '<p>[This is the stem.]</p>',
          //   value: '3'
          // }
        ],
        stimulus: {
          label: '',
          value: '0',
          placeholder: 'Compose The Question...'
        },
        stimulus_audio: '',
        penalty_score: 0,
        _comment: '',
        response_id: '',
        feedback_attempts: 1,
        instant_feedback: false,
        multiple_responses: false,
        type: 'classifyMatchList',
        validation: {
          scoring_type: 'exactMatch',
          valid_response: {
            score: 1,
            value: []
          },
          penalty: 0
        },
        ui_style: {
          type: 'horizontal',
          theme: 'light',
          _comment: ''
        },
        metadata: {
          name: 'Match-list',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        }
      },
      type: 'match-list',
      name: 'Match list',
      widget_type: 'response'
    }
  ];
  public templateData: Object;
  showOpengradeSubject: boolean = false;
  showOpenChapterTopic: boolean = false;
  animationFlag: any;
  availableGrades: any;
  availableSubjects: ApiSubject[] = [];
  selectedGradeLevel: any;
  selectedSubject: any;
  availableDivisions: ApiDivision[] = [];
  subjectSelectorSubscription: Subscription = new Subscription();
  availableChapters: any;
  availableTopics: any;
  selectedChapter: any;
  selectedTopic: any;
  myChapterUpload: any;
  topicChaptervalue: any;
  mySubjectUpload: any;
  gradeSubjectvalue: any;
  customQuestion: boolean;
  selectedGradeSub: string;
  selectedChapterTopic: string;
  uploadImgName: string;
  uploadImgFolder: string;
  deleteImage: boolean;

  constructor(
    private sidebarService: NbSidebarService,
    private questionBankService: QuestionbankserviceService,
    protected ref: NbDialogRef<QuestionEditorContainerComponent>,
    private dialogService: NbDialogService,
    private questionEditorService: QuestionEditorService,
    private toastr: ToastrService,
    private utilityConfigService: UtilityConfigService,
    private appConfigService: AppConfigService,
    private subjectSelectorService: SubjectSelectorService,
    private cdr: ChangeDetectorRef,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private sharedComponentService: SharedComponentService,
    private requestApiService: RequestApiService
  ) {}

  ngOnInit(): void {
    console.log('this.questionData', this.questionData, this.questionType);
    if (this.questionType !== 'TCE') {
      this.customQuestion = true;
      if (this.questionData) {
        let url = this.requestApiService.getUrl('getFile');
        let templateType = this.questionData.questionDetails.data.questions[0]
          .data.type;
        this.questionData.questionDetails['fileUrl'] = url;
        this.questionData.questionDetails[
          'encryptedPath'
        ] = this.questionData.encryptedPath;
        this.questionData.questionDetails[
          'questionId'
        ] = this.questionData.questionId;
        if (
          templateType === 'MCQ' ||
          templateType === 'SCQ' ||
          templateType === 'mcq-tf'
        ) {
          this.questionTemplateData = this.utilityConfigService.studyToSchemaMCQMapper(
            this.questionData.questionDetails
          );
        } else if (templateType === 'OPENENDEDSTEMONLY') {
          this.questionTemplateData = this.utilityConfigService.studiToOpenEndedMapper(
            this.questionData.questionDetails
          );
          console.log('this.questionData if', this.questionTemplateData);
        }
        this.editMode = true;
        let qbIdType = this.questionData.questionId.substring(0, 4);
        if (qbIdType === 'teqb') {
          this.xmlQuestion = true;
        }
      }
      this.getTemplatesData();
    } else {
      this.customQuestion = false;
    }
    this.animationFlag = this.appConfigService.getConfig('global_setting')[
      'animation'
    ];

    this.subjectSelectorSubscription.add(
      this.subjectSelectorService.availableGrades$.subscribe(
        (gradeLevels: GradeLevel[]) => {
          this.availableGrades = gradeLevels;
        }
      )
    );
    this.subjectSelectorSubscription.add(
      this.questionEditorService.getSelectedGrade().subscribe(gradeLevel => {
        this.selectedGradeLevel = gradeLevel;
        //console.log('selectedGrade ', this.selectedGradeLevel);
        this.selectedGradeSubChapTopic();
        if (gradeLevel) {
          this.availableDivisions = gradeLevel['divisions'] || [];
          this.availableSubjects = gradeLevel['subjects'];
        }
        this.cdr.detectChanges();
      })
    );

    this.subjectSelectorSubscription.add(
      this.questionEditorService.getSelectedSubject().subscribe(subject => {
        if (subject) {
          this.selectedSubject = subject;
          //console.log('selectedSub ', this.selectedSubject);

          this.selectedGradeSubChapTopic();
        }
      })
    );

    // this.curriculumPlaylistService.availableChapters$.subscribe(
    //   (chapters: Chapter[]) => {
    //     console.log('availChap', chapters);

    //     this.availableChapters = chapters;
    //   }
    // );

    // this.curriculumPlaylistService.chapterSelection$.subscribe(newChapter => {
    //   if (newChapter) {
    //     this.selectedChapter = newChapter;
    //     console.log('selectedChap ', this.selectedChapter);

    //     this.selectedGradeSubChapTopic();
    //     this.availableTopics = newChapter.topics;
    //   }
    // });

    this.questionEditorService.getSelectedChapter().subscribe(chapter => {
      if (chapter) {
        this.selectedChapter = chapter;
        // console.log('selectedChap ', this.selectedChapter);

        this.selectedGradeSubChapTopic();
        this.availableTopics = chapter['topics'];
      }
      // console.log('chapter1111', chapter);
    });

    this.questionEditorService.getSelectedTopic().subscribe(topic => {
      // console.log('topic1111', topic);
      this.selectedTopic = topic;
      // console.log('selectedTopic ', this.selectedTopic);

      this.selectedGradeSubChapTopic();
    });

    // this.curriculumPlaylistService.topicSelection$.subscribe(newTopic => {
    //   this.selectedTopic = newTopic;
    //   console.log('selectedTopic ', this.selectedTopic);

    //   this.selectedGradeSubChapTopic();
    // });

    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.myChapterUpload = fullContentSelection;
        }
      }
    );

    // this.curriculumPlaylistService.selectedSubjectChapterList$.subscribe(
    //   (chapters: Chapter[]) => {
    //     console.log(
    //       'CreateResourceComponent -> ngOnInit -> chapters',
    //       chapters
    //     );
    //     if (chapters && chapters.length > 0) {
    //       this.availableChapters = chapters;
    //       this.selectedChapter = chapters[0];
    //       this.availableTopics = chapters[0].topics;
    //       this.selectedTopic = chapters[0].topics[0];
    //       this.myChapterUpload.chapter = chapters[0];
    //       this.myChapterUpload.topic = chapters[0].topics[0];

    //       this.topicChaptervalue =
    //         this.myChapterUpload.chapter.chapterNumber +
    //         '.' +
    //         this.myChapterUpload.topic.topicNumber +
    //         ' | ' +
    //         this.myChapterUpload.topic.topicTitle;
    //       // this.addResourceOptionsForm.controls.chapterTopic.setValue(
    //       //   this.topicChaptervalue
    //       // );
    //     }
    //     this.cdr.detectChanges();
    //   }
    // );
    console.log('curriculum');
    this.questionEditorService.getAllChapters().subscribe(chapters => {
      this.availableChapters = chapters;
      // console.log('chapters in qe', chapters);

      if (this.availableChapters.length > 0) {
        // this.selectedChapter = chapters[0];
        // if (this.selectedChapter.topics.length > 0) {
        //   this.selectedTopic = this.selectedChapter.topics[0];
        //   this.selectedGradeSubChapTopic();
        // }
      }
    });

    this.curriculumPlaylistService.selectedSubjectChapterQuizList$.subscribe(
      (data: any) => {
        console.log('PlanningModeViewComponent -> data', data);

        if (data) {
          this.selectedGradeSub =
            data.currentGradeId.title +
            (data.currentGradeId.divisions && data.currentGradeId.divisions[0]
              ? data.currentGradeId.divisions[0].divisionTitle
              : '') +
            '|' +
            data.currentSubjectId.title;
          //console.log('PlanningModeViewComponent -> data', data);
          // this.contenteditorData = data;
          console.log('this.contenteditorData -> data', data);
          this.availableChapters = data.cueerntChapterTopic;
          // this.selectedChapter = data.cueerntChapterTopic[0];
          if (data.cueerntChapterTopic.length > 0) {
            this.selectedChapter = data.cueerntChapterTopic[0];
            if (this.selectedChapter.topics.length > 0) {
              this.availableTopics = data.cueerntChapterTopic[0].topics;
              this.selectedTopic = this.selectedChapter.topics[0];
            }
            this.selectedGradeSubChapTopic();
            console.log(
              'this.contenteditorData -> data if',
              data.cueerntChapterTopic[0],
              this.selectedChapter
            );

            // this.displayvalueGradeSubject(data.cueerntChapterTopic[0]);
          } else {
            this.selectedChapterTopic = 'No chapter and Topic';
          }
        }
      }
    );

    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          //console.log("CreateResourceComponent -> ngOnInit -> newClassSelection", newClassSelection)
          this.mySubjectUpload = newClassSelection;
        }
      }
    );

    // } else {
    //   this.customQuestion = false;
    // }
    this.sharedComponentService.getImageData$.subscribe(image => {
      //console.log('file in ', image);
      if (image) {
        this.uploadImgName = image['fileName'];
        this.uploadImgFolder = image['folder'];
      }
    });
    this.sharedComponentService.getDeleteImageStatus().subscribe(bool => {
      //console.log('file in ', image);
      this.deleteImage = bool;
      // console.log('deleteImage ', this.deleteImage);
    });
  }

  selectedGradeSubChapTopic() {
    if (this.selectedGradeLevel && this.selectedSubject) {
      this.selectedGradeSub =
        this.selectedGradeLevel.title + ' | ' + this.selectedSubject.title;
    }
    if (this.selectedChapter && this.selectedTopic) {
      let topic = '';
      if (this.selectedTopic === 'none') {
        topic = 'no topic selected';
      } else {
        topic =
          this.selectedTopic.topicNumber + '. ' + this.selectedTopic.topicTitle;
      }
      this.selectedChapterTopic =
        this.selectedChapter.chapterTitle + ' | ' + topic;
    }
  }

  getTemplatesData() {
    console.log('Question Data 11', this.questionTemplateData);
    if (this.questionData) {
      if (this.questionTemplateData.questionDetails) {
        this.selectedTemplate = this.questionTemplateData.questionDetails.type;
        this.templateData = { ...this.questionTemplateData.questionDetails };
      } else {
        this.selectedTemplate = this.questionTemplateData.type;
        this.templateData = { ...this.questionTemplateData };
      }
    } else {
      this.selectedTemplate = 'SCQ';
      this.selectTemplateData(this.selectedTemplate);
    }
    this.selectTemplate();
    // this.getEditQuestion = this.questionBankService.editQuestionData;
    // console.log('editQuestion', JSON.stringify(this.getEditQuestion));
  }

  // addQuestion(questionData) {
  //   this.questionBankService.addQuestion(questionData);
  //   this.questionBankService.addQuestionBroadcast$.subscribe(data => {
  //     console.log('add Question---------->', data);
  //   });
  // }

  submitQuestion() {
    console.log('templateData 1', this.templateData, this.selectedTopic);
    let tempType = this.templateData['type'];
    let invalid: boolean = false;
    this.toastr.clear();
    if (!this.selectedGradeLevel) {
      invalid = true;
      this.toastr.error('Please select Grade');
    } else if (!this.selectedSubject) {
      invalid = true;
      this.toastr.error('Please select Subject');
    } else if (!this.selectedChapter) {
      invalid = true;
      this.toastr.error('Please select Chapter');
    } else if (!this.selectedTopic || this.selectedTopic === 'none') {
      invalid = true;
      this.toastr.error('Please Select Topic');
    } else if (!this.templateData['data']['stimulus'].label) {
      // console.log(
      //   'addData ',
      //   this.selectedGradeLevel,
      //   this.selectedSubject,
      //   this.selectedChapter,
      //   this.selectedTopic
      // );
      invalid = true;
      this.toastr.error('Cannot submit empty Question!');
    } else {
      if (tempType === 'MCQ' || tempType === 'SCQ' || tempType == 'mcq-tf') {
        let filledOptions = 0;
        let optionLength = this.templateData['data']['options'].length;
        this.templateData['data']['options'].forEach(opt => {
          if (opt.label) {
            filledOptions++;
          }
        });
        if (optionLength < 2 || filledOptions < 2) {
          invalid = true;
          this.toastr.error('Please add atleast 2 options');
        } else if (
          this.templateData['data']['validation']['valid_response']['value']
            .length < 1
        ) {
          invalid = true;
          this.toastr.error('Please select the correct option');
        } else {
          let optionsData = [];
          this.templateData['data']['options'].forEach((opt, index) => {
            if (opt.label) {
              optionsData.push(opt);
            }
          });
          this.templateData['data']['options'] = optionsData;
          invalid = false;
        }
      }
      if (tempType === 'fib-text') {
        let code = this.templateData['data']['template'];
        let matched =
          this.templateData['data']['template'].match(/{{response}}/g) || [];
        if (matched.length == 0) {
          invalid = true;
          this.toastr.error('Please enter sentence with blank responses');
        }
      }
      if (tempType === 'classifyMatchList') {
        this.templateData['data']['stems'].forEach(stem => {
          if (stem.text == '') {
            invalid = true;
          }
        });
        this.templateData['data']['options'].forEach(option => {
          if (option.label == '') {
            invalid = true;
          }
        });
        if (invalid) {
          this.toastr.clear();
          this.toastr.error('Cannot submit blank response');
        }
      }
    }
    if (!invalid) {
      console.log(
        'data to submit ',
        this.templateData,
        this.uploadImgName,
        this.uploadImgFolder
      );

      let questionAdditionalDetails = {
        gradeId: this.selectedGradeLevel.id,
        subjectId: this.selectedSubject.subjectId,
        chapterId: this.selectedChapter.chapterId,
        topicId: this.selectedTopic.topicId,
        deleteImage: this.deleteImage
      };

      if (this.uploadImgName && this.uploadImgFolder) {
        questionAdditionalDetails['fileName'] = this.uploadImgName;
        questionAdditionalDetails['folder'] = this.uploadImgFolder;
      }
      // console.log(
      //   'questionAdditionalDetails ',
      //   this.templateData,
      //   questionAdditionalDetails
      // );

      if (!this.editMode || this.xmlQuestion) {
        if (tempType == 'MCQ' || tempType == 'SCQ' || tempType == 'mcq-tf') {
          //add mode, call studySchemaMapperAddUpdate(this.templateData)
          this.templateData['questionID'] = '';
          let schemaData = this.utilityConfigService.studySchemaMapperAddUpdate();
          let questionData = this.utilityConfigService.questionMapMcq(
            this.templateData,
            this.uploadImgName
          );
          schemaData.data.questions.push(questionData);
          // console.log('schemaData ', schemaData);
          questionAdditionalDetails['questionType'] =
            schemaData.data.questions[0].data.type;
          this.questionBankService.addQuestion(
            schemaData,
            questionAdditionalDetails
          );
          this.questionBankService.addQuestionBroadcast$.subscribe(data => {
            //console.log('add Question---------->', data);
          });
        } else if (tempType == 'fib-text') {
          this.templateData['questionID'] = '';
          let schemaData = this.utilityConfigService.studySchemaMapperFib(
            this.templateData
          );

          this.questionBankService.addQuestion(
            schemaData,
            questionAdditionalDetails
          );
          this.questionBankService.addQuestionBroadcast$.subscribe(data => {
            // console.log(
            //   'add Question---------->',
            //   JSON.stringify(schemaData),
            //   data
            // );
          });
        }
        if (tempType === 'plain-text') {
          this.templateData['questionID'] = '';
          let schemaData = this.utilityConfigService.studySchemaMapperAddUpdate();
          let questionData = this.utilityConfigService.questionMapOpenEnded(
            this.templateData,
            this.uploadImgName
          );
          schemaData.data.questions.push(questionData);
          // console.log('schemaDataOpen ', schemaData);

          questionAdditionalDetails['questionType'] =
            schemaData.data.questions[0].data.type;
          this.questionBankService.addQuestion(
            schemaData,
            questionAdditionalDetails
          );
          this.questionBankService.addQuestionBroadcast$.subscribe(data => {
            // console.log(
            //   'add Question---------->',
            //   JSON.stringify(schemaData),
            //   data
            // );
          });
        }

        // this.questionBankService.addQuestion(this.templateData);
      } else {
        if (tempType === 'MCQ' || tempType === 'SCQ' || tempType == 'mcq-tf') {
          //edit mode - only questions will be changed , call questionMap(this.temokateData)
          this.templateData['questionID'] = this.questionData.questionId;
          let q = this.utilityConfigService.questionMapMcq(
            this.templateData,
            this.uploadImgName
          );
          // console.log('schemaData ', q);
          this.questionData.questionDetails.data.questions = [];
          this.questionData.questionDetails.data.questions.push(q);
          questionAdditionalDetails['questionType'] = q.data.type;
          console.log('editQuestion  ', this.questionData, q);

          //commented by usman on 28/4/21. can be used later
          // this.questionBankService.editQuestion(
          //   this.questionData,
          //   questionAdditionalDetails,
          //   this.questionData.questionId
          // );
          this.questionBankService.editQuestion(
            this.questionData.questionDetails,
            questionAdditionalDetails,
            this.questionData.questionId
          );
        } else if (tempType === 'fib-text') {
          this.templateData['questionID'] = this.questionData.questionId;
          let q = this.utilityConfigService.studySchemaMapperFib(
            this.templateData
          );
          this.questionData.questionDetails.data.questions = [];
          this.questionData.questionDetails.data.questions.push(q);
          this.questionBankService.editQuestion(
            this.questionData.questionDetails,
            questionAdditionalDetails,
            this.questionData.questionId
          );
        } else if (tempType === 'plain-text') {
          this.templateData['questionID'] = this.questionData.questionId;
          let q = this.utilityConfigService.questionMapOpenEnded(
            this.templateData,
            this.uploadImgName
          );
          // console.log('schemaData ', q);
          this.questionData.questionDetails.data.questions = [];
          this.questionData.questionDetails.data.questions.push(q);
          questionAdditionalDetails['questionType'] = q.data.type;
          this.questionBankService.editQuestion(
            this.questionData.questionDetails,
            questionAdditionalDetails,
            this.questionData.questionId
          );
        }
      }
      this.editMode = false;
      this.dismiss(true);
    }
  }

  dismiss(type) {
    this.questionEditorService.updateQstem(null);
    this.questionEditorService.updateOptions([]);
    this.sharedComponentService.setDeleteImageStatus(false);
    this.ref.close(type);
  }

  toggleCompact() {
    this.sidebarService.toggle(true, 'right');
  }

  subtoggle() {
    //console.log('subtoggle');
    this.sidebarService.toggle(true, 'subtoggle');
  }

  selectTemplate() {
    // console.log('select Temp ', event);
    if (
      this.selectedTemplate == 'mcq-ss' ||
      this.selectedTemplate == 'mcq-ms' ||
      this.selectedTemplate == 'mcq-tf'
    ) {
      this.templateComponent = 'mcq-single-select';
    }
    if (this.selectedTemplate == 'fib-text') {
      this.templateComponent = 'fib-text';
    }
    if (this.selectedTemplate == 'hotspot') {
      this.templateComponent = 'hotspot';
    }
  }

  selectTemplateData(event) {
    this.templates.forEach(template => {
      if (event === template.type) {
        this.templateData = template;
      }
    });
  }

  showController() {
    this.showcontroller = !this.showcontroller;
    this.questionEditorService.updateShowController(true);
  }

  openPreview(status) {
    this.questionEditorService.updatePreviewState(status);
    this.globalPreviewState = status;

    this.questionEditorService.updateShowController(true);

    if (!status) {
      this.questionEditorService.updateSubmitAnsShow(status);
      setTimeout(() => {
        this.showcontroller = !this.showcontroller;
      }, 200);
    }
  }

  protected open(closeOnBackdropClick: boolean, closeOnEsc: boolean) {
    //console.log(closeOnBackdropClick, closeOnEsc);
    this.dialogService.open(QuestionEditorPreviewComponent, {
      context: { templateData: this.templateData },
      closeOnBackdropClick,
      closeOnEsc
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    //console.log('destroyed');
    this.templateData = {};
    this.sharedComponentService.getImageData.next({});
  }

  // convertToJson() {
  //   console.log(this.mcqXml, 'this.mcqXml');
  //   this.utilityConfigService.convertXmlToJson(this.mcqXml);
  // }

  openGradeSubChapTopic(data) {
    //console.log('allData ', data);
    this.showOpengradeSubject = data.grade;
    this.showOpenChapterTopic = data.topic;
  }

  displayvalueGradeSubject(subjectGrade) {
    console.log(
      'CreateResourceComponent -> displayvalueGradeSubject -> subjectGrade',
      subjectGrade,
      this.showOpengradeSubject
    );
    this.selectedChapter = null;
    this.selectedTopic = null;
    this.curriculumPlaylistService.getChaptersTopicsQuizEditor(subjectGrade);
    // this.curriculumPlaylistService.setChapterTopicPlanMode(subjectGrade);
    // this.mySubjectUpload = subjectGrade;
    this.selectedGradeLevel = subjectGrade.gradeLevel;
    this.selectedSubject = subjectGrade.subject;
    this.gradeSubjectvalue =
      subjectGrade.gradeLevel.title + ' | ' + subjectGrade.subject.title;
    // this.addResourceOptionsForm.controls.gradeSubject.setValue(
    //   this.gradeSubjectvalue
    // );
    this.showOpengradeSubject = false;
    this.cdr.detectChanges();
    this.selectedGradeSubChapTopic();
  }

  displayvalueChapterTopic(ChapterTopic) {
    // console.log(
    //   'CreateResourceComponent -> displayvalueChapterTopic -> ChapterTopic',
    //   ChapterTopic
    // );
    this.selectedChapter = ChapterTopic.chapter;
    this.availableTopics = ChapterTopic.chapter.topics;
    this.selectedTopic = ChapterTopic.topic;
    this.myChapterUpload = ChapterTopic;
    this.topicChaptervalue =
      ChapterTopic.chapter.chapterNumber +
      '.' +
      ChapterTopic.topic.topicNumber +
      ' | ' +
      ChapterTopic.topic.topicTitle;
    // this.addResourceOptionsForm.controls.chapterTopic.setValue(
    //   this.topicChaptervalue
    // );
    this.showOpenChapterTopic = false;
    this.cdr.detectChanges();
    this.selectedGradeSubChapTopic();
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    // if (this.editMode) {
    //   this.showOpengradeSubject = false;
    //   this.showOpenChapterTopic = false;
    // }
    // console.log('clickout ', event, event.target.classList[0]);
    if (event.target.id === 'grade-selector' && !this.editMode) {
      this.showOpengradeSubject = true;
    }
    if (event.target.id === 'gradeSubject' && !this.editMode) {
      this.showOpengradeSubject = true;
    } else if (event.target.id === 'chapterTopic' && !this.editMode) {
      this.showOpenChapterTopic = true;
    } else {
      this.showOpengradeSubject = false;
      this.showOpenChapterTopic = false;
    }
  }
}
