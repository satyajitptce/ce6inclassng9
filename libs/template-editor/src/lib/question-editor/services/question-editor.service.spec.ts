import { TestBed } from '@angular/core/testing';

import { QuestionEditorService } from './question-editor.service';

describe('QuestionEditorService', () => {
  let service: QuestionEditorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionEditorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
