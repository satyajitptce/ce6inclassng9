import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { QuestionEditorViewComponent } from './question-editor/component/question-editor-view/question-editor-view.component';
import { QuestionEditorModule } from './question-editor/question-editor.module';
import { QuestionEditorContainerComponent } from './question-editor/component/question-editor-container/question-editor-container.component';
import {
  NbDialogModule,
  NbDialogConfig,
  NbDialogService
} from '@nebular/theme';
// import { QuizTemplatesModule } from '@tce/quiz-templates';
import { QbTemplatePreviewModule } from '@tce/qb-template-preview';
import { CoreModule } from '@tce/core';

@NgModule({
  imports: [
    CommonModule,
    QuestionEditorModule,
    LibConfigModule.forChild(QuestionEditorContainerComponent),
    NbDialogModule.forChild(),
    // QuizTemplatesModule,
    QbTemplatePreviewModule,
    CoreModule
  ],
  providers: [NbDialogService]
})
export class TemplateEditorModule {}
