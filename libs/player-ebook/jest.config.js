module.exports = {
  name: 'player-ebook',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-ebook',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
