import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { FormsModule } from '@angular/forms';
import { EbookPlayerComponent } from './components/ebook-player/ebook-player.component';
import { BookDetailsNavComponent } from './components/book-details-nav/book-details-nav.component';
import { PageThumbnailsComponent } from './components/page-thumbnails/page-thumbnails.component';
import { ResourcesListComponent } from './components/resources-list/resources-list.component';
import { CoreModule } from '@tce/core';
import { ViewerPdfModule } from '@tce/viewer-pdf';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { EbookService } from './services/ebook.service';
import { ThumbnailScrollDirective } from './directives/thumbnail-scroll.directive';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { PlayerResourceComponent } from './components/player-resource/player-resource.component';
@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(EbookPlayerComponent),
    PdfViewerModule,
    FormsModule,
    CoreModule,
    ViewerPdfModule,
    AngularSvgIconModule
  ],
  declarations: [
    EbookPlayerComponent,
    BookDetailsNavComponent,
    ResourcesListComponent,
    PageThumbnailsComponent,
    ThumbnailScrollDirective
  ],
  providers: [EbookService],
  exports: [EbookPlayerComponent],
  entryComponents: []
})
export class PlayerEbookModule {}
