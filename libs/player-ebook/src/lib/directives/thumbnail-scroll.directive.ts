import { Directive, OnInit, ElementRef } from '@angular/core';
import { EbookService } from '../services/ebook.service';

@Directive({
  selector: '[tceThumbnailScroll]'
})
export class ThumbnailScrollDirective implements OnInit {
  constructor(
    private ebookService: EbookService,
    private elementRef: ElementRef
  ) {}

  ngOnInit() {
    this.ebookService.pageSelection$.subscribe(page => {
      this.scrollToThumbnail(page.pageNumber);
    });
  }

  scrollToThumbnail(pageNumber: number) {
    const thumbnailWrapper = this.elementRef.nativeElement;
    const pages = thumbnailWrapper.querySelectorAll('li.thumbnail');
    const offSetFromTop = 10;

    if (pageNumber && pages.length) {
      const distanceToScroll = pages[pageNumber - 1].offsetTop - offSetFromTop;
      thumbnailWrapper.scrollTop = distanceToScroll;
    }
  }
}
