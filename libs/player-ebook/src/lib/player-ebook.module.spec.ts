import { async, TestBed } from '@angular/core/testing';
import { PlayerEbookModule } from './player-ebook.module';

describe('PlayerEbookModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerEbookModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerEbookModule).toBeDefined();
  });
});
