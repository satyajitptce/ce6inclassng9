import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';
import {
  Resource,
  AppConfigService,
  RequestApiService,
  ToolbarService,
  CommonService
} from '@tce/core';
import { EbookService } from '../../services/ebook.service';
import { PlayerContainerService } from '@app-teacher/services';

@Component({
  selector: 'tce-resources-list',
  templateUrl: './resources-list.component.html',
  styleUrls: ['./resources-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResourcesListComponent implements OnInit {
  @Input() type: string;
  public chapterResources: Resource[] = [];
  public showChapterResources = true;

  constructor(
    private ebookService: EbookService,
    private appConfigService: AppConfigService,
    private requestApiService: RequestApiService,
    private playerContainerService: PlayerContainerService,
    private toolbarService: ToolbarService,
    private commonService: CommonService
  ) {}

  ngOnInit() {
    this.ebookService.chapterResources$.subscribe(resources => {
      //for a longer list -
      //this.chapterResources = resources.concat(resources).concat(resources);
      this.chapterResources = resources;
    });
  }

  handleResourceClick(resource: Resource) {
    // console.log(
    //   'TCL: ResourcesListComponent -> handleResourceClick -> resource',
    //   resource
    // );
    this.commonService.setebookResourcelistFlag(true);
    this.commonService.setebookResourcelist(resource);
    //this.playerContainerService.openResource(resource);
  }

  getResourceThumbnailIcon(resource: Resource, type: string) {
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }

  getResourceThumbnail(thumbnailParams) {
    if (thumbnailParams) {
      return this.requestApiService.getUrl('getFile') + thumbnailParams;
    } else {
      return '';
    }
  }

  triggerScroll(emittedObject: {
    eventName: string;
    type: string;
    cachedCurrentTarget: any;
  }) {
    if (
      emittedObject.eventName === 'mousedown' ||
      emittedObject.eventName === 'touchstart'
    ) {
      const pdfElement =
        emittedObject.cachedCurrentTarget.parentNode.parentNode.parentNode
          .parentNode.parentNode.parentNode.previousElementSibling.firstChild;
      if (emittedObject.type === 'up') {
        pdfElement.scrollTop -= 20;
      } else {
        pdfElement.scrollTop += 20;
      }
    }
  }
}
