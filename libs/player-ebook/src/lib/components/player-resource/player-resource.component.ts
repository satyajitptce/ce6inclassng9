import {
  Component,
  OnInit,
  OnDestroy,
  ViewContainerRef,
  ViewChild,
  ElementRef,
  ComponentRef,
  HostBinding,
  HostListener
} from '@angular/core';
import {
  PanZoomService,
  PlayerAbstractComponent,
  Resource,
  ResourceType,
  ToolbarService,
  CommonService
} from '@tce/core';
import { Subscription } from 'rxjs';
import { PlayerDrawerService } from '@app-teacher/services/player-drawer.service';
import { PlayerContainerService } from '@app-teacher/services';
import { PLAYER_OFFSET_TOP } from '../../../../../../apps/tce-teacher/src/app/shared/shared.constants';
@Component({
  selector: 'tce-player-resource',
  templateUrl: './player-resource.component.html',
  styleUrls: ['./player-resource.component.scss']
})
export class PlayerResourceComponent implements OnInit {
  @ViewChild('playerRef', { static: true, read: ViewContainerRef })
  playerViewRef: ViewContainerRef;

  playerFactory;

  playerComponentRef: ComponentRef<PlayerAbstractComponent>;
  playerCloseSubscriber: Subscription;

  associatedResource: Resource;
  currentZoomScale = 1;

  subscriptions: Subscription = new Subscription();

  isPlayerResourceDrawerOpen: boolean;

  isPointerDisabled = false;
  focusMode = false;
  currentTool = null;
  currentActiveResourceId;

  @HostBinding('class') get hostClasses() {
    const isPdfResource =
      this.associatedResource.resourceType === ResourceType.EBOOK ||
      this.associatedResource.resourceType === ResourceType.WORKSHEET;
    const isQuizPlayer =
      this.associatedResource.resourceType === ResourceType.QUIZ;

    const isTcePlayer =
      this.associatedResource.resourceType === ResourceType.TCEVIDEO;
    const isImageGallery =
      this.associatedResource.resourceType === ResourceType.GALLERY;
    const isCustomVideo =
      this.associatedResource.resourceType === ResourceType.VIDEO;
    const isUnsupport =
      this.associatedResource.resourceType === ResourceType.UNSUPPORT;
    let classes = '';
    if (this.isPointerDisabled) {
      classes += ' -disable-pointer-events';
    }
    if (isPdfResource) {
      classes += ' -pdf-viewer';
    }
    if (this.isPlayerResourceDrawerOpen) {
      classes += ' -drawer-open';
    }
    if (isQuizPlayer) {
      classes += ' -quiz-player';
    }
    if (isTcePlayer) {
      classes += ' -tce-video-player';
    }
    if (this.focusMode) {
      classes += ' -focus-mode-ON';
    }
    if (isImageGallery) {
      classes += '-image-gallery';
    }
    if (isCustomVideo) {
      classes += '-custom-video';
    }
    if (isUnsupport) {
      classes += ' -unsupport-player';
    }

    return classes;
  }

  constructor(
    private panZoomService: PanZoomService,
    private eleRef: ElementRef,
    private playerDrawerService: PlayerDrawerService,
    private playerContainerService: PlayerContainerService,
    private toolbarService: ToolbarService,
    private commonService: CommonService
  ) {}
  ngOnInit() {
    this.subscriptions.add(
      this.panZoomService.currentZoomScale$.subscribe(scaleValue => {
        this.currentZoomScale = scaleValue;
      })
    );
    this.subscriptions.add(
      this.playerDrawerService.drawerOpenStatus$.subscribe(isOpen => {
        this.isPlayerResourceDrawerOpen = isOpen;
      })
    );
    this.subscriptions.add(
      this.playerContainerService.currentActiveResourceId$.subscribe(
        resourceId => {
          this.currentActiveResourceId = resourceId;
          if (this.associatedResource.resourceId === resourceId) {
            if (
              this.playerComponentRef &&
              this.playerComponentRef.instance.playerActivated
            ) {
              this.playerComponentRef.instance.playerActivated();
            }
            this.moveToPlayer();
          }
        }
      )
    );
    this.toolbarService.focusMode$.subscribe(focusSatus => {
      this.focusMode = focusSatus;
    });
    this.toolbarService.currentSelectedTool$.subscribe(currentTool => {
      this.currentTool = currentTool;
    });
  }
  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  removePlayer() {
    //console.log("removePlayer")
    this.playerViewRef.clear();
    this.isPointerDisabled = true;
    this.playerComponentRef = null;
  }
  closeCurrentResource() {
    //console.log("closeCurrentResource")
    this.playerComponentRef.destroy();
    this.playerViewRef.clear();
    this.isPointerDisabled = true;
    this.playerComponentRef = null;
    this.ngOnDestroy();
    this.commonService.setebookResourcelistFlag(false);
  }
  moveToPlayer() {
    const {
      top,
      left,
      height,
      width
    } = this.eleRef.nativeElement.getBoundingClientRect();
    const playerMidX = left + width / 2;
    const playerMidY = top + height / 2;
    const middleOfWindow = {
      x: window.innerWidth / 2,
      y: window.innerHeight / 2
    };
    // Offset the player to be closer to the top of the screen.
    // 70px is subtracted as a buffer to allow for the logo to be above the players.
    // height / this.currentZoomScale gets the player's height at a zoom level of 1.
    // We then multiply by the zoom scale to scale the distance of the player from the middle of the screen.
    const playerOffset = Math.round(
      ((window.innerHeight - height / this.currentZoomScale) / 2 -
        PLAYER_OFFSET_TOP) *
        this.currentZoomScale
    );
    const panToPoint = {
      x: middleOfWindow.x - playerMidX,
      y: middleOfWindow.y - playerMidY - playerOffset
    };
    // 1. Pan center of player to center of screen.
    this.panZoomService.panToPoint(panToPoint);
    // 2. Zoom center of screen.
    // Zoom to center of screen needs to account for animation time.
    // This delay based on animation time should be a type variable.
    setTimeout(() => {
      this.panZoomService.setZoomToPointAndScale(middleOfWindow, 1);
    }, 10);
  }

  setPlayerResource() {
    if (this.playerComponentRef && this.associatedResource) {
      this.playerComponentRef.instance.resource = this.associatedResource;
    }
  }

  openPlayer() {
    if (!this.playerComponentRef) {
      this.playerComponentRef = this.playerViewRef.createComponent(
        this.playerFactory
      );
    }

    this.setPlayerResource();
    this.setPlayerCloseListener();
    this.isPointerDisabled = false;
    this.moveToPlayer();
  }
  setAssociatedResource(resource: Resource) {
    this.associatedResource = resource;
  }
  setPlayerCloseListener() {
    if (this.playerComponentRef) {
      this.playerCloseSubscriber = this.playerComponentRef.instance.playerCloseEmitter.subscribe(
        (nextResourceToOpen: Resource) => {
          this.playerContainerService.closeResource(this.associatedResource);
          if (nextResourceToOpen) {
            this.playerContainerService.openResource(nextResourceToOpen);
          }
        }
      );
      this.playerComponentRef.onDestroy(() => {
        this.playerCloseSubscriber.unsubscribe();
      });
    }
  }
  onLoadPlayerFactory(componentFactory, openPlayer: boolean) {
    if (!this.playerFactory) {
      this.playerFactory = componentFactory;
      if (openPlayer) {
        this.playerComponentRef = this.playerViewRef.createComponent(
          this.playerFactory
        );
      }
      this.setPlayerCloseListener();
      this.setPlayerResource();
      this.isPointerDisabled = !openPlayer;
    }
  }
}
