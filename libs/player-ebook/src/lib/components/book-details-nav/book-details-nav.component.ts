import {
  Component,
  OnInit,
  Output,
  OnDestroy,
  EventEmitter,
  Input,
  ViewChild,
  ElementRef
} from '@angular/core';
import { EbookService } from '../../services/ebook.service';
import {
  Ebook,
  Chapter,
  KeyboardState,
  KeyboardService,
  KeyboardTheme,
  CurriculumPlaylistService
} from '@tce/core';
import { Subscription } from 'rxjs';

@Component({
  selector: 'tce-book-details-nav',
  templateUrl: './book-details-nav.component.html',
  styleUrls: ['./book-details-nav.component.scss']
})
export class BookDetailsNavComponent implements OnInit, OnDestroy {
  @Output() openThumbnails: EventEmitter<boolean> = new EventEmitter();
  @Input() numEbookPages: Array<number>;
  @Input() showThumbnails: boolean;
  @ViewChild('goToInput', {static:false}) goToInput: ElementRef;
  currentChapterId: any;
  ebooks: Ebook[] = [];
  public selectedEbook: Ebook;
  public selectedChapter: Chapter;
  public selectedBookIndex: number;
  public $subs = new Subscription();

  constructor(
    public ebookService: EbookService,
    public keyboardService: KeyboardService,
    public curriculumPlaylistService: CurriculumPlaylistService
  ) {}

  ngOnInit() {
    this.$subs.add(
      this.ebookService.ebooks$.subscribe(ebooks => {
        //console.log('ebooks', ebooks);
        this.ebooks = ebooks;
      })
    );

    this.$subs.add(
      this.ebookService.ebookSelection$.subscribe(ebook => {
        this.selectedEbook = ebook;
        // console.log(
        //   'TCL: BookDetailsNavComponent -> ngOnInit -> selectedEbook',
        //   this.selectedEbook
        // );
        this.selectedBookIndex = this.ebooks.findIndex(
          book => ebook.bookId === book.bookId
        );
      })
    );

    this.$subs.add(
      this.ebookService.chapterSelection$.subscribe(chapter => {
        this.selectedChapter = chapter;
        if (this.goToInput) {
          this.goToInput.nativeElement.value = '';
        }
      })
    );

    this.$subs.add(
      this.keyboardService.keyboardEnterPress$.subscribe(inputVal => {
        if (!this.goToInput) {
          return;
        }
        if (this.goToInput.nativeElement.value === inputVal) {
          this.goToPage(this.goToInput.nativeElement.value);
        }
      })
    );
    // this.curriculumPlaylistService.fullContentSelection$.subscribe(chapters => {

    //   if (chapters) {
    //     this.currentChapterId = chapters.chapter.chapterId;
    //     console.log('ebook--->chapters', chapters);
    //     if(this.currentChapterId && this.selectedEbook && this.selectedEbook.chapters){
    //       for (let index = 0; index < this.selectedEbook.chapters.length; index++) {
    //         if(this.currentChapterId === this.selectedEbook.chapters[index].chapterId){
    //           this.selectedBookIndex = index;
    //           this.selectChapter(this.selectedEbook, this.selectedEbook.chapters[index])
    //         }

    //       }
    //     }
    //   }
    // });
  }

  ngOnDestroy() {
    this.$subs.unsubscribe();
  }

  public selectChapter(ebook: Ebook, chapter: Chapter) {
    // this.curriculumPlaylistService.setChapterSelection(
    //   chapter,
    //   chapter.topics[0]
    // );
    //console.log('ebook', ebook);
    this.ebookService.setEbookSelection(ebook);
    this.ebookService.setChapterSelection(chapter);
    this.ebookService.setPageSelection({
      pageNumber: 1,
      eventType: 'click'
    });
    this.openThumbnails.emit(true);
  }

  public goToPage(val: string) {
    const page = parseInt(val, 10);
    let _page = page === 0 ? 1 : page;
    _page =
      page > this.numEbookPages.length ? this.numEbookPages.length : _page;
    this.ebookService.setPageSelection({
      pageNumber: _page,
      eventType: 'click'
    });
    this.goToInput.nativeElement.value = _page;
    this.keyboardService.setCurrentInputValue(_page.toString());
  }

  public updateKeyboardValue(e: KeyboardEvent) {
    this.keyboardService.setCurrentInputValue(
      this.goToInput.nativeElement.value
    );
  }

  public setKeyboardFocus(e: FocusEvent) {
    const obj = {
      inputPattern: /^[0-9]*$/,
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.NUMERIC
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }
}
