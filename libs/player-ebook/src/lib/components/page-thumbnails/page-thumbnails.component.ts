import { Component, OnInit, Input } from '@angular/core';
import { EbookService } from '../../services/ebook.service';
import { Ebook } from '@tce/core';

@Component({
  selector: 'tce-page-thumbnails',
  templateUrl: './page-thumbnails.component.html',
  styleUrls: ['./page-thumbnails.component.scss']
})
export class PageThumbnailsComponent implements OnInit {
  @Input('pdfSrc') set pdfSrc(value) {
    this.pdfFilePath = value;
  }
  @Input() numEbookPages: [];
  @Input() ebooks: Ebook[];
  @Input() selectedPage: number;
  public selectedThumbnail = 0;
  public pdfFilePath: string;

  constructor(public ebookService: EbookService) {}

  ngOnInit() {
    this.ebookService.pageSelection$.subscribe(page => {
      this.selectedThumbnail = page.pageNumber - 1;
    });
  }

  public selectPage(i: number) {
    this.ebookService.setPageSelection({
      pageNumber: i + 1,
      eventType: 'click'
    });
  }
}
