module.exports = {
  name: 'qb-question-card-list',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/qb-question-card-list',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
