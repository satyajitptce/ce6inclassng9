import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbQuestionCardListComponent } from './qb-question-card-list.component';

describe('QbQuestionCardListComponent', () => {
  let component: QbQuestionCardListComponent;
  let fixture: ComponentFixture<QbQuestionCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QbQuestionCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbQuestionCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
