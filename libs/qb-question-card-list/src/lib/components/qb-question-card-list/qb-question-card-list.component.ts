import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { RequestApiService } from '@tce/core';
import {
  QuestionbankserviceService,
  UtilityConfigService
} from '@tce/question-bank';
import { PlanningModeService } from 'libs/planning-mode/src/lib/services/planning-mode.service';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'tce-qb-question-card-list',
  templateUrl: './qb-question-card-list.component.html',
  styleUrls: ['./qb-question-card-list.component.scss']
})
export class QbQuestionCardListComponent implements OnInit, OnDestroy {
  @Input() showCheckBox;
  @Input() allQuestionData: Array<object>;
  @Input() quizQuestionList;
  public showEdit = false;
  public edit_id;

  public noQuestionsAvailable: any = false;
  public statusMessage = '';
  public isAllQuestionsLoaded: boolean = true;
  public loaderQCountDisplay = 'Loading question ';
  public questionFilters: any;
  public getDataSubscription: Subscription;

  constructor(
    private questionBankService: QuestionbankserviceService,
    private planModeService: PlanningModeService,
    private requestApiService: RequestApiService,
    private utilityConfigService: UtilityConfigService,
    private dialogService: NbDialogService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    console.log('allData1');

    this.restartQbView();
  }

  ngOnDestroy() {
    console.log('destroy');

    if (this.getDataSubscription) this.getDataSubscription.unsubscribe();
  }

  restartQbView() {
    this.statusMessage = 'No questions found.';
    this.questionBankService.editQuestionMode$.subscribe(mode => {});
    this.questionBankService.noQuestionsAvailableBroadcast$.subscribe(flag => {
      this.noQuestionsAvailable = flag;
      if (flag) {
        this.isAllQuestionsLoaded = true;
      }
    });

    // this.planModeService.qbChangeChapterTopicGet().subscribe(value => {
    //   if (value) {
    //     this.getQuestionData();
    //     this.planModeService.qbChangeChapterTopicSet(false);
    //   }
    // });

    this.planModeService.getQuizQuestionList().subscribe(questions => {
      this.quizQuestionList = questions;
      console.log('questionList ', this.quizQuestionList);
    });

    // this.planModeService.getSelectQuiz().subscribe(select => {
    //   this.showCheckBox = select;
    //   console.log('checkbox', this.showCheckBox);
    // });

    // default load question card viewer
    // this.getQuestionData();
    // this.createFinalCardData();
  }

  // async getQuestionData() {
  //   //resets
  //   this.noQuestionsAvailable = false;
  //   this.isAllQuestionsLoaded = false;
  //   this.questionBankService.questionsLength = 0;
  //   this.allQuestionData = [];
  //   this.loaderQCountDisplay = 'Loading questions...';
  //   //---------------------------

  //   let newSelectedTopic: string;
  //   if (this.selectedTopic !== 'none') {
  //     newSelectedTopic = this.selectedTopic.topicId;
  //   } else {
  //     newSelectedTopic = 'none';
  //   }
  //   this.questionFilters = {
  //     selectedGrade: this.selectedGrade.id,
  //     selectedSubject: this.selectedSubject.subjectId,
  //     selectedChapter: this.selectedChapter.chapterId,
  //     selectedTopic: newSelectedTopic
  //   };
  //   await this.getCustomQuestions();
  //   await this.getTataQuestions();
  // }

  // getTataQuestions(): Promise<boolean> {
  //   return new Promise((resolve, reject) => {
  //     this.questionBankService.getQuestionBankData(
  //       'tata',
  //       this.questionFilters
  //     );
  //     resolve(true);
  //   });
  // }

  // getCustomQuestions(): Promise<boolean> {
  //   return new Promise((resolve, reject) => {
  //     let customQuestion = this.questionBankService.getQuestionBankData(
  //       'custom',
  //       this.questionFilters
  //     );
  //     resolve(true);
  //   });
  // }

  // createFinalCardData() {
  //   //resets
  //   this.questionBankService.questionsLength = 0;
  //   this.isAllQuestionsLoaded = false;
  //   //---------------------------------

  //   let random = Math.random();

  //   this.getDataSubscription = this.questionBankService.questionbankDataBroadcast$.subscribe(
  //     qData => {
  //       // console.log('qData ', qData);

  //       this.noQuestionsAvailable = false;
  //       if (qData) {
  //         let tataIndex = -1;
  //         let questionData: any;
  //         questionData = qData;

  //         let dataStr = JSON.stringify(questionData);

  //         if (dataStr.charAt(1) === '<') {
  //           //console.log('<<-- FOUND TESTEDGE QUESTION XML-->> ');
  //           questionData = this.utilityConfigService.convertToJson(
  //             questionData
  //           );
  //         } else {
  //           //console.log('not found');
  //         }

  //         if (questionData.questionDetails) {
  //           questionData.questionDetails.encryptedPath =
  //             questionData.encryptedPath;
  //           questionData.questionDetails.fileUrl = this.requestApiService.getUrl(
  //             'getFile'
  //           );
  //           if (questionData.questionDetails.qtext) {
  //             if (
  //               questionData.questionDetails.qtext.indexOf('<img src') != -1
  //             ) {
  //               let qtextImg = questionData.questionDetails.qtext.split("'");
  //               questionData.questionDetails['questionTitle'] =
  //                 "<img src='" +
  //                 questionData.questionDetails.fileUrl +
  //                 '/' +
  //                 questionData.questionDetails.encryptedPath +
  //                 '/' +
  //                 qtextImg[1] +
  //                 "'>";
  //             } else {
  //               questionData.questionDetails['questionTitle'] = this.stripHtml(
  //                 questionData.questionDetails.qtext
  //               );
  //             }
  //           }
  //           if (questionData.questionDetails.data) {
  //             //console.log('datadata ', question);
  //             if (questionData.questionDetails.data.stimulus) {
  //               if (questionData.questionDetails.data.stimulus.label) {
  //                 questionData.questionDetails[
  //                   'questionTitle'
  //                 ] = this.stripHtml(
  //                   questionData.questionDetails.data.stimulus.label
  //                 );
  //               } else {
  //                 questionData.questionDetails[
  //                   'questionTitle'
  //                 ] = this.stripHtml(
  //                   questionData.questionDetails.data.stimulus
  //                 );
  //               }
  //             }
  //             if (questionData.questionDetails.data.questions) {
  //               questionData.questionDetails['questionTitle'] = this.stripHtml(
  //                 questionData.questionDetails.data.questions[0].data.stimulus
  //               );
  //               if (
  //                 questionData.questionDetails.data.questions[0].data
  //                   .stimulus_media
  //               ) {
  //                 questionData.questionDetails['image'] =
  //                   questionData.questionDetails.fileUrl +
  //                   '/' +
  //                   questionData.encryptedPath +
  //                   '/' +
  //                   questionData.questionId +
  //                   '/images/' +
  //                   questionData.questionDetails.data.questions[0].data
  //                     .stimulus_media;
  //               }
  //             }
  //           }
  //         }
  //         if (questionData.questionSourceType) {
  //           this.allQuestionData.push(questionData);
  //         } else {
  //           tataIndex += 1;
  //           this.allQuestionData.splice(tataIndex, 0, questionData);
  //         }
  //         this.loaderQCountDisplay =
  //           'Loading question ' +
  //           this.allQuestionData.length +
  //           ' of ' +
  //           this.questionBankService.questionsLength;
  //         if (
  //           this.allQuestionData.length ===
  //           this.questionBankService.questionsLength
  //         ) {
  //           this.isAllQuestionsLoaded = true;
  //         } else {
  //           this.isAllQuestionsLoaded = false;
  //         }
  //         //console.log("All Questions Loaded : ",this.isAllQuestionsLoaded, " totalQ-->>",this.questionBankService.questionsLength + " currentQ-->>", this.allQuestionData.length)
  //       }
  //     }
  //   );
  // }

  stripHtml(html) {
    // Create a new div element
    var temporalDivElement = document.createElement('div');
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || '';
  }

  openPopup(questionData) {
    //console.log('questionData', questionData);
    let qData: any;
    let qType: any;

    if (questionData.data) {
      if (questionData.data.questionSourceType) {
        if (questionData.mode === 'edit') {
          qType = 'custom'; //changes done by usman. To load custom question in preview mode
          qData = JSON.parse(JSON.stringify(questionData.data));
        } else {
          questionData.data.questionDetails['questionId'] =
            questionData.data.questionId;
          qType = 'TCE'; //changes done by usman. To load custom question in preview mode
          qData = questionData.data.questionDetails;
        }
      } else {
        qType = 'TCE';
        qData = questionData.data.questionDetails;
      }
      this.open(false, false, qData, qType);
    } else {
      qType = 'custom';
      this.open(false, false, qData, qType);
    }
    this.showEdit = !this.showEdit;
  }

  protected open(
    closeOnBackdropClick: boolean,
    closeOnEsc: boolean,
    qData,
    qType
  ) {
    this.dialogService
      .open(QuestionEditorContainerComponent, {
        closeOnBackdropClick,
        closeOnEsc,
        context: { questionData: qData, questionType: qType }
      })
      .onClose.subscribe(save => {
        if (save) {
          this.planModeService.qbChangeChapterTopicSet(true);
        }
      });
  }

  deleteQuestion(questionId) {
    this.questionBankService.deleteQuestion(questionId);
    this.questionBankService.deleteQuestionBroadcast$.subscribe(data => {
      console.log('delete Question---------->', data);
      if (data && data['status'] === 'success') {
        this.toastr.success('Question deleted successfully!');
        this.planModeService.qbChangeChapterTopicSet(true);
      }
    });
  }
}
