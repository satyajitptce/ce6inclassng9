import {
  Component,
  HostListener,
  Input,
  OnInit,
  Output,
  EventEmitter,
  AfterViewInit
} from '@angular/core';
import { PlanningModeService } from 'libs/planning-mode/src/lib/services/planning-mode.service';
import { QuestionbankserviceService } from 'libs/question-bank/src/lib/services/questionbankservice.service';

@Component({
  selector: 'tce-qb-question-card-view',
  templateUrl: './qb-question-card-view.component.html',
  styleUrls: ['./qb-question-card-view.component.scss']
})
export class QbQuestionCardViewComponent implements OnInit {
  @Input() question;
  @Input() index;
  @Input() showCheckBox;
  @Input() quizQuestionList: Array<string>;
  @Input() viewType: string;
  @Output() openQtnEditor = new EventEmitter();
  @Output() dltQuestion = new EventEmitter();
  @Output() openpopup = new EventEmitter();
  public showEdit = false;
  public edit_id;
  public qEditId;

  public checkedQuestion: boolean;
  constructor(
    public questionBankService: QuestionbankserviceService,
    private planModeService: PlanningModeService
  ) {}

  ngOnInit(): void {
    this.questionBankService.getEditQuestionIndex().subscribe(index => {
      this.edit_id = index;
    });
    this.questionBankService.getQEditQuestionIndex().subscribe(index => {
      this.qEditId = index;
    });
    const questionIds = this.quizQuestionList.map(
      question => question['questionId']
    );
    if (questionIds.includes(this.question.questionId)) {
      console.log('checkedQuestion1');

      this.checkedQuestion = true;
    } else {
      this.checkedQuestion = false;
    }
  }

  ngOnChanges() {
    if (this.quizQuestionList) {
      if (this.quizQuestionList.includes(this.question)) {
        console.log('checkedQuestion2');

        this.checkedQuestion = true;
      } else {
        this.checkedQuestion = false;
      }
    }
  }

  openQuestionEditor() {
    console.log('index ', this.edit_id, this.index, this.showEdit);

    // if (this.edit_id === undefined) {
    //   console.log('this.edit_id', this.edit_id);
    // } else if (this.index !== this.edit_id) {
    //   this.showEdit = !this.showEdit;
    //   console.log('not equal');
    // }
    // if (this.edit_id === this.index) {
    //   this.showEdit = false;
    // }
    if (this.viewType === 'questionBank') {
      this.edit_id = this.index;

      this.questionBankService.setEditQuestionIndex(this.index);
      // this.showEdit = !this.showEdit;
      // this.openQtnEditor.emit(this.index);
    } else {
      this.qEditId = this.index;

      this.questionBankService.setQEditQuestionIndex(this.index);
      // this.showEdit = !this.showEdit;
      // this.openQtnEditor.emit(this.index);
    }
  }

  openPopup(mode, question) {
    let data = {
      data: question,
      mode: mode
    };
    console.log('questionData ', question);
    this.openpopup.emit(data);
  }

  deleteQuestion(questionId) {
    this.dltQuestion.emit(questionId);
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (event.target.id === 'question-more-option') {
      console.log('question-more-option---selected');
      this.showEdit = true;
    } else {
      this.showEdit = false;
      this.edit_id = null;
      this.qEditId = null;
    }
  }

  selectQuestions(event) {
    console.log('quizQuestions event', event, this.question);
    if (event.target.checked) {
      this.quizQuestionList.push(this.question);
    } else {
      let index = this.quizQuestionList.indexOf(this.question);
      this.quizQuestionList.splice(index, 1);
    }
    this.planModeService.setQuizQuestionList(this.quizQuestionList);
    console.log('quizQuestionList ', this.quizQuestionList);
  }
}
