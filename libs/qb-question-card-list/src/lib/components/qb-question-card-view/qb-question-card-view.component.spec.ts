import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbQuestionCardViewComponent } from './qb-question-card-view.component';

describe('QbQuestionCardViewComponent', () => {
  let component: QbQuestionCardViewComponent;
  let fixture: ComponentFixture<QbQuestionCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QbQuestionCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbQuestionCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
