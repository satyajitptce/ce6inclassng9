import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QbQuestionCardListComponent } from './components/qb-question-card-list/qb-question-card-list.component';
import { QbQuestionCardViewComponent } from './components/qb-question-card-view/qb-question-card-view.component';
import { CoreModule } from '@tce/core';

@NgModule({
  imports: [CommonModule, CoreModule],
  declarations: [QbQuestionCardListComponent, QbQuestionCardViewComponent],
  exports: [QbQuestionCardListComponent, QbQuestionCardViewComponent]
})
export class QbQuestionCardListModule {}
