import { async, TestBed } from '@angular/core/testing';
import { QbQuestionCardListModule } from './qb-question-card-list.module';

describe('QbQuestionCardListModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [QbQuestionCardListModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(QbQuestionCardListModule).toBeDefined();
  });
});
