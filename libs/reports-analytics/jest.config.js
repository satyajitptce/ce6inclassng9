module.exports = {
  name: 'reports-analytics',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/reports-analytics',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
