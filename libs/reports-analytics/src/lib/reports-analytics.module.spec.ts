import { async, TestBed } from '@angular/core/testing';
import { ReportsAnalyticsModule } from './reports-analytics.module';

describe('ReportsAnalyticsModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReportsAnalyticsModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ReportsAnalyticsModule).toBeDefined();
  });
});
