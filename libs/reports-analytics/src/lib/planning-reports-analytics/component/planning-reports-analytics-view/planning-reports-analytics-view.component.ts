import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tce-planning-reports-analytics-view',
  templateUrl: './planning-reports-analytics-view.component.html',
  styleUrls: ['./planning-reports-analytics-view.component.css']
})
export class PlanningReportsAnalyticsViewComponent implements OnInit {
  
  menu = [
    {
      name: "Principal Dashboard", 
      section: "principal-dashboard",
      active: true
    },
    {
      name: "School Usage",
      section: "school-usage",
      active: false
    }
  ]
  activeMenu = this.menu[0];
  constructor() {
    
  }

  ngOnInit(): void {}

  onMenuClick(menu){
    this.menu.forEach(x => x.active=false)
    menu.active = true;
    this.activeMenu = menu;
  }
}
