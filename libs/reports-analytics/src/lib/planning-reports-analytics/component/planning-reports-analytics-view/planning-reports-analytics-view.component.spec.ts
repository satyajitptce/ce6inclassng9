import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningReportsAnalyticsViewComponent } from './planning-reports-analytics-view.component';

describe('PlanningReportsAnalyticsViewComponent', () => {
  let component: PlanningReportsAnalyticsViewComponent;
  let fixture: ComponentFixture<PlanningReportsAnalyticsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningReportsAnalyticsViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningReportsAnalyticsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
