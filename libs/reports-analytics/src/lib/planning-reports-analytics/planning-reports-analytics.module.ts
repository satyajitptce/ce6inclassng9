import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningReportsAnalyticsViewComponent } from './component/planning-reports-analytics-view/planning-reports-analytics-view.component';

@NgModule({
  declarations: [PlanningReportsAnalyticsViewComponent],
  imports: [CommonModule]
})
export class PlanningReportsAnalyticsModule {}
