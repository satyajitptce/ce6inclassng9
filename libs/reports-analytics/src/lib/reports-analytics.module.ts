import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { PlanningReportsAnalyticsViewComponent } from './planning-reports-analytics/component/planning-reports-analytics-view/planning-reports-analytics-view.component';
import {
  NbButtonModule,
  NbLayoutModule,
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbActionsModule
} from '@nebular/theme';

@NgModule({
  declarations: [PlanningReportsAnalyticsViewComponent],
  imports: [
    CommonModule,
    LibConfigModule.forChild(PlanningReportsAnalyticsViewComponent),
    NbActionsModule,
    NbLayoutModule,
    NbCardModule,
    NbIconModule,
    NbSelectModule,
    NbButtonModule
  ],
  
})
export class ReportsAnalyticsModule {}
