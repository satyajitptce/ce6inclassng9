import { TestBed } from '@angular/core/testing';

import { QbTemplatePreviewService } from './qb-template-preview.service';

describe('QbTemplatePreviewService', () => {
  let service: QbTemplatePreviewService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QbTemplatePreviewService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
