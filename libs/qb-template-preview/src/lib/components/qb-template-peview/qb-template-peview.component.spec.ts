import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QbTemplatePeviewComponent } from './qb-template-peview.component';

describe('QbTemplatePeviewComponent', () => {
  let component: QbTemplatePeviewComponent;
  let fixture: ComponentFixture<QbTemplatePeviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [QbTemplatePeviewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QbTemplatePeviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
