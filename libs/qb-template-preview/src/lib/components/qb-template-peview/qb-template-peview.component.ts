import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  ComponentFactoryResolver,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { NbDialogRef, NbDialogService } from '@nebular/theme';
import { BehaviorSubject, Subject } from 'rxjs';
import { QuestionEditorService } from '@tce/template-editor';
import {
  FibTextLayoutComponent,
  McqSingleSelectLayoutComponent,
  HotspotLayoutComponent,
  EssayTextLayoutComponent,
  OrderListComponent,
  FibImageDragDropLayoutComponent,
  TokenHighlightLayoutComponent,
  MatchListComponent
} from '@tce/quiz-templates';
import { UtilityConfigService } from '@tce/question-bank';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'tce-qb-template-peview',
  templateUrl: './qb-template-peview.component.html',
  styleUrls: ['./qb-template-peview.component.scss']
})
export class QbTemplatePeviewComponent implements OnInit {
  @Input() templateData: any;
  @Input() mode: string;
  @Input() questionType: string;
  @ViewChild('targetRef', { read: ViewContainerRef, static: true })
  vcRef: ViewContainerRef;
  @Output() public allowSubmitButton = new EventEmitter();
  @Output() public correctAnswerCheck = new EventEmitter();
  @Output() public dismissDialog = new EventEmitter();
  public previewState: boolean = true;
  private submit: Subject<void> = new Subject<void>();
  private save: Subject<void> = new Subject<void>();
  private metadata: Subject<void> = new Subject<void>();
  private viewDevice: Subject<void> = new Subject<void>();
  private layoutView: Subject<void> = new Subject<void>();
  private showAnsState: boolean = false;
  private correctAnswer: string = 'incorrect';
  public templateType: string;
  public allowSubmit: boolean;
  private componentHashmap = {
    mcq: McqSingleSelectLayoutComponent,
    MCQ: McqSingleSelectLayoutComponent,
    'mcq-tf': McqSingleSelectLayoutComponent,
    SCQ: McqSingleSelectLayoutComponent,
    fib: FibTextLayoutComponent,
    hotspot: HotspotLayoutComponent,
    essay: EssayTextLayoutComponent,
    classify: OrderListComponent,
    fibImage: FibImageDragDropLayoutComponent,
    token: TokenHighlightLayoutComponent,
    classifyMatchList: MatchListComponent
  };

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private questionEditorService: QuestionEditorService,
    private cd: ChangeDetectorRef,
    private utilityConfigService: UtilityConfigService
  ) {}

  ngOnInit(): void {
    this.questionEditorService.updateSubmitAnsShow(false);
    this.questionEditorService.updatePreviewState(true);
    let dataStr = JSON.stringify(this.templateData);
    if (this.templateData) {
      console.log('mode ', this.templateData);
      if ('qtext' in this.templateData) {
        //console.log('abcdeabcde');

        // if (dataStr.charAt(1) === '<') {
        //   console.log('<<-- FOUND TESTEDGE QUESTION XML-->> ');
        //   this.templateData = this.utilityConfigService.convertToJson(
        //     this.templateData.value
        //   );
        // } else {
        ///console.log('templateData qText');

        this.templateData = this.utilityConfigService.testEdgeToSchemaMCQMapper(
          this.templateData
        );
        //console.log('mapper1 ', this.templateData);

        // }
      } else if (dataStr.charAt(1) === '<') {
        //console.log('<<-- FOUND TESTEDGE QUESTION XML-->> ');
        this.templateData = this.utilityConfigService.convertToJson(
          this.templateData.value
        );
      } else if ('meta' in this.templateData) {
        let templateType = this.templateData.data.questions[0].data.type;

        if (
          templateType === 'MCQ' ||
          templateType === 'SCQ' ||
          templateType === 'mcq-tf'
        ) {
          this.templateData = this.utilityConfigService.studyToSchemaMCQMapper(
            this.templateData
          );
        } else if (templateType === 'OPENENDEDSTEMONLY') {
          if ('meta' in this.templateData) {
            this.templateData = this.utilityConfigService.studiToOpenEndedMapper(
              this.templateData
            );
          }
        }
      } else {
        this.templateData.reference = this.templateData.reference + 'preview';
        this.templateData = this.templateData;
      }
      // }
      //
      this.questionEditorService.getSubmitAnsShow().subscribe(state => {
        this.showAnsState = state;
      });
      this.loadTemplate(this.templateData);
      console.log('mode 1', this.mode, this.questionType, this.templateData);
      if (this.templateData.data) {
        this.templateType = this.templateData.data.type;
      }
    }
  }

  changeShowAns() {
    //console.log('submit clicked', this.showAnsState);
    this.showAnsState = !this.showAnsState;
    this.questionEditorService.updateSubmitAnsShow(this.showAnsState);
  }

  loadTemplate(templateData) {
    console.log('templateData ', templateData);

    let componentTemplate: any;
    if (templateData.questionDetails) {
      componentTemplate = this.componentHashmap[
        templateData.questionDetails.type
      ];
      templateData = templateData.questionDetails;
    } else if (templateData.data.type) {
      componentTemplate = this.componentHashmap[templateData.data.type];
    } else {
      componentTemplate = this.componentHashmap[templateData.type];
    }
    console.log('componentTemplate', componentTemplate);

    this.vcRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
      componentTemplate
    );

    // const viewContainerRef = this.vcRef.viewContainerRef;

    const componentRef = this.vcRef.createComponent<any>(componentFactory);

    componentRef.instance.templateData = templateData;
    componentRef.instance.previewState = this.previewState;
    componentRef.instance.submit = this.submit;
    componentRef.instance.save = this.save;
    componentRef.instance.metadataSidebar = this.metadata;
    componentRef.instance.viewDevice = this.viewDevice;
    componentRef.instance.layoutView = this.layoutView;
    if (componentRef.instance.allowSubmit) {
      componentRef.instance.allowSubmit.subscribe(bool => {
        this.allowSubmit = bool;
        if (!bool) {
          this.questionEditorService.updateSubmitAnsShow(bool);
        }
        this.allowSubmitButton.emit(bool);
        //console.log('allowSubmitButton ', bool);
      });
    }

    if (componentRef.instance.correctAnswerCheck) {
      componentRef.instance.correctAnswerCheck.subscribe(value => {
        this.correctAnswer = value;
        this.correctAnswerCheck.emit(value);
        //console.log('correctAnswerCheck ', value);
      });
    }
  }

  dismiss() {
    this.dismissDialog.emit();
    // this.questionEditorService.closeDialog();
  }
}
