import { async, TestBed } from '@angular/core/testing';
import { QbTemplatePreviewModule } from './qb-template-preview.module';

describe('QbTemplatePreviewModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [QbTemplatePreviewModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(QbTemplatePreviewModule).toBeDefined();
  });
});
