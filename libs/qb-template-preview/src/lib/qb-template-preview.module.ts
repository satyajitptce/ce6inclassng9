import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QbTemplatePeviewComponent } from './components/qb-template-peview/qb-template-peview.component';
import { NbCardModule } from '@nebular/theme';

@NgModule({
  imports: [CommonModule, NbCardModule],
  exports: [QbTemplatePeviewComponent],
  declarations: [QbTemplatePeviewComponent]
})
export class QbTemplatePreviewModule {}
