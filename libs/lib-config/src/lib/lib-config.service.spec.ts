import { TestBed } from '@angular/core/testing';

import { LibConfigService } from './lib-config.service';

describe('LibConfigService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LibConfigService = TestBed.get(LibConfigService);
    expect(service).toBeTruthy();
  });
});
