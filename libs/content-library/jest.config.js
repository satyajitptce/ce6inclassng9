module.exports = {
  name: 'content-library',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/content-library',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
