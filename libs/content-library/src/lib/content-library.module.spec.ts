import { async, TestBed } from '@angular/core/testing';
import { ContentLibraryModule } from './content-library.module';

describe('ContentLibraryModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ContentLibraryModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(ContentLibraryModule).toBeDefined();
  });
});
