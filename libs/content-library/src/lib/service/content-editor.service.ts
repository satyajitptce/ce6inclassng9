import { Injectable } from '@angular/core';
import {
  RequestApiService,
  Resource,
  ApiResource,
  CurriculumUtils,
  ResourceRequestType,
  AppConfigService
} from '@tce/core';
import { FilterResourceType } from '../../../../core/src/lib/enums/filter-resource-type.enum';
import { ResourceType } from '../../../../core/src/lib/enums/resource-type.enum';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { title } from 'process';
import { ToastrService } from 'ngx-toastr';

interface SelectedResource {
  name: FilterResourceType;
  image: string;
  type: ResourceType[];
  selectedResourceList: Resource[];
}
@Injectable({
  providedIn: 'root'
})
export class ContentEditorService {
  defaultQuizList = false;
  editPlaylistQuiz = false;
  editPlaylistGallery = false;
  defaultGallery = false;
  assetsQuestion: any[];
  topicId: any;
  customResourceData;
  resourceRequestType: ResourceRequestType.ASSETS;
  public currentPlanningList = new Subject();
  public currentPlanningList$ = this.currentPlanningList.asObservable();
  public currentPlanningResourceData = new Subject();
  public currentPlanningResourceData$ = this.currentPlanningResourceData.asObservable();
  public currentPlanningResourceEvent = new Subject();
  
  // private updatePlanningResourceData = new Subject();
  // public updatePlanningResourceData$ = this.updatePlanningResourceData.asObservable();

  public currentPlanningResourceEvent$ = this.currentPlanningResourceEvent.asObservable();
  public addChapterTopic = new Subject();
  public addChapterTopic$ = this.addChapterTopic.asObservable();

  private updateContentData = new Subject();
  public updateContentData$ = this.updateContentData.asObservable();

  private updateChapterTopic = new Subject();
  public updateChapterTopic$ = this.updateChapterTopic.asObservable();
  private deleteChapter = new Subject();
  public deleteChapter$ = this.deleteChapter.asObservable();


  private updateTopic = new Subject();
  public updateTopic$ = this.updateTopic.asObservable();
  private deleteTopic = new Subject();
  public deleteTopic$ = this.deleteTopic.asObservable();

  private currentChapterNumber = new Subject();
  public currentChapterNumber$ = this.currentChapterNumber.asObservable();

  public openSearchLib = new Subject();
  public openSearchLib$ = this.openSearchLib.asObservable();
  public selectedResourceData = new Subject();
  public selectedResourceData$ = this.selectedResourceData.asObservable();
  private getResources = new Subject();
  public getResources$ = this.getResources.asObservable();
  private visibleChapterTopicData = new Subject();
  public visibleChapterTopicData$ = this.visibleChapterTopicData.asObservable();
  public sequencePlanningCurrentData = new Subject();
  public sequencePlanningCurrentData$ = this.sequencePlanningCurrentData.asObservable();
  public chooseFileResource = new Subject();
  public chooseFileResource$ = this.chooseFileResource.asObservable();
  public chooseFileResourceList = new Subject();
  public chooseFileResourceList$ = this.chooseFileResourceList.asObservable();
  public removeFormResource = new Subject();
  public removeFormResource$ = this.removeFormResource.asObservable();

  private _resourceList: SelectedResource[] = [
    {
      name: FilterResourceType.VIDEO,
      image: this.appConfigService.getResourceThumbnailIcon(
        'tcevideo',
        'animation',
        'icon'
      ),
      type: [
        ResourceType.INTERACTIVITY,
        ResourceType.TCEVIDEO,
        ResourceType.VIDEO
      ],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.AUDIO,
      image: this.appConfigService.getResourceThumbnailIcon(
        'audio',
        'tce-audio',
        'icon'
      ),
      type: [ResourceType.AUDIO],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.WORKSHEETS,
      image: this.appConfigService.getResourceThumbnailIcon(
        'worksheet',
        'worksheet',
        'icon'
      ),
      type: [ResourceType.WORKSHEET],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.GALLERY,
      image: this.appConfigService.getResourceThumbnailIcon(
        'default',
        'gallery',
        'icon'
      ),
      type: [ResourceType.GALLERY],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.UNSUPPORT,
      image: this.appConfigService.getResourceThumbnailIcon(
        'default',
        'unsupport',
        'icon'
      ),
      type: [ResourceType.UNSUPPORT],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.GAME,
      image: this.appConfigService.getResourceThumbnailIcon(
        'tcevideo',
        'tce-games',
        'icon'
      ),
      type: [ResourceType.GAME],
      selectedResourceList: []
    },
    {
      name: FilterResourceType.QUIZ,
      image: this.appConfigService.getResourceThumbnailIcon(
        'quiz',
        'quiz',
        'icon'
      ),
      type: [ResourceType.QUIZ],
      selectedResourceList: []
    }
  ];
  constructor(
    private requestApiService: RequestApiService,
    private http: HttpClient,
    private appConfigService: AppConfigService,
    private toastr:ToastrService
  ) {}

  getCurrentResource(data: any) {
    // console.log("data-->",data)
    this.getCustomResources(data)

    setTimeout(() => {
      
      this.topicId = data.currentTopicId;
    const newURL = this.requestApiService
      .getUrl('tpResource')
      .replace(
        '@ids@',
        'gradeId=' +
          data.currentGradeId +
          '&&subjectId=' +
          data.currentSubjectId +
          '&&ids=' +
          data.currentTopicId
      );
      
    if (newURL) {
      this.http
        .get<any>(newURL)
        .pipe(
          catchError(err => {
            throw err;
          })
        )
        .subscribe(response => {
          if (response && response[0]) {
            //  console.log("1-->",JSON.parse(response[0].playlistJson),this.customResourceData)
            this.getResourcesPlaylist(JSON.parse(response[0].playlistJson),this.customResourceData);
           // this.planningContentResource(response[0].playlistJson);
          }
          else{
          // console.log("resonse--> ",response,"this.customResourceData--> ",this.customResourceData)

            if(this.customResourceData && response.length === 0){
              this.getResourcesPlaylist(response,this.customResourceData);
            }
          }
        });
    }
    }, 500);
    
  }

  getCustomResources(data){
    const customShared = this.requestApiService
    .getUrl('shareCustomResource')
    .replace(
      '@ids@',
      'gradeId=' +
        data.currentGradeId +
        '&&subjectId=' +
        data.currentSubjectId +
        '&&ids=' +
        data.currentTopicId
    );
    if (customShared) {
      this.http.get(customShared).subscribe(response => {
        this.customResourceData = JSON.parse(response[0].playlistJson)
        //  console.log("2-->",this.customResourceData)
      })
  }
}

  setCurrentChapterNumber(seq){
    this.currentChapterNumber.next(seq);
  }

  setUpdateContentData(data){
    this.updateContentData.next(data)
  }

  getResourcesPlaylist(response,cResource){
    //  console.log("tpResource-------->",response)
    //  console.log("cResource-------->",cResource)
    let finalAsset;
    let finalPractice;
    let finalPlaylistJson;
    let finalGallery;
    let finalExercise;
    if(cResource === undefined || cResource === null){
      // console.log("1-->")
      finalAsset = response.asset;
      finalPractice = response.practice;
      finalGallery = response.gallery;
      finalExercise = response.exercise
    }
    else if (response === undefined || response === null || response.length === 0){
      // console.log("2-->")
        finalAsset = cResource.asset;
        finalPractice = cResource.practice;
        finalGallery = cResource.gallery;
        finalExercise = cResource.exercise;

    }
    //  if(response === undefined || response === null){
    //   finalAsset = cResource.asset;
    //   finalPractice = cResource.practice;
    //   finalGallery = cResource.gallery;
    // }
    else{
      // console.log("3-->")
      if(cResource.asset !== null && cResource.asset !== undefined ){
      if( response.asset !== null && response.asset !== undefined){
      finalAsset = response.asset.concat(cResource.asset);

      }
    }
      else{
      finalAsset = response.asset;

      }
      
      if(cResource.practice !== null && cResource.practice !== undefined){
        if(response.practice !== null && response.practice !== undefined){
      finalPractice = response.practice.concat(cResource.practice);

      }}
      else{
        finalPractice = response.practice;
      }
      if(cResource.exercise !== null && cResource.exercise !== undefined){
        if(response.exercise !== null && response.exercise !== undefined){
          finalExercise = response.exercise.concat(cResource.exercise);

      }}
      else{
        finalExercise = response.exercise;
      }
      if(cResource.gallery !== undefined  && cResource.gallery !== null){
      if(response.gallery !== undefined  && response.gallery !== null){
      finalGallery = response.gallery.concat(cResource.gallery);

      }}
      else{
        finalGallery = response.gallery;
      }

      //finalAsset = response.asset.concat(cResource.asset);
      //finalPractice = response.practice.concat(cResource.practice);
      //finalGallery = response.gallery.concat(cResource.gallery);
    }
    
    finalPlaylistJson = {
      creationDate: response.creationDate,
      asset: finalAsset,
      practice: finalPractice,
      gallery:finalGallery,
      exercise:finalExercise
      //practice:response.practice,
      //gallery:response.gallery
    }
  //  console.log("FINAL finalPlaylistJson---------->",finalPlaylistJson)
   this.planningContentResource(JSON.stringify(finalPlaylistJson));
  }



setCurrentResources(resource){
  if (resource && resource.length > 0) {
    this.currentPlanningResourceData.next(resource);
  }
}

  planningContentResource(playlistJson: string, isFromVtp = false) {
    let resources: Resource[] = [];
    this.editPlaylistQuiz = false;
    this.defaultQuizList = false;
    this.editPlaylistGallery = false;
    this.defaultGallery = false;
    if (playlistJson) {
      const playlist = JSON.parse(playlistJson);
      //console.log('setResourcesFromPlaylistJson--->playlist', playlist);
      //resources.push(this.getUnsupport());
      if (playlist.asset && playlist.asset.length > 0) {
        playlist.asset.forEach((resourceItem: ApiResource) => {
          if (resourceItem.assets_type === 'gallery') {
            this.editPlaylistGallery = true;
            this.defaultGallery = false;
            if (playlist.gallery && playlist.gallery.length > 0) {
              resources.push(this.getImageResource(playlist.gallery));
            }
          }
          if (resourceItem.assets_type === 'practice') {
            this.editPlaylistQuiz = true;
            this.defaultQuizList = false;
            if (playlist.practice && playlist.practice.length > 0) {
              resources.push(this.getQuizData(playlist.practice));
            }
          } else {
            if (
              !isFromVtp &&
              resourceItem.assets_type !== 'practice' &&
              resourceItem.assets_type !== 'gallery'
            ) {
              this.defaultGallery = true;
              this.defaultQuizList = true;
              const resource = new Resource(resourceItem);
              resources.push(resource);
            }
          }
        });
      } else {
        if (playlist.practice && playlist.practice.length > 0) {
          resources.push(this.getQuizData(playlist.practice));
        }
        if (playlist.gallery && playlist.gallery.length > 0) {
          resources.push(this.getImageResource(playlist.gallery));
        }
      }

      if (resources.length > 0) {
        resources = CurriculumUtils.removeDuplicateResources(resources);
        const resourceArrayTemp = [...resources];
        resources = resources.filter(item => {
          if (
            item.resourceType &&
            item.resourceType.toLowerCase() === 'worksheet' &&
            item.metaData.subType !== 'handout'
          ) {
            if (item.metaData.ansKeyId) {
              const matchingAnswerKeyResource = resourceArrayTemp.find(
                resource => resource.resourceId === item.metaData.ansKeyId
              );
              item.metaData.answerKeyResource = matchingAnswerKeyResource;
              return item;
            } else {
              return item;
            }
          } else {
            return item;
          }
        });
      }

      if (this.defaultQuizList && !this.editPlaylistQuiz) {
        if (playlist.practice && playlist.practice.length > 0) {
          resources.push(this.getQuizData(playlist.practice));
        }
      }
      if (!this.editPlaylistGallery && this.defaultGallery) {
        if (playlist.gallery && playlist.gallery.length > 0) {
          resources.push(this.getImageResource(playlist.gallery));
        }
      }
      if (resources && resources.length > 0) {
        this.currentPlanningResourceData.next(resources);
        return resources;
        //this.resources = resources; 
      }
    }
  }

  setCurrentPlanningResourceData(resources){
    this.currentPlanningResourceData.next(resources);

  }

  getImageResource(data) {
    //console.log('TCL: Topic -> getImageResource -> data', data);
    let currentImage: any;

    // data.map(item => {
    //   item['filePath'] = `${item.encryptedFilePath}/${item.fileName}`;
    // });

    currentImage = new Resource({
      title: data[0].title,
      assetType: 'gallery',
      subType: 'gallery',
      mimeType: 'gallery',
      assetId: data[0].assetId,
      tpId: data[0].tpId,
      metaData: data,
      fileName: '',
      thumbFileName: '',
      encryptedFilePath: ''
    });

    return currentImage;
  }
  getQuizData(data) {
    let currentQuiz: any;
    let quizQuestionIds = [];
    quizQuestionIds = quizQuestionIds.concat(data);
    if (this.resourceRequestType === ResourceRequestType.ASSETS) {
      if (this.assetsQuestion) {
        this.assetsQuestion.map(assetQuestion => {
          quizQuestionIds.push(assetQuestion.id);
        });
      }
    }
    if (quizQuestionIds && quizQuestionIds.length > 0) {
      const quizId = 'quiz-' + this.topicId;
      currentQuiz = new Resource({
        title: 'Play Quiz',
        assetType: 'quiz',
        subType: 'quiz',
        mimeType: 'quiz',
        assetId: quizId,
        tpId: quizId,
        metaData: { questionIds: quizQuestionIds },
        fileName: '',
        thumbFileName: '',
        encryptedFilePath: ''
      });
    }
    return currentQuiz;
  }

  setSelectedResourceData(data) {
    if (data === null) {
      this.removeAllData(this._resourceList);
    }
    if (data != null && data && data.length > 0) {
      for (
        let resourcesIndex = 0;
        resourcesIndex < data.length;
        resourcesIndex++
      ) {
        for (
          let resourceListIndex = 0;
          resourceListIndex < this._resourceList.length;
          resourceListIndex++
        ) {
          if (
            this._resourceList[resourceListIndex].type.includes(
              data[resourcesIndex].resourceType
            )
          ) {
            this._resourceList[resourceListIndex].selectedResourceList.push(
              data[resourcesIndex]
            );
          }
        }
      }
      if (this._resourceList && this._resourceList.length > 0) {
        this.selectedResourceData.next(this._resourceList);
        // console.log("ContentEditorService -> planningContentResource -> this._resourceList", this._resourceList)
      }
    }
  }

  removeAllData(resource) {
    let removeDataList: any = [];
    for (let index = 0; index < resource.length; index++) {
      resource[index].selectedResourceList = [];
      removeDataList.push(resource[index]);
    }
    if (removeDataList && removeDataList.length > 0) {
      this.selectedResourceData.next(removeDataList);
    }
  }

  setGetResources(resource) {
    // console.log(
    //   'ContentEditorService -> setGetResources -> resource',
    //   resource
    // );
    this.getResources.next(resource);
  }

  setVisibleChapterTopicData(val) {
    this.visibleChapterTopicData.next(val);
  }

  updateCustomBookJSON(data) {
   // console.log('ContentEditorService -> updateCustomBookJSON -> data', data);
    this.http
      .post<any>(this.requestApiService.getUrl('saveCustomBookJSON'), data)
      .subscribe(
        status => {
          // console.log(
          //   'ContentEditorService -> updateCustomBookJSON -> status',
          //   status
          // );
        },
        (error: HttpErrorResponse) => {
          let errormessage: any;
          if (error && error.error['errorMessage']) {
            errormessage = error.error['errorMessage'];
          } else {
            errormessage = 'updateResourceError';
          }

          //this.toastrService.error(errormessage);
        }
      );
  }

  editCustomTopic(data:any){
    if (data) {
    
      this.http
        .put<any>(
          this.requestApiService.getUrl('editCustomTopic'),
          data
        )
        .subscribe(res => {
          if(res){
          this.toastr.success('Topic Updated Successfully')
            this.updateTopic.next(data);
          }
        });
    }

  }

  deleteCustomTopic(data:any){

    //console.log("deleteCustomTopic id",data)
    if(data){
      this.http
      .delete<any>(
        this.requestApiService.getUrl('deleteCustomTopic') + '/' + data.data.currentTopicId
      ).subscribe(res => {
        //console.log(res);
        if(res){
          this.deleteTopic.next(data);
          this.toastr.success('Topic deleted successfully.')
        }
      });
      
    }
  }

  deleteCustomChapter(data:any){
    //console.log("deleteID",data.data.currentChapterId)
    if(data){
      this.http
      .delete<any>(
        this.requestApiService.getUrl('deleteCustomChapter') + '/' + data.data.currentChapterId
      ).subscribe(res => {
       // console.log(res);
        if(res){
          this.toastr.success('Chapter Deleted Successfully')
          this.deleteChapter.next(data);
        }
      });
      
    }
  }



  editCustomChapter(data: any) {

    if (data) {
      this.http
        .put<any>(
          this.requestApiService.getUrl('editCustomChapter'),
          data
        )
        .subscribe(res => {
          if(res){
            this.toastr.success('Chapter Updated Successfully')
            this.updateChapterTopic.next(data);
          }
        });
    }
  }

  postAddChapterTopic(data: any) {
    // console.log(
    //   'ContentEditorService -> postAddChapterTopic -> data',
    //   data.parentChapterTopic.type
    // );
    if (data) {
      if (data.parentChapterTopic.type === 'chapter') {
        let newChapterData = {
          gradeId: data.currentGradeID,
          subjectId: data.currentSubjectID,
          title: data.currentChapterTopic
        };
        if (newChapterData) {
          this.http
            .post<any>(
              this.requestApiService.getUrl('addCustomChapter'),
              newChapterData
            )
            .subscribe(
              status => {
                // console.log(
                //   'ContentEditorService -> postAddChapterTopic -> status',
                //   status
                // );
                if (status) {
                  this.toastr.success('Chapter Added Successfully')
                  let newCustomChapter: any = {
                    gradeId: data.currentGradeID,
                    subjectId: data.currentSubjectID,
                    parentChapterId: data.parentChapterTopic.currentChapterId,
                    currentChapterId: status.id,
                    currentChapterTitle: data.currentChapterTopic,
                    sequence: data.parentChapterTopic.sequence + 1,
                    type: 'chapter',
                    visible: 1,
                    isShare: 0,
                    custom: 1
                  };
                  if (newCustomChapter) {
                    this.addChapterTopic.next(newCustomChapter);
                  }
                }
              },
              (error: HttpErrorResponse) => {}
            );
        }
      }
      if (data.parentChapterTopic.type === 'topic') {
        let newTopicData: any = {
          chapterId: data.parentChapterTopic.currentChapterId,
          gradeId: data.currentGradeID,
          subjectId: data.currentSubjectID,
          title: data.currentChapterTopic
        };
       // console.log('newTopicData', newTopicData);
        if (newTopicData) {
          this.http
            .post<any>(
              this.requestApiService.getUrl('addCustomTopic'),
              newTopicData
            )
            .subscribe(
              status => {
                if (status) {
                  this.toastr.success('Topic Added Successfully')
                  let newCustomTopic = {
                    currentChapterId: data.parentChapterTopic.currentChapterId,
                    parentTopicID: data.parentChapterTopic.currentTopicId,
                    currentTopicId: status.id,
                    currentTopicTitle: data.currentChapterTopic,
                    sequence: data.parentChapterTopic.sequence + 1,
                    type: 'topic',
                    visible: 1,
                    isShare: 0,
                    custom: 1
                  };
                  if (newCustomTopic) {
                    this.addChapterTopic.next(newCustomTopic);
                  }
                }
              },
              (error: HttpErrorResponse) => {}
            );
        }
      }
    }
  }
}
