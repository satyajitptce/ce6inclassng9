import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContentEditorComponent } from './component/content-editor/content-editor.component';
import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';
import {
  NbTreeGridModule,
  NbAccordionModule,
  NbCardModule,
  NbIconModule,
  NbDialogModule,
  NbSelectModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { TreeMenuComponent } from './component/tree-menu/tree-menu.component';
import { AddChapterTopicComponent } from './component/add-chapter-topic/add-chapter-topic.component';
import { AddResourceComponent } from './component/add-resource/add-resource.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { FormResourceComponent } from './component/form-resource/form-resource.component';
import { CoreModule } from '@tce/core';
@NgModule({
  imports: [
    CommonModule,
    NbCardModule,
    NbDialogModule.forRoot(),
    NbTreeGridModule,
    NbSelectModule,
    NbAccordionModule,
    NbEvaIconsModule,
    NbIconModule,
    FormsModule,
    ReactiveFormsModule,
    AngularSvgIconModule,
    DragDropModule,
    CoreModule,
    LibConfigModule.forChild(ContentEditorComponent)
  ],
  declarations: [
    ContentEditorComponent,
    TreeMenuComponent,
    AddChapterTopicComponent,
    AddResourceComponent,
    FormResourceComponent
  ],
  exports: [
    ContentEditorComponent,
    TreeMenuComponent,
    AddChapterTopicComponent,
    AddResourceComponent,
    FormResourceComponent
  ],
  entryComponents: [
    ContentEditorComponent,
    TreeMenuComponent,
    AddChapterTopicComponent,
    AddResourceComponent,
    FormResourceComponent
  ]
})
export class ContentLibraryModule {}
