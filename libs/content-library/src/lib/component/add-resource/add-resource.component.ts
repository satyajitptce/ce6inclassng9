import {
  Component,
  OnInit,
  IterableDiffer,
  IterableDiffers,
  ElementRef,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  AfterContentChecked,
  ViewChild
} from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
import { ContentEditorService } from '../../service/content-editor.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'tce-add-resource',
  templateUrl: './add-resource.component.html',
  styleUrls: ['./add-resource.component.scss']
})
export class AddResourceComponent implements OnInit {
  addResoueceSubscription: Subscription = new Subscription();
  @ViewChild('widgetsContent', { read: ElementRef })
  public widgetsContent: ElementRef<any>;
  resourceList: any;
  constructor(
    protected ref: NbDialogRef<AddResourceComponent>,
    private contentEditorService: ContentEditorService
  ) {}

  ngOnInit(): void {
    this.addResoueceSubscription.add(
      this.contentEditorService.chooseFileResource$.subscribe((state: any) => {
        if (!state) {
          this.dismiss();
        }
      })
    );
  }

  dismiss() {
    this.ref.close();
  }

  destroySearch() {
    this.dismiss();
    this.contentEditorService.openSearchLib.next(false);
  }
  public scrollRight(): void {
    console.log(this.widgetsContent);
    this.widgetsContent.nativeElement.scrollTo({
      left: this.widgetsContent.nativeElement.scrollLeft - 150,
      behavior: 'smooth'
    });
  }

  public scrollLeft(): void {
    console.log(this.widgetsContent);
    this.widgetsContent.nativeElement.scrollTo({
      left: this.widgetsContent.nativeElement.scrollLeft + 150,
      behavior: 'smooth'
    });
  }
}
