import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef,
  HostListener,
  ViewChild
} from '@angular/core';
import { Subscription } from 'rxjs';
import { ContentEditorService } from '../../service/content-editor.service';
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from '@angular/cdk/drag-drop';
import { AppConfigService, CommonService, CurriculumPlaylistService, FileUploadService, FullClassSelection, FullContentSelection, Resource, SubjectSelectorService } from '@tce/core';
@Component({
  selector: 'tce-tree-menu',
  templateUrl: './tree-menu.component.html',
  styleUrls: ['./tree-menu.component.scss']
})
export class TreeMenuComponent implements OnInit {
  @Input() title: any;
  @ViewChild('myItem') accItem;
  clickMoreOption = false;
  selectedTopic: any;
  _children: any[];

  openResource = false;

  showEdit = false;
  showTopicEdit = false;
  clickOutFlyout;

  crrentTopicHeaderElement;


  editMode = false;
  currentEdit;
  showFlyout = false;
  currentEditor = false;
  currentTopicEditor = false;
  editActive = 'not-active';
  editChapterIndex;
  editTopicChapterIndex;
  currentFlyoutEdit;
  selectedTopicItem;
  updatedChapterData;
  updatedTopicData;
  currentResource;
  resourceFly = false;
  resourceId;
  currentGradeId;
  currentSubjectId;
  currentTopicId;
  currentChapterId;

  edit_id = -1;
  topicedit_id = -1;
  isTopicLoading = false;

  @Input('children')
  set children(val: any) {
    //console.log('TreeMenuComponent -> setchildren -> val', val);
    if (val) {
      val.sort(function(a, b) {
        return a.data.sequence - b.data.sequence;
      });
      this._children = val;
    } else {
      this._children = [];
    }
  }
  get children() {
    return this._children;
  }
  categoryName: any;
  @Output() getCurrentSelection = new EventEmitter();
  customContentEditViewSubscription: Subscription = new Subscription();
  @Input() level: number;
  @Input() label: string;
  @Input() parent: any;
  @Input() currentGradeID;
  @Input() currentSubjectID;
  
  _selectedChapterTopicInput: any;
  @Input('selectedChapterTopicInput')
  set selectedChapterTopicInput(val) {
    this._selectedChapterTopicInput = val;
    this.selectedChapterTopic = val;
  }
  get selectedChapterTopicInput() {
    return this._selectedChapterTopicInput;
  }
  addtopicClick = false;

  self = this;
  check: boolean;
  selectedChapterTopic: any;
  selected: any = {};
  topicChaptervalue;
  gradeSubjectvalue;
  chapterNumber;
  currentResources;
  constructor(
    private contentEditorService: ContentEditorService,
    private cdf: ChangeDetectorRef,
    private appConfigService: AppConfigService,
    private commonService:CommonService,
    private subjectSelectorService:SubjectSelectorService,
    private curriculumPlaylistService:CurriculumPlaylistService,
    private fileUploadService: FileUploadService

  ) {
    //console.log(this.children);
  }
  @HostListener('document:click', ['$event'])
  clickout(event) {
    //console.log("id->",event.target.id)
    if( event.target.id === "chpId" ){
      this.commonService.setFlyoutMode(true)
       this.showTopicEdit = false;  
       this.resourceFly = false;

    }

    else if(event.target.id === "tpId" ){
      this.showEdit = false;
      this.resourceFly = false;
      this.commonService.setFlyoutMode(true)
    }
    else if(event.target.id === "rsId"){
      this.resourceFly = true;
    }
    else{
      this.commonService.setFlyoutMode(false)
      this.resourceFly = false;

    }
  }

  ngOnInit(): void {
    //console.log("this._children",this._children)
    this.commonService.getFlyout$.subscribe(flag=>{
      this.clickOutFlyout = flag
      if(!flag){
        this.showTopicEdit = false;
        this.showEdit = false;
    }
    })

    this.contentEditorService.currentChapterNumber$.subscribe(data=>{
      this.chapterNumber = data;

    })

    this.contentEditorService.removeFormResource$.subscribe(data=>{
      //console.log("RESOURCE --- ADDDED --data--",data);
      //this.toggle()
    })

    this.contentEditorService.currentPlanningResourceData$.subscribe(
      (data: any) => {
        this.currentResources = data;
        //console.log('currentResources', this.currentResources);
        // this.currentTopicresourceArrayList(data);
      }
    )

    this.contentEditorService.currentPlanningResourceEvent$.subscribe(event=>{
      let data;
       data = event;
      this.currentGradeId = data.currentGradeId;
      this.currentSubjectId = data.currentSubjectId;
      this.currentTopicId = data.currentTopicId;
      this.currentChapterId = data.currentChapterId;
      
    })

    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.gradeSubjectvalue =
            newClassSelection.gradeLevel.title +
            ' | ' +
            newClassSelection.subject.title;
        }
      }
    );
  }
  currentSelection(data) {
    //console.log('TreeMenuComponent -> currentSelection -> data', data);
    this.getCurrentSelection.emit(data);
  }
  select(i) {
   
    this.currentEdit = i;
    if (this.edit_id === -1 || this.edit_id === i) {
    } else if (i !== this.edit_id && !this.showEdit) {
      this.showEdit = false;
    } else if (i !== this.edit_id) {
      this.showEdit = !this.showEdit;
    }
    this.edit_id = i;
    this.showEdit = !this.showEdit;
    if(this.clickOutFlyout && this.showEdit){
      this.showTopicEdit = false;
    }
  }

  selectTopic(i) {
    
    this.selectedTopicItem = i;
    if (this.topicedit_id === -1 || this.topicedit_id === i) {
    } else if (i !== this.topicedit_id && !this.showTopicEdit) {
      this.showTopicEdit = false;
    } else if (i !== this.topicedit_id) {
      this.showTopicEdit = !this.showTopicEdit;
    }
    this.topicedit_id = i;
    this.showTopicEdit = !this.showTopicEdit;

    if(this.clickOutFlyout && this.showTopicEdit){
      this.showEdit = false
    }
  }
  isActive(type, item) {
    return this.selected[type] === item;
  }
  editChapter(i, value) {
    this.editChapterIndex = i;
    this.showEdit = !this.showEdit;
    this.updatedChapterData = value.data.name;
    ///console.log('editValue', this.updatedChapterData);
  }
  editTopic(i,data) {
    this.editTopicChapterIndex = i;
    this.showTopicEdit = !this.showTopicEdit
    this.updatedTopicData = data.data.name
    //console.log('TopicValue', data);
    
  }
  deleteChapter(value) {
    //console.log('deletechapter', value);
    this.showEdit = !this.showEdit;
    this.contentEditorService.deleteCustomChapter(value);
  }
  deleteTopic(value) {
    //console.log('deleteTopic', value);
    this.showTopicEdit = !this.showTopicEdit
    this.contentEditorService.deleteCustomTopic(value);
  }

  updateTopic(data){
    this.currentTopicEditor = false;
    this.editTopicChapterIndex = -1;
    //console.log("updatedTopic",this.updatedTopicData)
    let editTopicData = {
      tpId: data.data.currentTopicId,
      title: this.updatedTopicData
    };
    //console.log('editTopicValue', editTopicData);
    this.contentEditorService.editCustomTopic(editTopicData);
  }

  update(item) {
    //this.editMode = true;
    this.currentEditor = false;
    this.editChapterIndex = -1;
    let editChapter = {
      title: this.updatedChapterData,
      chapterId: item.data.currentChapterId
    };
    //console.log('updateeditValue', editChapter);

    this.contentEditorService.editCustomChapter(editChapter);
  }

  cancelUpdate(){
    this.editChapterIndex = -1;
    this.editTopicChapterIndex = -1;
   
  }
  openMoreOption(i) {
    //console.log(i);
  }
  getCurrentData(eventData) {
    // console.log('TreeMenuComponent -> getCurrentData -> eventData', eventData);
    //this.categoryName = eventData.name
    this.contentEditorService.currentPlanningResourceEvent.next(eventData);
  }

 
 getCurrentResources(resources){
   this.currentResource = resources
  // let resource = resources.resourceData
  //console.log("--getCurrentResources-->>")
  }
  resourceMoreOption(id){
    //console.log("id-------->",id)
    //console.log("id2-------->",id.data.resourceData.resourceId)

    this.resourceFly = !this.resourceFly;
    this.resourceId = id
  }
  saveChapterData(item){
    if(item.data.type === "chapter"){
      this.contentEditorService.setCurrentChapterNumber(item.data.sequence)
      //console.log("saveChapterData------>",this.chapterNumber)
    }
  }

  resourceOption(type,resource?,data?){
      //console.log("resourceOption-->",type)

    //console.log(resource)
    if(type === 'cancel'){
      this.resourceFly = !this.resourceFly;
    }
    else if(type === 'edit'){
      this.commonService.setEditResource(type)

      this.topicChaptervalue =
      this.chapterNumber +
      '.' +
      data.data.sequence +
      ' | ' +
      data.data.name;
      const customData = {
        title: resource.title,
        grade: this.gradeSubjectvalue,
        chapter: this.topicChaptervalue,
        file: resource.fileName,
        share: resource.isShared,
        //type: 'resource-playlist',
        type: 'resource-playlist-CL',
        assetId: resource.resourceId,
        customResource: resource
      };

     
      //console.log("customData-->",customData)
  
      this.fileUploadService.setAddResourceFormData(customData);
      this.fileUploadService.setAddResourceFlagBroadcaster('create');
    }
    else if(type === 'delete'){
      //console.log("resourceOption-->2",type,resource)

     this.selectedDeleteResource(resource)
    }

  }

  selectedDeleteResource(resource) {
    //console.log("delete---->",resource)
    for (let index = 0; index < this.currentResources.length; index++) {
      if (this.currentResources[index].resourceId === resource.resourceId) {
        this.currentResources[index].visibility = 0;
        //this.handleResourceCloseClick(event, this.resources[index]);
        this.curriculumPlaylistService.setAvailableResources(this.currentResources)
        this.currentResources.splice(index,1) 
      }
    
    }
    const customData = {
      tpId: resource.tpId,
      gradeId: this.currentGradeId,
      subjectId: this.currentSubjectId,
      chapterId: this.currentChapterId,
      assetId: resource.resourceId,
      title: resource.title
    };

    this.fileUploadService.deleteResource(customData, this.currentResources);

  }


  selectedChapterTopicEvent(id: string, event: Event) {
    this.selectedChapterTopic = id;
    // console.log(
    //   'TreeMenuComponent -> selectedChapterTopicEvent -> this.selectedChapterTopic',
    //   this.selectedChapterTopic
    // );
  }

  getCancelEvent(event) {
    this.selectedChapterTopic = '';
  }

  openSearchLib(resources) {
    //console.log(resources);
    this.contentEditorService.openSearchLib.next(true);
    this.contentEditorService.setGetResources(resources);
    //console.log("OPEN SEACH LIB--->>")
  }

  

  openModule(id: any) {
    //console.log('TreeMenuComponent -> openModule -> id', id);
  }

  drop(event: CdkDragDrop<string[]>) {
    // console.log('previousIndex', event.previousIndex);
    //console.log('event.previousContainer.data', event);
    // console.log('currentIndex', event.currentIndex);
    // console.log('event.container.data', event.container.data);
    if (event.previousContainer === event.container) {
      //console.log('If');
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
      if (event.currentIndex != event.previousIndex) {
        let newSequenceData = {
          type: event.item.data.data.type,
          chapterId: event.item.data.data.currentChapterId || '',
          topicId: event.item.data.data.currentTopicId || '',
          data: event.container.data
        };
        if (newSequenceData) {
          this.contentEditorService.sequencePlanningCurrentData.next(
            newSequenceData
          );
        }
      }
    } else {
      //console.log('else');
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }


  selectedVisible(data, type, event) {
    this.showTopicEdit = !this.showTopicEdit;
    this.showEdit = !this.showEdit;
    //console.log('topic', data);
    // console.log('topic', type);
    //console.log('topic', event);
    //event.stopPropagation();
    let newData = {
      currentData: data,
      currentType: type
    };
    this.contentEditorService.setVisibleChapterTopicData(newData);
    //console.log("TreeMenuComponent -> selectedVisible -> data", data)
  }

  public getResourceThumbnailIcon(resource: Resource, type: string) {
    this.getCurrentResources(resource);
    // console.log(
    //   'TreeMenuComponent -> getResourceThumbnailIcon -> resource',
    //   resource
    // );
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }
  selectCurrentTopicEvent(id) {
    //console.log("openResource",id)
    this.openResource = !this.openResource;
    setTimeout(() => {
      this.selectedTopic = id;  
      //console.log("--selectCurrentTopicEvent-->>",id)
      
    }, 500);
  }
  toggle() {
    this.accItem.toggle();
  }
}
