import { Component, OnInit, Input } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { CurriculumPlaylistService, FileUploadService } from '@tce/core';
import { ContentEditorService } from '../../service/content-editor.service';
import { Subscription } from 'rxjs';
@Component({
  selector: 'tce-form-resource',
  templateUrl: './form-resource.component.html',
  styleUrls: ['./form-resource.component.scss']
})
export class FormResourceComponent implements OnInit {
  formResoueceSubscription: Subscription = new Subscription();
  _customResource: any;
  seperateTagList: any;
  filteredMenu: any;
  currentGradeId: any;
  currentSubjectId: any;
  @Input('customResource')
  set customResource(val) {
    console.log('FormResourceComponent -> setcustomResource -> val', val);
    if (val) {
      this._customResource = val;
    }
  }
  get customResource() {
    return this._customResource;
  }
  addResourceOptionsForm: FormGroup = new FormGroup({});
  constructor(
    private fb: FormBuilder,
    private contentEditorService: ContentEditorService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private fileUploadService: FileUploadService
  ) {
    this.addResourceOptionsForm = fb.group({
      resourceName: ['', [Validators.required]],
      resourceType: ['', [Validators.required]],
      resourcePrivacy: ['', [Validators.required]],
      description: ['', [Validators.required]],
      resourceTags: ['']
    });
  }

  ngOnInit(): void {
    this.formResoueceSubscription.add(
      this.curriculumPlaylistService.filteredMenu$.subscribe(
        (filteredMenu: any) => {
          // console.log(
          //   'FormResourceComponent -> ngOnInit -> filteredMenu',
          //   filteredMenu
          // );
          this.filteredMenu = filteredMenu;
        }
      )
    );
  }
  get f() {
    return this.addResourceOptionsForm.controls;
  }
  seperateTag() {
    if (this.addResourceOptionsForm.value.resourceTags) {
      let myArray = this.addResourceOptionsForm.value.resourceTags.split(
        /[ ,]+/
      );
      if (myArray) {
        this.seperateTagList = myArray;
      }
    }
  }

  removTag(value) {
    this.seperateTagList = this.seperateTagList.filter(a => a != value);
  }
  onSubmit() {
   
    let chapter = {
      topic: {
        topicId: this.customResource.currentChapterTopicSubject.currentTopicId
      },
      chapter: {
        chapterId: this.customResource.currentChapterTopicSubject
          .currentChapterId
      }
    };
    let subject = {
      gradeLevel: {
        id: this.customResource.currentChapterTopicSubject.currentGradeId
      },
      subject: {
        subjectId: this.customResource.currentChapterTopicSubject
          .currentSubjectId
      }
    };
    //  console.log(
    //   'this.addResourceOptionsForm.value',
    //   chapter
    // );
    if (chapter && subject) {
      this.fileUploadService.createCustomResource(
        chapter,
        subject,
        this.customResource.item,
        0,
        this.addResourceOptionsForm.value.resourceName,
        'planMode'
      );
      this.closeResource(this.customResource.item.name);
      this.contentEditorService.openSearchLib.next(false);

    }
  }
  
  closeResource(name) {
    this.contentEditorService.removeFormResource.next(name);
  }

  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  //(keyup)="updateFieldValue('resourceName', resourceName.value)"
  updateFieldValue(Name: string, val) {
    let format = /[*\<>:'"\\|,.\/?]+/;
    console.log("val-->",val)

    if(val){
      if (format.test(this.addResourceOptionsForm.value.resourceName)) {
        this.addResourceOptionsForm.controls[Name].setErrors({'incorrect': true});
        // console.log("True")
      } else {
        // return this.chapterTopicForm.controls;
        // console.log("False")
        this.addResourceOptionsForm.controls[Name].setValue(val);

  
      }
    }
    
  }

  
}
