import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { CurriculumPlaylistService, CommonService } from '@tce/core';
import { Subscription } from 'rxjs';
import {
  NbTreeGridDataSource,
  NbTreeGridDataSourceBuilder,
  NbDialogRef,
  NbDialogService
} from '@nebular/theme';
import { ContentEditorService } from '../../service/content-editor.service';
import { LibConfigService } from '@tce/lib-config';
import { SearchFilterService } from '../../../../../search/src/lib/service/search-filter.service';
import { SearchViewComponent } from '../../../../../search/src/lib/component/search-view/search-view.component';
import { AddResourceComponent } from '../add-resource/add-resource.component';
import { error } from 'console';
import { ToastrService } from 'ngx-toastr';
interface TreeNode<T> {
  data: T;
  children?: TreeNode<T>[];
  expanded?: boolean;
  resource?: [];
}

interface FSEntry {
  name: string;
  type: string;
  currentChapterId?: string;
  currentTopicId?: string;
  currentSubjectId?: string;
  custom?: any;
  sequence?: any;
  visible?: any;
  isShare?: any;
}
@Component({
  selector: 'tce-content-editor',
  templateUrl: './content-editor.component.html',
  styleUrls: ['./content-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ContentEditorComponent implements OnInit, OnDestroy {
  @ViewChild('searchResult', { static: false, read: ViewContainerRef })
  searchResult: ViewContainerRef | undefined;

  _selectedContaentData: any;
  dataSource: NbTreeGridDataSource<any>;
  data: TreeNode<FSEntry>[];
  myPlanningData: any;
  customColumn = 'name';
  defaultColumns = [];
  allColumns = [this.customColumn, ...this.defaultColumns];
  currentChapterId: any;
  currentTopicId: any;
  showSearchPanel: any = false;
  contentEditViewSubscription: Subscription = new Subscription();
  currentSubject: any;
  currentPlanningResource: any;
  currentSubjectID: any;
  currentGradeId: any;
  customResourceList: any;
  currentChapterResource: any;
  @Input('selectedContaentData')
  set selectedContaentData(value) {
    this.currentSubject = value.currentSubjectId.title;

    this._selectedContaentData = value;
    this.currentGradeId = value.currentGradeId.id;
    this.currentSubjectID = value.currentSubjectId.subjectId;
    this.loadDefaultData(value);
  }
  get selectedContaentData() {
    return this._selectedContaentData;
  }
  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private dataSourceBuilder: NbTreeGridDataSourceBuilder<any>,
    private contentEditorService: ContentEditorService,
    private cdr: ChangeDetectorRef,
    private libConfigService: LibConfigService,
    private searchFilterService: SearchFilterService,
    private dialogService: NbDialogService,
    private commonService: CommonService,
    private toastrService: ToastrService

  ) {
    // this.data = [{data:{currentSubjectId: "sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b",
    // name: "Mathematics",
    // type: "subject"}}]
    // this.currentSubjectID= "sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b";
   
    // console.log(
    //   'ContentEditorComponent -> setselectedContaentData -> value',
    //   this.data,
    //   this.currentSubjectID
    // );
    this.dataSource = this.dataSourceBuilder.create(this.data);
    // console.log(
    //   'ContentEditorComponent -> constructor -> this.dataSource',
    //   this.dataSource
    // );
  }

  ngOnInit(): void {
    
    this.contentEditViewSubscription.add(
      this.curriculumPlaylistService.selectedSubjectChapterPlanningList$.subscribe(
        currentPlanningData => {
          //console.log('currentPlanningData', currentPlanningData);
          this.selectedContaentData = {
            currentGradeId: currentPlanningData.currentGradeId,
            currentSubjectId: currentPlanningData.currentSubjectId,
            currentChapterTopic: currentPlanningData.cueerntChapterTopic
          };
        }
      )
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.currentPlanningResourceData$.subscribe(
        (data: any) => {
          this.currentChapterResource = data;
          //console.log('resourceArray', data);
          this.currentTopicresourceArrayList(data);
        }
      )
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.currentPlanningResourceEvent$.subscribe(
        (data: any) => {
//          console.log('selectedContaentData', this.selectedContaentData);
          //console.log('ContentEditorComponent -> ngOnInit -> data', data);

          this.customEvent(data);
        }
      )
    );
   
    this.contentEditViewSubscription.add(
      this.contentEditorService.addChapterTopic$.subscribe((newData: any) => {
        //console.log('ContentEditorComponent -> ngOnInit -> newData', newData);
        if (newData) {
          this.chapterTopicArray(newData);
        }
      })
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.updateChapterTopic$.subscribe(
        (updatedData: any) => {
          
          if (updatedData) {
            this.updateChapterTopicArray(updatedData);
          }
        }
      )
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.updateTopic$.subscribe((updatedData: any) => {
        
        if (updatedData) {
          this.updateTopicArray(updatedData);
        }
      })
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.deleteChapter$.subscribe(
        (deleteChapter: any) => {
          
          if (deleteChapter) {
            this.deleteChapter(deleteChapter);
          }
        }
      )
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.deleteTopic$.subscribe((deleteTopic: any) => {
        
        if (deleteTopic) {
          this.deleteTopic(deleteTopic);
        }
      })
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.openSearchLib$.subscribe((flag: any) => {
        this.showSearchPanel = flag;
        //console.log('ContentEditorComponent -> ngOnInit -> flag', flag);
        if (flag) {
          if (this.searchResult) {
            this.searchResult.clear();
          }
          this.loadSearchLib();
        } else {
          if (this.searchResult) {
            this.searchResult.clear();
          }
        }
      })
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.getResources$.subscribe((data: any) => {
        if (data) {
          //console.log('getResources', data);
          this.currentPlanningResource = data;
        }
      })
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.visibleChapterTopicData$.subscribe(
        (data: any) => {
          //console.log('ContentEditorComponent -> ngOnInit -> data', data);
          //console.log('this.data', this.data);
          if (this.data && this.data[0]) {
            this.visibleChapterTopicArray(data);
          }
        }
      )
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.sequencePlanningCurrentData$.subscribe(
        (data: any) => {
          if (data) {
            this.sequenceDataArray(data);
          }
        }
      )
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.chooseFileResourceList$.subscribe(
        (data: any) => {
          //console.log('chooseFileResourceList', data);
          if (data) {
            this.customResourceList = data;
          }
        }
      )
    );
    this.contentEditViewSubscription.add(
      this.contentEditorService.chooseFileResource$.subscribe((state: any) => {
        if (state) {
          this.openPopup();
        }
      })
    );

    this.contentEditViewSubscription.add(
      this.contentEditorService.removeFormResource$.subscribe((data: any) => {
        //console.log('ContentEditorComponent -> ngOnInit -> data', data);
        if (data) {
          if (this.customResourceList) {
            // console.log(
            //   'before-> this.customResourceList',
            //   this.customResourceList
            // );
            this.customResourceList = this.customResourceList.filter(
              // a => a.item.name != data
              function(value, index, arr) {
                return value.item.name != data;
              }
            );
            // console.log(
            //   'after-> this.customResourceList',
            //   this.customResourceList
            // );

            if (
              this.customResourceList &&
              this.customResourceList.length === 0
            ) {
              this.contentEditorService.chooseFileResource.next(false);
            } else {
              this.contentEditorService.chooseFileResource.next(false);
              this.openPopup();
              this.contentEditorService.chooseFileResource.next(true);
            }
          }
        }
      })
    );

    this.contentEditViewSubscription.add(
    this.contentEditorService.updateContentData$.subscribe(data=>{
      if(data === 'savaJson'){
        this.saveUpdateJson()
      }
    })
    );

    this.contentEditViewSubscription.add(
      this.commonService.newCustomResource$.subscribe(data => {
      //console.log('ContentEditorComponent -> ngOnInit -> newCustomResource', data);

        // if( this.currentChapterResource){
        //   this.currentChapterResource =  this.currentChapterResource
        // }else{
        //   this.currentChapterResource =[]
        // }
        this.currentChapterResource.push(data);
        //console.log('this.currentChapterResource', this.currentChapterResource);
        this.currentTopicresourceArrayList(this.currentChapterResource);
        this.reArrangResource(this.currentChapterResource);
      })
    );
  }
  getCurrentData(eventData) {
    //console.log("---getCurrentData---", eventData);
    this.contentEditorService.currentPlanningResourceEvent.next(eventData);
  }
  customEvent(data: any) {
    //console.log("customEvent data.type-->>",data.type)
    //this.dataSource.filter(data.name);
    if (data.type === 'topic') {
      this.currentChapterId = data.currentChapterId;
      this.currentTopicId = data.currentTopicId;
      data.currentGradeId = this.selectedContaentData.currentGradeId.id;
      data.currentSubjectId = this.selectedContaentData.currentSubjectId.subjectId;
      this.myPlanningData = data;
      //console.log('ContentEditorComponent -> customEvent -> this.myPlanningData',this.myPlanningData);
      this.contentEditorService.currentPlanningList.next(data);
      this.contentEditorService.getCurrentResource(data);
      //this.curriculumPlaylistService
      //console.log("ContentEditorComponent -> customEvent -> currentResourec", currentResourec)
    }

    //console.log(data);
  }

  loadSearchLib() {
    this.libConfigService
      .getComponentFactory<SearchViewComponent>('lazy-search')
      .subscribe({
        next: componentFactory => {
          if (!this.searchResult) {
            return;
          }
          const ref = this.searchResult.createComponent(componentFactory);
          ref.instance['type'] = 'planningMode';
          ref.instance['searchViewState'] = 'filterView';
          ref.instance['currentSubject'] = this.currentSubject;
          ref.instance['currentPlanningData'] = this.myPlanningData;
          ref.instance[
            'currentPlanningResource'
          ] = this.currentPlanningResource.resource;
          ref.instance[
            'currentChapterTopic'
          ] = this.currentPlanningResource.data;
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  dismiss() {
    //this.ref.close();
  }
  saveUpdateJson() {
    console.log('this.data', this.data);
    //console.log('selectedContaentData', this.selectedContaentData);
    if (this.data) {
      let createCustomJSON = {
        sequence: {
          id: this.data[0].data.currentSubjectId,
          label: this.data[0].data.name,
          type: 'category',
          node: []
        },
        property: []
      };
      if (
        this.data &&
        this.data[0].children &&
        this.data[0].children.length > 0
      ) {
        let proCustomJSOn: any;
        for (let index = 0; index < this.data[0].children.length; index++) {
          let customChildrenJSON = {
            id: this.data[0].children[index].data.currentChapterId,
            label: this.data[0].children[index].data.name,
            type: 'chapter',
            sequence:
              this.data[0].children[index].data.sequence === undefined
                ? index + 1
                : this.data[0].children[index].data.sequence,
            visible:
              this.data[0].children[index].data.visible === undefined
                ? 1
                : this.data[0].children[index].data.visible,
            custom:
              this.data[0].children[index].data.custom === undefined
                ? this.data[0].children[index].data.currentChapterId.startsWith(
                    'chp'
                  )
                  ? 0
                  : 1
                : this.data[0].children[index].data.custom,
            isShare:
              this.data[0].children[index].data.isShare === undefined
                ? 0
                : this.data[0].children[index].data.isShare,
            node: []
          };
          if (customChildrenJSON && customChildrenJSON.custom === 1) {
            proCustomJSOn = {
              id: customChildrenJSON.id,
              label: customChildrenJSON.label,
              type: customChildrenJSON.type,
              node: []
            };
          }

          if (customChildrenJSON) {
            if (
              this.data[0].children[index].children &&
              this.data[0].children[index].children.length > 0
            ) {
              for (
                let subIndex = 0;
                subIndex < this.data[0].children[index].children.length;
                subIndex++
              ) {
                let subCustomJSON = {
                  id: this.data[0].children[index].children[subIndex].data
                    .currentTopicId,
                  label: this.data[0].children[index].children[subIndex].data
                    .name,
                  type: 'teaching point',
                  sequence:
                    this.data[0].children[index].children[subIndex].data
                      .sequence === undefined
                      ? subIndex + 1
                      : this.data[0].children[index].children[subIndex].data
                          .sequence,
                  visible:
                    this.data[0].children[index].children[subIndex].data
                      .visible === undefined
                      ? 1
                      : this.data[0].children[index].children[subIndex].data
                          .visible,
                  custom:
                    this.data[0].children[index].children[subIndex].data
                      .custom === undefined
                      ? this.data[0].children[index].children[
                          subIndex
                        ].data.currentTopicId.startsWith('tp')
                        ? 0
                        : 1
                      : this.data[0].children[index].children[subIndex].data
                          .custom,
                  isShare:
                    this.data[0].children[index].children[subIndex].data
                      .isShare === undefined
                      ? 0
                      : this.data[0].children[index].children[subIndex].data
                          .isShare
                };
                //console.log("this.data[0].children[index].data.custom",this.data[0].children[index].data.custom)
                if (customChildrenJSON.custom === 1) {
                  let ProSubCustomJSON = {
                    id: this.data[0].children[index].children[subIndex].data
                      .currentTopicId,
                    label: this.data[0].children[index].children[subIndex].data
                      .name,
                    type: 'teaching point'
                  };
                  if (proCustomJSOn) {
                    proCustomJSOn.node.push(ProSubCustomJSON);
                  }
                }
                if (subCustomJSON) {
                  customChildrenJSON.node.push(subCustomJSON);
                }
              }
            }
            if (customChildrenJSON.custom === 1) {
              createCustomJSON.property.push(proCustomJSOn);
            }
            createCustomJSON.sequence.node.push(customChildrenJSON);
          }
        }
        if (createCustomJSON && createCustomJSON.sequence.node) {
          let newJSON = {
            gradeId: this.selectedContaentData.currentGradeId.id,
            subjectId: this.selectedContaentData.currentSubjectId.subjectId,
            bookId: this.selectedContaentData.currentSubjectId.books.bookId,
            bookContent: JSON.stringify(createCustomJSON)
          };
          if (newJSON) {
            this.contentEditorService.updateCustomBookJSON(newJSON);
            //console.log('createCustomJSON', createCustomJSON);
            this.toastrService.success('Content Saved Successfully');
            this.curriculumPlaylistService.getMyEbook(this.selectedContaentData.currentSubjectId);

            // localStorage.setItem(
            //   this.selectedContaentData.currentSubjectId.books.bookId,
            //   JSON.stringify(createCustomJSON)
            // );
          }

        }
      }
    }
  }
  reArrangResource(currentResourceList) {
    // console.log(
    //   'ContentEditorComponent -> reArrangResource -> currentResourceList',
    //   currentResourceList
    // );
    let newResourceList = {};
    let newCollectionAssets: any = [];
    let newCollectionPractice: any;
    let newCollectionGallery: any;
    let newCreationDate = Math.round(new Date().getTime() / 1000);
    let playlistJson = {
      creationDate: newCreationDate,
      asset: newCollectionAssets,
      gallery: newCollectionGallery,
      practice: newCollectionPractice
    };

    for (let index = 0; index < currentResourceList.length; index++) {
      if (currentResourceList[index].tcetype === 'quiz') {
        newCollectionAssets.push({ assets_type: 'practice' });
        newCollectionPractice = currentResourceList[index].metaData.questionIds;
        playlistJson.practice = newCollectionPractice;
      }
      if (currentResourceList[index].tcetype === 'gallery') {
        newCollectionAssets.push({ assets_type: 'gallery' });
        newCollectionGallery = currentResourceList[index].metaData;
        playlistJson.gallery = newCollectionGallery;
      }
      if (
        currentResourceList[index].tcetype !== 'quiz' &&
        currentResourceList[index].tcetype !== 'gallery'
      ) {
        newCollectionAssets.push(currentResourceList[index].metaData);
      }
    }
    if (playlistJson) {
      let string = JSON.stringify(playlistJson, function(k, v) {
        if (v !== undefined) {
          return v;
        }
      });
      newResourceList = {
        gradeId: this.currentGradeId,
        subjectId: this.currentSubjectID,
        playlistJson: {
          playlistJson: string,
          id: this.currentTopicId
        }
      };
      if (newResourceList) {
        //console.log('newResourceList', newResourceList);
        //get tp call comment thiru
        //this.curriculumPlaylistService.UpdateResourceList(newResourceList);
      }
      //
      // this.currentTpoicResource.resources = this.resources;
      // this.curriculumPlaylistService.setTopicSelection(
      //   this.currentTpoicResource
      // );
    }

    // this.curriculumPlaylistService.setAvailableResources(this.resources);
  }
  openPopup() {
    this.open(false, false);
  }
  protected open(closeOnBackdropClick: boolean, closeOnEsc: boolean) {
    this.dialogService.open(AddResourceComponent, {
      context: { resourceList: this.customResourceList },
      closeOnBackdropClick,
      closeOnEsc
    });
  }

  updateChapterTopicArray(upDatedTopic) {
    //console.log('Before Edit', this.data);
    //console.log(upDatedTopic);
    for (let index = 0; index < this.data[0].children.length; index++) {
      if (
        this.data[0].children[index].data.currentChapterId ===
        upDatedTopic.chapterId
      ) {
        this.data[0].children[index].data.name = upDatedTopic.title;

        //console.log('got the id', this.data[0].children[index].data.name);
        // if (this.data[0].children[0].data.name) {
        //   this.data[0].children.splice(++index, 0, newchapterData);}
        // else {
        // this.data[0].children[0] = newchapterData;
      }
    }
    //console.log('data', this.data);
  }
  updateTopicArray(upDatedTopic) {
    //console.log(upDatedTopic);
    for (let index = 0; index < this.data[0].children.length; index++) {
      for (let j = 0; j < this.data[0].children[index].children.length; j++) {
        //console.log("check id",this.data[0].children[index].children[j].data.currentTopicId)
        if (
          this.data[0].children[index].children[j].data.currentTopicId ===
          upDatedTopic.tpId
        ) {
          this.data[0].children[index].children[j].data.name =
            upDatedTopic.title;

          // console.log(
          //   'got the id',
          //   this.data[0].children[index].children[j].data.name
          // );
        }
      }
    }
    //console.log('data', this.data);
  }

  deleteTopic(data) {
    // console.log("deleteTopic--> ",data);
    for (let index = 0; index < this.data[0].children.length; index++) {
    // console.log("this.data[0].children--> ",this.data[0].children);

      for (let j = 0; j < this.data[0].children[index].children.length; j++) {
        if (
          this.data[0].children[index].children[j].data.currentTopicId ===
          data.data.currentTopicId
        ) {
        // console.log("check id",this.data[0].children[index].children[j].data.currentTopicId)
          //console.log("delete Topic id",data.data.currentTopicId)
          let deleteId = this.data[0].children[index].children[j]
        // console.log('delete this Topic id',deleteId);
        const deleteTopicId = this.data[0].children[index].children.indexOf(deleteId);
        // console.log("deleteTopic index",deleteTopicId)
        this.data[0].children[index].children.splice(deleteTopicId, 1);


         
        }
      }
    }
    //  console.log('data', this.data);
  }
  deleteChapter(data) {
    //console.log(data.data.currentChapterId);
    for (let index = 0; index < this.data[0].children.length; index++) {
      if (
        this.data[0].children[index].data.currentChapterId === data.data.currentChapterId
      ) {
        let deleteId = this.data[0].children[index]
        //console.log('delete the id',deleteId);
        const deleteChapterId = this.data[0].children.indexOf(deleteId);
        this.data[0].children.splice(deleteChapterId, 1);
       // this.data[0].children[index].data.name = data.title;

        //console.log('delete the id',deleteChapterId);

        // if (this.data[0].children[0].data.name) {
        //   this.data[0].children.splice(++index, 0, newchapterData);}
        // else {
        // this.data[0].children[0] = newchapterData;
      }
    }
    //console.log('data', this.data);
  }

  currentTopicresourceArrayList(data) {
   // console.log("currentTopicresourceArrayList",data)
    let resourceArray = [];

    for (let index = 0; index < data.length; index++) {
      resourceArray.push({
        data: {
          name: data[index].title,
          type: 'resource',
          resourceData: data[index]
        }
      });
    }
    if (resourceArray && resourceArray.length > 0) {
      for (
        let childrenIndex = 0;
        childrenIndex < this.data[0].children.length;
        childrenIndex++
      ) {
        if (
          this.data[0].children[childrenIndex].data.currentChapterId ===
          this.currentChapterId
        ) {
          for (
            let resourceIndex = 0;
            resourceIndex <
            this.data[0].children[childrenIndex].children.length;
            resourceIndex++
          ) {
            if (
              this.data[0].children[childrenIndex].children[resourceIndex].data
                .currentTopicId === this.currentTopicId
            ) {
              this.data[0].children[childrenIndex].children[
                resourceIndex
              ].resource = data;
              this.data[0].children[childrenIndex].children[
                resourceIndex
              ].children = resourceArray;
              //console.log(this.data);
              this.cdr.detectChanges();
            } else {
              // this.data[0].children[childrenIndex].children[resourceIndex].children =[]
            }
          }
        }
      }
    }
  }

  loadDefaultData(value) {
    let children = [];
    let subchildren = [];
    let subResourceList = [];
    this.data = [
      {
        data: {
          name: value.currentSubjectId.title,
          type: 'subject',
          currentSubjectId: value.currentSubjectId.subjectId
        },
        children: children,
        expanded: false
      }
    ];
    if (value.currentChapterTopic && value.currentChapterTopic.length > 0) {
      for (
        let childrenIndex = 0;
        childrenIndex < value.currentChapterTopic.length;
        childrenIndex++
      ) {
        children.push({
          data: {
            name: value.currentChapterTopic[childrenIndex].chapterTitle,
            currentChapterId:
              value.currentChapterTopic[childrenIndex].chapterId,
            type: 'chapter',
            custom:
              value.currentChapterTopic[childrenIndex].custom === undefined
                ? value.currentChapterTopic[childrenIndex].chapterId.startsWith(
                    'chp'
                  )
                  ? 0
                  : 1
                : value.currentChapterTopic[childrenIndex].custom,
            sequence:
              value.currentChapterTopic[childrenIndex].sequence === undefined
                ? childrenIndex + 1
                : value.currentChapterTopic[childrenIndex].sequence,
            visible:
              value.currentChapterTopic[childrenIndex].visible === undefined
                ? 1
                : value.currentChapterTopic[childrenIndex].visible,
            isShare:
              value.currentChapterTopic[childrenIndex].isShare === undefined
                ? 0
                : value.currentChapterTopic[childrenIndex].isShare
          },
          children: subchildren
        });
        let tempArray = [];
        for (
          let subChildrenIndex = 0;
          subChildrenIndex <
          value.currentChapterTopic[childrenIndex].topics.length;
          subChildrenIndex++
        ) {
          if (
            value.currentChapterTopic[childrenIndex].chapterTitle ===
            value.currentChapterTopic[childrenIndex].topics[subChildrenIndex]
              .chapterName
          ) {
            // console.log(
            //   'topicList',
            //   value.currentChapterTopic[childrenIndex].topics[subChildrenIndex]
            // );
            tempArray.push({
              data: {
                name:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].topicTitle,
                type: 'topic',
                currentChapterId:
                  value.currentChapterTopic[childrenIndex].chapterId,
                currentTopicId:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].topicId,
                custom:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].custom === undefined
                    ? value.currentChapterTopic[childrenIndex].topics[
                        subChildrenIndex
                      ].topicId.startsWith('tp')
                      ? 0
                      : 1
                    : value.currentChapterTopic[childrenIndex].topics[
                        subChildrenIndex
                      ].custom,
                sequence:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].sequence === undefined
                    ? subChildrenIndex + 1
                    : value.currentChapterTopic[childrenIndex].topics[
                        subChildrenIndex
                      ].sequence,
                visible:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].visible === undefined
                    ? 1
                    : value.currentChapterTopic[childrenIndex].topics[
                        subChildrenIndex
                      ].visible,
                isShare:
                  value.currentChapterTopic[childrenIndex].topics[
                    subChildrenIndex
                  ].isShare === undefined
                    ? 0
                    : value.currentChapterTopic[childrenIndex].topics[
                        subChildrenIndex
                      ].isShare
              },
              children: [],
              resource: []
            });
            if (tempArray && tempArray.length > 0) {
              this.data[0]['children'][childrenIndex]['children'] = tempArray;
            }
          }
        }
      }
    } else {
      const loadchapterData: any = {
        data: {
          name: '',
          type: 'chapter',
          currentChapterId: 'loadChapter',
          isShare: 0,
          visible: 1,
          sequence: 0,
          custom: 1
        },
        children: []
      };
      this.data[0].children.push(loadchapterData);
    }
    this.cdr.detectChanges();
    //console.log('this.data', this.data);
  }

  chapterTopicArray(newData) {
    if (newData.type === 'chapter') {
      const newchapterData: any = {
        data: {
          name: newData.currentChapterTitle,
          type: 'chapter',
          currentChapterId: newData.currentChapterId,
          custom: newData.custom,
          sequence: newData.sequence,
          visible: newData.visible,
          isShare: newData.isShare
        },
        children: [
          {
            data: {
              name: '',
              type: 'topic',
              currentChapterId: newData.currentChapterId,
              currentTopicId: 'newTopicId',
              custom: 1,
              sequence: 1,
              visible: 1,
              isShare: 0
            },
            children: []
          }
        ]
      };
      if (newchapterData) {
        for (let index = 0; index < this.data[0].children.length; index++) {
          if (
            this.data[0].children[index].data.currentChapterId ===
            newData.parentChapterId
          ) {
            if (this.data[0].children[0].data.name) {
              this.data[0].children.splice(++index, 0, newchapterData);
            } else {
              this.data[0].children[0] = newchapterData;
            }
          }
        }
      }
    }
    if (newData.type === 'topic') {
      const newTopicData: any = {
        data: {
          name: newData.currentTopicTitle,
          type: 'topic',
          currentChapterId: newData.currentChapterId,
          currentTopicId: newData.currentTopicId,
          custom: newData.custom,
          sequence: newData.sequence,
          visible: newData.visible,
          isShare: newData.isShare
        },
        children: []
      };
      if (newTopicData) {
        //console.log("newTopicData",newTopicData)
        let forLoop = true;
        for (
          let topicIndex = 0;
          topicIndex < this.data[0].children.length;
          topicIndex++
        ) {
          //console.log("this.data[0].children[topicIndex].data.currentChapterId",this.data[0].children[topicIndex].data.currentChapterId)
          if (
            this.data[0].children[topicIndex].data.currentChapterId ===
            newData.currentChapterId
          ) {
            for (
              let subTopicIndex = 0;
              subTopicIndex < this.data[0].children[topicIndex].children.length;
              subTopicIndex++
            ) {
              if (this.data[0].children[topicIndex].children[0].data.name) {
                if (forLoop) {
                  this.data[0].children[topicIndex].children.splice(
                    newData.sequence - 1,
                    0,
                    newTopicData
                  );
                  forLoop = false;
                }
              } else {
                this.data[0].children[topicIndex].children[0] = newTopicData;
              }
            }
          }
        }
      }
    }
    this.cdr.detectChanges();
  }

  visibleChapterTopicArray(data) {
    if (data.currentType === 'chapter') {
      for (let index = 0; index < this.data[0].children.length; index++) {
        if (
          this.data[0].children[index].data.currentChapterId ===
          data.currentData.data.currentChapterId
        ) {
          this.data[0].children[index].data.visible = this.data[0].children[
            index
          ].data.visible
            ? 0
            : 1;
          // console.log(
          //   'ContentEditorComponent -> ngOnInit -> this.data[0].children[index].data.visible',
          //   this.data[0].children[index].data.visible
          // );
          if (
            this.data[0].children[index].children &&
            this.data[0].children[index].children.length > 0
          ) {
            for (
              let Childindex = 0;
              Childindex < this.data[0].children[index].children.length;
              Childindex++
            ) {
              this.data[0].children[index].children[
                Childindex
              ].data.visible = this.data[0].children[index].data.visible;
            }
          }
        }
      }
    }
    if (data.currentType === 'topic') {
      for (let index = 0; index < this.data[0].children.length; index++) {
        if (
          this.data[0].children[index].data.currentChapterId ===
          data.currentData.data.currentChapterId
        ) {
          if (
            this.data[0].children[index].children &&
            this.data[0].children[index].children.length > 0
          ) {
            for (
              let Childindex = 0;
              Childindex < this.data[0].children[index].children.length;
              Childindex++
            ) {
              if (
                this.data[0].children[index].children[Childindex].data
                  .currentTopicId === data.currentData.data.currentTopicId
              ) {
                this.data[0].children[index].children[
                  Childindex
                ].data.visible = this.data[0].children[index].children[
                  Childindex
                ].data.visible
                  ? 0
                  : 1;
              }
            }
          }
        }
      }
    }
  }

  sequenceDataArray(data) {
    let newCustomSequenceData: any = [];
    //console.log('ContentEditorComponent -> ngOnInit -> data', data);
    if (data.type === 'resource') {
      let resourceListPlanning: any = [];
      for (
        let resourceIndex = 0;
        resourceIndex < data.data.length;
        resourceIndex++
      ) {
        resourceListPlanning.push(data.data[resourceIndex].data.resourceData);
      }
      if (resourceListPlanning && resourceListPlanning.length > 0) {
        this.reArrangResource(resourceListPlanning);
      }
    }
    if (data.type === 'chapter') {
      for (let index = 0; index < data.data.length; index++) {
        let chapterDataSequence = {
          data: {
            currentChapterId: data.data[index].data.currentChapterId,
            custom: data.data[index].data.custom,
            isShare: data.data[index].data.isShare,
            name: data.data[index].data.name,
            sequence: index + 1,
            type: data.data[index].data.type,
            visible: data.data[index].data.visible
          },
          children: data.data[index].children
        };
        if (chapterDataSequence) {
          newCustomSequenceData.push(chapterDataSequence);
        }
      }
      if (newCustomSequenceData && newCustomSequenceData.length > 0) {
        // console.log(
        //   'ContentEditorComponent -> ngOnInit -> newCustomSequenceData',
        //   newCustomSequenceData
        // );
        this.data[0].children = newCustomSequenceData;
        //console.log('this.data', this.data);
      }
    }
    if (data.type === 'topic') {
      for (let index = 0; index < this.data[0].children.length; index++) {
        if (
          this.data[0].children[index].data.currentChapterId === data.chapterId
        ) {
          newCustomSequenceData = [];
          if (data.data && data.data.length > 0) {
            for (
              let Topicindex = 0;
              Topicindex < data.data.length;
              Topicindex++
            ) {
              let topicDataSequence = {
                data: {
                  currentChapterId: data.data[Topicindex].data.currentChapterId,
                  currentTopicId: data.data[Topicindex].data.currentTopicId,
                  custom: data.data[Topicindex].data.custom,
                  isShare: data.data[Topicindex].data.isShare,
                  name: data.data[Topicindex].data.name,
                  sequence: Topicindex + 1,
                  type: data.data[Topicindex].data.type,
                  visible: data.data[Topicindex].data.visible
                },
                children: data.data[Topicindex].children
              };
              if (topicDataSequence) {
                newCustomSequenceData.push(topicDataSequence);
              }
            }
          }
          if (newCustomSequenceData && newCustomSequenceData.length > 0) {
            this.data[0].children[index].children = newCustomSequenceData;
            //console.log('this.data[0]', this.data[0]);
          }
        }
      }
    }
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.contentEditViewSubscription.unsubscribe();
  }
}
