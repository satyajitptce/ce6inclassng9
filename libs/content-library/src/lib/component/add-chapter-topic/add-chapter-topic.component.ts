import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  FormControl,
  Validators
} from '@angular/forms';
import { KeyboardService, KeyboardState, KeyboardTheme } from '@tce/core';
import { ContentEditorService } from '../../service/content-editor.service';
@Component({
  selector: 'tce-add-chapter-topic',
  templateUrl: './add-chapter-topic.component.html',
  styleUrls: ['./add-chapter-topic.component.scss']
})
export class AddChapterTopicComponent implements OnInit {
  _type: any;
  @Input() currentGradeID;
  @Input() currentSubjectID;
  @Input('type')
  set type(value) {
    this._type = value;
    // console.log("value-----------> ",value)
  }
  get type() {
    return this._type;
  }
  @Output() onCancelEvent = new EventEmitter();
  chapterTopicForm: FormGroup = new FormGroup({});
  // invalidFormat = 'invalid format ( ./:*?"<>|/ )';
  invalidFormat = 'invalid format';

  constructor(
    private fb: FormBuilder,
    private keyboardService: KeyboardService,
    private contentEditorService: ContentEditorService
  ) {
    this.chapterTopicForm = fb.group({
      name: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {}
  submitName() {
    // var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]+/;

    // if (format.test(this.chapterTopicForm.value.name)) {
    // } else {
    //   return this.chapterTopicForm.controls;
    // }
    const newdata = {
      currentSubjectID: this.currentSubjectID,
      currentGradeID: this.currentGradeID,
      parentChapterTopic: this.type,
      currentChapterTopic: this.chapterTopicForm.value.name
    };
    console.log(newdata);
    //this.contentEditorService.addChapterTopic.next(newdata);
    this.contentEditorService.postAddChapterTopic(newdata);
    this.cancel();
  }

  cancel() {
    this.onCancelEvent.emit(true);
    this.closeKeyboard();
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN,
      theme: KeyboardTheme.DEFAULT
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }

  updateFieldValue(Name: string, val) {
    let format = /[*\<>:'"\\|,.\/?]+/;
    console.log("val-->",val)

    if(val){
      if (format.test(this.chapterTopicForm.value.name)) {
        this.chapterTopicForm.controls[Name].setErrors({'incorrect': true});
        // console.log("True")
      } else {
        // return this.chapterTopicForm.controls;
        // console.log("False")
        this.chapterTopicForm.controls[Name].setValue(val);

  
      }
    }
    
  }

  get f() {
    return this.chapterTopicForm.controls;
  }
}
