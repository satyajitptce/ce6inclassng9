import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddChapterTopicComponent } from './add-chapter-topic.component';

describe('AddChapterTopicComponent', () => {
  let component: AddChapterTopicComponent;
  let fixture: ComponentFixture<AddChapterTopicComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AddChapterTopicComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddChapterTopicComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
