module.exports = {
  name: 'player-custom-video',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-custom-video',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
