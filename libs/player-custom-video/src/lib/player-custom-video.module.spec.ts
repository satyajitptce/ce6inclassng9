import { async, TestBed } from '@angular/core/testing';
import { PlayerCustomVideoModule } from './player-custom-video.module';

describe('PlayerCustomVideoModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerCustomVideoModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerCustomVideoModule).toBeDefined();
  });
});
