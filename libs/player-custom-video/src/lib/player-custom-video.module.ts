import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomPlayerComponent } from './component/custom-player/custom-player.component';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';
@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(CustomPlayerComponent),
    CoreModule,
    PlayerAnnotationModule
  ],
  declarations: [CustomPlayerComponent],
  exports: [CustomPlayerComponent],
  entryComponents: [CustomPlayerComponent]
})
export class PlayerCustomVideoModule {}
