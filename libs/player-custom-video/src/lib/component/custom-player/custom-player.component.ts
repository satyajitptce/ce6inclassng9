import {
  Component,
  OnInit,
  AfterViewInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import {
  PlayerAbstractComponent,
  PlayerAbstractInterface,
  PlayerAbstractAnimations,
  ResourceType,
  RequestApiService,
  ScriptLoaderService,
  FileUploadService,
  PlayerUsageService
} from '@tce/core';
declare let videojs;

@Component({
  selector: 'tce-custom-player',
  templateUrl: './custom-player.component.html',
  styleUrls: ['./custom-player.component.scss'],
  animations: PlayerAbstractAnimations
})
export class CustomPlayerComponent extends PlayerAbstractComponent
  implements OnInit, AfterViewInit, PlayerAbstractInterface {
  @ViewChild('video', { static: false })
  public video: ElementRef;
  videopath: any;
  poster: any;
  customControl: any = null;
  viedojsPlayer: any = null;
  constructor(
    private requestApiService: RequestApiService,
    private scriptLoader: ScriptLoaderService,
    private fileUploadService: FileUploadService,
    private playerUsageService: PlayerUsageService
  ) {
    super();
  }
  ngOnInit() {
    //console.log('---- ON NG INIT ----');
    this.fileUploadService.customPlayerFlag$.subscribe(flag => {
      //console.log('ngOnInit -> flag', flag);
      this.removeVideoJS();
    });
    this.beginLoadingResource();
  }
  ngAfterViewInit() {
    //console.log('---- ON NG AFTER INIT ----');
    this.createCustomPlayer();
  }

  get resourceType() {
    return ResourceType;
  }
  onEndLoadingResource(): void {}

  beginLoadingResource() {
    if (this.resource) {
      this.isLoading = false;
      const url = this.requestApiService.getUrl('getFile');
      this.poster = '//d2zihajmogu5jn.cloudfront.net/elephantsdream/poster.png';
      this.videopath = url + '/' + this.resource.metaData.filePath;
      if (this.video && this.video.nativeElement) {
        this.video.nativeElement.src = this.videopath;
        this.video.nativeElement.type = 'video/mp4';
        this.video.nativeElement.load();
      }
    } else {
      this.isLoading = true;
    }
  }

  createCustomPlayer() {
    this.customControl = {
      controls: true,
      autoplay: false,
      preload: 'auto',
      html5: {
        nativeTextTracks: false
      },
      userActions: {
        hotkeys: true
      },
      techOrder: ['html5'],
      textTrackDisplay: {
        allowMultipleShowingTracks: true
      },
      controlBar: {
        volumePanel: false,
        children: [
          'playToggle',
          'volumePanel',
          'currentTimeDisplay',
          'timeDivider',
          'durationDisplay',
          'progressControl',
          'liveDisplay',
          'remainingTimeDisplay',
          'customControlSpacer',
          'playbackRateMenuButton',
          'chaptersButton',
          'descriptionsButton',
          'subtitlesButton',
          'captionsButton',
          'subsCapsButton',
          'audioTrackButton',
          'fullscreenToggle'
        ]
      }
    };
    const playerUsageService = this.playerUsageService;
    const resource = this.resource
    var player = videojs('customPlayer', {}, function() {
      //some code
    });
    player.on("play",function(){
      playerUsageService.logResourceUsage(resource);
    });
    player.on("pause",function(){
      playerUsageService.logResourceUsage(resource);
    });
    player.on("muted",function(){
      playerUsageService.logResourceUsage(resource);
    });
    player.on("volume",function(){
      playerUsageService.logResourceUsage(resource);
    });
    player.on("volumechange",function(){
      playerUsageService.logResourceUsage(resource);
    });
    
    //console.log('this.viedojsPlayer', this.viedojsPlayer);

    /*
    if (!this.viedojsPlayer) {
      this.viedojsPlayer = new videojs(
        'customPlayer',
        this.customControl,
        function onPlayerReady() {
          videojs.log('Your player is ready!');
        }
      );
      console.log('--->this.viedojsPlayer--->', this.viedojsPlayer);
    }
    */
  }

  removeVideoJS() {
    //console.log('removeVideoJS -> this.viedojsPlayer', this.viedojsPlayer);
    if (this.viedojsPlayer != null) {
      if (this.viedojsPlayer.player_ != null) {
        //this.viedojsPlayer.dispose();
      }
    }

    //const oldPlayer = document.getElementById('customPlayer_html5_api');
    //console.log('----->>>> CCCC--->>', oldPlayer)
    // if(oldPlayer != null){

    //   videojs(oldPlayer).dispose();
    // }
  }
}
//new code
