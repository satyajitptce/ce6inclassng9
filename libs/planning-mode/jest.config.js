module.exports = {
  name: 'planning-mode',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/planning-mode',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
