import { async, TestBed } from '@angular/core/testing';
import { PlanningModeModule } from './planning-mode.module';

describe('PlanningModeModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlanningModeModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlanningModeModule).toBeDefined();
  });
});
