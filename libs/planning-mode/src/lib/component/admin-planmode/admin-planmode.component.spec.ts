import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminPlanmodeComponent } from './admin-planmode.component';

describe('AdminPlanmodeComponent', () => {
  let component: AdminPlanmodeComponent;
  let fixture: ComponentFixture<AdminPlanmodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPlanmodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminPlanmodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
