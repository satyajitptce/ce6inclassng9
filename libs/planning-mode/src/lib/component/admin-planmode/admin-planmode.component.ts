import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PlanningModeService } from '../../services/planning-mode.service';

@Component({
  selector: 'tce-admin-planmode',
  templateUrl: './admin-planmode.component.html',
  styleUrls: ['./admin-planmode.component.scss']
})
export class AdminPlanmodeComponent implements OnInit {
  adminMode = false;
  password = '';
  url;
  role;
  username;
  constructor(private planmodeService: PlanningModeService , private httpClient:HttpClient,private route:Router) {
    this.planmodeService.getAdminUrl().subscribe(data => {
      this.url = data.adminUrl[0].url;
      //console.log('url------------', this.url);
    });

    this.planmodeService.getAdminUrl().subscribe(data => {
      this.role = data.adminLink;
      //console.log("roles--------",this.role);
    });
  }

  ngOnInit(): void {
    //console.log(this.password);
  }

  onClickAdmin() {
    this.adminMode = !this.adminMode;
  }
  goToAdmin() {
   
    this.username = localStorage.getItem('username')
      this.adminMode = false;
      //console.log("username",this.username)
      // let url = this.url+ "?"+"user-id"+"="+this.username ; 
      let url = this.url;   
       this.openNewTab(url);
    
  }

 
  
  openNewTab(url) {
    console.log("url-->",url)
    window.open(url);
  }
}
