import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningModeBackgroundComponent } from './planning-mode-background.component';

describe('PlanningModeBackgroundComponent', () => {
  let component: PlanningModeBackgroundComponent;
  let fixture: ComponentFixture<PlanningModeBackgroundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningModeBackgroundComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningModeBackgroundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
