import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ViewContainerRef,
  ViewEncapsulation,
  Input,
  OnChanges,
  AfterViewInit,
  SimpleChanges,
  ComponentRef,
  HostListener,
  ChangeDetectorRef
} from '@angular/core';
import {
  NbSidebarService,
  NbMenuItem,
  NbMenuService,
  NbDialogService
} from '@nebular/theme';
import {
  CommonService,
  SubjectSelectorService,
  CurriculumPlaylistService,
  Chapter,
  GradeLevel,
  ApiSubject,
  UserProfileService,
  FullClassSelection,
  AuthenticationService,
  AuthStateEnum,
  UserState,
  FilterMenu
} from '@tce/core';
import { filter, takeUntil } from 'rxjs/operators';
import { Subject, BehaviorSubject, Subscription, from } from 'rxjs';
import {
  ContentEditorComponent,
  ContentEditorService
} from '@tce/content-library';
import { LibConfigService } from '@tce/lib-config';
import { PlanningModeService } from '../../services/planning-mode.service';
import { UserSettingViewComponent } from '@tce/user-profile';
import { ChangePasswordComponent } from '../../../../../../apps/tce-teacher/src/app/features/user-profile-change-password/change-password/change-password.component';
import { ChangePinComponent } from '../../../../../../apps/tce-teacher/src/app/features/user-profile-change-pin/change-pin/change-pin.component';
//import { PlanningDashboardComponent } from '../../modules/planning-dashboard/planning-dashboard.component';
//import { PlanningAssignmentViewComponent } from '../../../../../assignment/src/lib/planning-assignment/component/planning-assignment-view/planning-assignment-view.component';
import { QuestionBankViewComponent } from '@tce/question-bank';
import { PlanningReportsAnalyticsViewComponent } from 'libs/reports-analytics/src/lib/planning-reports-analytics/component/planning-reports-analytics-view/planning-reports-analytics-view.component';
import { PlanningGradebookViewComponent } from 'libs/gradebook/src/lib/planning-gradebook/component/planning-gradebook-view/planning-gradebook-view.component';
import { PlanningCalendarViewComponent } from 'libs/calendar/src/lib/planning-calendar/component/planning-calendar-view/planning-calendar-view.component';
import { PlanningStudentsViewComponent } from 'libs/students/src/lib/planning-students/component/planning-students-view/planning-students-view.component';
import { QuestionEditorComponent } from 'libs/question-bank/src/lib/components/question-editor/question-editor.component';
import { QuestionEditorViewComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-view/question-editor-view.component';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';
import {
  SignInModuleState,
  SignInState
} from '@app-teacher/models/enums/signInState.enum';
import { LoginPwdComponent } from '@app-teacher/features/login/sign-in/components/login-pwd/login-pwd.component';
import { Router } from '@angular/router';
import { ApplicationMode } from 'libs/core/src/lib/enums/app-mode.enum';
import { QuestionbankserviceService } from 'libs/question-bank/src/lib/services/questionbankservice.service';
import { QuestionEditorService } from '@tce/template-editor';
//import { SupportViewComponent } from '../../modules/support/components/support-view/support-view.component';
// import { OverViewComponent } from '../../modules/over-view/components/over-view/over-view.component';
// import { MyactivityViewComponent } from '../../modules/myactivity/components/myactivity-view/myactivity-view.component';
// import { ManageViewComponent } from '../../modules/manage/components/manage-view/manage-view.component';
// import { MessageViewComponent } from '../../modules/message/components/message-view/message-view.component';
// import { ProductViewComponent } from '../../modules/product/components/product-view/product-view.component';
@Component({
  selector: 'tce-planning-mode-view',
  templateUrl: './planning-mode-view.component.html',
  styleUrls: ['./planning-mode-view.component.scss']
  //encapsulation: ViewEncapsulation.None
})
export class PlanningModeViewComponent implements OnInit, OnDestroy {
  @ViewChild('loginOutlet', { read: ViewContainerRef })
  loginOutlet: ViewContainerRef | undefined;

  @ViewChild('planningContentLib', { static: false, read: ViewContainerRef })
  planningContentLib: ViewContainerRef | undefined;

  @ViewChild('userSettingView', { static: false, read: ViewContainerRef })
  userSettingView: ViewContainerRef | undefined;

  @ViewChild('changePassword', { static: false, read: ViewContainerRef })
  changePassword: ViewContainerRef | undefined;

  @ViewChild('changePin', { static: false, read: ViewContainerRef })
  changePin: ViewContainerRef | undefined;
  inputPrefereView: any = [];
  _isLoggedIn: any;
  YourItems: NbMenuItem[];
  userProfileImg;
  @Input('isLoggedIn')
  set isLoggedIn(val) {
    //console.log('PlanningModeViewComponent -> setisLoggedIn -> val', val);
    this._isLoggedIn = val;
  }
  get isLoggedIn() {
    return this._isLoggedIn;
  }
  _userProfile: any;
  currentMenu = 'Content Library';
  selectedSubject: any;
  selectedGradeLevel: any;
  availableChapters: any;
  selectedChapter: any;
  availableTopics: any;
  contenteditorData: any;
  selectedTopic: any;
  showUserModal = false;
  toggleState = false;
  availableGrades: any;
  availableSubjects: ApiSubject[] = [];
  currentClassSelectionTextMode: any;
  showSubjectChapter = false;
  openAdditionPopUp = false;
  showQuestionEdit = false;
  authChecked = false;
  userStateValue: any;
  isPlusIconRequired = false;
  adminMode = false;
  userRole;
  @Input('userProfile')
  set userProfile(val) {
    this._userProfile = val;
    // console.log("user-profile--> ",val)
  }
  get userProfile() {
    return this._userProfile;
  }
  protected destroy$ = new Subject<void>();
  planningModeViewSubscription: Subscription = new Subscription();
  questionBankRef: any;
  chapterDropdown: boolean;
  topicDropdown: boolean;

  constructor(
    private sidebarService: NbSidebarService,
    private commonService: CommonService,
    private authService: AuthenticationService,
    private menuService: NbMenuService,
    private libConfigService: LibConfigService,
    private subjectSelectorService: SubjectSelectorService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private planningModeService: PlanningModeService,
    private userProfileService: UserProfileService,
    private router: Router,
    private dialogService: NbDialogService,
    private contentEditorService: ContentEditorService,
    private questionBankService: QuestionbankserviceService,
    private questionEditorService: QuestionEditorService,
    private cdr: ChangeDetectorRef
  ) {
    //console.log('current menu', this.currentMenu);
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (event.target.id === 'user-profile') {
      this.showUserModal = true;
    } else {
      this.showUserModal = false;
    }
    if (event.target.id === 'shortcut-menu') {
      this.openAdditionPopUp = true;
    } else {
      //this.showUserModal = false;
      this.openAdditionPopUp = false;
      this.questionBankService.setQuestionEditMode(false);
    }
    if (event.target.id === 'chapterId') {
      this.chapterDropdown = true;
    } else {
      this.chapterDropdown = false;
    }
    if (event.target.id === 'topicId') {
      this.topicDropdown = true;
    } else {
      this.topicDropdown = false;
    }
  }

  ngOnInit(): void {
    //this.checkContenteditorData();
    this.userProfileImg = this.userProfileService.userProfileImg;

    this.authService.currentSignInState$.subscribe(state => {});

    //added by thiru

    this.commonService.classroomModeState$.subscribe(state => {
      if (!state) {
      }
    });

    this.YourItems = [
      {
        title: 'Content Library',
        icon: 'folder-outline',
        selected: true,
        data: [{ plusIconRequired: false }]
      },
      {
        title: 'Question Bank',
        icon: 'home-outline',
        selected: false,
        data: [{ plusIconRequired: true }]
      },
      {
        title: 'Reports & Analytics',
        icon: 'file-text-outline',
        selected: false,
        data: [{ plusIconRequired: false }]
      }
    ];
    this.authService.userState$
      .pipe(
        filter(
          (userState: UserState) =>
            userState.authStatus !== AuthStateEnum.PRE_AUTH
        )
      )
      .subscribe((userState: UserState) => {
        this.authChecked = true;
        if (this.authChecked) {
          this.userStateValue = userState;
          this.postAuthCheck(userState);
        }
      });
    this.userProfile = this.userProfileService.userData;
    this.userRole = this.userProfileService.userRole;
    this.checkForAdminAccess(this.userRole);

    this.planningModeViewSubscription.add(
      this.menuService
        .onItemClick()
        .pipe(takeUntil(this.destroy$))
        .subscribe((data: { tag: string; item: NbMenuItem }) => {
          //console.log(data);
          //console.log(this.planningContentLib);

          if (this.planningContentLib) {
            this.planningContentLib.clear();
          }
          this.currentMenu = data.item.title;
          //console.log("--data.item--",data.item.data[0].plusIconRequired)
          this.isPlusIconRequired = data.item.data[0].plusIconRequired;

          let selectedMenuIndex = 0;
          if (this.YourItems && this.YourItems.length > 0) {
            for (
              let yourItemsIndex = 0;
              yourItemsIndex < this.YourItems.length;
              yourItemsIndex++
            ) {
              if (this.YourItems[yourItemsIndex].title === data.item.title) {
                this.YourItems[yourItemsIndex].selected = true;
              } else {
                this.YourItems[yourItemsIndex].selected = false;
              }
              selectedMenuIndex = yourItemsIndex;
            }
            //this.toggle()
          }
          if (this.currentMenu === 'Question Bank') {
            this.loadQuestionBank();
          }
          if (this.currentMenu === 'Content Library') {
            this.loadContentLib();
          }

          if (this.currentMenu === 'Reports & Analytics') {
            this.loadReportsAndAnalyticsLib();
          }
        })
    );

    this.planningModeService.qbChangeChapterTopicGet().subscribe(change => {
      if (change) {
        if (this.questionBankRef) {
          this.questionBankRef.destroy();
          this.loadQuestionBank();
          this.planningModeService.qbChangeChapterTopicSet(false);
        }
      }
    });

    //this.loadDashboard();
    this.loadContentLib();
  }

  checkForAdminAccess(role) {
    // console.log("checkForAdminAccess",role);

    let roleCheck = role.search('Admin');
    if (roleCheck === -1) {
      this.adminMode = false;
    } else {
      this.adminMode = true;
    }
  }

  saveContentData() {
    this.contentEditorService.setUpdateContentData('savaJson');
  }

  checkContenteditorData() {
    this.planningModeViewSubscription.add(
      this.subjectSelectorService.subjectSelection$.subscribe(subject => {
        //console.log('PlanningModeViewComponent -> subject', subject);
        if (subject) {
          this.selectedSubject = subject;
          this.questionEditorService.setSubject(this.selectedSubject);
        }
      })
    );
    this.planningModeViewSubscription.add(
      this.subjectSelectorService.gradeLevelSelection$.subscribe(gradeLevel => {
        // console.log('PlanningModeViewComponent -> gradeLevel', gradeLevel);
        if (gradeLevel) {
          this.selectedGradeLevel = gradeLevel;
          this.availableSubjects = gradeLevel.subjects;
          this.questionEditorService.setGrade(this.selectedGradeLevel);
        }
      })
    );
    this.planningModeViewSubscription.add(
      this.curriculumPlaylistService.availableChapters$.subscribe(
        (chapters: Chapter[]) => {
          // console.log('PlanningModeViewComponent -> chapters', chapters);
          this.availableChapters = chapters;
          this.questionEditorService.setAllChapters(this.availableChapters);
        }
      )
    );
    this.curriculumPlaylistService.chapterSelection$.subscribe(newChapter => {
      if (newChapter) {
        this.selectedChapter = newChapter;
        console.log('selected chapter ', this.selectedChapter);

        this.availableTopics = newChapter.topics;
        this.questionEditorService.setSelectedChapter(newChapter);
      }
    });

    this.curriculumPlaylistService.topicSelection$.subscribe(newTopic => {
      //console.log('selectedTopic ', newTopic);

      this.selectedTopic = newTopic;
      this.questionEditorService.setSelectedTopic(newTopic);
    });

    this.planningModeViewSubscription.add(
      this.subjectSelectorService.availableGrades$.subscribe(
        (gradeLevels: GradeLevel[]) => {
          this.availableGrades = gradeLevels;
        }
      )
    );

    if (
      this.selectedGradeLevel &&
      this.selectedGradeLevel.id &&
      this.selectedSubject &&
      this.selectedSubject.subjectId &&
      this.availableChapters &&
      this.availableChapters.length > 0
    ) {
      // console.log(
      //   '--this.availableChapters.length--> ',
      //   this.availableChapters.length
      // );
      this.contenteditorData = {
        currentGradeId: this.selectedGradeLevel,
        currentSubjectId: this.selectedSubject,
        currentChapterTopic: this.availableChapters
      };
      // console.log('--this.contenteditorData-->>', this.contenteditorData);
    } else {
    }

    this.planningModeViewSubscription.add(
      this.curriculumPlaylistService.selectedSubjectChapterPlanningList$.subscribe(
        (data: any) => {
          // console.log('PlanningModeViewComponent -> data', data);

          if (data) {
            this.currentClassSelectionTextMode =
              data.currentGradeId.title +
              // (data.currentGradeId.divisions && data.currentGradeId.divisions[0]
              //   ? data.currentGradeId.divisions[0].divisionTitle
              //   : '') +
              '|' +
              data.currentSubjectId.title;
            //console.log('PlanningModeViewComponent -> data', data);
            this.contenteditorData = data;
            // console.log('this.contenteditorData -> data', data);
            this.availableChapters = data.cueerntChapterTopic;
            this.questionEditorService.setAllChapters(this.availableChapters);
            // this.selectedChapter = data.cueerntChapterTopic[0];
            if (data.cueerntChapterTopic.length > 0) {
              this.selectedChapter = data.cueerntChapterTopic[0];
              console.log(
                'selected chapter ',
                this.currentClassSelectionTextMode
              );

              if (this.selectedChapter.topics.length > 0) {
                this.availableTopics = this.selectedChapter.topics;
                this.selectedTopic = this.selectedChapter.topics[0];
                console.log('selected topic ', this.selectedTopic);
              }
              // console.log(
              //   'this.contenteditorData -> data if',
              //   data.cueerntChapterTopic[0],
              //   this.selectedChapter
              // );
              // this.questionBankRef.destroy();
              // this.loadQuestionBank();

              // this.changeChapter(data.cueerntChapterTopic[0]);
              this.changeTopic(this.selectedTopic);
              this.cdr.markForCheck();
            }
          }
        }
      )
    );
    this.planningModeViewSubscription.add(
      this.subjectSelectorService.fullClassSelection$.subscribe(
        (newClassSelection: FullClassSelection) => {
          if (newClassSelection) {
            this.currentClassSelectionTextMode =
              newClassSelection.gradeLevel.title +
              // (newClassSelection.division
              //   ? newClassSelection.division.divisionTitle
              //   : '') +
              ' | ' +
              newClassSelection.subject.title;
          }
        }
      )
    );
    this.planningModeViewSubscription.add(
      // this.userProfileService.userPrefereViewListBroadcast$.subscribe(data => {
      //   this.inputPrefereView = data;
      // })
      this.curriculumPlaylistService.filteredMenu$.subscribe(
        (filteredMenu: FilterMenu[]) => {
          this.inputPrefereView = filteredMenu;
        }
      )
    );
    this.planningModeViewSubscription.add(
      this.planningModeService.userProfileState$.subscribe(state => {
        if (state && state['userView'] && state['userView'] != null) {
          this.showUserModal = false;
          if (this.userSettingView) {
            this.userSettingView.clear();
          }
          if (this.changePassword) {
            this.changePassword.clear();
          }
          if (this.changePin) {
            this.changePin.clear();
          }
          this.loadUserView(state);
        } else {
          if (this.userSettingView) {
            this.userSettingView.clear();
          }
          if (this.changePassword) {
            this.changePassword.clear();
          }
          if (this.changePin) {
            this.changePin.clear();
          }
        }
      })
    );

    if (this.contenteditorData && this.contenteditorData.currentGradeId) {
      //this.loadContentLib();
    }
  }
  toggle(): void {
    this.toggleState = !this.toggleState;
    this.sidebarService.toggle(true, 'left');
  }
  toggleCompact() {
    this.sidebarService.toggle(true, 'right');
  }
  userModal(): void {
    this.showUserModal = !this.showUserModal;

    const userStae = {
      userView: '',
      currentState: ''
    };
    this.planningModeService.setUserProfileState(userStae);
    //this.commonService.setClassroomModeState(true);
  }

  //added by thiru
  postAuthCheck(userState: UserState) {
    const { authStatus, previousAuthStatus, firstTimeUserStatus } = userState;
    if (authStatus !== AuthStateEnum.AUTH) {
      this.loadSignIn();
    }
  }

  openPopup() {
    this.open(false, false);
  }
  protected open(closeOnBackdropClick: boolean, closeOnEsc: boolean) {
    this.dialogService
      .open(QuestionEditorContainerComponent, {
        closeOnBackdropClick,
        closeOnEsc
      })
      .onClose.subscribe(save => {
        if (save) {
          this.planningModeService.qbChangeChapterTopicSet(true);
        }
      });
  }

  loadContentLib() {
    // console.log('--LOAD CL--');
    //this.curriculumPlaylistService.getMyEbook(this.selectedSubject);
    setTimeout(() => {
      this.checkContenteditorData();
      this.libConfigService
        .getComponentFactory<ContentEditorComponent>('ContentLib')
        .subscribe({
          next: componentFactory => {
            if (!this.planningContentLib) {
              return;
            }
            const ref = this.planningContentLib.createComponent(
              componentFactory
            );
            ref.instance['selectedContaentData'] = this.contenteditorData;
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }, 100);
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.planningModeViewSubscription.unsubscribe();
    this.planningModeService.setQuizQuestionList([]);
    this.planningModeService.setSelectQuiz(false);
  }
  redirectHome() {
    this.loadContentLib();
  }

  loadUserView(state) {
    if (state['userView'] === 'userProfile') {
      this.libConfigService
        .getComponentFactory<UserSettingViewComponent>('lazy-userProfile')
        .subscribe({
          next: componentFactory => {
            if (!this.userSettingView) {
              return;
            }
            const ref = this.userSettingView.createComponent(componentFactory);
            ref.instance['currentView'] = state['currentState'];
            ref.instance['inputPrefereView'] = this.inputPrefereView;
            ref.instance['userProfile'] = this.userProfile;
            ref.instance['currentMode'] = 'planning';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
    if (state['userView'] === 'userChangePassword') {
      this.libConfigService
        .getComponentFactory<ChangePasswordComponent>('lazy-userChangePassword')
        .subscribe({
          next: componentFactory => {
            if (!this.changePassword) {
              return;
            }
            const ref = this.changePassword.createComponent(componentFactory);
            ref.instance['currentMode'] = 'planning';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
    if (state['userView'] === 'userChangePin') {
      this.libConfigService
        .getComponentFactory<ChangePinComponent>('lazy-userChangePin')
        .subscribe({
          next: componentFactory => {
            if (!this.changePin) {
              return;
            }
            const ref = this.changePin.createComponent(componentFactory);
            ref.instance['currentMode'] = 'planning';
            ref.changeDetectorRef.detectChanges();
          },
          error: err => {
            console.warn(err);
          }
        });
    }
  }
  displayvalueGradeSubject(subjectGrade) {
    // console.log('displayvalueGradeSubject -> subjectGrade', subjectGrade);
    this.showSubjectChapter = false;
    this.curriculumPlaylistService.setselectedSubjectChapter(subjectGrade);
    this.selectedGradeLevel = subjectGrade.gradeLevel;
    this.selectedSubject = subjectGrade.subject;
    this.questionEditorService.setGrade(this.selectedGradeLevel);
    this.questionEditorService.setSubject(this.selectedSubject);
  }

  loadAssignmentLib(): void {
    // this.libConfigService
    //   .getComponentFactory<PlanningAssignmentViewComponent>('AssignmentLib')
    //   .subscribe({
    //     next: componentFactory => {
    //       if (!this.planningContentLib) {
    //         //console.log('not added');
    //         return;
    //       }
    //       const ref = this.planningContentLib.createComponent(componentFactory);
    //       // ref.instance['selectedContaentData'] = this.contenteditorData;
    //       ref.changeDetectorRef.detectChanges();
    //     },
    //     error: err => {
    //       console.warn(err);
    //     }
    //   });
  }
  loadReportsAndAnalyticsLib(): void {
    this.libConfigService
      .getComponentFactory<PlanningReportsAnalyticsViewComponent>(
        'Report-AnalyticsLib'
      )
      .subscribe({
        next: componentFactory => {
          if (!this.planningContentLib) {
            //console.log('not added');
            return;
          }
          const ref = this.planningContentLib.createComponent(componentFactory);
          // ref.instance['selectedContaentData'] = this.contenteditorData;
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  loadQuestionBank(): void {
    //console.log('loadQuestionBank');
    this.libConfigService
      .getComponentFactory<QuestionBankViewComponent>('question-bank')
      .subscribe({
        next: componentFactory => {
          if (!this.planningContentLib) {
            return;
          }
          this.questionBankRef = this.planningContentLib.createComponent(
            componentFactory
          ) as ComponentRef<QuestionBankViewComponent>;

          // this.questionBankRef = this.questionBankRef;
          this.updateQuestionBankData();
          this.questionBankRef.changeDetectorRef.detectChanges();
          // if (this.questionBankRef.instance.addUpdateQuestion) {
          //   this.questionBankRef.instance.addUpdateQuestion.subscribe(val => {
          //     this.loadQuestionBank();
          //   });
          // }
          this.questionEditorService.updateSubmitAnsShow(false);
        },
        error: err => {
          console.warn(err);
        }
      });
  }
  loadQuestionEditor(): void {
    this.openAdditionPopUp = !this.openAdditionPopUp;
    this.openPopup();
  }
  openPlanningMode() {
    this.openAdditionPopUp = !this.openAdditionPopUp;
  }
  clicktoShowSubjectChapter() {
    this.showSubjectChapter = !this.showSubjectChapter;
  }

  openQuestionEditor() {
    //console.log('openQuestionEditor', this.planningContentLib);
    //this.currentMenu = 'Question-editor';
    this.loadQuestionEditor();

    // if (this.planningContentLib) {
    //   this.planningContentLib.clear();
    // }
  }

  //added by thiru
  loadSignIn() {
    // this.authService.setCurrentSignInState(SignInState.SESSION_DEFAULT);
    // this.authService.setSignInModuleState(SignInModuleState.INACTIVE);
    this.libConfigService
      .getComponentFactory<LoginPwdComponent>('lazy-sign-in')
      .subscribe({
        next: componentFactory => {
          if (!this.loginOutlet) {
            return;
          }
          const ref = this.loginOutlet.createComponent(componentFactory);
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }

  chapterToggle() {
    this.chapterDropdown = true;
  }

  topicToggle() {
    this.topicDropdown = true;
  }

  changeChapter(event) {
    // console.log('chapterchange ', event);
    this.availableChapters.forEach(chapter => {
      if (event.chapterId === chapter.chapterId) {
        this.availableTopics = chapter.topics;
      }
    });
    this.selectedChapter = event;
    this.questionEditorService.setSelectedChapter(this.selectedChapter);
    this.selectedTopic = 'none';
    this.questionEditorService.setSelectedTopic(this.selectedTopic);
    this.questionBankRef.destroy();
    this.loadQuestionBank();
    // this.curriculumPlaylistService.setChapterSelection(event, null);
    // this.updateQuestionBankData();
  }

  changeTopic(event) {
    this.selectedTopic = event;
    this.questionEditorService.setSelectedTopic(this.selectedTopic);
    this.questionBankRef.destroy();
    this.loadQuestionBank();
    // this.updateQuestionBankData();
  }

  updateQuestionBankData() {
    this.questionBankRef.instance.selectedGrade = this.selectedGradeLevel;
    this.questionBankRef.instance.selectedSubject = this.selectedSubject;
    this.questionBankRef.instance.chapters = this.availableChapters;
    this.questionBankRef.instance.selectedChapter = this.selectedChapter;
    this.questionBankRef.instance.selectedTopic = this.selectedTopic;
    this.questionBankRef.instance.topics = this.availableTopics;
  }
}
