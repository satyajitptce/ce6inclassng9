import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningModeViewComponent } from './planning-mode-view.component';

describe('PlanningModeViewComponent', () => {
  let component: PlanningModeViewComponent;
  let fixture: ComponentFixture<PlanningModeViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningModeViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningModeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
