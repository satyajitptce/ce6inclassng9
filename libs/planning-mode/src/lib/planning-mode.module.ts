import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningModeViewComponent } from './component/planning-mode-view/planning-mode-view.component';
import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';

import {
  NbButtonModule,
  NbMenuModule,
  NbLayoutModule,
  NbSidebarModule,
  NbDialogModule,
  NbThemeModule,
  NbCardModule,
  NbIconModule,
  NbSelectModule,
  NbActionsModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { ToolbarModule } from '@tce/toolbar';
import { CoreModule } from '@tce/core';
import { CommonSelectorModule } from '@app-teacher/features/common-selector/common-selector.module';
import { RouterModule, Routes } from '@angular/router';
import { PlanningModeBackgroundComponent } from './component/planning-mode-background/planning-mode-background.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
//import { SchoolViewInfoComponent } from './new-modules/common-models/school-view-info/school-view-info.component';
import { AdminPlanmodeComponent } from './component/admin-planmode/admin-planmode.component';

const routes: Routes = [
  {
    path: '',
    component: PlanningModeViewComponent
  }
];

const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'ContentLib',
    path: 'ContentLib',
    loadChildren: () =>
      import('@tce/content-library').then(m => m.ContentLibraryModule)
  },  
  {
    componentId: 'GradebookLib',
    path: 'GradebookLib',
    loadChildren: () => import('@tce/gradebook').then(m => m.GradebookModule)
  },
  {
    componentId: 'Report-AnalyticsLib',
    path: 'Report-AnalyticsLib',
    loadChildren: () =>
      import('@tce/reports-analytics').then(m => m.ReportsAnalyticsModule)
  },
  {
    componentId: 'CalendarLib',
    path: 'CalendarLib',
    loadChildren: () => import('@tce/calendar').then(m => m.CalendarModule)
  },
  {
    componentId: 'StudentsLib',
    path: 'StudentsLib',
    loadChildren: () => import('@tce/students').then(m => m.StudentsModule)
  },
  {
    componentId: 'lazy-search',
    path: 'lazy-search', // some globally-unique identifier, used internally by the router
    loadChildren: () => import('@tce/search').then(mod => mod.SearchModule)
  },
  {
    componentId: 'lazy-userProfile',
    path: 'lazy-userProfile', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/user-profile').then(mod => mod.UserProfileModule)
  },
  {
    componentId: 'lazy-userChangePassword',
    path: 'lazy-userChangePassword', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import(
        '../../../../apps/tce-teacher/src/app/features/user-profile-change-password/user-profile-change-password.module'
      ).then(mod => mod.UserProfileChangePasswordModule)
  },
  {
    componentId: 'lazy-userChangePin',
    path: 'lazy-userChangePin', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import(
        '../../../../apps/tce-teacher/src/app/features/user-profile-change-pin/user-profile-change-pin.module'
      ).then(mod => mod.UserProfileChangePinModule)
  },
  {
    componentId: 'question-bank',
    path: 'question-bank', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import('@tce/question-bank').then(mod => mod.QuestionBankModule)
  },

  {
    componentId: 'TemplateEditorLib',
    path: 'TemplateEditorLib',
    loadChildren: () =>
      import('@tce/template-editor').then(m => m.TemplateEditorModule)
  },
  {
    componentId: 'lazy-sign-in',
    path: 'lazy-sign-in', // some globally-unique identifier, used internally by the router
    loadChildren: () =>
      import(
        '../../../../apps/tce-teacher/src/app/features/login/sign-in/sign-in.module'
      ).then(mod => mod.SignInModule)
  }
];
@NgModule({
  imports: [
    CommonModule,
    NbMenuModule.forRoot(),
    NbSidebarModule.forRoot(),
    NbThemeModule.forRoot(),
    NbLayoutModule,
    NbButtonModule,
    NbCardModule,
    NbEvaIconsModule,
    NbIconModule,
    NbActionsModule,
    NbDialogModule.forRoot(),
    ToolbarModule,
    LibConfigModule.forRoot(manifests),
    LibConfigModule.forChild(PlanningModeViewComponent),
    CommonSelectorModule,
    CoreModule,
    NbSelectModule,
    NbActionsModule,
    //added by thiru
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    PlanningModeViewComponent,
    PlanningModeBackgroundComponent,
    AdminPlanmodeComponent
  ],
  exports: [
    PlanningModeViewComponent,
    RouterModule,
    PlanningModeBackgroundComponent,
    AdminPlanmodeComponent
  ],
  entryComponents: [
    PlanningModeViewComponent,
    PlanningModeBackgroundComponent,
    AdminPlanmodeComponent
  ]
})
export class PlanningModeModule {}
