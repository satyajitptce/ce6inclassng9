import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject, ReplaySubject, BehaviorSubject, Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PlanningModeService {
  private userProfileState = new Subject();
  private qbChangeChapterTopic = new Subject();
  public selectQuiz: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public quizQuestionList: BehaviorSubject<Array<string>> = new BehaviorSubject<
    any
  >([]);

  public userProfileState$ = this.userProfileState.asObservable();
  constructor(private http: HttpClient) {}
  getAdminUrl(): Observable<any> {
    return this.http.get('assets/config.json');
  }

  setUserProfileState(state) {
    this.userProfileState.next(state);
  }

  qbChangeChapterTopicSet(value) {
    this.qbChangeChapterTopic.next(value);
  }

  qbChangeChapterTopicGet() {
    return this.qbChangeChapterTopic;
  }

  setSelectQuiz(bool) {
    this.selectQuiz.next(bool);
  }

  getSelectQuiz() {
    return this.selectQuiz.asObservable();
  }

  setQuizQuestionList(quizQuestions) {
    this.quizQuestionList.next(quizQuestions);
  }

  getQuizQuestionList() {
    return this.quizQuestionList.asObservable();
  }
}
