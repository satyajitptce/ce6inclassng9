import { TestBed } from '@angular/core/testing';

import { PlanningModeService } from './planning-mode.service';

describe('PlanningModeService', () => {
  let service: PlanningModeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlanningModeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
