export interface UserRoles {
  name: string;
  orgId: string;
  partition: number;
  permission: string;
  roleId: string;
}

export interface UserCreateBody {
  userName: string;
  organization: string;
  role: string;
  expiryDate: string;
}

export interface SchoolSubjectsGradeList{
  data: {
    gradeId: string;
    gradename: string;
    subjects: {
      id: string;
      name: string;
    } [];
  } [];
  draw: null;
  recordsFiltered: number;
  recordsTotal: number;
}

export interface SchoolSubjectsList{
  data: {
    hasGames: string;
    mapping: string;
    status: string;
    subjectId: string;
    title: string;
  } [];
  draw: null
  recordsFiltered: number;
  recordsTotal: number;
}

export interface SchoolSettingsSubjectList{
  data: {
    subject: {
      id: string;
      name: string;
    }
    type: string;
  } [];
  draw: null;
  recordsFiltered: number;
  recordsTotal: number;
}

export interface SchoolSettingsSubjectType{
  data: {
    id: number;
    key: string;
    module: string;
    permission: null;
    value: string;
    xtraInfo: string;
  } [];
  draw: null;
  recordsFiltered: number;
  recordsTotal: number;
}

export interface SchoolSettingsTerminal{
  data: {
    metadata: string;
    schoolId: string;
    terminalKey: string;
    terminalType: string;
  } [];
  draw: null;
  recordsFiltered: number;
  recordsTotal: number;
}