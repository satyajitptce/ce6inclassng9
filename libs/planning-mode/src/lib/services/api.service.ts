import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SchoolSettingsSubjectList, SchoolSettingsSubjectType, SchoolSettingsTerminal, SchoolSubjectsGradeList, SchoolSubjectsList, UserCreateBody, UserRoles } from './api-interface';
import { from, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public orgId = 'tcdeli-lxplm';

  constructor(private http: HttpClient) {}

  getUserList(): Observable<UserRoles[]> {
    return this.http.get<UserRoles[]>(
      `tce-auth-api/1/api/1/admin/organization/${this.orgId}/roles`
    );
  }

  createSchoolAdminUser(body: UserCreateBody) {
    return this.http.post('tce-auth-api/1/api/1/admin/user', body);
  }

  tableData(start, length, sortColumn?, sortDir?, query?){
    let params = {};
    params["start"] = start;
    params["length"] = length;
    if(sortColumn){
      params["sortColumn"] = sortColumn;
    }
    if(sortDir){
      params["sortDir"] = sortDir;
    }
    if(query){
      params["q"] = query;
    }
    return this.http.get('assets/table-example.json', {
      params: params
    })
  }

  getSchoolGrades(start, length, sortColumn?, sortDir?, query?){
    let params = {};
    params["start"] = start;
    params["length"] = length;
    if(sortColumn){
      params["sortColumn"] = sortColumn;
    }
    if(sortDir){
      params["sortDir"] = sortDir;
    }
    if(query){
      params["q"] = query;
    }
    params["filterTerms"] = `schoolId:${this.orgId}`
    return this.http.get('tce-teach-api/1/api/1/school/grades', {
      params: params
    })
  }

  getGradeList(){
    let params = {
      "start": "0",
      "length": "-1",
      "sortDir": "asc",
      "sortColumn": "title",
      "filterTerms": [
        `schoolId:${this.orgId}`, "status:ACTIVE"
      ]
    };
    
    return this.http.get('tce-teach-api/1/api/1/content/grades', {
      params: params
    });
  }

  getGradeCategoryList(){
    return this.http.get('tce-auth-api/1/api/1/tool/lookup/grade-categories');
  }

  getDivisions(){
    return of([
      {id: 1, title: 1},
      {id: 2, title: 2},
      {id: 3, title: 3},
      {id: 4, title: 4},
      {id: 5, title: 5},
      {id: 6, title: 6},
      {id: 7, title: 7},
      {id: 8, title: 8},
      {id: 9, title: 9},
      {id: 10, title: 10}
    ]);
  }

  addSchoolGrade(body){
    return this.http.post("/tce-teach-api/1/api/1/school/grade", body);
  }

  deleteSchoolGrade(gradeId){
    return this.http.delete("tce-teach-api/1/api/1/school/grade", {
      params: { gradeId }
    });
  }

  getSchoolSubjectGradeList(): Observable<SchoolSubjectsGradeList>{
    let params = {
      "start": "0",
      "length": "-1",
      "filterTerms": [
        `schoolId:${this.orgId}`
      ]
    };
    return this.http.get<SchoolSubjectsGradeList>("tce-teach-api/1/api/1/school/subjects/grade", {
      params
    });
  }

  getSchoolSubjectList(): Observable<SchoolSubjectsList>{
    let params = {
      "start": "0",
      "length": "-1",
      "filterTerms": [
        `schoolId:${this.orgId}`
      ]
    };
    return this.http.get<SchoolSubjectsList>("tce-teach-api/1/api/1/content/subjects", {
      params
    });
  }

  deleteSchoolSubjectFromGrade(params){
    return this.http.delete("tce-teach-api/1/api/1/school/subject", {
      params: params
    })
  }

  addSchoolSubjectFromGrade(body){
    return this.http.post("tce-teach-api/1/api/1/school/subject/grade", body);
  }

  getSchoolSettingsSubjectList(): Observable<SchoolSettingsSubjectList>{
    let params = {
      "start": "0",
      "length": "-1",
      "filterTerms": [
        `schoolId:${this.orgId}`
      ]
    };
    return this.http.get<SchoolSettingsSubjectList>("tce-teach-api/1/api/1/school/subjects/classify", {
      params
    })
  }

  getSchoolSettingsSubjectType(): Observable<SchoolSettingsSubjectType>{
    let params = {
      "start": "0",
      "length": "-1",
      "filterTerms": [
        `schoolId:${this.orgId}`
      ]
    };
    return this.http.get<SchoolSettingsSubjectType>("tce-auth-api/1/api/1/tool/lookup/subject-types", { params });
  }

  changeSchoolSettingsSubjectType(body){
    return this.http.put("tce-teach-api/1/api/1/school/subject/classify", body);
  }

  getSchoolSettingsTerminal(start, length, sortColumn?, sortDir?, query?): Observable<SchoolSettingsTerminal>{
    let params = {};
    params["start"] = start;
    params["length"] = length;
    if(sortColumn){
      params["sortColumn"] = sortColumn;
    }
    if(sortDir){
      params["sortDir"] = sortDir;
    }
    if(query){
      params["q"] = query;
    }
    params["filterTerms"] = `schoolId:${this.orgId}`
    return this.http.get<SchoolSettingsTerminal>("tce-teach-api/1/api/1/school/terminals", {
      params
    });
  }

  addSchoolSettingsTerminal(body){
    return this.http.post("tce-teach-api/1/api/1/school/terminal", body);
  }

  editSchoolSettingsTerminal(body){
    return this.http.put("tce-teach-api/1/api/1/school/terminal", body);
  }

  getMiscellaneousSettings(){
    return this.http.get("tce-teach-api/1/api/1/school/settings", 
        {params: {keys: ["/daysOfWeek", "/startTime", "/endTime", "/periodDuration", "/breaks"]}
    })
  }

  patchMiscellaneousSettings(params){
      return this.http.patch("tce-teach-api/1/api/1/school/settings", params);
  }
}
