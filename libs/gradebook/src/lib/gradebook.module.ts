import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { PlanningGradebookViewComponent } from './planning-gradebook/component/planning-gradebook-view/planning-gradebook-view.component';

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(PlanningGradebookViewComponent)
  ]
})
export class GradebookModule {}
