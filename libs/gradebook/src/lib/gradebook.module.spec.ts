import { async, TestBed } from '@angular/core/testing';
import { GradebookModule } from './gradebook.module';

describe('GradebookModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GradebookModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(GradebookModule).toBeDefined();
  });
});
