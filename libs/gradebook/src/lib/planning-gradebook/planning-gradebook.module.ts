import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningGradebookViewComponent } from './component/planning-gradebook-view/planning-gradebook-view.component';

@NgModule({
  declarations: [PlanningGradebookViewComponent],
  imports: [CommonModule],
  exports: [PlanningGradebookViewComponent],
  entryComponents: [PlanningGradebookViewComponent]
})
export class PlanningGradebookModule {}
