import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningGradebookViewComponent } from './planning-gradebook-view.component';

describe('PlanningGradebookViewComponent', () => {
  let component: PlanningGradebookViewComponent;
  let fixture: ComponentFixture<PlanningGradebookViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningGradebookViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningGradebookViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
