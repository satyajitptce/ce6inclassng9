module.exports = {
  name: 'gradebook',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/gradebook',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
