module.exports = {
  name: 'player-tce-video',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-tce-video',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
