import { async, TestBed } from '@angular/core/testing';
import { PlayerTceVideoModule } from './player-tce-video.module';

describe('PlayerTceVideoModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerTceVideoModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerTceVideoModule).toBeDefined();
  });
});
