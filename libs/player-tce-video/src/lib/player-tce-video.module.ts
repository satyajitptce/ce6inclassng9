import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { TceVideoPlayerComponent } from './components/tce-video-player/tce-video-player.component';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';
import { MediaOverlayComponent } from './components/mediaoverlay/mediaoverlay.component';
import { CoreModule } from '@tce/core';

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(TceVideoPlayerComponent),
    PlayerAnnotationModule,
    CoreModule
  ],
  declarations: [TceVideoPlayerComponent, MediaOverlayComponent],
  exports: [TceVideoPlayerComponent],
  entryComponents: [MediaOverlayComponent]
})
export class PlayerTceVideoModule {}
