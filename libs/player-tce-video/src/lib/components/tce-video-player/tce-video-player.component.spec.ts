import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TceVideoPlayerComponent } from './tce-video-player.component';

describe('TceVideoPlayerComponent', () => {
  let component: TceVideoPlayerComponent;
  let fixture: ComponentFixture<TceVideoPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TceVideoPlayerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TceVideoPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
