import {
  Component,
  OnInit,
  Input,
  ElementRef,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ComponentFactoryResolver,
  HostListener
} from '@angular/core';
import {
  ScriptLoaderService,
  PlayerAbstractInterface,
  PlayerAbstractComponent,
  PlayerAbstractAnimations,
  ResourceType,
  ContentResizeService,
  Resource,
  RequestApiService,
  PlayerUsageService
} from '@tce/core';
import { MediaOverlayComponent } from '../mediaoverlay/mediaoverlay.component';
import { Subscription } from 'rxjs';

declare var load_TCE_PLAYER_Angular: any;
declare var tcePlayerReSize: any;
declare var tceVideoPlayerRef: any;
declare var tcePlayerReplay: any;

@Component({
  selector: 'tce-video-player',
  templateUrl: './tce-video-player.component.html',
  styleUrls: ['./tce-video-player.component.scss'],
  animations: PlayerAbstractAnimations
})
export class TceVideoPlayerComponent extends PlayerAbstractComponent
  implements OnInit, PlayerAbstractInterface {
  private url = this.requestApiService.getUrl('getFile');
  private containerWidth;
  private containerHeight;
  private path: string;

  @ViewChild('tcePlayerOutlet', { static: true }) tcePlayerOutlet: ElementRef;
  @ViewChild('mediaOverlayOutlet', { static: true, read: ViewContainerRef })
  mediaOverlayOutlet: ViewContainerRef | undefined;
  mediaOverlayRef: ComponentRef<any>;
  mediaOverlaySubscribers: Subscription = new Subscription();
  @Input() isPlayerDrawerVisible: boolean;
  resourceId;

  constructor(
    private scriptLoader: ScriptLoaderService,
    private requestApiService: RequestApiService,
    private contentResizeService: ContentResizeService,
    private factoryResolver: ComponentFactoryResolver,
    private playerUsageService: PlayerUsageService
  ) {
    super();
  }

  ngOnInit() {
    //console.log('tce-video-player--------->oninit');
    //this.lazyLoadLibs();
    this.contentResizeService.resizeContent$.subscribe(() => {
      if (typeof tcePlayerReSize === 'function') {
        tcePlayerReSize();
      }
    });
  }

  beginLoadingResource() {
    this.lazyLoadLibs();
  }

  buildPathAndLoad() {
    const { resourceId } = this.resource;
    const { encryptedFilePath } = this.resource.metaData;

    let requestResourceId = '';
    if (resourceId.toLowerCase().indexOf('test') !== -1) {
      requestResourceId = 'CD1DABD1-C988-4B81-A1D9-0C9A8E521CF7';
    } else {
      requestResourceId = resourceId;
    }

    this.resourceId = requestResourceId;

    //console.log(requestResourceId);
    this.path =
      this.url + '/' + encryptedFilePath + '/' + requestResourceId + '/';
    this.getMediaSource();
  }
  private lazyLoadLibs() {
    //console.log('lazyLoadLibs tce-video');
    this.scriptLoader.loadScript('assets/vendor/svg.js').subscribe(() => {
      this.scriptLoader
        .loadScripts([
          'assets/tcemedia/mediaplayer/tce_player_integrate.js',
          'assets/tcemedia/external/player-html/player/css/mainIndex.css',
          'assets/tcemedia/external/player-html/player/js/shell/tcePlayer.js',
          'assets/tcemedia/mediaplayer/media.js'
        ])
        .subscribe(() => {
          //this.eRef;
          tceVideoPlayerRef = this;
          this.buildPathAndLoad();
        });
    });
  }

  setTCEPlayerDimensions(zoomsize) {
    tcePlayerReSize(zoomsize);
  }

  getMediaSource() {
    const playerOutletElement: HTMLElement = this.tcePlayerOutlet.nativeElement;
    setTimeout(() => {
      this.containerWidth = playerOutletElement.clientWidth;
      this.containerHeight = playerOutletElement.clientHeight;
    }, 1000);

    //console.log('path', this.path);
    //console.log('resource', this.resource);
    load_TCE_PLAYER_Angular(playerOutletElement, this.path, this.resource);
  }

  get resourceType() {
    return ResourceType;
  }

  tcePlayerOnMediaEnded(arg: any) {
    this.loadMediaOverlay();
  }
  loadMediaOverlay() {
    const mediaOverlayFactory = this.factoryResolver.resolveComponentFactory(
      MediaOverlayComponent
    );
    this.mediaOverlayRef = this.mediaOverlayOutlet.createComponent(
      mediaOverlayFactory
    );
    this.mediaOverlayRef.instance.currentMedia = this.resource;
    this.setMediaOverlayListeners();
    this.mediaOverlayRef.changeDetectorRef.detectChanges();
  }

  setMediaOverlayListeners() {
    this.mediaOverlaySubscribers = new Subscription();
    this.mediaOverlaySubscribers.add(
      this.mediaOverlayRef.instance.replayMedia.subscribe(() => {
        this.mediaOverlayRef.destroy();
        tcePlayerReplay(
          (<HTMLElement>this.tcePlayerOutlet.nativeElement).querySelector(
            'iframe'
          )
        );
      })
    );

    this.mediaOverlaySubscribers.add(
      this.mediaOverlayRef.instance.playNewMedia.subscribe(
        (newResource: Resource) => {
          this.playerCloseEmitter.emit(newResource);
        }
      )
    );

    this.mediaOverlayRef.onDestroy(() => {
      this.mediaOverlaySubscribers.unsubscribe();
    });
  }

  playerActivated() {
    this.tceVideoPlayerRef = this;
  }

  @HostListener('document:tcePlayerLoaded', ['$event'])
  tcePlayerLoaded(event: CustomEvent) {
    this.isLoading = false;
    if (event.detail === 'success') {
      this.errorMessage = '';
    } else {
      this.errorMessage = 'Error Loading Player';
    }
  }

  @HostListener('window:playerEventClickListner', ['$event.detail'])
  playerEventClickListner(detail) {
    this.playerUsageService.logResourceUsage(detail.resorceData);
  }
}
