import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  ElementRef,
  HostListener,
  Input
} from '@angular/core';
import {
  Resource,
  AppConfigService,
  ContentResizeService,
  CurriculumPlaylistService,
  RequestApiService
} from '@tce/core';

@Component({
  selector: 'tce-mediaoverlay',
  templateUrl: './mediaoverlay.component.html',
  styleUrls: ['./mediaoverlay.component.css']
  //changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaOverlayComponent implements OnInit {
  index = 0;
  preview: any;
  nextview: any;
  currentview: any;
  currentid: any;
  mediadata: any;
  currentmediaref: any;
  sizingReferenceInterval;
  sizingReferenceElement: HTMLElement;
  setSizeTimeout = 200;
  @Input() currentMedia: Resource;
  @Output() replayMedia = new EventEmitter<any>();
  @Output() playNewMedia = new EventEmitter<Resource>();
  constructor(
    private appConfigService: AppConfigService,
    private requestApiService: RequestApiService,
    private elRef: ElementRef,
    private contentResizeService: ContentResizeService,
    private curriculumPlaylistService: CurriculumPlaylistService
  ) {}

  ngOnInit() {
    this.curriculumPlaylistService.availableVideos$.subscribe(
      (availableVideos: Resource[]) => {
        availableVideos.forEach((videoResource: Resource, index) => {
          if (videoResource.resourceId === this.currentMedia.resourceId) {
            if (index > 0) {
              this.preview = availableVideos[index - 1];
            }
            if (index + 1 < availableVideos.length) {
              this.nextview = availableVideos[index + 1];
            }
          }
        });
      }
    );

    const foundVideoIframe = this.elRef.nativeElement.parentElement.querySelector(
      'iframe'
    );
    if (foundVideoIframe) {
      clearInterval(this.sizingReferenceInterval);
      this.sizingReferenceElement = foundVideoIframe;
      setTimeout(() => {
        this.setSize();
      }, this.setSizeTimeout);
    }
    this.contentResizeService.resizeContent$.subscribe(() => {
      setTimeout(() => {
        this.setSize();
      }, this.setSizeTimeout);
    });
  }

  @HostListener('window:resize', ['$event'])
  windowResize() {
    setTimeout(() => {
      this.setSize();
    }, this.setSizeTimeout);
  }

  setSize() {
    if (this.sizingReferenceElement) {
      const boundingBox = this.sizingReferenceElement.getBoundingClientRect();
      const hostElement = <HTMLElement>this.elRef.nativeElement;
      hostElement.style.width = Math.ceil(boundingBox.width) + 'px';
      hostElement.style.height = Math.ceil(boundingBox.height) + 'px';
      hostElement.style.left = this.sizingReferenceElement.style.left;
    }
  }

  replay() {
    this.replayMedia.emit();
  }

  playRelatedMedia(resource: Resource, event) {
    this.playNewMedia.emit(resource);
  }

  getResourceThumbnail(thumbnailParams) {
    if (thumbnailParams) {
      return this.requestApiService.getUrl('getFile') + thumbnailParams;
    } else {
      return '';
    }
  }

  public getResourceThumbnailIcon(resource: Resource, type: string) {
    return this.appConfigService.getResourceThumbnailIcon(
      resource.resourceType,
      resource.tcetype,
      type
    );
  }
}
