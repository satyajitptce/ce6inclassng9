import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MediaOverlayComponent } from './mediaoverlay.component';

describe('MediaOverlayComponent', () => {
  let component: MediaOverlayComponent;
  let fixture: ComponentFixture<MediaOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MediaOverlayComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MediaOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
