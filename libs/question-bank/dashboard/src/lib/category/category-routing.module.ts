import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CategoryLayoutComponent } from './category-layout/category-layout.component';

const routes: Routes = [
  {
    path: '',
    component: CategoryLayoutComponent
  },
  {
    path: ':category',
    component: CategoryLayoutComponent
  },
  {
    path: ':category/:subcategory',
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../template-viewer/template-viewer.module').then(
            mod => mod.TemplateViewerModule
          )
      },
      {
        path: ':id',
        loadChildren: () =>
          import('../template-viewer/template-viewer.module').then(
            mod => mod.TemplateViewerModule
          )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CategoryRoutingModule {}
