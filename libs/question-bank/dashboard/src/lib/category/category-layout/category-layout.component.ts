import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DashboardService } from '../../dashboard.service';
import { CategoryData } from '../../interface/category.interface';

@Component({
  selector: 'adc-workspace-category-layout',
  templateUrl: './category-layout.component.html',
  styleUrls: ['./category-layout.component.scss']
})
export class CategoryLayoutComponent implements OnInit {
  public questionType: any;
  public pages: any;
  public currentOrientation: string = 'vertical';
  // public categoryData: any;
  // Object = Object;

  public questionData: CategoryData;
  constructor(
    public route: ActivatedRoute,
    public location: Location,
    public dashboardService: DashboardService
  ) {}

  ngOnInit() {
    this.initState();
  }

  /**
   * Function to fetch the category data
   */
  initState(): void {
    this.dashboardService.categoryData().subscribe((data: CategoryData) => {
      this.questionData = data;

      this.questionType = this.questionData['questiontypes'];

      this.route.params.subscribe(params => {
        if (params.category) {
          this.changeTab(params.category);
        }
      });
    });
  }

  /**
   * @description This function changes the sub categories according to the category selected in the left tab
   * @param name Type=String
   * @returns void
   */
  changeTab(name: string): void {
    this.questionType.forEach(question => {
      question.selected = false;

      if (question.name == name) {
        question.selected = true;
      }
    });
  }

  /**
   * @description This function is used to go to the previous page
   * @returns void
   */
  back(): void {
    this.location.back();
  }
}
