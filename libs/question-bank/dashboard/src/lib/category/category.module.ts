import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryLayoutComponent } from './category-layout/category-layout.component';
import { CategoryRoutingModule } from './category-routing.module';

@NgModule({
  declarations: [CategoryLayoutComponent],
  imports: [CommonModule, CategoryRoutingModule]
})
export class CategoryModule {}
