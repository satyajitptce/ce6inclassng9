import {
  Component,
  OnInit,
  Output,
  Input,
  EventEmitter,
  Renderer,
  Renderer2,
  ViewChild,
  ElementRef
} from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { DashboardService } from '../dashboard.service';
import { QuizTemplate } from 'libs/quiz-player/src/lib/quiz-player/models/quiz-template/quiz-template.model';

@Component({
  selector: 'adc-workspace-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public questionList: Array<QuizTemplate> = [];
  public templateType: string = 'dashboard';
  public showAns: boolean = false;
  public element: any;
  public showDashboardAnswers: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(false);

  public dashboardPreviewState: BehaviorSubject<boolean> = new BehaviorSubject<
    boolean
  >(true);

  public loadQuestionState: BehaviorSubject<string> = new BehaviorSubject<
    string
  >('single');
  public dashboardPreview: boolean = true;

  public viewMode: string = this.dashboardPreviewState.value
    ? 'preview'
    : 'edit';
  public questionMode: string = 'single';
  public editRedirectSubscription: Subscription;
  @ViewChild('addDiv', { static: true }) public addDiv: ElementRef;
  @ViewChild('mode', { static: true }) public mode: ElementRef;
  constructor(
    public dashboardService: DashboardService,
    public router: Router,
    public renderer: Renderer2
  ) {}

  ngOnInit() {
    const { questions } = this.dashboardService;

    this.questionList = questions;

    if (this.questionList.length == 0) {
      // this.viewMode = 'edit';
      // this.renderer.setAttribute(this.mode, 'data-checked', '');
      // this.onPreviewChange('');
    }
  }

  /**
   * @description This function is used to open he edit or preview mode of the quiz player component
   * @param event
   * @returns void
   */
  onPreviewChange(event): void {
    console.log(this.viewMode);
    if (this.viewMode == 'preview') {
      this.renderer.addClass(this.addDiv.nativeElement, 'show');
      let checked = false;
      this.viewMode = 'edit';
      this.dashboardPreview = checked;
      this.dashboardPreviewState.next(checked);
    } else {
      let checked = true;
      this.renderer.removeClass(this.addDiv.nativeElement, 'show');
      console.log(this.addDiv);
      this.viewMode = 'preview';
      this.dashboardPreview = checked;
      this.dashboardPreviewState.next(checked);
    }
  }

  /**
   * @description This function receives the data from the output function and redirects to the quiz player component with appropriate data
   * @param event type = JSON object
   * @returns void
   */
  editQuestionRedirect(event): void {
    if (event.id)
      this.router.navigate([
        `/category/${event['category']}/${event['subcategory']}/${event['id']}`
      ]);
  }

  /**
   * @description This function changes the question display mode between single and all view
   * @param event Type = boolean
   * @returns void
   */
  onQuestionModeChange(event): void {
    if (this.questionMode == 'single') this.questionMode = 'multiple';
    else this.questionMode = 'single';
    this.loadQuestionState.next(this.questionMode);
  }

  /**
   * @description This function changes the value of the show answer checkbox
   * @returns void
   */
  onAnsStatusChange(): void {
    // alert("hie");
    this.showAns = !this.showAns;
    this.showDashboardAnswers.next(this.showAns);
  }
}
