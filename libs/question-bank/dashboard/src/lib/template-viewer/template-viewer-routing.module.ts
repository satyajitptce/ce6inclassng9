import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { TemplateViewerLayoutComponent } from './template-viewer-layout/template-viewer-layout.component';

const routes: Routes = [
  {
    path: '',
    component: TemplateViewerLayoutComponent
  },
  {
    path: ':id',
    component: TemplateViewerLayoutComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), CommonModule],
  exports: [RouterModule]
})
export class TemplateViewerRoutingModule {}
