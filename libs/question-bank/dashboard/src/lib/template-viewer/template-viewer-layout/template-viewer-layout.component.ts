import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DashboardService } from '../../dashboard.service';
import { Quiz } from 'libs/quiz-player/src/lib/quiz-player/models/quiz/quiz.model';
import { QuizTemplate } from 'libs/quiz-player/src/lib/quiz-player/models/quiz-template/quiz-template.model';

@Component({
  selector: 'adc-workspace-template-viewer-layout',
  templateUrl: './template-viewer-layout.component.html',
  styleUrls: ['./template-viewer-layout.component.scss']
})
export class TemplateViewerLayoutComponent implements OnInit {
  public quizTemplateData: Array<QuizTemplate> = [];
  public templateType: string = 'quiz';
  public playerShow: boolean = false;
  public params: object;
  public quiz: Quiz;

  constructor(
    public route: ActivatedRoute,
    public dashboardService: DashboardService,
    public router: Router
  ) {}

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.params = params;
      if (params.id) {
        const { questions } = this.dashboardService;

        questions.forEach(ques => {
          if (ques['id'] == this.params['id']) {
            this.quizTemplateData = [ques];
            this.playerShow = true;
          }
        });

        this.playerShow = true;
      } else {
        this.playerShow = true;
      }
    });
  }

  /**
   * @description This functions either edits the existing question or adds a new question depending on the parameter passed.
   * @param sourceData Type = object
   * @returns void
   */
  getAnswers(sourceData: object): void {
    if (sourceData['id']) {
      this.dashboardService.editQuestion(sourceData).subscribe(
        res => {
          this.router.navigate(['/']);
        },
        err => {
          console.log(err);
        }
      );
    } else {
      sourceData['id'] = Math.random()
        .toString(36)
        .substring(7);

      this.dashboardService.addQuestion(sourceData).subscribe(
        res => {
          this.router.navigate(['/']);
        },
        err => {
          console.log(err);
        }
      );
    }
  }
}
