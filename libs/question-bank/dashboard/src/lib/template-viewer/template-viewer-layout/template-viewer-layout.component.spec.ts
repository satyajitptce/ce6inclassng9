import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TemplateViewerLayoutComponent } from './template-viewer-layout.component';

describe('TemplateViewerLayoutComponent', () => {
  let component: TemplateViewerLayoutComponent;
  let fixture: ComponentFixture<TemplateViewerLayoutComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TemplateViewerLayoutComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TemplateViewerLayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
