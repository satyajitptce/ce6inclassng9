import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateViewerRoutingModule } from './template-viewer-routing.module';
import { TemplateViewerLayoutComponent } from './template-viewer-layout/template-viewer-layout.component';
import { QuizPlayerModule } from '@adc-workspace/quiz-player';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from 'libs/quiz-player-templates/src/sharedComponents/core/shared/shared.module';
import { TemplateLoaderService } from 'libs/quiz-player/src/lib/template-loader/template-loader.service';
import { ClientTemplateLoaderService } from 'libs/quiz-player/src/lib/template-loader/client-template-loader.service';
import { QuizSharedModule } from 'libs/quiz-player-templates/src/sharedComponents/core/shared/quiz-shared.module';
import { DashboardService } from '../dashboard.service';

@NgModule({
  declarations: [TemplateViewerLayoutComponent],
  imports: [
    CommonModule,
    TemplateViewerRoutingModule,
    HttpClientModule,
    QuizPlayerModule,
    QuizSharedModule.forChild()
  ],
  providers: [
    SharedModule,
    // QuizSharedModule,
    { provide: TemplateLoaderService, useClass: ClientTemplateLoaderService }
    // {provide: DashboardService, useClass: DashboardService}
  ]
})
export class TemplateViewerModule {}
