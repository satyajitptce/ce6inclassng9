import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { QuizPlayerModule } from '@adc-workspace/quiz-player';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'libs/quiz-player-templates/src/sharedComponents/core/shared/shared.module';
import { TemplateLoaderService } from 'libs/quiz-player/src/lib/template-loader/template-loader.service';
import { ClientTemplateLoaderService } from 'libs/quiz-player/src/lib/template-loader/client-template-loader.service';
import { DashboardComponent } from './dashboard/dashboard.component';
import { QuizSharedModule } from 'libs/quiz-player-templates/src/sharedComponents/core/shared/quiz-shared.module';
import { DashboardService } from './dashboard.service';

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    HttpClientModule,
    QuizPlayerModule,
    NgbModule,
    FormsModule
  ],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
  providers: [
    QuizSharedModule,
    SharedModule,
    { provide: TemplateLoaderService, useClass: ClientTemplateLoaderService },
    { provide: DashboardService, useClass: DashboardService }
  ]
})
export class DashboardModule {}
