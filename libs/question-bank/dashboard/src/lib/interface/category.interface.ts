export interface Category {
  name: string;
  type: string;
}

export interface CategoryData {
  name: string;
  selected: boolean;
  category: Category[];
}
