import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Quiz } from './../models/quiz/quiz.model';
import { QuizTemplate } from './../models/quiz-template/quiz-template.model';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable, forkJoin } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizPlayerService {
  constructor(private http: HttpClient) {}

  getQuizData(): Observable<Quiz> {
    return this.http.get('./assets/json/quiz-data.json').pipe(
      map(data => new Quiz().deserialize(data)),
      catchError(err => throwError(err))
    );

    // return this.http.get('./assets/json/quiz-data.json').pipe(
    //   map(data => new Quiz().deserialize(data)),
    //   catchError(() => throwError('Quiz Model not found')),
    // );
  }

  getTemplatedata(templateData) {
    this.http.get(`${templateData.dataUrl}`).subscribe(res => {
      console.log(res);
    });
    return this.http.get(`${templateData.dataUrl}`).pipe(
      map(tmpltData => new QuizTemplate().deserialize(tmpltData)),
      catchError(err => throwError(err))
    );
  }

  getAllTemplateData(templatesData): Observable<any> {
    let templatesResponse: Array<any> = [];

    (templatesData.questions as Array<object>).forEach(template => {
      let templateResponse = this.getTemplatedata(template);
      templatesResponse.push(templateResponse);
    });

    return forkJoin(templatesResponse);
  }
}
