import { TestBed } from '@angular/core/testing';

import { QuizPlayerService } from './quiz-player.service';

describe('QuizPlayerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: QuizPlayerService = TestBed.get(QuizPlayerService);
    expect(service).toBeTruthy();
  });
});
