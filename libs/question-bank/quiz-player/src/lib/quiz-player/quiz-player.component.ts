import { APP_CONFIG } from '@adc-workspace/app-config';
import {
  Component,
  Inject,
  Injector,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  ElementRef,
  NgModuleFactory,
  NgModuleRef,
  ComponentFactory,
  Input,
  Output,
  EventEmitter,
  ChangeDetectorRef
} from '@angular/core';
import { TemplateConfigProvider } from '../template-loader/template-config-provider';
import { TemplateLoaderService } from '../template-loader/template-loader.service';
import { QuizPlayerService } from './services/quiz-player.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { PagesData } from './interface/quiz-player.interface';
import { Quiz } from './models/quiz/quiz.model';
import { Location } from '@angular/common';
import { QuizPage } from './models/quiz/quiz-page.model';
// import { QuizTemplateData } from './models/quiz-template/quiz-template-data.model';
import { QuizQuestion } from './models/quiz/quiz-question.model';
import { QuizTemplate } from './models/quiz-template/quiz-template.model';
import { QuizOptions } from './models/quiz-template/quiz-options.model';

@Component({
  selector: 'adc-workspace-quiz-player',
  templateUrl: './quiz-player.component.html',
  styleUrls: ['./quiz-player.component.scss']
})
export class QuizPlayerComponent implements OnInit {
  @ViewChild('targetRef', { read: ViewContainerRef, static: true })
  vcRef: ViewContainerRef;
  templateConfig: any;
  public quizData: Quiz;
  public showAns: boolean;
  public source: boolean = false;
  public totalPages: number;
  public currentPage: number = 1;
  public correctAnsPoints: number = 0;
  public points: number = 1;
  public currentPageObject: object = {};
  public selectedAnswers: Array<string> = [];
  public previewState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public sourceState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  public showAnsState: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
    false
  );
  @Input() public dashboardPreviewState: BehaviorSubject<boolean>;
  @Input() public loadQuestionState: BehaviorSubject<string>;

  private submit: Subject<void> = new Subject<void>();
  private save: Subject<void> = new Subject<void>();
  private metadata: Subject<void> = new Subject<void>();
  private viewDevice: Subject<void> = new Subject<void>();
  private layoutView: Subject<void> = new Subject<void>();
  public preview: boolean = true;
  // public model: string = 'edit';
  @Input() public quizTemplateData: Array<object>;
  @Input() public templateType: string;
  @Output() public navigateBack = new EventEmitter();
  @Output() public getAnswers = new EventEmitter();
  @Input() public showDashboardAnswers: BehaviorSubject<boolean>;
  @Output() public editRedirect: BehaviorSubject<object> = new BehaviorSubject<
    object
  >({});
  @Input() subcategory: string;
  public currentQuestionId: string;
  // public preview: boolean = this.previewState.value;
  public viewMode: boolean = this.previewState.value;
  public lightTheme: boolean = false;
  public loadQuestionMode: string = 'single';
  public displayType: string;
  public device: string = 'laptop';
  public layout: string = 'vertical';

  constructor(
    private injector: Injector,
    private templateLoader: TemplateLoaderService,
    private configProvider: TemplateConfigProvider,
    private quizPlayerService: QuizPlayerService,
    public location: Location,
    private cdr: ChangeDetectorRef,
    @Inject(APP_CONFIG) private appConfig: any
  ) {}

  ngOnInit() {
    console.log('dadsda ', this.quizTemplateData);

    // All the configs that are loaded
    this.templateConfig = this.configProvider.config;
    if (this.showDashboardAnswers) {
      this.showDashboardAnswers.subscribe((ans: boolean) => {
        this.onAnsStatusChange(ans);
      });
    }

    if (this.dashboardPreviewState) {
      this.dashboardPreviewState.subscribe((event: boolean) => {
        this.preview = event;
      });

      this.previewState.next(true);
    }

    if (this.quizTemplateData && this.quizTemplateData.length > 0) {
      this.quizTemplateData = this.quizTemplateData.map(quizTemplate =>
        new QuizTemplate().deserialize(quizTemplate)
      );

      if (this.loadQuestionState) {
        this.loadQuestionState.subscribe((state: string) => {
          this.displayType = state;
          this.renderQuestion(state);
        });
      } else {
        this.renderQuestion('single');
      }
    } else {
      this.viewMode = false;

      this.onPreviewChange(this.viewMode);
      this.getQuizData();
    }
    console.log('View Mode', this.viewMode);
  }

  /**
   * @description This function formulates the data in a required structure and renders it on the UI
   * @param data Type = string
   * @returns void
   */
  renderQuestion(data) {
    console.log('QUIZ-Data 123: ', this.quizTemplateData);
    let quizDataJson = {
      name: 'quiz'
    };
    quizDataJson['pages'] = [];
    let quizPageData = [];

    this.loadQuestionMode = data;
    if (data == 'single') {
      this.quizTemplateData.forEach((quiz, index) => {
        let pageJSON = {
          name: `page${index + 1}`,
          questions: []
        };

        let questionJSON = {
          name: this.getModuleType(quiz['type']),
          dataUrl: ''
        };

        questionJSON['templateData'] = new QuizTemplate().deserialize(quiz);
        questionJSON = new QuizQuestion().deserialize(questionJSON);

        pageJSON.questions.push(questionJSON);
        pageJSON = new QuizPage().deserialize(pageJSON);
        quizPageData.push(pageJSON);
      });

      quizDataJson['pages'] = quizPageData;
      this.quizData = new Quiz().deserialize(quizDataJson);
      this.totalPages = this.quizData.pages.length;
      this.loadCurrentPage();
    } else {
      this.vcRef.clear();
      this.quizTemplateData.forEach((quiz: QuizTemplate) => {
        if (quiz['type']) {
          let pluginName = this.getModuleType(quiz['type']);
          this.loadTemplate(pluginName, quiz);
        }
      });
    }
  }

  /**
   * @description This function takes the type as parameter as returns the plugin name which is used to identify the module
   * @param type Type = string
   * @returns string
   */
  getModuleType(type) {
    let pluginName;
    if (type == 'mcq-ms') {
      pluginName = 'mcq-multiple-select';
    } else if (type == 'mcq-ss') {
      pluginName = 'mcq-single-select';
    } else if (type == 'mcq-tf') {
      pluginName = 'mcq-true-false';
    } else if (type == 'mcq-mxs') {
      pluginName = 'mcq-matrix-standard';
    } else if (type == 'mcq-mxi') {
      pluginName = 'mcq-matrix-inline';
    } else if (type == 'mcq-mxl') {
      pluginName = 'mcq-matrix-labels';
    } else if (type == 'mcq-oi') {
      pluginName = 'mcq-option-image';
    } else if (type == 'mcq-qi') {
      pluginName = 'mcq-qstem-image';
    } else if (type == 'fib-text') {
      pluginName = type;
    } else if (type == 'fib-dropdown') {
      pluginName = type;
    } else if (type == 'fib-drag-drop') {
      pluginName = type;
    } else if (type == 'fib-image-drag-drop') {
      pluginName = type;
    } else if (type == 'fib-image-text') {
      pluginName = type;
    } else if (type == 'fib-image-dropdown') {
      pluginName = type;
    } else if (type == 'order-list') {
      pluginName = type;
    } else if (type == 'match-list') {
      pluginName = type;
    } else if (type == 'sort-list') {
      pluginName = type;
    } else if (type == 'classify-group') {
      pluginName = type;
    } else if (type == 'hotspot') {
      pluginName = type;
    } else if (type == 'short-text') {
      pluginName = type;
    } else if (type == 'plain-text') {
      pluginName = type;
    } else if (type == 'rich-text') {
      pluginName = type;
    } else if (type == 'multiple-question') {
      pluginName = type;
    } else if (type == 'token') {
      pluginName = type;
    }
    return pluginName;
  }

  /**
   * Load the template by name
   * Note: This method can be used to load different templates on the fly
   *
   * @param {string} pluginName
   * @memberof QuizPlayerComponent
   */
  loadTemplate(pluginName: string, templateData) {
    console.log('Plugin Name', pluginName, templateData);
    this.templateLoader
      .load(pluginName)
      .then((moduleFactory: NgModuleFactory<Injector>) => {
        // Get the reference to the module
        const moduleRef: NgModuleRef<Injector> = moduleFactory.create(
          this.injector
        );
        // Select the default component to load from the templates. This referene
        // can be found at the module level where the  static reference to the
        // component is defined like `static defaultEntryComponent = Component;`
        const entryComponent = (moduleFactory.moduleType as any)
          .defaultEntryComponent;
        // Get the component factory
        const compFactory: ComponentFactory<
          Component
        > = moduleRef.componentFactoryResolver.resolveComponentFactory(
          entryComponent
        );

        // Create the component on the container specified
        const componentRef: ComponentRef<any> = this.vcRef.createComponent(
          compFactory
        );

        console.log('Show ans', this.showAnsState.getValue());

        // console.log('template: ', templateData, this.quizTemplateData);
        // prompt("Template Data", JSON.stringify(templateData))
        // Passing @Input() data to Template Component
        componentRef.instance.templateData = templateData;
        componentRef.instance.quizTemplateData = this.quizTemplateData;
        componentRef.instance.previewState = this.previewState;
        componentRef.instance.sourceState = this.sourceState;
        componentRef.instance.dashboardPreviewState = this.dashboardPreviewState;
        componentRef.instance.showAnsState = this.showAnsState;
        componentRef.instance.submit = this.submit;
        componentRef.instance.save = this.save;
        componentRef.instance.metadataSidebar = this.metadata;
        componentRef.instance.viewDevice = this.viewDevice;
        componentRef.instance.layoutView = this.layoutView;

        if (componentRef.instance.sourceStateChange)
          componentRef.instance.sourceStateChange.subscribe(val => {
            this.source = val;
          });

        if (componentRef.instance.updatePoints)
          componentRef.instance.updatePoints.subscribe(val => {
            this.points = val;
          });

        if (componentRef.instance.showAnswers)
          componentRef.instance.showAnswers.subscribe(val => {
            this.correctAnsPoints = val.correctAnsPoints;
            this.points = val.points;
            this.cdr.detectChanges();
          });

        if (componentRef.instance.nextPage)
          componentRef.instance.nextPage.subscribe(val => {
            this.selectedPage(val);
          });

        if (componentRef.instance.selectQuestion)
          componentRef.instance.selectQuestion.subscribe(val => {
            this.selectedPage(val);
          });

        if (componentRef.instance.editQuestion)
          componentRef.instance.editQuestion.subscribe(val => {
            this.editRedirect.next(val);
          });

        if (componentRef.instance.getAnswers)
          componentRef.instance.getAnswers.subscribe(val => {
            this.getAnswers.emit(val);
          });

        if (componentRef.instance.updateSelectedAnswers)
          componentRef.instance.updateSelectedAnswers.subscribe(val => {
            // this.correctAnsPoints = val.correctAnsPoints;
            // this.points = val.points;
            console.log(val);
          });

        // Getting list of Template methods available to Player
        // const compnentInstance: ITemplate = componentRef.instance;

        // Detect changes on the component
        componentRef.changeDetectorRef.detectChanges();
        // Detach the change reference subscriptions if any
        componentRef.onDestroy(() => {
          componentRef.changeDetectorRef.detach();
        });
      });
  }

  getQuizData(): void {
    this.quizPlayerService.getQuizData().subscribe(
      (quizData: Quiz) => {
        console.log('QuizData: ', quizData);
        this.quizData = quizData;
        console.log(quizData);
        this.totalPages = quizData.pages.length;
        this.loadCurrentPage();
      },
      error => {
        console.log(error);
      }
    );
  }

  onPreviewChange(type: boolean): void {
    this.viewMode = type;
    this.preview = type;
    this.previewState.next(type);
    // this.onAnsStatusChange(!type);
  }

  /**
   * @description This function toggles between show and hide correct answer
   * @param event Type = boolean
   * @returns void
   */
  onAnsStatusChange(ansChecked: boolean): void {
    console.log('HEADER FALG', ansChecked);
    this.showAnsState.next(ansChecked);
  }

  onSourceChange(sourceChecked: boolean): void {
    // let sourceChecked: boolean = event.target.checked;
    this.sourceState.next(sourceChecked);
  }

  /**
   * @description This function is used to formulate the points scored by the user
   * @param type Type = string (Single question or all questions at the same time)
   * @param quizData Type = QuizPages, QuizTemplate
   * @param overallScore Type = number
   * @param overallTotal Type = number
   * @returns void
   */
  formulatePoints(type, quizData, overallScore, overallTotal): void {
    quizData.forEach(quiz => {
      let templateData;
      if (type == 'single') {
        templateData = quiz.questions[0].templateData.data;
      } else {
        templateData = quiz.data;
      }

      let answerCheck: number = 0;
      let score: number = 0;
      let total: number = 0;

      templateData.options.forEach(async (opts: QuizOptions) => {
        templateData.validation.valid_response.value.forEach(async value => {
          if (value == opts.value && opts.selected) {
            answerCheck += 1;
          }
        });
      });

      total = templateData.validation.valid_response.score;

      if (
        templateData.validation.valid_response.value.length > 0 &&
        templateData.validation.valid_response.value.length == answerCheck
      ) {
        score = templateData.validation.valid_response.score;
        console.log(`Correct ${score}/${total}`);
      } else {
        score = 0;
        console.log(`Incorrect ${score}/${total}`);
      }
      overallScore += score;
      overallTotal += total;

      this.onAnsStatusChange(true);
    });
    console.log(quizData, `Total marks = ${overallScore}/${overallTotal}`);
  }

  /**
   * @description This function is fired to save Question.
   * @returns void
   */
  saveQuestion(): void {
    this.save.next();
  }

  /**
   * @description This function is fired to toggle metadata
   * @returns void
   */
  metaData() {
    this.metadata.next();
  }

  /**
   * @description This function is fired on submit button click.
   * @returns void
   */
  submitAnswer(): void {
    let overallScore: number = 0;
    let overallTotal: number = 0;

    if (this.displayType == 'single') {
      this.formulatePoints(
        this.displayType,
        this.quizData.pages,
        overallScore,
        overallTotal
      );
    } else {
      this.formulatePoints(
        this.displayType,
        this.quizTemplateData,
        overallScore,
        overallTotal
      );
    }
    // this.submit.next();
  }

  /**
   * @description This function navigates to the previous question in single question mode
   * @returns void
   */
  prevPage(): void {
    if (this.currentPage > 1) {
      this.currentPage = --this.currentPage;
    } else {
      this.currentPage = this.totalPages;
    }
    this.resetPoints();
    this.loadCurrentPage();
  }

  /**
   * @description This function is used to reset the points
   * @returns void
   */
  resetPoints(): void {
    this.correctAnsPoints = 0;
    this.points = 1;
  }

  /**
   * @description This function navigates to the next question in single question mode
   * @returns void
   */
  nextPage(): void {
    if (this.currentPage < this.totalPages) {
      this.currentPage = ++this.currentPage;
    } else {
      this.currentPage = 1;
    }
    this.loadCurrentPage();
  }

  /**
   * @description This function loads a template according to their order
   * @param val Type = number
   * @returns void
   */
  selectedPage(val: number): void {
    this.currentPage = val;
    this.resetPoints();
    this.loadCurrentPage();
  }

  redirectBack(val) {
    this.navigateBack.emit(val);
  }

  loadCurrentPage() {
    console.log('THIS this.subcategory:', this.subcategory);
    let currentPageObject;

    if (this.subcategory) {
      let subcategory = this.getModuleType(this.subcategory);

      this.quizData.pages.forEach(page => {
        if (page.questions[0].name == subcategory) {
          currentPageObject = page;
        }
      });
    } else {
      currentPageObject = this.quizData.pages[this.currentPage - 1];
    }

    if (!currentPageObject.loaded) {
      if (this.quizTemplateData && this.quizTemplateData.length > 0) {
        currentPageObject.loaded = true;
        this.loadQuestions(currentPageObject.questions as Array<PagesData>);
      } else {
        this.quizPlayerService
          .getAllTemplateData(currentPageObject)
          .subscribe((data: any) => {
            currentPageObject.loaded = true;

            (currentPageObject.questions as Array<object>).map(
              (question, questionIndx) =>
                (question['templateData'] = data[questionIndx])
            );
            // prompt('Load Questions', JSON.stringify(currentPageObject))
            this.loadQuestions(currentPageObject.questions as Array<PagesData>);
          });
      }
    } else {
      // prompt('Load Questions 1', JSON.stringify(currentPageObject))
      this.loadQuestions(currentPageObject.questions as Array<PagesData>);
    }
    this.sourceState.next(false);
  }

  /**
   * @description This function navigates to the previous page in the browser stack
   * @returns void
   */
  back() {
    this.location.back();
  }

  /**
   * @description This function loads the question from the parameter
   * @param questions Type = Array<PagesData>
   * @returns void
   */
  loadQuestions(questions: Array<PagesData>): void {
    // Replace with new element
    this.vcRef.clear();

    questions.forEach((question: any) => {
      console.log('Questions', question);
      // prompt('Load Questions', JSON.stringify(question.templateData))

      this.loadTemplate(question.name, question.templateData);
    });
  }

  /**
   * @description This function toggles the value of the theme checkbox
   * @returns void
   */
  toggletheme(): void {
    this.lightTheme = !this.lightTheme;
  }

  /**
   * @description Function to toggle the device view
   * @param $event object
   */
  deviceViewChange(event) {
    this.device = event.target.id;
    // console.log(event);
    this.viewDevice.next(event.target.id);
  }

  /**
   * @description Function to change the layout of the template
   * @param event
   */
  layoutChange(event) {
    this.layout = event.target.id;
    this.layoutView.next(event.target.id);
  }
}
