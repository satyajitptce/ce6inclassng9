import { Deserializable } from '../deserializable.model';

export class QuizOptions implements Deserializable {
  public label: string;
  public value: string;
  public selected: boolean;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
