import { Deserializable } from './../deserializable.model';
import { QuizTemplateData } from './quiz-template-data.model';

export class QuizTemplate implements Deserializable {
  public data: QuizTemplateData;
  public reference: string;
  public type: string;
  public template: string;
  public widget_type: string;

  deserialize(input: any): this {
    Object.assign(this, input);
    // console.log(input)
    this.data = new QuizTemplateData().deserialize(input.data);

    return this;
  }
}
