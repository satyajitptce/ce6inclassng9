import { Deserializable } from './../deserializable.model';
import { QuizOptions } from './quiz-options.model';
import { QuizValidations } from './quiz-validation.model';
import { QuizStimulus } from './quiz-stimulus.model';
import { QuizUI } from './quiz-ui.model';

interface Metadata {
  name: string;
  template_reference: string;
}
export class QuizTemplateData implements Deserializable {
  public options: QuizOptions[];
  public stimulus: QuizStimulus;
  public type: string;
  public validation: QuizValidations;
  public response_id: string;
  public template: string;
  public penalty_score: number;
  public _comment: string;
  public multiple_responses: boolean;
  public instant_feedback: boolean;
  public stimulus_audio: string;
  public feedback_attempts: number;
  public ui_style: QuizUI;
  public metadata: Metadata;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (this.options)
      this.options = input.options.map(option =>
        new QuizOptions().deserialize(option)
      );

    this.validation = new QuizValidations().deserialize(input.validation);

    this.stimulus = new QuizStimulus().deserialize(input.stimulus);

    this.ui_style = new QuizUI().deserialize(input.ui_style);

    return this;
  }
}
