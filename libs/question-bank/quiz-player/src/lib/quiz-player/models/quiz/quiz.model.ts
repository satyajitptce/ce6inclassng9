import { Deserializable } from './../deserializable.model';
import { QuizPage } from './quiz-page.model';

export class Quiz implements Deserializable {
  public name: string;
  public pages: QuizPage[];

  deserialize(input: any): this {
    Object.assign(this, input);

    this.pages = input.pages.map(page => new QuizPage().deserialize(page));

    return this;
  }
}
