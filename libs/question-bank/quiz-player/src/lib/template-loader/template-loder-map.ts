import * as core from '@angular/core';
import * as common from '@angular/common';
import * as forms from '@angular/forms';
import * as router from '@angular/router';
import * as platform from '@angular/platform-browser';
import * as rxjs from 'rxjs';
import * as tslib from 'tslib';
import * as bablePolyfill from 'babel-polyfill';
import * as dnd from '@angular/cdk/drag-drop';
import * as http from '@angular/common/http';
import * as Quill from 'quill';

import * as toast from 'ngx-toastr';
// import * as ImageDrop  from 'quill-image-drop-module';
import * as ImageResize from 'quill-image-resize';
import * as ngselect from '@ng-select/ng-select';

export const TEMPLATE_EXTERNALS_MAP = {
  // Angular dependencies
  'ng.core': core,
  'ng.common': common,
  'ng.forms': forms,
  'ng.router': router,
  'ng.platform': platform,
  'ng.dnd': dnd,
  'ng.http': http,
  'ng.select': ngselect,
  // '@ng-select/ng-select':
  //   'node_modules/@ng-select/ng-select/bundles/ng-select-ng-select.umd.js',
  // External support dependencies
  rxjs,
  tslib,
  bablePolyfill,
  Quill,
  toast,
  // ImageDrop,
  ImageResize
  // NgSelectModule
  // Include additional support dependencies here
};
