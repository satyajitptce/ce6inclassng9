import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { QuizPlayerComponent } from './quiz-player/quiz-player.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { NgSelectModule } from '@ng-select/ng-select';

// import { Location } from '@angular/common';
// import { NgbdAlertBasic } from '@ng-bootstrap/ng-bootstrap/';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFontAwesomeModule,
    NgSelectModule
  ],
  declarations: [QuizPlayerComponent],
  exports: [QuizPlayerComponent]

  // providers: [
  //   // Provide the default loader for the loader service
  //   { provide: TemplateLoaderService, useClass: ClientTemplateLoaderService },
  //   // Configuration provider, this would load the default config into memory when application initializes
  //   TemplateConfigProvider,
  //   {
  //     provide: APP_INITIALIZER,
  //     useFactory: (provider: TemplateConfigProvider) => () =>
  //       provider.loadConfig(),
  //     multi: true,
  //     deps: [TemplateConfigProvider],
  //   },
  // ],
})
export class QuizPlayerModule {}
