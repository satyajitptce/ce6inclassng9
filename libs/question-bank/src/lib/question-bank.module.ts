import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuestionBankViewComponent } from './components/question-bank-view/question-bank-view.component';
import { QuestionEditorComponent } from './components/question-editor/question-editor.component';
import { QuestionSelectorComponent } from './components/question-selector/question-selector.component';
import { TemplateEditComponent } from './components/template-edit/template-edit.component';
import { TemplatePreviewComponent } from './components/template-preview/template-preview.component';
import {
  NbCardModule,
  NbDialogModule,
  NbButtonModule,
  NbSelectModule,
  NbIconModule,
  NbCheckboxModule,
  NbDialogService,
  NbLayoutModule,
  NbSidebarModule
} from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LibConfigModule, DynamicComponentManifest } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { QuestionbankserviceService } from './services/questionbankservice.service';
import { QuestionCardViewComponent } from './components/question-card-view/question-card-view.component';
import { QbQuizViewerModule } from '@tce/qb-quiz-viewer';
import { QuestionCardListComponent } from './components/question-card-list/question-card-list.component';
import { QbQuestionCardListModule } from '@tce/qb-question-card-list';

const manifests: DynamicComponentManifest[] = [
  {
    componentId: 'mcq-ss',
    path: 'mcq-ss',
    loadChildren: () =>
      import(
        '../../../quiz-templates/src/lib/mcq-single-select/mcq-single-select.module'
      ).then(m => m.McqSingleSelectModule)
  },
  {
    componentId: 'edit-question',
    path: 'edit-question',
    loadChildren: () =>
      import('../../../template-editor/src/lib/template-editor.module').then(
        m => m.TemplateEditorModule
      )
  }
];

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forRoot(manifests),
    LibConfigModule.forChild(QuestionBankViewComponent),
    NbDialogModule.forChild(),
    NbSidebarModule,
    NbCardModule,
    NbButtonModule,
    NbSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NbIconModule,
    NbLayoutModule,
    NbCheckboxModule,
    CoreModule,
    QbQuizViewerModule,
    QbQuestionCardListModule
  ],
  declarations: [
    QuestionBankViewComponent,
    QuestionEditorComponent,
    QuestionSelectorComponent,
    TemplateEditComponent,
    TemplatePreviewComponent,
    QuestionCardViewComponent,
    QuestionCardListComponent
  ],
  exports: [
    QuestionBankViewComponent,
    QuestionEditorComponent,
    QuestionSelectorComponent,
    TemplateEditComponent,
    TemplatePreviewComponent
  ],
  entryComponents: [
    QuestionBankViewComponent,
    QuestionEditorComponent,
    QuestionSelectorComponent,
    TemplateEditComponent,
    TemplatePreviewComponent
  ],
  providers: [QuestionbankserviceService, NbDialogService]
})
export class QuestionBankModule {}
