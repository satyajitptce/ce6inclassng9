import {
  Component,
  OnInit,
  ViewContainerRef,
  ViewChild,
  OnChanges,
  AfterViewInit,
  OnDestroy,
  Input,
  ViewEncapsulation,
  Output,
  EventEmitter,
  HostListener,
  ComponentFactoryResolver
} from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { RequestApiService, CoreModule } from '@tce/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { QuestionEditorComponent } from '../question-editor/question-editor.component';
import { LibConfigService } from '@tce/lib-config';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';
import { QuestionbankserviceService } from '../../services/questionbankservice.service';
import { QuestionEditorService } from '@tce/template-editor';
import { UtilityConfigService } from '../../services/utility-config.service';
import { QuestionEditorPreviewComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-preview/question-editor-preview.component';
import { PlanningModeService } from '../../../../../planning-mode/src/lib/services/planning-mode.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { QbQuestionCardListComponent } from 'libs/qb-question-card-list/src/lib/components/qb-question-card-list/qb-question-card-list.component';
@Component({
  selector: 'tce-question-bank-view',
  templateUrl: './question-bank-view.component.html',
  styleUrls: ['./question-bank-view.component.scss']
  // encapsulation: ViewEncapsulation.ShadowDom
})
export class QuestionBankViewComponent implements OnInit, OnDestroy, OnChanges {
  @ViewChild('editTemplateView', { static: false, read: ViewContainerRef })
  editTemplateView: ViewContainerRef | undefined;
  @Input() public selectedGrade: any;
  @Input() public selectedSubject: any;
  @Input() public chapters: any;
  @Input() public topics: any;
  @Input() public selectedChapter: any;
  @Input() public selectedTopic: any;
  @Output() public addUpdateQuestion = new EventEmitter();
  public allQuestionData;
  public check: any;
  public showCheckBox: boolean;
  public index;
  public showQuestions = true;
  public showQuestionEdit = false;
  public showFetchQuestionData = false;
  public showEdit = false;
  public edit_id;
  public questionFilters: any;
  public editQuestionMode;
  public getDataSubscription: Subscription;
  public quizQuestionList: Array<string>;
  public allDataArray: Array<any> = [];

  public jsonFormat: any;
  public isAllQuestionsLoaded: boolean = true;
  public loaderQCountDisplay = 'Loading question ';
  public noQuestionsAvailable: any = false;
  public statusMessage = '';
  @ViewChild('questionList', { read: ViewContainerRef, static: true })
  questionList: ViewContainerRef;
  questionListRef: any;

  constructor(
    private dialogService: NbDialogService,
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private libConfigService: LibConfigService,
    private questionBankService: QuestionbankserviceService,
    private questionEditorService: QuestionEditorService,
    private utilityConfigService: UtilityConfigService,
    private planModeService: PlanningModeService,
    private toastr: ToastrService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {}

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (event.target.id === 'question-more-option') {
      //console.log('question-more-option---selected');
      this.showEdit = true;
    } else {
      this.showEdit = false;
    }
  }
  ngOnDestroy(): void {
    if (this.getDataSubscription) {
      this.getDataSubscription.unsubscribe();
    }
  }

  ngOnInit(): void {
    // this.planModeService.qbChangeChapterTopicGet().subscribe(value => {
    //   if (value) {
    //     console.log('allData ', value);

    //     // this.getQuestionData();
    //     this.questionListInputs();
    //     this.planModeService.qbChangeChapterTopicSet(false);
    //   }
    // });

    this.restartQbView();
    // this.loadQuestionList();
    // this.statusMessage = 'No questions found.';
    // this.questionBankService.editQuestionMode$.subscribe(mode => {});
    // this.questionBankService.noQuestionsAvailableBroadcast$.subscribe(flag => {
    //   this.noQuestionsAvailable = flag;
    //   if (flag) {
    //     this.isAllQuestionsLoaded = true;
    //   }
    // });
    // this.planModeService.qbChangeChapterTopicGet().subscribe(value => {
    //   if (value) {
    //     this.questionListRef.destroy();
    //     this.loadQuestionList();
    //     this.planModeService.qbChangeChapterTopicSet(false);
    //   }
    // });
    // this.questionBankService.getQuizQuestionList().subscribe(questions => {
    //   this.quizQuestionList = questions;
    // });
    // // default load question card viewer
    // this.getQuestionData();
    // this.createFinalCardData1();
  }

  ngOnChanges() {
    // console.log('selectedTopic ', this.selectedChapter, this.selectedTopic);
  }

  restartQbView() {
    this.statusMessage = 'No questions found.';
    this.questionBankService.editQuestionMode$.subscribe(mode => {});
    this.questionBankService.noQuestionsAvailableBroadcast$.subscribe(flag => {
      this.noQuestionsAvailable = flag;
      if (flag) {
        this.isAllQuestionsLoaded = true;
      }
    });

    // this.planModeService.qbChangeChapterTopicGet().subscribe(value => {
    //   if (value) {
    //     this.getQuestionData();
    //     this.planModeService.qbChangeChapterTopicSet(false);
    //   }
    // });

    this.planModeService.getQuizQuestionList().subscribe(questions => {
      this.quizQuestionList = questions;
      console.log('questionList ', this.quizQuestionList);
    });

    this.planModeService.getSelectQuiz().subscribe(select => {
      this.showCheckBox = select;
      console.log('checkbox', this.showCheckBox);
    });

    // default load question card viewer
    this.getQuestionData();
    this.createFinalCardData1();
  }

  destroyQbView() {
    this.getDataSubscription.unsubscribe();
  }

  async getQuestionData() {
    //resets
    this.noQuestionsAvailable = false;
    this.isAllQuestionsLoaded = false;
    this.questionBankService.questionsLength = 0;
    this.allQuestionData = [];
    this.loaderQCountDisplay = 'Loading questions...';
    //---------------------------

    let newSelectedTopic: string;
    if (this.selectedTopic) {
      if (this.selectedTopic !== 'none') {
        newSelectedTopic = this.selectedTopic.topicId;
      } else {
        newSelectedTopic = 'none';
      }
    }
    this.questionFilters = {
      selectedGrade: this.selectedGrade.id,
      selectedSubject: this.selectedSubject.subjectId,
      selectedChapter: this.selectedChapter.chapterId,
      selectedTopic: newSelectedTopic
    };
    await this.getCustomQuestions();
    await this.getTataQuestions();
  }

  getTataQuestions(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      this.questionBankService.getQuestionBankData(
        'tata',
        this.questionFilters
      );
      resolve(true);
    });
  }

  getCustomQuestions(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let customQuestion = this.questionBankService.getQuestionBankData(
        'custom',
        this.questionFilters
      );
      resolve(true);
    });
  }

  createFinalCardData1() {
    //resets
    this.questionBankService.questionsLength = 0;
    this.isAllQuestionsLoaded = false;
    //---------------------------------

    let random = Math.random();

    this.getDataSubscription = this.questionBankService.questionbankDataBroadcast$.subscribe(
      (qData: Array<string>) => {
        console.log('qData ', qData);
        qData.forEach(data => {
          this.allDataArray.push(data);
        });
        // console.log('allDataArray ', this.allDataArray);

        this.noQuestionsAvailable = false;
        if (qData) {
          let tataIndex = -1;
          let questionData: any;
          // questionData = qData;

          // let dataStr = JSON.stringify(questionData);

          this.allDataArray.forEach(questionData => {
            if (questionData.questionDetails) {
              if (typeof questionData.questionDetails == 'string') {
                console.log('charAt ', questionData.questionDetails.charAt(0));

                if (questionData.questionDetails.charAt(0) === '<') {
                  //console.log('<<-- FOUND TESTEDGE QUESTION XML-->> ');
                  questionData.questionDetails = this.utilityConfigService.convertToJson(
                    questionData.questionDetails
                  );
                } else {
                  questionData.questionDetails = JSON.parse(
                    questionData.questionDetails
                  );
                }
              }
              if (questionData.questionSourceType === 'tata') {
                // let dataStr = questionData.questionDetails;
                console.log(
                  'questionData.questionDetails',
                  questionData.questionDetails
                );
                if (questionData.questionDetails.questionDetails) {
                  questionData.questionDetails.questionDetails.encryptedPath =
                    questionData.encryptedPath;
                  questionData.questionDetails.questionDetails.fileUrl = this.requestApiService.getUrl(
                    'getFile'
                  );
                  if (
                    questionData.questionDetails.questionDetails.qtext &&
                    questionData.questionDetails.questionDetails.qtext.indexOf(
                      '<img src'
                    ) != -1
                  ) {
                    let qtextImg = questionData.questionDetails.questionDetails.qtext.split(
                      "'"
                    );
                    questionData.questionDetails.questionDetails[
                      'questionTitle'
                    ] =
                      "<img src='" +
                      questionData.questionDetails.questionDetails.fileUrl +
                      '/' +
                      questionData.encryptedPath +
                      '/' +
                      qtextImg[1] +
                      "'>";
                  } else if (
                    questionData.questionDetails.questionDetails.data &&
                    questionData.questionDetails.questionDetails.data.questions
                  ) {
                    console.log('elseIf');

                    questionData.questionDetails.questionDetails[
                      'questionTitle'
                    ] = this.stripHtml(
                      questionData.questionDetails.questionDetails.data
                        .questions[0].data.stimulus
                    );
                    if (
                      questionData.questionDetails.questionDetails.data
                        .questions[0].data.stimulus_media
                    ) {
                      questionData.questionDetails.questionDetails['image'] =
                        questionData.questionDetails.questionDetails.fileUrl +
                        '/' +
                        questionData.encryptedPath +
                        '/' +
                        questionData.questionId +
                        '/images/' +
                        questionData.questionDetails.questionDetails.data
                          .questions[0].data.stimulus_media;
                    }
                  } else {
                    questionData.questionDetails.questionDetails[
                      'questionTitle'
                    ] = this.stripHtml(
                      questionData.questionDetails.questionDetails.qtext
                    );
                  }

                  questionData.questionDetails =
                    questionData.questionDetails.questionDetails;
                }
              } else {
                questionData.questionDetails.encryptedPath =
                  questionData.encryptedPath;
                questionData.questionDetails.fileUrl = this.requestApiService.getUrl(
                  'getFile'
                );
                if (questionData.questionDetails.data.stimulus) {
                  if (questionData.questionDetails.data.stimulus.label) {
                    questionData.questionDetails[
                      'questionTitle'
                    ] = this.stripHtml(
                      questionData.questionDetails.data.stimulus.label
                    );
                  } else {
                    questionData.questionDetails[
                      'questionTitle'
                    ] = this.stripHtml(
                      questionData.questionDetails.data.stimulus
                    );
                  }
                }
                if (questionData.questionDetails.data.questions) {
                  questionData.questionDetails[
                    'questionTitle'
                  ] = this.stripHtml(
                    questionData.questionDetails.data.questions[0].data.stimulus
                  );
                  if (
                    questionData.questionDetails.data.questions[0].data
                      .stimulus_media
                  ) {
                    questionData.questionDetails['image'] =
                      questionData.questionDetails.fileUrl +
                      '/' +
                      questionData.encryptedPath +
                      '/' +
                      questionData.questionId +
                      '/images/' +
                      questionData.questionDetails.data.questions[0].data
                        .stimulus_media;
                  }
                }
              }

              // questionData.questionDetails.encryptedPath =
              //   questionData.encryptedPath;
              // questionData.questionDetails.fileUrl = this.requestApiService.getUrl(
              //   'getFile'
              // );
              // if (questionData.questionDetails.qtext) {
              //   if (
              //     questionData.questionDetails.qtext.indexOf('<img src') != -1
              //   ) {
              //     let qtextImg = questionData.questionDetails.qtext.split("'");
              //     questionData.questionDetails['questionTitle'] =
              //       "<img src='" +
              //       questionData.questionDetails.fileUrl +
              //       '/' +
              //       questionData.questionDetails.encryptedPath +
              //       '/' +
              //       qtextImg[1] +
              //       "'>";
              //   } else {
              //     questionData.questionDetails['questionTitle'] = this.stripHtml(
              //       questionData.questionDetails.qtext
              //     );
              //   }
              // }
              // if (questionData.questionDetails.data) {
              //console.log('datadata ', question);
              // if (questionData.questionDetails.data.stimulus) {
              //   if (questionData.questionDetails.data.stimulus.label) {
              //     questionData.questionDetails[
              //       'questionTitle'
              //     ] = this.stripHtml(
              //       questionData.questionDetails.data.stimulus.label
              //     );
              //   } else {
              //     questionData.questionDetails[
              //       'questionTitle'
              //     ] = this.stripHtml(
              //       questionData.questionDetails.data.stimulus
              //     );
              //   }
              // }
              // if (questionData.questionDetails.data.questions) {
              //   questionData.questionDetails['questionTitle'] = this.stripHtml(
              //     questionData.questionDetails.data.questions[0].data.stimulus
              //   );
              //   if (
              //     questionData.questionDetails.data.questions[0].data
              //       .stimulus_media
              //   ) {
              //     questionData.questionDetails['image'] =
              //       questionData.questionDetails.fileUrl +
              //       '/' +
              //       questionData.encryptedPath +
              //       '/' +
              //       questionData.questionId +
              //       '/images/' +
              //       questionData.questionDetails.data.questions[0].data
              //         .stimulus_media;
              //   }
              // }
              // }
            }
          });
          // if (questionData.questionSourceType) {
          //   this.allQuestionData.push(questionData);
          // } else {
          //   tataIndex += 1;
          //   this.allQuestionData.splice(tataIndex, 0, questionData);
          // }
          // this.loaderQCountDisplay =
          //   'Loading question ' +
          //   this.allQuestionData.length +
          //   ' of ' +
          //   this.questionBankService.questionsLength;
          // if (
          //   this.allQuestionData.length ===
          //   this.questionBankService.questionsLength
          // ) {
          //   this.isAllQuestionsLoaded = true;
          // } else {
          //   this.isAllQuestionsLoaded = false;
          // }
          //console.log("All Questions Loaded : ",this.isAllQuestionsLoaded, " totalQ-->>",this.questionBankService.questionsLength + " currentQ-->>", this.allQuestionData.length)
          console.log('qData 1', this.allDataArray);
          this.allQuestionData = this.allDataArray;
          const questionIds = this.quizQuestionList.map(
            question => question['questionId']
          );
          this.allQuestionData.forEach(question => {
            if (questionIds.includes(question.questionId)) {
              let index = questionIds.indexOf(question.questionId);
              this.quizQuestionList[index] = question;
            }
          });
        }
      }
    );
  }

  stripHtml(html) {
    // Create a new div element
    var temporalDivElement = document.createElement('div');
    // Set the HTML content with the providen
    temporalDivElement.innerHTML = html;
    // Retrieve the text property of the element (cross-browser support)
    return temporalDivElement.textContent || temporalDivElement.innerText || '';
  }

  onSelect(value) {
    if (value === 'done') {
      // this.showCheckBox = !this.showCheckBox;
      this.planModeService.setQuizQuestionList([]);
      this.planModeService.setSelectQuiz(false);
    } else {
      // this.showCheckBox = true;
      this.planModeService.setSelectQuiz(true);
    }
    //console.log(value, this.showCheckBox);
  }

  getQuestionBankData(value) {
    this.questionBankService.getQuestionBankData(value, this.questionFilters);
  }

  addQuestion(questionData) {
    this.questionBankService.addQuestion(questionData, null);
    this.questionBankService.addQuestionBroadcast$.subscribe(data => {
      //console.log('add Question---------->', data);
    });
  }
  editQuestion(questionData) {
    //commented by usman for testing editApi
    // this.questionBankService.editQuestion(questionData);
    this.questionBankService.editQuestionBroadcast$.subscribe(data => {
      //console.log('edit Question---------->', data);
    });
  }

  selectQuestions(event, question) {
    console.log('event ', event, question);
  }

  // changeChapter(event) {
  //   // this.destroyQbView();
  //   console.log('chapterchange ', event);
  //   this.chapters.forEach(chapter => {
  //     if (event.chapterId === chapter.chapterId) {
  //       this.topics = chapter.topics;
  //     }
  //   });
  //   this.selectedChapter = event;
  //   this.selectedTopic = 'none';
  //   this.questionListRef.destroy();
  //   this.loadQuestionList();
  //   // this.restartQbView();
  //   // this.getQuestionData();
  //   // this.updateQuestionBankData();
  // }

  // changeTopic(event) {
  //   // this.destroyQbView();
  //   this.selectedTopic = event;
  //   this.questionListRef.destroy();
  //   this.loadQuestionList();
  //   // this.restartQbView();
  //   // this.getQuestionData();
  //   // this.updateQuestionBankData();
  // }

  // updateQuestionBankData() {
  //   this.planModeService.qbChangeChapterTopicSet(true);
  // }

  // loadQuestionList() {
  //   const componentFactory = this.componentFactoryResolver.resolveComponentFactory(
  //     QbQuestionCardListComponent
  //   );
  //   this.questionList.clear();

  //   this.questionListRef = this.questionList.createComponent<any>(
  //     componentFactory
  //   );
  //   this.questionListInputs();
  //   // this.questionListRef.instance.selectedGrade = this.selectedGrade;
  //   // this.questionListRef.instance.selectedSubject = this.selectedSubject;
  //   // this.questionListRef.instance.selectedChapter = this.selectedChapter;
  //   // this.questionListRef.instance.selectedTopic = this.selectedTopic;
  // }

  // questionListInputs() {
  //   console.log('allData ', this.selectedTopic);

  //   this.questionListRef.instance.selectedGrade = this.selectedGrade;
  //   this.questionListRef.instance.selectedSubject = this.selectedSubject;
  //   this.questionListRef.instance.selectedChapter = this.selectedChapter;
  //   this.questionListRef.instance.selectedTopic = this.selectedTopic;
  // }
}
