import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCardViewComponent } from './question-card-view.component';

describe('QuestionCardViewComponent', () => {
  let component: QuestionCardViewComponent;
  let fixture: ComponentFixture<QuestionCardViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCardViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCardViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
