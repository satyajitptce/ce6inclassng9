import {
  Component,
  EventEmitter,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  Output
} from '@angular/core';
import { QuestionbankserviceService } from '../../services/questionbankservice.service';

@Component({
  selector: 'tce-question-card-view',
  templateUrl: './question-card-view.component.html',
  styleUrls: ['./question-card-view.component.scss']
})
export class QuestionCardViewComponent implements OnInit, OnChanges {
  @Input() question;
  @Input() index;
  @Input() showCheckBox;
  @Input() quizQuestionList: Array<string>;
  @Output() openQtnEditor = new EventEmitter();
  @Output() dltQuestion = new EventEmitter();
  @Output() openpopup = new EventEmitter();
  public showEdit = false;
  public edit_id;
  public checkedQuestion: boolean;

  constructor(public questionBankService: QuestionbankserviceService) {}

  ngOnInit(): void {
    this.questionBankService.getEditQuestionIndex().subscribe(index => {
      this.edit_id = index;
    });
  }

  ngOnChanges() {
    if (this.quizQuestionList.includes(this.question)) {
      this.checkedQuestion = true;
    } else {
      this.checkedQuestion = false;
    }
  }

  openQuestionEditor() {
    if (this.edit_id === undefined) {
      //console.log(this.edit_id);
    } else if (this.index !== this.edit_id) {
      this.showEdit = !this.showEdit;
      //console.log('not equal');
    }
    if (this.edit_id === this.index) {
      this.showEdit = false;
    }

    this.questionBankService.setEditQuestionIndex(this.index);
    this.showEdit = !this.showEdit;
    this.openQtnEditor.emit(this.index);
  }

  openPopup(mode, question) {
    let data = {
      data: question,
      mode: mode
    };
    console.log('questionData ', question);
    this.openpopup.emit(data);
  }

  deleteQuestion(questionId) {
    this.dltQuestion.emit(questionId);
  }

  @HostListener('document:click', ['$event'])
  clickout(event) {
    if (event.target.id === 'question-more-option') {
      console.log('question-more-option---selected');
      this.showEdit = true;
    } else {
      this.showEdit = false;
      this.edit_id = null;
    }
  }

  selectQuestions(event) {
    console.log('quizQuestions event', event, this.question);
    if (event.target.checked) {
      this.quizQuestionList.push(this.question);
    } else {
      let index = this.quizQuestionList.indexOf(this.question);
      this.quizQuestionList.splice(index, 1);
    }
    // this.questionBankService.setQuizQuestionList(this.quizQuestionList);
    console.log('quizQuestionList ', this.quizQuestionList);
  }
}
