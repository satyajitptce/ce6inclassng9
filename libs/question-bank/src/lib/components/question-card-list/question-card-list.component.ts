import { Component, Input, OnInit } from '@angular/core';
import { NbDialogService } from '@nebular/theme';
import { PlanningModeService } from 'libs/planning-mode/src/lib/services/planning-mode.service';
import { QuestionEditorContainerComponent } from 'libs/template-editor/src/lib/question-editor/component/question-editor-container/question-editor-container.component';
import { ToastrService } from 'ngx-toastr';
import { QuestionbankserviceService } from '../../services/questionbankservice.service';

@Component({
  selector: 'tce-question-card-list',
  templateUrl: './question-card-list.component.html',
  styleUrls: ['./question-card-list.component.scss']
})
export class QuestionCardListComponent implements OnInit {
  @Input() allQuestionData: Array<object>;
  @Input() quizQuestionList;
  @Input() showCheckBox;
  @Input() selectedGrade;
  @Input() selectedSubject;
  @Input() selectedChapter;
  @Input() selectedTopic;

  public showEdit = false;
  public edit_id;

  constructor(
    private dialogService: NbDialogService,
    private planModeService: PlanningModeService,
    private questionBankService: QuestionbankserviceService,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
    console.log(
      'gradeSub ',
      this.selectedGrade,
      this.selectedSubject,
      this.selectedChapter,
      this.selectedTopic
    );

    if (this.allQuestionData)
      console.log('allQuestionData ', this.allQuestionData);
    // this.allQuestionData = this.allQuestionData.map(question => {
    //   if (question['questionDetails']) {
    //     return question;
    //   }
    // });
  }

  openQuestionEditor(i) {
    if (this.edit_id === undefined) {
      //console.log(this.edit_id);
    } else if (i !== this.edit_id) {
      this.showEdit = !this.showEdit;
      //console.log('not equal');
    }
    if (this.edit_id === i) {
      this.showEdit = false;
    }

    this.edit_id = i;
    this.showEdit = !this.showEdit;
  }

  // openPopup(questionData) {
  //   //console.log('questionData', questionData);
  //   let qData: any;
  //   let qType: any;

  //   if (questionData.data) {
  //     if (questionData.data.questionSourceType) {
  //       if (questionData.mode === 'edit') {
  //         qType = 'custom'; //changes done by usman. To load custom question in preview mode
  //         qData = JSON.parse(JSON.stringify(questionData.data));
  //       } else {
  //         questionData.data.questionDetails['questionId'] =
  //           questionData.data.questionId;
  //         qType = 'TCE'; //changes done by usman. To load custom question in preview mode
  //         qData = questionData.data.questionDetails;
  //       }
  //     } else {
  //       qType = 'TCE';
  //       qData = questionData.data.questionDetails;
  //     }
  //     this.open(false, false, qData, qType);
  //   } else {
  //     qType = 'custom';
  //     this.open(false, false, qData, qType);
  //   }
  //   this.showEdit = !this.showEdit;
  // }

  // protected open(
  //   closeOnBackdropClick: boolean,
  //   closeOnEsc: boolean,
  //   qData,
  //   qType
  // ) {
  //   this.dialogService
  //     .open(QuestionEditorContainerComponent, {
  //       closeOnBackdropClick,
  //       closeOnEsc,
  //       context: { questionData: qData, questionType: qType }
  //     })
  //     .onClose.subscribe(save => {
  //       if (save) {
  //         this.planModeService.qbChangeChapterTopicSet(true);
  //       }
  //     });
  // }

  // deleteQuestion(questionId) {
  //   this.questionBankService.deleteQuestion(questionId);
  //   this.questionBankService.deleteQuestionBroadcast$.subscribe(data => {
  //     console.log('delete Question---------->', data);
  //     if (data && data['status'] === 'success') {
  //       this.toastr.success('Question deleted successfully!');
  //       this.planModeService.qbChangeChapterTopicSet(true);
  //     }
  //   });
  // }
}
