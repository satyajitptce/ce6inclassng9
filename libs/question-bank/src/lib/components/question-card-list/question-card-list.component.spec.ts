import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionCardListComponent } from './question-card-list.component';

describe('QuestionCardListComponent', () => {
  let component: QuestionCardListComponent;
  let fixture: ComponentFixture<QuestionCardListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuestionCardListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionCardListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
