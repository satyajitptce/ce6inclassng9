import { Component, OnInit } from '@angular/core';

import { NbDialogRef,NbDialogService } from '@nebular/theme';
import {TemplatePreviewComponent} from '../template-preview/template-preview.component'
@Component({
  selector: 'tce-question-editor',
  templateUrl: './question-editor.component.html',
  styleUrls: ['./question-editor.component.scss']
})
export class QuestionEditorComponent implements OnInit {

  constructor(private dialogService: NbDialogService,protected ref: NbDialogRef<QuestionEditorComponent>) { }

  ngOnInit(): void {
  }
  dismiss() {
    this.ref.close();
  }
  onItemSelected(value){
    if(value === 'preview'){
      this.openPreview()
    }
  }
  openPreview(){
    this.open(false,false);
  }

  protected open(closeOnBackdropClick: boolean,closeOnEsc: boolean) {
    this.dialogService.open(TemplatePreviewComponent, { closeOnBackdropClick,closeOnEsc });
  }
}
