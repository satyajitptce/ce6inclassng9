import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'tce-question-selector',
  templateUrl: './question-selector.component.html',
  styleUrls: ['./question-selector.component.scss']
})
export class QuestionSelectorComponent implements OnInit {
  selectedQuestionType:any;
  questionList = [
    {
      id:11,
      title:'Multiple Choice - Standard',
      path:'mcq-ss'
    },
    {
      id:12,
      title:'Multiple Choice - Multiple response',
      path:'mcq-ms'
    },
    {
      id:13,
      title:'Multiple Choice - True or false',
      path:'mcq-tf'
    },
    {
      id:14,
      title:'Multiple Choice - Option Image',
      path:'mcq-oi'
    },
    {
      id:15,
      title:'Multiple Choice - Qstem Image',
      path:'mcq-qi'
    },
    {
      id:16,
      title:'Multiple Choice - Matrix Standard',
      path:'mcq-mxs'
    },
    {
      id:17,
      title:'Multiple Choice - Matrix Inline',
      path:'mcq-mxi'
    },
    {
      id:18,
      title:'Multiple Choice - Matrix Label',
      path:'mcq-mxl'
    },
    {
      id:19,
      title:'Multiple Question',
      path:'multiple-questio'
    },
    {
      id:21,
      title:'Cloze with drag & drop',
      path:'fib-drag-drop'
    },
    {
      id:22,
      title:'cloze with dropdown',
      path:'fib-dropdown'
    },
    {
      id:23,
      title:'cloze with text',
      path:'fib-text'
    },
    {
      id:24,
      title:'Label image drag drop',
      path:'fib-image-drag-d'
    },
    {
      id:25,
      title:'Label image dropdown',
      path:'fib-image-dropdo'
    },
    {
      id:26,
      title:'Label image text',
      path:'fib-image-text'
    },
    {
      id:31,
      title:'Match list',
      path:'match-list'
    },
    {
      id:32,
      title:'Order list',
      path:'order-list'
    },
    {
      id:33,
      title:'Sort list',
      path:'sort-list'
    },
    {
      id:34,
      title:'Classify Group',
      path:'classify-gr'
    },
    {
      id:41,
      title:'Essay with rich text',
      path:'rich-text'
    },
    {
      id:42,
      title:'Essay with plain text',
      path:'plain-text'
    },
    {
      id:43,
      title:'Short text',
      path:'short-text'
    },
    {
      id:51,
      title:'Hotspot',
      path:'hotspot'
    },
    {
      id:52,
      title:'Token highlight',
      path:'token'
    }
  ]
  constructor() { }

  ngOnInit(): void {
    this.selectedQuestionType = this.questionList[0].id;

  }
  selectedQuestionTypeEvent(data:any){
    this.selectedQuestionType = data.id;
    
  }
}

