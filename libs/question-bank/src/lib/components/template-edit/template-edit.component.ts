import { Component, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { LibConfigService } from '@tce/lib-config';
import { McqSingleSelectLayoutComponent } from '@tce/quiz-templates';
@Component({
  selector: 'tce-template-edit',
  templateUrl: './template-edit.component.html',
  styleUrls: ['./template-edit.component.scss']
})
export class TemplateEditComponent implements OnInit {
  @ViewChild('templateView', { static: false, read: ViewContainerRef })
  templateView: ViewContainerRef | undefined;
  constructor(private libConfigService: LibConfigService) {}

  ngOnInit(): void {
    this.loadTemplateView();
  }
  loadTemplateView() {
    this.libConfigService
      .getComponentFactory<McqSingleSelectLayoutComponent>('mcq-ss')
      .subscribe({
        next: componentFactory => {
          if (!this.templateView) {
            return;
          }
          const ref = this.templateView.createComponent(componentFactory);
          ref.changeDetectorRef.detectChanges();
        },
        error: err => {
          console.warn(err);
        }
      });
  }
}
