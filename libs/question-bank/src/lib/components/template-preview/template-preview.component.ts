import { Component, OnInit } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';
@Component({
  selector: 'tce-template-preview',
  templateUrl: './template-preview.component.html',
  styleUrls: ['./template-preview.component.scss']
})
export class TemplatePreviewComponent implements OnInit {

  constructor(protected ref: NbDialogRef<TemplatePreviewComponent>) { }

  ngOnInit(): void {
  }
  dismiss() {
    this.ref.close();
  }
}
