import { async, TestBed } from '@angular/core/testing';
import { QuestionBankModule } from './question-bank.module';

describe('QuestionBankModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [QuestionBankModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(QuestionBankModule).toBeDefined();
  });
});
