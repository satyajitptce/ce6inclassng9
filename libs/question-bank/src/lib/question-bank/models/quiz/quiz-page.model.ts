import { Deserializable } from '../deserializable.model';
import { QuizQuestion } from './quiz-question.model';

export class QuizPage implements Deserializable {
  public name: string;
  public loaded: boolean;
  public questions: QuizQuestion[];

  deserialize(input: any): this {
    Object.assign(this, input);

    this.questions = input.questions.map(question =>
      new QuizQuestion().deserialize(question)
    );

    return this;
  }
}
