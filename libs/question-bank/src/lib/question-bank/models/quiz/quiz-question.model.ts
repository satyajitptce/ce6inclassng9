import { Deserializable } from '../deserializable.model';
import { QuizTemplateData } from '../quiz-template/quiz-template-data.model';
import { QuizTemplate } from '../quiz-template/quiz-template.model';

export class QuizQuestion implements Deserializable {
  public name: string;
  public dataUrl: string;
  public templateData: QuizTemplate;

  deserialize(input: any): this {
    Object.assign(this, input);

    if (input.templateData)
      this.templateData = new QuizTemplate().deserialize(input.templateData);

    return this;
  }
}
