import { Deserializable } from '../deserializable.model';

export class QuizStimulus implements Deserializable {
  public label: string;
  public value: string;
  public feedbackInline: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
