import { Deserializable } from '../deserializable.model';

export class QuizValidations implements Deserializable {
  public scoring_type: string;
  public valid_response: {
    score: number;
    value: Array<any>;
  };
  public penalty: number;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
