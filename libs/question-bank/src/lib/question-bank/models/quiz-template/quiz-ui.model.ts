import { Deserializable } from '../deserializable.model';

export class QuizUI implements Deserializable {
  public type: string;
  public _comment: string;

  deserialize(input: any): this {
    return Object.assign(this, input);
  }
}
