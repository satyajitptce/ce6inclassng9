import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class QuizTemplateBrokerService {
  private previewStatus: Subject<boolean> = new Subject<boolean>();

  get previewStatusBroadCaster$() {
    return this.previewStatus;
  }

  get previewStatusObservable$() {
    return this.previewStatus.asObservable();
  }
}
