export interface PagesData {
  name: string;
  dataUrl: string;
}

export interface QuestionsData {
  name: string;
  loaded: boolean;
  questions: {
    [index: number]: PagesData;
  };
}

export interface QuizData {
  name: string;
  pages: {
    [index: number]: QuestionsData;
  };
}

// export interface QuizTemplateData{
//     reference: string,
//     data: object
// }

// export interface QuizTemplateData{
//     reference: string,
//     data:  QuizTemplateOptions;
// }

// export interface QuizTemplateOptions{
//     options: QuizOptions[]
// }

// export interface QuizOptions{
//     label: string,
//     value:string
// }

// templateData: string;
// previewState: BehaviorSubject<boolean>;
// submit: Subject<void>
