import { Injectable } from '@angular/core';
import { RequestApiService, Resource } from '@tce/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject, Observable, BehaviorSubject, ReplaySubject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { error } from 'console';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class QuestionbankserviceService {
  editCustomQuestionMode = false;
  private editQuestionMode = new ReplaySubject();
  public editQuestionMode$ = this.editQuestionMode.asObservable();

  private url = this.requestApiService.getUrl('getFile');
  public questionDataSource = [];
  public editQuestionData: Object;
  public questionbankDataBroadcast = new Subject();
  public questionbankDataBroadcast$ = this.questionbankDataBroadcast.asObservable();

  public noQuestionsAvailableBroadcast = new Subject();
  public noQuestionsAvailableBroadcast$ = this.noQuestionsAvailableBroadcast.asObservable();
  // public allQuestions: BehaviorSubject<any> = new BehaviorSubject<any>(null);

  public addQuestionBroadcast = new Subject();
  public addQuestionBroadcast$ = this.addQuestionBroadcast.asObservable();

  public editQuestionBroadcast = new Subject();
  public editQuestionBroadcast$ = this.editQuestionBroadcast.asObservable();

  public deleteQuestionBroadcast = new Subject();
  public deleteQuestionBroadcast$ = this.deleteQuestionBroadcast.asObservable();
  public questionFilters: any;
  public questionsLength: number = 0;
  public editQuestionIndex = new Subject();
  public qEditQuestionIndex = new Subject();
  // public quizQuestionList: BehaviorSubject<Array<string>> = new BehaviorSubject<
  //   any
  // >([]);
  // public selectQuiz: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
  //   false
  // );

  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private toastr: ToastrService
  ) {
    let obj;
    this.getQuestionType().subscribe(data => {
      //console.log(data);
    });

    //console.log('editQues===========service', this.editQuestionData);
  }
  getQuestionType(): Observable<any> {
    return this.http.get('assets/config.json');
  }

  setQuestionEditMode(value) {
    this.editQuestionMode.next(value);
  }

  // changeEditQuestionData(data) {
  //   console.log('editQuestionData befre ', data);

  //   this.allQuestions.next(data);
  // }

  // getEditQuestionData() {
  //   return this.allQuestions;
  // }

  getQuestionBankData(value, questionFilters) {
    let qData: any;
    //console.log('filters ', questionFilters);
    this.questionFilters = questionFilters;
    let requestParams;
    if (value === 'custom') {
      // requestParams =
      //   '?gradeId=grd-74cc96e3-3ba0-44af-b60e-722e44cdefdc&subjectId=sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b&chapterId=cchp-430c71fd-82a2-4e97-9088-0deef3f57192-tcdeli-lxplm&source=' +
      //   value;
      if (questionFilters) {
        if (questionFilters.selectedTopic !== 'none') {
          //console.log('topicId not none');

          requestParams =
            '?gradeId=' +
            questionFilters.selectedGrade +
            '&subjectId=' +
            questionFilters.selectedSubject +
            '&chapterId=' +
            questionFilters.selectedChapter +
            '&tpId=' +
            questionFilters.selectedTopic +
            '&source=' +
            value;
        } else {
          requestParams =
            '?gradeId=' +
            questionFilters.selectedGrade +
            '&subjectId=' +
            questionFilters.selectedSubject +
            '&chapterId=' +
            questionFilters.selectedChapter +
            '&source=' +
            value;
        }
      } else {
        requestParams =
          '?gradeId=grd-74cc96e3-3ba0-44af-b60e-722e44cdefdc&subjectId=sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b&chapterId=chp-a12ef36c-63f2-4d21-9f04-17280951201b&source=' +
          value;
      }
    } else {
      //console.log('tce else');
      if (questionFilters.selectedTopic !== 'none') {
        requestParams =
          '?gradeId=6&subjectId=Maths&chapterId=' +
          questionFilters.selectedChapter +
          '&tpId=' +
          questionFilters.selectedTopic +
          '&source=' +
          value;
      } else {
        requestParams =
          '?gradeId=6&subjectId=Maths&chapterId=' +
          questionFilters.selectedChapter +
          '&source=' +
          value;
      }
    }

    this.http
      .get<any>(
        this.requestApiService.getUrl('getQuestionBank') + requestParams
      )
      .subscribe(
        data => {
          console.log('data.length--->> ', data);
          if (data.length === 0) {
            //console.log('NO Q Data');
            this.noQuestionsAvailableBroadcast.next(true);
          }

          // qData = data;
          if (data) {
            this.questionbankDataBroadcast.next(data);
            // this.questionDataSource = [];
            // //this.questionsLength += data.length;
            // for (let i = 0; i < data.length; i++) {
            //   // commented by Usman for testing
            //   const questionFilepath = this.getQuestionSource(data[i]);
            //   //console.log('questionFilepath', questionFilepath);
            //   // this.httpGetQuestionSource(questionFilepath, data[i]);
            //   // this.getQuestionSource(data[i]);
            // }
          }

          //this.questionbankDataBroadcast.next(data);
          //console.log('get Question Bank------------------->data', data);
        },
        err => {
          console.log('err-->>', err);
        }
      );
  }

  addQuestion(questionData, questionAddDetails) {
    //console.log('questionData 1', questionData, questionAddDetails);
    //Commented by Usman on 28/4/21. can be used later
    // var qData = JSON.stringify({
    //   questionSourceType: 'custom',
    //   questionDetails: questionData
    // });

    let qData = JSON.stringify(questionData);
    //console.log('questionData 2', qData);

    let requestParams = {
      // chapterId: 'cchp-430c71fd-82a2-4e97-9088-0deef3f57192-tcdeli-lxplm',
      // chapterId: 'chp-a12ef36c-63f2-4d21-9f04-17280951201b',
      // tpId: 'ctp-aca7e55b-5c2a-4435-a47c-499c50ec00e5-tcdeli-lxplm',
      // gradeId: 'grd-74cc96e3-3ba0-44af-b60e-722e44cdefdc',
      // subjectId: 'sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b',
      chapterId: questionAddDetails.chapterId,
      tpId: questionAddDetails.topicId,
      gradeId: questionAddDetails.gradeId,
      subjectId: questionAddDetails.subjectId,
      questionType: questionAddDetails.questionType,
      fileName: questionAddDetails.fileName,
      folder: questionAddDetails.folder,
      //commented by Usman for teting
      // questionData:
      //   '{"questionSourceType":"custom","questionDetails":{"qtext":"What will be the radix value if the string begins with 0x?","options":{"option":[{"value":"13","id":1,"isCorrect":null},{"value":"14","id":2,"isCorrect":null},{"value":"16","id":3,"isCorrect":"true"},{"value":"15","id":4,"isCorrect":null}]}}}',

      questionData: qData,

      // questionData: data,
      difficulty: 'High'
    };

    //console.log('questionData after data', requestParams);

    this.http
      .post<any>(this.requestApiService.getUrl('addQuestion'), requestParams)
      .subscribe(
        data => {
          //console.log('Add Question------------------->data', data);
          this.toastr.success('Question added successfully!');
          this.addQuestionBroadcast.next(data);
          // this.getQuestionBankData('custom', this.questionFilters);
          //this.questionbankDataBroadcast.next();
        },
        err => {
          console.log('err-->>', err);
        }
      );
  }

  editQuestion(templateData, questionAddDetails, questionId) {
    console.log('edData ', templateData);
    let qData = JSON.stringify(templateData);
    let requestParams = {
      questionId: questionId,
      chapterId: questionAddDetails.chapterId,
      tpId: questionAddDetails.topicId,
      gradeId: questionAddDetails.gradeId,
      subjectId: questionAddDetails.subjectId,
      questionType: questionAddDetails.questionType,
      questionData: qData,
      difficulty: 'High'
    };
    console.log(
      'questionAddDetails.questionType',
      requestParams,
      questionAddDetails
    );

    if (questionAddDetails.fileName && questionAddDetails.folder) {
      requestParams['fileName'] = questionAddDetails.fileName;
      requestParams['folder'] = questionAddDetails.folder;
    }
    if (questionAddDetails.deleteImage) {
      console.log('deleteImage ', templateData);

      requestParams['keywords'] = 'delete';
      templateData.image = '';
      qData = JSON.stringify(templateData);
      requestParams.questionData = qData;
    }

    this.http
      .put<any>(this.requestApiService.getUrl('editQuestion'), requestParams)
      .subscribe(
        data => {
          console.log('edit Question------------------->data', data);
          if (data.status === 'success')
            this.toastr.success('Question updated successfully!');
        },
        err => {
          console.log('err-->>', err);
          if (err.error.errorMessage === 'Access Denied') {
            this.toastr.error('Only Question creator can delete the question!');
          }
        }
      );
  }

  deleteQuestion(questionId) {
    this.http
      .delete<any>(
        this.requestApiService.getUrl('editQuestion') + '/' + questionId
      )
      .subscribe(
        data => {
          console.log('Delete Question------------------->data', data);
          this.deleteQuestionBroadcast.next(data);
        },
        err => {
          console.log('err-->>', err);
          if (err) {
            if (err.error.errorMessage === 'Access Denied') {
              this.toastr.error(
                'Only question creator can edit/delete the question'
              );
            }
          }
        }
      );
  }

  getQuestionSource(resource) {
    //console.log('questions.this.url--->>>', this.url);
    //console.log('question.encryptedFilePathDDDDD--->>>', resource);
    this.noQuestionsAvailableBroadcast.next(false);
    let qbIdType = resource.questionId.substring(0, 4);
    //console.log('qbIdType ', qbIdType);
    let qbType: string;
    if (qbIdType === 'teqb') {
      qbType = 'qb.xml';
      const fileUrl =
        '/' + resource.encryptedPath + '/' + resource.questionId + '/qb.xml';
      this.loadXML(this.url + fileUrl);
    } else {
      this.questionsLength = this.questionsLength + 1;
      qbType = 'qb.json';
      const fileUrl =
        '/' + resource.encryptedPath + '/' + resource.questionId + '/qb.json';
      this.httpGetQuestionSource(this.url + fileUrl, resource);
    }

    //const fileUrl = '?encryptedPath=' + resource.encryptedFilePath + '&fileName=' + resource.resourceId + '/' + resource.fileName;
    const fileUrl =
      '/' + resource.encryptedPath + '/' + resource.questionId + '/' + qbType;
    // this.loadXML(this.url + fileUrl);
    // return this.url + fileUrl;
  }

  httpGetQuestionSource(questionFilepath: string, data) {
    return this.http.get(questionFilepath).subscribe(
      response => {
        this.noQuestionsAvailableBroadcast.next(false);
        let responseData: any;
        responseData = response;
        let getCustom: any;
        getCustom = data;
        if (responseData.questionSourceType) {
          getCustom.questionSourceType = responseData.questionSourceType;
          getCustom.questionDetails = responseData.questionDetails;
          responseData = getCustom;
        }
        // this.questionDataSource =
        // this.questionDataSource.push(responseData);
        // this.currentData.push(responseData);

        //console.log('response in service', this.questionDataSource, response);
        // console.log('questionDataSource', this.questionDataSource.length);

        //commented by Usman
        this.questionbankDataBroadcast.next(responseData);
      },
      error => {
        console.log('errors', error);
      }
    );
  }

  // httpGetQuestionSource(questionFilepath: string, data) {
  //   console.log('questionFilePath ', questionFilepath);
  // }

  loadXML(questionFilepath) {
    this.http
      .get(questionFilepath, {
        headers: new HttpHeaders()
          .set('Content-Type', 'text/xml')
          .append('Access-Control-Allow-Methods', 'GET')
          .append('Access-Control-Allow-Origin', '*')
          .append(
            'Access-Control-Allow-Headers',
            'Access-Control-Allow-Headers, Access-Control-Allow-Origin, Access-Control-Request-Method'
          ),
        responseType: 'text'
      })
      .subscribe(data => {
        //console.log('XMLdata ', data);
        this.questionDataSource.push(data);
        //console.log('response in service', this.questionDataSource, data);

        // this.questionbankDataBroadcast.next(this.questionDataSource);
      });
  }

  getQuestionData() {
    return this.http.get('./libs/question-bank/src/lib/assets/config.json');
  }

  setEditQuestionIndex(index) {
    this.editQuestionIndex.next(index);
  }

  setQEditQuestionIndex(index) {
    this.qEditQuestionIndex.next(index);
  }

  getEditQuestionIndex() {
    return this.editQuestionIndex.asObservable();
  }

  getQEditQuestionIndex() {
    return this.qEditQuestionIndex.asObservable();
  }

  // setQuizQuestionList(quizQuestions) {
  //   this.quizQuestionList.next(quizQuestions);
  // }

  // getQuizQuestionList() {
  //   return this.quizQuestionList.asObservable();
  // }

  // setSelectQuiz(bool) {
  //   this.selectQuiz.next(bool);
  // }

  // getSelectQuiz() {
  //   return this.selectQuiz.asObservable();
  // }
}
