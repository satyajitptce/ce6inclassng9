import { Injectable } from '@angular/core';
import { TemplateMcqData } from 'libs/quiz-templates/src/lib/core/interface/quiz-player-template.interface';
import { parse } from 'path';
import { NgxXmlToJsonService } from 'ngx-xml-to-json';
import { RequestApiService } from '@tce/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityConfigService {
  xml = `<contact-info><address category = "residence" type = "sale" ><name color='white'><![CDATA[ Welcome to TutorialsPoint]]>Tanmay Patil</name><company>TutorialsPoint</company><phone>(011) 123-4567</phone></address><address/><personal category = "commercial"> i dont know</personal></contact-info>`;
  constructor(
    private ngxXmlToJsonService: NgxXmlToJsonService,
    private requestApiService: RequestApiService
  ) {}

  questionFormatCheck(questionData) {
    //console.log('Data Parser called');

    //we will get the parsedQuestionData by  checking and converting the format  of the questonData
    const parsedQuestionData = questionData;
    let mappedQuestionData;

    if (parsedQuestionData == 'type 1') {
      mappedQuestionData = this.questionMapper2(parsedQuestionData);
    } else if (parsedQuestionData == 'type 2') {
      mappedQuestionData = this.questionMapper3(parsedQuestionData);
    } else {
      mappedQuestionData = this.studyToSchemaMCQMapper(parsedQuestionData);
    }

    //just for test. to convert xml to json and string to json
    const options = {
      // set up the default options
      textKey: 'text', // tag name for text nodes
      attrKey: 'attr', // tag for attr groups
      cdataKey: 'cdata' // tag for cdata nodes (ignored if mergeCDATA is true)
    };
    var stringData = JSON.stringify(questionData);
    // string to json
    let parsedData = JSON.parse(stringData);
    //XML to JSON
    const obj = this.ngxXmlToJsonService.xmlToJson(this.xml, options);
    return parsedData;

    return mappedQuestionData;
  }

  studyToSchemaMCQMapper(parsedData) {
    // console.log('mcqData 0 ', parsedData.data.questions[0].data);

    let mcqData = {};
    //we will ,make mcq data by mapping templateMCQData and parseData
    // return mcqData;
    //console.log('Question Data Mapper 1 called', parsedData, mcqData);
    let templateData = parsedData.data.questions[0].data;
    let correctOptions: Array<string> = [];
    templateData.options.forEach((opt, index) => {
      if (opt.isCorrect > 0) {
        //console.log('mcqData For ', opt.isCorrect > 0, opt.value + '');
        correctOptions.push(opt.value);
        if (opt.feedback) {
          templateData.options[index].feedbackInline = opt.feedback.text;
        }
      }
    });
    let layout: any;
    if (
      parsedData.data.questions[0].style.userInteraction.stylePreview ===
        'vertical scq' ||
      parsedData.data.questions[0].style.userInteraction.stylePreview ===
        'vertical mcq'
    ) {
      layout = 'vertical';
    } else if (
      parsedData.data.questions[0].style.userInteraction.stylePreview ===
        'horizontal scq' ||
      parsedData.data.questions[0].style.userInteraction.stylePreview ===
        'horizontal mcq'
    ) {
      layout = 'horizontal';
    } else {
      layout = 'grid';
    }
    let questionImage: any;

    let questionDetails = {
      reference: parsedData.questionId,
      data: {
        shuffle: templateData.metadata.shuffle,
        options: templateData.options,
        stimulus: {
          label: templateData.stimulus,
          value: '',
          feedbackInline: '',
          placeholder: '',
          imgMode: ''
        },
        type: '',
        validation: {
          scoring_type: '',
          valid_response: {
            score: templateData.marks,
            value: correctOptions
          },
          penalty: 0
        },
        ui_style: {
          type: layout,
          theme: '',
          _comment: ''
        },
        penalty_score: 0,
        response_id: '',
        stimulus_audio: '',
        feedback_attempts: 0,
        instant_feedback: false,
        _comment: '',
        metadata: {
          name: '',
          template_reference: ''
        }
      },
      type: templateData.type,
      name: '',
      widget_type: ''
    };
    if (templateData.stimulus_media) {
      let imagePath =
        parsedData.fileUrl +
        '/' +
        parsedData.encryptedPath +
        '/' +
        parsedData.questionId +
        '/images/';
      let imageName = templateData.stimulus_media;
      questionDetails.data.stimulus['imageUrl'] = imagePath;
      questionDetails.data.stimulus['image'] = imageName;
      // console.log('mcqData ', questionDetails);
    }
    mcqData['questionDetails'] = questionDetails;
    // console.log('mcqData ', mcqData);

    return mcqData;
  }

  testEdgeToSchemaMCQMapper(parsedData) {
    console.log('parsedData ', parsedData);
    let layout: any;
    let mediaImgSrc = '';
    if (parsedData.options.option) {
      parsedData.options = parsedData.options.option;
    }
    if (parsedData.layout !== '1') {
      if (parsedData.layout === '3' && parsedData.media) {
        mediaImgSrc =
          parsedData.fileUrl +
          '/' +
          parsedData.encryptedPath +
          '/' +
          parsedData.media.src;
        layout = 'optionImage';
      } else if (
        (parsedData.options && parsedData.options[0].optionImage) ||
        parsedData.layout === 4
      ) {
        layout = 'horizontal';
      } else {
        layout = 'grid';
      }
    } else {
      layout = 'vertical';
    }
    let qType: any;
    if (parsedData.type === 'SC') {
      qType = 'SCQ';
    } else {
      qType = 'MCQ';
    }
    let correctOptions: Array<string> = [];
    let newOptions: Array<object> = [];
    let questionText = parsedData.qtext;
    let imageUrl = '';
    let encryptedPath = '';
    if (parsedData.qtext.indexOf('<img src') != -1) {
      // console.log('imgSRC');
      // questionText = '';
      let imgName = parsedData.qtext.split("'");
      //console.log('imgName ', imgName[1]);
      questionText =
        "<p><img src='" +
        parsedData.fileUrl +
        '/' +
        parsedData.encryptedPath +
        '/' +
        imgName[1] +
        "'></p>";
      // imageUrl = parsedData.fileUrl+'/'+parsedData.encryptedPath+'/'+questio;
    } else if (parsedData.questImage) {
      imageUrl =
        parsedData.fileUrl +
        '/' +
        parsedData.encryptedPath +
        '/' +
        parsedData.id +
        '/images/' +
        parsedData.questImage;
    } else if (parsedData.questmedia && parsedData.questmedia.src) {
      imageUrl =
        parsedData.fileUrl +
        '/' +
        parsedData.encryptedPath +
        '/' +
        parsedData.questmedia.src;
    } else {
      imageUrl = '';
    }

    parsedData.options.forEach((opt, i) => {
      let optObject = {};
      opt['feedbackInline'] = opt.feedback;
      if (opt.value) {
        if (opt.value.indexOf('<img src') != -1) {
          let optImgName = opt.value.split("'");
          optObject['label'] =
            "<p><img src='" +
            parsedData.fileUrl +
            '/' +
            parsedData.encryptedPath +
            '/' +
            optImgName[1] +
            "'></p>";
        } else {
          optObject['label'] = opt.value;
        }
      } else {
        optObject['label'] = "<img src='" + opt.optionImage + "'>";
      }
      optObject['value'] = opt.id;
      if (opt.isCorrect) {
        correctOptions.push(opt.id);
      }
      optObject['feedbackInline'] = opt.feedback;
      newOptions.push(optObject);
    });
    // parsedData.

    if (parsedData.qadd) {
      questionText = questionText + ' ' + parsedData.qadd;
    }
    let mcqData = {
      reference: parsedData.id,
      data: {
        shuffle: '',
        options: newOptions,
        stimulus: {
          label: questionText,
          value: '',
          feedbackInline: '',
          placeholder: '',
          imgMode: 'medium',
          image: imageUrl
        },
        type: '',
        validation: {
          scoring_type: '',
          valid_response: {
            score: '',
            value: correctOptions
          },
          penalty: 0
        },
        ui_style: {
          type: layout,
          theme: '',
          _comment: ''
        },
        penalty_score: 0,
        response_id: '',
        stimulus_audio: '',
        feedback_attempts: 0,
        instant_feedback: false,
        _comment: '',
        metadata: {
          name: '',
          template_reference: ''
        },
        media: {
          src: mediaImgSrc
        }
      },
      type: qType,
      name: '',
      widget_type: ''
    };
    // mcqData['questionDetails'] = questionDetails;
    //console.log('mcqData ', mcqData);

    return mcqData;
  }

  studiToTestEdgeMapper(parsedData, questionId, encryptedPath) {
    console.log('baseData parsedData', parsedData, questionId, encryptedPath);
    let questionDetails = parsedData.data.questions[0];
    let templateType = questionDetails.data.type;
    if (
      templateType === 'SCQ' ||
      templateType === 'mcq-tf' ||
      templateType === 'MCQ'
    ) {
      let schemaData = this.studiToTestEdgeMcqMapper(
        questionDetails,
        questionId,
        encryptedPath
      );
      return schemaData;
    }
    if (templateType === 'OPENENDEDSTEMONLY') {
      let schemaData = this.studiToTestEdgeOpenEnded(
        questionDetails,
        questionId,
        encryptedPath
      );
      return schemaData;
    }
  }

  studiToTestEdgeMcqMapper(questionDetails, questionId, encryptedPath) {
    console.log('questionDetails', questionDetails);

    let qType = '';
    if (
      questionDetails.data.type === 'SCQ' ||
      questionDetails.data.type === 'mcq-tf'
    ) {
      qType = 'SC';
    } else {
      qType = 'MC';
    }
    let newOptions: Array<any> = [];
    questionDetails.data.options.forEach(opt => {
      let newOpt = {
        id: opt.value,
        isCorrect: opt.isCorrect == 1 ? true : null,
        value: opt.label,
        feedback: opt.feedback[0].text
      };
      newOptions.push(newOpt);
    });
    let layout: any;
    let stylePreview = questionDetails.style.userInteraction.stylePreview;
    if (stylePreview === 'vertical mcq' || stylePreview === 'vertical scq') {
      layout = 1;
    } else if (stylePreview === 'horizontal scq') {
      layout = 4;
    } else {
      layout = 5;
    }
    let qData = {
      metadata: {
        tp: '',
        objective: 'Application',
        difficultylevel: 'Average',
        keywords: null,
        copyright: null
      },
      qtext: questionDetails.data.stimulus,
      questImage: questionDetails.data.stimulus_media,
      media: {
        value: '',
        src: null,
        type: null
      },
      questmedia: null,
      type: qType,
      options: {
        option: newOptions
      },
      qadd: '',
      feedbacks: null,
      id: '',
      layout: layout,
      uid: ''
    };
    let newSchemaData = {
      questionId: questionId,
      questionDetails: qData,
      encryptedPath: encryptedPath
    };
    console.log('newSchemaData ', newSchemaData);

    // return newSchemaData;
    return qData;
  }

  studiToTestEdgeOpenEnded(questionDetails, questionId, encryptedPath) {}

  questionMapper2(parsedData) {
    let mcqData = {} as TemplateMcqData;
    //we will ,make mcq data by mapping templateMCQData and parseData
    return mcqData;
  }

  questionMapper3(parsedData) {
    let mcqData = {} as TemplateMcqData;
    //we will ,make mcq data by mapping templateMCQData and parseData
    return mcqData;
  }

  studySchemaMapperAddUpdate() {
    // console.log('schemaData original',schemaData, imageName);

    let schemaDataNew = {
      meta: {},
      data: {
        item: {
          content: '',
          questions: [
            {
              reference: '',
              type: ''
            }
          ],
          features: [],
          masterid: '',
          tpid: '',
          description: '',
          note: '',
          source: '',
          tags: [],
          status: 'draft',
          dt_created: '',
          dt_updated: '',
          originalReference: '',
          item_id: '',
          last_updated_by: {
            id: '',
            email: '',
            surname: ''
          },
          tpId: '',
          tqId: ''
        },
        questions: [],
        features: []
      }
    };

    // let questionsData = this.questionMapMcq(schemaData, imageName);
    // schemaDataNew.data.questions.push(questionsData);
    //console.log(schemaDataNew, 'schemaData New');
    return schemaDataNew;
  }

  questionMapMcq(schemaData, imageName) {
    console.log('imageName in mapper ', imageName, schemaData);

    schemaData.data.options.forEach((element, i) => {
      let feedback = {
        text: element.feedbackInline,
        media: null
      };
      element.feedback = [];
      element.feedback.push(feedback);
      element.media = null;
      if (
        schemaData.data.validation.valid_response.value.includes(element.value)
      ) {
        element.isCorrect = 1;
        element.score = schemaData.data.validation.valid_response.score;
      } else {
        element.isCorrect = 0;
        element.score = 0;
      }

      if (element.feedbackInline) {
        delete element.feedbackInline;
      }
      if (element.checked) {
        delete element.checked;
      }
      if (element.placeholder) {
        delete element.placeholder;
      }
      if (element.selected) {
        delete element.selected;
      }
    });
    // if(mode == "add"){
    //   qID = "";
    // }else{
    //   qID = schemaData.data.questions.questionID;
    // }
    let q = {
      questionID: schemaData.questionID,
      data: {
        stimulus: schemaData.data.stimulus.label,
        marks: schemaData.data.validation.valid_response.score,
        options: schemaData.data.options,
        type: schemaData.type,
        metadata: {
          shuffle: true,
          duration: 60,
          bloom: 'Application',
          construct: 'construct-54cf6bd0-3908-4fee-9e03-93d359c34502',
          difficulty: 'Difficult',
          version: 1,

          microConcept: [
            {
              id: 'micro-d15d9340-b8ba-4f92-8d0d-79a1953877f8',
              label: 'dfshfjsd'
            }
          ],
          distractor_rationale: {
            label: 'Incorrect feedback',
            audio: ''
          },
          acknowledgements: {
            label: 'Correct feedback',
            audio: ''
          }
        },
        style: {}
      },
      metadata: {
        name: '',
        template_reference: ''
      },
      type: '',
      widget_type: 'response',
      style: {
        qStyle: {
          bg: ''
        },
        userInteraction: {
          stylePrint: 'vertical print scq',
          styleAttempt: 'vertical scq',
          styleReview: 'vertical showTick instantFeedback showFeedback scq',
          stylePreview:
            schemaData.data.ui_style.type + ' ' + schemaData.type.toLowerCase()
        }
      },
      instructions: '<p>Select the correct answer.</p>',
      solution: null,
      solutionStrategies: '',
      attachment: ''
    };
    if (imageName) {
      q.data['stimulus_media'] = imageName;
    } else if (schemaData.data.stimulus.image) {
      q.data['stimulus_media'] = schemaData.data.stimulus.image;
    }
    //console.log(q, 'q in questionmap');

    return q;
  }

  questionMapOpenEnded(schemaData, imageName) {
    let q = {
      questionID: schemaData.questionID,
      data: {
        stimulus: schemaData.data.stimulus.label,
        type: 'OPENENDEDSTEMONLY',
        sample_answer: schemaData.sample_answer
      }
    };
    if (imageName) {
      q.data['stimulus_media'] = imageName;
    } else if (schemaData.data.stimulus.image) {
      q.data['stimulus_media'] = schemaData.data.stimulus.image;
    }
    return q;
  }

  // studyToOpenEnded(schemaData) {
  //   console.log('schemaData ', schemaData);
  // }

  studySchemaMapperFib(schemaData) {
    let schemaDataNew = {
      questionID: schemaData.questionID,
      data: {
        metadata: {
          duration: 60,
          difficulty: 'Average',
          distractor_rationale: {
            label: 'Incorrect feedback',
            audio: ''
          },
          bloom: 'Comprehension',
          acknowledgements: {
            label: 'Correct feedback',
            audio: ''
          },
          construct: 'costr-b8587f5d-2612-4571-8234-2115b5c21c82',
          shuffle: true,
          version: 1,
          microConcept: [
            {
              id: 'micro-8c0b863a-f8f9-4128-b6bd-1edaba51cf45',
              label:
                '"Good Touch and Bad Touch Family members At school Neighbours"'
            }
          ]
        },
        stimulus: schemaData.data.stimulus.label,
        questionData: schemaData.data.template,
        groups: {
          valid_response: {
            score: schemaData.data.validation.valid_response.score,
            value: schemaData.data.validation.valid_response.value
          },
          possible_responses: [
            ['silently', 'barking', 'thief', 'suddenly'],
            ['walked', 'carefully', 'slowly', 'caught']
          ]
        },
        marks: 2,
        type: schemaData.type,
        qTypes: ['WD', 'FIBDRPDWN']
      },
      style: {
        qStyle: {
          bg: ''
        },
        userInteraction: {
          stylePrint: 'vertical print mcq',
          stylePreview: 'vertical mcq',
          styleReview: 'vertical showTick instantFeedback showFeedback mcq',
          styleAttempt: 'vertical mcq'
        }
      },
      type: '',
      widget_type: 'response'
    };
    //console.log(schemaDataNew, 'schemaDataNew');
    return schemaDataNew;
  }

  convertToJson(mcqXmlSchema) {
    let parser: any = new DOMParser();
    let xmlData: any = parser.parseFromString(mcqXmlSchema, 'application/xml');
    //console.log('mcqXmlSchema', mcqXmlSchema, xmlData);

    if (xmlData.documentElement.nodeName === 'parsererror') {
      return '';
    } else {
      let qtype = xmlData
        .getElementsByTagName('Question')[0]
        .getAttribute('template_Type');
      if (qtype == 'MCMS' || qtype == 'MCTWO') {
        qtype = 'SCQ';
      } else if (qtype == 'FIBSINGLE') {
        qtype = 'FIB';
      }
      // else if (qtype == 'MCTWO') {
      //   qtype = 'SCQ';
      // }

      let qId = xmlData
        .getElementsByTagName('Question')[0]
        .getAttribute('question_id');
      let question_type = xmlData
        .getElementsByTagName('Question')[0]
        .getAttribute('question_type');
      let questionText = xmlData.getElementsByTagName('Text')[0].childNodes[0]
        .nodeValue;
      let marks = xmlData
        .getElementsByTagName('Meta_Suggestive_Marks')[0]
        .textContent.trim();
      let instructions = xmlData
        .getElementsByTagName('Instruction_Text')[0]
        .textContent.trim();
      // if(qtype == "MCQ"){
      let optionLength = xmlData.getElementsByTagName('Choice_Option').length;
      let optData = [];
      for (let i = 0; i < optionLength; i++) {
        let opDataEach = {
          value: i + '',
          label: xmlData
            .getElementsByTagName('Choice_Option')
            [i].textContent.trim(),
          media: null,
          isCorrect:
            xmlData
              .getElementsByTagName('Choice_Option')
              [i].getAttribute('correct') === 'true'
              ? 1
              : 0,
          score:
            xmlData
              .getElementsByTagName('Choice_Option')
              [i].getAttribute('correct') === 'true'
              ? 1
              : 0,
          feedback: [
            {
              // text:xmlData.getElementsByTagName('Solution')[0].textContent.trim(),
              text: '',
              media: null
            }
          ]
        };
        optData.push(opDataEach);
      }
      //console.log(optData, 'optData');

      let qDetails = {
        meta: {},
        data: {
          item: {
            content: '',
            questions: [
              {
                reference: '',
                type: ''
              }
            ],
            features: [],
            masterid: 'tqqb-ca92ac20-b5f7-42fa-ba36-94665a821bac',
            version: 5,
            tpid: '',
            description: '',
            note: '',
            source: '',
            tags: [],
            status: '',
            dt_created: '',
            dt_updated: '',
            originalReference: '',
            item_id: '',
            last_updated_by: {
              id: '',
              email: '',
              surname: ''
            },
            tpId: 'tp-4db27840-6ddb-41be-a2c5-6b90709f4d1a_gu',
            tqId: 'tq-c70b86b1-0795-4d23-82cf-551c85aaf2e2'
          },
          questions: [
            {
              questionID: qId,
              data: {
                stimulus: questionText,
                stimulus_media: null,
                marks: marks,
                options: optData,
                solution: {},
                type: qtype,
                metadata: {
                  shuffle: true,
                  duration: 60,
                  bloom: 'Application',
                  construct: '',
                  difficulty: 'Difficult',
                  microConcept: [
                    {
                      id: 'micro-0f3f0ced-81c1-422c-9368-718b33da8f16',
                      label: 'Test Mc 1'
                    },
                    {
                      id: 'micro-1a9dd539-602a-4290-b165-85c9f0f41557',
                      label: 'Test Mc 5'
                    }
                  ],
                  distractor_rationale: {
                    label: 'Incorrect feedback',
                    audio: ''
                  },
                  acknowledgements: {
                    label: 'Correct feedback',
                    audio: ''
                  }
                },
                style: {}
              },
              metadata: {
                name: '',
                template_reference: ''
              },
              type: '',
              widget_type: 'response',
              style: {
                qStyle: {
                  bg: ''
                },
                userInteraction: {
                  stylePrint: 'vertical print mcq',
                  styleAttempt: 'vertical mcq',
                  styleReview:
                    'vertical showTick instantFeedback showFeedback mcq',
                  stylePreview: 'vertical mcq'
                }
              },
              instructions: instructions,
              solution: {
                data: [],
                answer: '',
                step_nav: false,
                type: 'Solution'
              },
              solutionStrategies: '',
              attachment: ''
            }
          ],
          features: []
        }
      };
      let jsonFormat = {
        encryptedPath: '',
        questionDetails: qDetails,
        questionId: qId
      };
      //console.log(jsonFormat, 'jsonFormat');
      return jsonFormat;
      // }
      // else if(qtype == "FIB"){

      // }else{

      // }
    }
  }

  xmlToOpenEndedMapper(parsedData) {
    let parser: any = new DOMParser();
    let xmlData: any = parser.parseFromString(parsedData, 'application/xml');
    console.log('parsedData in open ', xmlData);
    if (xmlData.documentElement.nodeName === 'parsererror') {
      return '';
    } else {
      let qId = xmlData
        .getElementsByTagName('Question')[0]
        .getAttribute('question_id');
      let questionText = xmlData.getElementsByTagName('Text')[0].childNodes[0]
        .nodeValue;

      //console.log('qId ', qId, questionText);

      let openEnded = {
        reference: '',
        data: {
          type: 'essay',
          metadata: {
            name: 'Essay with Plain Text',
            template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
            subjects: []
          },
          stimulus: {
            label: questionText,
            value: '0',
            feedbackInline: ''
          }
        },
        show_copy: true,
        show_cut: true,
        show_paste: true,
        show_word_limit: 'visible',
        max_length: '10000',
        showSampleAnswer: false,
        spellcheck: true,
        type: 'plain-text',
        name: 'Plain text',
        sample_answer: ''
      };
      return openEnded;
    }
  }

  studiToOpenEndedMapper(parsedData) {
    let url = this.requestApiService.getUrl('getFile');
    parsedData['fileUrl'] = url;
    console.log('parsed in open ', parsedData);
    let questionData = parsedData.data.questions[0];
    let qId = '';
    let questionText = questionData.data.stimulus;
    let openEnded = {
      reference: '',
      data: {
        type: 'essay',
        metadata: {
          name: 'Essay with Plain Text',
          template_reference: '9e8149bd-e4d8-4dd6-a751-1a113a4b9163',
          subjects: []
        },
        stimulus: {
          label: questionText,
          value: '0',
          feedbackInline: ''
        }
      },
      show_copy: true,
      show_cut: true,
      show_paste: true,
      show_word_limit: 'visible',
      max_length: '10000',
      showSampleAnswer: false,
      spellcheck: true,
      type: 'plain-text',
      name: 'Plain text',
      sample_answer: questionData.data.sample_answer
    };
    if (questionData.data.stimulus_media) {
      let imagePath =
        parsedData.fileUrl +
        '/' +
        parsedData.encryptedPath +
        '/' +
        parsedData.questionId +
        '/images/';
      let imageName = questionData.data.stimulus_media;
      openEnded.data.stimulus['imageUrl'] = imagePath;
      openEnded.data.stimulus['image'] = imageName;
    }
    console.log('openended schema', openEnded);

    return openEnded;
  }
}
