import { TestBed } from '@angular/core/testing';

import { QuestionbankserviceService } from './questionbankservice.service';

describe('QuestionbankserviceService', () => {
  let service: QuestionbankserviceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(QuestionbankserviceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
