module.exports = {
  name: 'question-bank',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/question-bank',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
