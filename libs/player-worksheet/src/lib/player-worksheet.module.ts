import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { WorksheetPlayerComponent } from './components/worksheet-player/worksheet-player.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CoreModule } from '@tce/core';
import { WorksheetService } from './services/worksheet.service';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ViewerPdfModule } from '@tce/viewer-pdf';

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(WorksheetPlayerComponent),
    PdfViewerModule,
    CoreModule,
    AngularSvgIconModule,
    ViewerPdfModule
  ],
  declarations: [WorksheetPlayerComponent],
  providers: [WorksheetService],
  exports: [WorksheetPlayerComponent]
})
export class PlayerWorksheetModule {}
