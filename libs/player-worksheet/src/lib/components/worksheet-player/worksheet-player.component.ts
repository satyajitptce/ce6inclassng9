import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WorksheetService } from '../../services/worksheet.service';
import { WorksheetPageType } from '../../enums/worksheet.enum';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import {
  PlayerAbstractInterface,
  PlayerAbstractComponent,
  PlayerAbstractAnimations,
  RequestApiService,
  PlayerUsageService
} from '@tce/core';
import { ViewPdfComponent } from 'libs/viewer-pdf/src/lib/components/view-pdf/view-pdf.component';
import { saveAs as importedSaveAs } from 'file-saver';
@Component({
  selector: 'tce-worksheet-player',
  templateUrl: './worksheet-player.component.html',
  styleUrls: ['./worksheet-player.component.scss'],
  animations: PlayerAbstractAnimations
})
export class WorksheetPlayerComponent extends PlayerAbstractComponent
  implements OnInit, PlayerAbstractInterface {
  @ViewChild('pdfView', { static: false }) pdfView: ViewPdfComponent;
  public pdfSource: string;
  public pdfPageCount = 0;
  public pdfId: string;
  public activeWorksheetPage = WorksheetPageType.QUESTION;
  public pdfCurrentPage = { pageNumber: 1, eventType: 'click' };
  public showAnswerButton = false;
  public numEbookPages: Array<number>;
  public scrollContainerRef: HTMLElement;
  showDownloadOption = false;

  constructor(
    private http: HttpClient,
    public worksheetService: WorksheetService,
    private requestApiService: RequestApiService,
    private playerUsageService: PlayerUsageService
  ) {
    super();
  }

  ngOnInit() {
    //this.scrollContainerRef = this.pdfView.pdfWrapper.nativeElement;

    this.worksheetService.worksheetSelection$.subscribe(worksheet => {
      if (worksheet) {
        this.onEndLoadingResource();
        this.pdfSource = worksheet;
      }
    });

    this.worksheetService.worksheetState$.subscribe(state => {
      this.activeWorksheetPage = state;
      //this.pdfCurrentPage = { pageNumber: 1, eventType: 'click' };
    });

    this.worksheetService.worksheetIsFetching$.subscribe(isFetching => {
      if (isFetching) {
        this.onBeginLoadingResource();
      }
    });

    this.worksheetService.worksheetFetchError$.subscribe(err => {
      this.onResourceLoadError();
    });

    if (this.resource.metaData.answerKeyResource) {
      this.showAnswerButton = true;
    }
    if (this.resource.downloadFileExtension) {
      this.showDownloadOption = true;
    }
  }

  printDocument(event){
    this.playerUsageService.logResourceUsage(this.resource);
    const url = this.requestApiService.getUrl('getFile');
    const encryptedFilePath = this.resource.encryptedFilePath;
    const assetId = this.resource.resourceId;
    const fileName = this.resource.fileName;    
    const finalFileURL = url + '/' + encryptedFilePath + '/' + assetId + '/' + fileName
    
    const options = { responseType:'blob' as 'json' };
    this.http.get(finalFileURL, {
      responseType: options.responseType
    }).subscribe(
      (res: any) => { 
        //let blob = new Blob([res], { type: 'application/pdf' });
        const blobUrl = URL.createObjectURL(res);
        const iframe = document.createElement('iframe');
        iframe.style.display = 'none';
        iframe.src = blobUrl;
        document.body.appendChild(iframe);
        iframe.contentWindow.print();
    });  
  }

  beginLoadingResource() {
    this.worksheetService.setResource(this.resource);
  }

  public getPageData(event: any) {
    this.pdfPageCount = event._pdfInfo.numPages;
    this.numEbookPages = Array.from(Array(this.pdfPageCount).keys());
    if (this.resource.resourceId) {
      this.pdfId = this.resource.resourceId + event._pdfInfo.fingerprint;
    }
  }

  public triggerScroll(emittedObject) {
    if (
      emittedObject.eventName === 'mousedown' ||
      emittedObject.eventName === 'touchstart'
    ) {
      this.playerUsageService.logResourceUsage(this.resource);
      if (emittedObject.type === 'up') {
        emittedObject.cachedCurrentTarget.parentNode.parentNode.parentNode.parentNode.parentNode.previousElementSibling.firstChild.scrollTop -= 20;
      } else {
        emittedObject.cachedCurrentTarget.parentNode.parentNode.parentNode.parentNode.parentNode.previousElementSibling.firstChild.scrollTop += 20;
      }
    }
  }

  public goToPage(page: number) {
    this.playerUsageService.logResourceUsage(this.resource);
    this.pdfCurrentPage = { pageNumber: page, eventType: 'click' };
  }

  public pdfRendered(event: any) {
    this.isLoading = false;
  }

  public updatePdfPage(pageObj: { currentPage: number }) {
    this.pdfCurrentPage = {
      pageNumber: pageObj.currentPage,
      eventType: 'scroll'
    };
  }

  public toggleState() {
    this.playerUsageService.logResourceUsage(this.resource);
    const toggledState =
      this.activeWorksheetPage === WorksheetPageType.QUESTION
        ? WorksheetPageType.ANSWER
        : WorksheetPageType.QUESTION;
    this.worksheetService.setWorksheetSelection(toggledState);
  }

  downloadPdf(event) {
    event.stopPropagation();
    //console.log("TCL: download -> data", resource)
    const url = this.requestApiService.getUrl('getFile');
    let fileName = '';
    const encryptedFilePath = this.resource.encryptedFilePath;
    const assetId = this.resource.resourceId;
    if (this.resource.downloadFileExtension) {
      const suppredString = this.resource.fileName.split('.')[0];
      fileName = suppredString + '.' + this.resource.downloadFileExtension;
    }
    // console.log("TCL: download -> data",  'http://172.18.3.52:4200/'+data)
    const blob = url + '/' + encryptedFilePath + '/' + assetId + '/' + fileName;
    //console.log("ResourcePlaylistComponent -> download -> blob", blob)
    // const blob = new Blob([data], { type: 'image/jpeg' });
    // const url= window.URL.createObjectURL(blob);
    // window.open(url);
    if (fileName) {
      importedSaveAs(blob, fileName);
    }
  }
}
