import { Injectable, Inject } from '@angular/core';
import { ReplaySubject, BehaviorSubject } from 'rxjs';
import { WorksheetPageType } from '../enums/worksheet.enum';
import { Resource, RequestApiService } from '@tce/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable()
export class WorksheetService {
  private url = this.requestApiService.getUrl('getFile');
  private _questionPDF: ArrayBuffer;
  private _answerPDF: ArrayBuffer;
  private _resourceObj;

  private worksheetState = new ReplaySubject<WorksheetPageType>(1);
  private worksheetSelection = new ReplaySubject<any>(1);

  public worksheetState$ = this.worksheetState.asObservable();
  public worksheetSelection$ = this.worksheetSelection.asObservable();

  private worksheetIsFetching = new BehaviorSubject<boolean>(true);
  public worksheetIsFetching$ = this.worksheetIsFetching.asObservable();

  private worksheetFetchError = new ReplaySubject<any>(1);
  public worksheetFetchError$ = this.worksheetFetchError.asObservable();

  constructor(
    private requestApiService: RequestApiService,
    private http: HttpClient
  ) {}

  setResource(resource: Resource) {
    this._resourceObj = resource;
    this.setWorksheetSelection(WorksheetPageType.QUESTION);
  }

  setWorksheetSelection(pdfState: WorksheetPageType) {
    //console.log(pdfState);
    if (
      (pdfState === WorksheetPageType.ANSWER && !this._answerPDF) ||
      (pdfState === WorksheetPageType.QUESTION && !this._questionPDF)
    ) {
      const pdfFilepath = this.getPdfSource(
        pdfState === WorksheetPageType.QUESTION
          ? this._resourceObj
          : this._resourceObj.metaData.answerKeyResource
      );
      this.httpGetPdf(pdfFilepath, pdfState);
    } else {
      const pdf =
        pdfState === WorksheetPageType.QUESTION
          ? this._questionPDF
          : this._answerPDF;

      this.worksheetState.next(pdfState);
      this.worksheetSelection.next(pdf);
    }
  }

  httpGetPdf(pdfFilepath: string, pdfState: WorksheetPageType) {
    //console.log(pdfState, pdfFilepath);
    this.worksheetIsFetching.next(true);
    const accessToken = sessionStorage.getItem('token');
    return this.http
      .get(pdfFilepath, {
        headers: {
          Accept: 'application/pdf',
          requestContext: 'player-worksheet',
          Authorization: 'Bearer ' + accessToken
        },
        responseType: 'blob'
      })
      .pipe(
        catchError(err => {
          this.worksheetIsFetching.next(false);
          this.worksheetFetchError.next(err);
          throw err;
        })
      )
      .subscribe(response => {
        this.worksheetIsFetching.next(false);
        const reader = new FileReader();
        reader.readAsArrayBuffer(response);
        reader.onload = function() {
          if (pdfState === WorksheetPageType.QUESTION) {
            this._questionPDF = reader.result;
          } else if (WorksheetPageType.ANSWER) {
            this._answerPDF = reader.result;
          }
          this.worksheetState.next(pdfState);
          this.worksheetSelection.next(
            pdfState === WorksheetPageType.QUESTION
              ? this._questionPDF
              : this._answerPDF
          );
        }.bind(this);
      });
  }

  getPdfSource(resource: Resource) {
    //console.log('resource.this.url--->>>', this.url);
    
    //const fileUrl = '?encryptedPath=' + resource.encryptedFilePath + '&fileName=' + resource.resourceId + '/' + resource.fileName;
    const fileUrl =
      '/' +
      resource.encryptedFilePath +
      '/' +
      resource.resourceId +
      '/' +
      resource.fileName;
    return this.url + fileUrl;
  }
}
