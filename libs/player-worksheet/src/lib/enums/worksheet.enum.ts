export enum WorksheetPageType {
  QUESTION = 'question',
  ANSWER = 'answer'
}
