import { async, TestBed } from '@angular/core/testing';
import { PlayerWorksheetModule } from './player-worksheet.module';

describe('PlayerWorksheetModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerWorksheetModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerWorksheetModule).toBeDefined();
  });
});
