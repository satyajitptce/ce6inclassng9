module.exports = {
  name: 'player-worksheet',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-worksheet',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
