import {
  Component,
  OnInit,
  Input,
  ChangeDetectorRef,
  ChangeDetectionStrategy,
  OnDestroy
} from '@angular/core';
import { ToolbarService, CommonService, UserProfileService } from '@tce/core';
import { PlayerDrawerService } from '@app-teacher/services';
import { Subscription } from 'rxjs';
@Component({
  selector: 'tce-user-setting-view',
  templateUrl: './user-setting-view.component.html',
  styleUrls: ['./user-setting-view.component.scss']
})
export class UserSettingViewComponent implements OnInit, OnDestroy {
  _type = false;
  shownavTab: any;
  userSettingViewSubscription: Subscription = new Subscription();
  userProfileViewSubscription: Subscription = new Subscription();
  userProfileImg;

  @Input() userProfile;
  _currentMode: any;
  @Input() inputPrefereView;
  @Input('currentMode')
  set currentMode(val) {
    this._currentMode = val;
    // if(val === 'planning'){
    //   document.documentElement.style.setProperty(
    //     `--${'maincontainerBeforeAfter'}`,
    //     `${'none'}`
    //   );
    // }
    // if(val === 'teaching'){
    //   document.documentElement.style.setProperty(
    //     `--${'maincontainerBeforeAfter'}`,
    //     `${'block'}`
    //   );
    // }
  }
  get currentMode() {
    return this._currentMode;
  }
  @Input('type')
  set type(flag) {
    this._type = flag;
    this.loadDynamicCSS(flag);
  }
  get type() {
    return this._type;
  }
  newUserTop: any;
  set currentView(state) {
    if (state) {
      this.shownavTab = state;
    } else {
      this.shownavTab = 'account';
    }
  }
  get currentView() {
    return this.shownavTab;
  }
  constructor(
    private toolbarService: ToolbarService,
    private playerDrawerService: PlayerDrawerService,
    private commonService: CommonService,
    private cdf: ChangeDetectorRef,
    private userProfileService: UserProfileService
  ) {}

  ngOnInit(): void {
    this.userProfile = this.userProfileService.userData;
    this.userProfileImg = this.userProfileService.userProfileImg;
    this.userSettingViewSubscription.add(
      this.toolbarService.toolbarSubmenuTop$.subscribe((top: any) => {
        if (top) {
          this.newUserTop = top.userTop - 20;
        }
      })
    );

    this.userSettingViewSubscription.add(
      this.playerDrawerService.drawerOpenStatus$.subscribe(flag => {
        let getMainTop: any;
        if (flag) {
          getMainTop = 21;
          this.newUserTop = this.newUserTop;
        } else {
          getMainTop = 60;
          this.newUserTop = this.newUserTop - 20;
        }
        //console.log('--getMainTop--',getMainTop)
        const drawerDocumentElement = document.documentElement;
        drawerDocumentElement.style.setProperty(
          `--${'maincontainerTop'}`,
          `${getMainTop + 'px'}`
        );
        drawerDocumentElement.style.setProperty(
          `--${'arrowtop'}`,
          `${this.newUserTop + 'px'}`
        );
        // const userStae = {
        //   userView:'',
        //   currentState:''
        // }
        // this.commonService.setUserProfileState(userStae);
      })
    );
  }

  loadDynamicCSS(res) {
    let viewsidePosition: any;
    let viewsideRotation: any;
    if (!res) {
      viewsidePosition = '101%';
      viewsideRotation = 'rotate(270deg)';
    } else {
      viewsidePosition = '-15px';
      viewsideRotation = 'rotate(90deg)';
    }
    if (viewsidePosition && viewsideRotation) {
      const toolbarDocumentElement = document.documentElement;
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerposition'}`,
        `${viewsidePosition}`
      );
      toolbarDocumentElement.style.setProperty(
        `--${'maincontainerRotation'}`,
        `${viewsideRotation}`
      );
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.userSettingViewSubscription.unsubscribe();
    this.userProfileViewSubscription.unsubscribe();
  }
}
