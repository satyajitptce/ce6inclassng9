import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserSettingViewComponent } from './user-setting-view.component';

describe('UserSettingViewComponent', () => {
  let component: UserSettingViewComponent;
  let fixture: ComponentFixture<UserSettingViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserSettingViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserSettingViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
