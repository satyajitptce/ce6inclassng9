import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import {
  AppConfigService,
  CurriculumPlaylistService,
  UserProfileService
} from '@tce/core';
import { FilterMenu } from '../../../../../core/src/lib/models/curriculum/curriculum.interface';
import { Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
@Component({
  selector: 'tce-account-view',
  templateUrl: './account-view.component.html',
  styleUrls: ['./account-view.component.scss']
})
export class AccountViewComponent implements OnInit, OnDestroy {
  prefereViewList: any = [];
  filteredMenu: FilterMenu[] = [];
  userSetting: any;
  showPrefere = false;
  _currentMode: any;
  textContaint: any;
  textlist: any;
  showSubjectList = false;
  subjectList = [];
  selectedSubjectList = [];

  @Input('currentMode')
  set currentMode(val) {
    this._currentMode = val;
  }
  get currentMode() {
    return this._currentMode;
  }
  accountViewSubscription: Subscription = new Subscription();
  @Input('getInputPrefereView')
  set getInputPrefereView(data) {
    //add for preferview testing
   
      this.textDisplay(data);
    
  }
  get getInputPrefereView() {
    return this.prefereViewList;
  }
  constructor(
    private appConfigService: AppConfigService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private userProfileService: UserProfileService
  ) {}

  ngOnInit(): void {
    this.accountViewSubscription.add(
      this.curriculumPlaylistService.filteredMenu$.subscribe(
        (filteredMenu: FilterMenu[]) => {
          this.filteredMenu = filteredMenu;
        }
      )
    );
    this.accountViewSubscription.add(
      this.userProfileService.userPrefereViewListBroadcast$.subscribe(
        (data: any) => {
          if (data) {
            this.userProfileService.getUserPrefereViewListBroadcast();
          }
        }
      )
    );

    this.userProfileService.userPreferList$.subscribe(list => {
      this.textContaint = list;
      // this.textDisplay(this.textContaint);
      // console.log(this.textContaint);
    });
    //   this.prefereViewList = this.appConfigService.getConfig('general')[
    //     'playerTypesData'
    //   ];
    //   if (this.prefereViewList && this.prefereViewList.length > 0) {
    //     const result = this.prefereViewList.reduce((unique, o) => {
    //       if (!unique.some(obj => obj.type === o.type)) {
    //         unique.push({ type: o.type, state: 'inactive' });
    //       }
    //       return unique;
    //     }, []);
    //     this.prefereViewList = result;
    //     //console.log("AccountViewComponent -> ngOnInit -> this.prefereViewList", this.prefereViewList)
    //   }
    this.userProfileService.getSubjectList().pipe(
      map((data: any) => data.data)
    ).subscribe((data) => {
      const selectedSubjects: Array<string> = this.userProfileService.assignedSubjects;
      this.subjectList = data;
      this.subjectList = this.subjectList.filter((x) => {
        if(selectedSubjects.includes(x.subjectId)){
          this.selectedSubjectList.push(x);
          return false;
        }else{
          return true;
        }
      });
      this.sortSelectedSubjectList();
      this.sortSubjectList();
    });
  }
  showPrefereViewList() {
    this.showPrefere = !this.showPrefere;
  }

  addPrefereView(list) {
    let clickState = '';
    if (list.state === 'active') {
      clickState = 'remove';
    } else {
      clickState = 'add';
    }

    this.filteredMenu.forEach(element => {
      if (element.name === list.name) {
        if (element.state === 'active' && clickState === 'remove') {
          element.selected = false;
          element.state = 'inactive';
          this.activeState(this.filteredMenu);
        }
        if (element.state === 'inactive' && clickState === 'add') {
          element.selected = true;
          element.state = 'active';
          this.activeState(this.filteredMenu);
        }
      }
    });
  }

  activeState(list) {
    this.prefereViewList = [];
    list.forEach(element => {
      if (element.state === 'active') {
        this.prefereViewList.push(element);
      }
    });
    if (this.prefereViewList && this.prefereViewList.length > 0) {
      this.textDisplay(this.prefereViewList);
      // console.log('preferList', this.prefereViewList);
      this.userProfileService.setUserPrefereViewListBroadcast(
        this.prefereViewList
      );
    } else {
      this.userProfileService.setUserPrefereViewListBroadcast({});
    }
  }

  textDisplay(data) {
    this.textContaint = '';
    let textContaintArray = [];
    if (data && data.length > 0) {
      this.prefereViewList = data;
      this.prefereViewList.forEach(element => {
        if(element.selected){
          textContaintArray.push(element.name);
        }
      });
      if (textContaintArray && textContaintArray.length > 0) {
        this.textContaint = textContaintArray.join(', ');
        this.userProfileService.setUserPreferList(this.textContaint);
      }
    }
  }

  addSubjectList(){
    this.toggleSubjectList(); 
  }

  addSubject(subject){
    this.selectedSubjectList.push(subject);
    this.subjectList = this.subjectList.filter((x) => {
      return x.subjectId!==subject.subjectId;
    });
    this.sortSubjectList();
    this.sortSelectedSubjectList();
    this.toggleSubjectList();
    this.updateSelectedSubjects();
  }

  removeSubject(subject){
    this.selectedSubjectList = this.selectedSubjectList.filter((x) => {
      return x.subjectId!==subject.subjectId;
    });
    this.subjectList.push(subject);
    this.sortSubjectList();
    this.updateSelectedSubjects();
  }

  updateSelectedSubjects(){
    const selectedSubjectIds = this.selectedSubjectList.map((x) => x.subjectId);
    console.log(selectedSubjectIds);
    const body = {
      settings: {
        subjects: JSON.stringify(selectedSubjectIds)
      }
    }
    this.userProfileService.updateUserProfileDetails(body).subscribe();

    const selectedSubjectId = [];
    this.selectedSubjectList.forEach((x) => {
      selectedSubjectId.push(x.subjectId);
    })
    this.userProfileService.assignedSubjects = selectedSubjectId;
  }

  toggleSubjectList(){
    this.showSubjectList = !this.showSubjectList;
  }
  isOpen = false;

  sortSubjectList(){
    this.subjectList = this.subjectList.sort((a,b) => {
      return a.title < b.title ? -1 : (a.title > b.title ? 1 : 0)
    });
  }

  sortSelectedSubjectList(){
    this.selectedSubjectList = this.selectedSubjectList.sort((a,b) => {
      return a.title < b.title ? -1 : (a.title > b.title ? 1 : 0)
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.\
    this.accountViewSubscription.unsubscribe();
  }
}
