import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import { CommonService } from '@tce/core';
import { Subscription } from 'rxjs';
import { PlanningModeService } from '../../../../../planning-mode/src/lib/services/planning-mode.service';
@Component({
  selector: 'tce-profile-view',
  templateUrl: './profile-view.component.html',
  styleUrls: ['./profile-view.component.scss']
})
export class ProfileViewComponent implements OnInit, OnDestroy {
  profileViewSubscription: Subscription = new Subscription();
  @Input() currentMode;
  constructor(
    private commonService: CommonService,
    private planningModeService: PlanningModeService
  ) {}

  ngOnInit(): void {}
  changePassword(view) {
    const userStae = {
      userView: view,
      currentState: ''
    };
    if (this.currentMode === 'teaching') {
      this.commonService.setUserProfileState(userStae);
    }
    if (this.currentMode === 'planning') {
      this.planningModeService.setUserProfileState(userStae);
    }
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    this.profileViewSubscription.unsubscribe();
  }
}
