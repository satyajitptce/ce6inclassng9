import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSettingViewComponent } from './component/user-setting-view/user-setting-view.component';
import { AccountViewComponent } from './component/account-view/account-view.component';
import { NotificationViewComponent } from './component/notification-view/notification-view.component';
import { ProfileViewComponent } from './component/profile-view/profile-view.component';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { OverlayModule } from "@angular/cdk/overlay";

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    OverlayModule,
    LibConfigModule.forChild(UserSettingViewComponent)
  ],
  declarations: [
    UserSettingViewComponent,
    AccountViewComponent,
    NotificationViewComponent,
    ProfileViewComponent
  ],
  exports: [
    UserSettingViewComponent,
    AccountViewComponent,
    NotificationViewComponent,
    ProfileViewComponent
  ],
  entryComponents: [
    UserSettingViewComponent,
    AccountViewComponent,
    NotificationViewComponent,
    ProfileViewComponent
  ]
})
export class UserProfileModule {}
