import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { PlanningCalendarViewComponent } from './planning-calendar/component/planning-calendar-view/planning-calendar-view.component';

@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(PlanningCalendarViewComponent)
  ]
})
export class CalendarModule {}
