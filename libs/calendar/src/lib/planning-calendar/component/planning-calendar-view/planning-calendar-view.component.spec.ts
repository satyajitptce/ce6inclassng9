import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanningCalendarViewComponent } from './planning-calendar-view.component';

describe('PlanningCalendarViewComponent', () => {
  let component: PlanningCalendarViewComponent;
  let fixture: ComponentFixture<PlanningCalendarViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlanningCalendarViewComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanningCalendarViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
