import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlanningCalendarViewComponent } from './component/planning-calendar-view/planning-calendar-view.component';

@NgModule({
  declarations: [PlanningCalendarViewComponent],
  imports: [CommonModule],
  exports: [PlanningCalendarViewComponent],
  entryComponents: [PlanningCalendarViewComponent]
})
export class PlanningCalendarModule {}
