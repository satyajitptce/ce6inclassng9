module.exports = {
  name: 'player-quiz',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-quiz',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
