export enum QuestionStatusEnum {
  CORRECT = 'correct',
  INCORRECT = 'incorrect',
  NONE = 'none'
}
