export enum QuestionMediaTypeEnum {
  IMAGE = 'image',
  AUDIO = 'audio',
  VIDEO = 'video'
}
