import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LibConfigModule } from '@tce/lib-config';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CoreModule } from '@tce/core';
import { QuestionHeaderComponent } from './components/question-header/question-header.component';
import { QuestionHintComponent } from './components/question-hint/question-hint.component';
import { QuestionRendererComponent } from './components/question-renderer/question-renderer.component';
import { BaseQuestionComponent } from './components/base-question/base-question.component';
import { SingleChoiceQuestionComponent } from './components/single-choice-question/single-choice-question.component';
import { MultipleChoiceQuestionComponent } from './components/multiple-choice-question/multiple-choice-question.component';
import { QuizPlayerComponent } from './components/quiz-player/quiz-player.component';
import { QuestionOptionCardComponent } from './components/question-option-card/question-option-card.component';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';
import { QuizService } from './services/quiz-service';
import { QbTemplatePreviewModule } from '@tce/qb-template-preview';

@NgModule({
  imports: [
    CoreModule,
    CommonModule,
    LibConfigModule.forChild(QuizPlayerComponent),
    FormsModule,
    ReactiveFormsModule,
    PlayerAnnotationModule,
    QbTemplatePreviewModule
  ],
  declarations: [
    QuizPlayerComponent,
    BaseQuestionComponent,
    SingleChoiceQuestionComponent,
    MultipleChoiceQuestionComponent,
    QuestionHeaderComponent,
    QuestionHintComponent,
    QuestionRendererComponent,
    QuestionOptionCardComponent
  ],
  exports: [QuizPlayerComponent],
  providers: [QuizService],
  entryComponents: [
    QuizPlayerComponent,
    SingleChoiceQuestionComponent,
    MultipleChoiceQuestionComponent
  ]
})
export class PlayerQuizModule {}
