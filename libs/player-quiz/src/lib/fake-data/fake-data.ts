import { QuestionTypeEnum } from '../enums/question-type.enum';
import { IQuestion } from '../models/quiz-api.interface';
import { QuestionLayoutTypeEnum } from '../enums/question-layout-type.enum';

export const fakeData: IQuestion[] = [
  {
    questionId: 'question-id1',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      layout: QuestionLayoutTypeEnum.LAYOUT_1,
      type: QuestionTypeEnum.SINGLE_CHOICE,
      metadata: {
        tp: 'School',
        objective: 'Application',
        difficultylevel: 'Average'
      },
      qtext:
        'I want to be an artist. I look forward to the ______ class in school.',
      // media: '<img src="google.png" type="png" />',
      media: null,
      qadd: 'Additional sub question details',
      options: {
        option: [
          { id: 1, value: 'Religious Studies', isCorrect: null },
          { id: 2, value: 'Geometry', isCorrect: null },
          { id: 3, value: 'Physical Education (PE)', isCorrect: null },
          { id: 4, value: 'Art', isCorrect: true }
        ]
      },
      uid: '',
      feedbacks: '',
      id: '1',
      questmedia: ''
    }
  },
  {
    questionId: 'question-id2',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      layout: QuestionLayoutTypeEnum.LAYOUT_1,
      type: QuestionTypeEnum.SINGLE_CHOICE,
      metadata: {
        tp: 'School',
        objective: 'Application',
        difficultylevel: 'Average'
      },
      qtext: 'Who is the coolest person?',
      // media: '<img src="google.png" type="png" />',
      media: null,
      qadd: 'Additional sub question details',
      options: {
        option: [
          { id: 1, value: 'Alex Knipfer', isCorrect: true },
          { id: 2, value: 'Chris Ransdell', isCorrect: null },
          { id: 3, value: 'Tyler Knipfer', isCorrect: null },
          { id: 4, value: 'Josh Marston', isCorrect: null }
        ]
      },
      uid: '',
      feedbacks: '',
      id: '2',
      questmedia: ''
    }
  },
  {
    questionId: 'question-id3',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      id: '3',
      layout: QuestionLayoutTypeEnum.LAYOUT_1,
      type: QuestionTypeEnum.MULTIPLE_CHOICE,
      metadata: {
        tp: 'College',
        objective: 'Application',
        difficultylevel: 'Average'
      },
      qtext:
        'The universal set is usually represented in a Venn diagram by a ___________.',
      // media: '<img src="microsoft.jpg" type="jpg" />',
      media: null,
      questmedia: '',
      qadd: null,
      options: {
        option: [
          { id: 1, value: 'triangle', isCorrect: null },
          { id: 2, value: 'circle', isCorrect: true },
          { id: 3, value: 'rectangle', isCorrect: null },
          { id: 4, value: 'oval', isCorrect: true }
        ]
      },
      feedbacks: '',
      uid: ''
    }
  },
  {
    questionId: '00283684-0D10-460D-A0D0-918032E861BB_hi',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      layout: QuestionLayoutTypeEnum.LAYOUT_2,
      type: QuestionTypeEnum.SINGLE_CHOICE,
      qtext: "<P><img src='https://via.placeholder.com/150'/><\\/P>",
      metadata: {
        difficultylevel: 'Difficult',
        tp: 'Parts and Wholes',
        objective: 'Skill'
      },
      options: {
        option: [
          {
            id: 1,
            value: '<P><img src="qz_05maen_04_img05a.png"\'/><\\/P>',
            isCorrect: false
          },
          {
            id: 2,
            value: '<P><img src="qz_05maen_04_img05b.png"/><\\/P>',
            isCorrect: false
          },
          {
            id: 3,
            value: '<P><img src="qz_05maen_04_img05c.png"/><\\/P>',
            isCorrect: true
          },
          {
            id: 4,
            value: '<P><img src="qz_05maen_04_img05d.png"/><\\/P>',
            isCorrect: false
          }
        ]
      },
      id: '2',
      uid: '00283684-0D10-460D-A0D0-918032E861BB_hi',
      feedbacks: '',
      media: null,
      questmedia: '',
      qadd: null
    }
  },
  {
    questionId: '019394D4-94CE-45F9-A664-36DF6B70E4D4',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      layout: QuestionLayoutTypeEnum.LAYOUT_2,
      type: QuestionTypeEnum.SINGLE_CHOICE,
      qtext: '<img src="https://via.placeholder.com/150" />',
      metadata: {
        difficultylevel: 'Easy',
        tp: 'A',
        objective: 'Knowledge'
      },
      options: {
        option: [
          {
            id: 1,
            value: '10',
            isCorrect: null
          },
          {
            id: 2,
            value: '16',
            isCorrect: null
          },
          {
            id: 3,
            value: '20',
            isCorrect: true
          },
          {
            id: 4,
            value: '40',
            isCorrect: null
          }
        ]
      },
      id: '4',
      uid: '00283684-0D10-460D-A0D0-918032EB_hi',
      feedbacks: '',
      media: null,
      questmedia: '',
      qadd: null
    }
  },
  {
    questionId: '025CBD74-D08F-4A3E-87F0-5BB5BD64B945',
    encryptedPath: 'fake_encrypted_path/',
    questionDetails: {
      layout: QuestionLayoutTypeEnum.LAYOUT_2,
      qtext:
        "Savita says that 15 more than two times her age is her father's age. If her father is 45 years old, how old is Savita?",
      type: QuestionTypeEnum.SINGLE_CHOICE,
      metadata: {
        difficultylevel: 'Easy',
        tp: 'a',
        objective: 'Knowledge'
      },
      options: {
        option: [
          {
            id: 1,
            value: '5 years',
            isCorrect: null
          },
          {
            id: 2,
            value: '10 years',
            isCorrect: null
          },
          {
            id: 3,
            value: '15 years',
            isCorrect: true
          },
          {
            id: 4,
            value: '20 years',
            isCorrect: null
          }
        ]
      },
      id: '8',
      uid: '00283684-0D10-460D-A08032EB_hi',
      feedbacks: '',
      media: null,
      questmedia: '',
      qadd: null
    }
  }
];
