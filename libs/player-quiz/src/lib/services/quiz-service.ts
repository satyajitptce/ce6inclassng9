import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { QuestionTypeEnum } from '../enums/question-type.enum';
import {
  SingleChoiceQuestion,
  MultipleChoiceQuestion,
  BaseQuestion
} from '../models/quiz.interface';
import { CurriculumPlaylistService, RequestApiService } from '@tce/core';
import { catchError } from 'rxjs/operators';
import { UtilityConfigService } from '@tce/question-bank';
import { QuestionEditorService } from '@tce/template-editor';

@Injectable()
export class QuizService {
  xmlDoc;
  xmlData;
  option;
  public tpType = '';
  private questions = new ReplaySubject<BaseQuestion[]>(1);
  public questions$ = this.questions.asObservable();

  private fetchError = new ReplaySubject<any>(1);
  public fetchError$ = this.fetchError.asObservable();

  private isFetching = new ReplaySubject<boolean>(1);
  public isFetching$ = this.isFetching.asObservable();

  public customQuestionIdArray = [];

  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private utilityConfigService: UtilityConfigService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private questionEditorService: QuestionEditorService
  ) {}

  // selectFile(event){
  //   const reader = new FileReader();
  //   reader.onload = (e: any) => {
  //     let xml = e.target.result;
  // this.inputXml = xml;
  // let result1 = converter.xml2json(xml, {compact: true, spaces: 2});

  // const JSONData = JSON.parse(result1);
  // this.formGroup.patchValue(JSONData)
  // 	}
  //   reader.readAsText(event.target.files[0])
  // }

  //backup thiru
  // getQuestions(questionIds: string[], customQuestion?) {
  //   //  console.log('filedata--> ', questionIds);
  //   //if(questionIds[0].encryptedpath)
  //   // if(questionIds && questionIds.length > 0){
  //   //   for(let i = 0; i < questionIds.length; i++){
  //   //     if(questionIds[i].charAt(0) === 'c'){
  //   //             const questionFilepath = this.curriculumPlaylistService.getQuestionSource(questionIds[i]);
  //   //             this.curriculumPlaylistService.httpGetQuestionSource(questionFilepath, questionIds[i]);
  //   //     }
  //   //   }
  //   // }

  //   let customData = [];
  //   customData = customQuestion;
  //   // console.log('customData', customData);

  //   this.http
  //     .get<any>(
  //       this.requestApiService
  //         .getUrl('assetResourceQuestion')
  //         .replace('@ids@', questionIds.join(','))
  //     )
  //     .pipe(
  //       catchError(err => {
  //         this.setFetchError(err);
  //         throw err;
  //       })
  //     )
  //     .subscribe((questionResponse: any[]) => {
  //       // console.log('tpType in service ', this.tpType);
  //       console.log('questionResponse ', questionResponse);
  //       // console.log('tpType---------->', this.tpType);
  //       if (this.tpType === 'quiz') {
  //         console.log('tpType ', this.tpType);

  //         this.questionEditorService.updateSubmitAnsShow(false);

  //         if (customData && customData.length > 0) {
  //           console.log(
  //             'customQuestion ',
  //             customData[0].questionDetails.data.questions[0].data.type
  //           );
  //           customData.forEach(question => {
  //             if (
  //               question.questionDetails.data.questions[0].data.type !==
  //               'OPENENDEDSTEMONLY'
  //             )
  //               questionResponse.push(JSON.stringify(question));
  //           });
  //         }
  //         for (let i = 0; i < questionResponse.length; i++) {
  //           let str: any = questionResponse[i].questionDetails;

  //           if (str.charAt(0) === '<') {
  //             //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
  //             questionResponse[i] = JSON.stringify(
  //               this.getJSONfromXML(questionResponse[i])
  //             );
  //           } else {
  //             // console.log('baseData ', questionResponse[i]);
  //             let jsonStr: any = JSON.parse(
  //               questionResponse[i].questionDetails
  //             );
  //             let mappingData: any;
  //             if (
  //               jsonStr.questionDetails &&
  //               'meta' in jsonStr.questionDetails
  //             ) {
  //               questionResponse[i].questionDetails = JSON.stringify(
  //                 this.utilityConfigService.studiToTestEdgeMapper(
  //                   jsonStr.questionDetails,
  //                   questionResponse[i].questionId,
  //                   questionResponse[i].encryptedPath
  //                 )
  //               );
  //             } else if (jsonStr && 'meta' in jsonStr) {
  //               questionResponse[i].questionDetails = JSON.stringify(
  //                 this.utilityConfigService.studiToTestEdgeMapper(
  //                   jsonStr,
  //                   questionResponse[i].questionId,
  //                   questionResponse[i].encryptedPath
  //                 )
  //               );
  //             }
  //             // questionResponse[i] = data.questionDetails;
  //           }
  //         }

  //         console.log('questionsResponse ', questionResponse);

  //         // if (this.tpType === 'quiz') {
  //         try {
  //           // console.log('try', questionResponse.length);

  //           if (!questionResponse || questionResponse.length < 1) {
  //             throw new Error('No question response available!');
  //           }
  //           const questionArray: BaseQuestion[] = questionResponse.map(
  //             question => {
  //               const questionJSON = JSON.parse(question.questionDetails);
  //               console.log(
  //                 'questionJSON.questionDetails.type-->> ',
  //                 questionJSON
  //               );
  //               console.log(question);
  //               if (questionJSON.questionDetails) {
  //                 switch (questionJSON.questionDetails.type) {
  //                   case QuestionTypeEnum.SINGLE_CHOICE:
  //                     return new SingleChoiceQuestion(
  //                       questionJSON.questionDetails,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                   case QuestionTypeEnum.MULTIPLE_CHOICE:
  //                     return new MultipleChoiceQuestion(
  //                       questionJSON.questionDetails,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                   default:
  //                     throw new Error('Invalid question type, need to handle.');
  //                 }
  //               }
  //             }
  //           );
  //           //console.log('questionArray', questionArray);

  //           this.questions.next(questionArray);
  //           // customData = [];
  //         } catch (err) {
  //           console.log('catch');
  //           this.setFetchError(err);
  //         }
  //       }
  //       if (this.tpType === 'custom') {
  //         this.questionEditorService.updateSubmitAnsShow(false);

  //         if (customData && customData.length > 0) {
  //           console.log(
  //             'customQuestion ',
  //             customData[0].questionDetails.data.questions[0].data.type
  //           );
  //           customData.forEach(question => {
  //             if (
  //               question.questionDetails.data.questions[0].data.type !==
  //               'OPENENDEDSTEMONLY'
  //             )
  //               questionResponse.push(JSON.stringify(question));
  //           });
  //         }
  //         console.log('response in csutom', questionResponse);

  //         for (let i = 0; i < questionResponse.length; i++) {
  //           let str: any = questionResponse[i].questionDetails;

  //           if (str.charAt(0) === '<') {
  //             //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
  //             questionResponse[i] = JSON.stringify(
  //               this.getJSONfromXML(questionResponse[i])
  //             );
  //           } else {
  //             // console.log('baseData ', questionResponse[i]);
  //             let jsonStr: any = JSON.parse(
  //               questionResponse[i].questionDetails
  //             );
  //             let mappingData: any;
  //             if (
  //               jsonStr.questionDetails &&
  //               'meta' in jsonStr.questionDetails
  //             ) {
  //               questionResponse[i].questionDetails = JSON.stringify(
  //                 this.utilityConfigService.studiToTestEdgeMapper(
  //                   jsonStr.questionDetails,
  //                   questionResponse[i].questionId,
  //                   questionResponse[i].encryptedPath
  //                 )
  //               );
  //             } else if (jsonStr && 'meta' in jsonStr) {
  //               questionResponse[i].questionDetails = JSON.stringify(
  //                 this.utilityConfigService.studiToTestEdgeMapper(
  //                   jsonStr,
  //                   questionResponse[i].questionId,
  //                   questionResponse[i].encryptedPath
  //                 )
  //               );
  //             }
  //             // questionResponse[i] = data.questionDetails;
  //           }
  //         }

  //         console.log('questionsResponse ', questionResponse);

  //         // if (this.tpType === 'quiz') {
  //         try {
  //           // console.log('try', questionResponse.length);

  //           if (!questionResponse || questionResponse.length < 1) {
  //             throw new Error('No question response available!');
  //           }
  //           const questionArray: BaseQuestion[] = questionResponse.map(
  //             question => {
  //               const questionJSON = JSON.parse(question.questionDetails);
  //               console.log(
  //                 'questionJSON.questionDetails.type-->> ',
  //                 questionJSON
  //               );
  //               console.log(question);
  //               if (questionJSON.questionDetails) {
  //                 switch (questionJSON.questionDetails.type) {
  //                   case QuestionTypeEnum.SINGLE_CHOICE:
  //                     return new SingleChoiceQuestion(
  //                       questionJSON.questionDetails,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                   case QuestionTypeEnum.MULTIPLE_CHOICE:
  //                     return new MultipleChoiceQuestion(
  //                       questionJSON.questionDetails,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                   default:
  //                     throw new Error('Invalid question type, need to handle.');
  //                 }
  //               }
  //             }
  //           );
  //           //console.log('questionArray', questionArray);

  //           this.questions.next(questionArray);
  //           // customData = [];
  //         } catch (err) {
  //           console.log('catch');
  //           this.setFetchError(err);
  //         }
  //       }
  //       if (this.tpType === 'cquiz') {
  //         console.log('cQuiz', questionResponse);

  //         this.questionEditorService.updateSubmitAnsShow(false);
  //         if (customData && customData.length > 0) {
  //           console.log(
  //             'customQuestion ',
  //             customData[0].questionDetails.data.questions[0].data.type
  //           );
  //           customData.forEach(question => {
  //             if (
  //               question.questionDetails.data.questions[0].data.type !==
  //               'OPENENDEDSTEMONLY'
  //             )
  //               questionResponse.push(JSON.stringify(question));
  //           });
  //         }
  //         for (let i = 0; i < questionResponse.length; i++) {
  //           let str: any = questionResponse[i].questionDetails;
  //           // console.log('baseData cquiz', questionResponse[i]);
  //           if (questionResponse[i]) {
  //             if (str.charAt(0) === '<') {
  //               //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
  //               questionResponse[i] = JSON.stringify(
  //                 this.getJSONfromXML(questionResponse[i])
  //               );
  //             } else {
  //               let jsonStr: any = JSON.parse(
  //                 questionResponse[i].questionDetails
  //               );
  //               let mappingData: any;
  //               if (
  //                 jsonStr.questionDetails &&
  //                 'meta' in jsonStr.questionDetails
  //               ) {
  //                 questionResponse[i].questionDetails = JSON.stringify(
  //                   this.utilityConfigService.studiToTestEdgeMapper(
  //                     jsonStr.questionDetails,
  //                     questionResponse[i].questionId,
  //                     questionResponse[i].encryptedPath
  //                   )
  //                 );
  //               } else if (jsonStr && 'meta' in jsonStr) {
  //                 questionResponse[i].questionDetails = JSON.stringify(
  //                   this.utilityConfigService.studiToTestEdgeMapper(
  //                     jsonStr,
  //                     questionResponse[i].questionId,
  //                     questionResponse[i].encryptedPath
  //                   )
  //                 );
  //               }
  //               // questionResponse[i] = data.questionDetails;
  //             }
  //           }
  //         }

  //         // console.log('exercise questions ', questionResponse);

  //         // if (this.tpType === 'quiz') {
  //         try {
  //           // console.log('try');

  //           if (!questionResponse || questionResponse.length < 1) {
  //             throw new Error('No question response available!');
  //           }
  //           let arr = [];
  //           let questionArray: BaseQuestion[] = questionResponse.map(
  //             question => {
  //               if (question) {
  //                 const questionJSON = JSON.parse(question.questionDetails);
  //                 console.log(
  //                   'questionJSON.questionDetails.type-->> ',
  //                   questionJSON
  //                 );
  //                 //console.log(question)

  //                 switch (questionJSON.type) {
  //                   case QuestionTypeEnum.SINGLE_CHOICE:
  //                     let a = new SingleChoiceQuestion(
  //                       questionJSON,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                     arr.push(a);
  //                     return a;
  //                   case QuestionTypeEnum.MULTIPLE_CHOICE:
  //                     let b = new MultipleChoiceQuestion(
  //                       questionJSON,
  //                       this.requestApiService.getUrl('getFile'),
  //                       question.encryptedPath,
  //                       question.questionId
  //                     );
  //                     arr.push(b);
  //                     return b;
  //                   default:
  //                     throw new Error('Invalid question type, need to handle.');
  //                 }
  //               } else {
  //                 console.log('question ', question);
  //               }
  //             }
  //           );
  //           questionArray = arr;
  //           // console.log('questionArray', questionArray, arr);

  //           this.questions.next(questionArray);
  //           // customData = [];
  //         } catch (err) {
  //           console.log('catch');
  //           this.setFetchError(err);
  //         }
  //       }
  //       if (this.tpType === 'exercise') {
  //         customData.forEach(question => {
  //           if (
  //             question.questionDetails.data.questions[0].data.type ===
  //             'OPENENDEDSTEMONLY'
  //           )
  //             questionResponse.push(JSON.stringify(question));
  //         });
  //         // console.log('exercise qtn ', JSON.parse(str));
  //         for (let i = 0; i < questionResponse.length; i++) {
  //           let str: any = questionResponse[i].questionDetails;
  //           if (str.charAt(0) === '<') {
  //             questionResponse[
  //               i
  //             ] = this.utilityConfigService.xmlToOpenEndedMapper(str);
  //           } else {
  //             let jsonStr: any = JSON.parse(
  //               questionResponse[i].questionDetails
  //             );
  //             if (jsonStr && 'meta' in jsonStr) {
  //               questionResponse[
  //                 i
  //               ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
  //             }
  //             // else if (jsonStr && 'meta' in jsonStr) {
  //             //   questionResponse[
  //             //     i
  //             //   ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
  //             // }
  //           }
  //           this.questions.next(questionResponse);
  //         }
  //       }
  //       if (this.tpType === 'cexercise') {
  //         customData.forEach(question => {
  //           if (
  //             question.questionDetails.data.questions[0].data.type ===
  //             'OPENENDEDSTEMONLY'
  //           )
  //             questionResponse.push(JSON.stringify(question));
  //         });
  //         console.log('exercise qtn ', questionResponse);
  //         for (let i = 0; i < questionResponse.length; i++) {
  //           let str: any = questionResponse[i].questionDetails;
  //           if (str.charAt(0) === '<') {
  //             questionResponse[
  //               i
  //             ].questionDetails = this.utilityConfigService.xmlToOpenEndedMapper(
  //               str
  //             );
  //           } else {
  //             let jsonStr: any = JSON.parse(
  //               questionResponse[i].questionDetails
  //             );
  //             if (
  //               jsonStr.questionDetails &&
  //               'meta' in jsonStr.questionDetails
  //             ) {
  //               questionResponse[
  //                 i
  //               ] = this.utilityConfigService.studiToOpenEndedMapper(
  //                 jsonStr.questionDetails
  //               );
  //             } else if (jsonStr && 'meta' in jsonStr) {
  //               questionResponse[
  //                 i
  //               ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
  //             }
  //           }
  //           this.questions.next(questionResponse);
  //         }
  //       }
  //     });
  // }

  getQuestions(questionIds: string[], customQuestion?) {
    //console.log('filedata--> ', questionIds);
    // console.log('customQuestion --> ', customQuestion);
    //if(questionIds[0].encryptedpath)
    // if(questionIds && questionIds.length > 0){
    //   for(let i = 0; i < questionIds.length; i++){
    //     if(questionIds[i].charAt(0) === 'c'){
    //             const questionFilepath = this.curriculumPlaylistService.getQuestionSource(questionIds[i]);
    //             this.curriculumPlaylistService.httpGetQuestionSource(questionFilepath, questionIds[i]);
    //     }
    //   }
    // }

    // let customData = [];
    // customData = customQuestion;
    // console.log('customData', customData);

    this.http
      .get<any>(
        this.requestApiService
          .getUrl('assetResourceQuestion')
          .replace('@ids@', questionIds.join(','))
      )
      .pipe(
        catchError(err => {
          this.setFetchError(err);
          throw err;
        })
      )
      .subscribe((questionResponse: any[]) => {
        // console.log('tpType in service ', this.tpType);
        // console.log('questionResponse ', questionResponse);
        // console.log('tpType---------->', this.tpType);
        // if (this.tpType === 'quiz') {
        // console.log('tpType ', this.tpType);

        // this.questionEditorService.updateSubmitAnsShow(false);

        // if (customData && customData.length > 0) {
        //   console.log(
        //     'customQuestion ',
        //     customData[0].questionDetails.data.questions[0].data.type
        //   );
        //   customData.forEach(question => {
        //     if (
        //       question.questionDetails.data.questions[0].data.type !==
        //       'OPENENDEDSTEMONLY'
        //     )
        //       questionResponse.push(JSON.stringify(question));
        //   });
        // }

        // console.log('questionsResponse ', questionResponse);

        // if (this.tpType === 'quiz') {
        // if (
        //   this.tpType === 'quiz' ||
        //   this.tpType === 'cquiz' ||
        //   this.tpType === 'custom'
        // ) {
        // try {
        // console.log('try');

        if (this.tpType === 'quiz') {
          this.questionEditorService.updateSubmitAnsShow(false);
          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;

            if (str.charAt(0) === '<') {
              //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
              questionResponse[i] = JSON.stringify(
                this.getJSONfromXML(questionResponse[i])
              );
            } else {
              // console.log('baseData ', questionResponse[i]);
              let jsonStr: any = JSON.parse(
                questionResponse[i].questionDetails
              );
              let mappingData: any;
              if (
                jsonStr.questionDetails &&
                'meta' in jsonStr.questionDetails
              ) {
                questionResponse[i].questionDetails = JSON.stringify(
                  this.utilityConfigService.studiToTestEdgeMapper(
                    jsonStr.questionDetails,
                    questionResponse[i].questionId,
                    questionResponse[i].encryptedPath
                  )
                );
              } else if (jsonStr && 'meta' in jsonStr) {
                questionResponse[i].questionDetails = JSON.stringify(
                  this.utilityConfigService.studiToTestEdgeMapper(
                    jsonStr,
                    questionResponse[i].questionId,
                    questionResponse[i].encryptedPath
                  )
                );
              }
              // questionResponse[i] = data.questionDetails;
            }
          }
        }
        if (this.tpType === 'cquiz') {
          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;
            // console.log('baseData cquiz', questionResponse[i]);
            if (questionResponse[i]) {
              if (str.charAt(0) === '<') {
                //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
                questionResponse[i] = JSON.stringify(
                  this.getJSONfromXML(questionResponse[i])
                );
              } else {
                let jsonStr: any = JSON.parse(
                  questionResponse[i].questionDetails
                );
                let mappingData: any;
                if (
                  jsonStr.questionDetails &&
                  'meta' in jsonStr.questionDetails
                ) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr.questionDetails,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                } else if (jsonStr && 'meta' in jsonStr) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                }
                // questionResponse[i] = data.questionDetails;
              }
            }
          }
        }
        if (this.tpType === 'custom') {
          this.questionEditorService.updateSubmitAnsShow(false);
          // console.log('custom--> ', questionResponse);
          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;
            if (str.charAt(0) === '<') {
              //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
              questionResponse[i] = this.getJSONfromXML(str);
              // console.log('xmlxmlxml ', questionResponse[i]);
            } else {
              let jsonStr: any = JSON.parse(
                questionResponse[i].questionDetails
              );
              if (questionResponse[i].questionSourceType === 'tata') {
                questionResponse[i].questionDetails = jsonStr.questionDetails;
              }
              if (
                jsonStr.questionDetails &&
                'meta' in jsonStr.questionDetails
              ) {
                let questionType =
                  jsonStr.questionDetails.data.questions[0].data.type;
                if (
                  questionType === 'SCQ' ||
                  questionType === 'MCQ' ||
                  questionType === 'mcq-tf'
                ) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr.questionDetails,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                } else {
                  jsonStr.questionDetails['questionId'] =
                    questionResponse[i].questionId;
                  jsonStr.questionDetails['encryptedPath'] =
                    questionResponse[i].encryptedPath;
                  questionResponse[
                    i
                  ] = this.utilityConfigService.studiToOpenEndedMapper(
                    jsonStr.questionDetails
                  );
                }
              } else if (jsonStr && 'meta' in jsonStr) {
                let questionType = jsonStr.data.questions[0].data.type;
                if (
                  questionType === 'SCQ' ||
                  questionType === 'MCQ' ||
                  questionType === 'mcq-tf'
                ) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                } else {
                  jsonStr['questionId'] = questionResponse[i].questionId;
                  jsonStr['encryptedPath'] = questionResponse[i].encryptedPath;
                  questionResponse[
                    i
                  ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
                }
              } else {
                questionResponse[i].questionDetails = JSON.stringify(jsonStr);
              }

              // let questionType =
              //   if (str.charAt(0) === '<') {
              //     //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
              //     questionResponse[i] = JSON.stringify(
              //       this.getJSONfromXML(questionResponse[i])
              //     );
              //   } else {
              //     // console.log('baseData ', questionResponse[i]);
              //     let jsonStr: any = JSON.parse(
              //       questionResponse[i].questionDetails
              //     );
              //     let mappingData: any;
              // if (
              //   jsonStr.questionDetails &&
              //   'meta' in jsonStr.questionDetails
              // ) {
              //   questionResponse[i].questionDetails = JSON.stringify(
              //     this.utilityConfigService.studiToTestEdgeMapper(
              //       jsonStr.questionDetails,
              //       questionResponse[i].questionId,
              //       questionResponse[i].encryptedPath
              //     )
              //   );
              // } else if (jsonStr && 'meta' in jsonStr) {
              //   questionResponse[i].questionDetails = JSON.stringify(
              //     this.utilityConfigService.studiToTestEdgeMapper(
              //       jsonStr,
              //       questionResponse[i].questionId,
              //       questionResponse[i].encryptedPath
              //     )
              //   );
              // }
              //     // questionResponse[i] = data.questionDetails;
              //   }
            }
          }
        }
        // console.log('try', questionResponse);
        // if(questionResponse[i].)
        if (!questionResponse || questionResponse.length < 1) {
          throw new Error('No question response available!');
        }
        const questionArray: BaseQuestion[] = questionResponse.map(question => {
          // console.log('question in custom ', question);
          if (question.questionDetails) {
            const questionJSON = JSON.parse(question.questionDetails);
            // console.log(
            //   'questionJSON.questionDetails.type-->> ',
            //   questionJSON
            // );
            // console.log(question);
            if (questionJSON.questionDetails) {
              switch (questionJSON.questionDetails.type) {
                case QuestionTypeEnum.SINGLE_CHOICE:
                  return new SingleChoiceQuestion(
                    questionJSON.questionDetails,
                    this.requestApiService.getUrl('getFile'),
                    question.encryptedPath,
                    question.questionId
                  );
                case QuestionTypeEnum.MULTIPLE_CHOICE:
                  return new MultipleChoiceQuestion(
                    questionJSON.questionDetails,
                    this.requestApiService.getUrl('getFile'),
                    question.encryptedPath,
                    question.questionId
                  );
                case QuestionTypeEnum.OE:
                  return question;
                default:
                  return question;
              }
            } else {
              let arr = [];
              console.log('questionJSON.type', question);

              switch (questionJSON.type) {
                case QuestionTypeEnum.SINGLE_CHOICE:
                  let a = new SingleChoiceQuestion(
                    questionJSON,
                    this.requestApiService.getUrl('getFile'),
                    question.encryptedPath,
                    question.questionId
                  );
                  arr.push(a);
                  return a;
                case QuestionTypeEnum.MULTIPLE_CHOICE:
                  let b = new MultipleChoiceQuestion(
                    questionJSON,
                    this.requestApiService.getUrl('getFile'),
                    question.encryptedPath,
                    question.questionId
                  );
                  arr.push(b);
                  return b;
                case QuestionTypeEnum.OE:
                  return question;
                default:
                  return question;
              }
            }
          } else {
            return question;
          }
        });
        //console.log('questionArray', questionArray);

        this.questions.next(questionArray);
        // customData = [];
        // } catch (err) {
        //   console.log('catch', err);
        //   this.setFetchError(err);
        // }
        // }
        // }
        /*if (this.tpType === 'custom') {
          this.questionEditorService.updateSubmitAnsShow(false);

          // if (customData && customData.length > 0) {
          //   console.log(
          //     'customQuestion ',
          //     customData[0].questionDetails.data.questions[0].data.type
          //   );
          //   customData.forEach(question => {
          //     if (
          //       question.questionDetails.data.questions[0].data.type !==
          //       'OPENENDEDSTEMONLY'
          //     )
          //       questionResponse.push(JSON.stringify(question));
          //   });
          // }
          // console.log('response in csutom', questionResponse);

          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;

            if (str.charAt(0) === '<') {
              //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
              questionResponse[i] = JSON.stringify(
                this.getJSONfromXML(questionResponse[i])
              );
            } else {
              // console.log('baseData ', questionResponse[i]);
              let jsonStr: any = JSON.parse(
                questionResponse[i].questionDetails
              );
              let mappingData: any;
              if (
                jsonStr.questionDetails &&
                'meta' in jsonStr.questionDetails
              ) {
                questionResponse[i].questionDetails = JSON.stringify(
                  this.utilityConfigService.studiToTestEdgeMapper(
                    jsonStr.questionDetails,
                    questionResponse[i].questionId,
                    questionResponse[i].encryptedPath
                  )
                );
              } else if (jsonStr && 'meta' in jsonStr) {
                questionResponse[i].questionDetails = JSON.stringify(
                  this.utilityConfigService.studiToTestEdgeMapper(
                    jsonStr,
                    questionResponse[i].questionId,
                    questionResponse[i].encryptedPath
                  )
                );
              }
              // questionResponse[i] = data.questionDetails;
            }
          }

          // console.log('questionsResponse ', questionResponse);

          // if (this.tpType === 'quiz') {
          try {
            // console.log('try', questionResponse.length);

            if (!questionResponse || questionResponse.length < 1) {
              throw new Error('No question response available!');
            }
            const questionArray: BaseQuestion[] = questionResponse.map(
              question => {
                const questionJSON = JSON.parse(question.questionDetails);
                // console.log(
                //   'questionJSON.questionDetails.type-->> ',
                //   questionJSON
                // );
                // console.log(question);
                if (questionJSON.questionDetails) {
                  switch (questionJSON.questionDetails.type) {
                    case QuestionTypeEnum.SINGLE_CHOICE:
                      return new SingleChoiceQuestion(
                        questionJSON.questionDetails,
                        this.requestApiService.getUrl('getFile'),
                        question.encryptedPath,
                        question.questionId
                      );
                    case QuestionTypeEnum.MULTIPLE_CHOICE:
                      return new MultipleChoiceQuestion(
                        questionJSON.questionDetails,
                        this.requestApiService.getUrl('getFile'),
                        question.encryptedPath,
                        question.questionId
                      );
                    default:
                      throw new Error('Invalid question type, need to handle.');
                  }
                }
              }
            );
            //console.log('questionArray', questionArray);

            this.questions.next(questionArray);
            // customData = [];
          } catch (err) {
            console.log('catch');
            this.setFetchError(err);
          }
        }*/
        /*if (this.tpType === 'cquiz') {
          // console.log('cQuiz', questionResponse);

          this.questionEditorService.updateSubmitAnsShow(false);
          // if (customData && customData.length > 0) {
          //   console.log(
          //     'customQuestion ',
          //     customData[0].questionDetails.data.questions[0].data.type
          //   );
          //   customData.forEach(question => {
          //     if (
          //       question.questionDetails.data.questions[0].data.type !==
          //       'OPENENDEDSTEMONLY'
          //     )
          //       questionResponse.push(JSON.stringify(question));
          //   });
          // }
          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;
            // console.log('baseData cquiz', questionResponse[i]);
            if (questionResponse[i]) {
              if (str.charAt(0) === '<') {
                //console.log("<<-- FOUND TESTEDGE QUESTION XML-->> ");
                questionResponse[i] = JSON.stringify(
                  this.getJSONfromXML(questionResponse[i])
                );
              } else {
                let jsonStr: any = JSON.parse(
                  questionResponse[i].questionDetails
                );
                let mappingData: any;
                if (
                  jsonStr.questionDetails &&
                  'meta' in jsonStr.questionDetails
                ) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr.questionDetails,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                } else if (jsonStr && 'meta' in jsonStr) {
                  questionResponse[i].questionDetails = JSON.stringify(
                    this.utilityConfigService.studiToTestEdgeMapper(
                      jsonStr,
                      questionResponse[i].questionId,
                      questionResponse[i].encryptedPath
                    )
                  );
                }
                // questionResponse[i] = data.questionDetails;
              }
            }
          }

          // console.log('exercise questions ', questionResponse);

          // if (this.tpType === 'quiz') {
          try {
            // console.log('try');

            if (!questionResponse || questionResponse.length < 1) {
              throw new Error('No question response available!');
            }
            let arr = [];
            let questionArray: BaseQuestion[] = questionResponse.map(
              question => {
                if (question) {
                  const questionJSON = JSON.parse(question.questionDetails);
                  // console.log(
                  //   'questionJSON.questionDetails.type-->> ',
                  //   questionJSON
                  // );
                  //console.log(question)

                  switch (questionJSON.type) {
                    case QuestionTypeEnum.SINGLE_CHOICE:
                      let a = new SingleChoiceQuestion(
                        questionJSON,
                        this.requestApiService.getUrl('getFile'),
                        question.encryptedPath,
                        question.questionId
                      );
                      arr.push(a);
                      return a;
                    case QuestionTypeEnum.MULTIPLE_CHOICE:
                      let b = new MultipleChoiceQuestion(
                        questionJSON,
                        this.requestApiService.getUrl('getFile'),
                        question.encryptedPath,
                        question.questionId
                      );
                      arr.push(b);
                      return b;
                    default:
                      throw new Error('Invalid question type, need to handle.');
                  }
                } else {
                  // console.log('question ', question);
                }
              }
            );
            questionArray = arr;
            // console.log('questionArray', questionArray, arr);

            this.questions.next(questionArray);
            // customData = [];
          } catch (err) {
            console.log('catch');
            this.setFetchError(err);
          }
        }*/
        if (this.tpType === 'exercise') {
          // customData.forEach(question => {
          //   if (
          //     question.questionDetails.data.questions[0].data.type ===
          //     'OPENENDEDSTEMONLY'
          //   )
          //     questionResponse.push(JSON.stringify(question));
          // });
          // console.log('exercise qtn ', JSON.parse(str));
          for (let i = 0; i < questionResponse.length; i++) {
            let str: any = questionResponse[i].questionDetails;
            if (str.charAt(0) === '<') {
              questionResponse[
                i
              ] = this.utilityConfigService.xmlToOpenEndedMapper(str);
            } else {
              let jsonStr: any = JSON.parse(
                questionResponse[i].questionDetails
              );
              if (jsonStr && 'meta' in jsonStr) {
                jsonStr['questionId'] = questionResponse[i].questionId;
                jsonStr['encryptedPath'] = questionResponse[i].encryptedPath;
                questionResponse[
                  i
                ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
              }
              // else if (jsonStr && 'meta' in jsonStr) {
              //   questionResponse[
              //     i
              //   ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
              // }
            }
            this.questions.next(questionResponse);
          }
        }
        if (this.tpType === 'cexercise') {
          // console.log('exercise ', questionResponse);
          // customData.forEach(question => {
          //   if (
          //     question.questionDetails.data.questions[0].data.type ===
          //     'OPENENDEDSTEMONLY'
          //   )
          //     questionResponse.push(JSON.stringify(question));
          //});
          // console.log('exercise qtn ', questionResponse);
          for (let i = 0; i < questionResponse.length; i++) {
            // console.log('questionResponse ', questionResponse[i]);
            let str: any = questionResponse[i].questionDetails;
            if (str.charAt(0) === '<') {
              questionResponse[
                i
              ].questionDetails = this.utilityConfigService.xmlToOpenEndedMapper(
                str
              );
            } else {
              let jsonStr: any = JSON.parse(
                questionResponse[i].questionDetails
              );
              if (
                jsonStr.questionDetails &&
                'meta' in jsonStr.questionDetails
              ) {
                jsonStr.questionDetails['questionId'] =
                  questionResponse[i].questionId;
                jsonStr.questionDetails['encryptedPath'] =
                  questionResponse[i].encryptedPath;
                questionResponse[
                  i
                ] = this.utilityConfigService.studiToOpenEndedMapper(
                  jsonStr.questionDetails
                );
              } else if (jsonStr && 'meta' in jsonStr) {
                jsonStr['questionId'] = questionResponse[i].questionId;
                jsonStr['encryptedPath'] = questionResponse[i].encryptedPath;
                questionResponse[
                  i
                ] = this.utilityConfigService.studiToOpenEndedMapper(jsonStr);
              }
            }
            this.questions.next(questionResponse);
          }
        }
      });
  }

  setFetchError(err) {
    this.fetchError.next(err);
  }

  getJSONfromXML(response) {
    // console.log('inside getJSONfromXML>>>>>>>>>>>>>>>>', response);
    let parser: any = new DOMParser();
    let xmlData: any = parser.parseFromString(response, 'application/xml');
    // console.log('xmlData', xmlData);
    if (xmlData.documentElement.nodeName === 'parsererror') {
      return '';
    } else {
      // console.log('xmlData.documentElement ', xmlData.documentElement);
      // let type = xmlData
      //   .getElementsByTagName('Question')[0]
      //   .getAttribute('template_Type');
      // if (type === 'OPENENDEDSTEMONLY') {
      //   return this.utilityConfigService.xmlToOpenEndedMapper(response);
      // } else {
      return this.schemaMCMSFormater(xmlData);
      // }
    }
    /// MCMS
  }
  schemaMCMSFormater(xmlData) {
    // console.log('inside getJSONfromXML>>>>>>>>>>>>>>>>', xmlData);

    let optionData = [];
    let opData;

    let type = xmlData
      .getElementsByTagName('Question')[0]
      .getAttribute('template_Type');
    if (type === 'MCTWO') {
      type = 'SC';
    } else if (type === 'MCMS') {
      type = 'MC';
    }
    // else if(type="OPENENDEDSTEMONLY"){
    //   type= ''
    // }
    else {
    }
    let optionLength = xmlData.getElementsByTagName('Choice')[0].childNodes
      .length;
    for (let i = 1; i <= optionLength; i++) {
      opData = {
        id: i,
        value: xmlData.getElementsByTagName('Text')[i].childNodes[0].nodeValue,
        isCorrect: JSON.parse(
          xmlData
            .getElementsByTagName('Choice_Option')
            [i - 1].getAttribute('correct')
        )
      };
      optionData.push(opData);
    }
    let xmlToJson = {
      questionId: xmlData
        .getElementsByTagName('Question')[0]
        .getAttribute('question_id'),
      questionDetails: {
        qtext: xmlData.getElementsByTagName('Text')[0].childNodes[0].nodeValue,
        options: {
          option: optionData
        },
        layout: xmlData
          .getElementsByTagName('Question')[0]
          .getAttribute('layout_Type'),
        type: type //xmlData.getElementsByTagName('Question')[0].getAttribute('question_Type');,
      }
    };

    return xmlToJson;
  }
}
