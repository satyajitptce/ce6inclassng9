import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { fakeData } from './../fake-data/fake-data';
import { QuestionTypeEnum } from '../enums/question-type.enum';
import {
  SingleChoiceQuestion,
  MultipleChoiceQuestion
} from '../models/quiz.interface';

@Injectable()
export class FakeQuestionsService {
  constructor(private http: HttpClient) {}

  /**
   * Fake api to get the list of questions
   * for the quiz player.
   *
   * @returns
   * @memberof FakeQuestionsService
   */
  getQuestions() {
    return fakeData.map(question => {
      switch (question.questionDetails.type) {
        case QuestionTypeEnum.SINGLE_CHOICE:
          return new SingleChoiceQuestion(question);
        case QuestionTypeEnum.MULTIPLE_CHOICE:
          return new MultipleChoiceQuestion(question);
        default:
          throw new Error('Invalid question type, need to handle.');
      }
    });
  }

  getRealQuestions(): Observable<any> {
    return this.http.get<any>(
      '/tce-teach-api/1/api/1/serve/question?ids=026B1480-8084-4C53-A7E2-CA3E2B2956EF,337F0227-F4E6-45A8-A474-785650B45E2A,4E150808-DD28-4414-99E4-EB89EE509FDF,AEEFB6AF-37F7-412B-93D9-4656D7998DD7,B4355A9A-6861-48CA-B316-834289EA93B7,C48A21F6-71B4-4264-9287-EE27DB4DEA41,E88CAFBA-A5B2-4CD1-992B-0BE85B4F6E5F,F481BBDB-FFFE-47C4-8B42-43D727713A96',
      {
        headers: new HttpHeaders({
          'Content-Type': 'application/x-www-form-urlencoded',
          Authorization: 'Bearer ' + sessionStorage.getItem('token')
        })
      }
    );
  }

  getQuestionImage(imageContent, encryptedPath): Promise<any> {
    const regex = /<img.*?src='(.*?)'/;
    const imageSrc = regex.exec(imageContent)[1];
    return new Promise((resolve, reject) => {
      if (imageSrc && encryptedPath) {
        const requestPath =
          '/tce-teach-api/0/api/content/filerangeservice?encryptedPath=' +
          encryptedPath +
          '&fileName=' +
          imageSrc;
        this.http
          .get(requestPath, {
            headers: new HttpHeaders({
              'Content-Type': 'application/octet-stream',
              Authorization: 'Bearer ' + sessionStorage.getItem('token')
            }),
            responseType: 'blob'
          })
          .subscribe(imageResp => {
            const reader = new FileReader();
            reader.onload = () => {
              resolve(reader.result);
            };

            if (imageResp) {
              reader.readAsDataURL(imageResp);
            }
          });
      }
    });
  }
}
