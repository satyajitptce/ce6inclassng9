import { FormArray, FormGroup, FormControl } from '@angular/forms';

import { BaseQuestionForm } from './_base/question-form.model';
import { SingleChoiceQuestion } from '../quiz.interface';

/**
 * Class definition for single choice question form
 * for implmenting in reactives forms
 *
 * @export
 * @class SingleChoiceQuestionForm
 * @extends {BaseQuestionForm}
 */
export class SingleChoiceQuestionForm extends BaseQuestionForm {
  options = new FormArray([]);
  // Collected the answer user submits
  selected = new FormControl();

  constructor(question: SingleChoiceQuestion) {
    // Set data into the parent class
    super(question);

    // Set options from parent, Set validators if required
    if (question.options) {
      question.options.forEach(op => {
        this.options.push(
          new FormGroup({
            id: new FormControl(op.id),
            value: new FormControl(op.value),
            // TBD - Check if required
            isCorrect: new FormControl(op.isCorrect),
            optionImage: new FormControl(op.optionImage)
          })
        );
      });
    }
  }
}
