import { FormArray, FormGroup, FormControl } from '@angular/forms';

import { BaseQuestionForm } from './_base/question-form.model';
import { MultipleChoiceQuestion } from '../quiz.interface';

/**
 * Class definition for multiple choice question
 * for implmenting in reactives forms
 *
 * @export
 * @class MultipleChoiceQuestionFrom
 * @extends {BaseQuestionForm}
 */
export class MultipleChoiceQuestionFrom extends BaseQuestionForm {
  options = new FormArray([]);

  constructor(question: MultipleChoiceQuestion) {
    // Set data into the parent class
    super(question);

    // Temp field to collect the selected items
    question.options.forEach(op => {
      this.options.push(
        new FormGroup({
          id: new FormControl(op.id),
          value: new FormControl(op.value),
          checked: new FormControl(false),
          // TBD - Check if required
          isCorrect: new FormControl(op.isCorrect)
        })
      );
    });
  }
}
