import { FormControl } from '@angular/forms';

import { BaseQuestion } from '../../quiz.interface';

/**
 * Class definition for question form
 * for implmenting in reactives forms
 *
 * @export
 * @class BaseQuestionForm
 */
export class BaseQuestionForm {
  id = new FormControl();
  layout = new FormControl();
  type = new FormControl();
  metadata = new FormControl();
  qtext = new FormControl();
  media = new FormControl();
  qadd = new FormControl();

  constructor(question: BaseQuestion) {
    // ID, Set validators if required
    this.id.setValue(question.id);

    // Layout, Set validators if required
    this.layout.setValue(question.layout);

    // Type, Set validators if required
    this.type.setValue(question.type);

    // Metadata, Set validators if required
    this.metadata.setValue(question.metadata);

    // Question, Set validators if required
    this.qtext.setValue(question.qtext);

    // Media, Set validators if required
    this.media.setValue(question.media);

    // Additional question data, Set validators if required
    this.qadd.setValue(question.qadd);
  }
}
