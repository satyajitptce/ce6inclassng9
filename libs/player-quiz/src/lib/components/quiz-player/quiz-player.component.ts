import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { QuestionStatusEnum } from '../../enums/question-status-type.enum';
import {
  BaseQuestion,
  SingleChoiceQuestion,
  MultipleChoiceQuestion,
  QuestionOption
} from '../../models/quiz.interface';
import { QuestionTypeEnum } from '../../enums/question-type.enum';
import { SingleChoiceQuestionForm } from '../../models/forms/sc-question-form.model';
import { MultipleChoiceQuestionFrom } from '../../models/forms/mc-question-form.model';
import {
  PlayerAbstractComponent,
  PlayerAbstractInterface,
  PlayerAbstractAnimations,
  ResourceType,
  CurriculumPlaylistService,
  PlayerUsageService,
  RequestApiService
} from '@tce/core';
import { QuizService } from '../../services/quiz-service';
import { QuestionEditorService } from '@tce/template-editor';

@Component({
  selector: 'tce-quiz-player',
  templateUrl: './quiz-player.component.html',
  styleUrls: ['./quiz-player.component.scss'],
  animations: PlayerAbstractAnimations
})
export class QuizPlayerComponent extends PlayerAbstractComponent
  implements OnInit, PlayerAbstractInterface {
  // Can be used to pass on the state of the quiz player to the state management service
  @Output() quizEvent = new EventEmitter<any>();

  // Question form reference
  quizForm: FormGroup;

  // Data will be prepopulated from the parent component
  questions: BaseQuestion[] = [];

  // Pagination
  questionOnDisplay: BaseQuestion;
  customQuestions: Array<any> = [];
  currentPage = 1;
  index: number;
  hasSubmitted = false;
  public arrayOfPages: any[] = [];

  quizQuestionStatuses: QuestionStatusEnum[];
  correctAnswerCheck: QuestionStatusEnum[];
  public allowSubmit: boolean = false;
  public tpType: string;
  private url = this.requestApiService.getUrl('getFile');

  constructor(
    private quizFormGroup: FormBuilder,
    private quizService: QuizService,
    private questionEditorService: QuestionEditorService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private playerUsageService: PlayerUsageService,
    private requestApiService: RequestApiService
  ) {
    super();
  }

  ngOnInit() {
    this.quizService.questions$.subscribe(newQuestionsArray => {
      this.onEndLoadingResource();
      // console.log('before quiz-player-ques', this.questions, newQuestionsArray);
      this.questions = newQuestionsArray;
      //console.log('quiz-player-ques', this.questions);
      // newQuestionsArray.forEach((question, index) => {
      //   if (question) this.arrayOfPages.push(index);
      // });
      this.arrayOfPages = Array.from(Array(newQuestionsArray.length).keys());
      this._initForm();
      this.quizQuestionStatuses = Array(this.questions.length).fill(
        QuestionStatusEnum.NONE
      );
      this.correctAnswerCheck = Array(this.questions.length).fill(
        QuestionStatusEnum.NONE
      );
    });
    this.quizService.fetchError$.subscribe(error => {
      this.onResourceLoadError('Unable to Load Quiz Questions');
    });

    /*this.curriculumPlaylistService.questionbankDataBroadcast$.subscribe(
      customQuestions => {
        let temp;
       // console.log('before customQuestions ',customQuestions);
        
        if (customQuestions) {
          // temp.push(customQuestions)
          this.customQuestions.push(customQuestions);

        }
        //this.customQuestions = this.customQuestions.filter((element, i) => i === this.customQuestions.indexOf(element))

        
      }
    );*/
    //console.log("questionIDs-------->",this.curriculumPlaylistService.customQuestionIds);
    this.customQuestions = this.curriculumPlaylistService.customQuestionDetails;
    // console.log('customQuestions ', this.customQuestions);

    //console.log("length of customquestion",this.customQuestions.length)
    this.customQuestions.reverse();
  }

  beginLoadingResource() {
    // this.customQuestions.reverse();
    //console.log("beginLoadingResource",this.customQuestions.length)
    let tpType = this.resource.tpId.split('-');
    this.tpType = tpType[0];
    // console.log('tpType ', this.resource.tpId);

    this.quizService.tpType = tpType[0];
    if (this.tpType === 'custom') {
      let customQuiz = this.resource.metaData.questionIds[0];
      const fileUrl =
        '/' +
        customQuiz.encryptedFilePath +
        '/' +
        customQuiz.assetId +
        '/quiz.json';
      this.curriculumPlaylistService.httpGetCustomQuiz(this.url + fileUrl);
      this.curriculumPlaylistService.cutomQuizIds$.subscribe(
        (data: string[]) => {
          console.log('custom-quiz ids--> ', data);

          if (data) {
            this.quizService.getQuestions(data);
          }
        }
      );
      //if (this.curriculumPlaylistService.cutomQuizIds) {
      // this.quizService.getQuestions(
      //   this.curriculumPlaylistService.cutomQuizIds,
      //   this.customQuestions
      // );
      // console.log(
      //   'usman questionIds-----------',
      //   this.curriculumPlaylistService.cutomQuizIds
      // );
      //}
    } else {
      if (this.resource.metaData.questionIds) {
        this.quizService.getQuestions(
          this.resource.metaData.questionIds,
          this.customQuestions
        );
      }
    }
  }

  private _initForm() {
    // Form initializer
    this.quizForm = this.quizFormGroup.group({
      questions: this.quizFormGroup.array([])
    });

    // Current question
    this.questionOnDisplay = this.questions[0];

    // console.log(
    //   'quizForm ',
    //   this.questions,
    //   this.quizForm,
    //   this.questionOnDisplay
    // );

    // Render the questions
    // this._renderQuestion(this.questionOnDisplay);
    this._addQuestionsControl();
  }

  private async _addQuestionsControl() {
    // Remove previous question, This would change based on the pagination
    // implementation that the developer is trying to use. But the underlying
    // concept would remain the same.
    if (this.questionsControl.length > 0) {
      this.questionsControl.removeAt(0);
    }

    // Create options control form field if not present
    if (!this.quizForm.get('questions')) {
      this.quizForm.addControl('questions', this.quizFormGroup.array([]));
    }
    // Add the questions control based on the question type
    this.questionsControl.push(
      this.quizFormGroup.group(this.generateFormControl(this.questionOnDisplay))
    );
    // console.log(
    //   'questionControl ',
    //   this.questionsControl,
    //   this.questionOnDisplay
    // );

    this.quizForm.markAsPristine();
  }

  generateFormControl(q: BaseQuestion) {
    // console.log('qType ', q.type);
    // if (this.tpType === 'quiz') {
    switch (q.type) {
      case QuestionTypeEnum.SINGLE_CHOICE:
        return new SingleChoiceQuestionForm(q as SingleChoiceQuestion);

      case QuestionTypeEnum.MULTIPLE_CHOICE:
        return new MultipleChoiceQuestionFrom(q as MultipleChoiceQuestion);

      default:
        // throw new Error('Invalid question type specified');
        return q;
    }
    // }
    // else {
    //   return q;
    // }
  }

  // async getImageValueForOption(content, encryptedPath) {
  //   let imageDataString;
  //   await this.fakeService
  //     .getQuestionImage(content, encryptedPath)
  //     .then(promisedImageData => {
  //       imageDataString = promisedImageData;
  //     });
  //   const imageDataStringSanitized = this._sanitizer.bypassSecurityTrustUrl(
  //     imageDataString
  //   );
  //   return imageDataStringSanitized;
  // }

  // GETTERS //-----------------------------------------------------------------

  /**
   * Get options field control
   */
  get questionsControl() {
    return this.quizForm.get('questions') as FormArray;
  }

  handlePageChange(currentSelectedPage: number) {
    this.questionEditorService.updateSubmitAnsShow(false);
    this.allowSubmit = false;
    if (currentSelectedPage <= this.questions.length) {
      this.playerUsageService.logResourceUsage(this.resource);
      this.hasSubmitted = false;
      this.currentPage = currentSelectedPage;
      this.questionOnDisplay = this.questions[currentSelectedPage - 1];
      this._addQuestionsControl();
      console.log('questionOnDisplay', this.questionOnDisplay);
    }
  }

  nextPage() {
    this.questionEditorService.updateSubmitAnsShow(false);
    this.playerUsageService.logResourceUsage(this.resource);
    this.currentPage++;
    this.handlePageChange(this.currentPage);
  }

  onSubmit() {
    this.playerUsageService.logResourceUsage(this.resource);
    this.hasSubmitted = true;
    this.quizForm.value.questions[0].submitted = true;
    if (this.questionOnDisplay.type === QuestionTypeEnum.SINGLE_CHOICE) {
      const selectedAnswer = this.questionsControl.value[0]
        .selected as QuestionOption;
      this.quizQuestionStatuses[this.currentPage - 1] = selectedAnswer.isCorrect
        ? QuestionStatusEnum.CORRECT
        : QuestionStatusEnum.INCORRECT;
    } else if (
      this.questionOnDisplay.type === QuestionTypeEnum.MULTIPLE_CHOICE
    ) {
      const options = this.questionsControl.value[0].options;
      let correct = true;
      options.forEach(option => {
        if (option.isCorrect && !option.checked) {
          correct = false;
        } else if (!option.isCorrect && option.checked) {
          correct = false;
        }
      });
      this.quizQuestionStatuses[this.currentPage - 1] = correct
        ? QuestionStatusEnum.CORRECT
        : QuestionStatusEnum.INCORRECT;
    }
  }

  correctAnsCheck(evt, i) {
    //console.log('correctAnswer in quiz ', evt, i);
    this.correctAnswerCheck[this.currentPage - 1] = evt;
  }

  onQuestionSubmit() {
    this.questionEditorService.updateSubmitAnsShow(true);
    this.hasSubmitted = true;
    this.quizQuestionStatuses[this.currentPage - 1] = this.correctAnswerCheck[
      this.currentPage - 1
    ];
  }

  get resourceType() {
    return ResourceType;
  }

  allowSubmitButton(evt) {
    //console.log('allowSubmitButton ', evt);
    this.allowSubmit = evt;
  }

  // ngOnDestroy() {
  //   this.curriculumPlaylistServic.unsubscribe();
  // }
}
