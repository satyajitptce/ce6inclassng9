import * as uuid from 'uuid';
import { Component, OnInit } from '@angular/core';

import { BaseQuestionComponent } from '../base-question/base-question.component';
import { MultipleChoiceQuestion } from '../../models/quiz.interface';

@Component({
  selector: 'tce-multiple-choice-question',
  templateUrl: './multiple-choice-question.component.html',
  styleUrls: [
    './multiple-choice-question.component.scss',
    '../quiz-player/quiz-player.component.scss'
  ]
})
export class MultipleChoiceQuestionComponent extends BaseQuestionComponent
  implements OnInit {
  question: MultipleChoiceQuestion;

  randomFieldName = uuid.v4();

  constructor() {
    super();
  }

  ngOnInit() {}
}
