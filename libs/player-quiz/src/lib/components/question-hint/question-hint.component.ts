import { Component, OnInit, Input } from '@angular/core';

import { IQuestion } from '../../models/quiz-api.interface';

@Component({
  selector: 'tce-question-hint',
  templateUrl: './question-hint.component.html',
  styleUrls: ['./question-hint.component.scss']
})
export class QuestionHintComponent implements OnInit {
  @Input() question: IQuestion;

  constructor() {}

  ngOnInit() {}

  /**
   * Trigger for displaying hint
   *
   * @memberof QuestionHintComponent
   */
  onShowHint() {}
}
