import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { BaseQuestion } from '../../models/quiz.interface';

@Component({
  selector: 'tce-base-question',
  templateUrl: './base-question.component.html',
  styleUrls: ['./base-question.component.scss']
})
export class BaseQuestionComponent {
  @Input() currentQuestionNumber: number;
  @Input() question: BaseQuestion;
  @Input() formGroupReference: FormGroup;
  @Input() totalQuestionCount: number;
  @Input() layout: string;

  constructor() {}

  /**
   * For testing the form control data
   *
   * @param {*} g
   * @memberof BaseQuestionComponent
   */
}
