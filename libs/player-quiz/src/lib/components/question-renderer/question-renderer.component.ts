import {
  Component,
  ComponentFactoryResolver,
  Input,
  OnInit,
  ViewChild,
  ViewContainerRef,
  ComponentRef,
  OnDestroy
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { QuestionTypeEnum } from '../../enums/question-type.enum';
import { BaseQuestionComponent } from '../base-question/base-question.component';
import { SingleChoiceQuestionComponent } from '../single-choice-question/single-choice-question.component';
import { MultipleChoiceQuestionComponent } from '../multiple-choice-question/multiple-choice-question.component';
import { BaseQuestion } from '../../models/quiz.interface';

@Component({
  selector: 'tce-question-renderer',
  templateUrl: './question-renderer.component.html',
  styleUrls: ['./question-renderer.component.scss']
})
export class QuestionRendererComponent implements OnInit, OnDestroy {
  @Input() currentQuestionNumber: number;
  @Input() totalQuestionCount: number;
  @Input() questionFormGroup: FormGroup;
  @Input() question: BaseQuestion;

  // Container on which the component will get mounted
  @ViewChild('question_container', { read: ViewContainerRef, static: true })
  private questionContainer: ViewContainerRef;

  private componentRef: ComponentRef<{}>;

  // Component mapper
  readonly templateMapper = {
    [QuestionTypeEnum.SINGLE_CHOICE]: SingleChoiceQuestionComponent,
    [QuestionTypeEnum.MULTIPLE_CHOICE]: MultipleChoiceQuestionComponent
  };

  constructor(private factoryResolver: ComponentFactoryResolver) {}

  ngOnInit() {
    this._loadComponent();
  }

  ngOnDestroy() {
    if (this.componentRef) {
      this.componentRef.destroy();
      this.componentRef = null;
    }
  }

  private _loadComponent() {
    const componentFactory = this.factoryResolver.resolveComponentFactory(
      this.templateMapper[this.questionFormGroup.value.type]
    );

    const viewContainerRef = this.questionContainer;
    // Clear any item on the screen
    viewContainerRef.clear();

    // Dynamically create the component
    this.componentRef = viewContainerRef.createComponent(componentFactory);

    // Initialize the base component
    (this.componentRef
      .instance as BaseQuestionComponent).currentQuestionNumber = this.currentQuestionNumber;
    (this.componentRef
      .instance as BaseQuestionComponent).question = this.question;
    (this.componentRef
      .instance as BaseQuestionComponent).totalQuestionCount = this.totalQuestionCount;
    (this.componentRef
      .instance as BaseQuestionComponent).formGroupReference = this.questionFormGroup;
    (this.componentRef.instance as BaseQuestionComponent).layout = this.question
      ? this.question.layout
      : '1';
  }
}
