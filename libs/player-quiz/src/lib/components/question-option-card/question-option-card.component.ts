import {
  Component,
  OnInit,
  OnChanges,
  Input,
  HostBinding
} from '@angular/core';
import {
  FormGroup,
  ControlContainer,
  FormGroupDirective,
  FormControl
} from '@angular/forms';
import { QuestionOption } from '../../models/quiz.interface';
import { AppConfigService } from '@tce/core';

const enum layoutTypes {
  TEXT_OPTION = 'textOption',
  IMAGE_OPTION = 'imageOption'
}

@Component({
  selector: 'tce-question-option-card',
  templateUrl: './question-option-card.component.html',
  styleUrls: [
    './question-option-card.component.scss',
    '../quiz-player/quiz-player.component.scss'
  ],
  viewProviders: [
    { provide: ControlContainer, useExisting: FormGroupDirective }
  ]
})
export class QuestionOptionCardComponent implements OnInit, OnChanges {
  @Input() option: QuestionOption;
  @Input() isSubmitted: boolean;
  @Input() isSelected: boolean;
  @Input() isCorrect: boolean;
  @Input() randomFieldName: string;
  @Input() index: number;
  @Input() inputType: string;
  @Input() pureOption: any;
  @Input() parentForm: FormGroup;
  @Input() typeFormControl: FormControl;
  @Input() layout: string;

  @HostBinding('class')
  get setClass() {
    const additionalClass =
      this.layout !== '1' &&
      this.option.optionImage &&
      this.option.optionImage.length
        ? '-image-layout'
        : '-text-layout';
    return 'option-container ' + additionalClass;
  }

  optionLayoutType = layoutTypes.TEXT_OPTION;
  isSubmittedOption = false;
  correctnessClass = '';

  constructor(private appConfigService: AppConfigService) {}

  ngOnInit() {
    if (this.layout !== '1') {
      if (this.option && this.option.optionImage) {
        this.optionLayoutType = layoutTypes.IMAGE_OPTION;
      } else {
        this.optionLayoutType = layoutTypes.TEXT_OPTION;
      }
    } else {
      this.optionLayoutType = layoutTypes.TEXT_OPTION;
    }
  }

  ngOnChanges(changes) {
    const { isSubmitted } = changes;
    if (isSubmitted && isSubmitted.currentValue !== isSubmitted.previousValue) {
      if (isSubmitted.currentValue) {
        if (!this.isCorrect && this.isSelected) {
          this.correctnessClass = 'incorrect-answer';
        } else if (this.isCorrect) {
          this.correctnessClass = 'correct-answer';
        }
      } else {
        this.correctnessClass = '';
      }
    }
  }

  imageLayout() {
    return this.optionLayoutType === layoutTypes.IMAGE_OPTION;
  }
  textLayout() {
    return this.optionLayoutType === layoutTypes.TEXT_OPTION;
  }

  handleImageError(evnt) {
    (<HTMLImageElement>(
      evnt.target
    )).src = this.appConfigService.getGeneralConfig('imageErrorReplacement');
  }
}
