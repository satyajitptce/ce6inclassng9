import { Component, OnInit, Input } from '@angular/core';
import { SafeHtml } from '@angular/platform-browser';

import { BaseQuestion } from '../../models/quiz.interface';

@Component({
  selector: 'tce-question-header',
  templateUrl: './question-header.component.html',
  styleUrls: ['./question-header.component.scss']
})
export class QuestionHeaderComponent implements OnInit {
  @Input() currentQuestionNumber: number;
  @Input() question: BaseQuestion;
  @Input() totalQuestionCount: number;
  @Input() layout: string;

  questionHtml: SafeHtml;

  constructor() {}

  ngOnInit() {}
}
