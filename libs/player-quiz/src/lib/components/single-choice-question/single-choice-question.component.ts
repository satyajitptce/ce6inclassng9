import * as uuid from 'uuid';
import { Component, OnInit } from '@angular/core';

import { BaseQuestionComponent } from '../base-question/base-question.component';
import { SingleChoiceQuestion } from '../../models/quiz.interface';

@Component({
  selector: 'tce-single-choice-question',
  templateUrl: './single-choice-question.component.html',
  styleUrls: [
    './single-choice-question.component.scss',
    '../quiz-player/quiz-player.component.scss'
  ]
})
export class SingleChoiceQuestionComponent extends BaseQuestionComponent
  implements OnInit {
  question: SingleChoiceQuestion;
  randomFieldName = uuid.v4();

  constructor() {
    super();
  }

  ngOnInit() {}
}
