import { async, TestBed } from '@angular/core/testing';
import { PlayerQuizModule } from './player-quiz.module';

describe('QuizPlayerV2Module', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerQuizModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerQuizModule).toBeDefined();
  });
});
