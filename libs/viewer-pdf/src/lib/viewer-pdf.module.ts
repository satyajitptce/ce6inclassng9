import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ViewPdfComponent } from './components/view-pdf/view-pdf.component';
import { PdfScrollDirective } from './directives/pdf-scroll.directive';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';

@NgModule({
  imports: [CommonModule, PdfViewerModule, PlayerAnnotationModule],
  declarations: [ViewPdfComponent, PdfScrollDirective],
  exports: [ViewPdfComponent]
})
export class ViewerPdfModule {}
