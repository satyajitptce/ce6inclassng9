import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnsupportPlayerComponent } from './component/unsupport-player/unsupport-player.component';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';

@NgModule({
  declarations: [UnsupportPlayerComponent],
  imports: [
    CommonModule,
    LibConfigModule.forChild(UnsupportPlayerComponent),
    CoreModule,
    PlayerAnnotationModule
  ],
  exports: [UnsupportPlayerComponent],
  entryComponents: [UnsupportPlayerComponent]
})
export class PlayerUnsupportModule {}
