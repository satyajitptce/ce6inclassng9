import { Component, OnInit } from '@angular/core';
import {
  PlayerAbstractComponent,
  PlayerAbstractInterface,
  PlayerAbstractAnimations,
  ResourceType,
  RequestApiService,
  PlayerUsageService
} from '@tce/core';
import { saveAs as importedSaveAs } from 'file-saver';
@Component({
  selector: 'unsupport-player',
  templateUrl: './unsupport-player.component.html',
  styleUrls: ['./unsupport-player.component.scss'],
  animations: PlayerAbstractAnimations
})
export class UnsupportPlayerComponent extends PlayerAbstractComponent
  implements OnInit, PlayerAbstractInterface {
  unsupportData: any;
  constructor(
    private requestApiService: RequestApiService,
    private playerUsageService: PlayerUsageService
  ) {
    super();
  }

  ngOnInit() {
    this.onEndLoadingResource();
  }
  get resourceType() {
    return ResourceType;
  }
  onEndLoadingResource(): void {
    this.isLoading = false;
  }

  beginLoadingResource() {
    this.unsupportData = this.resource.metaData;
  }

  download(data) {
    this.playerUsageService.logResourceUsage(this.resource);
    //console.log("TCL: download -> data", data)
    const url = this.requestApiService.getUrl('getFile');
    const fileName = data.fileName;
    const encryptedFilePath = data.encryptedFilePath;
    const assetId = data.assetId;
    // console.log("TCL: download -> data",  'http://172.18.3.52:4200/'+data)
    const blob = url + '/' + encryptedFilePath + '/' + assetId + '/' + fileName;
    // const blob = new Blob([data], { type: 'image/jpeg' });
    // const url= window.URL.createObjectURL(blob);
    // window.open(url);
    importedSaveAs(blob, fileName);
    // this.sanitizer.bypassSecurityTrustResourceUrl(window.URL.createObjectURL(blob));
  }
}
