import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnsupportPlayerComponent } from './unsupport-player.component';

describe('UnsupportPlayerComponent', () => {
  let component: UnsupportPlayerComponent;
  let fixture: ComponentFixture<UnsupportPlayerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UnsupportPlayerComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnsupportPlayerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
