import {
  transition,
  trigger,
  style,
  animate,
  keyframes,
  query,
  stagger,
  animation
} from '@angular/animations';

export const TCEAnimations = {
  resourceDisplay: trigger('resourceDisplay', [
    transition('* => *', [
      query(':enter', style({ opacity: 0 }), { optional: true }),
      query(
        '.resource-card:enter',
        stagger('150ms', [
          animate(
            '1s ease-in',
            keyframes([
              style({
                opacity: 0,
                transform: 'scale(0.8) translate3d(-60%, 0, 0)',
                offset: 0
              }),
              style({
                opacity: 0.5,
                transform: 'scale(0.9) translate3d(30px,0,0)',
                offset: 0.5
              }),
              style({
                opacity: 1,
                transform: 'scale(1) translate3d(0,0,0)',
                offset: 1.0
              })
            ])
          )
        ]),
        { optional: true }
      )
    ])
  ]),
  jackInBox: animation(
    [
      animate(
        '{{ timing }}s {{ delay }}s',

        keyframes([
          style({
            opacity: 0,
            transform: 'scale(0.1) rotate(30deg)',
            'transform-origin': 'center bottom',
            offset: 0
          }),
          style({
            opacity: 0.5,
            transform: 'rotate(-10deg)',
            offset: 0.5
          }),
          style({
            opacity: 0.7,
            transform: 'rotate(3deg)',
            offset: 0.7
          }),
          style({
            opacity: 1,
            transform: 'scale(1)',
            offset: 1
          })
        ])
      )
    ],
    {
      params: { timing: 1, delay: 0 }
    }
  ),

  bounceIn: animation(
    animate(
      '{{ timing }}s {{ delay }}s cubic-bezier(0.215, 0.610, 0.355, 1.000)',
      keyframes([
        style({ opacity: 0, transform: 'scale3d(.3, .3, .3)', offset: 0 }),
        style({ transform: 'scale3d(1.1, 1.1, 1.1)', offset: 0.2 }),
        style({ transform: 'scale3d(.9, .9, .9)', offset: 0.4 }),
        style({
          opacity: 1,
          transform: 'scale3d(1.03, 1.03, 1.03)',
          offset: 0.6
        }),
        style({ transform: 'scale3d(.97, .97, .97)', offset: 0.8 }),
        style({ opacity: 1, transform: 'scale3d(1, 1, 1)', offset: 1 })
      ])
    ),
    { params: { timing: 0.75, delay: 0 } }
  ),

  bounceOut: animation(
    animate(
      '{{ timing }}s {{ delay }}s',
      keyframes([
        style({ transform: 'scale3d(.9, .9, .9)', offset: 0.2 }),
        style({
          opacity: 1,
          transform: 'scale3d({{ scale }}, {{ scale }}, {{ scale }})',
          offset: 0.5
        }),
        style({
          opacity: 1,
          transform: 'scale3d({{ scale }}, {{ scale }}, {{ scale }})',
          offset: 0.55
        }),
        style({ opacity: 0, transform: 'scale3d(.3, .3, .3)', offset: 1 })
      ])
    ),
    { params: { timing: 0.75, delay: 0, scale: 1.1 } }
  ),

  flipOut: animation(
    [
      style({ 'backface-visibility': 'visible' }),
      animate(
        '{{ timing }}s {{ delay }}s',
        keyframes([
          style({
            transform: 'perspective(400px)',
            offset: 0
          }),
          style({
            opacity: 1,
            transform:
              'perspective(400px) rotate3d({{ rotateX }}, {{ rotateY }}, 0, -20deg)',
            offset: 0.3
          }),
          style({
            opacity: 0,
            transform:
              'perspective(400px) rotate3d({{ rotateX }}, {{ rotateY }}, 0, 90deg)',
            offset: 1
          })
        ])
      )
    ],
    { params: { timing: 1, delay: 0, rotateX: 1, rotateY: 0 } }
  ),

  rotateIn: animation(
    animate(
      '{{ timing }}s {{ delay }}s',
      keyframes([
        style({
          'transform-origin': '{{ origin }}',
          opacity: 0,
          transform: 'rotate3d(0, 0, 1, {{ degrees }})',
          offset: 0
        }),
        style({
          'transform-origin': '{{ origin }}',
          opacity: 1,
          transform: 'none',
          offset: 1
        })
      ])
    ),
    {
      params: { timing: 1, delay: 0, origin: 'center', degrees: '-900deg' }
    }
  )
};
