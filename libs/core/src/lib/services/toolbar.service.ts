import { Injectable } from '@angular/core';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { ToolType } from '../models/toolbar/ToolType';
import { ThemeService } from './theme.service';
import { AppConfigService } from './app-config.service';
@Injectable({
  providedIn: 'root'
})
export class ToolbarService {
  public userProfileData: any;
  public userProfileImg: string;
  public selectedSubjectObject = new ReplaySubject();
  public selectedSubjectObject$ = this.selectedSubjectObject.asObservable();
  public editMode = new ReplaySubject();
  _editMode = false;
  public editMode$ = this.editMode.asObservable();
  public toolbarUserFlag = new Subject();
  public toolbarUserFlag$ = this.toolbarUserFlag.asObservable();
  public toolbarSubmenuTop = new ReplaySubject();
  public toolbarSubmenuTop$ = this.toolbarSubmenuTop.asObservable();
  public toolbarScroll = new ReplaySubject();
  public toolbarScroll$ = this.toolbarScroll.asObservable();
  public currentPenSize = new ReplaySubject();
  public currentSelectedTool = new ReplaySubject<ToolType>(1);
  public currentSelectedTool$ = this.currentSelectedTool.asObservable();
  public previousSelectedTool = '';
  //private _currentToolColor = '#FFFFFF';
  public globalPanZoom;
  private _currentToolColor: any;
  private selectedToolColor = new ReplaySubject<string>(1);
  public selectedToolColor$ = this.selectedToolColor.asObservable();

  private _currentToolbarSide = 'right';
  private toolbarside = new Subject<any>();

  private _currentToolbarAvailableHeightReduction = 0;
  private toolbarAvailableHeightReduction = new ReplaySubject<number>();
  public toolbarAvailableHeightReduction$ = this.toolbarAvailableHeightReduction.asObservable();

  private selectedSubjectId = new ReplaySubject<string>();
  public selectedSubjectId$ = this.selectedSubjectId.asObservable();

  private penColors = new ReplaySubject<object>(1);
  public penColors$ = this.penColors.asObservable();

  public toolBarActionDone = new Subject();
  public toolBarActionDone$ = this.toolBarActionDone.asObservable();
  //private _currentDefaultToolColor = '#FFFFFF';
  private _currentDefaultToolColor: any;
  public defaultToolColor = new ReplaySubject<string>(1);
  public defaultToolColor$ = this.defaultToolColor.asObservable();

  private clearAnnotationsListener = new ReplaySubject<boolean>(1);
  public clearAnnotationsListener$ = this.clearAnnotationsListener.asObservable();

  private focusMode = new BehaviorSubject<boolean>(false);
  public focusMode$ = this.focusMode.asObservable();

  private clearAllAnnotationsBroadcaster = new Subject();
  public clearAllAnnotationsBroadcaster$ = this.clearAllAnnotationsBroadcaster.asObservable();

  private clearWhiteboardBroadcaster = new Subject();
  public clearWhiteboardBroadcaster$ = this.clearWhiteboardBroadcaster.asObservable();
  private clearWhiteboardSetting = new Subject();
  public clearWhiteboardSetting$ = this.clearWhiteboardSetting.asObservable();
  constructor(
    private themeService: ThemeService,
    private appConfigService: AppConfigService
  ) {
    this.themeService.currentTheme$.subscribe(theme => {
      this.setPenColors(theme['toolbar']['pen']);
    });
    this._currentDefaultToolColor = this.appConfigService.getConfig(
      'global_setting'
    )['defaultcolor'];
    this._currentToolColor = this.appConfigService.getConfig('global_setting')[
      'defaultcolor'
    ];
  }

  selectTool(toolType: ToolType) {
    this.currentSelectedTool.next(toolType);
    this.previousSelectedTool = '';
  }

  setToolColor(color: string) {
    this._currentToolColor = color;
    this.selectedToolColor.next(this._currentToolColor);
    document.documentElement.style.setProperty(
      '--selected-tool-color',
      this._currentToolColor
    );
  }

  settoolbarside(isOnLeft: boolean) {
    this._currentToolbarSide = isOnLeft ? 'left' : 'right';
    this.toolbarside.next(isOnLeft);
    this.setToolbarAvailableHeightReduction(
      this._currentToolbarAvailableHeightReduction
    );
  }
  gettoolbarside() {
    return this.toolbarside.asObservable();
  }

  setToolbarAvailableHeightReduction(amount: number) {
    this._currentToolbarAvailableHeightReduction = amount;
    let amountWithModifiers = amount;

    //to account for the subject selector
    if (this._currentToolbarSide === 'left') {
      amountWithModifiers += 53;
    }

    this.toolbarAvailableHeightReduction.next(amountWithModifiers);
  }

  setSelectedSubjectId(id: string) {
    this.selectedSubjectId.next(id);
  }

  setPenColors(toolColors: Array<object>) {
    this.penColors.next(toolColors);
    //const newDefaultToolColor = toolColors[0]['value'];
    let newDefaultToolColor = this._currentToolColor;

    if (newDefaultToolColor === '#FFFFFF') {
      newDefaultToolColor = '#000000';
    }
    if (this._currentToolColor === '#000000') {
      newDefaultToolColor = '#FFFFFF';
    }

    /*
    Determine if the currently selected color is present in the new color options.
    */
    const currentSelectionIndex = toolColors.findIndex(colorItem => {
      return colorItem['value'] === this._currentToolColor;
    });

    if (
      this._currentToolColor === this._currentDefaultToolColor ||
      currentSelectionIndex === -1
    ) {
      this.setToolColor(newDefaultToolColor);
    }
    this._currentDefaultToolColor = newDefaultToolColor;
    this.defaultToolColor.next(this._currentDefaultToolColor);
  }

  clearWhiteboardAnnotations() {
    this.clearAnnotationsListener.next(false);
  }

  onToolBarActionDone(value: any){
    this.toolBarActionDone.next(value);
  }

  loadNewWhiteBoardAnnotations() {
    this.clearAnnotationsListener.next(true);
  }

  setPenSize(size: any) {
    this.currentPenSize.next(size);
  }

  setFocusMode(value: boolean) {
    this.focusMode.next(value);
  }

  broadcastClearAllAnnotations(value: any) {
    this.clearAllAnnotationsBroadcaster.next(value);
  }

  broadcastClearWhiteboard(value: any) {
    this.clearWhiteboardBroadcaster.next(value);
  }
  settingClearWhiteboard(value: any) {
    this.clearWhiteboardSetting.next(value);
  }
  setprevTool(value: any) {
    this.previousSelectedTool = value;
  }
  setEditModeResoursePlayerList(value) {
    this._editMode = value;
    this.editMode.next(this._editMode);
  }
}
