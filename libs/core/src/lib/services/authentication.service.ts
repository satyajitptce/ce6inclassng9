/**
 * @author Prasanna
 * @email [prasannap@tataclassedge.com]
 * @create date 2020-01-08 15:44:12
 * @modify date 2020-01-08 17:06:57
 * @desc [description]
 * @TODO
 * [ Timers for sessions in a debug panel/stay signedin]
 */

import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import dayjs from 'dayjs';
import {
  AutoGeneratePin,
  ChangePassword,
  CheckPin,
  ClientID,
  ILogout,
  LoginData,
  organizationList,
  RefreshToken
} from '@app-teacher/models';
import {
  SignInModuleState,
  SignInState
} from '@app-teacher/models/enums/signInState.enum';
import 'rxjs';
import { BehaviorSubject, from, fromEvent, ReplaySubject } from 'rxjs';
import { mergeMap, take } from 'rxjs/operators';
import { SessionTimeoutService } from '../../../../../apps/tce-teacher/src/app/services/session.timeout.service';
import { CMode } from '../enums/cmode.enum';
import {
  AuthStateEnum,
  FirstTimeUserStatusEnum,
  UserState,
  UserStatusEnum
} from '../models/auth/auth.interface';
import { AppConfigService } from './app-config.service';
import { RequestApiService } from './request-api.service';
import { ToolbarService } from './toolbar.service';
import { CommonService } from './common.service';
@Injectable()
export class AuthenticationService {
  private _loginInProgress = false;
  private loginInProgress = new ReplaySubject<boolean>(1);
  public loginProgress$ = this.loginInProgress.asObservable();

  private _tokenData: RefreshToken;
  private tokenData = new ReplaySubject<RefreshToken>(1);
  public $tokenData = this.tokenData.asObservable();

  private authenticatedStateSubject = new BehaviorSubject<AuthStateEnum>(
    AuthStateEnum.PRE_AUTH
  );
  public authenticatedState$ = this.authenticatedStateSubject.asObservable();

  private _userState: UserState = {
    authStatus: AuthStateEnum.PRE_AUTH,
    previousAuthStatus: null,
    firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_DETERMINED
  };
  private userState = new BehaviorSubject<UserState>(this._userState);
  public userState$ = this.userState.asObservable();

  private sessionInterval: NodeJS.Timeout;

  private organizationName = "";
  /** TODO: make organization dynamic*/
  // private organization = 'classedge-school';
  // private organization = '';
  private grant_type = 'password';
  httpPostOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded'
    })
  };

  private userStatus: number;
  
  private _defaultSignInState = SignInState.LOGIN_PASSWORD;
  private _currentSignInState = this._defaultSignInState;
  private currentSignInState = new ReplaySubject<SignInState>(1);
  public currentSignInState$ = this.currentSignInState.asObservable();

  private signInModuleState = new ReplaySubject<SignInModuleState>(1);
  public signInModuleState$ = this.signInModuleState.asObservable();

  private logoutTriggered = new ReplaySubject<SignInModuleState>(1);
  public logoutTriggered$ = this.logoutTriggered.asObservable();

  private _authFetching = false;
  private authFetching = new ReplaySubject<boolean>(1);
  public authFetching$ = this.authFetching.asObservable();

  private authError = new ReplaySubject<string>(1);
  public authError$ = this.authError.asObservable();

  private autoSignOutTimeout = null;
  private _clientIdRetrieved = new ReplaySubject<any>(1);

  private clientIdStorageName = 'clientIdExpirationDate';
  private sessionTimeoutName = 'sessionTimeoutKey';
  private timingDetails = {
    //trackingTime: +this.getUtcDate(),
    //RefreshTime: +this.getUtcDate()
    trackingTime: +dayjs(),
    RefreshTime: +dayjs()
  };
  sessionIntervalDurDate: Date;

  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private appConfigService: AppConfigService,
    private sessionTimeoutService: SessionTimeoutService,
    private toolbarService: ToolbarService,
    private commonService: CommonService
  ) {
    //const timeUntilClientIdExpiration =
    //this.getTimeUntilClientIdExpiration() -
    //Shave off 5 minutes, since it would be frustrating to only have 5 minutes left before being booted off
    //1000 * 60 * 5;
    const timeUntilClientIdExpiration = this.getTimeUntilClientIdExpiration();
    // console.log('TCE: AuthenticationService -> cookieService', cookieService);
    //console.log( 'TCE: AuthenticationService -> sessionStorage.getItem()', sessionStorage.getItem('token') );

    // intially remove everything
    sessionStorage.removeItem(this.clientIdStorageName);
    sessionStorage.removeItem(this.sessionTimeoutName);
    this.setUserState({ authStatus: AuthStateEnum.NO_AUTH });

    //page refresh refreshToken
    // let currentUser = sessionStorage.getItem('token');
    // console.log('currentuser token', currentUser);
    // if (currentUser) {
    //   this.setUserState({ authStatus: AuthStateEnum.AUTH });
      //added for debugging 
      // this.sessionTimeoutService.setIdleScreen(false, 'staySigned333');
      // this.extendSession("fromUI" + ' > staySigned222')
      // this.sessionTimeoutService.makeSessionTimeout('!idleIsSet');
    // } else {
    //   // original state
    //   sessionStorage.removeItem(this.clientIdStorageName);
    //   sessionStorage.removeItem(this.sessionTimeoutName);
    //   this.setUserState({ authStatus: AuthStateEnum.NO_AUTH });
    // }

    /**
     * tce: Need to check the functionality
     */
    // this.sessionTimeoutService.onTimeout$.subscribe(ontimeOut => {
    //   console.log("ontimeout",ontimeOut)
    //this.logoutUser(false, "sessionTimeoutService onTimeout");
    //});

    setTimeout(() => {
      this._defaultSignInState =
        this.appConfigService.getCmode() === CMode.ENABLED
          ? SignInState.LOGIN_PIN
          : SignInState.LOGIN_PASSWORD;
      this.setCurrentSignInState(this._defaultSignInState);
    }, 0);

    /**
     * Call for list of organizations
     */
    this.getOrganizations('init-contructor');
  }

  getTimeUntilClientIdExpiration(): number {
    const clientIdExpiration = sessionStorage.getItem(this.clientIdStorageName);

    if (!clientIdExpiration) {
      return -1;
    }
    /** change tce */
    //const now = this.getUtcDate();
    const now = dayjs();
    const clientIdExpDate = dayjs(clientIdExpiration);
    //return parseInt(clientIdExpiration, 10) - now.getTime();
    return clientIdExpDate.diff(now, 'millisecond');
  }

  setSessionTimeout(timeoutMillis: number, callee: string) {
    // clearTimeout(this.autoSignOutTimeout);
    /**
     * tce TODO: 3000 to be global var
     */
    this.autoSignOutTimeout = setTimeout(() => {
      /* if (this._userState.authStatus !== AuthStateEnum.AUTH) {
        this.logoutUser(true, callee + " > autoSignOutTimeout");
      } */
      this.extendSession('sessionTimeout');
    }, timeoutMillis + 1000);
  }

  setTokenData(tokenData: RefreshToken, callee: string) {
    //console.log('TCE: AuthenticationService -> setTokenData -> callee', callee);
    this._tokenData = tokenData;
    this.tokenData.next(tokenData);
    //console.log('tokenData-->> ', tokenData);
    if (tokenData) {
      sessionStorage.setItem('token', tokenData.access_token);
      sessionStorage.setItem('refreshToken', tokenData.refresh_token);
      const myDate = new Date(
        new Date().getTime() + tokenData.expires_in * 1000
      );
      document.cookie =
        'access_token=' +
        tokenData.access_token +
        ';expires=' +
        myDate.toUTCString() +
        ';path=/tce-repo-api/1/web/1/content/fileservice';
      this.startTrackingUser();
      this.setRefreshTokenInterval('setTokenData');
      if (this._tokenData.loginstatus !== undefined) {
        if(this._tokenData.loginstatus === UserStatusEnum.CHANGE_PASSWORD){
          this.setUserStatus(this._tokenData.loginstatus);
          this.setUserState({
            authStatus: AuthStateEnum.PRE_AUTH,
            firstTimeUserStatus: FirstTimeUserStatusEnum.FIRST_TIME
          });
        }
        else {
        this.setUserStatus(this._tokenData.loginstatus);
        //console.log(this._tokenData.loginstatus)
        this.setUserState({
          authStatus: AuthStateEnum.AUTH,
          firstTimeUserStatus:
            this._tokenData.loginstatus !== UserStatusEnum.ACTIVE_USER
              ? FirstTimeUserStatusEnum.FIRST_TIME
              : FirstTimeUserStatusEnum.NOT_FIRST_TIME
        });
      }
      }
     

      /**
       * tce: Need to check the functionality
       */
      /* if (!this.sessionTimeoutService.idleIsSet) {
        this.sessionTimeoutService.makeSessionTimeout("!idleIsSet");
      } */
    } else {
      sessionStorage.removeItem('token');
      sessionStorage.removeItem('refreshToken');
    }
  }

  private generateClientIDVersion(callee: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.http
        .get<ClientID>(this.requestApiService.getUrl('clientId'))
        .subscribe(clientID => {
          //console.log('TCE: AuthenticationService -> clientID', clientID);

          if (clientID.sessionTimeout) {
            const sessionTimeout = parseInt(clientID.sessionTimeout, 10);
            //console.log( 'TCE: AuthenticationService -> generateClientIDVersion -> clientID',  clientID  );

            const dateOfExpiration = dayjs().add(sessionTimeout, 'second');
            sessionStorage.setItem(
              this.clientIdStorageName,
              //'' + dateOfExpiration.getTime()
              dateOfExpiration.toISOString()
            );
            sessionStorage.setItem(
              this.sessionTimeoutName,
              clientID.sessionTimeout
            );

            // console.log('TCE: AuthenticationService -> generateClientIDVersion -> sessionTimeout', sessionTimeout);
            // this.setSessionTimeout(sessionTimeout * 1000, callee + " > generateClientIDVersion");
          }
          this.requestApiService.versionId = clientID.apiVersion;
          this._clientIdRetrieved.next();

          resolve(clientID);
        });
    });
  }

  loginPWD(value: LoginData, callee: string) {
    //console.log('TCE: AuthenticationService -> loginPWD -> callee', callee);
    this.getOrganizations('login-PWD');
    let userName = '';
    let password = '';
    this.organizationName = localStorage.getItem("organization") || "";
    if (value.loginType === 'pin') {
      userName = value.userName;
      password = value.password;
    } else {
      userName = value.userName;
      password = value.password;
      if (userName !== 'dev.admin') {
        localStorage.setItem("username",userName);


        //console.log( 'TCE: AuthenticationService -> loginPWD -> this.organizationName',userName);
        userName = this.organizationName + '#' + userName;

      }
    }

    const body = `username=${userName}&password=${password}&grant_type=${this.grant_type}`;

    this.onBeforeAuthFetch();
    /**
     * Taken to top was in bootom as the sequence of token and client was wrong
     * :: clientid is to be called before token call
     */

    /**
     * Used a promise fun to wait for the response to come before login fires.
     */
    this.generateClientIDVersion('login-PWD').then(promisedData => {
      //console.log('TCE: AuthenticationService -> loginPWD -> promisedData', promisedData);

      this._clientIdRetrieved.pipe(take(1)).subscribe(() => {
        this.http
          .post<RefreshToken>(
            this.requestApiService.getUrl('signIn'),
            body,
            this.httpPostOptions
          )
          .subscribe(
            (tokenData: RefreshToken) => {
              //console.log('login tokenData-->', tokenData);
              //console.log('login value', value.loginType);
              this.setTokenData(tokenData, 'loginPWD');

              if (value.loginType === 'password') {
                //console.log('login value if', value.loginType);
                switch (tokenData.loginstatus) {
                  case UserStatusEnum.ACTIVE_USER:
                    this.setCurrentSignInState(SignInState.DESTROY);
                    //console.log('case-->', UserStatusEnum.CHANGE_PASSWORD);
                    break;
                  case UserStatusEnum.CHANGE_PASSWORD:
                    this.setCurrentSignInState(SignInState.CHANGE_PASSWORD);
                    //console.log('case-->', UserStatusEnum.CHANGE_PASSWORD);
                    break;
                  case UserStatusEnum.CHANGE_PIN:
                    this.setCurrentSignInState(SignInState.CHANGE_PIN);
                    break;
                  case UserStatusEnum.NEW_USER:
                    this.setCurrentSignInState(SignInState.CHANGE_PASSWORD);
                    //console.log('case-->new user', UserStatusEnum.NEW_USER);
                    //this.router.navigate(['/login/changepassword']);
                    break;
                  default:
                    break;
                }
              } else {
                //console.log('login else value', value.loginType);
                this.setCurrentSignInState(SignInState.DESTROY);
                //this.router.navigate(['/' + postLoginRoute]);
              }
              this.onAfterAuthFetch();
            },
            error => {
              this.onAfterAuthFetch();
              if (value.loginType === 'password') {
                this.setAuthError('Incorrect User ID or Password');
              } else if (value.loginType === 'pin') {
                //console.log('login value ele', value.loginType);
                this.setAuthError('Incorrect PIN');
              }

              //console.log('Error logging in!', error);
            }
          );
      });
    });
  }

  checkPIN(pinValue: string) {
    this.onBeforeAuthFetch();
    this.http
      .get<CheckPin>(this.requestApiService.getUrl('checkPin') + pinValue)
      .subscribe(
        data => {
          const loginDetails = {
            userName: data.orgs[0] + '#' + data.userName + '#pin',
            password: pinValue,
            loginType: 'pin'
          };
          this.loginPWD(loginDetails, 'check-PIN');
        },
        err => {
          this.onAfterAuthFetch();
          this.setAuthError('Invalid PIN');
        }
      );
  }

  setRefreshTokenInterval(callee: string) {
    //console.log(      'TCE: AuthenticationService -> setRefreshTokenInterval -> callee', callee);
    if (this.sessionInterval) {
      clearInterval(this.sessionInterval);
    }
    // from server session clientId storage
    const refreshSessionTimeout =
      1000 * parseInt(sessionStorage.getItem(this.sessionTimeoutName));
    //console.log( 'TCE: AuthenticationService -> setRefreshTokenInterval -> refreshSessionTimeout', refreshSessionTimeout);
    //from config
    // const staySigninPopupDuration: number = this.appConfigService.getConfig('global_setting')['staySigninPopup-duration']
    const staySigninPopupDuration: number =
      this.appConfigService.getConfig('global_setting')
        .staySigninPopupDuration * 1000;
    //console.log( 'TCE: AuthenticationService -> setRefreshTokenInterval -> staySigninPopupDuration',staySigninPopupDuration);
    const sessionIntervalDur = refreshSessionTimeout - staySigninPopupDuration;
    /**
     * Make it to a date format to be used in html popup
     */
    this.sessionIntervalDurDate = new Date(0, 0, 0, 0, 0, 0);
    this.sessionIntervalDurDate.setSeconds(sessionIntervalDur / 1000);
    //console.log( 'TCE: AuthenticationService -> setRefreshTokenInterval -> sessionIntervalDur',sessionIntervalDur);

    this.sessionInterval = setInterval(() => {
      // this.timingDetails.RefreshTime = +new Date();
      this.timingDetails.RefreshTime = +dayjs();
      const lastActivityTime =
        this.timingDetails.RefreshTime - this.timingDetails.trackingTime;
      //console.log('TCE: AuthenticationService -> this.sessionInterval -> lastActivityTime RefreshTime trackingTime : ',lastActivityTime,' ',this.timingDetails.RefreshTime,' ',this.timingDetails.trackingTime);

      //console.log('TCE: AuthenticationService -> this.sessionInterval -> sessionInterval',sessionIntervalDur);
      /** lhs < rhs */
      if (lastActivityTime < sessionIntervalDur) {
        //-1000 is the error gap
        /**
         * There is activity so extend the session
         */
        this.extendSession('on-activity');
      } else {
        /*
        This is to account for a backend limitation, where if a refreshtoken call is made,
        and the next most recent API request was ALSO a refreshtoken request, it will invalidate the token.
        So we throw an interstitial request to the recentViews API if the user has still been active (clicking, moving the mouse, etc)
        */
        /* 
        
        TODO: check usage
        
        this.http
          .get<any>(this.requestApiService.getUrl('recentViews'))
          .subscribe(
            data => {
              this.refreshToken(callee + " > refresh else");
            },
            err => this.logoutUser(true, callee + " > err refreshToken")
          ); */

        /**
         * Open stay signedin popup
         */
        this.sessionTimeoutService.setIdleScreen(true, 'else refreshToken');
        this.setSessionTimeout(
          staySigninPopupDuration,
          callee + ' > generateClientIDVersion'
        );
      }
    }, sessionIntervalDur);
  }

  /**
   *
   * @param callee
   * extend the session
   */
  extendSession(callee: string) {
    //   console.log("AuthenticationService -> extendSession -> callee", callee)
    this.getOrganizations('extend-Session');

    const accessToken = sessionStorage.getItem('token');
    const httpPostOptionsExtend = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: 'Bearer ' + accessToken
      })
    };

    const body = `access_token=${accessToken}`;
    this.http
      .post<any>(
        this.requestApiService.getUrl('extendSession'),
        body,
        httpPostOptionsExtend
      )
      .subscribe(
        (res: any) => {
          this.refreshToken(callee + ' > if-refresh');
        },
        (error: HttpErrorResponse) => {
          this.logoutUser(true, callee + ' > err-if-refreshToken');
        }
      );
  }

  getOrganizations(callee: string) {
    //console.log( 'TCE: AuthenticationService -> getOrganizations -> callee', callee);
    this.http
      .get<any>(this.requestApiService.getUrl('organizations'))
      .subscribe(
        data => {
          //console.log( 'TCE: AuthenticationService -> getOrganizations -> data',   data );
          /** TODO: set it after school list comes from mel */
          // this.organizationName = data.suggestions[0];
          //console.log( 'TCE: AuthenticationService -> getOrganizations -> this.organizationName', this.organizationName );
        },
        err => {}
      );
  }

  refreshToken(callee: string) {
    //console.log("AuthenticationService -> refreshToken -> callee", callee)
    const refreshToken = sessionStorage.getItem('refreshToken');
    const accessToken = sessionStorage.getItem('token');
    const body = `grant_type=refresh_token&refresh_token=${refreshToken}&access_token=${accessToken}`;

    this.getOrganizations('refresh-Token');
    // RefreshToken
    this.http
      .post<RefreshToken>(
        this.requestApiService.getUrl('refreshToken'),
        body,
        this.httpPostOptions
      )
      .subscribe(
        (tokenData: RefreshToken) => {
          this.setTokenData(tokenData, callee + ' > if-refreshToken');
          this.setCurrentSignInState(SignInState.DESTROY);
        },
        (error: HttpErrorResponse) => {
          this.logoutUser(true, callee + ' > err-refreshToken');
        }
      );
  }

  logoutUser(showSignIn: boolean, callee: string) {
     //console.log("AuthenticationService -> logoutUser -> callee", callee)
    const myDate = new Date(new Date().getTime());
    this.logoutTriggered.next();
    this.http
      .delete<ILogout>(this.requestApiService.getUrl('logout'), {})
      .subscribe(() => {});
    this.setTokenData(null, 'logoutUser');
    this.setUserState({
      authStatus: AuthStateEnum.NO_AUTH,
      firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_DETERMINED
    });
    clearInterval(this.sessionInterval);
    clearTimeout(this.autoSignOutTimeout);
    sessionStorage.removeItem(this.clientIdStorageName);
    sessionStorage.removeItem(this.sessionTimeoutName);
    this.sessionTimeoutService.stop();
    document.cookie =
      "access_token=''" +
      ';expires=' +
      myDate.toUTCString() +
      ';path=/tce-repo-api/1/web/1/content/fileservice';
    // this.toolbarService.clearWhiteboardAnnotations();
    this.commonService.setClassroomModeState(true);
    if (showSignIn) {
      this.setAuthError(
        'Your session has automatically expired. Please sign in again below.'
      );
      this.setSignInModuleState(SignInModuleState.ACTIVE);
    }
  }

  changePassword(value: string) {
    const body = {
      op: 'add',
      //oldpassword:null,
      //path: '/password',
      newpassword: value
    };

   
    
         //this.setCurrentSignInState(SignInState.);

    

    this.onBeforeAuthFetch();
    this.http
      .put<ChangePassword>(
        this.requestApiService.getUrl('changePassword'),
        body,
        this.httpPostOptions
      )
      .subscribe(
        data => {
          this.onAfterAuthFetch();
          //console.log('changepassword-->data', data);
          if (this.userStatus === UserStatusEnum.NEW_USER) {
            this.setCurrentSignInState(SignInState.CHANGE_PIN);
          } 
          else if (this.userStatus === UserStatusEnum.CHANGE_PASSWORD){
            this.setUserState({authStatus: AuthStateEnum.AUTH , firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_FIRST_TIME})
            this.setCurrentSignInState(SignInState.DESTROY);

          }
            else {
            // const postLoginRoute = this.appConfigService.getCmodeConfig()
            //   .postloginroute;
            // this.router.navigate(['/' + postLoginRoute]);
            this.setCurrentSignInState(SignInState.DESTROY);

   
          }
        },
        err => {
          this.setAuthError('Error changing password');
          this.onAfterAuthFetch();
        }
      );
  }

  changePIN(pin: string) {
    const body = {
      op: 'add',
      //path: '/pin',
      pin: pin
    };

    return this.http.put<ChangePassword>(
      this.requestApiService.getUrl('changePin'),
      body,
      this.httpPostOptions
    );
  }

  setUserStatus(value: number) {
    this.userStatus = value;
  }

  getUserStatus() {
    return this.userStatus;
  }

  autoGeneratePin() {
    /*
    Below code is to generate the PIN. This will autofill the Input fields of Set Pin Form.
    */
    return this.http.get<AutoGeneratePin>(
      this.requestApiService.getUrl('generatePin')
    );
  }

  setUserState(newState: UserState) {
    //console.log("setUserState",newState)
    this._userState = {
      ...this._userState,
      ...newState,
      ...{
        previousAuthStatus: this._userState.authStatus
      }
    };
    this.userState.next(this._userState);
  }

  setCurrentSignInState(state: SignInState) {
    //console.log("setCurrentSignInState",state)
    let nextState = state;
    if (nextState === SignInState.SESSION_DEFAULT) {
      nextState = this._defaultSignInState;
    }
    else if(nextState === SignInState.CHANGE_PASSWORD){
      this.setUserState({ authStatus: AuthStateEnum.PRE_AUTH , firstTimeUserStatus: FirstTimeUserStatusEnum.FIRST_TIME});
    
    }
    this._currentSignInState = nextState;
    this.currentSignInState.next(this._currentSignInState);
  }

  setSignInModuleState(state: SignInModuleState) {
    return this.signInModuleState.next(state);
  }

  onAfterAuthFetch() {
    this.setAuthFetching(false);
  }

  onBeforeAuthFetch() {
    this.setAuthFetching(true);
    this.setAuthError('');
  }

  setAuthError(errorMessage: string) {
    this.authError.next(errorMessage);
  }

  setAuthFetching(isFetching: boolean) {
    if (isFetching !== this._authFetching) {
      this._authFetching = isFetching;
      this.authFetching.next(this._authFetching);
    }
  }
  /**
   * tce TODO: check all activities is fired or not
   */
  private startTrackingUser() {
    //Below block of code is written to check the activity/state of the user.To check whether if user is dealing or speanding more time on resources.
    const events = [
      'scroll',
      'wheel',
      'touchmove',
      'touchend',
      'click',
      'mousemove',
      'keypress'
    ];
    from(events)
      .pipe(mergeMap(event => fromEvent(document, event)))
      .subscribe(event => {
        //this.timingDetails.trackingTime = +this.getUtcDate();
        this.timingDetails.trackingTime = +dayjs();
        // console.log('TCE: AuthenticationService -> startTrackingUser -> this.timingDetails.trackingTime', this.timingDetails.trackingTime);
        // console.log('TCE: AuthenticationService -> startTrackingUser -> dayjs()', dayjs());
      });
  }
  /*
  getUtcDate() {
    const date = new Date();
    const now_utc = Date.UTC(
      date.getUTCFullYear(),
      date.getUTCMonth(),
      date.getUTCDate(),
      date.getUTCHours(),
      date.getUTCMinutes(),
      date.getUTCSeconds()
    );

    return new Date(now_utc);
  }
  */
 getOrganizationList(query){
    return this.http.get<organizationList>(
      this.requestApiService.getUrl('organization').replace('@query@', query)
    );
  }
}
