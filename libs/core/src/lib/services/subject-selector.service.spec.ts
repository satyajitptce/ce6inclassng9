import { TestBed } from '@angular/core/testing';

import { SubjectSelectorService } from './subject-selector.service';

describe('SubjectSelectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SubjectSelectorService = TestBed.get(SubjectSelectorService);
    expect(service).toBeTruthy();
  });
});
