import { Injectable } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { KeyboardState, KeyboardTheme } from '../enums/keyboard.enum';

interface KeyboardOptions {
  inputPattern?: RegExp;
  maxLength?: number;
  state: KeyboardState;
  theme?: KeyboardTheme;
}

@Injectable({
  providedIn: 'root'
})
export class KeyboardService {
  private isMobileTablet = mobileAndTabletcheck();
  private userId = 'default';
  private userStorageKey: string = this.userId + '.keyboard';
  private virtualKeyboardState: boolean;

  private _keyboardOpenState = false;
  private keyboardDisplayState = new Subject<KeyboardOptions>();
  public keyboardDisplayState$ = this.keyboardDisplayState.asObservable();

  private keyboardUserState = new Subject<boolean>();
  public keyboardUserState$ = this.keyboardUserState.asObservable();

  private currentInputElement = new Subject<HTMLElement>();
  public currentInputElement$ = this.currentInputElement.asObservable();

  private keyboardEnterPress = new Subject<string>();
  public keyboardEnterPress$ = this.keyboardEnterPress.asObservable();

  private currentInputValue = new ReplaySubject<string>(1);
  public currentInputValue$ = this.currentInputValue.asObservable();

  constructor() {
    //localStorage.clear();
    const lsKeyboard = localStorage.getItem(this.userStorageKey);

    if (lsKeyboard === null) {
      this.virtualKeyboardState = !this.isMobileTablet;
      localStorage.setItem(
        this.userStorageKey,
        JSON.stringify(this.virtualKeyboardState)
      );
    } else {
      this.virtualKeyboardState = lsKeyboard === 'true';
    }

    this.keyboardUserState.next(this.virtualKeyboardState);
  }

  openKeyboard(elem: HTMLElement, keyboardOptions: KeyboardOptions) {
    if (!this.virtualKeyboardState) {
      return;
    }

    if (!this.isMobileTablet) {
      this._keyboardOpenState = true;
      this.currentInputElement.next(elem);
      this.keyboardDisplayState.next(keyboardOptions);
      return;
    }
    console.log('mobile device detected, no virtual keyboard needed');
  }

  closeKeyboard() {
    this._keyboardOpenState = false;
    this.keyboardDisplayState.next({ state: KeyboardState.CLOSE });
  }

  enterKeyPress(inputValue: string) {
    this.keyboardEnterPress.next(inputValue);
  }

  setCurrentInputValue(inputVal: string) {
    this.currentInputValue.next(inputVal);
  }

  toggleUserState() {
    this.virtualKeyboardState = !this.virtualKeyboardState;
    localStorage.setItem(
      this.userStorageKey,
      JSON.stringify(this.virtualKeyboardState)
    );
    if (this._keyboardOpenState) {
      this.closeKeyboard();
    }
    this.keyboardUserState.next(this.virtualKeyboardState);
  }
  planModeKeyBoardState() {
    this.virtualKeyboardState = false;
    localStorage.setItem(
      'onPlanmode',
      JSON.stringify(this.virtualKeyboardState)
    );
    if (this._keyboardOpenState) {
      this.closeKeyboard();
    }
    this.keyboardUserState.next(this.virtualKeyboardState);
  }

  teachModeKeyBoardstate(){
    this.virtualKeyboardState = JSON.parse(localStorage.getItem('default.keyboard'))
    if (this._keyboardOpenState) {
      this.closeKeyboard();
    }
    this.keyboardUserState.next(this.virtualKeyboardState);

  }
}



function mobileAndTabletcheck() {
  //Old option - https://stackoverflow.com/questions/11381673/detecting-a-mobile-browser
  //another possible option - https://github.com/matthewhudson/current-device
  //current option - https://stackoverflow.com/a/52855084/3961075
  //return window.matchMedia("(any-pointer: coarse)").matches;
  return window.matchMedia('(pointer: coarse)').matches;
}
