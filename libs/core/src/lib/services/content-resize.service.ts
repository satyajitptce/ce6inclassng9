import { Injectable, Output } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ContentResizeService {
  private resizeContentSubject = new Subject();
  public resizeContent$ = this.resizeContentSubject.asObservable();

  constructor() {}

  triggerResize() {
    this.resizeContentSubject.next(null);
  }
}
