import { TestBed } from '@angular/core/testing';

import { AppStateStorageService } from './app-state-storage.service';

describe('AppStateStorageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppStateStorageService = TestBed.get(AppStateStorageService);
    expect(service).toBeTruthy();
  });
});
