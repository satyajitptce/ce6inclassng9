import { TestBed } from '@angular/core/testing';

import { PlayerUsageService } from './player-usage.service';

describe('PlayerUsageService', () => {
  let service: PlayerUsageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PlayerUsageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
