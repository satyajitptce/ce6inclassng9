import { Injector, Inject, Injectable } from '@angular/core';
import { ErrorHandler } from '@angular/core';
import { ReplaySubject } from 'rxjs';
import { CoreError } from '../models/error/error.interface';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {
  constructor(@Inject(Injector) private injector: Injector) {}
  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }

  handleError(error: any): CoreError {
    const coreError = new CoreError(error as Error);
    console.log('IN__coreError__', coreError);
    console.log('IN__this.toastrService__', this.toastrService);

    this.toastrService.error(coreError.stack, coreError.message);

    if (this.toastrService && coreError.userMessage) {
      this.toastrService.error(
        coreError.userMessage,
        coreError.userMessageHeader
      );
    }
    console.warn('hello Error Intercepted', error);
    return coreError;
  }
}

/*
export class ErrorHandlerService implements ErrorHandler {
  constructor(private toastrService: ToastrService) {}
  
  handleError(error: any): CoreError {
    const coreError = new CoreError(error as Error);
    console.warn('Error Intercepted', error);
    if (this.toastrService && coreError.userMessage) {
      this.toastrService.error(
        coreError.userMessage,
        coreError.userMessageHeader
      );
    }
    return coreError;
  }
}
*/

/*
// working

export class ErrorHandlerService implements ErrorHandler {
  constructor(
    @Inject(Injector) private injector: Injector) { 
    
  }
  private get toastrService(): ToastrService {
    return this.injector.get(ToastrService);
  }
  
  handleError(error: any): CoreError {
    const coreError = new CoreError(error as Error);
    console.warn('hello Error Intercepted', error);
    return coreError;
  }
}
*/
