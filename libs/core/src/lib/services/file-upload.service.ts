import { Injectable } from '@angular/core';
import {
  HttpClientModule,
  HttpClient,
  HttpRequest,
  HttpResponse,
  HttpEventType,
  HttpErrorResponse
} from '@angular/common/http';

import { CurriculumPlaylistService } from '../services/curriculum-playlist.service';
import { ToolbarService } from '../services/toolbar.service';
import { RequestApiService } from './request-api.service';
import {
  Topic,
  Resource,
  FullContentSelection
} from '../models/curriculum/curriculum.interface';
//import {ContentEditorService} from '../../../../content-library/src/lib/service/content-editor.service'
import { CommonService } from './common.service';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class FileUploadService {
  percentDone: number;
  uploadSuccess: boolean;
  currentChapter: any;
  fileToUpload: any;
  selectedTopic: Topic;
  errormessage: any;
  currentResourceList: any;
  currentAssetId: any;
  currentTpoicResource: any;
  private addResourceFlag = new Subject();
  public addResourceFlag$ = this.addResourceFlag.asObservable();
  private updateCustomResourceFlag = new Subject();
  public updateCustomResourceFlag$ = this.updateCustomResourceFlag.asObservable();
  public addResourceFormData = new ReplaySubject();
  public addResourceFormData$ = this.addResourceFormData.asObservable();
  private addResourceFlagBroadcaster = new Subject();
  public addResourceFlagBroadcaster$ = this.addResourceFlagBroadcaster.asObservable();
  private ResourceFlagBroadcaster = new Subject();
  public ResourceFlagBroadcaster$ = this.ResourceFlagBroadcaster.asObservable();
  public customPlayerFlag = new ReplaySubject();
  public customPlayerFlag$ = this.customPlayerFlag.asObservable();
  myChapterTopicID: any;
  constructor(
    private requestApiService: RequestApiService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private http: HttpClient,
    private toolbarService: ToolbarService,
    private commonService: CommonService,
    private toastrService: ToastrService
  ) {
    this.curriculumPlaylistService.currAddResourceFlagBroadcaster$.subscribe(
      flag => {
        this.addResourceFlagBroadcaster.next('');
        this.addResourceFlag.next(true);
      }
    );
    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.myChapterTopicID = fullContentSelection.topic.topicId;
          this.currentResourceList = fullContentSelection.topic;
        }
      }
    );
    this.curriculumPlaylistService.topicSelection$.subscribe(currentTopic => {
      if (currentTopic) {
        this.currentTpoicResource = currentTopic;
      }
    });
  }
  createCustomResource(
    chapter,
    subject,
    file: FileList,
    shareFlag,
    title,
    currentMode
  ) {
    // console.log('file', file);
    this.fileToUpload = file;
    let formData: FormData = new FormData();
    formData.append('lpFile', this.fileToUpload);
    this.http
      .post(this.requestApiService.getUrl('addResource'), formData)
      .subscribe(fileResponse => {
        if (fileResponse) {
          const currentSessionData = {
            tpId: chapter.topic.topicId,
            gradeId: subject.gradeLevel.id,
            subjectId: subject.subject.subjectId,
            chapterId: chapter.chapter.chapterId,
            title: title,
            description: 'TCE',
            keywords: subject.subject.title,
            fileName: fileResponse['fileName'],
            folder: fileResponse['folder'],
            isShared: shareFlag
          };
          this.http
            .post(
              this.requestApiService.getUrl('dynamicCustomResource'),
              currentSessionData
            )
            .subscribe(
              customResource => {
                this.commonService.setAddResourcePanel(false);
                this.customResourceData(
                  customResource,
                  chapter,
                  'create',
                  currentMode
                );
              },
              (error: HttpErrorResponse) => {
                if (error && error.error['errorMessage']) {
                  this.errormessage = error.error['errorMessage'];
                } else {
                  this.errormessage = 'fileUploadError';
                }
                //this.toastrService.error(this.errormessage);
              }
            );
        }
      });
  }

  customCustomQuestion(){
    
  }

  

  customResourceData(customResource, chapter, type, currentMode) {
    if (customResource) {
      let newmimeType: any;
      let selectedType: any;
      let assetType: any, subType: any, tcetype: any;
      if (customResource['mimeType']) {
        newmimeType = customResource['mimeType'];
        if (newmimeType === 'application/pdf') {
          tcetype = 'worksheet';
          selectedType = 'pdf';
          assetType = 'asset_print';
          subType = tcetype;
        }
        if (newmimeType !== 'application/pdf') {
          if (newmimeType.startsWith('image')) {
            selectedType = 'image';
          } else if (newmimeType.startsWith('video')) {
            selectedType = 'video';
          } else {
            selectedType = customResource['mimeType'];
          }
          assetType = selectedType;
          subType = selectedType;
        }
      }
      const newdata1 = new Resource({
        assetId: customResource['assetId'],
        tpId: customResource['tpId'],
        title: customResource['title'],
        mimeType: selectedType,
        assetType: assetType,
        thumbFileName: customResource['thumbFileName']
          ? customResource['thumbFileName']
          : '',
        fileName: customResource['fileName'],
        subType: subType,
        encryptedFilePath: customResource['encryptedFilePath'],
        isShared: customResource['isShared'],
        visibility: 1,
        isOwner: customResource['isOwner'],
        downloadFileExtension: customResource['downloadFileExtension'],
        metaData: {
          assetId: customResource['assetId'],
          tpId: customResource['tpId'],
          lcmsSubjectId: customResource['lcmsSubjectId'],
          lcmsGradeId: customResource['lcmsGradeId'],
          title: customResource['title'],
          mimeType: selectedType,
          assetType: assetType,
          thumbFileName: customResource['thumbFileName']
            ? customResource['thumbFileName']
            : '',
          fileName: customResource['fileName'],
          ansKeyId: customResource['ansKeyId'],
          copyright: customResource['copyright'],
          subType: subType,
          description: customResource['description'],
          keywords: customResource['keywords'],
          encryptedFilePath: customResource['encryptedFilePath'],
          filePath:
            customResource['encryptedFilePath'] +
            '/' +
            customResource['assetId'] +
            '/' +
            customResource['fileName'],
          isShared: customResource['isShared'],
          isOwner: customResource['isOwner'],
          downloadFileExtension: customResource['downloadFileExtension']
        }
      });
      newdata1.visibility = 1;
      if (currentMode === 'teachMode') {
        if (chapter.topic.topicId === this.myChapterTopicID) {
          if (type == 'create') {
            this.toastrService.success('Resource Added successfully');
            chapter.topic.resources.push(newdata1);
            this.curriculumPlaylistService.setTopicSelection(chapter.topic);
          }
          if (type == 'update') {
            this.toastrService.success('Resource Updated successfully');
            if (chapter.topic.resources && chapter.topic.resources.length > 0) {
              for (
                let index = 0;
                index < chapter.topic.resources.length;
                index++
              ) {
                if (
                  chapter.topic.resources[index].resourceId ===
                  newdata1.resourceId
                ) {
                  chapter.topic.resources[index] = newdata1;
                  chapter.topic.resources[index].visibility = 1;
                  chapter.topic.resources[index].isOwner = 1;
                }
              }
              this.curriculumPlaylistService.setAvailableResources(
                chapter.topic.resources
              );
            }
          }
        } else {
          if (type == 'update' && this.currentAssetId) {
            this.customTopicResource(this.currentAssetId);
          }
        }
        if (type == 'update') {
          if (customResource['assetId']) {
            this.updateCustomResourceFlag.next(customResource['assetId']);
          }
        }
        this.addResourceFlagBroadcaster.next('');
        this.addResourceFlag.next(true);
        this.ResourceFlagBroadcaster.next(false);
        this.toolbarService.setEditModeResoursePlayerList(false);
      }
      if (currentMode === 'planMode') {
        // console.log("newdata1--> ",newdata1,"customResource-->",customResource)
        if (type === 'create') {
          this.toastrService.success('Resource Added successfully');
          if (newdata1.tpId.charAt(0) === 'c') {
            //  console.log('custom-------------->',newdata1);
            this.curriculumPlaylistService.getCustomSharedDataPlanmode(
              newdata1
            );
          } else {
            this.commonService.newCustomResource.next(newdata1);
          }
        }

        if (type == 'update') {
          this.toastrService.success('Resource Updated successfully')
          this.addResourceFlagBroadcaster.next('');
          this.commonService.setEditResource('');
          // this.commonService.newCustomResource.next(newdata1);
          //this.curriculumPlaylistService.getCustomSharedData(chapter.topic.resources);
        }
        //this.commonService.newCustomResource.next(newdata1);
      }
    }
  }

 

  updateCustomResource(
    chapter,
    subject,
    file: FileList,
    shareFlag,
    title,
    assetId,
    currentMode
  ) {
    this.currentAssetId = assetId;
    if (file) {
      console.log("file-->",file);
      
      this.fileToUpload = file;
      let formData: FormData = new FormData();
      formData.append('lpFile', this.fileToUpload);
      this.http
        .post(this.requestApiService.getUrl('addResource'), formData)
        .subscribe(fileResponse => {
          if (fileResponse) {
            const currentSessionData = {
              tpId: chapter.topic.topicId,
              gradeId: subject.gradeLevel.id,
              subjectId: subject.subject.subjectId,
              chapterId: chapter.chapter.chapterId,
              title: title,
              description: 'TCE',
              keywords: subject.subject.title,
              fileName: fileResponse['fileName'],
              folder: fileResponse['folder'],
              isShared: shareFlag,
              assetId: assetId
            };
            this.http
              .put(
                this.requestApiService.getUrl('dynamicCustomResource'),
                currentSessionData
              )
              .subscribe(
                customResource => {
                  this.customResourceData(
                    customResource,
                    chapter,
                    'update',
                    currentMode
                  );
                },
                (error: HttpErrorResponse) => {
                  if (error && error.error['errorMessage']) {
                    this.errormessage = error.error['errorMessage'];
                  } else {
                    this.errormessage = 'fileUploadError';
                  }
                  //this.toastrService.error(this.errormessage);
                }
              );
          }
        });
    } else {
      const currentSessionData = {
        tpId: chapter.topic.topicId,
        gradeId: subject.gradeLevel.id,
        subjectId: subject.subject.subjectId,
        chapterId: chapter.chapter.chapterId,
        title: title,
        description: 'TCE',
        keywords: subject.subject.title,
        isShared: shareFlag,
        assetId: assetId
      };
      this.http
        .put(
          this.requestApiService.getUrl('dynamicCustomResource'),
          currentSessionData
        )
        .subscribe(
          customResource => {
            this.customResourceData(
              customResource,
              chapter,
              'update',
              currentMode
            );
          },
          (error: HttpErrorResponse) => {
            if (error && error.error['errorMessage']) {
              this.errormessage = error.error['errorMessage'];
            } else {
              this.errormessage = 'fileUploadError';
            }
            //this.toastrService.error(this.errormessage);
          }
        );
    }
  }
  customTopicResource(id) {
    let customresource = [];
    if (
      this.currentResourceList.resources &&
      this.currentResourceList.resources.length > 0
    ) {
      for (
        let index = 0;
        index < this.currentResourceList.resources.length;
        index++
      ) {
        if (this.currentResourceList.resources[index].resourceId != id) {
          customresource.push(this.currentResourceList.resources[index]);
        }
      }
      this.currentResourceList.resources = customresource;
      // this.curriculumPlaylistService.setTopicSelection(
      //   this.currentResourceList
      // );
      this.curriculumPlaylistService.setAvailableResources(
        this.currentResourceList.resources
      );
    }
  }
  deleteResource(data, resource) {
    if (data) {
      const newURL = this.requestApiService
        .getUrl('dynamicCustomResourceDelete')
        .replace('@ids@', data.assetId);
      this.http.delete<any>(newURL, {}).subscribe(
        responce => {
          if (responce) {
            if (resource) {
              //this.updateCustomResourceFlag.next(data.assetId);
              //this.curriculumPlaylistService.setAvailableResources(resource);
              this.toastrService.success('Resource Deleted Successfully')
              this.currentTpoicResource.resources = resource;
              this.curriculumPlaylistService.setTopicSelection(
                this.currentTpoicResource
              );
              this.addResourceFlagBroadcaster.next('');
              this.addResourceFlag.next(true);
              this.commonService.openEditmodeOnDeleteResource(true);
              this.toolbarService.setEditModeResoursePlayerList(true);
            }
          }
        },
        (error: HttpErrorResponse) => {
          if (error && error.error['errorMessage']) {
            this.errormessage = error.error['errorMessage'];
          } else {
            this.errormessage = 'fileUploadError';
          }
          //this.toastrService.error(this.errormessage);
        }
      );
    }
  }
  setAddResourceFlagBroadcaster(value: any) {
    // console.log('Value', value);
    this.addResourceFlagBroadcaster.next(value);
  }
  setAddResourceFlag(value: any) {
    this.addResourceFlag.next(value);
  }
  setAddResourceFormData(value) {
    // console.log('FileUploadService -> setAddResourceFormData -> value', value);
    this.addResourceFormData.next(value);
  }
}
