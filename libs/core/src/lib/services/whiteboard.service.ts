import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class WhiteboardService {
  private whiteBoardActive = new BehaviorSubject<boolean>(false);
  public whiteBoardActive$ = this.whiteBoardActive.asObservable();

  constructor() {}

  setWhiteboardActive(value: boolean) {
    this.whiteBoardActive.next(value);
  }
}
