import { Injectable } from '@angular/core';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { RequestApiService } from './request-api.service';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class CommonService {
  public applicationMode = 'teaching';
  public signInActive = false;
  private ebookResourcelist = new ReplaySubject();
  public ebookResourcelist$ = this.ebookResourcelist.asObservable();
  private ebookResourcelistFlag = new ReplaySubject();
  public ebookResourcelistFlag$ = this.ebookResourcelistFlag.asObservable();
  public updateResourceList = new ReplaySubject();
  public updateResourceList$ = this.updateResourceList.asObservable();
  private tempUpdateResourceList = new ReplaySubject();
  public tempUpdateResourceList$ = this.tempUpdateResourceList.asObservable();
  private SearchResource = new Subject();
  public SearchResource$ = this.SearchResource.asObservable();
  private classroomModeState = new Subject();
  public classroomModeState$ = this.classroomModeState.asObservable();
  private userProfileState = new Subject();
  public userProfileState$ = this.userProfileState.asObservable();
  private userAdmin = new Subject();
  public userAdmin$ = this.userAdmin.asObservable();

  private setFlyout = new Subject();
  public getFlyout$ = this.setFlyout.asObservable();

  private setEditResoureForm = new Subject();
  public setEditResoureForm$ = this.setEditResoureForm.asObservable();

  private addResourceState = new Subject();
  public addResourceState$ = this.addResourceState.asObservable();

  private openAddResourceFormBrodCast = new Subject();
  public openAddResourceFormBrodCast$ = this.openAddResourceFormBrodCast.asObservable();
  public newCustomResource = new Subject();
  public newCustomResource$ = this.newCustomResource.asObservable();

  private closeLessonPlayer = new Subject();
  public closeLessonPlayer$ = this.closeLessonPlayer.asObservable();

  private onDeleteResource = new Subject();
  public onDeleteResource$ = this.onDeleteResource.asObservable();

  private addResourcePanel = new BehaviorSubject(0);
  public addResourcePanel$ = this.addResourcePanel.asObservable();

  constructor(
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private toastrService: ToastrService
  ) {}

  setAddResourceState(value){
    this.addResourceState.next(value);
  }

  setAddResourcePanel(value){    
    this.addResourcePanel.next(value);
  }

  openEditmodeOnDeleteResource(value){
    this.onDeleteResource.next(value);
  }

  setEditResource(value){
    this.setEditResoureForm.next(value)
  }

  setFlyoutMode(value){
    this.setFlyout.next(value);
  }

  setLessonPlayerMode(value){
    this.closeLessonPlayer.next(value);
  }

  setebookResourcelist(resource) {
    this.ebookResourcelist.next(resource);
  }
  setebookResourcelistFlag(resource) {
    this.ebookResourcelistFlag.next(resource);
  }
  setTempResourceList(resource){
    this.tempUpdateResourceList.next(resource);
  }
  setupdateResourcelist(resource) {
    //console.log("---------FINAL----------------RESOURCES",resource);
    this.updateResourceList.next(resource);
  }
  setSearchResource(resource) {
    this.SearchResource.next(resource);
  }
  setClassroomModeState(flag) {
    //console.log('setClassroomModeState', flag);
    // let check = false
    this.classroomModeState.next(flag);
  }
  setUserProfileState(state) {
    this.userProfileState.next(state);
  }

  setUserAdmin(state) {
    this.userAdmin.next(state);
  }

  setOpenAddResourceFormBrodCast(val) {
    this.openAddResourceFormBrodCast.next(val);
  }

  setNewPassword(data) {
    this.http
      .put<any>(this.requestApiService.getUrl('changePassword'), data)
      .subscribe(
        status => {
        this.toastrService.success('Password Changed Successfully')

          //console.log('CommonService -> setNewPassword -> status', status);
        },
        (error: HttpErrorResponse) => {
          let errormessage: any;
          if (error && error.error['errorMessage']) {
            errormessage = error.error['errorMessage'];
          } else {
            errormessage = 'Update Password Error';
          }

          this.toastrService.error(errormessage);
        }
      );
  }

  setNewPin(data) {
    this.http
      .put<any>(this.requestApiService.getUrl('changePin'), data)
      .subscribe(
        status => {
        this.toastrService.success('Pin Changed Successfully')

          //console.log('CommonService -> setNewPassword -> status', status);
        },
        (error: HttpErrorResponse) => {
          let errormessage: any;
          if (error && error.error['errorMessage']) {
            errormessage = error.error['errorMessage'];
          } else {
            errormessage = 'Update PIN Error';
          }

          this.toastrService.error(errormessage);
        }
      );
  }
}
