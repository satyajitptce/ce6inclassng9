import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  Subject,
  ReplaySubject,
  Observable,
  combineLatest,
  BehaviorSubject
} from 'rxjs';
import { map, filter, take } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';
import { RequestApiService } from './request-api.service';
import {
  IGetRecentData,
  IRecentViewState
} from '../models/curriculum/recent-classes.interface';
import {
  GradeLevel,
  FullClassSelection,
  Curriculum,
  RecentViewState
} from '../models/curriculum/curriculum.interface';
import {
  ApiDivision,
  ApiSubject,
  ApiCurriculum
} from '../models/curriculum/curriculum-api.interface';
import { ToolbarService } from './toolbar.service';
import {
  AuthStateEnum,
  FirstTimeUserStatusEnum
} from '../models/auth/auth.interface';

@Injectable({
  providedIn: 'root'
})
export class SubjectSelectorService {
  private _recentViewsData: RecentViewState[] = [];
  private recentViewsData = new ReplaySubject<RecentViewState[]>(1);
  public recentViewsData$ = this.recentViewsData.asObservable();

  private mostRecentView = new BehaviorSubject<RecentViewState>(null);
  public mostRecentView$ = this.mostRecentView.asObservable();

  private updateRecentStatus = new ReplaySubject<IRecentViewState>(1);
  public updateRecentStatus$ = this.updateRecentStatus.asObservable();

  private _availableGrades: GradeLevel[] = [];
  private availableGrades = new ReplaySubject<GradeLevel[]>(1);
  public availableGrades$ = this.availableGrades.asObservable();

  private _gradeLevelSelection: GradeLevel = null;
  private gradeLevelSelection = new BehaviorSubject<GradeLevel>(
    this._gradeLevelSelection
  );
  public gradeLevelSelection$ = this.gradeLevelSelection.asObservable();

  private _divisionSelection: ApiDivision = null;
  private divisionSelection = new BehaviorSubject<ApiDivision>(
    this._divisionSelection
  );
  public divisionSelection$ = this.divisionSelection.asObservable();

  private _subjectSelection: ApiSubject = null;
  private subjectSelection = new BehaviorSubject<ApiSubject>(
    this._subjectSelection
  );
  public subjectSelection$ = this.subjectSelection.asObservable();

  private _topicSelection: any;
  private topicSelection = new BehaviorSubject<any>(this._topicSelection);
  public _topicSelection$ = this.topicSelection.asObservable();

  private _fullClassSelection = null;
  private fullClassSelection = new ReplaySubject<FullClassSelection>(1);
  public fullClassSelection$ = this.fullClassSelection
    .asObservable()
    .pipe(filter(fullClassSelection => fullClassSelection !== null));

  public selectedRecentView: RecentViewState = null;

  constructor(
    private http: HttpClient,
    private toolbarService: ToolbarService,
    private authService: AuthenticationService,
    private requestApiService: RequestApiService
  ) {
    this.authService.userState$
      .pipe(
        filter(userState => userState.authStatus !== AuthStateEnum.PRE_AUTH)
      )
      .subscribe(userState => {
        if (userState.authStatus !== userState.previousAuthStatus) {
          if (userState.authStatus === AuthStateEnum.AUTH) {
            this.getInitialData();
          } else if (userState.authStatus === AuthStateEnum.NO_AUTH) {
            this.setRecentViewsData([]);
            this.resetAllSelections();
          }
        }
      });
  }

  getInitialData() {
    // console.log('getInitialData');

    combineLatest([this.getCurriculum(), this.getRecentViews()]).subscribe(
      ([curriculum, recentViews]) => {
        if (recentViews && recentViews.length > 0) {
          this.authService.setUserState({
            firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_FIRST_TIME
          });
        } else {
          this.authService.setUserState({
            firstTimeUserStatus: FirstTimeUserStatusEnum.FIRST_TIME
          });
        }
        const presetFullClassSelection = this.getMostRecentFullClassSelection(
          curriculum,
          recentViews
        );
        if (presetFullClassSelection) {
          // console.log("presetFullClassSelection--> ",presetFullClassSelection);
          
          this.setGradeLevelSelection(
            presetFullClassSelection.gradeLevel,
            false
          );
          this.setDivisionSelection(presetFullClassSelection.division, false);
          this.setSubjectSelection(presetFullClassSelection.subject, false);
          // console.log("1--");
          
          this.setFullClassSelection(presetFullClassSelection);
        } else {
          this.setGradeLevelSelection(curriculum.gradeLevels[0]);
        }
        this.setAvailableGrades(curriculum.gradeLevels);
        this.setRecentViewsData(recentViews);
      }
    );
  }

  resetAllSelections() {
    this.setGradeLevelSelection(null, false);
    this.setDivisionSelection(null, false);
    this.setSubjectSelection(null, false);
    this._fullClassSelection = null;
    this.fullClassSelection.next(this._fullClassSelection);
  }

  private setAvailableGrades(grades: GradeLevel[]) {
    this._availableGrades = grades;
    this.availableGrades.next(this._availableGrades);
  }

  public setGradeLevelSelection(gradeLevel: GradeLevel, sideEffects = true) {
    this._gradeLevelSelection = gradeLevel;
    this.gradeLevelSelection.next(this._gradeLevelSelection);
    if (sideEffects) {
      this.setSubjectSelection(null);
      this.setDivisionSelection(null);
      this.determineFullClassSelection();
    }
  }

  public setDivisionSelection(division: ApiDivision, sideEffects = true) {
    this._divisionSelection = division;
    this.divisionSelection.next(this._divisionSelection);
    if (sideEffects) {
      this.determineFullClassSelection();
    }
  }

  public setSubjectSelection(subject: ApiSubject, sideEffects = true) {
    this._subjectSelection = subject;
    this.subjectSelection.next(this._subjectSelection);
    if (sideEffects) {
      this.determineFullClassSelection();
    }
  }

  private determineFullClassSelection() {
    let divisionSelectionMadeOrUnecessary = false;
    if (
      (this._gradeLevelSelection.divisions && this._divisionSelection) ||
      !this._gradeLevelSelection.divisions
    ) {
      divisionSelectionMadeOrUnecessary = true;
    }
    if (
      divisionSelectionMadeOrUnecessary &&
      this._gradeLevelSelection &&
      this._subjectSelection
    ) {
      // console.log("2--");

      this.setFullClassSelection({
        gradeLevel: this._gradeLevelSelection,
        division: this._divisionSelection,
        subject: this._subjectSelection
      });
    }
  }

  private getMostRecentFullClassSelection(
    curriculum: Curriculum,
    recentViews: RecentViewState[]
  ): FullClassSelection {
    const mostRecentView = this.getMostRecentView(recentViews);
    this.selectedRecentView = mostRecentView;
    this.mostRecentView.next(mostRecentView);

    if (!mostRecentView) {
      return null;
    }

    const matchingGradeLevel = curriculum.gradeLevels.find(gradeLevel => {
      return mostRecentView.gradeId === gradeLevel.id;
    });
    //console.log('matchingGradeLevel', matchingGradeLevel);
    if (!matchingGradeLevel) {
      return null;
    }

    if (
      (!matchingGradeLevel.divisions && mostRecentView.division) ||
      (matchingGradeLevel.divisions && !mostRecentView.division)
    ) {
      return null;
    }
    let matchingDivision = null;
    if (mostRecentView.division) {
      matchingDivision = matchingGradeLevel.divisions.find(
        division => mostRecentView.division === division.divisionTitle
      );
    }

    const matchingSubject = matchingGradeLevel.subjects.find(
      subject => mostRecentView.subjectId === subject.subjectId
    );
    //console.log('matcingsub', matchingSubject);

    if (!matchingSubject) {
      return null;
    }
    const matchingFullClassSelection: FullClassSelection = {
      gradeLevel: matchingGradeLevel,
      subject: matchingSubject
    };
    if (matchingDivision) {
      matchingFullClassSelection.division = matchingDivision;
    }

    return matchingFullClassSelection;
  }

  private getMostRecentView(recentViews: RecentViewState[]): RecentViewState {
    // console.log("getMostRecentView");
    
    // if (sessionStorage.getItem('mostRecentView')) {
    //   console.log("in if");
      
    //   return <IRecentViewState>(
    //     JSON.parse(sessionStorage.getItem('mostRecentView'))
    //   );
    // }

    if (!recentViews) {
      // console.log("in else");
      return null;
    }

    const recentViewsClone = [...recentViews];
    recentViewsClone.sort((a, b) => {
      const aDate = new Date(a.lastAccessedOn);
      const bDate = new Date(b.lastAccessedOn);
      return bDate.getTime() - aDate.getTime();
    });

    return recentViewsClone[0];
  }

  /*
  This GET API call will be made to get the curriculum which includes the selection
  of Class, division and grades
  */
  private getCurriculum(): Observable<Curriculum> {
    return this.http.get<any>(this.requestApiService.getUrl('curriculum')).pipe(
      map((res: any) => {
        return new Curriculum(<ApiCurriculum>res.curriculumVO);
      })
    );
  }

  selectRecentView(recent: RecentViewState) {
    const grade = this._availableGrades.find(g => g.id === recent.gradeId);

    if (grade) {
      const subject = grade.subjects.find(
        s => s.subjectId === recent.subjectId
      );
      const division =
        grade.divisions &&
        grade.divisions.find(d => d.divisionTitle === recent.division);

      this.selectedRecentView = recent;
      // console.log("3--");

      this.setFullClassSelection({
        gradeLevel: grade,
        division,
        subject
      });
    }
  }

  /*
  Below GET API call will be made to get the recent data.
  The recent data will consider only the first 10 records sorted based on
  the accessCount.
  */
  private getRecentViews(): Observable<RecentViewState[]> {
    return this.http
      .get<any>(this.requestApiService.getUrl('recentViews'))
      .pipe(
        map((res: any) => {
          if (res.data !== '') {
            return JSON.parse(res.data)
              .map((recentView: IGetRecentData) => {
                return new RecentViewState(recentView);
              })
              .sort((a: RecentViewState, b: RecentViewState) => {
                return b.accessCount - a.accessCount;
              })
              .slice(0, 7) as RecentViewState[];
          } else {
            return [];
          }
        })
      );
  }

  /*
  Below POST API will be called only once based on the data received
  from the recentView GET call.
  If the length of the response received from the GET call is 0,this
  denotes that the user has loggedIn for the first time.Else, this API
  wont be called.
  */
  getRecentViewState(data: IRecentViewState): Observable<any> {
    return this.http
      .post<any>(this.requestApiService.getUrl('recentViews'), {
        data: JSON.stringify([data])
      })
      .pipe(
        map(result => {
          return result.responseData;
        })
      );
  }

  /*
  Below PUT API will be called to update the accessCount and the lastAccessedOn attributes.
  This API will return whether the update was successful or not.
  */
  updateRecentViewState(data: IRecentViewState[]): void {
    this.http
      .put<any>(this.requestApiService.getUrl('recentViews'), {
        data: JSON.stringify(data)
      })
      .subscribe(status => {
        this.updateRecentStatus.next(status);
      });
  }

  setFullClassSelection(classSelection: FullClassSelection) {
    if (classSelection.gradeLevel && classSelection.subject) {
      this._fullClassSelection = classSelection;
      this.fullClassSelection.next(this._fullClassSelection);
      this.toolbarService.setSelectedSubjectId(
        classSelection.subject.subjectId
      );
      this.authService.setUserState({
        firstTimeUserStatus: FirstTimeUserStatusEnum.NOT_FIRST_TIME
      });
    }
  }

  private setRecentViewsData(recentViews: RecentViewState[]) {
    this._recentViewsData = recentViews;
    this.recentViewsData.next(this._recentViewsData);
  }

  setMostRecentViewInSession(recentView: RecentViewState) {
    sessionStorage.setItem('mostRecentView', JSON.stringify(recentView));
  }

  setMostRecentViewByChapterAndTopicIds(chapterId: string, topicId: string) {
    if (topicId) {
      this.topicSelection.next(topicId);
    }
    const recentView = new RecentViewState({
      accessCount: 1,
      subjectId: this._fullClassSelection.subject.subjectId,
      subjectTitle: this._fullClassSelection.subject.title,
      division: this._fullClassSelection.division.divisionTitle,
      lastAccessedOn: new Date(),
      gradeName: this._fullClassSelection.gradeLevel.title,
      gradeId: this._fullClassSelection.gradeLevel.id,
      className: this._fullClassSelection.gradeLevel.title,
      classId: this._fullClassSelection.gradeLevel.id,
      chapterId: chapterId,
      topicId: topicId
    });
    this.setMostRecentViewInSession(recentView);
    this.findRecentViewState(recentView);
  }

  findRecentViewState(fullChapterSelection: RecentViewState) {
    if (this._recentViewsData.length === 0) {
      this.getRecentViewState(fullChapterSelection).subscribe(state => {
        //when the data is updated for the first time user , call the get API to update your recent panel.
        this.getRecentViews();
      });
    } else {
      const toUpdate: IRecentViewState[] = [];

      /*
      Here, we are filtering down the update array to JUST unique values, as dictacted by the gradeId, division, and subjectId.
      In general, this should be unnecessary, but for now it is here to reconcile duplicates from before we put in protections
      for that further below, and probably not a bad safeguard to have in general.
      */
      const uniqueMap = new Map();
      for (const recentView of this._recentViewsData) {
        const uniqueKey =
          recentView.gradeId + recentView.division + recentView.subjectId;
        if (!uniqueMap.has(uniqueKey)) {
          uniqueMap.set(uniqueKey, true);
          toUpdate.push(recentView);
        }
      }

      const foundRecentView = toUpdate.find(recentView => {
        return (
          recentView.gradeName === fullChapterSelection.gradeName &&
          recentView.division === fullChapterSelection.division &&
          recentView.subjectTitle === fullChapterSelection.subjectTitle
        );
      });
      if (foundRecentView) {
        foundRecentView.chapterId = fullChapterSelection.chapterId;
        foundRecentView.topicId = fullChapterSelection.topicId;
        foundRecentView.accessCount += 1;
        foundRecentView.lastAccessedOn = new Date();
      } else {
        toUpdate.push(fullChapterSelection);
      }

      this.updateRecentViewState(toUpdate);
    }
  }
}
