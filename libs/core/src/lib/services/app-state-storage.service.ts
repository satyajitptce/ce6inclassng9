import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { timer, of, interval, Subject, Observable } from 'rxjs';
import { filter, map, tap } from 'rxjs/operators';
import { AppConfigService } from '../services/app-config.service';
import { SubjectSelectorService } from './subject-selector.service';
import { RequestApiService } from './request-api.service';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from './authentication.service';
import { AuthStateEnum } from '../models/auth/auth.interface';
import { switchMap, takeUntil, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AppStateStorageService {
  // userId is a placeholder and might need to be set in the authentication or user services.
  private userId = 'default';
  private currentStorageKey: string;
  private currentGrade: any;
  private currentSubject: any;
  private currentTopic: any;
  isLoggedIn: any = false;
  // private saveWhiteBoardInterval$ = interval(
  //   this.appConfigService.getGlobalSettingConfig('saveWhiteboardStateInterval')
  // ).pipe(filter(() => !!this.currentStorageKey && !!this.userId));

  private saveToWhiteboard = new Subject();
  public saveToWhiteBoard$ = this.saveToWhiteboard.asObservable();

  constructor(
    private requestApiService: RequestApiService,
    private appConfigService: AppConfigService,
    private subjectSelectorService: SubjectSelectorService,
    private http: HttpClient,
    private toastrService: ToastrService,
    private authService: AuthenticationService
  ) {
    // this.saveWhiteBoardInterval$.subscribe(() => {
    //   this.saveCurrentWhiteboardState();
    // });
    // get selected "Grade" from subjectSelectorService
    this.subjectSelectorService.gradeLevelSelection$.subscribe(grade => {
      if (grade) {
        this.currentGrade = grade.id;
      }
    });

    // get selected "Subject" from subjectSelectorService
    this.subjectSelectorService.subjectSelection$.subscribe(subject => {
      if (subject) {
        this.currentSubject = subject.subjectId;
      }
    });

    // get selected "topic" from subjectSelectorService
    this.subjectSelectorService._topicSelection$.subscribe(topic => {
      if (topic) {
        this.currentTopic = topic;
      }
    });
    this.authService.userState$.subscribe(userState => {
      this.isLoggedIn = userState.authStatus === AuthStateEnum.AUTH;
      // console.log("AppStateStorageService ->  this.isLoggedIn",  this.isLoggedIn)
    });
    this.authService.logoutTriggered$.subscribe(() => {
      //console.log("authservice")
      this.isLoggedIn = false;
    });
  }

  setUserId(userId: string) {
    this.userId = userId;
  }

  setStorageKey(storageKey: string) {
    this.currentStorageKey = storageKey;
  }

  public getStorageKeyName() {
    return this.userId + '.' + this.currentStorageKey;
  }

  private writeToLocalStorage(data) {
    try {
      // if (this.currentStorageKey && this.userId) {
      //   localStorage.setItem(this.getStorageKeyName(), JSON.stringify(data));
      //   //this.saveMyWhiteBoard(this.getStorageKeyName(),JSON.stringify(data))
      // }
      if (this.currentStorageKey && this.userId) {
        this.saveMyWhiteBoard(JSON.stringify(data));
      }
    } catch (e) {
      console.dir(e);
      return;
    }
  }

  saveToLocalStorage(objectToSave) {
    // if (this.currentTopic) {
    //console.log("this.isLoggedIn",this.isLoggedIn)
    if (this.isLoggedIn) {
      this.myAppData(objectToSave);
    }

    // }

    //   const localStorageData = this.myGetAppStorage();
    //  this.saveMyWhiteBoard(JSON.stringify(localStorageData));
    //   this.getAppStorageByCurrentStorageKey();

    //   this.writeToLocalStorage({
    //     ...localStorageData,
    //     ...objectToSave
    //   });
  }
  myGetAppStorage() {
    return JSON.parse(localStorage.getItem(this.getStorageKeyName()));
  }
  myAppData(objectToSave) {
    let myData: any = [];
    if (this.currentTopic) {
      const currentSessionData = {
        tpId: this.currentTopic,
        gradeId: this.currentGrade,
        subjectId: this.currentSubject
      };
      //console.log("currentSessionData.tpId",currentSessionData)
      this.http
        .get(this.requestApiService.getUrl('saveWb'), {
          params: currentSessionData
        })
        .subscribe(
          response => {
            // console.log(
            //   'TCL: AppStateStorageService -> myAppData -> response',
            //   response
            // );
            if (response === null) {
              const dummyarray = { players: [], drawings: [] };
              this.writeToLocalStorage({
                //topicid:this.currentTopic,
                ...dummyarray,
                ...objectToSave
              });
            }
            if (response && response['wbContent']) {
              let newParseData: any;
              if (response['wbContent'] !== null) {
                myData = response['wbContent'];
                newParseData = this.validJSON(myData);
                if (newParseData) {
                  const parseData = JSON.parse(myData);
                  //console.log('onload', parseData);
                  this.writeToLocalStorage({
                    //topicid:this.currentTopic,
                    ...parseData,
                    ...objectToSave
                  });
                }
              }
            }
          },
          (error: HttpErrorResponse) => {
            let errormessage: any;
            if (error && error.error['errorMessage']) {
              errormessage = error.error['errorMessage'];
            } else {
              errormessage = 'getAppStorageError';
            }
            //this.toastrService.error(errormessage);
          }
        );
    }
  }
  validJSON(response) {
    try {
      JSON.parse(response);
    } catch (e) {
      return false;
    }
    return true;
  }
  //new code
  myGetAppStorageByCurrentStorageKey() {
     //console.log("AppStateStorageService -> myGetAppStorageByCurrentStorageKey ->")
    let myData: any = null;
    if (this.currentTopic) {
      const currentSessionData = {
        tpId: this.currentTopic,
        gradeId: this.currentGrade,
        subjectId: this.currentSubject
      };

      return this.http
        .get(this.requestApiService.getUrl('saveWb'), {
          params: currentSessionData
        })
        .pipe(
          tap(
            response => {
              if (response && response['wbContent']) {
                if (response['wbContent'] !== null) {
                  let newParseData: any;
                  myData = response['wbContent'];
                  newParseData = this.validJSON(myData);
                  //console.log("onload", newParseData)
                  if (newParseData) {
                    return newParseData;
                  } else {
                    return { players: [], drawings: [] };
                  }
                }
                //return true;
              }
            },
            (error: HttpErrorResponse) => {
              let errormessage: any;
              if (error && error.error['errorMessage']) {
                errormessage = error.error['errorMessage'];
              } else {
                errormessage = 'getAppStorageError';
              }
              //this.toastrService.error(errormessage);
            }
          )
        );
    }
    //return JSON.parse(localStorage.getItem(this.getStorageKeyName()));
  }
  //old code
  getAppStorageByCurrentStorageKey(): Observable<any> {
    return this.myGetAppStorageByCurrentStorageKey();
    //return JSON.parse(localStorage.getItem(this.getStorageKeyName()));
  }
  saveCurrentWhiteboardState() {
    this.saveToWhiteboard.next(true);
  }

  //
  /*
  save whiteboard state on db
  API : /1/api/{version}/serve/wb
  POST: params
  { "tpId": "string", "gradeId": "string", "subjectId": "string", "whiteBoardContent": "string" }
  */
  private saveMyWhiteBoard(wbData) {
    // console.log("AppStateStorageService -> saveMyWhiteBoard")
    //console.log("TCL: AppStateStorageService -> saveMyWhiteBoard -> wbData", JSON.parse(wbData))
    if (this.currentTopic) {
      const currentSessionData = {
        tpId: this.currentTopic,
        gradeId: this.currentGrade,
        subjectId: this.currentSubject,
        whiteBoardContent: wbData
      };

      this.http
        .post<any>(this.requestApiService.getUrl('saveWb'), currentSessionData)
        .subscribe(
          status => {
            //console.log('status', status);
          },
          (error: HttpErrorResponse) => {
            let errormessage: any;
            if (error && error.error['errorMessage']) {
              errormessage = error.error['errorMessage'];
            } else {
              errormessage = 'saveAppStorageError';
            }
            //this.toastrService.error(errormessage);
          }
        );
    }
  }
}
