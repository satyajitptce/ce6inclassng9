import { TestBed, async } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { of } from 'rxjs';
import { User } from '../utilities/login';

describe('LoginService', () => {
  let service: AuthenticationService;
  beforeEach(() => {
    const httpClientStub = {
      post: (arg1, object2, object3) => ({ pipe: () => ({}) })
    };
    const routerStub = {};
    TestBed.configureTestingModule({
      providers: [
        AuthenticationService,
        { provide: HttpClient, useValue: httpClientStub },
        { provide: Router, useValue: routerStub }
      ],
      imports: [HttpClientTestingModule]
    });
    service = TestBed.get(AuthenticationService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  it('returned Observable should match the right data', () => {
    const loginService = service.loginUserIdorPIN(null, '', '', '00000');
    const mockCourse = [{ responseData: 'Successfully' }];
    let response: any;
    spyOn(service, 'loginUserIdorPIN').and.returnValue(of(mockCourse));
    service
      .loginUserIdorPIN(null, '', '', '00000')
      .subscribe(res => (response = res));
    expect(response).toEqual(mockCourse);
    expect(loginService).toBeTruthy();
  });

  it('should call getUsers and return list of users', async(() => {
    const response: User[] = [];
    spyOn(service, 'loginUserIdorPIN').and.returnValue(of(response));
  }));
});
