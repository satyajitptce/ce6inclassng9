import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import {
  CmodeConfig,
  ResourceThumbnail,
  AnimationTimingConfig
} from '../models/config/config.interface';
import { EnvironmentConfig } from '../models/environment/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class AppConfigService {
  private _config: any = null;
  private _globalSettingConfig: object = null;
  private _currentCmode: string = null;
  private _cmodeConfig: CmodeConfig = null;
  private _defaultResourceThumbnails: any = null;
  href: any;
  // Path to your config file
  private CONFIG_FILE_PATH = this.environment.configFile;
  private animationTimings: AnimationTimingConfig = {
    short: 100,
    medium: 300,
    long: 500,
    'x-long': 1000,
    'delay-x-short': 50,
    'delay-medium': 200
  };

  constructor(
    private http: HttpClient,
    @Inject('environment') private environment: EnvironmentConfig
  ) {}

  /**
   * Get config by key
   *
   * @param {*} key
   * @returns
   * @memberof AppConfigService
   */
  public getConfig(key: string) {
    return this._config[key];
  }

  /**
   * Get the list of all configs
   *
   * @returns
   * @memberof AppConfigService
   */
  public getAllConfig() {
    return this._config;
  }

  /**
   * Get the list of all global setting configs
   *
   * @returns
   * @memberof AppConfigService
   */
  public getAllGlobalSetting() {
    return (this._globalSettingConfig = this._config['global_setting']);
  }

  private setCmodeConfig() {
    this._cmodeConfig = this._config['cmodes'][this._currentCmode];
  }

  public getCmode() {
    return this._currentCmode;
  }
  public getCmodeConfig(): CmodeConfig {
    return this._cmodeConfig;
  }

  public getGlobalSettingConfig(key: any) {
    return this._globalSettingConfig[key];
  }

  public getGeneralConfig(key: string): any {
    return this._config['general'][key];
  }

  private setDefaultResourceThumbnails() {
    return (this._defaultResourceThumbnails = this._config['general'][
      'playerTypesData'
    ]);
  }

  public getResourceThumbnailIcon(
    resourceType: string,
    tcetype: string,
    imageType: string
  ) {
    let getType = this._defaultResourceThumbnails[0][imageType];
    if (resourceType) {
      this._defaultResourceThumbnails.map((resource: ResourceThumbnail) => {
        if (
          resource.type.toLowerCase() === resourceType.toLowerCase() &&
          resource.tcetypes.includes(tcetype.toLowerCase())
        ) {
          getType = resource[imageType];
        }
      });
    }

    return getType;
  }

  /**
   *Sets the animation 0
   * based on the config file
   * @memberof AppConfigService
   */
  private checkAnimationStatus() {
    if (this.getConfig('global_setting')['animation'] === 0) {
      for (const [key, value] of Object.entries(this.animationTimings)) {
        this.animationTimings[key] = 0;
      }
    }
  }

  public getAnimationTimings(): AnimationTimingConfig {
    return this.animationTimings;
  }

  /**
   * Load application configuration
   *
   * @returns
   * @memberof AppConfigService
   */
  public load() {
    const urlParams = new URLSearchParams(window.location.search);
    //satyajit default cmode can be 0 or 1 below
    this._currentCmode = urlParams.get('cmode') || '1';
    return new Promise((resolve, reject) => {
      this.http
        .get(this.CONFIG_FILE_PATH)
        .pipe(
          map((res: any) => {
            /* If using the mockdata environment, turn off the login and intro, because it's annoying for developers. */
            if (this.environment.mockdata) {
              res.global_setting.modulelogin = 0;
              res.global_setting.moduleintro = 0;
            }
            this.href = window.location.href;
            if (this.href) {
              const letterNumber = /^[0-9]+$/;
              const newhref = this.href.split('?').pop();
              if (newhref) {
                const newconfigvalue = newhref
                  .split('&')
                  .map(pair => pair.split('='));

                const paramArr = {
                  pb: { name: 'playerbehaviour' },
                  wb: { name: 'modulewhiteboard' },
                  anim: { name: 'animation' }
                };
                newconfigvalue.forEach(([key, value]) => {
                  Object.entries(paramArr).map(([newkey, newvalue]) => {
                    if (key === newkey) {
                      key = newvalue.name;
                    }
                  });
                  if (value && value.match(letterNumber)) {
                    res.global_setting[key] = parseInt(value);
                  } else {
                    res.global_setting[key] = value;
                  }
                });
              }
            }
            return res;
          }),
          catchError(error => throwError(error))
        )
        .subscribe(confRes => {
          this._config = confRes;
          this.setCmodeConfig();
          this.getAllGlobalSetting();
          this.setDefaultResourceThumbnails();
          this.checkAnimationStatus();
          resolve(true);
        });
    });
  }
}
