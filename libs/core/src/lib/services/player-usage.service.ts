import { Injectable } from '@angular/core';
import { SubjectSelectorService } from '@tce/core';
import { Resource } from '../models/curriculum/curriculum.interface';
import { CurriculumPlaylistService } from './curriculum-playlist.service';

@Injectable({
  providedIn: 'root'
})
export class PlayerUsageService {
  private _currentResourceId = null;
  private resourceTimeRecords = {};
  public gardeId;
  public subjectId;

  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private subjectSelectorService: SubjectSelectorService,
  ) {

    this.subjectSelectorService.gradeLevelSelection$.subscribe(grade => {
      if (grade) {
        this.gardeId = grade.id;
      }
    });

    this.subjectSelectorService.subjectSelection$.subscribe(subject => {
      if (subject) {
        this.subjectId = subject.subjectId;
      }
    });
  }

  log(resourceId, assetType, duration){
    this.curriculumPlaylistService.captureLogUsageReport({
      type: assetType, //resource.metaData.assetType,
      resourceId: resourceId,
      duration: duration,
      launchedFrom: "ce6",
      subjectId: this.subjectId,
      gradeId: this.gardeId
    }).subscribe();
  }

  endResourceUsage(resourceId){
    const time = new Date().getTime();
    const start = this.resourceTimeRecords[resourceId].start;
    const assetType = this.resourceTimeRecords[resourceId].assetType;
    const duration = Math.round((time-start)/1000);
    delete this.resourceTimeRecords[resourceId];
    this.log(resourceId, assetType, duration);
  }

  logResourceUsageOnClose(resource: Resource){
    if(this._currentResourceId===resource.resourceId){
      this.endResourceUsage(
        this._currentResourceId
      );
    }
  }

  logResourceUsage(resource: Resource){
    console.log("🚀 ~ file: player-usage.service.ts ~ line 34 ~ PlayerUsageService ~ logResourceUsage ~ resource", resource)
    //console.log(this.resourceTimeRecords)
    if(!this._currentResourceId || this._currentResourceId!==resource.resourceId){
      if(!this.resourceTimeRecords[resource.resourceId]){
        // end time of previous resource
        if(this.resourceTimeRecords[this._currentResourceId]){
          this.endResourceUsage(
            this._currentResourceId
          );
        }
        //start time of current resource
        this.resourceTimeRecords[resource.resourceId] = {
          start: new Date().getTime(),
          assetType: resource.metaData.assetType,
        }
      }
      this._currentResourceId = resource.resourceId;
    }
  }
}
