import { TestBed } from '@angular/core/testing';

import { CurriculumPlaylistService } from './curriculum-playlist.service';

describe('CurriculumPlaylistService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CurriculumPlaylistService = TestBed.get(
      CurriculumPlaylistService
    );
    expect(service).toBeTruthy();
  });
});
