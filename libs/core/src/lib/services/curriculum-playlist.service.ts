import { Injectable, Injector } from '@angular/core';
import { ReplaySubject, Subscription, Subject, from } from 'rxjs';
import { Observable, forkJoin } from 'rxjs';
import { SubjectSelectorService } from './subject-selector.service';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { RequestApiService } from './request-api.service';
import { AuthenticationService } from './authentication.service';
import { ToastrService } from 'ngx-toastr';
import {
  Resource,
  Ebook,
  Chapter,
  Topic,
  FullContentSelection,
  LessonPlanResource,
  FullClassSelection,
  RecentViewState,
  FilterMenu,
  CustomEbookData
} from '../models/curriculum/curriculum.interface';
import { AppStateStorageService } from './app-state-storage.service';
import { ToolbarService } from './toolbar.service';
import { AuthStateEnum } from '../models/auth/auth.interface';
import { ApiEbook } from '../models/curriculum/curriculum-api.interface';
import { CurriculumUtils } from '../utils/curriculum.utils';
import { ResourceRequestType } from '../enums/resource-request-type.enum';
import { ResourceType } from '../enums/resource-type.enum';
import { FilterResourceType } from '../enums/filter-resource-type.enum';
import { AppConfigService } from './app-config.service';
import { PlayerContainerService } from '@app-teacher/services';
import { CommonService } from './common.service';
import { ContentEditorService } from '../../../../content-library/src/lib/service/content-editor.service';
@Injectable({
  providedIn: 'root'
})
export class CurriculumPlaylistService {
  private url = this.requestApiService.getUrl('getFile');
  public auxTopic: Topic;

  public questionbankDataBroadcast = new Subject();
  public questionbankDataBroadcast$ = this.questionbankDataBroadcast.asObservable();

  public customQuestionIds = [];
  public customQuestionDetails = [];

  // public cutomQuizIds;

  public cutomQuizIds = new Subject();
  public cutomQuizIds$ = this.cutomQuizIds.asObservable();

  private _availableVideos: Resource[] = [];
  private availableVideos = new ReplaySubject();
  public availableVideos$ = this.availableVideos.asObservable();

  private _ebookSelection: Ebook;
  private ebookSelection = new ReplaySubject<Ebook>(1);
  public ebookSelection$ = this.ebookSelection.asObservable();

  private _ebookFetching = false;
  private ebookFetching = new ReplaySubject<boolean>(1);
  public ebookFetching$ = this.ebookFetching.asObservable();

  private ebookFetchError = new ReplaySubject<string>(1);
  public ebookFetchError$ = this.ebookFetchError.asObservable();
  private selectedChapterErrorMessage = new ReplaySubject<string>(1);
  public selectedChapterErrorMessage$ = this.selectedChapterErrorMessage.asObservable();

  private _chapterSelection: Chapter;
  private chapterSelection = new ReplaySubject<Chapter>(1);
  public chapterSelection$ = this.chapterSelection.asObservable();

  private _topicSelection: Topic;
  private topicSelection = new ReplaySubject<Topic>(1);
  public topicSelection$ = this.topicSelection.asObservable();

  private fullContentSelection = new ReplaySubject<FullContentSelection>(1);
  public fullContentSelection$ = this.fullContentSelection.asObservable();

  private lessonPlanSelection = new ReplaySubject<LessonPlanResource>(1);
  public lessonPlanSelection$ = this.lessonPlanSelection.asObservable();

  public availableEbooks: Ebook[];
  private availableEbooksSubj = new ReplaySubject<Ebook[]>(1);
  public availableEbooks$ = this.availableEbooksSubj.asObservable();

  private _availableChapters: Chapter[];
  private availableChapters = new ReplaySubject<Chapter[]>(1);
  private selectedSubjectChapterList = new Subject<Chapter[]>();
  public selectedSubjectChapterList$ = this.selectedSubjectChapterList.asObservable();
  private selectedSubjectChapterPlanningList = new Subject<any>();
  public selectedSubjectChapterPlanningList$ = this.selectedSubjectChapterPlanningList.asObservable();
  private selectedSubjectChapterQuizList = new Subject<any>();
  public selectedSubjectChapterQuizList$ = this.selectedSubjectChapterQuizList.asObservable();
  public availableChapters$ = this.availableChapters.asObservable();

  private _availableResources: Resource[];
  private availableResources = new ReplaySubject<Resource[]>(1);
  public availableResources$ = this.availableResources.asObservable();

  private availableCustomResources = new Subject();
  public availableCustomResources$ = this.availableCustomResources.asObservable();
  private currAddResourceFlagBroadcaster = new Subject();
  public currAddResourceFlagBroadcaster$ = this.currAddResourceFlagBroadcaster.asObservable();
  private _resourcesFetching = false;
  private resourcesFetching = new ReplaySubject<boolean>(1);
  public resourcesFetching$ = this.resourcesFetching.asObservable();

  private resourcesLoadingError = new ReplaySubject<string>(1);
  public resourcesLoadingError$ = this.resourcesLoadingError.asObservable();

  private recentViewsData: RecentViewState[];

  private _resourceRequest: Subscription;

  public appliedResourceFilters = new ReplaySubject<FilterMenu[]>(1);
  public appliedResourceFilters$ = this.appliedResourceFilters.asObservable();

  private filteredMenu = new ReplaySubject<FilterMenu[]>(1);
  public filteredMenu$ = this.filteredMenu.asObservable();
  customBook: Chapter;
  currentGrade: any;
  currentSubject: any;
  checkCustomResourceFlag = false;
  private _resourceFilter: FilterMenu[] = [
    {
      name: FilterResourceType.VIDEO,
      image: this.appConfigService.getResourceThumbnailIcon(
        'tcevideo',
        'animation',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [
        ResourceType.INTERACTIVITY,
        ResourceType.TCEVIDEO,
        ResourceType.VIDEO
      ],
      state: 'inactive'
    },
    {
      name: FilterResourceType.AUDIO,
      image: this.appConfigService.getResourceThumbnailIcon(
        'audio',
        'tce-audio',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.AUDIO],
      state: 'inactive'
    },
    {
      name: FilterResourceType.WORKSHEETS,
      image: this.appConfigService.getResourceThumbnailIcon(
        'worksheet',
        'worksheet',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.WORKSHEET],
      state: 'inactive'
    },
    // {
    //   name: FilterResourceType.IMAGES,
    //   image: this.appConfigService.getResourceThumbnailIcon(
    //     'default',
    //     'tce-image',
    //     'icon'
    //   ),
    //   resourceCount: 0,
    //   selected: false,
    //   type: [ResourceType.IMAGE]
    // },
    {
      name: FilterResourceType.GALLERY,
      image: this.appConfigService.getResourceThumbnailIcon(
        'default',
        'gallery',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.GALLERY],
      state: 'inactive'
    },
    {
      name: FilterResourceType.UNSUPPORT,
      image: this.appConfigService.getResourceThumbnailIcon(
        'default',
        'unsupport',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.UNSUPPORT],
      state: 'inactive'
    },
    {
      name: FilterResourceType.GAME,
      image: this.appConfigService.getResourceThumbnailIcon(
        'tcevideo',
        'tce-games',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.GAME],
      state: 'inactive'
    },
    {
      name: FilterResourceType.QUIZ,
      image: this.appConfigService.getResourceThumbnailIcon(
        'quiz',
        'quiz',
        'icon'
      ),
      resourceCount: 0,
      selected: false,
      type: [ResourceType.QUIZ],
      state: 'inactive'
    }
  ];

  constructor(
    private subjectSelectorService: SubjectSelectorService,
    private http: HttpClient,
    private requestApiService: RequestApiService,
    private appStateStorageService: AppStateStorageService,
    private toolbarService: ToolbarService,
    private authService: AuthenticationService,
    private appConfigService: AppConfigService,
    private toastrService: ToastrService,
    private injector: Injector, //private playerContainerService: PlayerContainerService
    private commonService: CommonService,
    private contentEditorService: ContentEditorService
  ) {
    //injector.get(PlayerContainerService);
    this.subjectSelectorService.gradeLevelSelection$.subscribe(grade => {
      if (grade) {
        this.currentGrade = grade.id;
      }
    });
    this.subjectSelectorService.subjectSelection$.subscribe(subject => {
      if (subject) {
        this.currentSubject = subject.subjectId;
      }
    });
    this.subjectSelectorService.recentViewsData$.subscribe(recentViews => {
      const recentViewsSorted = [...recentViews];
      recentViewsSorted.sort((a, b) => {
        const aDate = new Date(a.lastAccessedOn);
        const bDate = new Date(b.lastAccessedOn);
        return bDate.getTime() - aDate.getTime();
      });
      this.recentViewsData = recentViewsSorted;
    });

    this.subjectSelectorService.fullClassSelection$.subscribe(
      (classSelection: FullClassSelection) => {
        if (classSelection) {
          this.getEbookDetails(classSelection.subject);
        } else {
          //console.log('else classSelection');
        }
      }
    );
    this.authService.userState$.subscribe(userState => {
      const { previousAuthStatus, authStatus } = userState;
      if (
        previousAuthStatus !== authStatus &&
        authStatus === AuthStateEnum.NO_AUTH
      ) {
        this.resetAllSelections();
      }
    });
    this.toolbarService.clearAllAnnotationsBroadcaster$.subscribe(() => {
      this.clearStoredSessionAnnotations();
    });
    this.toolbarService.clearWhiteboardBroadcaster$.subscribe(() => {
      this.clearStoredSessionAnnotations();
    });
  }

  clearStoredSessionAnnotations() {
    Object.keys(sessionStorage).forEach(storageKey => {
      const foundResourceIndex = this._availableResources.findIndex(
        resource => {
          return storageKey.includes(resource.resourceId);
        }
      );
      if (foundResourceIndex !== -1) {
        sessionStorage.removeItem(storageKey);
      }
    });
  }

  setEbookFetching(isFetching: boolean) {
    if (this._ebookFetching !== isFetching) {
      this._ebookFetching = isFetching;
      this.ebookFetching.next(this._ebookFetching);
    }
  }

  setEbookFetchError(errorMessage: string) {
    this.ebookFetchError.next(errorMessage);
  }

  getEbookDetails(books) {
    // console.log('CurriculumPlaylistService -> getEbookDetails -> books', books);
    this.setEbookFetchError('');
    this.setEbookFetching(true);
    if (books.books && books.books.bookId) {
      this.http
        .get<any>(
          this.requestApiService
            .getUrl('book')
            .replace('@bookid@', books.books.bookId)
        )
        .pipe(
          catchError(err => {
            this.setEbookFetching(false);
            this.setEbookFetchError(
              'Error getting curriculum for selected class.'
            );
            throw err;
          })
        )
        .pipe(
          map((res: ApiEbook) => {
            return res;
          })
        )
        .subscribe((bookDetails: ApiEbook) => {
          // console.log("bookDetails--> ",bookDetails);
          
          if (bookDetails) {
            this.http
              .get<any>(
                this.requestApiService
                  .getUrl('getCustomBookJSON')
                  .replace('@bookid@', books.books.bookId)
              )
              .subscribe((data: any) => {
                //console.log('customBook', data);
                if (data && data != null) {
                  CustomEbookData(data);
                  //console.log('CustomEbookData', data);
                } else {
                  CustomEbookData([]);
                }
                this.setEbookFetching(false);
                let myJson: any = JSON.parse(bookDetails.json);
                // console.log('myJson-->', myJson);

                //old code
              //   if (myJson) {
              //     if (
              //       data &&
              //       data.property &&
              //       data.property.length > 0 &&
              //       data != null
              //     ) {
              //       Array.prototype.push.apply(myJson.node, data.property);
              //     }

              //     bookDetails.json = JSON.stringify(myJson);

              //     this.generateBookData(bookDetails);
              //   }
              // });


                if (myJson) {   
                  // console.log("myjson",data);
                  
                  if (
                    data &&
                    data.property &&
                    data.property.length > 0 &&
                    data != null
                  ) {
                    // console.log("no data");

                    if(myJson.booktree){
                      // console.log("tree");
                      
                      Array.prototype.push.apply(myJson.booktree.node, data.property);
                      bookDetails.json = JSON.stringify(myJson.booktree);

                    }
                    else{
                      // console.log("else");

                      Array.prototype.push.apply(myJson.node, data.property);
                      bookDetails.json = JSON.stringify(myJson);
                    }
                  }
                  else{
                    // console.log("am the culprit",bookDetails);
                    if(myJson.booktree){
                      bookDetails.json = JSON.stringify(myJson.booktree);
                    }
                    
                  }
                  this.generateBookData(bookDetails);
                }
              });
            //let myItem: any = localStorage.getItem(books.books.bookId);
            //console.log("CurriculumPlaylistService -> getEbookDetails -> myItem", JSON.parse( myItem).property)
          }
        });
    } else {
      this.resetAllSelections(true);
      this.setEbookFetching(false);
      this.setEbookFetchError(
        'No curriculum associated with the selected subject'
      );
    }
  }

  getMyEbook(books) {
    //console.log('CurriculumPlaylistService -> getEbookDetails -> books', books);
    //this.setEbookFetchError('');
    //this.setEbookFetching(true);
    if (books.books && books.books.bookId) {
      this.http
        .get<any>(
          this.requestApiService
            .getUrl('book')
            .replace('@bookid@', books.books.bookId)
        )
        .pipe(
          catchError(err => {
            this.setEbookFetching(false);
            this.setEbookFetchError(
              'Error getting curriculum for selected class.'
            );
            throw err;
          })
        )
        .pipe(
          map((res: ApiEbook) => {
            return res;
          })
        )
        .subscribe((bookDetails: ApiEbook) => {
          if (bookDetails) {
            this.http
              .get<any>(
                this.requestApiService
                  .getUrl('getCustomBookJSON')
                  .replace('@bookid@', books.books.bookId)
              )
              .subscribe((data: any) => {
                //console.log('customBook', data);
                if (data && data != null) {
                  CustomEbookData(data);
                  //console.log('CustomEbookData', data);
                } else {
                  CustomEbookData([]);
                }
                this.setEbookFetching(false);
                let myJson: any = JSON.parse(bookDetails.json);
                if (myJson) {
                  if (
                    data &&
                    data.property &&
                    data.property.length > 0 &&
                    data != null
                  ) {
                    if(myJson.booktree){
                      // console.log("tree");
                      
                      Array.prototype.push.apply(myJson.booktree.node, data.property);
                      bookDetails.json = JSON.stringify(myJson.booktree);

                    }
                    else{
                      
                      Array.prototype.push.apply(myJson.node, data.property);
                      bookDetails.json = JSON.stringify(myJson);
                    }
                  }
                  else{
                    if(myJson.booktree){
                      bookDetails.json = JSON.stringify(myJson.booktree);
                    }
                  }

                  //console.log("bookDetails-->>", bookDetails)
                  this.generateMYBookData(bookDetails);
                }
              });
            //let myItem: any = localStorage.getItem(books.books.bookId);
            //console.log("CurriculumPlaylistService -> getEbookDetails -> myItem", JSON.parse( myItem).property)
          }
        });
    }
  }

  generateMYBookData(data: ApiEbook) {
    // console.log('generateBookData -> data');

    try {
      const eBook = new Ebook(data);
      //console.log('final ebook', eBook);
      if (eBook.eBookBasePath) {
        // console.log('final ebook IF ');
        this.setAvailableEbooks([eBook]);
      } else {
        // console.log('final ebook ELSE ');
        this.setAvailableEbooks([]);
      }

      //console.log('generateBookData ->  eBook.chapters', eBook.chapters);
      //console.log("generateBookData -> eBook.chapters", eBook.chapters)
      //eBook.chapters.concat(customData.custom)
      this.setAvailableChapters(eBook.chapters);
      // console.log('generateBookData -> eBook.chapters.length --', eBook.chapters.length);
      //const preselectedChapterAndTopic = this.determinePreselectedChapterAndTopic();
      // this.setChapterSelection(
      //   preselectedChapterAndTopic.chapter,
      //   preselectedChapterAndTopic.topic
      // );
    } catch (error) {
      this.setEbookFetchError('Error parsing class data');
      //this.resetAllSelections();
    }
  }

  setselectedSubjectChapter(subjectBookID) {
    // console.log(
    //   'CurriculumPlaylistService -> setselectedSubjectChapter',
    //   subjectBookID
    // );
    this.selectedChapterError('');
    if (
      subjectBookID &&
      subjectBookID.subject.books &&
      subjectBookID.subject.books.bookId
    ) {
      //console.log("CurriculumPlaylistService -> setselectedSubjectChapter -> subjectBookID", subjectBookID)
      this.http
        .get<any>(
          this.requestApiService
            .getUrl('book')
            .replace('@bookid@', subjectBookID.subject.books.bookId)
        )
        .pipe(
          catchError(err => {
            this.setEbookFetchError(
              'Error getting curriculum for selected class.'
            );
            throw err;
          })
        )
        .pipe(
          map((res: ApiEbook) => {
            return res;
          })
        )
        .subscribe((bookDetails: ApiEbook) => {
          if (bookDetails) {
            this.http
              .get<any>(
                this.requestApiService
                  .getUrl('getCustomBookJSON')
                  .replace('@bookid@', subjectBookID.subject.books.bookId)
              )
              .subscribe((data: any) => {
                if (data && data != null) {
                  CustomEbookData(data);
                } else {
                  CustomEbookData([]);
                }
                let myJson: any = JSON.parse(bookDetails.json);
                if (myJson) {
                  if (
                    data &&
                    data.property &&
                    data.property.length > 0 &&
                    data != null
                  ) {
                    if(myJson.booktree){
                      // console.log("tree");
                      
                      Array.prototype.push.apply(myJson.booktree.node, data.property);
                      bookDetails.json = JSON.stringify(myJson.booktree);

                    }
                    else{
                      // console.log("else");

                      Array.prototype.push.apply(myJson.node, data.property);
                      bookDetails.json = JSON.stringify(myJson);
                    }
                  }
                  else{
                    if(myJson.booktree){
                      bookDetails.json = JSON.stringify(myJson.booktree);
                    }
                  }
                  // bookDetails.json = JSON.stringify(myJson);
                  const eBook = new Ebook(bookDetails);

                  if (subjectBookID.type === 'planningMode') {
                    // console.log('subjectBookID.type if');
                    this.selectedSubjectChapterPlanning(
                      eBook.chapters,
                      subjectBookID
                    );
                  } else {
                    this.selectedSubjectChapter(eBook.chapters);
                  }
                }
              });
            // let myItem: any = localStorage.getItem(
            //   subjectBookID.subject.books.bookId
            // );
          }
        });
    }
    // if (subjectBookID.type === 'planningMode') {
    //   console.log('subjectBookID.type else if');
    //   this.selectedSubjectChapterPlanning([], subjectBookID);
    // }
    else {
      // console.log('subjectBookID else');

      if (subjectBookID.type === 'planningMode') {
        // console.log('subjectBookID.type else if');
        this.selectedSubjectChapterPlanning([], subjectBookID);
      } else {
        this.selectedChapterError('Selected Subject no chapters');
      }
    }
  }

  getChaptersTopicsQuizEditor(subjectBookID) {
    this.selectedChapterError('');
    if (
      subjectBookID &&
      subjectBookID.subject.books &&
      subjectBookID.subject.books.bookId
    ) {
      //console.log("CurriculumPlaylistService -> setselectedSubjectChapter -> subjectBookID", subjectBookID)
      this.http
        .get<any>(
          this.requestApiService
            .getUrl('book')
            .replace('@bookid@', subjectBookID.subject.books.bookId)
        )
        .pipe(
          catchError(err => {
            this.setEbookFetchError(
              'Error getting curriculum for selected class.'
            );
            throw err;
          })
        )
        .pipe(
          map((res: ApiEbook) => {
            return res;
          })
        )
        .subscribe((bookDetails: ApiEbook) => {
          if (bookDetails) {
            this.http
              .get<any>(
                this.requestApiService
                  .getUrl('getCustomBookJSON')
                  .replace('@bookid@', subjectBookID.subject.books.bookId)
              )
              .subscribe((data: any) => {
                if (data && data != null) {
                  // CustomEbookData(data);
                } else {
                  // CustomEbookData([]);
                }
                let myJson: any = JSON.parse(bookDetails.json);
                if (myJson) {
                  if (
                    data &&
                    data.property &&
                    data.property.length > 0 &&
                    data != null
                  ) {
                   if(myJson.booktree){
                      // console.log("tree");
                      
                      Array.prototype.push.apply(myJson.booktree.node, data.property);
                      bookDetails.json = JSON.stringify(myJson.booktree);

                    }
                    else{
                      // console.log("else");

                      Array.prototype.push.apply(myJson.node, data.property);
                      bookDetails.json = JSON.stringify(myJson);
                    }
                  }
                  else{
                    if(myJson.booktree){
                      bookDetails.json = JSON.stringify(myJson.booktree);
                    }
                  }
                  // bookDetails.json = JSON.stringify(myJson);
                  const eBook = new Ebook(bookDetails);

                  if (subjectBookID.type === 'planningMode') {
                    // console.log('subjectBookID.type if');
                    this.selectedSubjectChapterQuiz(
                      eBook.chapters,
                      subjectBookID
                    );
                  } else {
                    // this.selectedSubjectChapter(eBook.chapters);
                  }
                }
              });
          }
        });
    } else {
      // console.log('subjectBookID else');
      // if (subjectBookID.type === 'planningMode') {
      //   console.log('subjectBookID.type else if');
      //   this.selectedSubjectChapterPlanning([], subjectBookID);
      // } else {
      //   this.selectedChapterError('Selected Subject no chapters');
      // }
    }
  }

  selectedChapterError(message) {
    this.selectedChapterErrorMessage.next(message);
  }
  selectedSubjectChapter(chapters: Chapter[]) {
    // console.log('selectedSubjectChapter');

    chapters.sort(function(a, b) {
      return a.sequence - b.sequence;
    });
    this.selectedSubjectChapterList.next(chapters);
  }
  selectedSubjectChapterPlanning(chapters: Chapter[], subjectBookID) {
    // console.log('subjectBookID-->>', subjectBookID);
    // console.log('chapters-->>', subjectBookID);
    chapters.sort(function(a, b) {
      return a.sequence - b.sequence;
    });
    const newData = {
      currentGradeId: subjectBookID.gradeLevel,
      currentSubjectId: subjectBookID.subject,
      cueerntChapterTopic: chapters
    };
    // console.log('subjectBookID newData ', newData);

    this.selectedSubjectChapterPlanningList.next(newData);
  }

  selectedSubjectChapterQuiz(chapters: Chapter[], subjectBookID) {
    // console.log('subjectBookID-->>', subjectBookID);
    // console.log('chapters-->>', subjectBookID);
    chapters.sort(function(a, b) {
      return a.sequence - b.sequence;
    });
    const newData = {
      currentGradeId: subjectBookID.gradeLevel,
      currentSubjectId: subjectBookID.subject,
      cueerntChapterTopic: chapters
    };
    // console.log('subjectBookID newData ', newData);

    this.selectedSubjectChapterQuizList.next(newData);
  }

  setAvailableEbooks(ebooks: Ebook[]) {
    this.availableEbooks = ebooks;

    return this.availableEbooksSubj.next(this.availableEbooks);
  }

  setEbookSelection(ebook: Ebook) {
    this._ebookSelection = ebook;
    this.ebookSelection.next(ebook);
  }

  setChapterSelection(chapter: Chapter, preselectedTopic: Topic) {
    // console.log('selectedChapter ', chapter);

    this._chapterSelection = chapter;

    this.chapterSelection.next(this._chapterSelection);

    if (chapter) {
      if (
        !CurriculumUtils.doAllTopicsHaveResources(this._chapterSelection.topics)
      ) {
        this.setResourcesLoadingError('');
        this.setResourcesFetching(true);
        this.getChapterWideResources().subscribe(response => {
          //console.log('getChapterWideResources--response', response);

          response.forEach(responseItem => {
            responseItem.forEach(
              (playlistObj: { id: string; playlistJson: string }) => {
                const matchingTopic = this._chapterSelection.topics.find(
                  topic => {
                    if (playlistObj.id) {
                      return playlistObj.id === topic.topicId;
                    }
                    //Tce : during ebook resource list fixess this else condition not required
                    //else if (
                    //   topic.resourceRequestType ===
                    //     ResourceRequestType.ASSETS &&
                    //   topic.doesPlaylistJsonMatchRequestedResources(
                    //     playlistObj.playlistJson
                    //   )
                    // ) {
                    //   return true;
                    // }
                    return false;
                  }
                );
                if (matchingTopic) {
                  matchingTopic.setResourcesFromPlaylistJson(
                    playlistObj.playlistJson
                  );
                }
              }
            );
          });
          this.setAvailableVideoResourcesForChapter();

          if (preselectedTopic) {
            this.setTopicSelection(preselectedTopic, 'setchapter');
            // setTimeout(() => {
            // this.setTopicSelection(preselectedTopic,"setchapter");

            // }, 500);
          }
        });
      } else {
        this.setAvailableVideoResourcesForChapter();
        if (preselectedTopic) {
          this.setTopicSelection(preselectedTopic, 'else if setchapter');
        }
      }
    }
  }

  getQuestionSource(resource) {
    const fileUrl =
      '/' +
      resource.encryptedPath +
      '/' +
      resource.questionId +
      '/' +
      'qb.json';
    return this.url + fileUrl;
  }

  httpGetCustomQuiz(questionFilepath: string) {
    this.http.get(questionFilepath).subscribe(response => {
      // console.log("current data --> ",response)
      this.cutomQuizIds.next(response);
      // console.log("current cutomQuizIds --> ",this.cutomQuizIds)
    });
  }

  httpGetQuestionSource(questionFilepath: string, data) {
    return this.http.get(questionFilepath).subscribe(
      response => {
        //console.log("response -------->",response)
        let responseData: any;
        responseData = response;
        let getCustom: any;
        getCustom = data;
        if (responseData.questionSourceType) {
          getCustom.questionSourceType = responseData.questionSourceType;
          getCustom.questionDetails = responseData.questionDetails;
          responseData = getCustom;
        }
        //this.questionDataSource.push(responseData);
        //console.log('curriculum-response', responseData);

        this.customQuestionDetails.push(responseData);
        //this.questionbankDataBroadcast.next(responseData);
      },
      error => {
        console.log('errors', error);
      }
    );
  }

  setTopicSelection(topic: Topic, calle?) {
    this.auxTopic = topic;
    // console.log('testing setTopicSelection -> topic again ', topic,"calle--",calle);
    this.toolbarService.setEditModeResoursePlayerList(false);
    this.currAddResourceFlagBroadcaster.next(true);
    this._topicSelection = topic;
    this.topicSelection.next(this._topicSelection);
    //console.log('setTopicSelection -> this._topicSelection', this._topicSelection);

    if (this._topicSelection) {
      this.getPlaylistResourcesForTopic(this._topicSelection);
      // console.log('testing this._topicSelection ->', this._topicSelection);
      this.subjectSelectorService.setMostRecentViewByChapterAndTopicIds(
        this._chapterSelection.chapterId,
        this._topicSelection.topicId
      );

      // this.checkAndGetCustomQuestionID(
      //   this.currentGrade,
      //   this.currentSubject,
      //   this._chapterSelection.chapterId,
      //   topic.topicId
      // );
      // let customQuestionIDArr = this.checkAndGetCustomQuestionID(this.currentGrade,this.currentSubject,this._chapterSelection.chapterId, topic.topicId)

      //  console.log("--customQuestionIDArr--THIRUMANI-->>",customQuestionIDArr)
      /*
      if(customQuestionIDArr.length > 0){
         for(let i = 0; i < topic.resources.length; i++){
           if(topic.resources[i].tcetype === "quiz"){
             for(let j = 0; j < customQuestionIDArr.length; j++){
               topic.resources[i].metaData.questionIds.push(customQuestionIDArr[j])
             }
           }
         }
      }*/
      this.appStateStorageService.setStorageKey(this._topicSelection.topicId);
      this.fullContentSelection.next({
        ebook: this._ebookSelection,
        chapter: this._chapterSelection,
        topic: topic
      });
      this.toolbarService.loadNewWhiteBoardAnnotations();
      //this.appStateStorageService.saveCurrentWhiteboardState();
    } else {
      this.appStateStorageService.setStorageKey(null);
      this.fullContentSelection.next(null);
    }
  }

  checkAndGetCustomQuestionID(gradeID, subjectID, chapterID, topicID) {
    let requestParams;
    let questionIdArr = [];
    this.customQuestionDetails = [];
    //requestParams ='?gradeId='+this.currentGrade+'&subjectId='+this.currentSubject+'&chapterId='+this._chapterSelection.chapterId+'&tpId='+this._topicSelection.topicId+'&source=custom';
    requestParams =
      '?gradeId=' +
      gradeID +
      '&subjectId=' +
      subjectID +
      '&chapterId=' +
      chapterID +
      '&tpId=' +
      topicID +
      '&source=custom';
    // requestParams ='?gradeId=grd-74cc96e3-3ba0-44af-b60e-722e44cdefdc&subjectId=sub-ae8fce9e-0bfc-4c9c-9562-9d5823788a6b&chapterId=cchp-430c71fd-82a2-4e97-9088-0deef3f57192-tcdeli-lxplm&source=' +value;
    this.http
      .get<any>(
        this.requestApiService.getUrl('getQuestionBank') + requestParams
      )
      .subscribe(
        data => {
          if (data) {
            if (data.length > 0) {
              for (let i = 0; i < data.length; i++) {
                questionIdArr.push(data[i].questionId);
                const questionFilepath = this.getQuestionSource(data[i]);
                this.httpGetQuestionSource(questionFilepath, data[i]);
              }
              // this.customQuestionIds = questionIdArr;
              // console.log('questionID--->', questionIdArr);
            }
            this.setQuestionInAssetJson();
          }
        },
        err => {
          console.log('err-->>', err);
        }
      );
    //return questionIdArr;
  }

  setQuestionInAssetJson() {
    // console.log("QuestionIs inside",this.customQuestionIds)
    // this.http
    // .post(
    //   this.requestApiService.getUrl('dynamicCustomResource')
  }

  private setAvailableChapters(chapters: Chapter[]) {
    chapters.sort(function(a, b) {
      return a.sequence - b.sequence;
    });
    this._availableChapters = chapters;

    this.availableChapters.next(chapters);
  }

  public setAvailableResources(resources: Resource[], calle?) {
    resources.forEach(element => {
      // if(element.tpId.charAt(0)==='c'){
      //   if(!element.show){
      //     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
      //     element.filterStatus = true;
      //     element.visibility = 1;
      //     element.show = true;
      //   }
      // }
    });
    //  console.log("setAvailableResources -> resources", resources)
    // console.log("setAvailableResources -> resources -> calle", calle)
    this.contentEditorService.setCurrentResources(resources);

    this._availableResources = resources;
    this.resetResourceFilters(resources, 'test');
    this.availableResources.next(this._availableResources);
  }

  resetAllSelections(skipEbooks = false) {
    if (!skipEbooks) {
      this.setAvailableEbooks([]);
    }

    this.setAvailableChapters([]);
    this.setAvailableResources([]);
    this.setChapterSelection(null, null);
    this.setTopicSelection(null, 'resetAllSelections');
  }

  setLessonPlanResource(lessonPlan: LessonPlanResource) {
    this.lessonPlanSelection.next(lessonPlan);
  }

  /*
  The following code is for decoding the book.xml
  */

  generateBookData(data: ApiEbook) {
    //  console.log('generateBookData -> data', data);
    if (!data) {
      this.resetAllSelections();
    } else {
      try {
        
        const eBook = new Ebook(data);
        console.log('final ebook', eBook);
        if (eBook.eBookBasePath) {
          this.setAvailableEbooks([eBook]);
        } else {
          this.setAvailableEbooks([]);
        }

        //console.log('generateBookData ->  eBook.chapters', eBook.chapters);
        //console.log("generateBookData -> eBook.chapters", eBook.chapters)
        //eBook.chapters.concat(customData.custom)
        this.setAvailableChapters(eBook.chapters);
        const preselectedChapterAndTopic = this.determinePreselectedChapterAndTopic();
        this.setChapterSelection(
          preselectedChapterAndTopic.chapter,
          preselectedChapterAndTopic.topic
        );
      } catch (error) {
        this.setEbookFetchError('Error parsing class data');
        this.resetAllSelections();
      }
    }
  }

  determinePreselectedChapterAndTopic() {
    const returnObject = {
      chapter: this._availableChapters[0],
      topic: this._availableChapters[0].topics[0]
    };
    if (this.subjectSelectorService.selectedRecentView) {
      const { selectedRecentView } = this.subjectSelectorService;
      const matchedChapter = this._availableChapters.find(
        chapter => selectedRecentView.chapterId === chapter.chapterId
      );
      if (matchedChapter) {
        returnObject.chapter = matchedChapter;
        const matchedTopicIndex = matchedChapter.topics.findIndex(
          topic => selectedRecentView.topicId === topic.topicId
        );
        if (matchedTopicIndex !== -1) {
          returnObject.topic = matchedChapter.topics[matchedTopicIndex];
        } else {
          returnObject.topic = matchedChapter.topics[0];
        }
        this.subjectSelectorService.selectedRecentView = null;
      }
      return returnObject;
    }
    if (!this.recentViewsData || !this.recentViewsData.length) {
      return returnObject;
    }
    for (const chapter of this._availableChapters) {
      const matchedRecentView = this.recentViewsData.find(
        recentView => recentView.chapterId === chapter.chapterId
      );
      if (matchedRecentView) {
        returnObject.chapter = chapter;
        for (const topic of chapter.topics) {
          if (topic.topicId === matchedRecentView.topicId) {
            returnObject.topic = topic;
            break;
          }
        }
        break;
      }
    }
    return returnObject;
  }

  setResourcesFetching(isFetching: boolean) {
    if (this._resourcesFetching !== isFetching) {
      this._resourcesFetching = isFetching;
      this.resourcesFetching.next(this._resourcesFetching);
    }
  }

  setResourcesLoadingError(errorMessage: string) {
    this.resourcesLoadingError.next(errorMessage);
  }

  /* Get the playlist resources */
  getPlaylistResourcesForTopic(topic: Topic) {
    let resources = topic.resources;
    // console.log(
    //   'testing getPlaylistResourcesForTopic -> topic.resources',
    //   resources
    // );
    if (this._resourceRequest) {
      this._resourceRequest.unsubscribe();
    }
    this.setResourcesLoadingError('');
    this.setResourcesFetching(false);

    if (topic.resources.length > 0) {
      // console.log('testing length resources------------>',resources);
      this.setAvailableResources(
        topic.resources,
        'getPlaylistResourcesForTopic'
      );
      //console.log("getPlaylistResourcesForTopic -> topic.resources", topic.resources)
    } else {
      let apiPath;
      if (topic.resourceRequestType === ResourceRequestType.TQ) {
        apiPath = this.requestApiService
          .getUrl('tqResource')
          .replace('@ids@', topic.resourceRequestIds.join(','));
      }
      if (topic.topicId.charAt(0) === 'c') {
        apiPath = this.requestApiService
          .getUrl('shareCustomResource')
          .replace(
            '@ids@',
            'gradeId=' +
              this.currentGrade +
              '&&subjectId=' +
              this.currentSubject +
              '&&ids=' +
              this._topicSelection.topicId
          );
        //console.log("SATYAJIT--2",apiPath)
      } else if (
        topic.resourceRequestType === ResourceRequestType.TP &&
        this.currentGrade &&
        this.currentSubject
      ) {
        apiPath = this.requestApiService
          .getUrl('tpResource')
          .replace(
            '@ids@',
            'gradeId=' +
              this.currentGrade +
              '&&subjectId=' +
              this.currentSubject +
              '&&ids=' +
              topic.resourceRequestIds.join(',')
          );
      } else {
        apiPath = this.requestApiService
          .getUrl('assetResource')
          .replace('@ids@', topic.resourceRequestIds.join(','));
      }
      // apiPath = apiPath.replace('@ids@', topic.resourceRequestIds.join(','));

      // console.log("SATYAJIT",apiPath)
      this._resourceRequest = this.http
        .get<any>(apiPath)
        .pipe(
          catchError(err => {
            this.setResourcesFetching(false);

            this.setResourcesLoadingError('Error Fetching Resources for Topic');
            throw err;
          })
        )
        .subscribe(response => {
          this.setResourcesFetching(false);
          this._resourceRequest = null;
          // console.log("testing-1---------------->",response)
          // console.log("testing-2---------------->",topic.resources)
          // console.log(" testing-3-length----------------->",response.length)

          topic.setResourcesFromApiResponse(response);
          if (topic.resources.length == 0) {
            this.setResourcesLoadingError('No Resources Available for Topic');
          }

          this.setAvailableResources(topic.resources, 'assetResource-->API');

          if (response) {
            // console.log(" testing-4-topic---------------->",topic.resources)
          }
        });
    }
  }

  removeDuplicates(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
      return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  private getChapterWideResources(): Observable<any> {
    let tqTopicIds = [];
    let tpTopicIds = [];
    const vtpTopicIdGroups = [];
    let returnValue: Observable<any> = null;
    if (this._chapterSelection) {
      for (const topic of this._chapterSelection.topics) {
        if (topic.resourceRequestType === ResourceRequestType.TQ) {
          tqTopicIds = tqTopicIds.concat(topic.resourceRequestIds);
        } else if (
          topic.resourceRequestType === ResourceRequestType.TP &&
          this.currentGrade &&
          this.currentSubject
        ) {
          // TCE: new addition
          tpTopicIds = tpTopicIds.concat(topic.resourceRequestIds);
          // TCE: new addition
        } else {
          vtpTopicIdGroups.push(topic.resourceRequestIds);
        }
      }

      const resourceRequests = [];
      if (vtpTopicIdGroups.length > 0) {
        vtpTopicIdGroups.forEach(topicIdGroup => {
          const vtpRequestPath = this.requestApiService
            .getUrl('assetResource')
            .replace('@ids@', topicIdGroup.join(','));
          resourceRequests.push(this.http.get(vtpRequestPath));
        });
      }

      // TCE: new addition revised api call for TP ids
      if (tpTopicIds.length > 0) {
        const tpRequestPath = this.requestApiService
          .getUrl('tpResource')
          .replace(
            '@ids@',
            'gradeId=' +
              this.currentGrade +
              '&&subjectId=' +
              this.currentSubject +
              '&&ids=' +
              tpTopicIds.join('&&ids=')
          );
        resourceRequests.push(this.http.get(tpRequestPath));
      }
      if (tqTopicIds.length > 0) {
        const tqRequestPath = this.requestApiService
          .getUrl('tqResource')
          .replace('@ids@', tqTopicIds.join(','));
        resourceRequests.push(this.http.get(tqRequestPath));
      }

      if (resourceRequests.length > 0) {
        returnValue = forkJoin(resourceRequests);
      }
    }
    return returnValue;
  }
  setAvailableVideoResourcesForChapter() {
    this._availableVideos = [];
    if (this._chapterSelection) {
      this._chapterSelection.topics.forEach(topic => {
        if (topic.resources) {
          this._availableVideos = this._availableVideos.concat(
            topic.resources.filter(resource => {
              return (
                resource.resourceType &&
                resource.resourceType === ResourceType.TCEVIDEO
              );
            })
          );
        }
      });
    }
    this.availableVideos.next(this._availableVideos);
  }

  setAppliedResourceFilters() {
    this.appliedResourceFilters.next(
      this._resourceFilter.filter(
        (resourceFilter: FilterMenu) => resourceFilter.selected
      )
    );
    // console.log("setAppliedResourceFilters------>",this._resourceFilter)
  }

  async setUserPreferViewResourceFilters(list: FilterMenu) {
    //console.log('curriculam list', list);
    this._resourceFilter.forEach((resourceFilter: FilterMenu) => {
      if (resourceFilter.name === list.name) {
        // console.log('resourceFilter.name', resourceFilter);
        resourceFilter.selected = list.selected;
        //  console.log('resourceFilter.selected',resourceFilter.name, resourceFilter.selected);
      }
    });
    this.setAppliedResourceFilters();
  }
  toggleResourceFilter(passedResourceFilter: FilterMenu) {
    this._resourceFilter.forEach((resourceFilter: FilterMenu) => {
      if (resourceFilter.name === passedResourceFilter.name) {
        //console.log("resourceFilter.name",resourceFilter)
        resourceFilter.selected = !resourceFilter.selected;
      }
    });
    this.setAppliedResourceFilters();
  }

  toggleAllResourceFilters(toggleOn: boolean) {
    this._resourceFilter.forEach((resourceFilter: FilterMenu) => {
      if (resourceFilter.resourceCount > 0) {
        resourceFilter.selected = toggleOn;
      } else {
        resourceFilter.selected = false;
      }
    });
    this.setAppliedResourceFilters();
  }

  public resetResourceFilters(resources: Resource[], value) {
    // console.log("resources reset",resources,value)
    this._resourceFilter.forEach((type: FilterMenu) => {
      type.resourceCount = 0;
      type.selected = false;
    });
    resources.forEach((resource: Resource) => {
      this._resourceFilter.forEach((filterType: FilterMenu) => {
        if (resource.visibility) {
          if (filterType.type.includes(resource.resourceType)) {
            filterType.resourceCount += 1;
          }
        }
      });
    });
    this.filteredMenu.next(this._resourceFilter);
    setTimeout(() => {
      this.setAppliedResourceFilters();
    }, 500);
  }

  UpdateResourceList(collectionRecourceData) {
    // console.log('curriculam resources data', collectionRecourceData);
    let newdata = {
      gradeId: collectionRecourceData.gradeId,
      subjectId: collectionRecourceData.subjectId,
      playlistJson: collectionRecourceData.playlistJson.playlistJson,
      tpId: collectionRecourceData.playlistJson.id
    };
    this.http
      .post<any>(this.requestApiService.getUrl('updateResource'), newdata)
      .subscribe(
        status => {
          const tpRequestPath = this.requestApiService
            .getUrl('tpResource')
            .replace(
              '@ids@',
              'gradeId=' +
                collectionRecourceData.gradeId +
                '&&subjectId=' +
                collectionRecourceData.subjectId +
                '&&ids=' +
                collectionRecourceData.playlistJson.id
            );
          if (tpRequestPath) {
            //this.getUpdateResourceList(tpRequestPath);
          }
        },
        (error: HttpErrorResponse) => {
          let errormessage: any;
          if (error && error.error['errorMessage']) {
            errormessage = error.error['errorMessage'];
          } else {
            errormessage = 'updateResourceError';
          }

          //this.toastrService.error(errormessage);
        }
      );
  }

  resetResourceList(collectionRecourceData) {
    let selectedChapter: any;
    let selectedTopic: any;
    let selectedTopicIndex: any;
    let selectedChapterIndex: any;
    let currentChapters: Chapter[];
    const newURL = this.requestApiService
      .getUrl('tpResource')
      .replace(
        '@ids@',
        'tpId=' +
          collectionRecourceData.tpId +
          '&&gradeId=' +
          collectionRecourceData.gradeId +
          '&&subjectId=' +
          collectionRecourceData.subjectId
      );
    this.availableChapters.subscribe(chapter => {
      currentChapters = chapter;
    });
    if (currentChapters) {
      this.fullContentSelection.subscribe((content: any) => {
        if (content) {
          selectedChapter = content.chapter;
          selectedTopic = content.topic;
          selectedChapterIndex = currentChapters.findIndex(
            c => c.chapterId === content.chapter.chapterId
          );
          selectedTopicIndex = selectedChapter.topics.findIndex(
            t => t.topicId === content.topic.topicId
          );
        }
      });
      if (newURL) {
        this.http.delete<any>(newURL, {}).subscribe(
          responce => {
            if (responce) {
              //this.cdn1.detectChanges();
              const tpRequestPath = this.requestApiService
                .getUrl('tpResource')
                .replace(
                  '@ids@',
                  'gradeId=' +
                    collectionRecourceData.gradeId +
                    '&&subjectId=' +
                    collectionRecourceData.subjectId +
                    '&&ids=' +
                    collectionRecourceData.tpId
                );
              if (tpRequestPath) {
                this.getUpdateResourceList(tpRequestPath);
              }
            }
          },
          (error: HttpErrorResponse) => {
            let errormessage: any;
            if (error && error.error['errorMessage']) {
              errormessage = error.error['errorMessage'];
            } else {
              errormessage = 'resetResourceError';
            }

            //this.toastrService.error(errormessage);
          }
        );
      }
    }
  }

  getUpdateResourceList(path) {
    this.http.get(path).subscribe(
      responce => {
        if (responce && responce['length'] > 0) {
          this._topicSelection.setResourcesFromPlaylistJson(
            responce[0].playlistJson,
            false
          );
          if (
            this._topicSelection.resources &&
            this._topicSelection.resources.length > 0
          ) {
            this.availableResources.next(this._topicSelection.resources);
            this.setTopicSelection(this._topicSelection, 'update');
            // this.commonService.setupdateResourcelist(
            //   this._topicSelection.resources
            // );
          }
        }
      },
      (error: HttpErrorResponse) => {
        let errormessage: any;
        if (error && error.error['errorMessage']) {
          errormessage = error.error['errorMessage'];
        } else {
          errormessage = 'getUpdateResourceError';
        }

        //this.toastrService.error(errormessage);
      }
    );
  }

  captureLogUsageReport(body: {
    type: string;
    resourceId: string;
    launchedFrom: string;
    duration: number;
    subjectId: string;
    gradeId: string;
  }) {
    return this.http.post(this.requestApiService.getUrl('usageLog'), body);
  }

  getCustomSharedData(resource) {
    //  console.log("A -->",resource)

    if (this._topicSelection && this._availableResources) {
      // console.log("B -->")

      const tpRequestPath = this.requestApiService
        .getUrl('shareCustomResource')
        .replace(
          '@ids@',
          'gradeId=' +
            this.currentGrade +
            '&&subjectId=' +
            this.currentSubject +
            '&&ids=' +
            this._topicSelection.topicId
        );
      if (tpRequestPath) {
        // console.log("C -->")

        this.http.get(tpRequestPath).subscribe(response => {
          if (response) {
            // console.log("D -->")

            if (response && response['length'] > 0) {
              // console.log("E -> response", response)
              let getcustomwithTCE = this._topicSelection.setCustomResourcePlaylistJson(
                response[0].playlistJson,
                resource
              );
              //console.log("----getcustomwithTCE----",getcustomwithTCE)
              if (!this.checkCustomResourceFlag) {
                this.availableCustomResources.next(true);
                this.checkCustomResourceFlag = true;
              }
              if (getcustomwithTCE) {
                // console.log(
                //   'getCustomSharedData -> getcustomwithTCE',
                //   getcustomwithTCE
                // );
                this.setAvailableResources(
                  getcustomwithTCE,
                  'getcustomwithTCE'
                );
                // let updateCL = {

                //     gradeId: this.currentGrade,
                //     subjectId:this.currentSubject,
                //     playlistJson: collectionRecourceData.playlistJson.playlistJson,
                //     tpId: this._topicSelection.topicId

                // }
              }
            }
          }
        });
      }
    }
  }

  getCustomSharedDataPlanmode(resource) {
    if (resource && this._availableResources) {
      const tpRequestPath = this.requestApiService
        .getUrl('shareCustomResource')
        .replace(
          '@ids@',
          'gradeId=' +
            this.currentGrade +
            '&&subjectId=' +
            this.currentSubject +
            '&&ids=' +
            resource.tpId
        );
      if (tpRequestPath) {
        this.http.get(tpRequestPath).subscribe(response => {
          if (response) {
            if (response && response['length'] > 0) {
              // console.log("getCustomSharedData -> response", response)
              let getcustomwithTCE = this._topicSelection.setCustomResourcePlaylistJson(
                response[0].playlistJson,
                resource
              );
              //console.log("----getcustomwithTCE----",getcustomwithTCE)
              if (!this.checkCustomResourceFlag) {
                this.availableCustomResources.next(true);
                this.checkCustomResourceFlag = true;
              }
              if (getcustomwithTCE) {
                // console.log(
                //   'getCustomSharedData -> getcustomwithTCE',
                //   getcustomwithTCE
                // );
                this.setAvailableResources(
                  getcustomwithTCE,
                  'getcustomwithTCE-planmode'
                );
                // let updateCL = {

                //     gradeId: this.currentGrade,
                //     subjectId:this.currentSubject,
                //     playlistJson: collectionRecourceData.playlistJson.playlistJson,
                //     tpId: this._topicSelection.topicId

                // }
              }
            }
          }
        });
      }
    }
  }
}
