import { Injectable, Inject } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import {
  EnvironmentConfig,
  EnvironmentApiUrl,
  EnvironmentApiBaseUrlType
} from '../models/environment/environment.interface';

@Injectable({
  providedIn: 'root'
})
export class RequestApiService {
  public userLoggedIn = false;
  //below is the apiversion
  public versionId = '1';
  //stateType is the state whether it is public/private.O=>public,1=>private
  public stateType = '0';
  private privateType = '1';

  constructor(@Inject('environment') private environment: EnvironmentConfig) {}

  getUrl(urlName: string): string {
    //console.log('urlName-->> ', urlName);
    const { api } = this.environment;

    //console.log('this.environment-->> ', this.environment);
    const urlObject: EnvironmentApiUrl = api[urlName];
    // console.log('api[urlName]-->> ', api[urlName]);
    //console.log('urlObject-->> ', urlObject);

    //console.log('api.baseUrls-->> ', api.baseUrls);
    const baseUrl = urlObject.baseUrlType
      ? api.baseUrls[urlObject.baseUrlType]
      : api.baseUrls[EnvironmentApiBaseUrlType.GENERAL];
    //const baseUrl = api.baseUrls[urlObject.baseUrlType];
    //console.log('baseUrl-->> ', baseUrl);
    const relativeUrl = urlObject.url
      .replace('@apiVersion@', this.versionId)
      .replace('@currentRefreshToken@', sessionStorage.getItem('refreshToken'));
    return baseUrl + relativeUrl;
  }
}
