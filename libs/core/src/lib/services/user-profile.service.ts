import { Injectable } from '@angular/core';
import { Subject, ReplaySubject, BehaviorSubject } from 'rxjs';
import { CurriculumPlaylistService } from './curriculum-playlist.service';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { RequestApiService } from './request-api.service';
import { map, filter, take } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { config } from 'process';
@Injectable({
  providedIn: 'root'
})
export class UserProfileService {
  private userPrefereViewListBroadcast = new Subject();
  public userPrefereViewListBroadcast$ = this.userPrefereViewListBroadcast.asObservable();

  private userPreferList = new Subject();
  public userPreferList$ = this.userPreferList.asObservable();
  public prefereData;

  public userProfileData;
  public userData;
  public userProfileImg;
  public userRole;
  public configUserData;

  public userProfileAPIData = new Subject();
  public userProfileAPIData$ = this.userProfileAPIData.asObservable();

  private _assignedSubjects = [];

  constructor(
    private curriculumPlaylistService: CurriculumPlaylistService,
    private http: HttpClient,
    private requestApiService: RequestApiService
  ) {
    let obj;
    this.getJSON().subscribe(data => {
      this.configUserData = data;
      this.userProfileImg = data.user_img;
      // console.log('configUserData', this.userProfileImg);
    });
  }

  get assignedSubjects(){
    return this._assignedSubjects;
  }

  set assignedSubjects(assignedSubjects){
    this._assignedSubjects = assignedSubjects;
  }

  getJSON(): Observable<any> {
    return this.http.get('assets/mockdata/userProfile/userdata.json');
  }

  setUserPreferList(list){
    this.userPreferList.next(list)
  }
  setUserPrefereViewListBroadcast(list) {
    let idParams = {
      settings: { addInfo1: JSON.stringify(list) }
    };

    this.http
      .put<any>(this.requestApiService.getUrl('getUserProfile'), idParams)
      .subscribe(
        data => {
          localStorage.setItem('userSettings', JSON.stringify(list));
          this.userPrefereViewListBroadcast.next(list);
        },
        err => {}
      );
  }

  getUserPrefereViewListBroadcast() {
    const requestParams = '?columns=addInfo1';
    this.http
      .get<any>(this.requestApiService.getUrl('getUserProfile') + requestParams)
      .subscribe(
        data => {
          this.prefereData = JSON.parse(data.userSettings.addInfo1);
        },
        err => {}
      );
  }

  updateUserProfileDetails(body){
    return this.http.put<any>(this.requestApiService.getUrl('getUserProfile'), body);
  }

  getUserProfileAPIData() {
    const requestParams = '?columns=title&columns=userName&columns=subjects&columns=role&columns=addInfo1';
    //const requestParams ='?columns=addInfo1&columns=lastName&columns=firstName&columns=middleName&columns=subjects&columns=title';
    this.http
      .get<any>(this.requestApiService.getUrl('getUserProfile') + requestParams)
      .subscribe(
        data => {
          
          this.userProfileAPIData.next(data);
          this.userProfileData = data;
          this.userData = data.user;
          this.userRole = data.role.name;
          this.userProfileImg = this.userData.user_img
            ? this.userData.user_img
            : this.configUserData.user_img;
          //console.log('this.userData.user_img', this.userProfileImg);
          //console.log('configUserData', this.configUserData);
          // test with local data
          //console.log("profile service this.userData---->",this.userData)
          // console.log("userr-------------------",this.userRole);

          if (data.user) {
      
            for (const [key, value] of Object.entries(data.user)) {
              if (value === 'Not Set' || value === null || value === '') {
                Object.defineProperty(this.userData, key, {
                  value: ''
                });
              }
              else{
                Object.defineProperty(this.userData, key, {
                  value: value
                });
              }
            }
           
          }

          if (data.userSettings.addInfo1) {
            //console.log("if userSettings")
            this.prefereData = JSON.parse(data.userSettings.addInfo1);
          } else {
            // console.log("else userSettings")
            // let userSettingData = localStorage.getItem('userSettings');
            // this.prefereData = userSettingData;
          }

          if(data.userSettings?.subjects){
            this.assignedSubjects = JSON.parse(data.userSettings?.subjects);
          }
          
        },
        err => {}
      );
  }

  setUserPrefereViewFilter(list) {
    this.curriculumPlaylistService.setUserPreferViewResourceFilters(list);
  }

  getSubjectList() {
    let params = {
      "start": "0",
      "length": "-1",
      "filterTerms": [
        `schoolId:tcdeli-lxplm`
      ]
    };
    return this.http.get<any>(this.requestApiService.getUrl('subjectList'), {
      params: params
    });
  }


}
