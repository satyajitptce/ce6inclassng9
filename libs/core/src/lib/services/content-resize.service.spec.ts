import { TestBed } from '@angular/core/testing';

import { ContentResizeService } from './content-resize.service';

describe('ContentResizeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContentResizeService = TestBed.get(ContentResizeService);
    expect(service).toBeTruthy();
  });
});
