import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//import { JwPaginationComponent } from 'jw-angular-pagination';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { FlyoutComponent } from './components/flyout/flyout.component';
import { BadgeComponent } from './components/badge/badge.component';
import { ScrollComponent } from './components/scroll/scroll.component';
import { ClickedOutsideDirective } from './directives/click-outside.directive';
import { TabBoxComponent } from './components/tabs/tab-box/tab-box.component';
import { TabContentComponent } from './components/tabs/tab-content/tab-content.component';
import { TabSearchContentComponent } from './components/tabs/tab-search-content/tab-search-content.component';
import { LongPressDirective } from './directives/long-press/long-press.directive';
import { PlayerAbstractComponent } from './components/player-abstract/player-abstract.component';
import { PaginationControlsComponent } from './components/pagination-controls/pagination-controls.component';
import { UtilityBarComponent } from './components/utility-bar/utility-bar.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { ErrorLayoverComponent } from './components/error-layover/error-layover.component';
import { KeyboardComponent } from './components/keyboard/keyboard.component';
import { SlideButtonComponent } from './components/slide-button/slide-button.component';
import { NumberRestrictDirective } from './directives/number-restrict.directive';

@NgModule({
  imports: [CommonModule, AngularSvgIconModule],

  declarations: [
    SpinnerComponent,
    MenuListComponent,
    FlyoutComponent,
    BadgeComponent,
    ScrollComponent,
    ClickedOutsideDirective,
    TabBoxComponent,
    TabContentComponent,
    TabSearchContentComponent,
    LongPressDirective,
    PlayerAbstractComponent,
    PaginationControlsComponent,
    UtilityBarComponent,
    ErrorLayoverComponent,
    KeyboardComponent,
    SlideButtonComponent,
    NumberRestrictDirective
  ],
  exports: [
    SpinnerComponent,
    MenuListComponent,
    FlyoutComponent,
    BadgeComponent,
    ScrollComponent,
    ClickedOutsideDirective,
    TabBoxComponent,
    TabContentComponent,
    TabSearchContentComponent,
    LongPressDirective,
    PlayerAbstractComponent,
    ErrorLayoverComponent,
    PaginationControlsComponent,
    //JwPaginationComponent,
    UtilityBarComponent,
    KeyboardComponent,
    SlideButtonComponent,
    NumberRestrictDirective
  ],
  entryComponents: [ErrorLayoverComponent]
})
export class CoreModule {}
