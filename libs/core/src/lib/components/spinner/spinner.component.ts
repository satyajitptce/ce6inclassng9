import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'tce-loading-layover',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() size: 'small' | 'medium' | 'large' = 'large';
  @Input() message: any;
  @HostBinding('class.no-background') @Input() noBackgroundColor = false;
  @HostBinding('class.no-overlay') @Input() noOverlay = false;
  @HostBinding('class.dark') @Input() darkMode = false;

  constructor() {}

  ngOnInit() {}
}
