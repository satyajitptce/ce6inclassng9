import {
  Component,
  OnInit,
  ViewEncapsulation,
  HostListener
} from '@angular/core';
import Keyboard from 'simple-keyboard';
import { KeyboardState, KeyboardTheme } from '../../enums/keyboard.enum';
import { KeyboardService } from '../../services/keyboard.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'tce-keyboard',
  templateUrl: './keyboard.component.html',
  styleUrls: ['./keyboard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class KeyboardComponent implements OnInit {
  keyboard: Keyboard = null;
  isKeyboardActive = false;
  selectedInputId = '';
  currentTarget;
  isNumericKeyboard = false;

  constructor(
    private keyboardService: KeyboardService,
    private toastrService: ToastrService
  ) {}

  @HostListener('click', ['$event'])
  clickHandler(event: Event) {
    event.stopPropagation();
  }

  ngOnInit() {
    this.keyboardService.keyboardDisplayState$.subscribe(obj => {
      const { state, theme, ...options } = obj;

      if (obj.state === KeyboardState.OPEN) {
        this.isNumericKeyboard = false;
        this.keyboard = new Keyboard({
          onChange: input => this.onChange(input),
          onKeyPress: button => this.onKeyPress(button),
          onKeyReleased: button => this.onKeyReleased(button),
          tabCharOnTab: false,
          mergeDisplay: true,
          display: { '{enter}': 'enter' },
          ...options
        });

        if (obj.theme === KeyboardTheme.NUMERIC) {
          this.keyboard.setOptions({
            layout: {
              default: ['1 2 3', '4 5 6', '7 8 9', '{bksp} 0 {enter}']
            },
            theme: 'hg-theme-default numeric-theme'
          });

          this.isNumericKeyboard = true;
        }

        this.keyboard.setInput(this.currentTarget.value);

        this.isKeyboardActive = true;
      } else {
        this.isKeyboardActive = false;
      }
    });

    this.keyboardService.currentInputElement$.subscribe(elem => {
      this.currentTarget = elem;
    });

    this.keyboardService.currentInputValue$.subscribe(val => {
      this.keyboard.setInput(val);
    });
  }

  onChange = (input: string) => {
    const highlightedText = this.getSelectionText();
    if (highlightedText) {
      this.keyboard.setInput(input.replace(highlightedText, ''));
      this.currentTarget.value = input.replace(highlightedText, '');
    } else {
      this.currentTarget.value = input;
    }
  };

  // onInputChange = (event: any) => {
  //   console.log('input change')
  //   this.keyboard.setInput(event.target.value);
  // };

  onKeyPress(button: string) {}

  onKeyReleased(button: string) {
    //we trigger the keyboard event in here cause onKeyPress also listens
    //for the event and we get into a loop
    if (button === '{tab}') {
      this.tabNextField();
      return;
    }
    if (button === '{shift}' || button === '{lock}') {
      this.handleShift();
      return;
    }
    if (button === '{enter}') {
      this.keyboardService.enterKeyPress(this.keyboard.getInput());
      return;
    }

    const event = new KeyboardEvent('keyup', {
      key: button,
      keyCode: button.charCodeAt(0)
    } as KeyboardEventInit);
    this.currentTarget.dispatchEvent(event);
    return;
  }

  handleShift() {
    const currentLayout = this.keyboard.options.layoutName;
    const shiftToggle = currentLayout === 'default' ? 'shift' : 'default';

    this.keyboard.setOptions({
      layoutName: shiftToggle
    });
  }

  closeKeyboard() {
    this.keyboardService.closeKeyboard();
  }

  tabNextField() {
    const allElements = document.querySelectorAll('input, button');
    let inputElem;
    let index;

    const currentIndex = Array.prototype.slice
      .call(allElements)
      .findIndex(el => this.currentTarget.isEqualNode(el));

    index = !allElements[currentIndex + 1] ? 0 : currentIndex + 1;
    inputElem = allElements[index] as HTMLInputElement;
    inputElem.focus();
    this.currentTarget = inputElem;
    return;
  }

  getSelectionText() {
    let text = '';
    if (typeof window.getSelection !== 'undefined') {
      const sel = window.getSelection();
      text = sel.toString();
    }
    return text;
  }

  toggleKeyboardUserState() {
    this.keyboardService.toggleUserState();
    this.toastrService.warning(
      'You can turn the virtual keyboard back on by clicking your user avatar in the toolbar, and selecting the "Virtual Keyboard" option',
      'Virtual Keyboard Disabled'
    );
    this.closeKeyboard();
  }
}
