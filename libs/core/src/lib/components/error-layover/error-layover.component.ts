import {
  Component,
  OnInit,
  Input,
  ChangeDetectionStrategy,
  HostBinding
} from '@angular/core';

@Component({
  selector: 'tce-error-layover',
  templateUrl: './error-layover.component.html',
  styleUrls: ['./error-layover.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ErrorLayoverComponent implements OnInit {
  @Input() message: string;
  @Input() size: 'small' | 'medium' | 'large' = 'large';
  @HostBinding('class.no-background') @Input() noBackgroundColor = false;

  constructor() {}

  ngOnInit() {}
}
