import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ErrorLayoverComponent } from './error-layover.component';

describe('ErrorLayoverComponent', () => {
  let component: ErrorLayoverComponent;
  let fixture: ComponentFixture<ErrorLayoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ErrorLayoverComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ErrorLayoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
