import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlayerAbstractComponent } from './player-abstract.component';

describe('PlayerAbstractComponent', () => {
  let component: PlayerAbstractComponent;
  let fixture: ComponentFixture<PlayerAbstractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PlayerAbstractComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlayerAbstractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
