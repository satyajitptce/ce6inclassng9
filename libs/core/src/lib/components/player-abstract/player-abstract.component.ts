import {
  AnimationEvent,
  AnimationTriggerMetadata,
  transition,
  trigger,
  useAnimation
} from '@angular/animations';
import {
  Component,
  EventEmitter,
  HostBinding,
  HostListener,
  Input,
  Output
} from '@angular/core';
import { Resource } from '../../models/curriculum/curriculum.interface';
import { TCEAnimations } from '../../utils/animations';

export interface PlayerAbstractInterface {
  beginLoadingResource(): void;
}

export const PlayerAbstractAnimations: AnimationTriggerMetadata[] = [
  trigger('enterLeave', [
    transition('void -> *', useAnimation(TCEAnimations.bounceIn)),
    transition('* -> void', useAnimation(TCEAnimations.bounceOut))
  ])
];

@Component({
  template: ''
})
export class PlayerAbstractComponent {
  [beginLoadingResource: string]: any;

  @Output() playerCloseEmitter = new EventEmitter<any>();

  @Input() resource: Resource;

  isLoading = true;

  errorMessage = '';

  @HostBinding('@enterLeave') animate = true;

  @HostListener('@enterLeave.done', ['$event']) animationEnd = (
    event: AnimationEvent
  ) => {
    if (event.fromState === 'void') {
      this.beginLoadingResource();
    }
  };

  constructor() {}

  onBeginLoadingResource() {
    this.errorMessage = '';
    this.isLoading = true;
  }

  onEndLoadingResource(): void {
    this.isLoading = false;
  }

  onResourceLoadError(message = 'Unable to Load Resource!') {
    this.isLoading = false;
    this.errorMessage = message;
  }

  playerActivated() {
    //nothing for now
  }
}
