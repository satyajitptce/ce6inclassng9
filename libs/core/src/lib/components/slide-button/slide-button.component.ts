import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  HostListener
} from '@angular/core';

@Component({
  selector: 'tce-slide-button',
  templateUrl: './slide-button.component.html',
  styleUrls: ['./slide-button.component.scss']
})
export class SlideButtonComponent implements OnInit {
  @Input('toggleBtnChecked') toggleBtnChecked: any = false;
  @Input('labelname') labelname: any = '';
  @Output() toggle = new EventEmitter();

  @HostListener('click', ['$event'])
  onClick(evnt: Event) {
    evnt.stopPropagation();
    if ((<HTMLElement>evnt.target).tagName === 'INPUT') {
      this.toggle.emit(evnt);
    }
  }
  @HostListener('mousedown', ['$event'])
  @HostListener('touchstart', ['$event'])
  onMouseDownOrTouchStart(evnt: Event) {
    //evnt.preventDefault();
    evnt.stopPropagation();
  }

  constructor() {}

  ngOnInit() {}
}
