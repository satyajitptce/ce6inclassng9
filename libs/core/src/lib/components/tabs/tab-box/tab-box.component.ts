import {
  Component,
  AfterContentInit,
  ContentChildren,
  EventEmitter,
  QueryList,
  Input,
  Output,
  OnInit,
  ViewChild,
  ElementRef
} from '@angular/core';
import { TabContentComponent } from '../tab-content/tab-content.component';
import { Observable } from 'rxjs';
import { KeyboardState } from '../../../enums/keyboard.enum';
import { KeyboardService } from '../../../services/keyboard.service';

@Component({
  selector: 'tce-tab-box',
  templateUrl: './tab-box.component.html',
  styleUrls: ['./tab-box.component.scss']
})
export class TabBoxComponent implements OnInit, AfterContentInit {
  @ContentChildren(TabContentComponent) tabContent: QueryList<
    TabContentComponent
  >;
  @ViewChild('inputElement', {static:false}) set content(content: ElementRef) {
    if (content) {
      content.nativeElement.focus();
    }
  }

  searchText = '';
  searchActive = false;

  @Input() searchEnabled = false;
  @Input() searchItemSelected: Observable<any>;
  @Output() searchTextChanged = new EventEmitter<string>();

  toggleSearchEmitter = new EventEmitter();

  constructor(private keyboardService: KeyboardService) {}

  ngOnInit() {
    this.toggleSearchEmitter.subscribe(() => {
      this.searchActive = !this.searchActive;
      if (!this.searchActive) {
        this.keyboardService.closeKeyboard();
      }
    });
    if (this.searchItemSelected) {
      this.searchItemSelected.subscribe(() => this.toggleSearch());
    }
  }

  ngAfterContentInit() {
    // get all active tabs
    const activeTabs = this.tabContent.filter(tab => tab.active);

    // if there is no active tab set, activate the first
    if (activeTabs.length === 0) {
      this.selectTab(this.tabContent.first);
    }
  }

  selectTab(tab: TabContentComponent) {
    // deactivate all tabs
    this.tabContent.toArray().forEach(content => (content.active = false));

    // activate the tab the user has clicked on.
    tab.active = true;
  }

  clearSearch(e: Event) {
    e.stopPropagation();
    this.searchText = '';
    this.searchChanged('');
  }

  public toggleSearch() {
    this.searchText = '';
    this.toggleSearchEmitter.emit();
  }

  public searchChanged(value: string) {
    this.searchText = value;
    this.searchTextChanged.emit(this.searchText);
  }

  setKeyboardFocus(e: FocusEvent) {
    const obj = {
      state: KeyboardState.OPEN
    };
    this.keyboardService.openKeyboard(<HTMLElement>e.target, obj);
  }
}
