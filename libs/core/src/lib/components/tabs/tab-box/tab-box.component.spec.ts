import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabBoxComponent } from './tab-box.component';

describe('TabBoxComponent', () => {
  let component: TabBoxComponent;
  let fixture: ComponentFixture<TabBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabBoxComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
