import { Component, OnInit, Input, HostBinding } from '@angular/core';

@Component({
  selector: 'tce-tab-content',
  templateUrl: './tab-content.component.html',
  styleUrls: ['./tab-content.component.scss']
})
export class TabContentComponent implements OnInit {
  @Input() tabTitle: string;
  @Input() active = false;

  @HostBinding('style.display') get display() {
    if (this.active) {
      return '';
    } else {
      return 'none';
    }
  }

  constructor() {}

  ngOnInit() {}
}
