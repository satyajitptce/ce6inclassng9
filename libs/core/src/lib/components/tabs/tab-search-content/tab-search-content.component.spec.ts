import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabSearchContentComponent } from './tab-search-content.component';

describe('TabContentComponent', () => {
  let component: TabSearchContentComponent;
  let fixture: ComponentFixture<TabSearchContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TabSearchContentComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabSearchContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
