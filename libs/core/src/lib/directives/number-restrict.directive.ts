import { Directive, OnDestroy, ElementRef } from '@angular/core';

@Directive({
  selector: '[tceNumberRestrict]'
})
export class NumberRestrictDirective implements OnDestroy {
  constructor(private eRef: ElementRef) {
    this.eRef.nativeElement.addEventListener('keydown', e => {
      const keyPressed = parseInt(e.key, 10);
      const allowedKeys = ['Backspace', 'Delete', 'Enter'];

      if (isNaN(keyPressed) && !allowedKeys.includes(e.key)) {
        e.preventDefault();
      }
    });
  }

  ngOnDestroy() {
    this.eRef.nativeElement.removeEventListener('keydown', () => {});
  }
}
