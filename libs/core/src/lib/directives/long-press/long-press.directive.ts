import {
  Directive,
  HostListener,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';

@Directive({
  selector: '[tceLongPress]'
})
export class LongPressDirective {
  @Output() longPressEvent = new EventEmitter();

  constructor(private eRef: ElementRef) {}

  @HostListener('mousedown', ['$event'])
  longPressMouseDown(event) {
    event.preventDefault();
    this.longPressEvent.emit(event);
    const elementReference = <HTMLElement>this.eRef.nativeElement;
    const mouseOutFunc = e => {
      this.longPressEvent.emit(e);
      elementReference.removeEventListener('mouseout', mouseOutFunc);
    };
    elementReference.addEventListener('mouseout', mouseOutFunc);
  }

  @HostListener('mouseup', ['$event'])
  longPressMouseUp(event) {
    event.preventDefault();
    this.longPressEvent.emit(event);
  }

  @HostListener('touchstart', ['$event'])
  longPressTouchStart(event) {
    //event.preventDefault();
    this.longPressEvent.emit(event);
  }

  @HostListener('touchend', ['$event'])
  longPressTouchEnd(event) {
    // event.preventDefault();
    this.longPressEvent.emit(event);
  }
}
