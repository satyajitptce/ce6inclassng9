import {
  Directive,
  HostListener,
  ElementRef,
  Output,
  EventEmitter
} from '@angular/core';

@Directive({
  selector: '[tceClickedOutside]'
})
export class ClickedOutsideDirective {
  @Output() clickedOutside = new EventEmitter();

  constructor(private eRef: ElementRef) {}

  @HostListener('document:click', ['$event'])
  outsideClickEvent(event: Event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.clickedOutside.emit(event);
    }
  }
}
