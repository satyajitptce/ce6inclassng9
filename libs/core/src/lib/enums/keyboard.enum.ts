export enum KeyboardState {
  OPEN = '1',
  CLOSE = '0'
}

export enum KeyboardTheme {
  DEFAULT = 'default',
  NUMERIC = 'numeric'
}
