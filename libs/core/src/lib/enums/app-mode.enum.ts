export enum ApplicationMode {
  TEACHING = 'teaching',
  PLANNING = 'planning'
}
