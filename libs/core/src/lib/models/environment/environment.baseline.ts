import { version } from '../../../../../../package.json';
import {
  EnvironmentApiBaseUrlType,
  EnvironmentConfig
} from './environment.interface';

/*
Baseline implementation of app-specific Environment Configuration.
*/
export const EnvironmentBaseline: EnvironmentConfig = {
  coreLibVersion: version,
  production: false,
  mockdata: false,
  configFile: '/assets/config.json',
  // earlier base url has phyical url + api, now it is '/api-name' only
  // this change will allow resolving relative api path
  api: {
    globalResponseDelay: 0,
    baseUrls: {
      auth: '../tce-auth-api',
      general: '../tce-teach-api',
      file: '../tce-repo-api',
      fileupload: '../tce-repo-api',
      report: '../tce-report-api',
      school: '../tce-school-api'
    },
    recentViews: {
      url: '/1/api/@apiVersion@/nav/recentviews',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    curriculum: {
      url: '/1/api/@apiVersion@/curriculum',
      baseUrlType: EnvironmentApiBaseUrlType.SCHOOL
    },
    book: {
      url: '/1/api/@apiVersion@/curriculum/book/@bookid@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },

    lessonPlan: {
      url: '/1/api/@apiVersion@/serve/lp/@tpid@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    tpResource: {
      url: '/1/api/@apiVersion@/serve/tp?@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    tqResource: {
      url: '/1/api/@apiVersion@/serve/tq?ids=@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    assetResource: {
      url: '/1/api/@apiVersion@/serve/asset?ids=@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    assetResourceQuestion: {
      url: '/1/api/@apiVersion@/serve/question?ids=@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    getFile: {
      url: '/1/web/@apiVersion@/content/fileservice',
      baseUrlType: EnvironmentApiBaseUrlType.FILE
    },

    clientId: {
      url: '/0/api/@apiVersion@/sso/clientid',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    signIn: {
      url: '/0/api/@apiVersion@/sso/token',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    refreshToken: {
      url: '/0/api/@apiVersion@/sso/token',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    /**
     * TODO:add in mockdata also
     */
    extendSession: {
      url: '/1/api/@apiVersion@/sso/extend',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    organizations: {
      url: '/0/api/@apiVersion@/admin/organizations/tcde',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    organization: {
      url: '/0/api/@apiVersion@/admin/organizations/@query@',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    changePassword: {
      url: '/1/api/@apiVersion@/admin/user/password',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    generatePin: {
      url: '/0/api/@apiVersion@/sso/pin',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    checkPin: {
      url: '/0/api/@apiVersion@/sso/pin/',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    changePin: {
      url: '/1/api/@apiVersion@/admin/user/pin',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    logout: {
      url: '/1/api/@apiVersion@/sso/revoke/@currentRefreshToken@',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    updateResource: {
      url: '/1/api/@apiVersion@/serve/tp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    addResource: {
      url: '/1/api/@apiVersion@/content/package',
      baseUrlType: EnvironmentApiBaseUrlType.FILEUPLOAD
    },
    dynamicCustomResource: {
      url: '/1/api/@apiVersion@/serve/custom/asset',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    dynamicCustomResourceDelete: {
      url: '/1/api/@apiVersion@/serve/custom/asset/@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    saveWb: {
      url: '/1/api/@apiVersion@/serve/wb',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    shareCustomResource: {
      url: '/1/api/@apiVersion@/serve/custom/asset?@ids@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    searchFilter: {
      url: '/1/api/@apiVersion@/content/search?@searchItem@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    searchSuggestion: {
      url: '/1/api/@apiVersion@/content/spell?@searchSuggestion@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    saveCustomBookJSON: {
      url: '/1/api/@apiVersion@/curriculum/custom/book',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    getCustomBookJSON: {
      url: '/1/api/@apiVersion@/curriculum/custom/book/@bookid@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    addCustomChapter: {
      url: '/1/api/@apiVersion@/serve/custom/chp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    editCustomChapter: {
      url: '/1/api/@apiVersion@/serve/custom/chp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    deleteCustomChapter: {
      url: '/1/api/@apiVersion@/serve/custom/chp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    addCustomTopic: {
      url: '/1/api/@apiVersion@/serve/custom/tp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    editCustomTopic: {
      url: '/1/api/@apiVersion@/serve/custom/tp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    deleteCustomTopic: {
      url: '/1/api/@apiVersion@/serve/custom/tp',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    getUserProfile: {
      url: '/1/api/@apiVersion@/admin/user/details',
      baseUrlType: EnvironmentApiBaseUrlType.AUTH
    },
    getQuestionBank: {
      url: '/1/api/@apiVersion@/serve/custom/qb',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    addQuestion: {
      url: '/1/api/@apiVersion@/serve/custom/qb',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    editQuestion: {
      url: '/1/api/@apiVersion@/serve/custom/qb',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    deleteQuestion: {
      url: '/1/api/@apiVersion@/serve/custom/qb/@qbId@',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    subjectList: {
      url: '/1/api/@apiVersion@/curriculum/subjects',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    },
    usageLog: {
      url: '/1/api/@apiVersion@/usage/log',
      baseUrlType: EnvironmentApiBaseUrlType.SCHOOL
    },
    customQuiz: {
      url: '/1/api/@apiVersion@/serve/custom/quiz',
      baseUrlType: EnvironmentApiBaseUrlType.GENERAL
    }
  }
};
