/* These interfaces inform base level environment configuration for individual apps. If there is a requirement for additional properties that
are APP SPECIFIC, there should be specific interfaces inside of that app that extend these to define the app specific interfaces. */

export interface EnvironmentConfig {
  coreLibVersion: any;
  production: boolean;
  mockdata: boolean;
  api: EnvironmentApiConfig;
  configFile?: string;
}

export interface EnvironmentApiConfig {
  globalResponseDelay: number;
  manualErrorTriggerEndpoints?: string[];
  baseUrls: {
    auth: string;
    general: string;
    file: string;
    fileupload: string;
    report: string;
    school: string;
  };
  recentViews: EnvironmentApiUrl;
  curriculum: EnvironmentApiUrl;
  book: EnvironmentApiUrl;

  lessonPlan: EnvironmentApiUrl;
  tpResource: EnvironmentApiUrl;
  tqResource: EnvironmentApiUrl;
  assetResource: EnvironmentApiUrl;
  assetResourceQuestion: EnvironmentApiUrl;
  getFile: EnvironmentApiUrl;

  clientId: EnvironmentApiUrl;
  signIn: EnvironmentApiUrl;
  refreshToken: EnvironmentApiUrl;
  extendSession: EnvironmentApiUrl;
  organizations: EnvironmentApiUrl;
  organization: EnvironmentApiUrl;
  changePassword: EnvironmentApiUrl;
  generatePin: EnvironmentApiUrl;
  checkPin: EnvironmentApiUrl;
  changePin: EnvironmentApiUrl;
  logout: EnvironmentApiUrl;
  updateResource: EnvironmentApiUrl;
  addResource: EnvironmentApiUrl;
  dynamicCustomResource: EnvironmentApiUrl;
  saveWb: EnvironmentApiUrl;
  shareCustomResource: EnvironmentApiUrl;
  dynamicCustomResourceDelete: EnvironmentApiUrl;
  searchFilter: EnvironmentApiUrl;
  searchSuggestion: EnvironmentApiUrl;
  saveCustomBookJSON: EnvironmentApiUrl;
  getCustomBookJSON: EnvironmentApiUrl;
  addCustomChapter: EnvironmentApiUrl;
  editCustomChapter: EnvironmentApiUrl;
  deleteCustomChapter: EnvironmentApiUrl;
  addCustomTopic: EnvironmentApiUrl;
  editCustomTopic: EnvironmentApiUrl;
  deleteCustomTopic: EnvironmentApiUrl;
  getUserProfile: EnvironmentApiUrl;
  getQuestionBank: EnvironmentApiUrl;
  addQuestion: EnvironmentApiUrl;
  editQuestion: EnvironmentApiUrl;
  deleteQuestion: EnvironmentApiUrl;
  subjectList: EnvironmentApiUrl;
  usageLog: EnvironmentApiUrl;
  customQuiz: EnvironmentApiUrl;
}

export interface EnvironmentApiUrl {
  url: string;
  baseUrlType: EnvironmentApiBaseUrlType;
}

export enum EnvironmentApiBaseUrlType {
  AUTH = 'auth',
  GENERAL = 'general',
  FILE = 'file',
  FILEUPLOAD = 'fileupload',
  REPORT = 'report',
  SCHOOL = 'school'
}
