import { EnvironmentConfig } from './environment.interface';
import { EnvironmentBaseline } from './environment.baseline';
import * as merge from 'deepmerge';

/*
Baseline implementation of app-specific Mock Data Environment Configuration. Deep merged from the baseline environment file.
*/
export const MockDataEnvironmentBaseline: EnvironmentConfig = merge(
  EnvironmentBaseline,
  {
    mockdata: true,
    api: {
      baseUrls: {
        general: '/assets/mockdata'
      },
      recentViews: {
        url: '/recentviews/recentviews.json'
      },
      curriculum: {
        url: '/curriculum/curriculum.json'
      },
      book: {
        url: '/book/book.json'
      },
      lessonPlan: {
        url: '/lessonplan/lessonplan.@tpid@.json'
      },
      tpResource: {
        url: '/tp/tp.json'
      },
      tqResource: {
        url: '/tq/tq.json'
      },
      assetResource: {
        url: '/asset/asset.json'
      },
      assetResourceQuestion: {
        url: '/asset/asset_question.json'
      },
      getFile: {
        url: '/0/api/content/filerangeservice'
      },

      clientId: {
        url: '/0/api/@apiVersion@/sso/clientid'
      },
      signIn: {
        url: '/0/api/@apiVersion@/sso/token'
      },
      refreshToken: {
        url: '/0/api/@apiVersion@/sso/token'
      },
      changePassword: {
        url: '/1/api/@apiVersion@/admin/user'
      },
      generatePin: {
        url: '/0/api/@apiVersion@/sso/pin'
      },
      checkPin: {
        url: '/0/api/@apiVersion@/sso/pin/'
      },
      changePin: {
        url: '/1/api/@apiVersion@/admin/user'
      },
      logout: {
        url: '/1/api/@apiVersion@/sso/revoke/@currentRefreshToken@'
      }
    }
  }
);

{
}
