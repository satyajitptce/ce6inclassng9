import { GlobalConfigState } from '../../enums/global-config-state.enum';

export interface AppConfig {
  global_setting: GlobalSettingConfig;
  cmodes: CmodeShape;
  themes: object;
  general: GeneralConfig;
}

export interface GeneralConfig {
  errormsg: GeneralErrorMessageConfig[];
  icons: GeneralIconConfig[];
  welcomemsg: object;
}

export interface GeneralErrorMessageConfig {
  errorcode: string;
  msg: string;
}

export interface GeneralIconConfig {
  subid: string;
  subtilte: string;
  icon: string;
}

export interface ThemeConfig {
  default: object;
  background: string;
  toolbar: object;
}
export interface GlobalSettingConfig {
  modulewhiteboard: GlobalConfigState;
  playerbehaviour: string;
  animation: GlobalConfigState;
  moduleintro: GlobalConfigState;
  moduledate: GlobalConfigState;
  playlistautoclose: GlobalConfigState;
}

export interface CmodeShape {
  0: CmodeConfig;
  1: CmodeConfig;
}

export interface CmodeConfig {
  loginroute: string;
  postloginroute: string;
  toolbar: ToolbarConfig[];
}

export interface ToolbarConfig {
  enabled: number;
  event: string;
  icon: string;
  tooltip?: string;
  onlyif?: object;
}

export interface ResourceThumbnail {
  type: string;
  thumbnail: string;
  tcetypes: Array<string>;
}

export interface AnimationTimingConfig {
  short: number;
  medium: number;
  long: number;
  'x-long': number;
  'delay-x-short': number;
  'delay-medium': number;
}
