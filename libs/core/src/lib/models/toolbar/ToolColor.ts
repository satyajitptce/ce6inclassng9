export enum ToolColor {
  Purple = '#BB86FC',
  Blue = '#71B9FF',
  Orange = '#FFA726',
  Green = '#66FF59',
  Red = '#FF4E4E',
  White = '#FFFFFF',
  Black = '#000000'
}
