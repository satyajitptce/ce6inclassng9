import { HttpErrorResponse } from '@angular/common/http';

export class CoreError {
  userMessageHeader = '';
  userMessage = '';
  message: string;
  context = '';
  stack = '';
  constructor(baseError: Error) {
    this.message = baseError.message;

    if (baseError instanceof HttpErrorResponse) {
      const { url, status, error } = baseError;
      if (error) {
        if (error.errorMessage) {
          this.message = error.errorMessage;
        }
      }
      if (url) {
        if (url.includes('/serve/tp') || url.includes('serve/asset')) {
          this.context = 'resourceListFetch';
        } else if (url.includes('filerange')) {
          this.context = 'resourceItemFetch';
        }
      }

      if (this.context === 'resourceItemFetch') {
        if (baseError.status === 404 || baseError.status === 400) {
          this.userMessageHeader = 'Resource Not Found!';
          this.userMessage =
            'The requested resource could not be loaded because it is not present on the server.';
        }
      } else if (this.context === 'resourceListFetch') {
        this.userMessageHeader = 'Error Getting Resources';
        this.userMessage =
          'The resources for the selected chapter cannot be loaded due to a server error.';
      }
    }
    this.stack = baseError.stack;
    if (!this.context) {
      this.context = 'app';
    }
  }
}

export enum ErrorContext {
  PLAYER = 'player'
}
