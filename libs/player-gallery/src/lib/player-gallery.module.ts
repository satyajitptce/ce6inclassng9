import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImageGalleryComponent } from './component/image-gallery/image-gallery.component';
import { LibConfigModule } from '@tce/lib-config';
import { CoreModule } from '@tce/core';
import { PlayerAnnotationModule } from '@tce/player-annotation-canvas';
import { AngularSvgIconModule } from 'angular-svg-icon';
@NgModule({
  imports: [
    CommonModule,
    LibConfigModule.forChild(ImageGalleryComponent),
    CoreModule,
    PlayerAnnotationModule,
    AngularSvgIconModule
  ],
  declarations: [ImageGalleryComponent],
  exports: [ImageGalleryComponent],
  entryComponents: [ImageGalleryComponent]
})
export class PlayerGalleryModule {}
