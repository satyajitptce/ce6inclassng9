import { async, TestBed } from '@angular/core/testing';
import { PlayerGalleryModule } from './player-gallery.module';

describe('PlayerGalleryModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [PlayerGalleryModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(PlayerGalleryModule).toBeDefined();
  });
});
