import { Component, OnInit } from '@angular/core';

import {
  PlayerAbstractComponent,
  PlayerAbstractInterface,
  PlayerAbstractAnimations,
  ResourceType,
  FullContentSelection,
  FullClassSelection,
  RequestApiService,
  CommonService,
  FileUploadService,
  CurriculumPlaylistService,
  SubjectSelectorService,
  PlayerUsageService
} from '@tce/core';
@Component({
  selector: 'tce-image-gallery',
  templateUrl: './image-gallery.component.html',
  styleUrls: ['./image-gallery.component.scss'],
  animations: PlayerAbstractAnimations
})
export class ImageGalleryComponent extends PlayerAbstractComponent
  implements OnInit, PlayerAbstractInterface {
  currentSrc: any;
  currentPage: any;
  public arrayOfPages: any = [];
  public imageList: any = [];
  showImageContainer: Boolean = false;
  showSettingOption = false;
  gradeSubjectvalue: any;
  topicChaptervalue: any;
  dummyData = [
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05588_nb0dma.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'A',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05466_kwlv0n.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'B',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05621_zgtcco.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'C',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05513_gfbiwi.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'D',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05459_ziuomy.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'E',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05586_oj8jfo.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'F',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05465_dtkwef.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'G',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05626_ytsf3j.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'H',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05449_l9kukz.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'I',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05544_aczrb9.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'J',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814785/photostream-photos',
      fileName: 'DSC05447_mvffor.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'K',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    },
    {
      ansKeyId: 'dummy',
      assetId: 'img-00b83ae7-617d-4c3c-96c9-9dbc912c2486',
      assetType: 'null',
      copyright: 'null',
      description: 'null',
      encryptedFilePath:
        'https://res.cloudinary.com/css-tricks/image/upload/f_auto,q_auto/v1568814783/photostream-photos',
      fileName: 'DSC05620_qfwycq.jpg',
      keywords: 'null',
      lcmsGradeId: 'Fruits',
      lcmsSubjectId: 'Environmental Science',
      mimeType: 'gallery',
      subType: 'null',
      thumbFileName: '',
      title: 'A',
      tpId: 'tp-fdfee93f-ea5f-4853-804d-99e4c5b1ebf7'
    }
  ];

  constructor(
    private requestApiService: RequestApiService,
    private commonService: CommonService,
    private fileUploadService: FileUploadService,
    private curriculumPlaylistService: CurriculumPlaylistService,
    private subjectSelectorService: SubjectSelectorService,
    private playerUsageService: PlayerUsageService
  ) {
    super();
  }

  ngOnInit() {
    this.onEndLoadingResource();

    this.curriculumPlaylistService.fullContentSelection$.subscribe(
      (fullContentSelection: FullContentSelection) => {
        if (fullContentSelection) {
          this.currentFullChapter = fullContentSelection;
          this.topicChaptervalue =
            fullContentSelection.chapter.chapterNumber +
            '.' +
            fullContentSelection.topic.topicNumber +
            ' | ' +
            fullContentSelection.topic.topicTitle;
        }
      }
    );
    this.subjectSelectorService.fullClassSelection$.subscribe(
      (newClassSelection: FullClassSelection) => {
        if (newClassSelection) {
          this.currentFullSubject = newClassSelection;
          this.gradeSubjectvalue =
            newClassSelection.gradeLevel.title +
            ' | ' +
            newClassSelection.subject.title;
        }
      }
    );
  }
  get resourceType() {
    return ResourceType;
  }
  onEndLoadingResource(): void {
    this.isLoading = false;
  }

  beginLoadingResource() {
    let tempImageData: any;
    if (this.resource.metaData.imageData) {
      tempImageData = this.resource.metaData.imageData;
    } else {
      tempImageData = this.resource.metaData;
    }

    if (tempImageData) {
      if (tempImageData.length > 0) {
        // if (this.dummyData) {
        //   for (let j = 0; j < this.dummyData.length; j++) {
        //     tempImageData.push(this.dummyData[j]);
        //   }
        // }

        for (let i = 0; i < tempImageData.length; i++) {
          let url = this.requestApiService.getUrl('getFile');

          const encryptedFilePath = tempImageData[i].encryptedFilePath;
          const fileName = tempImageData[i].fileName;

          if (tempImageData[i].ansKeyId === 'dummy') {
            url = '';
            this.imageList.push(encryptedFilePath + '/' + fileName);
          } else {
            this.imageList.push(url + '/' + encryptedFilePath + '/' + fileName);
          }
          //console.log('url-->> ', url);
          //console.log('filename-->> ', fileName);

          this.arrayOfPages.push(i);
        }
        //console.log( "image list-->> ",this.imageList)
      } else {
        const url = this.requestApiService.getUrl('getFile');
        this.imageList.push(url + '/' + tempImageData.filePath);
        this.arrayOfPages.push(0);
      }
    }
  }

  openImageViewer(currentSelectedPage: number, calee: any) {
    this.playerUsageService.logResourceUsage(this.resource);
    //console.log('doNextBack calee ', calee);
    //console.log('this.imageList-->> ', this.imageList);
    if (currentSelectedPage <= this.imageList.length) {
      this.showImageContainer = true;
      this.currentPage = currentSelectedPage;
      this.currentSrc = this.imageList[currentSelectedPage - 1];
      //console.log("__this.currentSrc__"+this.currentSrc)
    }
  }

  printImage() {
    //console.log("PRINT-->>>")
    this.playerUsageService.logResourceUsage(this.resource);
    let popupWinindow;
    let innerContents = document.getElementById(
      'gallery-player-preview-container'
    ).innerHTML;
    popupWinindow = window.open(
      '',
      '_blank',
      'width=650,height=500,scrollbars=no,menubar=no,toolbar=no,location=no,status=no,titlebar=no'
    );
    popupWinindow.document.open();
    popupWinindow.document.write(
      '<html><head></head><body onload="window.print(); window.close()">' +
        innerContents +
        '</html>'
    );
    popupWinindow.document.close();

    setTimeout(() => popupWinindow.close(), 1000);
  }

  closeImageViewer() {
    this.playerUsageService.logResourceUsage(this.resource);
    this.showImageContainer = false;
  }

  clickToOpenSetting() {
    this.playerUsageService.logResourceUsage(this.resource);
    this.showSettingOption = !this.showSettingOption;
  }
  openCreateLib(resource) {
    this.playerUsageService.logResourceUsage(this.resource);
    console.log('resource-->> ', resource);

    let currentAssetData = resource.metaData[this.currentPage];
    console.log('currentAssetData-->> ', currentAssetData);
    this.clickToOpenSetting();
    const customData = {
      title: currentAssetData.title,
      grade: this.gradeSubjectvalue,
      chapter: this.topicChaptervalue,
      file: currentAssetData.fileName,
      share: resource.isShared,
      type: 'resource-playlist',
      assetId: currentAssetData.assetId,
      customResource: resource
    };
    /*
    const customData = {
      title: resource.title,
      grade: this.gradeSubjectvalue,
      chapter: this.topicChaptervalue,
      file: resource.fileName,
      share: resource.isShared,
      type: 'resource-playlist',
      assetId: resource.resourceId,
      customResource: resource
    };
    */

    this.fileUploadService.setAddResourceFormData(customData);
    this.fileUploadService.setAddResourceFlagBroadcaster('create');
    //this.commonService.setOpenAddResourceFormBrodCast(true);
  }
}
