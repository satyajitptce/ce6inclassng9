module.exports = {
  name: 'player-gallery',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/libs/player-gallery',
  snapshotSerializers: [
    'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
    'jest-preset-angular/build/AngularSnapshotSerializer.js',
    'jest-preset-angular/build/HTMLCommentSerializer.js'
  ]
};
